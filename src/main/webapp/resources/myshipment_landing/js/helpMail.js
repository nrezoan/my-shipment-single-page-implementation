function mailUser() {
	var name = $('#name2').val();
	var email = $('#email2').val();
	var message = $('#message2').val();
	
		if(name == '' && email == '' && message == ''){
    		alert('All fields are blank.')
    		return false;
    	}
		else if(name== ''){
			alert('Please fill the Name Field.')
    		return false;
		}
		else if(email == ''){
    		alert('Please fill Email Field.')
    		return false;
    	}
		else if(message == ''){
    		alert('Please write a message.')
    		return false;
    	}
 		else {
		$('#name2').val('');

		$('#email2').val('');

		$('#message2').val('');

		var x = document.getElementById("snackbar")
		x.className = "show";
		setTimeout(function() {
			x.className = x.className.replace("show", "");
		}, 3000);

		var helpEmailParam = {
			"name2" : name,
			"email2" : email,
			"message2" : message
		}

		$.ajax({
			url : '/MyShipmentFrontApp/helpEmail',
			data : helpEmailParam,
			type : "POST",
			dataType : "json",
			success : function(data) {

			},
		});
	}

}