var headerLength = 0;
var X = XLSX;
var wtf_mode = false;
var XW = {
	/* worker message */
	msg: 'xlsx',
	/* worker scripts */
	rABS: './xlsxworker2.js',
	norABS: './xlsxworker1.js',
	noxfer: './xlsxworker.js'
};

function check() {

	var file = $("#uploadXL")[0].files;
	x = true;
	handleFile(file);
}


$(document).on('click', '.browse', function () {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on(
	'change',
	'.file',
	function () {

		$(this).parent().find('.form-control').val(
			$(this).val().replace(/C:\\fakepath\\/i, ''));
	});


function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for (; l < data.byteLength / w; ++l)
		o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l
			* w + w)));
	o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
	return o;
}

function process_wb(wb) {

	var header = get_header_row(wb.Sheets[wb.SheetNames[0]]);
	headerLength = header.length;

	resetSelectList();

	for (var i = 0; i < header.length; i++) {

		buildSelectList(header, i);
	}
	document.getElementById("hideDiv").style.display = "block";
}

function resetSelectList() {

	var po_select = document.getElementById("po_options");
	po_select.options.length = 1;

	var shipper_select = document.getElementById("shipper_options");
	shipper_select.options.length = 1;

	var total_gw_select = document.getElementById("total_gw_options");
	total_gw_select.options.length = 1;

	var carton_length_select = document.getElementById("carton_length_options");
	carton_length_select.options.length = 1;

	var carton_height_select = document.getElementById("carton_height_options");
	carton_height_select.options.length = 1;

	var carton_width_select = document.getElementById("carton_width_options");
	carton_width_select.options.length = 1;

	var total_pieces_select = document.getElementById("total_pieces_options");
	total_pieces_select.options.length = 1;

	var carton_quantity_select = document.getElementById("carton_quantity_options");
	carton_quantity_select.options.length = 1;

	var carton_unit_select = document.getElementById("carton_unit_options");
	carton_unit_select.options.length = 1;

	var total_cbm_select = document.getElementById("total_cbm_options");
	total_cbm_select.options.length = 1;

	var hs_code_select = document.getElementById("hs_code_options");
	hs_code_select.options.length = 1;

	var commodity_select = document.getElementById("commodity_options");
	commodity_select.options.length = 1;

	var style_no_select = document.getElementById("style_no_options");
	style_no_select.options.length = 1;

	var wh_code_select = document.getElementById("wh_code_options");
	wh_code_select.options.length = 1;

	var size_no_select = document.getElementById("size_no_options");
	size_no_select.options.length = 1;

	var sku_select = document.getElementById("sku_options");
	sku_select.options.length = 1;

	var article_no_select = document.getElementById("article_no_options");
	article_no_select.options.length = 1;

	var color_select = document.getElementById("color_options");
	color_select.options.length = 1;

	var total_nw_select = document.getElementById("total_nw_options");
	total_nw_select.options.length = 1;

	var comm_inv_select = document.getElementById("comm_inv_options");
	comm_inv_select.options.length = 1;

	var comm_inv_date_select = document.getElementById("comm_inv_date_options");
	comm_inv_date_select.options.length = 1;

	var pieces_ctn_select = document.getElementById("pieces_ctn_options");
	pieces_ctn_select.options.length = 1;

	var carton_sr_no_select = document.getElementById("carton_sr_no_options");
	carton_sr_no_select.options.length = 1;

	var reference_1_select = document.getElementById("reference_1_options");
	reference_1_select.options.length = 1;

	var reference_2_select = document.getElementById("reference_2_options");
	reference_2_select.options.length = 1;

	var reference_3_select = document.getElementById("reference_3_options");
	reference_3_select.options.length = 1;

	var terms_of_shipment_select = document.getElementById("terms_of_shipment_options");
	terms_of_shipment_select.options.length = 1;

	var freight_mode_select = document.getElementById("freight_mode_options");
	freight_mode_select.options.length = 1;

	var division_select = document.getElementById("division_options");
	division_select.options.length = 1;

	var pol_select = document.getElementById("pol_options");
	pol_select.options.length = 1;

	var pod_select = document.getElementById("pod_options");
	pod_select.options.length = 1;

	var place_of_delivery_select = document.getElementById("place_of_delivery_options");
	place_of_delivery_select.options.length = 1;

	var place_of_delivery_address_select = document.getElementById("place_of_delivery_address_options");
	place_of_delivery_address_select.options.length = 1;

	var cargo_handover_date_select = document.getElementById("cargo_handover_date_options");
	cargo_handover_date_select.options.length = 1;

	var fvsl_select = document.getElementById("fvsl_options");
	fvsl_select.options.length = 1;

	var mvsl1_select = document.getElementById("mvsl1_options");
	mvsl1_select.options.length = 1;

	var mvsl2_select = document.getElementById("mvsl2_options");
	mvsl2_select.options.length = 1;

	var mvsl3_select = document.getElementById("mvsl3_options");
	mvsl3_select.options.length = 1;

	var voyage1_select = document.getElementById("voyage1_options");
	voyage1_select.options.length = 1;

	var voyage2_select = document.getElementById("voyage2_options");
	voyage2_select.options.length = 1;

	var etd_select = document.getElementById("etd_options");
	etd_select.options.length = 1;

	var transhipment1etd_select = document.getElementById("transhipment1etd_options");
	transhipment1etd_select.options.length = 1;

	var transhipment2etd_select = document.getElementById("transhipment2etd_options");
	transhipment2etd_select.options.length = 1;

	var ata_select = document.getElementById("ata_options");
	ata_select.options.length = 1;

	var atd_select = document.getElementById("atd_options");
	atd_select.options.length = 1;

	var transhipment1eta_select = document.getElementById("transhipment1eta_options");
	transhipment1eta_select.options.length = 1;

	var transhipment2eta_select = document.getElementById("transhipment2eta_options");
	transhipment2eta_select.options.length = 1;

	var eta = document.getElementById("eta_options");
	eta_select.options.length = 1;
}


function get_header_row(sheet) {

	var headers = [];
	var range = XLSX.utils.decode_range(sheet['!ref']);
	var C, R = range.s.r; /* start in the first row */
	/* walk every column in the range */
	for (C = range.s.c; C <= range.e.c; ++C) {
		var cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })] /* find the cell in the first row */

		var hdr = "UNKNOWN " + C; // <-- replace with your desired default 
		if (cell && cell.t) hdr = XLSX.utils.format_cell(cell);

		headers.push(hdr);
	}
	console.log(headers);
	return headers;
}

function buildSelectList(header, count) {

	var po_select = document.getElementById("po_options");
	po_select.appendChild(buildSelect(header, count));

	var shipper_select = document.getElementById("shipper_options");
	shipper_select.appendChild(buildSelect(header, count));

	var total_gw_select = document.getElementById("total_gw_options");
	total_gw_select.appendChild(buildSelect(header, count));

	var carton_length_select = document.getElementById("carton_length_options");
	carton_length_select.appendChild(buildSelect(header, count));

	var carton_height_select = document.getElementById("carton_height_options");
	carton_height_select.appendChild(buildSelect(header, count));

	var carton_width_select = document.getElementById("carton_width_options");
	carton_width_select.appendChild(buildSelect(header, count));

	var total_pieces_select = document.getElementById("total_pieces_options");
	total_pieces_select.appendChild(buildSelect(header, count));

	var carton_quantity_select = document.getElementById("carton_quantity_options");
	carton_quantity_select.appendChild(buildSelect(header, count));

	var carton_unit_select = document.getElementById("carton_unit_options");
	carton_unit_select.appendChild(buildSelect(header, count));

	var total_cbm_select = document.getElementById("total_cbm_options");
	total_cbm_select.appendChild(buildSelect(header, count));

	var hs_code_select = document.getElementById("hs_code_options");
	hs_code_select.appendChild(buildSelect(header, count));

	var commodity_select = document.getElementById("commodity_options");
	commodity_select.appendChild(buildSelect(header, count));

	var style_no_select = document.getElementById("style_no_options");
	style_no_select.appendChild(buildSelect(header, count));

	var wh_code_select = document.getElementById("wh_code_options");
	wh_code_select.appendChild(buildSelect(header, count));

	var size_no_select = document.getElementById("size_no_options");
	size_no_select.appendChild(buildSelect(header, count));

	var sku_select = document.getElementById("sku_options");
	sku_select.appendChild(buildSelect(header, count));

	var article_no_select = document.getElementById("article_no_options");
	article_no_select.appendChild(buildSelect(header, count));

	var color_select = document.getElementById("color_options");
	color_select.appendChild(buildSelect(header, count));

	var total_nw_select = document.getElementById("total_nw_options");
	total_nw_select.appendChild(buildSelect(header, count));

	var comm_inv_select = document.getElementById("comm_inv_options");
	comm_inv_select.appendChild(buildSelect(header, count));

	var comm_inv_date_select = document.getElementById("comm_inv_date_options");
	comm_inv_date_select.appendChild(buildSelect(header, count));

	var pieces_ctn_select = document.getElementById("pieces_ctn_options");
	pieces_ctn_select.appendChild(buildSelect(header, count));

	var carton_sr_no_select = document.getElementById("carton_sr_no_options");
	carton_sr_no_select.appendChild(buildSelect(header, count));

	var reference_1_select = document.getElementById("reference_1_options");
	reference_1_select.appendChild(buildSelect(header, count));

	var reference_2_select = document.getElementById("reference_2_options");
	reference_2_select.appendChild(buildSelect(header, count));

	var reference_3_select = document.getElementById("reference_3_options");
	reference_3_select.appendChild(buildSelect(header, count));

	var terms_of_shipment_select = document.getElementById("terms_of_shipment_options");
	terms_of_shipment_select.appendChild(buildSelect(header, count));

	var freight_mode_select = document.getElementById("freight_mode_options");
	freight_mode_select.appendChild(buildSelect(header, count));

	var division_select = document.getElementById("division_options");
	division_select.appendChild(buildSelect(header, count));

	var pol_select = document.getElementById("pol_options");
	pol_select.appendChild(buildSelect(header, count));

	var pod_select = document.getElementById("pod_options");
	pod_select.appendChild(buildSelect(header, count));

	var place_of_delivery_select = document.getElementById("place_of_delivery_options");
	place_of_delivery_select.appendChild(buildSelect(header, count));

	var place_of_delivery_address_select = document.getElementById("place_of_delivery_address_options");
	place_of_delivery_address_select.appendChild(buildSelect(header, count));

	var cargo_handover_date_select = document.getElementById("cargo_handover_date_options");
	cargo_handover_date_select.appendChild(buildSelect(header, count));

}


function buildSelect(header, count) {

	var option = document.createElement('option');
	option.innerHTML = header[count];
	option.value = header[count];
	return option;

}

function buildJson() {

	var po_select = document.getElementById("po_options");
	var po_number = po_select.options[po_select.selectedIndex].value;

	var shipper_select = document.getElementById("shipper_options");
	var shipper_number = shipper_select.options[shipper_select.selectedIndex].value;

	var total_gw_select = document.getElementById("total_gw_options");
	var total_gw = total_gw_select.options[total_gw_select.selectedIndex].value;

	var carton_length_select = document.getElementById("carton_length_options");
	var carton_length = carton_length_select.options[carton_length_select.selectedIndex].value;

	var carton_height_select = document.getElementById("carton_height_options");
	var carton_height = carton_height_select.options[carton_height_select.selectedIndex].value;

	var carton_width_select = document.getElementById("carton_width_options");
	var carton_width = carton_width_select.options[carton_width_select.selectedIndex].value;

	var total_pieces_select = document.getElementById("total_pieces_options");
	var total_pieces = total_pieces_select.options[total_pieces_select.selectedIndex].value;

	var carton_quantity_select = document.getElementById("carton_quantity_options");
	var carton_quantity = carton_quantity_select.options[carton_quantity_select.selectedIndex].value;

	var carton_unit_select = document.getElementById("carton_unit_options");
	var carton_unit = carton_unit_select.options[carton_unit_select.selectedIndex].value;

	var total_cbm_select = document.getElementById("total_cbm_options");
	var total_cbm = total_cbm_select.options[total_cbm_select.selectedIndex].value;

	var hs_code_select = document.getElementById("hs_code_options");
	var hs_code = hs_code_select.options[hs_code_select.selectedIndex].value;

	var commodity_select = document.getElementById("commodity_options");
	var commodity = commodity_select.options[commodity_select.selectedIndex].value;

	var style_no_select = document.getElementById("style_no_options");
	var style_no = style_no_select.options[style_no_select.selectedIndex].value;

	var wh_code_select = document.getElementById("wh_code_options");
	var wh_code = wh_code_select.options[wh_code_select.selectedIndex].value;

	var size_no_select = document.getElementById("size_no_options");
	var size_no = size_no_select.options[size_no_select.selectedIndex].value;

	var sku_select = document.getElementById("sku_options");
	var sku = sku_select.options[sku_select.selectedIndex].value;

	var article_no_select = document.getElementById("article_no_options");
	var article_no = article_no_select.options[article_no_select.selectedIndex].value;

	var color_select = document.getElementById("color_options");
	var color = color_select.options[color_select.selectedIndex].value;

	var total_nw_select = document.getElementById("total_nw_options");
	var total_nw = total_nw_select.options[total_nw_select.selectedIndex].value;

	var comm_inv_select = document.getElementById("comm_inv_options");
	var comm_inv = comm_inv_select.options[comm_inv_select.selectedIndex].value;

	var comm_inv_date_select = document.getElementById("comm_inv_date_options");
	var comm_inv_date = comm_inv_date_select.options[comm_inv_date_select.selectedIndex].value;

	var pieces_ctn_select = document.getElementById("pieces_ctn_options");
	var pieces_ctn = pieces_ctn_select.options[pieces_ctn_select.selectedIndex].value;

	var carton_sr_no_select = document.getElementById("carton_sr_no_options");
	var carton_sr_no = carton_sr_no_select.options[carton_sr_no_select.selectedIndex].value;

	var reference_1_select = document.getElementById("reference_1_options");
	var reference_1 = reference_1_select.options[reference_1_select.selectedIndex].value;

	var reference_2_select = document.getElementById("reference_2_options");
	var reference_2 = reference_2_select.options[reference_2_select.selectedIndex].value;

	var reference_3_select = document.getElementById("reference_3_options");
	var reference_3 = reference_3_select.options[reference_3_select.selectedIndex].value;

	var terms_of_shipment_select = document.getElementById("terms_of_shipment_options");
	var terms_of_shipment = terms_of_shipment_select.options[terms_of_shipment_select.selectedIndex].value;

	var freight_mode_select = document.getElementById("freight_mode_options");
	var freight_mode = freight_mode_select.options[freight_mode_select.selectedIndex].value;

	var division_select = document.getElementById("division_options");
	var division = division_select.options[division_select.selectedIndex].value;

	var pol_select = document.getElementById("pol_options");
	var pol = pol_select.options[pol_select.selectedIndex].value;

	var pod_select = document.getElementById("pod_options");
	var pod = pod_select.options[pod_select.selectedIndex].value;

	var place_of_delivery_select = document.getElementById("place_of_delivery_options");
	var place_of_delivery = place_of_delivery_select.options[place_of_delivery_select.selectedIndex].value;

	var place_of_delivery_address_select = document.getElementById("place_of_delivery_address_options");
	var place_of_delivery_address = place_of_delivery_address_select.options[place_of_delivery_address_select.selectedIndex].value;

	var cargo_handover_date_select = document.getElementById("cargo_handover_date_options");
	var cargo_handover_date = cargo_handover_date_select.options[cargo_handover_date_select.selectedIndex].value;

	model = {
		po_number: po_number,
		shipper: shipper_number,
		total_gw: total_gw,
		carton_length: carton_length,
		carton_height: carton_height,
		carton_width: carton_width,
		total_pieces: total_pieces,
		carton_quantity: carton_quantity,
		carton_unit: carton_unit,
		total_cbm: total_cbm,
		hs_code: hs_code,
		commodity: commodity,
		style_no: style_no,
		wh_code: wh_code,
		size_no: size_no,
		sku: sku,
		article_no: article_no,
		color: color,
		total_nw: total_nw,
		comm_inv: comm_inv,
		comm_inv_date: comm_inv_date,
		pieces_ctn: pieces_ctn,
		carton_sr_no: carton_sr_no,
		reference_1: reference_1,
		reference_2: reference_2,
		reference_3: reference_3,
		terms_of_shipment: terms_of_shipment,
		freight_mode: freight_mode,
		division: division,
		pol: pol,
		pod: pod,
		place_of_delivery: place_of_delivery,
		place_of_delivery_address: place_of_delivery_address,
		cargo_handover_date: cargo_handover_date,

		//added on 12 November 2018 by Tahmid Alam.

		fvsl: fvsl,
		mvsl1: mvsl1,
		mvsl2: mvsl2,
		mvsl3: mvsl3,
		voyage1: voyage1,
		voyage2: voyage2,
		voyage3: voyage3,
		etd: etd,
		transhipment1etd: transhipment1etd,
		transhipment2etd: transhipment2etd,
		ata: ata,
		atd: atd,
		transhipment1eta: transhipment1eta,
		transhipment2eta: transhipment2eta,
		eta: eta
	}

	console.log(model);
	var data = JSON.stringify(model);
	console.log(data);
	var $overlay = $.LoadingOverlay("show");
	var templateMapper = $.ajax({
		type: 'POST',
		url: myContextPath + "/saveTemplate",
		data: data,
		contentType: "application/json",

		success: function (resultData, textStatus, xhr) {
			$.LoadingOverlay("hide");
			console.log("Successfully Sent");
			if (xhr.status === 201) {
				document.getElementById("savemsg").innerHTML = "Template Saved Successfully!!!";
				$("#templateSaveSuccessModal").modal();
			}
		},
		error: function (resultData, textStatus, xhr) {
			$.LoadingOverlay("hide");
			if (textStatus === "error" || resultData === 400) {
				document.getElementById("msgBody").innerHTML = "Template for this Buyer already exists!!!";
				$("#templateExistsModal").modal();
				/*window.location.replace(myContextPath + "/sendTemplate");*/
			}
		},
	});

}


function handleFile(files) {
	/* var files = e.target.files; */
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function (e) {
			// if(typeof console !== 'undefined') console.log("onload", new
			// Date(), rABS, use_worker);
			var data = e.target.result;
			var wb;
			var arr = fixdata(data);
			wb = X.read(btoa(arr), {
				type: 'base64'
			});
			process_wb(wb);
		};
		// if(rABS) reader.readAsBinaryString(f);
		reader.readAsArrayBuffer(f);
	}
}
function rebuildJson() {

	var po_select = document.getElementById("po_options");
	var po_number = po_select.options[po_select.selectedIndex].value;

	var shipper_select = document.getElementById("shipper_options");
	var shipper_number = shipper_select.options[shipper_select.selectedIndex].value;

	var total_gw_select = document.getElementById("total_gw_options");
	var total_gw = total_gw_select.options[total_gw_select.selectedIndex].value;

	var carton_length_select = document.getElementById("carton_length_options");
	var carton_length = carton_length_select.options[carton_length_select.selectedIndex].value;

	var carton_height_select = document.getElementById("carton_height_options");
	var carton_height = carton_height_select.options[carton_height_select.selectedIndex].value;

	var carton_width_select = document.getElementById("carton_width_options");
	var carton_width = carton_width_select.options[carton_width_select.selectedIndex].value;

	var total_pieces_select = document.getElementById("total_pieces_options");
	var total_pieces = total_pieces_select.options[total_pieces_select.selectedIndex].value;

	var carton_quantity_select = document.getElementById("carton_quantity_options");
	var carton_quantity = carton_quantity_select.options[carton_quantity_select.selectedIndex].value;

	var carton_unit_select = document.getElementById("carton_unit_options");
	var carton_unit = carton_unit_select.options[carton_unit_select.selectedIndex].value;

	var total_cbm_select = document.getElementById("total_cbm_options");
	var total_cbm = total_cbm_select.options[total_cbm_select.selectedIndex].value;

	var hs_code_select = document.getElementById("hs_code_options");
	var hs_code = hs_code_select.options[hs_code_select.selectedIndex].value;

	var commodity_select = document.getElementById("commodity_options");
	var commodity = commodity_select.options[commodity_select.selectedIndex].value;

	var style_no_select = document.getElementById("style_no_options");
	var style_no = style_no_select.options[style_no_select.selectedIndex].value;

	var wh_code_select = document.getElementById("wh_code_options");
	var wh_code = wh_code_select.options[wh_code_select.selectedIndex].value;

	var size_no_select = document.getElementById("size_no_options");
	var size_no = size_no_select.options[size_no_select.selectedIndex].value;

	var sku_select = document.getElementById("sku_options");
	var sku = sku_select.options[sku_select.selectedIndex].value;

	var article_no_select = document.getElementById("article_no_options");
	var article_no = article_no_select.options[article_no_select.selectedIndex].value;

	var color_select = document.getElementById("color_options");
	var color = color_select.options[color_select.selectedIndex].value;

	var total_nw_select = document.getElementById("total_nw_options");
	var total_nw = total_nw_select.options[total_nw_select.selectedIndex].value;

	var comm_inv_select = document.getElementById("comm_inv_options");
	var comm_inv = comm_inv_select.options[comm_inv_select.selectedIndex].value;

	var comm_inv_date_select = document.getElementById("comm_inv_date_options");
	var comm_inv_date = comm_inv_date_select.options[comm_inv_date_select.selectedIndex].value;

	var pieces_ctn_select = document.getElementById("pieces_ctn_options");
	var pieces_ctn = pieces_ctn_select.options[pieces_ctn_select.selectedIndex].value;

	var carton_sr_no_select = document.getElementById("carton_sr_no_options");
	var carton_sr_no = carton_sr_no_select.options[carton_sr_no_select.selectedIndex].value;

	var reference_1_select = document.getElementById("reference_1_options");
	var reference_1 = reference_1_select.options[reference_1_select.selectedIndex].value;

	var reference_2_select = document.getElementById("reference_2_options");
	var reference_2 = reference_2_select.options[reference_2_select.selectedIndex].value;

	var reference_3_select = document.getElementById("reference_3_options");
	var reference_3 = reference_3_select.options[reference_3_select.selectedIndex].value;

	var terms_of_shipment_select = document.getElementById("terms_of_shipment_options");
	var terms_of_shipment = terms_of_shipment_select.options[terms_of_shipment_select.selectedIndex].value;

	var freight_mode_select = document.getElementById("freight_mode_options");
	var freight_mode = freight_mode_select.options[freight_mode_select.selectedIndex].value;

	var division_select = document.getElementById("division_options");
	var division = division_select.options[division_select.selectedIndex].value;

	var pol_select = document.getElementById("pol_options");
	var pol = pol_select.options[pol_select.selectedIndex].value;

	var pod_select = document.getElementById("pod_options");
	var pod = pod_select.options[pod_select.selectedIndex].value;

	var place_of_delivery_select = document.getElementById("place_of_delivery_options");
	var place_of_delivery = place_of_delivery_select.options[place_of_delivery_select.selectedIndex].value;

	var place_of_delivery_address_select = document.getElementById("place_of_delivery_address_options");
	var place_of_delivery_address = place_of_delivery_address_select.options[place_of_delivery_address_select.selectedIndex].value;

	var cargo_handover_date_select = document.getElementById("cargo_handover_date_options");
	var cargo_handover_date = cargo_handover_date_select.options[cargo_handover_date_select.selectedIndex].value;

	model = {
		po_number: po_number,
		shipper: shipper_number,
		total_gw: total_gw,
		carton_length: carton_length,
		carton_height: carton_height,
		carton_width: carton_width,
		total_pieces: total_pieces,
		carton_quantity: carton_quantity,
		carton_unit: carton_unit,
		total_cbm: total_cbm,
		hs_code: hs_code,
		commodity: commodity,
		style_no: style_no,
		wh_code: wh_code,
		size_no: size_no,
		sku: sku,
		article_no: article_no,
		color: color,
		total_nw: total_nw,
		comm_inv: comm_inv,
		comm_inv_date: comm_inv_date,
		pieces_ctn: pieces_ctn,
		carton_sr_no: carton_sr_no,
		reference_1: reference_1,
		reference_2: reference_2,
		reference_3: reference_3,
		terms_of_shipment: terms_of_shipment,
		freight_mode: freight_mode,
		division: division,
		pol: pol,
		pod: pod,
		place_of_delivery: place_of_delivery,
		place_of_delivery_address: place_of_delivery_address,
		cargo_handover_date: cargo_handover_date,
		//added on 12 November 2018 by Tahmid Alam.

		fvsl: fvsl,
		mvsl1: mvsl1,
		mvsl2: mvsl2,
		mvsl3: mvsl3,
		voyage1: voyage1,
		voyage2: voyage2,
		voyage3: voyage3,
		etd: etd,
		transhipment1etd: transhipment1etd,
		transhipment2etd: transhipment2etd,
		ata: ata,
		atd: atd,
		transhipment1eta: transhipment1eta,
		transhipment2eta: transhipment2eta,
		eta: eta
	}

	console.log(model);
	var data = JSON.stringify(model);
	console.log(data);

	$.LoadingOverlay("show");
	var templateMapper = $.ajax({
		type: 'POST',
		url: myContextPath + "/updateTemplate",
		data: data,
		contentType: "application/json",

		success: function (resultData, textStatus, xhr) {
			$.LoadingOverlay("hide");
			console.log("Successfully Sent");
			//  window.location = xhr.getResponseHeader("Location");
			if (xhr.status === 201) {
				$("#templateSaveSuccessModal").modal();
				/*                var link = xhr.getResponseHeader("Location");
								var win = window.open(link, '_blank');
								win.focus();*/
			}
		},

		error: function (resultData, textStatus, xhr) {
			$.LoadingOverlay("hide");
			if (textStatus === "error" || resultData === 400) {
				$("#templateUpdateErrorModal").modal();
			}
		},
	});

}

function openConfimationBox() {
	$("#templateUploadConfirmation").modal();
}