function disableFields() {
	var blNumber = $("#hblNumber").val();
	var fromDate = document.getElementById("fromDate");
	var toDate = document.getElementById("toDate");
	var billStatus = document.getElementById("billStatus");
	if (blNumber == "" || blNumber == null || blNumber == undefined) {
		fromDate.disabled = false;
		toDate.disabled = false;
		billStatus.disabled = false;
	} else {
		fromDate.disabled = true;
		toDate.disabled = true;
		billStatus.disabled = true;
	}
}

function mandatoryValidation(){
	var fromDate = $("#fromDate").val();
	var toDate = $("#toDate").val();
	if(document.getElementById("billStatus").value == "2" || document.getElementById("billStatus").value == "3" ){
		if (fromDate == null || fromDate == ""||toDate == null || toDate == "") {
			swal("Dates","Please provide From & To Date for searching Cleared/All invoices", "error")
			return false;
		}
		else{
			return true;
		}
	}
}