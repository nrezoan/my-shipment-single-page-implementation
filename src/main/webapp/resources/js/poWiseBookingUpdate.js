function finishUpdate() {
    var checkboxes = document.getElementsByName("checking");
    var checkboxesChecked = [];
    var poDetails = [];
    // loop over them all
    for (var count = 1; ; count++) {

        var poNumber = document.getElementById("po_no_" + count);
        if (poNumber === null) {
            break;
        }

        model = {
            shipper: document.getElementById("shipper_" + count).value,
            division: document.getElementById("division_" + count).value,
            cargo_handover_date: document.getElementById("cargo_handover_date_" + count).value,
            comm_inv: document.getElementById("comm_inv_" + count).value,
            comm_inv_date: document.getElementById("comm_inv_date_" + count).value,
            freight_mode: document.getElementById("freight_mode_" + count).value,
            terms_of_shipment: document.getElementById("terms_of_shipment_" + count).value,
            app_id: document.getElementById("app_id_" + count).value,
            po_no: document.getElementById("po_no_" + count).value,
            total_gw: document.getElementById("total_gw_" + count).value,
            carton_length: document.getElementById("carton_length_"
                + count).value,
            carton_height: document.getElementById("carton_height_"
                + count).value,
            carton_width: document.getElementById("carton_width_" + count).value,
            carton_quantity: document.getElementById("carton_quantity_"
                + count).value,
            total_cbm: document.getElementById("total_cbm_" + count).value,
            total_nw: document.getElementById("total_nw_" + count).value,
            total_pieces: document.getElementById("total_pieces_" + count).value,
            style_no: document.getElementById("style_no_" + count).value,
            color: document.getElementById("color_" + count).value,
            hs_code: document.getElementById("hs_code_" + count).value,
            unit: document.getElementById("unit_" + count).value,
            size_no: document.getElementById("size_no_" + count).value,
            article_no: document.getElementById("article_no_" + count).value,
            department: document.getElementById("department_" + count).value,
            wh_code: document.getElementById("wh_code_" + count).value,
            sku: document.getElementById("sku_" + count).value,
            total_nw: document.getElementById("total_nw_" + count).value,
            carton_unit: document.getElementById("carton_unit_" + count).value,
            commodity: document.getElementById("commodity_" + count).value,
            reference_1: document.getElementById("reference_1_" + count).value,
            reference_2: document.getElementById("reference_2_" + count).value,
            reference_3: document.getElementById("reference_3_" + count).value
            //reference_4: document.getElementById("reference_4_"+count).value,
        }
        console.log(model);
        poDetails.push(model);
    }
    console.log(checkboxesChecked);


    // if (checkMandatoryForBooking(poDetails)) {
    var data = JSON.stringify(poDetails);
    $.ajax({
        type: 'POST',
        url: myContextPath + "/poForUpdate",
        data: data,
        contentType: "application/json",
        success: function (resultData, textStatus, xhr) {
            console.log("Successfully Sent");
            if (xhr.status === 201) {
                $("#poUpdateStatus").modal();
               // window.location = myContextPath + '/poUpdatePage';
                $.LoadingOverlay("hide");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.LoadingOverlay("hide");
        }

    });
    // Return the array if it is non-empty, or null

    // }
}

function deletePO(appID,count){
	console.log(appID);
	console.log(count);
	
	var data = JSON.stringify(appID);
/*	$("#po_search_"+count).remove();
    $("#expand_"+count).remove();*/
	$.ajax({
        type: 'POST',
        url: myContextPath + "/deletePO",
        data: data,
        contentType: "application/json",
        success: function (resultData, textStatus, xhr) {
            console.log("Successfully Sent");
       $("#po_search_"+count).remove();
       $("#expand_"+count).remove();
            
            if (xhr.status === 201) {
                $("#poDeleteModal").modal();
                $.LoadingOverlay("hide");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.LoadingOverlay("hide");
        }

    });
}