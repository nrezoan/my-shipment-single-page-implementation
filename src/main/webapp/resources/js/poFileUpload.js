// bind the on-change event for the input element (triggered when a file
// is chosen)
// $(document).ready(function () {
// 	$("#upload-file-input").on("change", uploadFile);
// });



$(document).on('click', '.browse', function () {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on(
	'change',
	'.file',
	function () {

		$(this).parent().find('.form-control').val(
			$(this).val().replace(/C:\\fakepath\\/i, ''));
	});

function uploadFile() {

	var templateType = document.querySelector('input[name="tempType"]:checked').value;

	var link = "/fileUpload";
	
	if(templateType === "general"){
		link = "/general/fileUpload";
	}

	var file = new FormData($('#upload-file-form')[0]);
	
	var dateFormat = document.getElementById("dateformat").value;
	//var other_data = dateFormat.serialize();
	file.append("dateFormat", dateFormat);
	let formdata = new FormData();
	file.append("dateFormat", dateFormat);
	formdata.append("file", $('#upload-file-form')[0]);
	formdata.append("dateFormat",dateFormat);
	 $.LoadingOverlay("show");
	$.ajax({
			url: myContextPath + link,
			type: "POST",
			//data: new FormData($("#upload-file-form")[0]),
			data: file,
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			cache: false,
	/*		success: function (resultData, textStatus, xhr) {
				console.log("Successfully Sent");
				//  window.location = xhr.getResponseHeader("Location");
				if (xhr.status === 201) {

					$("#modal").modal();
					document.getElementById("modalParagraph").innerHTML = "Order Uploaded Successfully";
				}
			},
			error: function (resultData, textStatus, xhr) {

				if (resultData.status === 400) {
					$("#modal").modal();
					document.getElementById("modalParagraph").innerHTML = resultData.responseText;
				}
			},*/
			success: function (resultData, textStatus, xhr) {
	            console.log("Successfully Sent");
	            $.LoadingOverlay("hide");
	            //  window.location = xhr.getResponseHeader("Location");
	            if (xhr.status === 201) {
	                $("#fileUploadSuccessModal").modal();
	            }
	        },

			 error: function (resultData, textStatus, xhr) {
				 	$.LoadingOverlay("hide");
		            if (resultData.status === 404) {
		                $("#fileUploadErrorModal").modal();
		                document.getElementById("msgBody2").innerHTML= "Data upload Unsuccessful!Template for the buyer does not exist. Please upload template";
		            }else if (resultData.status === 400) {
		            	 $("#fileUploadErrorModal").modal();
		            	 document.getElementById("msgBody2").innerHTML= "Data Upload Unsuccessful!!!";
		            	 
		            }
		        },

			/*failure: function (resultData) {
				console.log("Error Occured");
			}*/
		});
} // function uploadFile

/*function addTemplate() {
	$
		.ajax({
			url: myContextPath + "/sendTemplate",
			type: "GET",
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			cache: false,
			success: function (resultData, textStatus, xhr) {
				console.log("Successfully Sent");
				//  window.location = xhr.getResponseHeader("Location");
				if (xhr.status === 201) {

					$("#modal").modal();
					document.getElementById("modalParagraph").innerHTML = "Order Uploaded Successfully";
				}
			},
			error: function (resultData, textStatus, xhr) {

				if (resultData.status === 400) {
					$("#modal").modal();
					document.getElementById("modalParagraph").innerHTML = resultData.responseText;
				}
			},

			failure: function (resultData) {
				console.log("Error Occured");
			}
		});
}*/
function loadFileUploadPage() {
	location.reload();
}

function showConfirmationBox(){
	$("#poUploadConfirmation").modal();
}

function downloadGeneralTemplate(){

	var templateType = document.querySelector('input[name="tempType"]:checked').value;

	if(templateType ==="general"){
		document.getElementById("addNewTemp").style.display = "none";
		document.getElementById("showDown").style.display = "block";
	}else{
		document.getElementById("showDown").style.display = "none";
		document.getElementById("addNewTemp").style.display = "block";
	}

}

function uploadFileByBuyer() {

	var link = "/fileUpload";

	var file = new FormData($('#upload-file-form')[0]);

	var dateFormat = document.getElementById("dateformat").value;
	//var other_data = dateFormat.serialize();
	file.append("dateFormat", dateFormat);
	let formdata = new FormData();
	file.append("dateFormat", dateFormat);
	formdata.append("file", $('#upload-file-form')[0]);
	formdata.append("dateFormat", dateFormat);
	$.LoadingOverlay("show");
	$
			.ajax({
				url : myContextPath + link,
				type : "POST",
				//data: new FormData($("#upload-file-form")[0]),
				data : file,
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false,
				cache : false,
				success : function(resultData, textStatus, xhr) {
					console.log("Successfully Sent");
					$.LoadingOverlay("hide");
					//  window.location = xhr.getResponseHeader("Location");
					if (xhr.status === 201) {
						$("#fileUploadSuccessModal").modal();
					}
				},

				error : function(resultData, textStatus, xhr) {
					$.LoadingOverlay("hide");
					if (resultData.status === 404) {
						$("#fileUploadErrorModal").modal();
						document.getElementById("msgBody2").innerHTML = "Data upload Unsuccessful!Template for the buyer does not exist. Please upload template";
					} else if (resultData.status === 400) {
						$("#fileUploadErrorModal").modal();
						document.getElementById("msgBody2").innerHTML = "Data Upload Unsuccessful!!!";

					}
				},
			});
}