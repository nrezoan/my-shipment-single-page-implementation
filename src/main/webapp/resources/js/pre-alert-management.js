var errorMessageColor = "#dd4b39";
var successMessageColor = "#252525";



$(document).ready(function() {
	
});

$(document).on('click', '.browse', function() {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on(
		'change',
		'.file',
		function() {

			$(this).parent().find('.form-control').val(
					$(this).val().replace(/C:\\fakepath\\/i, ''));
		});

var isPDF = function(name) {
    return name.match(/pdf$/i)
};
    
var isXLS = function(name) {
    return name.match(/xlsx$/i)
};

function jsonObjectToArray(temp_Object) {
	var temp_Array = new Array();
	for ( var name in temp_Object) {
		temp_Array[name] = temp_Object[name];
	}
	return temp_Array;
}

function setUploadFilesValue(blNo, consignee, agent, isPreAlertDocExists) {
	$("#blNo").text(blNo);
	$("#consignee").text(consignee);
	$("#agent").text(agent);
	
	$("#bl-number").val(blNo);
	if(isPreAlertDocExists == "") {
		isPreAlertDocExists = directBookingIsPreAlertDocExist;
	}
	$("#isPreAlertDocExists").val(isPreAlertDocExists);
	preAlertDocumentStatus(isPreAlertDocExists);
}

function setUploadFilesValueMBL(blNo, consignee, agent, isPreAlertDocExists) {
	$("#mblNo").text(blNo);
	$("#mbl-consignee").text(consignee);
	$("#mbl-agent").text(agent);
	
	$("#mbl-number").val(blNo);
	
	$("#isPreAlertMblExists").val(isPreAlertDocExists);
	preAlertDocumentStatusMBL(isPreAlertDocExists);
}

function setDownloadFilesValueMBL(blNo, consignee, agent, isPreAlertDocExists) {
	$("#mblNo").text(blNo);
	$("#mbl-consignee").text(consignee);
	$("#mbl-agent").text(agent);
	
	$("#mbl-number").val(blNo);
	
	$("#isPreAlertMblExists").val(isPreAlertDocExists);
	$("#mblDownload").attr("href", "/MyShipmentFrontApp/downloadMBL?mblNumber="+blNo);
}

function setUploadFilesValueMBLWithAgent(blNo, consignee, agent, isPreAlertDocExists, agentId) {
	$("#mblNo").text(blNo);
	$("#mbl-consignee").text(consignee);
	$("#mbl-agent").text(agent);
	
	$("#mbl-number").val(blNo);
	
	//hamid
	$("#agentIdInput").val(agentId);
	
	$("#isPreAlertMblExists").val(isPreAlertDocExists);
	preAlertDocumentStatusMBL(isPreAlertDocExists);
}

function setViewIplValue(blNo, commInvNo, agent, isPreAlertDocExists) {
	$("#blNoIpl").text(blNo);
	$("#commercialInvoiceNumber").text(commInvNo);
	$("#agent").text(agent);
	
	$("#bl-number-ipl").val(blNo);
	
	if(isPreAlertDocExists == '0') {
		swal({
			title : "No File Found!",
			text : "Please Upload Commercial Invoice and Packing List.",
			type : "error",
			showCancelButton : false,
			confirmButtonColor : "#dd4b39",
			confirmButtonText : "OK",
			cancelButtonText : "",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				swal.close();
			}
		});
		//$('#viewIplModal').modal('toggle');
		return false;
	} else {
		$("#viewIplModal").modal({
			show: 'false'
		});
		$("#displayIplFrame").attr("src", "/MyShipmentFrontApp/showPdfIPL?blNumber="+blNo+"#zoom=100");
	}
}

function preAlertDocumentStatus(isPreAlertDocExists) {
	if(isPreAlertDocExists == '0') {
		$("#preAlertDocStatus").text("Please Upload Necessary Pre Alert Documents");
		$("#preAlertDocStatus").css("color", errorMessageColor);
	} else if(isPreAlertDocExists == '1') {
		$("#preAlertDocStatus").text("Document Exists\n\nN.B: Uploading Files again will replace the existing document.");
		$("#preAlertDocStatus").html($("#preAlertDocStatus").html().replace(/\n/g,'<br/>'));
		$("#preAlertDocStatus").css("color", successMessageColor);
	}
}

function preAlertDocumentStatusMBL(isPreAlertDocExists) {
	if(isPreAlertDocExists == '0') {
		$("#preAlertMblStatus").text("Please Upload MBL/MAWB to Generate Pre-Alert");
		$("#preAlertMblStatus").css("color", errorMessageColor);
	} else if(isPreAlertDocExists == '1') {
		$("#preAlertMblStatus").text("MBL/MAWB Exists\n\nN.B: Uploading Files again will replace the existing document.");
		$("#preAlertMblStatus").html($("#preAlertMblStatus").html().replace(/\n/g,'<br/>'));
		$("#preAlertMblStatus").css("color", successMessageColor);
	}
}

function uploadCommercialInvoice() {
	var file = $('[name="commercialInvoiceFile"]');
	var blNumber = $('#bl-number').val();
	var filename = $.trim(file.val());
	
	var fileType = "IPL";
	
	var isPreAlertDocExists = $('#isPreAlertDocExists').val();
	
	if(filename == "") {
		swal('No File Found','Please browse a PDF file to upload ...', 'error');
        return;
	}
	
	if(!(isPDF(filename))) {
        swal('Wrong File Format','Please browse a PDF file to upload ...', 'error');
        return;
    }
	
	if(isPreAlertDocExists == '0') {
		sendPreAlertDocuments(blNumber, fileType);
	} else if(isPreAlertDocExists == '1') {
		swal({
			title : "Are You Sure!",
			text : "Do you want to replace existing documents?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#009688",
			cancelButtonColor : "#f44336",
			confirmButtonText : "Yes",
			cancelButtonText : "Cancel",
			closeOnConfirm : false,
			closeOnCancel : true,
			showLoaderOnConfirm : true
		}, function(isConfirm) {
			sendPreAlertDocuments(blNumber, fileType);
		});
	}
	
	
	return false;
}

function uploadMasterBL() {
	var file = $('[name="masterBlFile"]');
	var blNumber = $('#mbl-number').val();
	var filename = $.trim(file.val());
	
	//hamid
	var inpAgentId = $('#agentIdInput').val();
	if(inpAgentId == "") {
		swal('Agent\'s SAP ID Empty','Please enter respective Agent\'s SAP ID', 'error');
        return;
	} else if(inpAgentId.length < 10) {
		swal('Agent\'s SAP ID Not Valid','Please enter valid Agent\'s SAP ID', 'error');
        return;
	} else if(!(inpAgentId.startsWith("104"))) {
		swal('Agent\'s SAP ID Not Valid','Please enter valid Agent\'s SAP ID', 'error');
        return;
	}
	
	var fileType = "MST";
	
	var isPreAlertMblExists = $('#isPreAlertMblExists').val();
	
	if(filename == "") {
		swal('No File Found','Please browse a PDF file to upload ...', 'error');
        return;
	}
	
	if(!(isPDF(filename))) {
        swal('Wrong File Format','Please browse a PDF file to upload ...', 'error');
        return;
    }
	
	if(isPreAlertMblExists == '0') {
		sendPreAlertDocuments(blNumber, fileType, inpAgentId);
	} else if(isPreAlertMblExists == '1') {
		swal({
			title : "Are You Sure!",
			text : "Do you want to replace existing documents?",
			type : "warning",
			showCancelButton : true,
			confirmButtonColor : "#009688",
			cancelButtonColor : "#f44336",
			confirmButtonText : "Yes",
			cancelButtonText : "Cancel",
			closeOnConfirm : false,
			closeOnCancel : true,
			showLoaderOnConfirm : true
		}, function(isConfirm) {
			sendPreAlertDocuments(blNumber, fileType, inpAgentId);
		});
	}
	
	
	return false;
}

function sendPreAlertDocuments(blNumber, fileType, inpAgentId) {
	var oMyForm = new FormData();
	if(fileType == "IPL") {
		oMyForm.append("file", commercialInvoiceFile.files[0]);
	} else if(fileType == "MST") {
		oMyForm.append("file", masterBlFile.files[0]);
	}
	

	$.ajax({
		url : 'uploadPreAlertDocs',
		data : oMyForm,
		beforeSend: function(request) {
		    request.setRequestHeader("bl-number", blNumber);
		    request.setRequestHeader("file-type", fileType);
		    request.setRequestHeader("agent-id", inpAgentId);
		},
		dataType : 'json',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			
			if(fileType == "IPL") {
				$("#commercialInvoiceFile").val("").clone(true);
				$("#commercialInvoiceFileText").val("");
			} else if(fileType == "MST") {
				$("#masterBlFile").val("").clone(true);
				$("#masterBlFileText").val("");
			}
			
			swal({
				title : "Updated!",
				text : "File Uploaded Successfully",
				type : "success",
				showCancelButton : false,
				confirmButtonColor : "#367fa9",
				confirmButtonText : "OK",
				cancelButtonText : "",
				closeOnConfirm : false,
				closeOnCancel : false
			}, function(isConfirm) {
				if (isConfirm) {
					if(fileType == "IPL") {
						preAlertDocumentStatus(data.isPreAlertDoc);
						if(typeof(preAlertManagement) != "undefined") {
							if(typeof(preAlertTableList) != "undefined") {
								for(var i=0; i<preAlertTableList.length; i++) {
									if(preAlertTableList[i].preAlertHbl.hblNumber == data.hblNumber) {
										preAlertTableList[i].preAlertHbl.isPreAlertDoc = data.isPreAlertDoc;
										$("#preAlertTable").DataTable().destroy();
										generatePreAlertTable(preAlertTableList);
										$('#preAlertTable').DataTable();
										$("#displayIplFrame").attr("src", "");
										break;
									}
								}
							} else {
								$.each(preAlertMBLTableList, function(key, value) {
									var updatedList = false;
									for(var i=0; i<preAlertMBLTableList[key].length; i++) {
										if(preAlertMBLTableList[key][i].preAlertHbl.hblNumber == data.hblNumber) {
											preAlertMBLTableList[key][i].preAlertHbl.isPreAlertDoc = data.isPreAlertDoc;
											updatedList = true;
											break;
										}
									}
									if(updatedList) {
										generatePreAlertTableMBL(preAlertMBLTableList);
										return;
									}
									
								});
							}
							
						} else {
							directBookingIsPreAlertDocExist = '1';
							$('#preAlertDocUploadHints').text('(File Uploaded.)');
						}
					} else if(fileType == "MST") {
						preAlertDocumentStatusMBL(data.isMbl);
						if(typeof(preAlertManagement) != "undefined") {
							for(var i=0; i<preAlertMBLTableList.length; i++) {
								if(preAlertMBLTableList[i].preAlertHbl.hblNumber == data.hblNumber) {
									preAlertTableList[i].preAlertHbl.isPreAlertDoc = data.isPreAlertDoc;
									//$("#preAlertTable").DataTable().destroy();
									generatePreAlertTableMBL(preAlertMBLTableList);
									//$('#preAlertTable').DataTable();
									//$("#displayIplFrame").attr("src", "");
									break;
								}
							}
						}
					}
					swal.close();
					//$('#updateFormModal	').modal('toggle');
				}
			});
		},
		error : function(data, textStatus, xhr) {
			swal({
				title : "Error Occured",
				text : data.responseText,
				type : "error",
				showCancelButton : false,
				confirmButtonColor : "#dd4b39",
				confirmButtonText : "OK",
				cancelButtonText : "",
				closeOnConfirm : false,
				closeOnCancel : false
			}, function(isConfirm) {
				if (isConfirm) {
					swal.close();
				}
			});
			return false;
		}
	});
}

function uploadPackingList() {
	var file = $('[name="packingListFile"]');
	var blNumber = $('#bl-number').val();
	var filename = $.trim(file.val());
	if (!(isPDF(filename) || isXLS(filename))) {
        swal('Wrong File Format','Please browse a PDF/XLS file to upload ...', 'error');
        return;
    }
	
	var oMyForm = new FormData();
	oMyForm.append("file", packingListFile.files[0]);

	$.ajax({
		url : 'uploadPreAlertDocs',
		data : oMyForm,
		beforeSend: function(request) {
		    request.setRequestHeader("bl-number", blNumber);
		    request.setRequestHeader("file-type", "PAD");
		},
		dataType : 'text',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function(data) {
			$("#packingListFile").val("").clone(true);
			$("#packingListText").val("");
	    
			swal({
				title : "Updated!",
				text : data,
				type : "success",
				showCancelButton : false,
				confirmButtonColor : "#367fa9",
				confirmButtonText : "OK",
				cancelButtonText : "",
				closeOnConfirm : false,
				closeOnCancel : false
			}, function(isConfirm) {
				if (isConfirm) {
					swal.close();
				}
			});
		},
		error : function(data, textStatus, xhr) {
			swal({
				title : "Error Occured",
				text : data.responseText,
				type : "error",
				showCancelButton : false,
				confirmButtonColor : "#dd4b39",
				confirmButtonText : "OK",
				cancelButtonText : "",
				closeOnConfirm : false,
				closeOnCancel : false
			}, function(isConfirm) {
				if (isConfirm) {
					swal.close();
				}
			});
			return false;
		}
	});
	return false;
}

function searchHBLpreAlertDoc() {
	var hblNumber = $("#hblNumberSearchInput").val();
	if(hblNumber == "") {
		swal({
			title : "HBL/HAWB Empty!",
			text : "Please enter a valid HBL/HAWB number",
			type : "error",
			showCancelButton : false,
			confirmButtonColor : "#dd4b39",
			confirmButtonText : "OK",
			cancelButtonText : "",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				swal.close();
				
			}
		});
		return false;
	} else {
		$.LoadingOverlay("show");
		$.ajax({
			url : 'getPreAlertDetailsHBLwise',
			data : {blNumber:hblNumber},
			dataType : 'json',
			type : 'POST',
			success : function(data) {
				$("#preAlertTable").DataTable().destroy();
				preAlertTableList = data;
				generatePreAlertTable(data);
				$('#preAlertTable').DataTable();
				$.LoadingOverlay("hide");
			},
			error : function(data, textStatus, xhr) {
				$.LoadingOverlay("hide");
				swal({
					title : "Error Occured",
					text : data.responseText,
					type : "error",
					showCancelButton : false,
					confirmButtonColor : "#dd4b39",
					confirmButtonText : "OK",
					cancelButtonText : "",
					closeOnConfirm : false,
					closeOnCancel : false
				}, function(isConfirm) {
					if (isConfirm) {
						swal.close();
					}
				});
				return false;
			}
		});
	}
	$.LoadingOverlay("hide");			//hamid
	return false;
}

function searchMBLpreAlertDoc() {
	var mblNumber = $("#mblNumberSearchInput").val();
	if(mblNumber == "") {
		swal({
			title : "MBL/MAWB Empty!",
			text : "Please enter a valid MBL/MAWB number",
			type : "error",
			showCancelButton : false,
			confirmButtonColor : "#dd4b39",
			confirmButtonText : "OK",
			cancelButtonText : "",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				swal.close();
				
			}
		});
		return false;
	} else {
		$.LoadingOverlay("show");
		$.ajax({
			url : 'getPreAlertDetailsMBLwise',
			data : {blNumber:mblNumber},
			dataType : 'json',
			type : 'POST',
			success : function(data) {
				//$("#preAlertTableMBL").DataTable().destroy();
				preAlertTableList = data;
				generatePreAlertTableMBL(data);
				$('#preAlertTableMBL').DataTable();
				$.LoadingOverlay("hide");
			},
			error : function(data, textStatus, xhr) {
				$.LoadingOverlay("hide");
				swal({
					title : "Error Occured",
					text : data.responseText,
					type : "error",
					showCancelButton : false,
					confirmButtonColor : "#dd4b39",
					confirmButtonText : "OK",
					cancelButtonText : "",
					closeOnConfirm : false,
					closeOnCancel : false
				}, function(isConfirm) {
					if (isConfirm) {
						swal.close();
					}
				});
				return false;
			}
		});
	}
	
	return false;
}

function showIPL(isPreAlertDoc, filePathPreAlertDoc, hblNumber) {
	if(isPreAlertDoc == '0') {
		swal({
			title : "Not Found!",
			text : "Please upload commercial invoice and packing list.",
			type : "error",
			showCancelButton : false,
			confirmButtonColor : "#dd4b39",
			confirmButtonText : "OK",
			cancelButtonText : "",
			closeOnConfirm : false,
			closeOnCancel : false
		}, function(isConfirm) {
			if (isConfirm) {
				swal.close();
			}
		});
	} else {
		if(filePathPreAlertDoc != 'undefined') {
			$.ajax({
				url : 'showPdfIPL',
				data : {blNumber:hblNumber},
				dataType : 'application/pdf',
				type : 'POST',
				success : function(data, textStatus, xhr) {
					$.LoadingOverlay("hide");
				},
				error : function(data, textStatus, xhr) {
					$.LoadingOverlay("hide");
					swal({
						title : "Error Occured",
						text : data.responseText,
						type : "error",
						showCancelButton : false,
						confirmButtonColor : "#dd4b39",
						confirmButtonText : "OK",
						cancelButtonText : "",
						closeOnConfirm : false,
						closeOnCancel : false
					}, function(isConfirm) {
						if (isConfirm) {
							swal.close();
						}
					});
					return false;
				}
			});
		}
	}
}

function generatePreAlertTable(tableDataArray) {
	//var tableDataArray = preAlertTableList;
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var tableDataHTML = '';
	
	var no_data_available = '<td class="text-center" colspan="6"><a class="not-found">No HBL/HAWB Found</a></td>';
	
	if (tableDataArray.length < 1) {
		tableDataHTML = tr_start + no_data_available + tr_end;
	} else {
		for (var i = 0; i < tableDataArray.length; i++) {
			tableDataHTML = tableDataHTML + tr_start;
			
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.bl_no + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.buyer + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.comm_inv + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.status + td_end;
			if(tableDataArray[i].preAlertHbl.isPreAlertDoc == '0') {
				tableDataHTML = tableDataHTML + td_start + '<span class="badge" style="background-color: #f44336;">Pending</span>' + td_end;
			} else if(tableDataArray[i].preAlertHbl.isPreAlertDoc == '1') {
				tableDataHTML = tableDataHTML + td_start + '<span class="badge" style="background-color: #00a65a;">Completed</span>' + td_end;
			}
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
				+'<a data-toggle="modal" data-target="#updateFormModal" class="button btn btn-null btn-sm" onclick="setUploadFilesValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.buyer+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
				+'<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload'
				+'</a>'
				+'</td>';
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
			+'<a class="button btn btn-primary btn-sm" id="showIpl" onclick="setViewIplValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.comm_inv+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
			+'<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View'
			+'</a>'
			+'</td>';
			
			//onclick="showIPL(\''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.filePathPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.hblNumber+'\');"
			
			tableDataHTML = tableDataHTML + tr_end;
		}
	}
	
	$('#preAlertTable').find('tbody').empty().append(tableDataHTML);
}

function generateHblTableMblWise(tableDataArray) {
	//var tableDataArray = preAlertTableList;
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var tableDataHTML = '';
	
	var no_data_available = '<td class="text-center" colspan="6"><a class="not-found">No HBL/HAWB Found</a></td>';
	
	if (tableDataArray.length < 1) {
		tableDataHTML = tr_start + no_data_available + tr_end;
	} else {
		for (var i = 0; i < tableDataArray.length; i++) {
			tableDataHTML = tableDataHTML + tr_start;
			
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.bl_no + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.buyer + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.comm_inv + td_end;
			if(tableDataArray[i].preAlertHbl.isPreAlertDoc == '0') {
				tableDataHTML = tableDataHTML + td_start + '<span class="badge" style="background-color: #f44336;">Pending</span>' + td_end;
			} else if(tableDataArray[i].preAlertHbl.isPreAlertDoc == '1') {
				tableDataHTML = tableDataHTML + td_start + '<span class="badge" style="background-color: #00a65a;">Completed</span>' + td_end;
			}
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
				+'<a data-toggle="modal" data-target="#updateFormModal" class="button btn btn-null btn-sm" onclick="setUploadFilesValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.buyer+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
				+'<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload'
				+'</a>'
				+'</td>';
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
			+'<a class="button btn btn-primary btn-sm" id="showIpl" onclick="setViewIplValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.comm_inv+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
			+'<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View'
			+'</a>'
			+'</td>';
			
			//onclick="showIPL(\''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.filePathPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.hblNumber+'\');"
			
			tableDataHTML = tableDataHTML + tr_end;
		}
	}
	
	return tableDataHTML;
}

function generateAgentHblTableMblWise(tableDataArray) {
	//var tableDataArray = preAlertTableList;
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var tableDataHTML = '';
	
	var no_data_available = '<td class="text-center" colspan="6"><a class="not-found">No HBL/HAWB Found</a></td>';
	
	if (tableDataArray.length < 1) {
		tableDataHTML = tr_start + no_data_available + tr_end;
	} else {
		for (var i = 0; i < tableDataArray.length; i++) {
			tableDataHTML = tableDataHTML + tr_start;
			
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.bl_no + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.buyer + td_end;
			tableDataHTML = tableDataHTML + td_start + tableDataArray[i].trackHeaderBean.comm_inv + td_end;
			tableDataHTML = tableDataHTML + td_start + '<span class="badge" style="background-color: #00a65a;">Completed</span>' + td_end;
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
			+'<a class="button btn btn-primary btn-sm" id="showIpl" onclick="setViewIplValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.comm_inv+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
			+'<i class="fa fa-eye" aria-hidden="true"></i>&nbsp;View'
			+'</a>'
			+'</td>';
			
			tableDataHTML = tableDataHTML + tr_end;
		}
	}
	
	return tableDataHTML;
}

function generatePreAlertTableMBL(tableDataArray) {
	//var tableDataArray = preAlertTableList;
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var th_start = '<th style="text-align: center;">';
	var th_end = '</th>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var masterList = Object.keys(tableDataArray);
	
	var tableDataHTML = '';
	
	var no_data_available = '<td class="text-center" colspan="6"><a class="not-found">No MBL/MAWB Found</a></td>';
	
	if (masterList.length < 1) {
		tableDataHTML = tr_start + no_data_available + tr_end;
	} else {
		for (var i = 0; i < masterList.length; i++) {
			var tempTableDataArray = tableDataArray[masterList[i]];
			tableDataHTML = tableDataHTML + tr_start;
			
			tableDataHTML = tableDataHTML + td_start + masterList[i] + td_end;
			tableDataHTML = tableDataHTML + td_start + tempTableDataArray[0].trackHeaderBean.buyer + td_end;
			//tableDataHTML = tableDataHTML + td_start + '<div class="col-md-2"></div><div class="input-group col-md-8"><input type="text" class="form-control col-md-4" id="agentIdInput" name="agentIdInput" placeholder="Agent ID"><span class="input-group-btn"><button class="btn" style="height: 34px; font-size: 13px; background-color: #009688; color: #fff; border-color: #009688;">Save Agent</button></span></div><div class="col-md-2"></div>' + td_end;
			/*tableDataHTML = tableDataHTML + td_start + tempTableDataArray[0].trackHeaderBean.load_dt + td_end;
			tableDataHTML = tableDataHTML + td_start + tempTableDataArray[0].trackHeaderBean.eta_dt + td_end;*/
			
			var iplStatus = false;
			var mblStatus = false;
			var agentIdExist = false;
			
			for(var j = 0; j < tempTableDataArray.length; j++) {
				if(tempTableDataArray[j].preAlertHbl.isPreAlertDoc == '0') {
					iplStatus = false;
					break;
				} else if(tempTableDataArray[j].preAlertHbl.isPreAlertDoc == '1') {
					iplStatus = true;
				}
			}
			
			for(var j = 0; j < tempTableDataArray.length; j++) {
				if(typeof(tempTableDataArray[j].preAlertHbl.agentId) == 'undefined') {
					agentIdExist = false;
					break;
				} else  {
					agentIdExist = true;
				}
			}
			
/*			if(agentIdExist == false) {
				//tableDataHTML = tableDataHTML + td_start + '<div class="col-md-2"></div><div class="input-group col-md-8"><input type="text" class="form-control col-md-4" id="agentIdInput" name="agentIdInput" placeholder="Enter Agent\'s SAP ID"><span class="input-group-btn"><button class="btn" required="true" style="height: 34px; font-size: 13px; background-color: #009688; color: #fff; border-color: #009688;">Save Agent</button></span></div><div class="col-md-2"></div>' + td_end;
				tableDataHTML = tableDataHTML
						+ td_start
						+ '<div class="col-md-2"></div><div class="input-group col-md-8"><input type="text" class="form-control col-md-4" id="agentIdInput" name="agentIdInput" required="true" placeholder="Enter Agent\'s SAP ID"><span class="input-group-btn"><button class="btn get-master" id="MyButton" style="height: 34px; font-size: 13px; background-color: #009688; color: #fff; border-color: #009688;">Save Agent</button></span></div><div class="col-md-2"></div>'
						+ td_end;
			} else {
				tableDataHTML = tableDataHTML
						+ td_start
						+ '<div class="col-md-2"></div><div class="input-group col-md-8"><span type="text" class="form-control" id="agentIdText">'
						+ tempTableDataArray[0].preAlertHbl.agentId
						+ '</span><span class="input-group-btn"><button class="btn" style="height: 34px; font-size: 13px; background-color: #a89fb9; color: #fff; border-color: #a89fb9;">Change Agent</button></span></div><div class="col-md-2"></div>'
						+ td_end;
			}*/
			
			if(iplStatus) {
				if(agentIdExist) {
					tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
					+'<a data-toggle="modal" data-target="#updateFormModalMBL" class="button btn btn-success btn-sm" onclick="setUploadFilesValueMBLWithAgent(\''+masterList[i]+'\', \''+tempTableDataArray[0].trackHeaderBean.buyer+'\', \''+tempTableDataArray[0].trackHeaderBean.agent+'\', \''+tempTableDataArray[0].preAlertHbl.isMBL+'\',  \''+tempTableDataArray[0].preAlertHbl.agentId+'\')">'
					+'<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload MBL/MAWB'
					+'</a>'
					+'</td>';
				}
				else {
					tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
					+'<a data-toggle="modal" data-target="#updateFormModalMBL" class="button btn btn-success btn-sm" onclick="setUploadFilesValueMBL(\''+masterList[i]+'\', \''+tempTableDataArray[0].trackHeaderBean.buyer+'\', \''+tempTableDataArray[0].trackHeaderBean.agent+'\', \''+tempTableDataArray[0].preAlertHbl.isMBL+'\')">'
					+'<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload MBL/MAWB'
					+'</a>'
					+'</td>';
				}
			}
			else {
				tableDataHTML = tableDataHTML + td_start + '<span class="btn btn-danger btn-sm" style="background-color: #f44336;">Incomplete</span>' + td_end;
			}
			
			
			/*tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
				+'<a data-toggle="modal" data-target="#updateFormModal" class="button btn btn-null btn-sm" onclick="setUploadFilesValue(\''+tableDataArray[i].trackHeaderBean.bl_no+'\', \''+tableDataArray[i].trackHeaderBean.buyer+'\', \''+tableDataArray[i].trackHeaderBean.agent+'\', \''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\')">'
				+'<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload'
				+'</a>'
				+'</td>';*/
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
			+'<a class="button btn btn-null btn-sm" data-toggle="collapse" href="#sub_'+i+'" onclick="hideexpand(this);" aria-expanded="false" id="showIpl">'
			+'<span id="showHideHBL">HBL/HAWB</span>&nbsp;<i id="bl_sub_'+i+'" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></i>'
			+'</a>'
			+'</td>';
			
			//onclick="showIPL(\''+tableDataArray[i].preAlertHbl.isPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.filePathPreAlertDoc+'\', \''+tableDataArray[i].preAlertHbl.hblNumber+'\');"
			
			tableDataHTML = tableDataHTML + tr_end;
			tableDataHTML = tableDataHTML + tr_start + '<td colspan="6">';
			var subTableDataHTML = generateSubTable(tempTableDataArray, i);
			
			tableDataHTML = tableDataHTML + subTableDataHTML;
			
			tableDataHTML = tableDataHTML + td_end + tr_end;
		}
	}
	//generateHblTableMblWise
	$('#preAlertTableMBL').find('tbody').empty().append(tableDataHTML);
}

function generateSubTable(tempTableDataArray, count) {
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var th_start = '<th style="text-align: center;">';
	var th_end = '</th>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var subTableDataHTML = '<div class="collapse" id="sub_'+count+'">';
	subTableDataHTML = subTableDataHTML + '<table class="table table-striped" id="preAlertHblTable" style="width:90%; margin-left: 5%;">';
	subTableDataHTML = subTableDataHTML + '<thead style="">';
	subTableDataHTML = subTableDataHTML + tr_start;
	subTableDataHTML = subTableDataHTML + th_start + 'HBL/HAWB' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Consignee' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Commercial Invoice' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Status' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + th_end;
	subTableDataHTML = subTableDataHTML + th_start + th_end;
	subTableDataHTML = subTableDataHTML + tr_end;
	subTableDataHTML = subTableDataHTML + '</thead>';
	subTableDataHTML = subTableDataHTML + '<tbody>';
	subTableDataHTML = subTableDataHTML + generateHblTableMblWise(tempTableDataArray);
	subTableDataHTML = subTableDataHTML + '</tbody>';
	subTableDataHTML = subTableDataHTML + '</table>';
	subTableDataHTML = subTableDataHTML + '</div>';
	
	return subTableDataHTML;
}

function generateAgentHBLTabel(tempTableDataArray, count) {
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var th_start = '<th style="text-align: center;">';
	var th_end = '</th>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var subTableDataHTML = '<div class="collapse" id="sub_'+count+'">';
	subTableDataHTML = subTableDataHTML + '<table class="table table-striped" id="preAlertHblTable" style="width:90%; margin-left: 5%;">';
	subTableDataHTML = subTableDataHTML + '<thead style="">';
	subTableDataHTML = subTableDataHTML + tr_start;
	subTableDataHTML = subTableDataHTML + th_start + 'HBL/HAWB' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Consignee' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Commercial Invoice' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + 'Status' + th_end;
	subTableDataHTML = subTableDataHTML + th_start + th_end;
	subTableDataHTML = subTableDataHTML + tr_end;
	subTableDataHTML = subTableDataHTML + '</thead>';
	subTableDataHTML = subTableDataHTML + '<tbody>';
	subTableDataHTML = subTableDataHTML + generateAgentHblTableMblWise(tempTableDataArray);
	subTableDataHTML = subTableDataHTML + '</tbody>';
	subTableDataHTML = subTableDataHTML + '</table>';
	subTableDataHTML = subTableDataHTML + '</div>';
	
	return subTableDataHTML;
}

function hideexpand(varobj) {
	var res = varobj.href.split("#");
	var status = varobj.getAttribute('aria-expanded');
	
	if(status == "false") {
		$('#bl_'+res[1]).removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
		//$('#showHideHBL').text("Hide HBL/HAWB");
	} else if(status = "true"){
		$('#bl_'+res[1]).removeClass('glyphicon glyphicon-chevron-up').addClass('glyphicon glyphicon-chevron-down');
		//$('#showHideHBL').text("Show HBL/HAWB");
	} else {
		$('#bl_'+res[1]).removeClass('glyphicon glyphicon-chevron-down').addClass('glyphicon glyphicon-chevron-up');
		//$('#showHideHBL').text("Hide HBL/HAWB");
	}
}
function hideexpand_dep(varobj) {
	var res = varobj.href.split("#");
	
	var $myGroup = $('#preAlertTableMBLBody');


	$($myGroup)
			.find('.collapse')
			.each(
					function() {
						if (res[1] == $(this).attr('id')) {

							$(this).closest("tr").toggleClass("hide");
							$(this).toggle();

							if (document.getElementById("#" + res[1]).className == "glyphicon glyphicon-chevron-down") {
								document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-up";
							} else {
								document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-down";
							}

						} else {
						}
					});

}

function generatePreAlertDownloadTableMBL(tableDataArray) {
	//var tableDataArray = preAlertTableList;
	var tr_start = '<tr>';
	var tr_end = '</tr>';
	var th_start = '<th style="text-align: center;">';
	var th_end = '</th>';
	var td_start = '<td style="text-align: center;">';
	var td_end = '</td>';
	
	var masterList = Object.keys(tableDataArray);
	
	var tableDataHTML = '';
	
	var no_data_available = '<td class="text-center" colspan="6"><a class="not-found">No MBL/MAWB Found</a></td>';
	
	if (masterList.length < 1) {
		tableDataHTML = tr_start + no_data_available + tr_end;
	} else {
		for (var i = 0; i < masterList.length; i++) {
			var tempTableDataArray = tableDataArray[masterList[i]];
			tableDataHTML = tableDataHTML + tr_start;
			
			tableDataHTML = tableDataHTML + td_start + masterList[i] + td_end;
			tableDataHTML = tableDataHTML + td_start + tempTableDataArray[0].trackHeaderBean.buyer + td_end;
			tableDataHTML = tableDataHTML + td_start +tempTableDataArray[0].preAlertHbl.updatedAt + td_end;
			//tableDataHTML = tableDataHTML + td_start +tempTableDataArray[0].trackHeaderBean.shipper + td_end;
			
			var iplStatus = false;
			var mblStatus = false;
			
			for(var j = 0; j < tempTableDataArray.length; j++) {
				if(tempTableDataArray[j].preAlertHbl.isPreAlertDoc == '0') {
					iplStatus = false;
					break;
				} else if(tempTableDataArray[j].preAlertHbl.isPreAlertDoc == '1') {
					iplStatus = true;
				}
			}
			
			if(iplStatus) {
				tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
				+'<a class="button btn btn-success btn-sm" id="mblDownload" onclick="setDownloadFilesValueMBL(\''+masterList[i]+'\', \''+tempTableDataArray[0].trackHeaderBean.buyer+'\', \''+tempTableDataArray[0].trackHeaderBean.agent+'\', \''+tempTableDataArray[0].preAlertHbl.isMBL+'\')">'
				+'<i class="fa fa-download" aria-hidden="true"></i>&nbsp;Download MBL/MAWB'
				+'</a>'
				+'</td>';
			} else {
				tableDataHTML = tableDataHTML + td_start + '<span class="btn btn-danger btn-sm" style="background-color: #f44336;">Incomplete</span>' + td_end;
			}
			
			tableDataHTML = tableDataHTML + '<td style="text-align: center; width: 5%;">'
			+'<a class="button btn btn-null btn-sm" data-toggle="collapse" href="#sub_'+i+'" onclick="hideexpand(this);" aria-expanded="false" id="showIpl">'
			+'<span id="showHideHBL">HBL/HAWB</span>&nbsp;<i id="bl_sub_'+i+'" class="glyphicon glyphicon-chevron-down" aria-hidden="true"></i>'
			+'</a>'
			+'</td>';
			
			tableDataHTML = tableDataHTML + tr_end;
			tableDataHTML = tableDataHTML + tr_start + '<td colspan="6">';
			var subTableDataHTML = generateAgentHBLTabel(tempTableDataArray, i);
			
			tableDataHTML = tableDataHTML + subTableDataHTML;
			
			tableDataHTML = tableDataHTML + td_end + tr_end;
		}
	}
	//generateHblTableMblWise
	$('#preAlertTableMBL').find('tbody').empty().append(tableDataHTML);
	
}
