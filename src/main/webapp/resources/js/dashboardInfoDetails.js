function zoom() {
	document.body.style.zoom = "90%";

}
function drawChart(dataArray, chartId) {
	//var dataArray = createDataArray(details);
	$
			.getScript(
					'resources/js/canvasjsmin.js',
					function() {
						$("#"+chartId)
								.CanvasJSChart(
										{

											backgroundColor : null,
											title : {
											/*
											 * text: "Buyer Wise Shipment",
											 * fontSize: 24
											 */
											},
											axisY : {
												title : "Products in %"
											},
											legend : {
												verticalAlign : "center",
												horizontalAlign : "right"
											},

											data : [ {
												type : "pie",
												showInLegend : true,
												toolTipContent : "{label} <br/> {y} %",
												indexLabel : "{y} %",
												dataPoints : dataArray,
												click : function(e) {
													// alert( e.dataSeries.type+
													// " x:" + e.dataPoint.label
													// + ", y: "+
													// e.dataPoint.y);

													var details = e.dataPoint.label
															.split('-');
													var id = details[details.length - 1];
													var url = window.location.href;
													var urlDetails = url.split('=');
													window.location.href = myContextPath
															+ '/shipmentChartDetails?id='
															+ id + "&details=" + urlDetails[urlDetails.length - 1];
												}
											} ]

										});

					}).done(function(script, textStatus){
						//$("#tab_2").removeClass("active");
					});
	
	

}
function createTotalShipmentDataArray(data) {
	var arrObj = [];
	var object = {};
	$.each(data, function(index, value) {
		/*if(loginDetails.accgroup == 'ZMSP' || loginDetails.accgroup == 'ZMFF') {
			
		} else if(loginDetails.accgroup == 'ZMBY' || loginDetails.accgroup == 'ZMBH') {
			object['label'] = value.name + '-' + value.id;
			object['legendText'] = value.name;
		} else if(loginDetails.accgroup == 'ZMDA' || loginDetails.accgroup == 'ZMOA') {
			object['label'] = value.name + '-' + value.id;
			object['legendText'] = value.name;
		}*/
		
		object['label'] = value.name + '-' + value.id;
		object['legendText'] = value.name;
		object['y'] = Number(value.shipmentPercentage.toFixed(2));
		object['cursor'] = 'pointer'
		arrObj.push(object);
		object = {};
	})
	return arrObj;
}

function createGWTDataArray(data) {
	var arrObj = [];
	var object = {};
	$.each(data, function(index, value) {
		object['label'] = value.name + '-' + value.id;
		object['legendText'] = value.name;
		object['y'] = Number(value.valuePercentage.toFixed(2));
		object['cursor'] = 'pointer'
		arrObj.push(object);
		object = {};
	})
	return arrObj;
}

function createCBMCRGWTDataArray(data) {
	var arrObj = [];
	var object = {};
	$.each(data, function(index, value) {
		object['label'] = value.name + '-' + value.id;
		object['legendText'] = value.name;
		object['y'] = Number(value.totalCBMCRGPercentage.toFixed(2));
		object['cursor'] = 'pointer'
		arrObj.push(object);
		object = {};
	})
	return arrObj;
}
