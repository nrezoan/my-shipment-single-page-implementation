
function zoom() {
	document.body.style.zoom = "90%";

}

//var buyerIdArray=[];
var buyerIdMap={};
function drawChart(frmDate, toDate)
{
	if(lastNShipmentsJsonOutputData != undefined) {
		var table='';
		var itHeaderBeanLst=lastNShipmentsJsonOutputData.itHeaderBeanLst;
		$.each(itHeaderBeanLst,function(index,value){
			table=table+'<tr>';
			if(value.zzhblhawbno==null || value.zzhblhawbno==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td><a href=getTrackingInfoFrmUrl?searchString='+value.zzhblhawbno+'>'+value.zzhblhawbno+'</a></td>';
			
			/*if(value.bookingdate==null || value.bookingdate==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.bookingdate+'</td>';
			
			*/
			if(value.zzcomminvno==null || value.zzcomminvno==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.zzcomminvno+'</td>';				
			if(value.bookingdate==null || value.bookingdate==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.bookingdate+'</td>';				
			if(value.zzportofloading==null || value.zzportofloading==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.zzportofloading+'</td>';
			if(value.zzportofdest==null || value.zzportofdest==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.zzportofdest+'</td>';
			
			
			/*if(value.fvEtdPort==null || value.fvEtdPort==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.fvEtdPort+'</td>';
			
			if(value.mvEtaaPort==null || value.mvEtaaPort==undefined)
				table=table+'<td></td>';
			else
				table=table+'<td>'+value.mvEtaaPort+'</td>';
			*/
			
			 table=table+'</tr>';
	})
	$("#tbl-recent-shipment").find("tbody").empty();
	$("#tbl-recent-shipment").find("tbody").append(table);
	}
$.getScript('resources/js/canvasjsmin.js',function(){
	var $overlay=$.LoadingOverlay("show");		
	$.when($.ajax({
		url : myContextPath + '/suppwiseshipsummarybuyer',
		data : {
			fromDate : frmDate,
			toDate : toDate
		},
		type : "GET",
		dataType : "json",

	success:function(result1) {
		
	},
	error:function(xhr, status, errorThrown) {
		alert("Sorry, there was a problem!");
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
		
	}
	})).then(function(){
		$.ajax({
			url : myContextPath + '/recentshipment',
		data : {
			NoOfRecords:5
		},
		type : "GET",
		dataType : "json",
		success:function(data){
			var table='';
			var itHeaderBeanLst=data.itHeaderBeanLst;
			$.each(itHeaderBeanLst,function(index,value){
				table=table+'<tr>';
				if(value.zzhblhawbno==null || value.zzhblhawbno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td><a href=getTrackingInfoFrmUrl?searchString='+value.zzhblhawbno+'>'+value.zzhblhawbno+'</a></td>';
				
				/*if(value.bookingdate==null || value.bookingdate==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.bookingdate+'</td>';
				
				*/
				if(value.zzcomminvno==null || value.zzcomminvno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzcomminvno+'</td>';				
				if(value.bookingdate==null || value.bookingdate==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.bookingdate+'</td>';				
				if(value.zzportofloading==null || value.zzportofloading==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofloading+'</td>';
				if(value.zzportofdest==null || value.zzportofdest==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofdest+'</td>';
				
				
				/*if(value.fvEtdPort==null || value.fvEtdPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.fvEtdPort+'</td>';
				
				if(value.mvEtaaPort==null || value.mvEtaaPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.mvEtaaPort+'</td>';
				*/
				
				 table=table+'</tr>';
		})
		$("#tbl-recent-shipment").find("tbody").empty();
		$("#tbl-recent-shipment").find("tbody").append(table);
		var $overlay=$.LoadingOverlay("hide");
		}
		})
	})
	
});

	}
function createTable(topFiveShipmentJsonData)
{
	var tableData="";
	$.each(topFiveShipmentJsonData,function(index,value){
var tr='<tr>';
	tr+='<td><a href="#poo" onclick="handleClick(this)">'+value.portOfOrigin+'</a></td><td>'+value.totalShipment+'</td><td>'+Number(value.totalCBM.toFixed(2)).toLocaleString()+'</td><td>'+Number(value.totalGWT.toFixed(2)).toLocaleString()+'</td>';
	tr+='</tr>'
		tableData+=tr;
	})
	return tableData;
	}
function createColorArray( length)
{
var arrayColour=[];
for(var i=0;i<length;i++)
	{
	var color='#'+(Math.random()*0xFFFFFF<<0).toString(16);
	arrayColour.push(color);
	}
return arrayColour;
}



function parseData(lstShipperWiseShipmentCount)
{
	var arrObj=[];
	var object={};
	var count=0;
	
	$.each(lstShipperWiseShipmentCount,function(index,value){
		
		count++;
		buyerIdMap[count]=value.shipperNo;
		if($.isNumeric(value.shipperNo))
			{
			object['x']=Number(count);
			object['label']=value.shipperName;
			}
		else
			{
			object['label']=count;
			}
		
		
		object['y']=Number(value.shipmentCount);
		object['cursor']='pointer';
		//buyerIdArray.push(buyerIdMap);
		arrObj.push(object);
		object={};
	})
	return arrObj;
	}



function handleClick(element)
{
	var fromDate=$("#dashboard_from_date").val();
    var toDate=$("#dashboard_to_date").val();
    var poo=element.text;
    var url=myContextPath+'/suppwiseshipdetailsbypoo?fromDate='+fromDate+'&toDate='+toDate+'&poo='+poo;
    window.location=url;
	//alert("handled"+element.text);
	}

function getSoShipmentDetails(frmDate, toDate) {
	//var $overlay = $.LoadingOverlay("show");
	$.ajax({
		url : myContextPath + '/sowiseshipsummaryshipper',
		data : {
			fromDate : frmDate,
			toDate : toDate
		},
		type : "GET",
		dataType : "json",

	success:function(data) {
		var jsonString = JSON.stringify(data);
		
		//$.LoadingOverlay("hide")
		drawChart(data);
	},
	error:function(xhr, status, errorThrown) {
		alert("Sorry, there was a problem!");
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
		//$.LoadingOverlay("hide")
	}
	})
}