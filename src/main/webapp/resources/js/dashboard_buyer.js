$(document).ready(function() {
    if (loginDTO != undefined) {
        $("#dashboard_to_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", loginDTO.dashBoardToDate);
        $("#dashboard_from_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", loginDTO.dashBoardFromDate);
    } else {
        $("#dashboard_to_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", new Date());
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - 2);
        $("#dashboard_from_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", currentDate);
    }
    
    $("#totalBooking-dateRange").text("In Last 60 Days");
    $("#openBookings-dateRange").text("In Last 60 Days");
    $("#goodsHandOver-dateRange").text("In Last 60 Days");
    $("#stuffingDone-dateRange").text("In Last 60 Days");
    $("#inTransit-dateRange").text("In Last 60 Days");
    $("#arrived-dateRange").text("In Last 60 Days");
    
});


/*$("#totalShipment").text(Math.round(sowiseShipmentSummary.totalShipmentCount));

$("#totalCBM").text(Number(sowiseShipmentSummary.totalCBMCount.toFixed(0)).toLocaleString());
$("#totalGWT").text(Number(sowiseShipmentSummary.totalGWTCount.toFixed(0)).toLocaleString());
$("#inTransit").text("115");*/

$("#totalBookings-title").text("Total Bookings");
$("#openBookings-title").text("Open Bookings");
$("#goodsHandOver-title").text("Goods Handover");
if(loginDTO.divisionSelected == 'SE') {
	$("#stuffingDone-title").text("Stuffing Done");
	$("#inTransit-image").addClass("fa fa-ship");
} else if(loginDTO.divisionSelected == 'AR') {
	$("#stuffingDone-title").text("MAWB Issued");
	$("#inTransit-image").addClass("fa fa-plane");
}

$("#inTransit-title").text("In Transit (*EST)");
$("#arrived-title").text("Arrived (*EST)");

$("#totalBooking-number").text(Math.round(dashboardInfoBox.totalShipment));
$("#openBookings-number").text(Math.round(dashboardInfoBox.openOrder));
$("#goodsHandOver-number").text(Math.round(dashboardInfoBox.goodsReceived));
$("#stuffingDone-number").text(Math.round(dashboardInfoBox.stuffingDone));
$("#inTransit-number").text(Math.round(dashboardInfoBox.inTransit));
$("#arrived-number").text(Math.round(dashboardInfoBox.delivered));

var gwtValue;
var cbmValue;

if(loginDTO.divisionSelected == 'SE') {
	$("#tableHeaderGWT").text("Total Gross Weight");
	$("#tableHeaderCBM").text("Total Volume (CBM)");
	
	gwtValue = Number(dashboardInfoBox.totalGWT.toFixed(2)).toLocaleString();
	cbmValue = Number(dashboardInfoBox.totalCBM.toFixed(3)).toLocaleString();
} else if(loginDTO.divisionSelected == 'AR') {
	$("#tableHeaderGWT").text("Total Gross Weight");
	$("#tableHeaderCBM").text("Total Charge Weight");
	
	gwtValue = Number(dashboardInfoBox.totalGWT.toFixed(2)).toLocaleString();
	cbmValue = Number(dashboardInfoBox.totalCRGWT.toFixed(2)).toLocaleString();
}
var tableGWTCBM;
if(gwtValue == 0 && cbmValue == 0) {
	tableGWTCBM = '<tr><td class="text-center" colspan="2"><a class="not-found">No Weight or Volume Found</a></td></tr>';
} else {
	tableGWTCBM = '<tr><td class="text-center"><a href="dashboardInfoDetails?details=totalGWT">'+gwtValue+'</a></td>';
	tableGWTCBM = tableGWTCBM + '<td class="text-center"><a href="dashboardInfoDetails?details=totalCBM">'+cbmValue+'</a></td></tr>';
}

$("#tbl-totalGWTCBM").find("tbody").empty();
$("#tbl-totalGWTCBM").find("tbody").append(tableGWTCBM);

/*if (topFiveShipment != undefined) {
    var topFiveShipmentTable = generateTableRowTopFiveShipment(topFiveShipment);
    $("#tbl-top-shipment").find("tbody").empty();
    $("#tbl-top-shipment").find("tbody").append(topFiveShipmentTable);
}

if (lastNShipmentsJsonOutputData != undefined) {
    var lastNShipmentsTable = generateTableRowLastNShipments(lastNShipmentsJsonOutputData);
    $("#tbl-recent-shipment").find("tbody").empty();
    $("#tbl-recent-shipment").find("tbody").append(lastNShipmentsTable);
}*/

var openOrderPercentage = 0;
var inTransitPercentage = 0;
var goodsReceivedPercentage = 0;
var stuffingDonePercentage = 0;
var deliveredPercentage = 0;

if(dashboardInfoBox.totalShipment != 0) {
	openOrderPercentage = Math.round((dashboardInfoBox.openOrder / dashboardInfoBox.totalShipment) * 100);
	inTransitPercentage = Math.round((dashboardInfoBox.inTransit / dashboardInfoBox.totalShipment) * 100);
	goodsReceivedPercentage = Math.round((dashboardInfoBox.goodsReceived / dashboardInfoBox.totalShipment) * 100);
	stuffingDonePercentage = Math.round((dashboardInfoBox.stuffingDone / dashboardInfoBox.totalShipment) * 100);
	deliveredPercentage = Math.round((dashboardInfoBox.delivered / dashboardInfoBox.totalShipment) * 100);
} else {
	$("#shipmentStatus").css("display", "none");
	$("#shipmentStatusNone").text("No Orders Found");
	$("#mapBody").css("display", "none");
	$("#mapNone").text("No Destination Found");
}

function generateTableRowTopFiveShipment(topFiveShipment) {
    var topFiveShipmentJsonData = topFiveShipment.lstTopFiveShipmentBuyer;
    var tableData = "";
    if (topFiveShipmentJsonData.length == 0) {
        var tr = '<tr>';
        tr += '<td class="text-center" colspan="4"><a class="not-found">No Shipment Found</a></td>';
        tr += '</tr>';
        tableData += tr;
    } else {
        $.each(topFiveShipmentJsonData, function(index, value) {
            var tr = '<tr>';
            tr += '<td><a href="#poo" onclick="handleClick(this,\''+value.portOfOrigin+'\')" >' + value.portOfOriginDesc + '</a></td><td>' + value.totalShipment + '</td><td>' + value.totalCBM.toFixed(2) + '</td><td>' + value.totalGWT.toFixed(2) + '</td>';
            tr += '</tr>';
            tableData += tr;
        });
    }

    return tableData;
}

function generateTableRowLastNShipments() {
    var table = '';
    var itHeaderBeanLst = lastNShipmentsJsonOutputData.itHeaderBeanLst;
    if (itHeaderBeanLst.length == 0) {
        var tr = '<tr>';
        tr += '<td class="text-center" colspan="4"><a class="not-found">No Shipment Found</a></td>';
        tr += '</tr>';
        table += tr;
    } else {
        $.each(itHeaderBeanLst, function(index, value) {
            table = table + '<tr>';
            if (value.zzhblhawbno == null || value.zzhblhawbno == undefined)
                table = table + '<td></td>';
            else
                table = table + '<td><a href=getTrackingInfoFrmUrl?searchString=' + value.zzhblhawbno + '>' + value.zzhblhawbno + '</a></td>';
            if (value.zzmblmawbno == null || value.zzmblmawbno == undefined)
                table = table + '<td></td>';
            else
                table = table + '<td>' + value.zzmblmawbno + '</td>';

            if (value.zzportofloading == null || value.zzportofloading == undefined)
                table = table + '<td></td>';
            else
                table = table + '<td>' + value.zzportofloading + '</td>';
            if (value.zzportofdest == null || value.zzportofdest == undefined)
                table = table + '<td></td>';
            else
                table = table + '<td>' + value.zzportofdest + '</td>';
            
            table = table + '<td><span class="badge status-badge-green">Delivered</span></td>';
            
            table = table + '</tr>';
        });
    }
    return table;
}

$('#total-bookings').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=totalBooking';
    window.location.assign(url);
});

$('#open-bookings').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=openBooking';
    window.location.assign(url);
});

$('#goods-hand-over').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=goodsReceived';
    window.location.assign(url);
});

$('#stuffing-done').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=stuffingDone';
    window.location.assign(url);
});

$('#in-transit').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=inTransit';
    window.location.assign(url);
});

$('#arrived').click(function() {
    var fromDate = $("#dashboard_from_date").val();
    var toDate = $("#dashboard_to_date").val();
    var url = myContextPath + '/dashboardInfoDetails?details=arrived';
    window.location.assign(url);
});

$.getScript('resources/js/canvasjsmin.js',function(){
	$("#chartContainer").CanvasJSChart({

		backgroundColor : null,
		title : {
			text : "",
			fontSize : 24
		},
		axisY : {
			title : "Products in %"
		},
		legend : {
			verticalAlign : "center",
			horizontalAlign : "right"
		},
		data : [ {
			type : "pie",
			showInLegend : true,
			click: function(e){ 
        	    //alert(  e.dataSeries.type+ " x:" + e.dataPoint.label + ", y: "+ e.dataPoint.y);
        	    /*var fromDate=$("#dashboard_from_date").val();
        	    var toDate=$("#dashboard_to_date").val();
        	    var blStatus=e.dataPoint.label;
        	    window.location.href=myContextPath+'/blstatusshipper?fromDate='+fromDate+'&toDate='+toDate+'&blStatus='+blStatus;*/
				var shipmentStatus=e.dataPoint.label;
        	    
        	    var status = "";
        	    if(shipmentStatus == "Open Order") {
        	    	status = "openOrder";
        	    } else if(shipmentStatus == "Goods Received") {
        	    	status = "goodsReceived";
        	    } else if(shipmentStatus == "Stuffing Done" || shipmentStatus == "Shipment Done") {
        	    	status = "stuffingDone";
        	    } else if(shipmentStatus == "In Transit") {
        	    	status = "inTransit";
        	    } else if(shipmentStatus == "Delivered") {
        	    	status = "delivered";
        	    }
        	   // alert(salesOrg);
        	    if(shipmentStatus != undefined) {
        	    	window.location.href=myContextPath+'/shipmentStatusDetails?status='+status;
        	    }
        	  },
			toolTipContent : "{label} <br/> {y} %",
			indexLabel : "{y} %",
			dataPoints : [ {
				label : "Open Order",
				y : openOrderPercentage,
				legendText : "Open Order",
				cursor: "pointer"
			}, {
				label : "Goods Received",
				y : goodsReceivedPercentage,
				legendText : "Goods Received",
				cursor: "pointer"
			}, {
				label : loginDTO.divisionSelected == 'SE' ? "Stuffing Done" : "Shipment Done",
				y : stuffingDonePercentage,
				legendText : loginDTO.divisionSelected == 'SE' ? "Stuffing Done" : "Shipment Done",
				cursor: "pointer"
			}, {
				label : "In Transit",
				y : inTransitPercentage,
				legendText : "In Transit",
				cursor: "pointer"
			}, {
				label : "Delivered",
				y : deliveredPercentage,
				legendText : "Delivered",
				cursor: "pointer"
			}

			]
		} ]
	});
});
