/**
 * 
 */
function Validate() { 
	var originalpassword = document.getElementById("pass").value;
	var password = document.getElementById("pass1").value;
	var confirmPassword = document.getElementById("pass2").value;
	
	if(originalpassword==password) {
		sweetAlert("Oops...", "Current Password Cannot be New Password", "error");
		return false;
	}
	if(originalpassword==""||password=="" ||confirmPassword=="") {
		sweetAlert("Oops...", "Field Cannot Be Empty", "error");
		return false;
	}
	if (password  != confirmPassword) {
		sweetAlert("Oops...", "Passwords Do Not Match", "error");
		return false;
	}
	
	swal({
	      title: "Are you sure?",
	      text: "Do you want to Change Password?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#009688",
	      cancelButtonColor: "#f44336",
	      confirmButtonText: "Yes",
	      cancelButtonText: "No",
	      closeOnConfirm: false,
	      closeOnCancel: false,
	      showLoaderOnConfirm: true
	    },
	    function(isConfirm){
	      if(isConfirm) {
	    	  sendChangePasswordRequest(originalpassword, password, confirmPassword);
	    	  
	    	  //swal.close();
	      } else {
	    	  swal.close();
	      }

	      
	    });
	
	return false;
}

function sendChangePasswordRequest(originalpassword, password, confirmPassword) {
	$.ajax({
		url: myContextPath + '/changeOldPassword',
		type: "POST",
		data: {
			oldPasswordParam : originalpassword,
			newPasswordParam : password,
			verifyPasswordParam : confirmPassword
		},
		
		success:function(data, textStatus, xhr){
			var responseType = "";
			var responseMessageTitle = "";
			var responseMessageText = "";
			if(textStatus == 'success') {
				if(data == 'CHANGED') {
					responseType = 'success';
					responseMessageTitle = 'Password Successfully Changed';
					responseMessageText = '';
				} else if(data == 'ERROR') {
					responseType = 'error';
					responseMessageTitle = 'Error Occured';
					responseMessageText = 'Error Occured While changing password. Please Try Again.';
				} else if(data == 'WRONG') {
					responseType = 'error';
					responseMessageTitle = 'Wrong Password';
					responseMessageText = 'You have entered wrong password in the Old password field.';
				}
				swal({
		              title: responseMessageTitle,
		              text: responseMessageText,
		              type: responseType,
		              showCancelButton: false,
		              confirmButtonColor: "#367fa9",
		              confirmButtonText: "Continue",
		              cancelButtonText: "",
		              closeOnConfirm: false,
		              closeOnCancel: false
		            },
		            function(isConfirm){
		              if(isConfirm) {
		            	  
		            	  swal.close();
		            	  //window.location.reload();
		              }
		            });
			} else {
				swal("Error Occured", "Error Occured While changing password. Please Try Again.", "error");
				
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			//$.LoadingOverlay("hide");
			swal("Error Occured", "Please Try Again", "error");
	    }
	});
}