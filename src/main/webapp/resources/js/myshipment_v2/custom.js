/**********************************
             WOW JS
**********************************/

$(function () {
    new WOW().init();

});


/**********************************
        RESPONSIVE NAVIGATION
**********************************/


$(document).ready(function () {
    var windowWidth = $(window).width();
    if (windowWidth < 1242) {
        $(".navbar-collapse ul li a").on("click touch", function () {
            $(".navbar-toggle").click();
        });
    }


});


/**********************************
             ACCORDION
**********************************/

// FOR LOCATIONS ACCORDION
$(document).ready(function () {

    $(".fa-minus-circle").hide();
    $(".bd-plus").hide();
    $(".bd-minus").show();
});


function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('fa-plus-circle fa-minus-circle');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);


/**********************************
        GOOGLE ANALYTICS
**********************************/

$(function () {
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-102694461-1', 'auto');
    ga('send', 'pageview');

});


/**********************************
           TOP NAV SCROLL
**********************************/
$(function () {

    $(window).scroll(function () {

        if ($(this).scrollTop() < 50) {
            // hide nav
            $("nav").removeClass("myshipment-top-nav");


        } else {
            // show nav
            $("nav").addClass("myshipment-top-nav");

        }
    });
});

// Smooth scrolling
$(function () {

    $("a.smooth-scroll").click(function (event) {

        event.preventDefault();

        var section = $(this).attr("href");

        $('html, body').animate({
            scrollTop: $(section).offset().top - 64
        }, 1250, "easeInOutExpo");
    });
});

