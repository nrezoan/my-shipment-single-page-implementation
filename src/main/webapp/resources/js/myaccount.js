
$(document).ready(function() {
	
	
    
});

var defaultCompanyOption = '<option value="">None</option>';
var defaultDistributionChannelOption = '';
var defaultDivisionOption = '';

var defaultDivision = "";

setDefaultCompanyActiveStatus();

function setDefaultCompanyActiveStatus() {
	if(defaultCompanyJson.active == "0") {
		$('#default-company-active-state-text').text("Inactive");
		$('#default-company-active-state-text').removeClass("btn-success").addClass("btn-danger");
		$('#default-company-active').removeClass("active");
		$('#default-company-inactive').addClass("active");
	} else if(defaultCompanyJson.active == "1") {
		$('#default-company-active-state-text').text("Active");
		$('#default-company-active-state-text').removeClass("btn-danger").addClass("btn-success");
		$('#default-company-inactive').removeClass("active");
		$('#default-company-active').addClass("active");
	} else {
		$('#default-company-active-state-text').text("Inactive");
		$('#default-company-active-state-text').removeClass("btn-success").addClass("btn-danger");
		$('#default-company-active').removeClass("active");
		$('#default-company-inactive').addClass("active");
	}
}

$("#default-company-active").click(function() {
	if($('#default-company-active-state-text').text() != "Active") {
		if(defaultCompanyJson.preferredCompanyId == null) {
			sweetAlert("Select Default Company", "Please Select a Default Company to activate this option", "error");
			return false;
		} else {
			swal({
			      title: "Are you sure?",
			      text: "Do you want to activate default company Option?",
			      type: "warning",
			      showCancelButton: true,
			      confirmButtonColor: "#009688",
			      cancelButtonColor: "#f44336",
			      confirmButtonText: "Yes",
			      cancelButtonText: "Cancel",
			      closeOnConfirm: false,
			      closeOnCancel: false,
			      showLoaderOnConfirm: true
			    },
			    function(isConfirm){
			      if(isConfirm) {
			    	  sendActivateDefaultCompanyRequest("1");
			    	  
			    	  //swal.close();
			      } else {
			    	  swal.close();
			      }

			      
			    });
		}
	}
});

$("#default-company-inactive").click(function() {
	if($('#default-company-active-state-text').text() != "Inactive") {
		if(defaultCompanyJson.preferredCompanyId == null) {
			sweetAlert("Select Default Company", "Please Select a Default Company to activate this option", "error");
			return false;
		} else {
			swal({
			      title: "Are you sure?",
			      text: "Do you want to de-activate default company Option?",
			      type: "warning",
			      showCancelButton: true,
			      confirmButtonColor: "#009688",
			      cancelButtonColor: "#f44336",
			      confirmButtonText: "Yes",
			      cancelButtonText: "Cancel",
			      closeOnConfirm: false,
			      closeOnCancel: false,
			      showLoaderOnConfirm: true
			    },
			    function(isConfirm){
			      if(isConfirm) {
			    	  sendActivateDefaultCompanyRequest("0");
			    	  //swal.close();
			      } else {
			    	  swal.close();
			      }

			      
			    });
		}
	}
});

function sendActivateDefaultCompanyRequest(activeStatus) {
	var activeStatusText = "";
	if(activeStatus == '0') {
		activeStatusText = "Deactivated";
	} else if(activeStatus == '1') {
		activeStatusText = "Activated";
	}
	$.ajax({
		url: myContextPath + '/activationDefaultCompany',
		type: "POST",
		data: {
				active: activeStatus
		},
		
		success:function(data, textStatus, xhr){
			//$.LoadingOverlay("show");
			//window.location=xhr.getResponseHeader("Location");
			//window.location.reload();
			//$.LoadingOverlay("hide");
			if(textStatus == 'success') {
				swal({
		              title: activeStatusText,
		              text: "Default Sales Organization Activated!",
		              type: "success",
		              showCancelButton: false,
		              confirmButtonColor: "#367fa9",
		              confirmButtonText: "Continue",
		              cancelButtonText: "",
		              closeOnConfirm: false,
		              closeOnCancel: false
		            },
		            function(isConfirm){
		              if(isConfirm) {
		            	  if(activeStatus == "0") {
		            		  $('#default-company-active-state-text').text("Inactive");
					    	  $('#default-company-active-state-text').removeClass("btn-success").addClass("btn-danger");
					    	  $('#default-company-active').removeClass("active");
					    	  $('#default-company-inactive').addClass("active");
		            	  } else if(activeStatus == "1") {
		            		  $('#default-company-active-state-text').text("Active");
					    	  $('#default-company-active-state-text').removeClass("btn-danger").addClass("btn-success");
					    	  $('#default-company-inactive').removeClass("active");
					    	  $('#default-company-active').addClass("active");
		            	  }
		            	  swal.close();
		            	  //window.location.reload();
		              }
		            });
			} else {
				if(activeStatus == "0") {
					swal("Deactivation Failed", "Could not deactivate Default Company Option. Please Try Again", "error");
				} else if(activeStatus == "1") {
					swal("Activation Failed", "Could not activate Default Company Option. Please Try Again", "error");
				}
				
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			//$.LoadingOverlay("hide");
			swal("Error Occured", "Please Try Again", "error");
	    }
	});
}

	
$.each(loginDetails.mpSalesOrgTree, function(index, value) {
	var salesOrg = value;
	if (salesOrg.salesOrg == defaultCompanyJson.preferredCompanyId) {
		defaultCompanyOption = defaultCompanyOption
				+ '<option value="'+salesOrg.salesOrg+'" selected >'
				+ salesOrg.salesOrgText + '</option>';
	} else {
		defaultCompanyOption = defaultCompanyOption + '<option value="'+salesOrg.salesOrg+'" >'
				+ salesOrg.salesOrgText + '</option>';
	}
});

setDefaultDistribution(defaultCompanyJson);
setDefaultDivision(defaultCompanyJson);

$("#defaultCompanyList").append(defaultCompanyOption);


function defaultDistributionChannelOnChange(preferredCompanyId) {
	var defaultCompanyOnChangeJson = {
			"preferredCompanyId" : preferredCompanyId,
			"distributionChannel" : "",
			"division" : ""
	};
	
	setDefaultDistribution(defaultCompanyOnChangeJson);
}
function defaultDivisionOnChange(distributionChannel) {
	var preferredCompanyId = $("#defaultCompanyList").val();
	var defaultCompanyOnChangeJson = {
			"preferredCompanyId" : preferredCompanyId,
			"distributionChannel" : distributionChannel,
			"division" : ""
	};
	
	setDefaultDivision(defaultCompanyOnChangeJson);
}

function setDefaultDistribution(defaultCompanyJsonTemp) {
	defaultDistributionChannelOption = '<option value="">None</option>';
	$.each(loginDetails.mpSalesOrgTree,function(index,value){
		
		if(value.salesOrg==defaultCompanyJsonTemp.preferredCompanyId) {
			
			var distChnlLst=value.lstDistChnl;
			var Ex='';
			var Im='';
			var cha='';
			
			//identify Export, Import and CHA of the selected distribution channel
			//sets variable Ex,Im,cha value if selected distribution channel has any of these business
			$.each(distChnlLst,function(index,value) {
				
				if(value.distrChan==defaultCompanyJsonTemp.distributionChannel) {
					defaultDivision=value.lstDivision;
				}
				
				var disChnl=value;
				
				if(disChnl.distrChan=='EX') {
					Ex=disChnl.distrChan;
				}
				if(disChnl.distrChan=='IM') {
					Im=disChnl.distrChan;
				}
				if(disChnl.distrChan=='CHA') {
					cha=disChnl.distrChan;
				}
				
			});
			
			if(Ex == 'EX') {
				if(defaultCompanyJsonTemp.distributionChannel == 'EX') {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+defaultCompanyJsonTemp.distributionChannel+'" selected>Export</option>';
				} else {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+Ex+'">Export</option>';
				}
			}
			
			if(Im == 'IM') {
				if(defaultCompanyJsonTemp.distributionChannel == 'IM') {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+defaultCompanyJsonTemp.distributionChannel+'" selected>Import</option>';
				} else {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+Im+'">Import</option>';
				}
			}
			
			if(cha == 'CHA') {
				if(defaultCompanyJsonTemp.distributionChannel == 'CHA') {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+defaultCompanyJsonTemp.distributionChannel+'" selected>CHA</option>';
				} else {
					defaultDistributionChannelOption = defaultDistributionChannelOption
					+ '<option value="'+cha+'">CHA</option>';
				}
			}
			
			
		}
	});
	$("#defaultDistributionChannelList").empty().append(defaultDistributionChannelOption);
	defaultDivisionOption = '<option value="">None</option>';
	$("#defaultDivisionList").empty().append(defaultDivisionOption);
	
	
}

function setDefaultDivision(defaultCompanyJsonTemp) {
	defaultDivisionOption = '<option value="">None</option>';
	defaultDivision = '';
	
	$.each(loginDetails.mpSalesOrgTree,function(index,value){
		
		if(value.salesOrg==defaultCompanyJsonTemp.preferredCompanyId) {
			
			var distChnlLst=value.lstDistChnl;
			$.each(distChnlLst,function(index,value) {
				
				if(value.distrChan==defaultCompanyJsonTemp.distributionChannel) {
					defaultDivision=value.lstDivision;
					return false;
				}
			});
			return false;
		}
	});
	
	
	
	$.each(defaultDivision,function(index,value){
		if(value.division=='SE') {
    		if(defaultCompanyJsonTemp.division==value.division) {
    			defaultDivisionOption = defaultDivisionOption
				+ '<option value="'+defaultCompanyJsonTemp.division+'" selected>SEA</option>';
    		} else {
    			defaultDivisionOption = defaultDivisionOption
				+ '<option value="'+value.division+'">SEA</option>';
    		}
		}
    	if(value.division=='AR') {
    		if(defaultCompanyJsonTemp.division==value.division) {
    			defaultDivisionOption = defaultDivisionOption
				+ '<option value="'+defaultCompanyJsonTemp.division+'" selected>AIR</option>';
    		} else {
    			defaultDivisionOption = defaultDivisionOption
				+ '<option value="'+value.division+'">AIR</option>';
    		}
    	}
	});
	$("#defaultDivisionList").empty().append(defaultDivisionOption);
}

$("#saveDefaultCompany").click(function(){
    
	var salesOrg=$("#defaultCompanyList").val();
	var salesOrgText=$("#defaultCompanyList option[value='"+salesOrg+"']").text();
	var distChannel=$("#defaultDistributionChannelList").val();
	var distChannelText=$("#defaultDistributionChannelList option[value='"+distChannel+"']").text();
	var division=$("#defaultDivisionList").val();
	var divisionText=$("#defaultDivisionList option[value='"+division+"']").text();
	
	if(salesOrg == "") {
		sweetAlert("Sales Organization", "Please Select a sales organization", "error");
		return false;
	}
	if(distChannel == "") {
		sweetAlert("Distribution Channel", "Please Select a Distribution Channel", "error");
		return false;
	}
	if(division == "") {
		sweetAlert("Division", "Please Select a Division", "error");
		return false;
	}
	
	if(salesOrg == defaultCompanyJson.preferredCompanyId 
			&& distChannel == defaultCompanyJson.distributionChannel 
			&& division == defaultCompanyJson.division) {
		sweetAlert(salesOrgText+" : "+distChannelText+" : "+divisionText, "This is already your default Company", "error");
		return false;
	}
	
	
	swal({
	      title: "Are you sure?",
	      text: "Do you want to set "+salesOrgText+" : "+distChannelText+" : "+divisionText+" as you default company?",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#009688",
	      /*cancelButtonColor: "#f44336",*/
	      confirmButtonText: "Save!",
	      cancelButtonText: "Cancel!",
	      closeOnConfirm: false,
	      closeOnCancel: false,
	      showLoaderOnConfirm: true
	    },
	    function(isConfirm){
	      if(isConfirm) {
	    	  sendSaveDefaultCompanyRequest(salesOrg, distChannel, division, salesOrgText, distChannelText, divisionText);
	    	  
	      } else {
	        //swal("Cancelled", "", "error");
	    	  swal.close()
	      }

	      
	    });
	
	
	
});

function sendSaveDefaultCompanyRequest(salesOrg, distChannel, division, salesOrgText, distChannelText, divisionText) {
	$.ajax({
		url: myContextPath + '/saveDefaultCompany',
		type: "POST",
		data: {
				salesOrg:salesOrg,
				distChannel:distChannel,
				division:division
		},
		
		success:function(data, textStatus, xhr){
			//$.LoadingOverlay("show");
			//window.location=xhr.getResponseHeader("Location");
			//window.location.reload();
			//$.LoadingOverlay("hide");
			if(textStatus == 'success') {
				var updatedLoginDetails = data;
				swal({
		              title: salesOrgText+" : "+distChannelText+" : "+divisionText,
		              text: "Default Sales Organization Saved!",
		              type: "success",
		              showCancelButton: false,
		              confirmButtonColor: "#367fa9",
		              confirmButtonText: "Continue",
		              cancelButtonText: "",
		              closeOnConfirm: false,
		              closeOnCancel: false
		            },
		            function(isConfirm){
		              if(isConfirm) {
		            	  //window.location.reload();
		            	  defaultCompanyJson.preferredCompanyId = salesOrg;
		            	  defaultCompanyJson.active = "1";
		            	  defaultCompanyJson.distributionChannel = distChannel;
		            	  defaultCompanyJson.division = division;
		            	  
		            	  setDefaultCompanyActiveStatus();
		            	  
		            	  headerNavDetails(updatedLoginDetails);
		            	  
		            	  swal.close();
		              }
		            });
			} else {
				swal("Could Not save Default Company", "Please Try Again", "error");
			}
			
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
			//$.LoadingOverlay("hide");
			swal("Error Occured", "Please Try Again", "error");
	    }
	});
}
