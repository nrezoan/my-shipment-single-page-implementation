$(document).ready(
		function() {
			$("#insTrack")
					.submit(
							function() {
								var frmDate = $("#fromDate").val();
								if (frmDate == "") {
									swal("Inspection Date",
											"Inspection Date cannot be empty!",
											"error");
									return false;
								} else {
									return true;
								}
							});
		});