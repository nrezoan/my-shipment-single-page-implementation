function findMawb() {
    var mawb = document.getElementById("mawb").value;

    var data = JSON.stringify(mawb);

    if (data !== null || data !== "") {

        $.ajax({
            url: myContextPath + '/findMawb',
            type: "POST",
            data: data,
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {

                console.log(data);
                document.getElementById("performanceForm").style.display = "block";
                createTable(data.hawbList)
            }
        });
    }

}


function createTable(tableData) {

    console.log(tableData);
    $("#valueTable> tbody").empty();

    var table = document.getElementById('valueTable');

    var tableBody = document.getElementById('valueTableBody');
    var rowCounter = 0;

    tableData
        .forEach(function (rowData) {

            if (rowData != null) {
                // create row
                var row = document.createElement('tr');

                // createAnchor(row, rowCounter);

                var hawb = rowData.hawb;
                createCellData(row, hawb);

                var incoterms = rowData.incoterms;
                createCellData(row, incoterms);

                var origin = rowData.origin;
                createCellData(row, origin);

                var destination = rowData.destination;
                createCellData(row, destination);

                var receiver = rowData.receiver;
                createCellData(row, receiver);

                var grossWeight = rowData.grossWeight;
                createCellData(row, grossWeight);

                var volume = rowData.volume;
                createCellData(row, volume);

                var chargeableWeight = rowData.chargeableWeight;
                createCellData(row, chargeableWeight);

                // appending new row
                tableBody.appendChild(row);

                table.appendChild(tableBody);
                document.getElementById('headingTwo').appendChild(table);

            }
        });
}

function createCellData(row, cellData) {

    var cell = document.createElement('td');
    cell.appendChild(document.createTextNode(cellData));
    row.appendChild(cell);

}