function createTable(tableData) {

	$("#valueTable> tbody").empty();

	var table = document.getElementById('valueTable');

	var tableBody = document.getElementById('valueTableBody');
	var rowCounter = 0;

	tableData.forEach(function(rowData) {
				if (rowData != null) {
					// create row
					var row = document.createElement('tr');

					// createAnchor(row, rowCounter);

					var itemNumber = rowData.itemNumber;
					createCellData(row, itemNumber);

					var poNumber = rowData.poNumber;
					createCellData(row, poNumber);

					var styleNo = rowData.styleNo;
					createCellData(row, styleNo);

					var sizeNo = rowData.sizeNo;
					createCellData(row, sizeNo);

					var color = rowData.color;
					createCellData(row, color);

					var hsCode = rowData.hsCode;
					createCellData(row, hsCode);

					var material = rowData.material;
					createCellData(row, material);

					createAnchor(row, rowCounter);

					deleteRow(row, rowCounter);
					// appending new row
					tableBody.appendChild(row);
					if (tableData[rowCounter].poNumber === copyArray[rowCounter].poNumber) {
					} else {
						modelCopy = addToLog("PO Number",
								copyArray[rowCounter].poNumber,
								tableData[rowCounter].poNumber,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].poNumber = tableData[rowCounter].poNumber;
					}
					if (tableData[rowCounter].hsCode === copyArray[rowCounter].hsCode) {
					} else {
						modelCopy = addToLog("HS Code",
								copyArray[rowCounter].hsCode,
								tableData[rowCounter].hsCode,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].hsCode = tableData[rowCounter].hsCode;
					}
					if (tableData[rowCounter].material === copyArray[rowCounter].material) {
					} else {
						modelCopy = addToLog("Commodity",
								copyArray[rowCounter].material,
								tableData[rowCounter].material,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].material = tableData[rowCounter].material;
					}
					if (tableData[rowCounter].sizeNo === copyArray[rowCounter].sizeNo) {
					} else {
						modelCopy = addToLog("Size",
								copyArray[rowCounter].sizeNo,
								tableData[rowCounter].sizeNo,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].sizeNo = tableData[rowCounter].sizeNo;
					}
					if (tableData[rowCounter].color === copyArray[rowCounter].color) {
					} else {
						modelCopy = addToLog("Color",
								copyArray[rowCounter].color,
								tableData[rowCounter].color,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].color = tableData[rowCounter].color;
					}
					if (tableData[rowCounter].styleNo === copyArray[rowCounter].styleNo) {
					} else {
						modelCopy = addToLog("Style",
								copyArray[rowCounter].styleNo,
								tableData[rowCounter].styleNo,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].styleNo = tableData[rowCounter].styleNo;
					}
					if (tableData[rowCounter].curtonQuantity === copyArray[rowCounter].curtonQuantity) {
					} else {
						modelCopy = addToLog("Carton Quantity",
								copyArray[rowCounter].curtonQuantity,
								tableData[rowCounter].curtonQuantity,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].curtonQuantity = tableData[rowCounter].curtonQuantity;
					}
					if (tableData[rowCounter].totalPieces === copyArray[rowCounter].totalPieces) {
					} else {
						modelCopy = addToLog("Total Pieces",
								copyArray[rowCounter].totalPieces,
								tableData[rowCounter].totalPieces,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].totalPieces = tableData[rowCounter].totalPieces;
					}
					if (tableData[rowCounter].totalVolume === copyArray[rowCounter].totalVolume) {
					} else {
						modelCopy = addToLog("Total Volume",
								copyArray[rowCounter].totalVolume,
								tableData[rowCounter].totalVolume,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].totalVolume = tableData[rowCounter].totalVolume;
					}
					if (tableData[rowCounter].grossWeight === copyArray[rowCounter].grossWeight) {
					} else {
						modelCopy = addToLog("Gross Weight",
								copyArray[rowCounter].grossWeight,
								tableData[rowCounter].grossWeight,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].grossWeight = tableData[rowCounter].grossWeight;
					}
					if (tableData[rowCounter].netWeight === copyArray[rowCounter].netWeight) {
					} else {
						modelCopy = addToLog("Net Weight ",
								copyArray[rowCounter].netWeight,
								tableData[rowCounter].netWeight,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].netWeight = tableData[rowCounter].netWeight;
					}
					if (tableData[rowCounter].cbmPerCarton === copyArray[rowCounter].cbmPerCarton) {
					} else {
						modelCopy = addToLog("CBM Per Carton",
								copyArray[rowCounter].cbmPerCarton,
								tableData[rowCounter].cbmPerCarton,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cbmPerCarton = tableData[rowCounter].cbmPerCarton;
					}
					if (tableData[rowCounter].grossWeightPerCarton === copyArray[rowCounter].grossWeightPerCarton) {
					} else {
						modelCopy = addToLog("Gross Weight Per Carton",
								copyArray[rowCounter].grossWeightPerCarton,
								tableData[rowCounter].grossWeightPerCarton,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].grossWeightPerCarton = tableData[rowCounter].grossWeightPerCarton;
					}
					if (tableData[rowCounter].netWeightPerCarton === copyArray[rowCounter].netWeightPerCarton) {
					} else {
						modelCopy = addToLog("Net Weight Per Carton",
								copyArray[rowCounter].netWeightPerCarton,
								tableData[rowCounter].netWeightPerCarton,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].netWeightPerCarton = tableData[rowCounter].netWeightPerCarton;
					}
					if (tableData[rowCounter].cartonUnit === copyArray[rowCounter].cartonUnit) {
					} else {
						modelCopy = addToLog("Carton Unit ",
								copyArray[rowCounter].cartonUnit,
								tableData[rowCounter].cartonUnit,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cartonUnit = tableData[rowCounter].cartonUnit;
					}
					if (tableData[rowCounter].cartonLength === copyArray[rowCounter].cartonLength) {
					} else {
						modelCopy = addToLog("Carton Length",
								copyArray[rowCounter].cartonLength,
								tableData[rowCounter].cartonLength,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cartonLength = tableData[rowCounter].cartonLength;
					}
					if (tableData[rowCounter].cartonWidth === copyArray[rowCounter].cartonWidth) {
					} else {
						modelCopy = addToLog("Carton Width",
								copyArray[rowCounter].cartonWidth,
								tableData[rowCounter].cartonWidth,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cartonWidth = tableData[rowCounter].cartonWidth;
					}
					if (tableData[rowCounter].cartonHeight === copyArray[rowCounter].cartonHeight) {
					} else {
						modelCopy = addToLog("Carton Height",
								copyArray[rowCounter].cartonHeight,
								tableData[rowCounter].cartonHeight,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cartonHeight = tableData[rowCounter].cartonHeight;
					}
					if (tableData[rowCounter].cartonSerNo === copyArray[rowCounter].cartonSerNo) {
					} else {
						modelCopy = addToLog("Carton Serial No",
								copyArray[rowCounter].cartonSerNo,
								tableData[rowCounter].cartonSerNo,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].cartonSerNo = tableData[rowCounter].cartonSerNo;
					}
					if (tableData[rowCounter].reference1 === copyArray[rowCounter].reference1) {
					} else {
						modelCopy = addToLog("Reference#1",
								copyArray[rowCounter].reference1,
								tableData[rowCounter].reference1,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].reference1 = tableData[rowCounter].reference1;
					}
					if (tableData[rowCounter].reference2 === copyArray[rowCounter].reference2) {
					} else {
						modelCopy = addToLog("Reference#2",
								copyArray[rowCounter].reference2,
								tableData[rowCounter].reference2,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].reference2 = tableData[rowCounter].reference2;
					}
					if (tableData[rowCounter].reference3 === copyArray[rowCounter].reference3) {
					} else {
						modelCopy = addToLog("Reference#3",
								copyArray[rowCounter].reference3,
								tableData[rowCounter].reference3,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].reference3 = tableData[rowCounter].reference3;
					}
					if (tableData[rowCounter].reference4 === copyArray[rowCounter].reference4) {
					} else {
						modelCopy = addToLog("Reference#4",
								copyArray[rowCounter].reference4,
								tableData[rowCounter].reference4,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].reference4 = tableData[rowCounter].reference4;
					}
					if (tableData[rowCounter].qcDate === copyArray[rowCounter].qcDate) {
					} else {
						modelCopy = addToLog("QC Date",
								copyArray[rowCounter].qcDate,
								tableData[rowCounter].qcDate,
								tableData[rowCounter].itemNumber), "Line Item";
						sendArray.push(modelCopy);
						copyArray[rowCounter].qcDate = tableData[rowCounter].qcDate;
					}
					if (tableData[rowCounter].productCode === copyArray[rowCounter].productCode) {
					} else {
						modelCopy = addToLog("Prodcut Code/SKU",
								copyArray[rowCounter].productCode,
								tableData[rowCounter].productCode,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].productCode = tableData[rowCounter].productCode;
					}
					if (tableData[rowCounter].projectNo === copyArray[rowCounter].projectNo) {
					} else {
						modelCopy = addToLog("Project No",
								copyArray[rowCounter].projectNo,
								tableData[rowCounter].projectNo,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].projectNo = tableData[rowCounter].projectNo;
					}
					if (tableData[rowCounter].pcsPerCarton === copyArray[rowCounter].pcsPerCarton) {
					} else {
						modelCopy = addToLog("Pieces Per Carton",
								copyArray[rowCounter].pcsPerCarton,
								tableData[rowCounter].pcsPerCarton,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].pcsPerCarton = tableData[rowCounter].pcsPerCarton;
					}
					if (tableData[rowCounter].commInvoice === copyArray[rowCounter].commInvoice) {
					} else {
						modelCopy = addToLog("Commercial Invoice No",
								copyArray[rowCounter].commInvoice,
								tableData[rowCounter].commInvoice,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].commInvoice = tableData[rowCounter].commInvoice;
					}
					if (tableData[rowCounter].commInvoiceDate === copyArray[rowCounter].commInvoiceDate) {
					} else {
						modelCopy = addToLog("Commercial Invoice Date",
								copyArray[rowCounter].commInvoiceDate,
								tableData[rowCounter].commInvoiceDate,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].commInvoiceDate = tableData[rowCounter].commInvoiceDate;
					}
					if (tableData[rowCounter].releaseDate === copyArray[rowCounter].releaseDate) {
					} else {
						modelCopy = addToLog("Release Date",
								copyArray[rowCounter].releaseDate,
								tableData[rowCounter].releaseDate,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].releaseDate = tableData[rowCounter].releaseDate;
					}
					if (tableData[rowCounter].description === copyArray[rowCounter].description) {
					} else {
						modelCopy = addToLog("Description of Goods",
								copyArray[rowCounter].description,
								tableData[rowCounter].description,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].description = tableData[rowCounter].description;
					}
					if (tableData[rowCounter].itemDescription === copyArray[rowCounter].itemDescription) {
					} else {
						modelCopy = addToLog("Item Description",
								copyArray[rowCounter].itemDescription,
								tableData[rowCounter].itemDescription,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].itemDescription = tableData[rowCounter].itemDescription;
					}
					if (tableData[rowCounter].shippingMark === copyArray[rowCounter].shippingMark) {
					} else {
						modelCopy = addToLog("Shipping Mark",
								copyArray[rowCounter].shippingMark,
								tableData[rowCounter].shippingMark,
								tableData[rowCounter].itemNumber, "Line Item");
						sendArray.push(modelCopy);
						copyArray[rowCounter].shippingMark = tableData[rowCounter].shippingMark;
					}
					console.log(sendArray);
					rowCounter++;
				}

			});

	table.appendChild(tableBody);
	document.getElementById('headingTwo').appendChild(table);

}

function createAnchor(row, rowCounter) {

	var a = document.createElement('a');
	var data_toggle = document.createAttribute('data-toggle');
	data_toggle.value = 'modal';
	var data_target = document.createAttribute('data-target');
	data_target.value = '#updateFormModal';
	var cl = document.createAttribute('class');
	cl.value = 'button';
	var onClick = document.createAttribute('onclick');
	onClick.value = "setValues(modelArray[" + rowCounter + "]," + rowCounter
			+ ")";
	a.setAttributeNode(data_toggle);
	a.setAttributeNode(data_target);
	a.setAttributeNode(cl);
	a.setAttributeNode(onClick);
	/* a.innerText = 'Expand'; */
	/* a.innerHTML = "<i class='fa fa-pencil-square-o' aria-hidden='true'></i>"; */
	a.innerHTML = "<a class='btn btn-warning btn-sm'><i class='fa fa-pencil' aria-hidden='true'></i>&nbsp;Edit</a>";

	var cell = document.createElement('td');
	cell.style.width = '5%';
	cell.appendChild(a);
	row.appendChild(cell);

}

function deleteRow(row, rowCounter) {

	var a = document.createElement('a');

	var cl = document.createAttribute('class');
	cl.value = 'button';
	var onClick = document.createAttribute('onclick');
	onClick.value = "deleteItem(" + rowCounter + ")";
	a.setAttributeNode(cl);
	a.setAttributeNode(onClick);
	/* a.innerText = 'Delete'; */
	/* a.innerHTML = "<i class='fa fa-trash-o' aria-hidden='true'></i>"; */
	a.innerHTML = "<a class='btn btn-danger btn-sm'><i class='fa fa-trash-o' aria-hidden='true'></i>&nbsp;Delete</a>";

	var cell = document.createElement('td');
	cell.style.width = '5%';
	cell.appendChild(a);
	row.appendChild(cell);

}

function createCellData(row, cellData) {

	var cell = document.createElement('td');
	cell.appendChild(document.createTextNode(cellData));
	row.appendChild(cell);

}

function validateDecimalDataType(element, e) {
	// alert(e.which);
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)
			&& e.which != 46) {
		// display error message
		$(element).tooltip({

			tooltipClass : "alert alert-danger"

		})
		return false;

	} else
		return true;
}

function validateDataType(element, e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		// display error message
		$(element).tooltip({

			tooltipClass : "alert alert-danger"

		})
		return false;

	} else
		return true;
}

function calculateCBM() {

	try {

		// need to check wheter added item or new Item.
		var length = document.getElementById('id_add_cartonLength').value;
		var width = document.getElementById('id_add_cartonWidth').value;
		var height = document.getElementById('id_add_cartonHeight').value;
		var unit = document.getElementById('id_add_cartonMeasurementUnit').value;
		var noOfCar = document.getElementById('id_add_cartonQuantity').value;
		if (unit == 'IN') {
			var cbm = ((length * width * height) / 61024) * noOfCar;
		}
		if (unit == 'CM') {
			var cbm = ((length * width * height) / 1000000) * noOfCar;
		}

		document.getElementById('id_add_totalVolume').value = cbm.toFixed(3);
		cbmFieldFunctionFromAddModal();
	} catch (Exception) {
		document.getElementById('id_add_totalVolume').value = '';
	}

}

function calculateCBMUpdate() {

	try {

		// need to check wheter added item or new Item.
		var length = document.getElementById('id_cartonLength').value;
		var width = document.getElementById('id_cartonWidth').value;
		var height = document.getElementById('id_cartonHeight').value;
		var unit = document.getElementById('id_cartonMeasurementUnit').value;
		var noOfCar = document.getElementById('id_cartonQuantity').value;
		if (unit == 'IN') {
			var cbm = ((length * width * height) / 61024) * noOfCar;
		}
		if (unit == 'CM') {
			var cbm = ((length * width * height) / 1000000) * noOfCar;
		}

		document.getElementById('id_totalVolume').value = cbm.toFixed(3);
		cbmFieldFunction();
	} catch (Exception) {
		document.getElementById('id_totalVolume').value = '';
	}

}

/* hamid */
function validateDecimalDataTypeStrict(element, event) {
	// alert(e.which);
	var charCode = (event.which) ? event.which : event.keyCode
	var ident = event.target.value;
	if ((charCode != 46 || ident.indexOf('.') != -1)
			&& (charCode < 48 || charCode > 57)) {
		return false;
	} else {
		return true;
	}

}

function validateDateFieldsBookingUpdate() {
	var count = 0;
	/*
	 * if ($("#lcexpdt").val() != "") { var isLcExpDtValid =
	 * validatedate($("#lcexpdt").val()); if (!isLcExpDtValid) {
	 * $("#lcexpdt").focus(); alert("Invalid Date, Only dd-mm-yyyy Format Valid
	 * For LC Expiry Date"); return false; } else { //return true; count++; } }
	 * else { count++; } if ($("#lcomInvDate").val() != "") { var
	 * isComInvDtValid = validatedate($("#lcomInvDate").val()); if
	 * (!isComInvDtValid) { $("#lcomInvDate").focus(); alert("Invalid Date, Only
	 * dd-mm-yyyy Format Valid For Commercial Invoice Date"); return false; }
	 * else { //return true; count++; } } else { count++; }
	 */
	if ($("#expDate").val() != "") {
		var isExpDtValid = validatedate($("#expDate").val());
		if (!isExpDtValid) {
			$("#expDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Exp. Date");
			return false;
		} else {
			// return true;
			count++;
		}
	} else {
		count++;
	}
	if ($("#lcTtPoDate").val() != "") {
		var isLcTtPoDtValid = validatedate($("#lcTtPoDate").val());
		if (!isLcTtPoDtValid) {
			$("#lcTtPoDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For LC/TT/PO Date");
			return false;
		} else {
			// return true;
			count++;
		}
	} else {
		count++;
	}
	/*
	 * if ($("#cargoHandoverDate").val() != "") { var ischdtValid =
	 * validatedate($("#cargoHandoverDate").val()); if (!ischdtValid) {
	 * $("#cargoHandoverDate").focus(); alert("Invalid Date, Only dd-mm-yyyy
	 * Format Valid For Cargo Handover Date"); return false; } else { //return
	 * true; count++; } } else { count++; }
	 */
	if (count == 2) {
		return true;
	} else {
		return false;
	}
}

function validatedate(inputText) {
	var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	// Match the date format through regular expression
	if (inputText.match(dateformat)) {
		// document.form1.text1.focus();
		// Test which seperator is used '/' or '-'
		/*
		 * var opera1 = inputText.split('/'); var opera2 = inputText.split('-');
		 * lopera1 = opera1.length; lopera2 = opera2.length; if (lopera1 > 1) {
		 * var pdate = inputText.split('/'); } else if (lopera2 > 1) {
		 */
		var pdate = inputText.split('-');
		// }
		var dd = parseInt(pdate[0]);
		var mm = parseInt(pdate[1]);
		var yy = parseInt(pdate[2]);
		// Create list of days of a month [assume there is no leap year by
		// default]
		var ListofDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		if (yy < 1900 || yy > 2100) {
			// alert('Invalid date format!');
			return false;
		}
		if (mm < 0 || mm > 12) {
			// alert('Invalid date format!');
			return false;
		}
		if (mm == 1 || mm > 2) {
			if (dd > ListofDays[mm - 1]) {
				// alert('Invalid date format!');
				return false;
			}
		}
		if (mm == 2) {
			var lyear = false;
			if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
				lyear = true;
			}
			if ((lyear == false) && (dd >= 29)) {
				// alert('Invalid date format!');
				return false;
			}
			if ((lyear == true) && (dd > 29)) {
				// alert('Invalid date format!');
				return false;
			}
		}
		return true;
	} else {
		return false;
	}
}
// nusrat
function addToLog(field, prev, updated, lineItemNo, type) {
	return modelcopy = {
		fieldName : field,
		prev : prev,
		updated : updated,
		lineItemNo : lineItemNo,
		dataType : type

	}
}

// hamid
function createTableGoodsReceived(tableData) {

	$("#grValueTable> tbody").empty();

	var table = document.getElementById('grValueTable');

	var tableBody = document.getElementById('grValueTableBody');
	var rowCounter = 0;

	tableData.forEach(function(rowData) {

		if (rowData != null) {
			// create row
			var row = document.createElement('tr');

			// createAnchor(row, rowCounter);

			var itemNumber = rowData.itemNumber;
			createCellData(row, itemNumber);

			var poNumber = rowData.poNumber;
			createCellData(row, poNumber);

			var styleNo = rowData.styleNo;
			createCellData(row, styleNo);

			var sizeNo = rowData.sizeNo;
			createCellData(row, sizeNo);

			var color = rowData.color;
			createCellData(row, color);

			var hsCode = rowData.hsCode;
			createCellData(row, hsCode);

			var material = rowData.material;
			createCellData(row, material);

			createDisplayAnchor(row, rowCounter);

			// appending new row
			tableBody.appendChild(row);
			rowCounter++;
		}

	});

	table.appendChild(tableBody);
	document.getElementById('headingTwo').appendChild(table);

}

function createDisplayAnchor(row, rowCounter) {

	var a = document.createElement('a');
	var data_toggle = document.createAttribute('data-toggle');
	data_toggle.value = 'modal';
	var data_target = document.createAttribute('data-target');
	data_target.value = '#displayFormModal';
	var cl = document.createAttribute('class');
	cl.value = 'button';
	var onClick = document.createAttribute('onclick');
	onClick.value = "setValues(modelArray[" + rowCounter + "]," + rowCounter
			+ ")";
	a.setAttributeNode(data_toggle);
	a.setAttributeNode(data_target);
	a.setAttributeNode(cl);
	a.setAttributeNode(onClick);
	/* a.innerText = 'Expand'; */
	/* a.innerHTML = "<i class='fa fa-pencil-square-o' aria-hidden='true'></i>"; */
	a.innerHTML = "<a class='btn btn-default btn-sm'><i class='fa fa-search-plus' aria-hidden='true'></i>&nbsp;View</a>";

	var cell = document.createElement('td');
	cell.appendChild(a);
	row.appendChild(cell);

}
function cbmFieldFunction() {
	var TotalCbm = document.getElementById("id_totalVolume");
	var width = document.getElementById("id_cartonWidth").value;
	var height = document.getElementById("id_cartonHeight").value;
	var length = document.getElementById("id_cartonLength").value;
	var quantity = document.getElementById("id_cartonQuantity").value;

	if (width <= 0 || height <= 0 || length <= 0 || quantity <= 0) {
		TotalCbm.disabled = false;

	} else if (width != "" && height != "" && length != "" && quantity != "") {
		TotalCbm.disabled = true;
	} else {
		TotalCbm.disabled = false;
	}
}
function cbmFieldFunctionFromAddModal() {
	var TotalCbm = document.getElementById("id_add_totalVolume");
	var width = document.getElementById("id_add_cartonWidth").value;
	var height = document.getElementById("id_add_cartonHeight").value;
	var length = document.getElementById("id_add_cartonLength").value;
	var quantity = document.getElementById("id_add_cartonQuantity").value;

	if (width <= 0 || height <= 0 || length <= 0 || quantity <= 0) {
		TotalCbm.disabled = false;

	} else if (width != "" && height != "" && length != "" && quantity != "") {
		TotalCbm.disabled = true;
	} else {
		TotalCbm.disabled = false;
	}
}
function checkCharacterLimit() {
	/* var text = $("#disccriptonOfGoods").val(); */
	var LINE_LENGTH_CHARS = 33;
	var lines = $("#description").val().split("\n");
	var last_line = lines[lines.length - 1];
	if (last_line.length >= LINE_LENGTH_CHARS) {
		$("#description").val($("#description").val() + "\n")
	}
}
function checkCharacterLimitShipingMark() {
	/* var text = $("#disccriptonOfGoods").val(); */
	var LINE_LENGTH_CHARS = 33;
	var lines = $("#shippingMark").val().split("\n");
	var last_line = lines[lines.length - 1];
	if (last_line.length >= LINE_LENGTH_CHARS) {
		$("#shippingMark").val($("#shippingMark").val() + "\n")
	}
}