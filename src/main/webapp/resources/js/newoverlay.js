
var interval;
(function($, undefined){
    var _defaults = {
        color           : "rgba(255, 255, 255, 1)",
        custom          : "",
        fade            : true,
		imageFadeInTime	:0,
		dotInterval		:200,
        fontawesome     : "",
        image           : myContextPath+"/resources/images/loader.png",
        imagePosition   : "center center",
        maxSize         : "100px",
        minSize         : "20px",
        resizeInterval  : 0,
        size            : "50%",
        zIndex          : undefined,
		setInterval		:"",
		displayText     :"Processing"
			
    };

    $.LoadingOverlaySetup = function(settings){
        $.extend(true, _defaults, settings);
    };

    $.LoadingOverlay = function(action, options){
        switch (action.toLowerCase()) {
            case "show":
                var settings = $.extend(true, {}, _defaults, options);
                _Show("body", settings);
                break;
                
            case "hide":
                _Hide("body", settings);
                break;
        }
    };

    $.fn.LoadingOverlay = function(action, options){
        switch (action.toLowerCase()) {
            case "show":
                var settings = $.extend(true, {}, _defaults, options);
                return this.each(function(){
                    _Show(this, settings);
                });
                
            case "hide":
                return this.each(function(){
                    _Hide(this, settings);
                });
        }
    };


    function _Show(container, settings){
        container = $(container);
        var fixed = container.is("body");
        
        
            var overlay = $("<div>", {
                class   : "loadingoverlay",
                css     : {
                    "position"  : "fixed",
                    "width"     : "100%",
                    "height"    : "100%",
                    "top"       : "0",
					"left"		:"0",
                    "background-color"   : "gray",
					"z-index":"1000",
					"opacity":"0.9"
                }
            });
           
             var fadingDiv= $("<div>", {
				 class   : "fade1",
                css:{
					"height": "200px", 
                "width": "400px",
                "margin": "200px auto", 
                "border-radius": "110px"
				}
            });
			var image=$("<img>",{
						class:"namefade",
						css:{
							
                 "margin-left":"70px",
                 "padding-top": "60px",
                	 "width":"263px",
                	 "height":"115px"
						},
						src:myContextPath+"/resources/images/loader.png",
						onload:"fadeIn(this)"
				
			});
			var para=$("<p></p>",{
				id:"loading",
				text:"Processing",
				css:{
					"color": "#fff",
                 "padding-left":"170px",
                 "font-size":"18px",
                 "margin-top": "20px",
                 "float":"left"
				}
			});
			

			var spinPara=$("<p></p>",{
				class:"img-para",
				css:{
					"float":"left",
					"margin-top":"25px",
					"margin-left":"10px"
				}
			});
			
			var spinImage=$("<img>",{
						class:"spin-img",
						css:{
							
						},
						src:myContextPath+"/resources/images/ajax-loader.gif",
						onload:"fadeIn(this)"
				
			});
			image.appendTo(fadingDiv);
			spinImage.appendTo(spinPara);
			para.appendTo(fadingDiv);
			spinPara.appendTo(fadingDiv);
			fadingDiv.appendTo(overlay);
			//overlay.hide().appendTo(container).fadeIn(settings.imageFadeInTime);
            var isLoaderLoaded=$(container).find(".loadingoverlay");
            if(isLoaderLoaded!=null && isLoaderLoaded!=undefined && isLoaderLoaded.length>0)
            	$(isLoaderLoaded).fadeIn(settings.imageFadeInTime);
            else
            	overlay.appendTo(container).fadeIn(settings.imageFadeInTime);	
			 window.fadeIn = function(obj) {
              $(obj).fadeIn(settings.imageFadeInTime);
              }
		
    }

    function _Hide(container, settings){
        container = $(container);
            container.children(".loadingoverlay").fadeOut( function(){
			});
            container.children(".loadingoverlay").remove();
			
           
    }

    

}(jQuery));
