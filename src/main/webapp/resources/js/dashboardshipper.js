//Morris charts snippet - js



function zoom() {
	document.body.style.zoom = "90%";

}

var buyerIdMap={};
function drawChart(frmDate, toDate)
{
	//var $overlay=$.LoadingOverlay("show");
$.getScript('resources/js/canvasjsmin.js',function(){
	//$.LoadingOverlay("show");
	if(sowiseShipmentSummary != undefined && topFiveShipment != undefined) {
		var colours=createColorArray(sowiseShipmentSummary.lstSalesOrgWiseShipCount.length);
		var salesData=parseData(sowiseShipmentSummary.lstSalesOrgWiseShipCount);
		$("#bar-example").CanvasJSChart({
			backgroundColor : null,
			title:{
		        text: "Company wise Shipment"
		      },
		      data:[
		            {
		            	click: function(e){ 
		            		var fromDate=$("#dashboard_from_date").val();
		            	    var toDate=$("#dashboard_to_date").val();
		            	    if(e.dataPoint.x!=undefined)
		            	    var salesOrg=e.dataPoint.x;
		            	    else
		            	    	var salesOrg=e.dataPoint.label;
		            	    salesOrg=buyerIdMap[salesOrg];
		            	    
		            	   // alert(salesOrg);
		            	    if(salesOrg!=undefined)
		            	    window.location.href=myContextPath+'/sowiseshipdetailsshipper?fromDate='+fromDate+'&toDate='+toDate+'&salesOrg='+salesOrg;
		            	    
		            	  },
		            	  
		            	  type: "column",
		            	  dataPoints:salesData
		            }
		            ]
		});
		$("#chartContainer").CanvasJSChart({

			backgroundColor : null,
			title : {
				text : "",
				fontSize : 24
			},
			axisY : {
				title : "Products in %"
			},
			legend : {
				verticalAlign : "center",
				horizontalAlign : "right"
			},
			data : [ {
				type : "pie",
				showInLegend : true,
				click: function(e){ 
            	    //alert(  e.dataSeries.type+ " x:" + e.dataPoint.label + ", y: "+ e.dataPoint.y);
            	    var fromDate=$("#dashboard_from_date").val();
            	    var toDate=$("#dashboard_to_date").val();
            	    var blStatus=e.dataPoint.label;
            	    window.location.href=myContextPath+'/blstatusshipper?fromDate='+fromDate+'&toDate='+toDate+'&blStatus='+blStatus;
            	  },
				toolTipContent : "{label} <br/> {y} %",
				indexLabel : "{y} %",
				dataPoints : [ {
					label : "Pending",
					y : sowiseShipmentSummary.blPendingPercentage,
					legendText : "Pending",
					cursor: "pointer"
				}, {
					label : "Released",
					y : sowiseShipmentSummary.blReleasePercentage,
					legendText : "Released",
					cursor: "pointer"
				}

				]
			} ]
		});
		$("#total-shipment").text(Math.round(sowiseShipmentSummary.totalShipmentCount));
		$("#total-cbm").text(Number(sowiseShipmentSummary.totalCBMCount.toFixed(0)).toLocaleString());
		$("#total-gwt").text(Number(sowiseShipmentSummary.totalGWTCount.toFixed(0)).toLocaleString());
		var tableRow=createTable(topFiveShipment.topFiveShipmentJsonData)
		$("#tbl-top-shipment").find("tbody").empty();
		$("#tbl-top-shipment").find("tbody").append(tableRow);
		
		if(lastNShipmentsJsonOutputData != undefined) {
			var table='';
			var itHeaderBeanLst=lastNShipmentsJsonOutputData.itHeaderBeanLst;
			$.each(itHeaderBeanLst,function(index,value){
				table=table+'<tr>';
				if(value.zzhblhawbno==null || value.zzhblhawbno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td><a href=getTrackingInfoFrmUrl?searchString='+value.zzhblhawbno+'>'+value.zzhblhawbno+'</a></td>';
				
				if(value.zzcomminvno==null || value.zzcomminvno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzcomminvno+'</td>';
				
				if(value.zzportofloading==null || value.zzportofloading==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofloading+'</td>';
				if(value.zzportofdest==null || value.zzportofdest==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofdest+'</td>';
				
				if(value.zzairlinename1==null || value.zzairlinename1==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzairlinename1+'</td>';
				
				if(value.fvEtdPort==null || value.fvEtdPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.fvEtdPort+'</td>';
				
				if(value.fvEtaPort==null || value.fvEtaPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.fvEtaPort+'</td>';
				
				if(value.zzairlinename2==null || value.zzairlinename2==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzairlinename2+'</td>';
				if(value.mvEtdPort==null || value.mvEtdPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.mvEtdPort+'</td>';
				
				if(value.mvEtaaPort==null || value.mvEtaaPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.mvEtaaPort+'</td>';
						
				 table=table+'</tr>';
			})
			$("#tbl-recent-shipment").find("tbody").empty();
			$("#tbl-recent-shipment").find("tbody").append(table);
		}
		
		
	}
			/*$.when($.ajax({
		url : myContextPath + '/sowiseshipsummaryshipper',
		data : {
			fromDate : frmDate,
			toDate : toDate
		},
		type : "GET",
		dataType : "json",

	success:function(result1) {
		
	},
	error:function(xhr, status, errorThrown) {
		alert("Sorry, there was a problem!");
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
		
	}
	}),$.ajax({
		url : myContextPath + '/top5shipmentshipper',
		data : {
			fromDate : frmDate,
			toDate : toDate
		},
		type : "GET",
		dataType : "json",

	success:function(result2) {
		
	},
	error:function(xhr, status, errorThrown) {
		alert("Sorry, there was a problem!");
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
		//$.LoadingOverlay("hide")
	}
	})).then(function(result1,result2){
		var colours=createColorArray(result1[0].lstSalesOrgWiseShipCount.length);
		var salesData=parseData(result1[0].lstSalesOrgWiseShipCount);
		$("#bar-example").CanvasJSChart({
			backgroundColor : null,
			title:{
		        text: "Company wise Shipment"
		      },
		      data:[
		            {
		            	click: function(e){ 
		            		var fromDate=$("#dashboard_from_date").val();
		            	    var toDate=$("#dashboard_to_date").val();
		            	    if(e.dataPoint.x!=undefined)
		            	    var salesOrg=e.dataPoint.x;
		            	    else
		            	    	var salesOrg=e.dataPoint.label;
		            	    salesOrg=buyerIdMap[salesOrg];
		            	    
		            	   // alert(salesOrg);
		            	    if(salesOrg!=undefined)
		            	    window.location.href=myContextPath+'/sowiseshipdetailsshipper?fromDate='+fromDate+'&toDate='+toDate+'&salesOrg='+salesOrg;
		            	    
		            	  },
		            	  
		            	  type: "column",
		            	  dataPoints:salesData
		            }
		            ]
		});
		$("#chartContainer").CanvasJSChart({

			backgroundColor : null,
			title : {
				text : "",
				fontSize : 24
			},
			axisY : {
				title : "Products in %"
			},
			legend : {
				verticalAlign : "center",
				horizontalAlign : "right"
			},
			data : [ {
				type : "pie",
				showInLegend : true,
				click: function(e){ 
            	    //alert(  e.dataSeries.type+ " x:" + e.dataPoint.label + ", y: "+ e.dataPoint.y);
            	    var fromDate=$("#dashboard_from_date").val();
            	    var toDate=$("#dashboard_to_date").val();
            	    var blStatus=e.dataPoint.label;
            	    window.location.href=myContextPath+'/blstatusshipper?fromDate='+fromDate+'&toDate='+toDate+'&blStatus='+blStatus;
            	  },
				toolTipContent : "{label} <br/> {y} %",
				indexLabel : "{y} %",
				dataPoints : [ {
					label : "Pending",
					y : result1[0].blPendingPercentage,
					legendText : "Pending",
					cursor: "pointer"
				}, {
					label : "Released",
					y : result1[0].blReleasePercentage,
					legendText : "Released",
					cursor: "pointer"
				}

				]
			} ]
		});
		$("#total-shipment").text(Math.round(result1[0].totalShipmentCount));
		$("#total-cbm").text(Number(result1[0].totalCBMCount.toFixed(0)).toLocaleString());
		$("#total-gwt").text(Number(result1[0].totalGWTCount.toFixed(0)).toLocaleString());
		var tableRow=createTable(result2[0].topFiveShipmentJsonData)
		$("#tbl-top-shipment").find("tbody").empty();
		$("#tbl-top-shipment").find("tbody").append(tableRow);
		
	}).then(function(){
		$.ajax({
			url : myContextPath + '/recentshipment',
		data : {
			NoOfRecords:5
		},
		type : "GET",
		dataType : "json",
		success:function(data){
			var table='';
			var itHeaderBeanLst=data.itHeaderBeanLst;
			$.each(itHeaderBeanLst,function(index,value){
				table=table+'<tr>';
				if(value.zzhblhawbno==null || value.zzhblhawbno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td><a href=getTrackingInfoFrmUrl?searchString='+value.zzhblhawbno+'>'+value.zzhblhawbno+'</a></td>';
				
				if(value.zzcomminvno==null || value.zzcomminvno==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzcomminvno+'</td>';
				
				if(value.zzportofloading==null || value.zzportofloading==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofloading+'</td>';
				if(value.zzportofdest==null || value.zzportofdest==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzportofdest+'</td>';
				
				if(value.zzairlinename1==null || value.zzairlinename1==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzairlinename1+'</td>';
				
				if(value.fvEtdPort==null || value.fvEtdPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.fvEtdPort+'</td>';
				
				if(value.fvEtaPort==null || value.fvEtaPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.fvEtaPort+'</td>';
				
				if(value.zzairlinename2==null || value.zzairlinename2==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.zzairlinename2+'</td>';
				if(value.mvEtdPort==null || value.mvEtdPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.mvEtdPort+'</td>';
				
				if(value.mvEtaaPort==null || value.mvEtaaPort==undefined)
					table=table+'<td></td>';
				else
					table=table+'<td>'+value.mvEtaaPort+'</td>';
						
				 table=table+'</tr>';
		})
		$("#tbl-recent-shipment").find("tbody").empty();
		$("#tbl-recent-shipment").find("tbody").append(table);
		$.LoadingOverlay("hide");
		}
		})
	})*/
			
	
});
//var $overlay=$.LoadingOverlay("hide");
	}
function createTable(topFiveShipmentJsonData)
{
	var tableData="";
	$.each(topFiveShipmentJsonData,function(index,value){
	var tr='<tr>';
	tr+='<td><a href="#pod" onclick="handleClick(this)" >'+value.portOfDischarge+'</a></td><td>'+value.totalShipment+'</td><td>'+value.totalCBM.toFixed(2)+'</td><td>'+value.totalGWT.toFixed(2)+'</td>';
	tr+='</tr>'
		tableData+=tr;
	})
	return tableData;
	}

function handleClick(element)
{
	var fromDate=$("#dashboard_from_date").val();
    var toDate=$("#dashboard_to_date").val();
    var pod=element.text;
    var url=myContextPath+'/buyerwiseshipdetailsbypod?fromDate='+fromDate+'&toDate='+toDate+'&pod='+pod;
    window.location=url;
	//alert("handled"+element.text);
	}
function createColorArray( length)
{
var arrayColour=[];
for(var i=0;i<length;i++)
	{
	var color='#'+(Math.random()*0xFFFFFF<<0).toString(16);
	arrayColour.push(color);
	}
return arrayColour;
}



function parseData(lstSalesOrgWiseShipCount)
{
	var arrObj=[];
	var object={};
	var count=0;
	$.each(lstSalesOrgWiseShipCount,function(index,value){
		count++;
		buyerIdMap[count]=value.salesOrg;
		if($.isNumeric(value.salesOrg))
			{
			object['x']=Number(count);
			object['label']=value.salesOrgName;
			}
		else
			{
			object['label']=count;
			}
		
		
		object['y']=Number(value.shipmentCount);
		object['cursor']='pointer'
		arrObj.push(object);
		object={};
	})
	return arrObj;
	}

function getSoShipmentDetails(frmDate, toDate) {
	//var $overlay = $.LoadingOverlay("show");
	
	$.ajax({
		url : myContextPath + '/sowiseshipsummaryshipper',
		data : {
			fromDate : frmDate,
			toDate : toDate
		},
		type : "GET",
		dataType : "json",

	success:function(data) {
		var jsonString = JSON.stringify(data);
		
		//$.LoadingOverlay("hide")
		drawChart(data);
	},
	error:function(xhr, status, errorThrown) {
		alert("Sorry, there was a problem!");
		console.log("Error: " + errorThrown);
		console.log("Status: " + status);
		console.dir(xhr);
		//$.LoadingOverlay("hide")
	}
	})
}