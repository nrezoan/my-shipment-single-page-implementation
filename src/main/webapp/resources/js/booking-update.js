$(document)
		.ready(
				function() {

					$('a[href="#addItem"]').click(function() {
						$("#addItem").css("display", "");
						$("#update-2").css("display", "none");
						$("#collapseTwo").toggle();

					})

					$("#update").click(function(e) {
						if (validateHeaderMandatory()) {
							if (validateHeaderDataType()) {
								updateHeader();
								$("#collapseOne").toggle();
								var text = $('a[href="#One"]').text();
								if (text == 'Expand')
									$('a[href="#One"]').text("Collapse");
								else
									$('a[href="#One"]').text("Expand");
							}
						}
					})

					$('a[href="#One"]').click(function(e) {
						$("#collapseOne").toggle();
						var text = $('a[href="#One"]').text();
						if (text == 'Expand')
							$('a[href="#One"]').text("Collapse");
						else
							$('a[href="#One"]').text("Expand");

					})

					$('a[href="#Three"]').click(function(e) {
						$("#collapseThree").toggle();
						var text = $('a[href="#Three"]').text();
						if (text == 'Expand')
							$('a[href="#Three"]').text("Collapse");
						else
							$('a[href="#Three"]').text("Expand");
						renderPackingList();

					})

					// line item update
					$("#update-2")
							.click(
									function(e) {

										if (validateLineItemMandatory()) {
											if (validateLineItemDataType()) {
												var lineItem = {};
												var itemNumber = $(
														"#collapseTwo")
														.find(
																'input[name="itemNumber"]')
														.val();
												var lstItems = directBookingParams.orderItemLst;
												$
														.each(
																lstItems,
																function(index,
																		value) {
																	if (parseInt(value.itemNumber) === parseInt(itemNumber)) {
																		lineItem = value;
																	}
																})
												$("#collapseTwo")
														.find(
																'input[type="text"]')
														.each(
																function(index,
																		value) {
																	var name = $(
																			value)
																			.attr(
																					"name");
																	if (isPropertyOfObject(
																			lineItem,
																			name)) {
																		lineItem[name] = $(
																				value)
																				.val();
																	}
																})
												$("#collapseTwo")
														.find('textarea')
														.each(
																function(index,
																		value) {
																	var name = $(
																			value)
																			.attr(
																					"name");
																	if (isPropertyOfObject(
																			lineItem,
																			name)) {
																		lineItem[name] = $(
																				value)
																				.val();
																	}
																})
												/*
												 * $("#collapseTwo").find('select').each(function(index,value){
												 * var
												 * name=$(value).attr("name");
												 * if(isPropertyOfObject(lineItem,name)) {
												 * lineItem[name]=$(value).val(); } })
												 */
												var lstItemsNew = $.extend(
														true, {}, lstItems);
												;
												$
														.each(
																lstItemsNew,
																function(index,
																		value) {
																	if (parseInt(value.itemNumber) === parseInt(lineItem.itemNumber)) {
																		lstItems
																				.splice(
																						index,
																						1);
																	}
																})

												lstItems.push(lineItem);
												directBookingParams.orderItemLst = lstItems;
												$.LoadingOverlay("show");
												$
														.when(
																$
																		.ajax({
																			url : myContextPath
																					+ '/doBookingUpdate',
																			type : "POST",
																			data : JSON
																					.stringify(directBookingParams),
																			contentType : 'application/json',
																			dataType : 'json'
																		}))
														.then(
																function(
																		data,
																		textStatus,
																		jqXHR) {
																	$
																			.LoadingOverlay("hide");

																	if (textStatus == 'success') {
																		$(
																				"#span-message")
																				.addClass(
																						"message-success");
																		$(
																				"#span-message")
																				.text(
																						"Linte Item updated successfully!");
																		$(
																				"#collapseTwo")
																				.toggle();
																		$
																				.each(
																						$(
																								"#headingTwo")
																								.find(
																										"table")
																								.find(
																										"a"),
																						function(
																								index,
																								value) {
																							if ($(
																									value)
																									.text() == 'Collapse') {
																								$(
																										value)
																										.text(
																												"Expand");
																							}
																						})
																	} else {
																		$(
																				"#span-message")
																				.addClass(
																						"message-error");
																		$(
																				"#span-message")
																				.text(
																						"Error occured while saving ! Please contact Admin !");
																	}
																})
												/*
												 * $.ajax({
												 * url:myContextPath+'/doBookingUpdate',
												 * type:"POST",
												 * data:JSON.stringify(directBookingParams),
												 * contentType :
												 * 'application/json',
												 * dataType:'json',
												 * success:function(data){
												 * $.LoadingOverlay("hide");
												 * return itemNumber;
												 * //$("#span-message").addClass("message-success");
												 * //$("#span-message").text("Header
												 * details updated
												 * successfully!"); },
												 * error:function(){
												 * //$("#span-message").addClass("message-error");
												 * //$("#span-message").text("Error
												 * occured while saving !");
												 * $.LoadingOverlay("hide");
												 * return "Error occured while
												 * saving !"; } })
												 */
												// var
												// itemNumber=updateLineItem();
												/*
												 * if(isNaN(itemNumber)) {
												 * $("#span-message").addClass("message-error");
												 * $("#span-message").text(itemNumber); }
												 * else {
												 * $("#collapseTwo").toggle();
												 * $("#span-message").addClass("message-success");
												 * $("#span-message").text("Line
												 * item updated successfully
												 * !");
												 * $.each($("#headingTwo").find("table").find("a"),function(index,value){
												 * if($(value).text()=='Collapse');
												 * $(value).text("Expand"); }) }
												 * 
												 */

											}
										}
									})

					$('a[href="#Two"]').click(
							function(e) {
								var $this = $(this);
								var text = $($this).text();
								var parent = $($this).parent().parent()
										.parent().parent();
								if (text == "Expand") {
									var tr = $(parent).find('a');
									$.each(tr, function(index, value) {
										if ($(value).text() == 'Collapse') {
											$(value).text("Expand");
										}
									})
									var itemNumber = $this.parent().parent()
											.find("input").val();
									renderItems(itemNumber);
									var cssProp = $("#collapseTwo").css(
											"display");
									if (cssProp == "none") {
										$("#collapseTwo").toggle();
										$("#addItem").css("display", "none");
										$("#update-2").css("display", "");
									}
									$($this).text("Collapse");
								} else if (text == "Collapse") {
									$("#collapseTwo").toggle();
									$($this).text("Expand");
								}

							})

					$("#cancel-head").click(function(e) {
						$("#collapseOne").toggle();
						var text = $('a[href="#One"]').text();
						if (text == 'Expand')
							$('a[href="#One"]').text("Collapse");
						else
							$('a[href="#One"]').text("Expand");

					})
					$("#cancel-item").click(
							function(e) {
								$("#collapseTwo").toggle();

								$.each(
										$("#headingTwo").find("table")
												.find("a"), function(index,
												value) {
											if ($(value).text() == 'Collapse') {
												$(value).text("Expand");
											}

										})

							})

					$("#btn-add-item")
							.click(
									function() {
										var tableRow = $("#table-container")
												.find("table").find("thead")
												.find("tr");

										var NoOfCol = $(tableRow[1]).find("th").length;
										if (NoOfCol <= 0) {
											alert("Please add Size details before adding items !");
										} else {
											var tblRow = '<tr>';
											var tblCol = '';
											var noOfItemRows = $(
													"#table-container").find(
													"table").find("tbody")
													.find("tr").length;
											if (noOfItemRows == 0) {
												noOfItemRows++;
												tblCol = tblCol
														+ '<td ><input type="text" id="ctnSrlNo" style=" height:30px;" ></td>';
												tblCol = tblCol
														+ '<td ><input type="text" id="noOfContainers" style=" height:30px;" ></td>';
											} else {
												noOfItemRows++;
												var firstRow = $(
														"#table-container")
														.find("table").find(
																"tbody").find(
																"tr")[0];
												var columns = $(firstRow).find(
														"td");
												$(columns[0]).attr("rowspan",
														noOfItemRows);
												$(columns[1]).attr("rowspan",
														noOfItemRows);
											}

											tblCol = tblCol
													+ '<td ><input type="text" id="color_'
													+ noOfItemRows
													+ '" style=" height:30px;" ></td>';
											for (var i = 0; i < NoOfCol; i++) {
												tblCol = tblCol
														+ '<td ><input type="text" id="size_'
														+ (i + 1)
														+ '_'
														+ noOfItemRows
														+ '" style=" height:30px;" ></td>';
											}

											tblCol = tblCol
													+ '<td ><input type="text" id="pcs_per_car_'
													+ noOfItemRows
													+ '" style=" height:30px;" ></td>';
											tblCol = tblCol
													+ '<td ><input type="text" id="total_pcs_'
													+ noOfItemRows
													+ '" style=" height:30px;" ></td>';
											tblCol = tblCol
													+ '<td ><input type="text" id="remarks_'
													+ noOfItemRows
													+ '" style=" height:30px;" ></td>';
											tblCol = tblCol + '<td ></td>';

											tblRow = tblRow + tblCol + '</tr>';
											$("#table-container").find("table")
													.find("tbody").append(
															tblRow);
										}

									})

					$("#btn-add-size")
							.click(
									function() {
										var text = $("#txt-add-size").val()
												.toUpperCase();
										if (text.trim() == null
												|| text.trim() == '')
											alert("Please enter valid size names!");
										else {
											if ($("#table-container").find(
													"table").find("tbody")
													.find("tr").length > 0) {
												alert(" Please remove items to add size !");

											} else {
												var tableRow = $(
														"#table-container")
														.find("table").find(
																"thead").find(
																"tr");
												var alreadyExist = false;
												$
														.each(
																$(tableRow[1])
																		.find(
																				"th"),
																function(index,
																		value) {
																	if (value.innerText == text) {
																		alert("Please enter different Size name!");
																		alreadyExist = true;
																		return false;
																	}
																})
												if (!alreadyExist) {
													var tableColumn = $(
															tableRow[0]).find(
															"th");
													var colspan = $(
															tableColumn[3])
															.attr("colspan");
													$(tableColumn[3])
															.removeAttr(
																	"colspan");
													$(tableColumn[3]).attr(
															"colspan",
															++colspan);
													$(tableRow[1]).append(
															'<th>' + text
																	+ '</th>');
												}
											}
										}
									})

					$("#btn-update-pck-lst").click(updatePackingList);
					$("#cancel-packlist").click(function() {
						$("#collapseThree").toggle();

					})
					$(".date-picker").datepicker({
						dateFormat : "dd-mm-yy",
						changeYear : true
					});

					/*
					 * $("#btn-upload").click(function(){ var
					 * files=$("#uploadXL")[0].files; handleUpload(files); })
					 */

				})
function headerUpdate(){
	
	/*var headerItem = {
			
			
	};
	mo*/
}

//tahmid
function setValues(model, rowCount) {

	rowNumber = rowCount;
	/*
	console.log("TEST");
	console.log(model);
	document.getElementById("id_poNo").value = model.poNumber;
	document.getElementById("id_hsCode").value = model.hsCode;

	document.getElementById("id_material").value = model.material;
	document.getElementById("id_styleNo").value = model.styleNo;
	// document.getElementById("id_refNo").value = model.refNo;
	document.getElementById("id_projectNo").value = model.projectNo;
	document.getElementById("id_productCode").value = model.productCode;
	document.getElementById("id_color").value = model.color;
	document.getElementById("id_sizeNo").value = model.sizeNo;
	document.getElementById("id_totalPieces").value = model.totalPieces;
	document.getElementById("id_cartonQuantity").value = model.curtonQuantity;
	// document.getElementById("").value = model.totalCbm;
	// document.getElementById("").value = model.grossWeight;
	document.getElementById("id_netWeight").value = model.netWeight;
	document.getElementById("id_payCond").value = model.payCond;
	document.getElementById("id_unitPrice").value = model.unitPrice;
	document.getElementById("id_currency").value = model.currency;

	document.getElementById("id_commInvoice").value = model.commInvoice;
	document.getElementById("id_commInvoiceDate").value = model.commInvoiceDate;
	document.getElementById("id_netCost").value = model.netCost;
	document.getElementById("id_pcsPerCarton").value = model.pcsPerCarton;
	// document.getElementById("").value = model.cbmPerCarton;
	document.getElementById("id_grossWeightPerCarton").value = model.grossWeightPerCarton;
	// document.getElementById("").value = model.netWeightPerCarton;
	document.getElementById("id_cartonLength").value = model.cartonLength;
	document.getElementById("id_cartonWidth").value = model.cartonWidth;
	document.getElementById("id_cartonHeight").value = model.cartonHeight;
	// document.getElementById("").value = model.cartonUnit;
	document.getElementById("id_cartonSerNo").value = model.cartonSerNo;
	document.getElementById("id_reference1").value = model.reference1;
	document.getElementById("id_reference2").value = model.reference2;
	document.getElementById("id_reference3").value = model.reference3;
	document.getElementById("id_reference4").value = model.reference4;
	document.getElementById("id_qcDate").value = model.qcDate;
	document.getElementById("id_releaseDate").value = model.releaseDate;
	document.getElementById("id_description").value = model.description;
	document.getElementById("id_itemDescription").value = model.itemDescription;
	document.getElementById("id_shippingMark").value = model.shippingMark;
	*/
	
	
	document.getElementById("id_poNo").value = model.poNumber;
	document.getElementById("id_cartonQuantity").value = model.curtonQuantity;
	document.getElementById("id_referNo").value = model.refNo;
	document.getElementById("id_grossWeight").value = model.grossWeight; // Total Gross Weight
	document.getElementById("id_totalPieces").value = model.totalPieces;
	document.getElementById("id_productCode").value = model.productCode; // Product Code / SKU
	document.getElementById("id_cartonLength").value = model.cartonLength;
	document.getElementById("id_netWeight").value = model.netWeight; // Total Net Weight
	document.getElementById("id_projectNo").value = model.projectNo; // Project No/ Article No
	document.getElementById("id_cartonWidth").value = model.cartonWidth; // Carton Width
	document.getElementById("id_styleNo").value = model.styleNo;
	document.getElementById("id_sizeNo").value = model.sizeNo;
	document.getElementById("id_cartonHeight").value = model.cartonHeight;
	document.getElementById("id_hsCode").value = model.hsCode;
	document.getElementById("id_color").value = model.color;
	//document.getElementById("id_cartonMeasurementUnit").value = model.;
	document.getElementById("id_material").value = model.material;// Commodity
	//document.getElementById("id_totalVolume").value = model.totalCbm; // Total CBM
	document.getElementById("id_totalVolume").value = model.totalVolume; // Total CBM
	
	
	document.getElementById("id_payCond").value = model.payCond; //Pay Conday Cond.
	document.getElementById("id_unitPrice").value = model.unitPrice;// Unit Price/Pcs
	document.getElementById("id_currency").value = model.currency; //Currency Unit
	
	
	document.getElementById("id_commInvoice").value = model.commInvoice;
	document.getElementById("id_commInvoiceDate").value = model.commInvoiceDate;
	document.getElementById("id_cartonSerNo").value = model.cartonSerNo; // Carton Serial No
	document.getElementById("id_grossWeightPerCarton").value = model.grossWeightPerCarton;// GWeight/Carton
	document.getElementById("id_netWeightPerCarton").value = model.netWeightPerCarton;// Net Weight/Carton 
	document.getElementById("id_pcsPerCarton").value = model.pcsPerCarton;// Pieces/Carton
	document.getElementById("id_netCost").value = model.netCost;// Net Cost
	document.getElementById("id_reference1").value = model.reference1;
	document.getElementById("id_reference2").value = model.reference2;
	document.getElementById("id_qcDate").value = model.qcDate;
	document.getElementById("id_reference3").value = model.reference3;
	document.getElementById("id_reference4").value = model.reference4;
	document.getElementById("id_releaseDate").value = model.releaseDate;
/*	document.getElementById("id_description").value;// Description of Goods
	document.getElementById("id_itemDescription").value;
	document.getElementById("id_shippingMark").value;*/

}

/*
 * function check(itemNumber) {
 * 
 * var number = 0; itemNumber = itemNumber.toString(); console.log(itemNumber);
 * console.log("test");
 * 
 * for (var count = 0; count < modelArray.length; count++) { //
 * console.log("loop"); console.log(count); // console.log(t[count].itemNumber);
 * 
 * if (modelArray[count].itemNumber == itemNumber) { number = count;
 * console.log("Row Number: "+number); rowNumber = number; return number; } } }
 */

function validateDateFieldsUpdateItemModal() {
	var count = 0;	
	if ($("#id_releaseDate").val() != "") {
		var isRelDtValid = validatedate($("#id_releaseDate").val());
		if (!isRelDtValid) {								
			$("#id_releaseDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Release Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ($("#id_qcDate").val() != "") {
		var isQcDtValid = validatedate($("#id_qcDate").val());
		if (!isQcDtValid) {								
			$("#id_qcDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For QC Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ($("#id_commInvoiceDate").val() != "") {
		var isComInvDtValid = validatedate($("#id_commInvoiceDate").val());
		if (!isComInvDtValid) {								
			$("#id_commInvoiceDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Commercial Invoice Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if(count == 3) {
		return true;
	}
	else {
		return false;
	}
}

function validateDateFieldsAddItemModal() {
	var count = 0;	
	if ($("#id_add_releaseDate").val() != "") {
		var isRelDtValid = validatedate($("#id_add_releaseDate").val());
		if (!isRelDtValid) {								
			$("#id_add_releaseDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Release Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ($("#id_add_qcDate").val() != "") {
		var isQcDtValid = validatedate($("#id_add_qcDate").val());
		if (!isQcDtValid) {								
			$("#id_add_qcDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For QC Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ($("#id_add_commInvoiceDate").val() != "") {
		var isComInvDtValid = validatedate($("#id_add_commInvoiceDate").val());
		if (!isComInvDtValid) {								
			$("#id_add_commInvoiceDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Commercial Invoice Date");
			return false;
		}
		else {
			//return true;
			count++;
		}
	}
	else {
		count++;
	}
	if(count == 3) {
		return true;
	}
	else {
		return false;
	}
}

function validatedate(inputText) {
	var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	// Match the date format through regular expression  
	if (inputText.match(dateformat)) {
		//document.form1.text1.focus();
		//Test which seperator is used '/' or '-'  
/*		var opera1 = inputText.split('/');
		var opera2 = inputText.split('-');
		lopera1 = opera1.length;
		lopera2 = opera2.length;
		if (lopera1 > 1) {
			var pdate = inputText.split('/');
		} else if (lopera2 > 1) {*/
		var pdate = inputText.split('-');
		//}
		var dd = parseInt(pdate[0]);
		var mm = parseInt(pdate[1]);
		var yy = parseInt(pdate[2]);
		// Create list of days of a month [assume there is no leap year by default]  
		var ListofDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		if (yy < 1900 || yy > 2100) {
			//alert('Invalid date format!');
			return false;
		}
		if (mm < 0 || mm > 12) {
			//alert('Invalid date format!');
			return false;
		}
		if (mm == 1 || mm > 2) {
			if (dd > ListofDays[mm - 1]) {
				//alert('Invalid date format!');
				return false;
			}
		}
		if (mm == 2) {
			var lyear = false;
			if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
				lyear = true;
			}
			if ((lyear == false) && (dd >= 29)) {
				//alert('Invalid date format!');
				return false;
			}
			if ((lyear == true) && (dd > 29)) {
				//alert('Invalid date format!');
				return false;
			}
		}
		return true;
	} else {		
		return false;
	}
}


//nusrat
function submit_by_id(){
	if(validateDateFieldsUpdateItemModal() === false) {
		return false;
	}		
	var poNo = document.getElementById("id_poNo").value;
	if (poNo == "") {								
		$("#id_poNo").focus();
		alert("PO No. is mandatory field, cannot be left empty");
		return false;
	}
	var grossWeight = document.getElementById("id_grossWeight").value;
	if (grossWeight == "") {								
		$("#id_grossWeight").focus();
		alert("Gross Weight is mandatory field, cannot be left empty");
		return false;
	}
	var totalVolume = document.getElementById("id_totalVolume").value;
	if (totalVolume == "") {								
		$("#id_totalVolume").focus();
		alert("Total CBM is mandatory field, cannot be left empty");
		return false;
	}
	var cartonQuantity = document.getElementById("id_cartonQuantity").value;
	if (cartonQuantity == "") {								
		$("#id_cartonQuantity").focus();
		alert("Carton Quantity is mandatory field, cannot be left empty");
		return false;
	}
	var totalPieces = document.getElementById("id_totalPieces").value;
	if (totalPieces == "") {								
		$("#id_totalPieces").focus();
		alert("Total Pieces is mandatory field, cannot be left empty");
		return false;
	}
	var hsCode = document.getElementById("id_hsCode").value;
	if (hsCode == "") {								
		$("#id_hsCode").focus();
		alert("HS Code is mandatory field, cannot be left empty");
		return false;
	}
	var material = document.getElementById("id_material").value;
	if (material == "") {								
		$("#id_material").focus();
		alert("Material is mandatory field, cannot be left empty");
		return false;
	}	
	var styleNo = document.getElementById("id_styleNo").value;
	var projectNo = document.getElementById("id_projectNo").value;
	var productCode = document.getElementById("id_productCode").value;
	var color = document.getElementById("id_color").value;
	var sizeNo = document.getElementById("id_sizeNo").value;
	
	if (poNo == "" || grossWeight == "" || cartonQuantity == "" || totalPieces == ""
			|| hsCode == "" || material == "" || totalVolume == "") {
		alert("You can not add new item without mandatory fields!");
		return false;

	}else {

		modelArray[rowNumber].poNumber = poNo;

		modelArray[rowNumber].hsCode = hsCode;

		modelArray[rowNumber].material = material;

		modelArray[rowNumber].styleNo = styleNo;

		// document.getElementById("id_refNo").value = model.refNo;

		modelArray[rowNumber].projectNo = projectNo;

		modelArray[rowNumber].productCode = productCode;

		modelArray[rowNumber].color = color;

		modelArray[rowNumber].sizeNo = sizeNo;

		modelArray[rowNumber].totalPieces = totalPieces;

		modelArray[rowNumber].curtonQuantity = cartonQuantity;
		
		modelArray[rowNumber].totalVolume = totalVolume;
		
		modelArray[rowNumber].grossWeight = grossWeight;
		// document.getElementById("").value = model.totalCbm;
		// document.getElementById("").value = model.grossWeight;

		var netWeight = document.getElementById("id_netWeight").value;
		modelArray[rowNumber].netWeight = netWeight;

		var payCond = document.getElementById("id_payCond").value;
		modelArray[rowNumber].payCond = payCond;

		var unitPrice = document.getElementById("id_unitPrice").value;
		modelArray[rowNumber].unitPrice = unitPrice;

		var currency = document.getElementById("id_currency").value;
		modelArray[rowNumber].currency = currency;

		var commInvoice = document.getElementById("id_commInvoice").value;
		modelArray[rowNumber].commInvoice = commInvoice;

		var commInvoiceDate = document.getElementById("id_commInvoiceDate").value;
		modelArray[rowNumber].commInvoiceDate = commInvoiceDate;

		var netCost = document.getElementById("id_netCost").value;
		modelArray[rowNumber].netCost = netCost;

		var pcsPerCarton = document.getElementById("id_pcsPerCarton").value;
		// modelArray[rowNumber].pcsPerCarton = pcsPerCarton;

		// document.getElementById("").value = model.cbmPerCarton;
		var grossWeightPerCarton = document
				.getElementById("id_grossWeightPerCarton").value;
		modelArray[rowNumber].grossWeightPerCarton = grossWeightPerCarton;

		// document.getElementById("").value = model.netWeightPerCarton;
		var cartonLength = document.getElementById("id_cartonLength").value;
		modelArray[rowNumber].cartonLength = cartonLength;

		var cartonWidth = document.getElementById("id_cartonWidth").value;
		modelArray[rowNumber].cartonWidth = cartonWidth;

		var cartonHeight = document.getElementById("id_cartonHeight").value;	
		modelArray[rowNumber].cartonHeight = cartonHeight;

		// document.getElementById("").value = model.cartonUnit;

		var cartonSerNo = document.getElementById("id_cartonSerNo").value;
		modelArray[rowNumber].cartonSerNo = cartonSerNo;
		
		var referNo = document.getElementById("id_referNo").value;
		modelArray[rowNumber].refNo = referNo;

		var reference1 = document.getElementById("id_reference1").value;
		modelArray[rowNumber].reference1 = reference1;

		var reference2 = document.getElementById("id_reference2").value;
		modelArray[rowNumber].reference2 = reference2;

		var reference3 = document.getElementById("id_reference3").value;
		modelArray[rowNumber].reference3 = reference3;

		var reference4 = document.getElementById("id_reference4").value;
		modelArray[rowNumber].reference4 = reference4;

		var qcDate = document.getElementById("id_qcDate").value;
		modelArray[rowNumber].cartonLenght = cartonLength;

		var releaseDate = document.getElementById("id_releaseDate").value;
		modelArray[rowNumber].qcDate = qcDate;

		var description = document.getElementById("id_description").value;
		modelArray[rowNumber].description = description;

		var itemDescription = document.getElementById("id_itemDescription").value;
		modelArray[rowNumber].cartonLenght = itemDescription;

		var shippingMark = document.getElementById("id_shippingMark").value;
		modelArray[rowNumber].shippingMark = shippingMark;

		/*
		 * console.log("poNo: "+ poNo); console.log("hsCode: "+ hsCode);
		 * console.log("material: "+material); console.log("styleNo: "+
		 * styleNo); console.log("projectNo: "+ projectNo);
		 * console.log("productCode: "+ productCode); console.log("color: "+
		 * color); console.log("sizeNo: "+sizeNo); console.log("total Pieces:
		 * "+totalPieces); console.log("cartonQuantity: "+ cartonQuantity);
		 * console.log("netWeight: "+ netWeight); console.log("payCond: "+
		 * payCond); console.log("unitPrice: "+unitPrice);
		 * console.log("currency: "+ currency); console.log("commInvoice:
		 * "+commInvoice); console.log("commInvoiceDate: "+commInvoiceDate);
		 * console.log("netCost: "+ netCost); console.log("pcsPerCarton: "+
		 * pcsPerCarton); console.log("grossWeightPerCarton: "+
		 * grossWeightPerCarton); console.log("cartonLength: "+ cartonLength);
		 * console.log("cartonWeight: "+ cartonWeight);
		 * console.log("cartonSerNo: "+ cartonSerNo); console.log("reference1: "+
		 * reference1); console.log("reference2: "+ reference2);
		 * console.log("reference3: "+ reference3); console.log("reference4: "+
		 * reference4); console.log("qcDate: "+ qcDate);
		 * console.log("releaseDate: "+ releaseDate); console.log("description: "+
		 * description); console.log("itemDescription: "+ itemDescription);
		 * console.log("shippingMark: "+ shippingMark);
		 */
		 //nusrat
		 //for CRM Logger
/*		 if (modelArray[rowNumber].poNumber === copyArray[rowNumber].poNumber) {
		} else {
			modelCopy=addToLog("PO Number",copyArray[rowNumber].poNumber,modelArray[rowNumber].poNumber,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].hsCode === copyArray[rowNumber].hsCode) {
		} else {
			modelCopy=addToLog("HS Code",copyArray[rowNumber].hsCode,modelArray[rowNumber].hsCode,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].material === copyArray[rowNumber].material) {
		} else {
			modelCopy=addToLog("Commodity",copyArray[rowNumber].material,modelArray[rowNumber].material,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].sizeNo === copyArray[rowNumber].sizeNo) {
		} else {
			modelCopy=addToLog("Size",copyArray[rowNumber].sizeNo,modelArray[rowNumber].sizeNo,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].color === copyArray[rowNumber].color) {
		} else {
			modelCopy=addToLog("Color",copyArray[rowNumber].color,modelArray[rowNumber].color,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].styleNo === copyArray[rowNumber].styleNo) {
		} else {
			modelCopy=addToLog("Style",copyArray[rowNumber].styleNo,modelArray[rowNumber].styleNo,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].curtonQuantity === copyArray[rowNumber].curtonQuantity) {
		} else {
			modelCopy=addToLog("Carton Quantity",copyArray[rowNumber].curtonQuantity,modelArray[rowNumber].curtonQuantity,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].totalPieces === copyArray[rowNumber].totalPieces) {
		} else {
			modelCopy=addToLog("Total Pieces",copyArray[rowNumber].totalPieces,modelArray[rowNumber].totalPieces,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].totalVolume === copyArray[rowNumber].totalVolume) {
		} else {
			modelCopy=addToLog("Total Volume",copyArray[rowNumber].totalVolume,modelArray[rowNumber].totalVolume,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].grossWeight === copyArray[rowNumber].grossWeight) {
		} else {
			modelCopy=addToLog("Gross Weight",copyArray[rowNumber].grossWeight,modelArray[rowNumber].grossWeight,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].netWeight === copyArray[rowNumber].netWeight ) {
		} else {
			modelCopy=addToLog("Net Weight ",copyArray[rowNumber].netWeight ,modelArray[rowNumber].netWeight ,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cbmPerCarton === copyArray[rowNumber].cbmPerCarton) {
		} else {
			modelCopy=addToLog("CBM Per Carton",copyArray[rowNumber].cbmPerCarton,modelArray[rowNumber].cbmPerCarton,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].grossWeightPerCarton === copyArray[rowNumber].grossWeightPerCarton) {
		} else {
			modelCopy=addToLog("Gross Weight Per Carton",copyArray[rowNumber].grossWeightPerCarton,modelArray[rowNumber].grossWeightPerCarton,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].netWeightPerCarton === copyArray[rowNumber].netWeightPerCarton) {
		} else {
			modelCopy=addToLog("Net Weight Per Carton",copyArray[rowNumber].netWeightPerCarton,modelArray[rowNumber].netWeightPerCarton,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cartonUnit === copyArray[rowNumber].cartonUnit) {
		} else {
			modelCopy=addToLog("Carton Unit ",copyArray[rowNumber].cartonUnit,modelArray[rowNumber].cartonUnit,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cartonLength === copyArray[rowNumber].cartonLength) {
		} else {
			modelCopy=addToLog("Carton Length",copyArray[rowNumber].cartonLength,modelArray[rowNumber].cartonLength,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cartonWidth === copyArray[rowNumber].cartonWidth) {
		} else {
			modelCopy=addToLog("Carton Width",copyArray[rowNumber].cartonWidth,modelArray[rowNumber].cartonWidth,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cartonHeight === copyArray[rowNumber].cartonHeight) {
		} else {
			modelCopy=addToLog("Carton Height",copyArray[rowNumber].cartonHeight,modelArray[rowNumber].cartonHeight,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].cartonSerNo === copyArray[rowNumber].cartonSerNo) {
		} else {
			modelCopy=addToLog("Carton Serial No",copyArray[rowNumber].cartonSerNo,modelArray[rowNumber].cartonSerNo,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].reference1 === copyArray[rowNumber].reference1) {
		} else {
			modelCopy=addToLog("Reference#1",copyArray[rowNumber].reference1,modelArray[rowNumber].reference1,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].reference2 === copyArray[rowNumber].reference2) {
		} else {
			modelCopy=addToLog("Reference#2",copyArray[rowNumber].reference2,modelArray[rowNumber].reference2,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].reference3 === copyArray[rowNumber].reference3) {
		} else {
			modelCopy=addToLog("Reference#3",copyArray[rowNumber].reference3,modelArray[rowNumber].reference3,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].reference4 === copyArray[rowNumber].reference4) {
		} else {
			modelCopy=addToLog("Reference#4",copyArray[rowNumber].reference4,modelArray[rowNumber].reference4,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].reference4 === copyArray[rowNumber].reference4) {
		} else {
			modelCopy=addToLog("Reference#4",copyArray[rowNumber].reference4,modelArray[rowNumber].reference4,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].qcDate === copyArray[rowNumber].qcDate) {
		} else {
			modelCopy=addToLog("QC Date",copyArray[rowNumber].qcDate,modelArray[rowNumber].qcDate,modelArray[rowNumber].itemNumber),"Line Item";
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].productCode === copyArray[rowNumber].productCode) {
		} else {
			modelCopy=addToLog("Prodcut Code/SKU",copyArray[rowNumber].productCode,modelArray[rowNumber].productCode,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].projectNo === copyArray[rowNumber].projectNo) {
		} else {
			modelCopy=addToLog("Project No",copyArray[rowNumber].projectNo,modelArray[rowNumber].projectNo,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].pcsPerCarton === copyArray[rowNumber].pcsPerCarton) {
		} else {
			modelCopy=addToLog("Pieces Per Carton",copyArray[rowNumber].pcsPerCarton,modelArray[rowNumber].pcsPerCarton,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].commInvoice === copyArray[rowNumber].commInvoice) {
		} else {
			modelCopy=addToLog("Commercial Invoice No",copyArray[rowNumber].commInvoice,modelArray[rowNumber].commInvoice,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].commInvoiceDate === copyArray[rowNumber].commInvoiceDate) {
		} else {
			modelCopy=addToLog("Commercial Invoice Date",copyArray[rowNumber].commInvoiceDate,modelArray[rowNumber].commInvoiceDate,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].releaseDate === copyArray[rowNumber].releaseDate) {
		} else {
			modelCopy=addToLog("Release Date",copyArray[rowNumber].releaseDate,modelArray[rowNumber].releaseDate,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].description === copyArray[rowNumber].description) {
		} else {
			modelCopy=addToLog("Description of Goods",copyArray[rowNumber].description,modelArray[rowNumber].description,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].itemDescription === copyArray[rowNumber].itemDescription) {
		} else {
			modelCopy=addToLog("Item Description",copyArray[rowNumber].itemDescription,modelArray[rowNumber].itemDescription,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}
		if (modelArray[rowNumber].shippingMark === copyArray[rowNumber].shippingMark) {
		} else {
			modelCopy=addToLog("Shipping Mark",copyArray[rowNumber].shippingMark,modelArray[rowNumber].shippingMark,modelArray[rowNumber].itemNumber,"Line Item");
			sendArray.push(modelCopy);
		}

		console.log(sendArray);*/
		document.getElementById("id_totalVolume").disabled = false;
		createTable(modelArray);

		// document.getElementById("updateFormModal").close();
		$('#updateFormModal').modal('hide');
	}
}

/*//nusrat
function addToLog(field,prev,updated,lineItemNo,type){
	return modelcopy = {
			fieldName : field,
			prev : prev,
			updated : updated,
			lineItemNo : lineItemNo,
			dataType :type

		}
}*/
//tahmid
function addNewItem() {	
	
	//validate dates first
	if(validateDateFieldsAddItemModal() === false) {
		return false;
	}
	
	// Item Details
	var poNo = document.getElementById("id_add_poNo").value;
	if (poNo == "") {								
		$("#id_add_poNo").focus();
		alert("PO No. is mandatory field, cannot be left empty");
		return false;
	}
	var grossWeight = document.getElementById("id_add_grossWeight").value; // Total Gross Weight
	if (grossWeight == "") {								
		$("#id_add_grossWeight").focus();
		alert("Gross Weight is mandatory field, cannot be left empty");
		return false;
	}
	var totalVolume = document.getElementById("id_add_totalVolume").value; // Total CBM
	if (totalVolume == "") {								
		$("#id_add_totalVolume").focus();
		alert("Total CBM is mandatory field, cannot be left empty");
		return false;
	}
	var cartonQuantity = document.getElementById("id_add_cartonQuantity").value;
	if (cartonQuantity == "") {								
		$("#id_add_cartonQuantity").focus();
		alert("Carton Quantity is mandatory field, cannot be left empty");
		return false;
	}
	var totalPieces = document.getElementById("id_add_totalPieces").value;
	if (totalPieces == "") {								
		$("#id_add_totalPieces").focus();
		alert("Total Pieces is mandatory field, cannot be left empty");
		return false;
	}
	var hsCode = document.getElementById("id_add_hsCode").value;
	if (hsCode == "") {								
		$("#id_add_hsCode").focus();
		alert("HS Code is mandatory field, cannot be left empty");
		return false;
	}
	var material = document.getElementById("id_add_material").value;// Commodity
	if (material == "") {								
		$("#id_add_material").focus();
		alert("Material is mandatory field, cannot be left empty");
		return false;
	}	
		
	var referNo = document.getElementById("id_add_referNo").value;	
	var productCode = document.getElementById("id_add_productCode").value; // Product Code / SKU
	var cartonLength = document.getElementById("id_add_cartonLength").value;
	var netWeight = document.getElementById("id_add_netWeight").value; // Total Net Weight
	var projectNo = document.getElementById("id_add_projectNo").value; // Project No/ Article No
	var cartonWidth = document.getElementById("id_add_cartonWidth").value; // Carton Width
	var styeleNo = document.getElementById("id_add_styleNo").value;
	var size = document.getElementById("id_add_sizeNo").value;
	var cartonHeight = document.getElementById("id_add_cartonHeight").value;	
	var color = document.getElementById("id_add_color").value;
	var cartonUnit = document.getElementById("id_add_cartonMeasurementUnit").value;	
	
	// Commercial Invoice Details
	var payCond = document.getElementById("id_add_payCond").value; //Pay Conday Cond.
	var unitPrice = document.getElementById("id_add_unitPrice").value;// Unit Price/Pcs
	var currency = document.getElementById("id_add_currency").value; //Currency Unit
	
	
	//Item Extra
	var commInvoice = document.getElementById("id_add_commInvoice").value;
	var commInvDate =  document.getElementById("id_add_commInvoiceDate").value;
	var cartonSerNo = document.getElementById("id_add_cartonSerNo").value; // Carton Serial No
	var gWeightPerCarton = document.getElementById("id_add_grossWeightPerCarton").value;// GWeight/Carton
	var netWeightPerCarton = document.getElementById("id_add_netWeightPerCarton").value;// Net Weight/Carton
	var pcsPerCarton = document.getElementById("id_add_pcsPerCarton").value;// Pieces/Carton
	var netCost = document.getElementById("id_add_netCost").value;// Net Cost
	var reference1 = document.getElementById("id_add_reference1").value;
	var reference2 = document.getElementById("id_add_reference2").value;
	var qcDate = document.getElementById("id_add_qcDate").value;
	var reference3 = document.getElementById("id_add_reference3").value;
	var reference4 = document.getElementById("id_add_reference4").value;
	var releaseDate = document.getElementById("id_add_releaseDate").value;
	var desciption = document.getElementById("id_add_description").value;// Description of Goods
	var itemDescription = document.getElementById("id_add_itemDescription").value;
	var shippingMark = document.getElementById("id_add_shippingMark").value;
	

	if (poNo == "" || cartonQuantity == "" || grossWeight == "" || totalPieces == ""
		|| hsCode == "" || totalVolume == "" ||  material == "") {

		alert("You can not add new item without mandatory fields!");
		return false;

	} else {
		
		var count = 0;
		
		var itemNumber = modelArray[modelArray.length - 1].itemNumber;
		
		for (var c = 0; c < itemNumber.length; c++) {

			if (itemNumber.charAt(c) != '0') {
				count = c;
				break;
			}
		}

		itemNumber = itemNumber.substring(count, itemNumber.length);
		var newItemNumber = itemNumber / 10;
		
		newItemNumber = (newItemNumber + 1) * 10;

		for (var i = 0; i < count; i++) {
			
			newItemNumber = '0' + newItemNumber;
		}
		
		itemNumber = newItemNumber;
		console.log(itemNumber);

		model = {
				itemNumber      : itemNumber,
				poNumber        : poNo,
				styleNo         : styeleNo,
				sizeNo          : size,
				color           : color,
				hsCode          : hsCode,
				material        : material,
				refNo           : referNo,
				projectNo       : projectNo,
				productCode     : productCode,
				color 		    : color,
				totalPieces     : totalPieces,
				curtonQuantity  : cartonQuantity,
				totalVolume 	: totalVolume,
				grossWeight     : grossWeight,
				netWeight 	    : netWeight,
				payCond 	    : payCond,
				unitPrice 	    : unitPrice,
				currency	    : currency,
				commInvoice  : commInvoice,
				commInvoiceDate : commInvDate,
				netCost 		: netCost,
				pcsPerCarton 	: pcsPerCarton,
				grossWeightPerCarton : gWeightPerCarton,
				netWeightPerCarton : netWeightPerCarton,
				cartonLength    : cartonLength,
				cartonWidth     : cartonWidth,
				cartonHeight    : cartonHeight,
				cartonUnit      : cartonUnit,
				cartonSerNo 	: cartonSerNo,
				reference1 		: reference1,
				reference2 		: reference2,
				reference3  	: reference3,
				reference4 		: reference4,
				qcDate 			: qcDate,
				releaseDate 	: releaseDate,
				//description 	: description,
				//itemDescription : itemDescription,
				//shippingMark    : shippingMark
			};
		
		modelArray.push(model);
		copyArray.push(model);
		modelCopy=addToLog("New Line Item Added","","",itemNumber,"Line Item");
		sendArray.push(modelCopy);

		document.getElementById("id_add_totalVolume").disabled = false;
		createTable(modelArray);

		$('#addItemModal').modal('hide');
	}

}

function deleteItem(rowCount) {

	var result = window.confirm("Do you want to Delete the Item "
			+ modelArray[rowCount].itemNumber + "?");
	console.log(result);
	if (result) {

		modelArray[rowCount] = null;
		var tempArray = [];
		var c = 0;
		for (var i = 0; i < modelArray.length; i++) {

			if (modelArray[i] != null) {

				tempArray[c] = modelArray[i];
				c++;
			}
		}
		itemNumber = copyArray[rowCount].itemNumber;
		poNumber = copyArray[rowCount].poNumber;
		weight = copyArray[rowCount].grossWeight;
		quantity = copyArray[rowCount].curtonQuantity;
		cbm = copyArray[rowCount].totalCbm;
		copyArray[rowCount] = null;
		var tempCopyArray = [];
		var f = 0;
		for (var i = 0; i < copyArray.length; i++) {

			if (copyArray[i] != null) {

				tempCopyArray[f] = copyArray[i];
				f++;
			}
		}
		modelArray = tempArray;
		copyArray = tempCopyArray;
		modelCopy=addToLog("Line Item Deleted","PO:"+ poNumber+" GW:"+weight+" Quanity:"+quantity+" CBM:"+cbm,"",itemNumber,"Line Item");
		sendArray.push(modelCopy);
		createTable(modelArray);
		console.log("row count:" + rowCount);
	}

}

function saveLineItem(item) {
	console.log(item);
	var parser = new DOMParser();
	var doc = parser.parseFromString(item, "text/xml");
	var lstItems = directBookingParams.orderItemLst;

	var lineItem = {};
	var existLineItem = lstItems[0];

	$("#collapseTwo").find('input[type="text"]').each(function(index, value) {
		var name = $(value).attr("name");
		// if (isPropertyOfObject(existLineItem, name)) {
		lineItem[name] = $(value).val();
		// }
	})
	$("#collapseTwo").find('textarea').each(function(index, value) {
		var name = $(value).attr("name");
		// if (isPropertyOfObject(existLineItem, name)) {
		lineItem[name] = $(value).val();
		// }
	})
	$("#collapseTwo").find('select').each(function(index, value) {
		var name = $(value).attr("name");
		// if (isPropertyOfObject(existLineItem, name)) {
		lineItem[name] = $(value).val();
		// }
	})

	$.LoadingOverlay("show");
	$
			.when($.ajax({
				url : myContextPath + '/additem',
				type : "POST",
				data : JSON.stringify(lineItem),
				contentType : 'application/json',
				dataType : 'json'
			}))
			.then(
					function(data, textStatus, jqXHR) {
						$.LoadingOverlay("hide");

						if (textStatus == 'success') {
							var bapiReturn = data.bapiReturn3;
							var success = true;
							var message;
							$.each(bapiReturn, function(index, value) {
								if (value.type == 'A' || value.type == 'E') {
									success = false;
									mesaage = message + " " + value.messageV1;
									return;
								}
							});
							if (success) {
								$("#span-message").addClass("message-success");
								$("#span-message").text(
										"---Line Item Added successfully!---");
								$("#collapseTwo").toggle();
								window.location.reload(true);
								// window.location.href.split('#')[0];
							} else {
								$("#span-message").addClass("message-error");
								$("#span-message").text(mesaage);
							}

							/*
							 * $.each($("#headingTwo").find("table").find("a"),function(index,value){
							 * if($(value).text()=='Collapse');
							 * $(value).text("Expand"); })
							 */
						} else {
							$("#span-message").addClass("message-error");
							$("#span-message")
									.text(
											"Error occured while saving ! Please contact Admin !");
						}
					})

}

//function validateHeaderMandatory() {
//	if ($("#collapseOne").find("#lcTtPono").val() == '') {
//		$("#collapseOne").find("#lcTtPono").focus();
//		return false;
//	} else if ($("#collapseOne").find("#shippingMark").val() == '') {
//		$("#collapseOne").find("#shippingMark").focus();
//		return false;
//	} else if ($("#collapseOne").find("#expNo").val() == '') {
//		$("#collapseOne").find("#expNo").focus();
//		return false;
//	} else if ($("#collapseOne").find("#expDate").val() == '') {
//		$("#collapseOne").find("#expDate").focus();
//		return false;
//	} else if ($("#collapseOne").find("#lcTtPoDate").val() == '') {
//		$("#collapseOne").find("#lcTtPoDate").focus();
//		return false;
//	} else if ($("#collapseOne").find("#description").val() == '') {
//		$("#collapseOne").find("#description").focus();
//		return false;
//	}  else if ($("#collapseOne").find("#commInvoice").val() == '') {
//		$("#collapseOne").find("#commInvoice").focus();
//		return false;
//	}	else if ($("#collapseOne").find("#commInvoiceDate").val() == '') {
//		$("#collapseOne").find("#commInvoiceDate").focus();
//		return false;
//	}	else
//		return true;
//
//}
function validateHeaderMandatory() {
	if (document.getElementById("commInvoice").value == "") {								
		$("#commInvoice").focus();
		alert("Commercial Invocie is mandatory field, cannot be left empty");
		return false;
	} else if(document.getElementById("commInvoiceDate").value == "") {
		$("#commInvoiceDate").focus();
		alert("Commercial Invocie Date is mandatory field, cannot be left empty");
		return false;
	} else if(document.getElementById("description").value == "") {
		$("#description").focus();
		alert("Description of Goods is mandatory field, cannot be left empty");
		return false;
	} else if(document.getElementById("shippingMark").value == "") {
		$("#shippingMark").focus();
		alert("Shipping Mark is mandatory field, cannot be left empty");
		return false;
	}else
		return true;
}

function validateLineItemMandatory() {

	if ($("#collapseTwo").find('input[name="poNumber"]').val() == '') {
		$("#collapseTwo").find('input[name="poNumber"]').focus();
		return false;
	} else if ($("#collapseTwo").find('input[name="totalPieces"]').val() == '') {
		$("#collapseTwo").find('input[name="totalPieces"]').focus();
		return false;
	} else if ($("#collapseTwo").find('input[name="grossWeight"]').val() == '') {
		$("#collapseTwo").find('input[name="grossWeight"]').focus();
		return false;
	} else if ($("#collapseTwo").find('input[name="totalVolume"]').val() == '') {
		$("#collapseTwo").find('input[name="totalVolume"]').focus();
		return false;
	} else if ($("#collapseTwo").find('input[name="curtonQuantity"]').val() == '') {
		$("#collapseTwo").find('input[name="curtonQuantity"]').focus();
		return false;
	} else if ($("#collapseTwo").find('input[name="hsCode"]').val() == '') {
		$("#collapseTwo").find('input[name="hsCode"]').focus();
		return false;
	}
	/*
	 * else if($("#collapseTwo").find('input[name="commInvoice"]').val()=='') {
	 * $("#collapseTwo").find('input[name="commInvoice"]').focus(); return
	 * false; } else
	 * if($("#collapseTwo").find('input[name="qcDate"]').val()=='') {
	 * $("#collapseTwo").find('input[name="qcDate"]').focus(); return false; }
	 * else if($("#collapseTwo").find('input[name="releaseDate"]').val()=='') {
	 * $("#collapseTwo").find('input[name="releaseDate"]').focus(); return
	 * false; } else
	 * if($("#collapseTwo").find('input[name="shippingMark"]').val()=='') {
	 * $("#collapseTwo").find('input[name="shippingMark"]').focus(); return
	 * false; } else
	 */

	return true;

}

function validateLineItemDataType() {
	var value;
	value = $("#collapseTwo").find('input[name="commInvoiceDate"]').val();
	if (!validateDateType(value)) {
		$("#collapseTwo").find('input[name="commInvoiceDate"]').focus();
		return false;
	} else if (!validateDateType($("#collapseTwo").find('input[name="qcDate"]')
			.val())) {
		$("#collapseTwo").find('input[name="qcDate"]').focus();
		return false;
	} else if (!validateDateType($("#collapseTwo").find(
			'input[name="releaseDate"]').val())) {
		$("#collapseTwo").find('input[name="releaseDate"]').focus();
		return false;
	} else
		return true;

}
/*function validateHeaderDataType() {
	var value;
	value = $("#collapseOne").find("#expDate").val();
	if (!validateDateType(value)) {
		$("#collapseOne").find("#expDate").focus();
		return false;
	} else if (!validateDateType($("#collapseOne").find("#lcTtPoDate").val())) {
		$("#collapseOne").find("#lcTtPoDate").focus();
		return false;
	}
	
	 * else if(!validateDateType($("#collapseOne").find("#shippingDate").val())) {
	 * $("#collapseOne").find("#shippingDate").focus(); return false; }
	 
	else
		return true;
}
*/
function validateHeaderDataType() {
	var value;
	value = $("#expDate").val();
	if (!validateDateType(value)) {
		$("#expDate").focus();
		return false;
	} else if (!validateDateType($("#lcTtPoDate").val())) {
		$("#lcTtPoDate").focus();
		return false;
	}
	/*
	 * else if(!validateDateType($("#collapseOne").find("#shippingDate").val())) {
	 * $("#collapseOne").find("#shippingDate").focus(); return false; }
	 */
	else
		return true;
}


function validateDateType(dateType) {
	if (dateType.length > 0) {
		var date = dateType.split("-");
		if (date.length >= 3) {
			var d = parseInt(date[0], 10);
			var m = parseInt(date[1], 10);
			var y = parseInt(date[2], 10);
		}
		if (d != undefined && m != undefined && y != undefined)
			return true;
		else
			return false;
	} else
		return true;
}


/*function updateHeader() {
	var orderHeader = {};
	orderHeaderOrg = directBookingParams.orderHeader;
	orderHeader["expNo"] = $("#collapseOne").find("#expNo").val();
	orderHeader["lcTtPono"] = $("#collapseOne").find("#lcTtPono").val();
	orderHeader["shippingDate"] = $("#collapseOne").find("#shippingDate").val();
	orderHeader["description"] = $("#collapseOne").find("#description").val();
	orderHeader["shipperName"] = $("#collapseOne").find("#shipperName").val();
	orderHeader["shipperAddress1"] = $("#collapseOne").find("#shipperAddress1")
			.val();
	orderHeader["shipperAddress2"] = $("#collapseOne").find("#shipperAddress2")
			.val();
	orderHeader["shipperAddress3"] = $("#collapseOne").find("#shipperAddress3")
			.val();
	orderHeader["shipperCity"] = $("#collapseOne").find("#shipperCity").val();
	orderHeader["notifyParty"] = $("#collapseOne").find("#notifyParty").val();
	orderHeader["notifyPartyAddress1"] = $("#collapseOne").find(
			"#notifyPartyAddress1").val();
	orderHeader["notifyPartyCity"] = $("#collapseOne").find("#notifyPartyCity")
			.val();
	orderHeader["remarks1"] = $("#collapseOne").find("#remarks1").val();
	orderHeader["expDate"] = $("#collapseOne").find("#expDate").val();
	orderHeader["lcTtPoDate"] = $("#collapseOne").find("#lcTtPoDate").val();
	orderHeader["shippingMark"] = $("#collapseOne").find("#shippingMark").val();
	orderHeader["buyerName"] = $("#collapseOne").find("#buyerName").val();
	orderHeader["buyerAddress1"] = $("#collapseOne").find("#buyerAddress1")
			.val();
	orderHeader["buyerAddress2"] = $("#collapseOne").find("#buyerAddress2")
			.val();
	orderHeader["buyerAddress3"] = $("#collapseOne").find("#buyerAddress3")
			.val();
	orderHeader["buyerCity"] = $("#collapseOne").find("#buyerCity").val();
	orderHeader["shippersBankName"] = $("#collapseOne").find(
			"#shippersBankName").val();
	orderHeader["shippersBankAddress1"] = $("#collapseOne").find(
			"#shippersBankAddress1").val();
	orderHeader["shippersBankCity"] = $("#collapseOne").find(
			"#shippersBankCity").val();
	orderHeader["remarks2"] = $("#collapseOne").find("#remarks2").val();
	$.each(orderHeader, function(index, value) {
		orderHeaderOrg[index] = value;
	})
	directBookingParams.orderHeader = orderHeaderOrg;
	$.LoadingOverlay("show");
	$.ajax({
		url : myContextPath + '/doBookingUpdate',
		type : "POST",
		data : JSON.stringify(directBookingParams),
		contentType : 'application/json',
		dataType : 'json',
		success : function(data) {
			$.LoadingOverlay("hide");
			$("#span-message").addClass("message-success");
			$("#span-message").text("Header details updated successfully!");
		},
		error : function() {
			$.LoadingOverlay("hide");
			$("#span-message").addClass("message-error");
			$("#span-message").text("Error occured while saving !");
		}

	})
}*/


/*function updateHeader() {
	var orderHeader = {};
	orderHeaderOrg = directBookingParams.orderHeader;
	
	orderHeader["expNo"] = $("#expNo").val();
	orderHeader["lcTtPono"] = $("#lcTtPono").val();
	orderHeader["shippingDate"] = $("#shippingDate").val();
	orderHeader["description"] = $("#description").val();
	orderHeader["notifyParty"] = $("#notifyParty").val();
	orderHeader["notifyPartyAddress1"] = $("#notifyPartyAddress1").val();
	orderHeader["notifyPartyCity"] = $("#notifyPartyCity").val();
	orderHeader["remarks1"] = $("#remarks1").val();
	orderHeader["expDate"] = $("#expDate").val();
	orderHeader["lcTtPoDate"] = $("#lcTtPoDate").val();
	orderHeader["shippingMark"] = $("#shippingMark").val();
	orderHeader["shippersBankName"] = $("#shippersBankName").val();
	orderHeader["shippersBankAddress1"] = $("#shippersBankAddress1").val();
	orderHeader["shippersBankCity"] = $("#shippersBankCity").val();
	orderHeader["remarks2"] = $("#remarks2").val();
	
	
	
	$.each(orderHeader, function(index, value) {
		orderHeaderOrg[index] = value;
	})
	directBookingParams.orderHeader = orderHeaderOrg;

}*/


function updateHeader() {
	var orderHeader = {};
	orderHeaderOrg = directBookingParams.orderHeader;
	copyOrderHeader = directBookingParams.orderHeader;
	
	/*orderHeader["shippingDate"] = $("#shippingDate").val();*/
	orderHeader["expNo"] = $("#expNo").val();
	orderHeader["lcTtPono"] = $("#lcTtPono").val();
	orderHeader["expDate"] = $("#expDate").val();
	orderHeader["lcTtPoDate"] = $("#lcTtPoDate").val();
	orderHeader["description"] = $("#description").val();
	orderHeader["shippingMark"] = $("#shippingMark").val();
	orderHeader["remarks1"] = $("#remarks1").val();
	orderHeader["remarks2"] = $("#remarks2").val();
	
	orderHeader["comInvNo"] = $("#commInvoice").val();
	orderHeader["comInvDate"] = $("#commInvoiceDate").val();
	
	orderHeader["notifyPartyName"] = $("#notifyPartyName").val();
	orderHeader["notifyPartyAddress1"] = $("#notifyPartyAddress1").val();
	orderHeader["notifyPartyAddress2"] = $("#notifyPartyAddress2").val();
	orderHeader["notifyPartyAddress3"] = $("#notifyPartyAddress3").val();
	orderHeader["notifyPartyCity"] = $("#notifyPartyCity").val();
	orderHeader["notifyPartyCountry"] = $("#notifyPartyCountry").val();
	
	orderHeader["shippersBankName"] = $("#shippersBankName").val();
	orderHeader["shippersBankAddress1"] = $("#shippersBankAddress1").val();
	orderHeader["shippersBankAddress2"] = $("#shippersBankAddress2").val();
	orderHeader["shippersBankAddress3"] = $("#shippersBankAddress3").val();
	orderHeader["shippersBankCity"] = $("#shippersBankCity").val();
	orderHeader["shippersBankCountry"] = $("#shippersBankCountry").val();
	
	$.each(orderHeader, function(index, value) {
		if(copyOrderHeader[index]=== null){
			copyOrderHeader[index]= "";
		}
		if (copyOrderHeader[index] != orderHeader[index]) {
			modelCopy = addToLog(index, copyOrderHeader[index],
					orderHeader[index], "", "Header");
			sendArray.push(modelCopy);
		}
		orderHeaderOrg[index] = value;		
	})
	
	directBookingParams.orderHeader = orderHeaderOrg;
	/*
	$.LoadingOverlay("show");
	$.ajax({
		url : myContextPath + '/doBookingUpdate',
		type : "POST",
		data : JSON.stringify(directBookingParams),
		contentType : 'application/json',
		dataType : 'json',
		success : function(data) {
			$.LoadingOverlay("hide");
			$("#span-message").addClass("message-success");
			$("#span-message").text("Header details updated successfully!");
		},
		error : function() {
			$.LoadingOverlay("hide");
			$("#span-message").addClass("message-error");
			$("#span-message").text("Error occured while saving !");
		}

	})*/
}


function updateLineItem() {

	// return itemNumber;
}

function isPropertyOfObject(lineItem, name) {
	var returnValue = false;
	$.each(lineItem, function(key, value) {
		if (key == name) {
			returnValue = true;
			return false;
		}
	})
	return returnValue;
}

function renderItems(itemNumber) {
	var lineItem;
	var lstLineItem = directBookingParams.orderItemLst;
	$.each(lstLineItem, function(index, value) {
		if (parseInt(value.itemNumber) === parseInt(itemNumber)) {
			lineItem = value;
		}
	})
	if (lineItem != undefined) {
		$("#collapseTwo").find('input[type="text"]').each(
				function(index, value) {
					var name = $(value).attr("name");
					$(value).val(getValueByKey(lineItem, name));
				})

		$("#collapseTwo").find('textarea').each(function(index, value) {
			var name = $(value).attr("name");
			$(value).text(getValueByKey(lineItem, name));
		})
		$("#collapseTwo").find('input[name="itemNumber"]').val(
				lineItem.itemNumber);
	}

}

function renderPackingList() {
	var packingList = directBookingParams.poBookingPacklistDetailBean;
	if (packingList == null || packingList == undefined) {
		$("#errorMsg-pkl").text(
				"Packing List is empty! Please add packing List");

	} else

	{

		var packingListColorList = packingList.packlistColors;
		var tbody = $("#tbl-packing-list").find("tbody");
		$(tbody).empty();
		var sizeList = packingListColorList[0].packlistSizes;
		var tr = $("#tbl-packing-list").find("thead").find("tr");
		var th = $(tr[0]).find("th");
		$(th[3]).attr("colspan", sizeList.length);
		$.each(sizeList, function(index, value) {
			var col = '';
			col = col + '<th>' + value.sizeName + '</th>';
			$(tr[1]).append(col);

		})
		$
				.each(
						packingListColorList,
						function(rowIndex, value) {
							var tr = '<tr>';
							if (rowIndex == 0) {
								tr = tr
										+ '<td rowspan="'
										+ packingListColorList.length
										+ '"><input type="text" style=" height:30px;" id="ctnSrlNo" value="'
										+ packingList.ctnSrlNo + '"></td>';
								tr = tr
										+ '<td rowspan="'
										+ packingListColorList.length
										+ '"><input type="text" style=" height:30px;" id="noOfContainers" value="'
										+ packingList.noOfContainers
										+ '"></td>';
							}
							tr = tr
									+ '<td><input type="text" style=" height:30px;" id="color_'
									+ (rowIndex + 1) + '" value="'
									+ value.colorName + '"></td>';
							$
									.each(
											value.packlistSizes,
											function(columnIndex, value) {
												tr = tr
														+ '<td><input type="text" style=" height:30px;" id="size_'
														+ (columnIndex + 1)
														+ '_' + (rowIndex + 1)
														+ '" value="'
														+ value.noOfPcs
														+ '"></td>';
												// tr=tr+'<td>'+value.noOfPcs+'</td>';

											});
							tr = tr
									+ '<td><input type="text" style=" height:30px;" id="pcs_per_car_'
									+ (rowIndex + 1) + '" value="'
									+ value.pcsPerCtn + '"></td>';
							tr = tr
									+ '<td><input type="text" style=" height:30px;" id="total_pcs_'
									+ (rowIndex + 1) + '" value="'
									+ value.totalPcs + '"></td>';
							tr = tr
									+ '<td><input type="text" style=" height:30px;" id="remarks_'
									+ (rowIndex + 1) + '" value="'
									+ value.remarks + '"></td>';

							tr = tr + '</tr>';
							$("#tbl-packing-list").find("tbody").append(tr);
						})

	}
}

function updatePackingList() {
	var poBookingPacklistDetailBean = {};
	var packlistColors = [];
	var packlistColorsObject = {};
	var packingListSize = [];
	var packingListSizeobject = {};
	var sizes = $("#tbl-packing-list").find("thead").find("tr");
	sizes = $(sizes[1]).find("th");
	$.each($("#tbl-packing-list").find("tbody").find("tr"), function(rowIndex,
			value) {
		var tableColumns = $(value).find("td");
		if (rowIndex == 0) {
			poBookingPacklistDetailBean["ctnSrlNo"] = $(value)
					.find("#ctnSrlNo").val();
			poBookingPacklistDetailBean["noOfContainers"] = $(value).find(
					"#noOfContainers").val();
		}
		packlistColorsObject = {};
		packlistColorsObject["colorName"] = $(tableColumns).find(
				"#color_" + (rowIndex + 1)).val();
		packlistColorsObject["pcsPerCtn"] = $(tableColumns).find(
				"#pcs_per_car_" + (rowIndex + 1)).val();
		packlistColorsObject["totalPcs"] = $(tableColumns).find(
				"#total_pcs_" + (rowIndex + 1)).val();
		packlistColorsObject["remarks"] = $(tableColumns).find(
				"#remarks_" + (rowIndex + 1)).val();
		packingListSize = [];
		$.each(sizes, function(columnIndex, value) {
			packingListSizeobject = {};
			packingListSizeobject["sizeName"] = $(value).text();
			packingListSizeobject["noOfPcs"] = $(tableColumns).find(
					"#size_" + (columnIndex + 1) + "_" + (rowIndex + 1)).val();
			packingListSize.push(packingListSizeobject);
		})
		packlistColorsObject["packlistSizes"] = packingListSize;
		packlistColors.push(packlistColorsObject);
	})
	poBookingPacklistDetailBean["packlistColors"] = packlistColors;
	directBookingParams.poBookingPacklistDetailBean = poBookingPacklistDetailBean;
	$.LoadingOverlay("show");
	$
			.when($.ajax({
				url : myContextPath + '/doBookingUpdate',
				type : "POST",
				data : JSON.stringify(directBookingParams),
				contentType : 'application/json',
				dataType : 'json'
			}))
			.then(
					function(data, textStatus, jqXHR) {
						$.LoadingOverlay("hide");

						if (textStatus == 'success') {
							$("#span-message").addClass("message-success");
							$("#span-message").text(
									"Packing List updated successfully!");
							$("#collapseThree").toggle();
							if ($('a[href="#Three"]').text() == 'Collapse')
								$('a[href="#Three"]').text('Expand');
						} else {
							$.LoadingOverlay("hide");
							$("#span-message").addClass("message-error");
							$("#span-message")
									.text(
											"Error occured while saving ! Please contact Admin !");
						}
					})

}

function getValueByKey(object, keyName) {
	var returnValue;
	$.each(object, function(key, value) {
		if (key == keyName) {
			returnValue = value;
			return false;
		}
	})
	return returnValue;
}

function handleUpload(files) {
	var fileData = new FormData();
	fileData.append("multipartFile", files);
	$.ajax({
		url : myContextPath + '/download/uploadExcel',
		data : fileData,
		dataType : 'json',
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		type : 'POST',
		success : function() {

		}

	})
}

$(document).on('click', '.browse', function() {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on(
		'change',
		'.file',
		function() {

			$(this).parent().find('.form-control').val(
					$(this).val().replace(/C:\\fakepath\\/i, ''));
		});

/*$(document).on(
		'click',
		'#btn-submit',
		function() {
			var directBookingParams = directBookingParams;
			window.location.href = myContextPath
					+ '/doBookingUpdate?directBookingParams='
					+ directBookingParams;
			$.LoadingOverlay("show");
			$.ajax({
				url : myContextPath + '/doBookingUpdate',
				type : "POST",
				data : JSON.stringify(directBookingParams),
				contentType : 'application/json',
				dataType : 'json',
				success : function(data) {
					window.location = myContextPath + '/bookingupdateresponse';
					$.LoadingOverlay("hide");
				}
			})
		})*/

function methodCallBookingUpdate() {
	/*
	 * var directBookingParams = directBookingParams;
	 * window.location.href = myContextPath +
	 * '/doBookingUpdate?directBookingParams=' + directBookingParams;
	 */
	updateHeader();
	directBookingParams.loggerUpdate = sendArray;

	if (grShow == 'show' && modelArray.length == 0) {

		alert('You cannot add without line item');
		return false;
	} else {

		/*
		 * if((modelArray != null && modelArray.length!=0) && grShow
		 * =='hide' ){
		 */
		if ((modelArray != null && modelArray.length != 0)
				&& grShow == 'show') {
			directBookingParams.orderItemLst = modelArray;			
		}

		$.LoadingOverlay("show");
		$.ajax({
			url : myContextPath + '/doBookingUpdate',
			type : "POST",
			data : JSON.stringify(directBookingParams),
			contentType : 'application/json',
			dataType : 'json',
			success : function(data) {
				window.location = myContextPath
						+ '/bookingupdateresponse';
				$.LoadingOverlay("hide");
			}
		})
	}
}

/*$(document).on('click', '#btn-finish-update', function() {
			updateHeader();
			if (grShow == 'show' && modelArray.length == 0) {
				alert('You cannot add without line item');
				return false;
			} else {
				if ((modelArray != null && modelArray.length != 0) && grShow == 'show') {
					directBookingParams.orderItemLst = modelArray;
				}
				$.LoadingOverlay("show");
				$.ajax({
					url : myContextPath + '/doBookingUpdate',
					type : "POST",
					data : JSON.stringify(directBookingParams),
					contentType : 'application/json',
					dataType : 'json',
					success : function(data) {
						window.location = myContextPath + '/bookingupdateresponse';
						$.LoadingOverlay("hide");
					}
				})
			}

		})*/

$(document).on('click', '#cancelUpdate', function(){
    location.reload(true);
	});