<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-update.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-updatedHelperMethods.js"></script>


<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>

<style>
html, body {
	background-color: #fff;
}

tbody td {
	font-size: 14px;
}
</style>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/poTracking.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>


<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">PO Detail</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">PO Detail</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getPoTrackingReportDetail" method="post"
			commandName="poTrackingParams" id="poTrack">
			
				<%-- 			<div class="col-md-2 col-sm-12 col-xs-12">
						<div class='form-group'>
							<label>PO Number</label>
							<form:input path="doc_no" id="poNo" cssClass="form-control" />
						</div>
					</div> --%>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class='form-group'>
						<label>From Date</label>
						<form:input path="fromDate" id="fromDate"
							class="date-picker form-control glyphicon glyphicon-calendar" required="true" />
					</div>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class='form-group'>
						<label>TO Date</label>
						<form:input path="toDate" id="toDate"
							class="date-picker form-control glyphicon glyphicon-calendar" required="true" />
					</div>
				</div>
				<%-- 			<div class="col-md-2 col-sm-12 col-xs-12">
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="statusType" id="sType" cssClass="form-control">
								<form:option value="NONE">--- Select ---</form:option>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div>
					<div class="col-md-2 col-sm-12 col-xs-12" style="margin-top: 2%;">
						<div class='form-group'>
							<button type="submit" class="btn btn-success btn-lg form-control">Search</button>
						</div>
					</div>				--%>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class='form-group'>
						<label style="display: block; color: #f5f5f5">. </label>
						<!-- added for alignment purpose, dont delete -->
						<input type="submit" id="btn-appr-po-search"
							class="btn btn-success form-control" value="Search" />
					</div>
				</div>
			
		</form:form>
	</div>
	<hr>
	<div class="row row-without-margin">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h3 style="margin-top: 0px; margin-bottom: 20px;">Search Result</h3>
		</div>
	</div>
	<div class="row row-without-margin">
		<div class="container-fluid table-responsive">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover display"
						id="po_tables">
						<thead style="background-color: #222534; color: #fff;">
							<tr>

								<!-- hamid -->
								<th style="display: none;">SALES ORDER NO</th>
								<!-- 0 -->
								<th>HBL NO</th>
								<th style="display: none;">BOOKING DATE</th>
								<th style="display: none;">SHIPPER</th>
								<th>BUYER</th>
								<th style="display: none;">BUYING HOUSE</th>
								<th style="display: none;">COMM INV</th>
								<th style="display: none;">LC NO</th>
								<th style="display: none;">FREIGHT MODE</th>
								<th>ORDER NO</th>
								<th>COMMODITY</th>
								<!-- 10 -->
								<th style="display: none;">STYLE NO</th>
								<th style="display: none;">REF. NO</th>
								<th style="display: none;">PROJECT/ARTICLE NO</th>
								<th style="display: none;">SKU NO</th>
								<th style="display: none;">ORDER SIZE</th>
								<th style="display: none;">COLOR</th>
								<th style="display: none;">INDENT NO</th>
								<th style="display: none;">CRT QTY</th>
								<th style="display: none;">GROSS WT</th>
								<th style="display: none;">NET WT</th>
								<!-- 20 -->
								<th style="display: none;">CBM</th>
								<th style="display: none;">PIECES</th>
								<th style="display: none;">LENGTH</th>
								<th style="display: none;">WIDTH</th>
								<th style="display: none;">HEIGHT</th>
								<th style="display: none;">CARGO DESCRIPTION</th>
								<th style="display: none;">CUSTOM DOC RCV DATE</th>
								<th style="display: none;">DOC DATE</th>
								<th style="display: none;">EXP. CARGO H/OVER DATE</th>
								<th style="display: none;">PLACE OF RECEIPT</th>
								<!-- 30 -->
								<th style="display: none;">CARGO H/OVER DATE</th>
								<th style="display: none;">CARGO RCV TIME</th>
								<th style="display: none;">SHIP. DATE</th>
								<!--  <th>RL-DATE</th>-->
								<th style="display: none;">MBL NO</th>
								<th style="display: none;">CARRIER NAME</th>
								<th style="display: none;">PORT OF LOADING</th>
								<th style="display: none;">FVSL NAME</th>
								<th style="display: none;">FVSL VOY NO</th>
								<th>FVSL ETD</th>
								<th>TRANS. PORT</th>
								<!-- 40 -->
								<th style="display: none;">FVSL ETA</th>
								<th style="display: none;">MVSL ETD</th>
								<th style="display: none;">DESTINATION PORT</th>
								<th>MVSL ETA</th>
								<th>MVSL NAME</th>
								<th style="display: none;">MVSL VOY NO</th>
								<th style="display: none;">CONTAINER</th>
								<th style="display: none;">CONTAINER SIZE</th>
								<th style="display: none;">CONTAINER SEAL NO</th>
								<th style="display: none;">CONTAINER MODE</th>
								<!-- 50 -->
								<th style="display: none;">FCR DATE</th>
								<th style="display: none;">HBL RELEASE DATE</th>
								<th style="display: none;">STATUS</th>
								<th style="display: none;">GSP</th>

							</tr>
						</thead>
						<tbody>

							<c:forEach items="${poTrackingResultBeanLst}" var="i">
								<tr>
									<!-- hamid -->
									<td style="display: none;">${i.document_no}</td>
									<!-- 0 -->
									<td>${i.bl_no}</td>
									<td style="display: none;">${i.bl_date}</td>
									<td style="display: none;">${i.shipper_name}</td>
									<td>${i.buyer_name}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.comm_invoice_no}</td>
									<td style="display: none;">${i.lc_tt_po_no}</td>
									<td style="display: none;">${i.freight_mode}</td>
									<td style="display: none;">${i.po_no}</td>
									<td>${i.material_desc}</td>
									<!-- 10 -->
									<td style="display: none;">${i.style_no}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.article_no}</td>
									<td style="display: none;">${i.sku_no}</td>
									<td style="display: none;">${i.size1}</td>
									<td style="display: none;">${i.color}</td>
									<td style="display: none;">${i.indent_no}</td>
									<td style="display: none;">${i.tot_qty}</td>
									<td style="display: none;">${i.gross_wt}</td>
									<td style="display: none;">${i.net_wt}</td>
									<!-- 20 -->
									<td style="display: none;">${i.tot_volume}</td>
									<td style="display: none;">${i.tot_pcs}</td>
									<td style="display: none;">${i.length}</td>
									<td style="display: none;">${i.width}</td>
									<td style="display: none;">${i.height}</td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.chdt}</td>
									<td style="display: none;"></td>
									<!-- 30 -->
									<td style="display: none;">${i.gr_date}</td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.carrier_name}</td>
									<td style="display: none;">${i.pol_name}</td>
									<td style="display: none;">${i.fvslName}</td>
									<td></td>
									<td><fmt:formatDate pattern="dd-MMM-yy" type="date"
											value="${i.fvsletd}" /></td>
									<td>${i.tship_port_code}</td>
									<!-- 40 -->
									<td style="display: none;"><fmt:formatDate
											pattern="dd-MMM-yy" type="date" value="${i.fvsleta}" /></td>
									<td style="display: none;"><fmt:formatDate
											pattern="dd-MMM-yy" type="date" value="${i.mvsletd}" /></td>
									<td style="display: none;">${i.pod_name}</td>
									<!-- 								<td></td> -->
									<td><fmt:formatDate pattern="dd-MMM-yy" type="date"
											value="${i.mvsleta}" /></td>
									<td>${i.mvslname}</td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<!-- 50 -->
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>


								</tr>
							</c:forEach>


						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<br>

<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>

<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		$('.date-picker').each(function() {
			$(this).removeClass('hasDatepicker').datepicker({
				dateFormat : "dd.mm.yy"
			});
		});
	});

	/*   $("nav nav-tabs nav-justified nav-book-update").click(function(){
	 $(this).tab('show');
	 */

	$('#po_tables').DataTable({

		responsive : true,
		dom : 'Bfrtip',
		buttons : [ 'csv', 'excel' ]
	/* ,
	"columnDefs" : [ {
		"targets" : [ 0 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 2 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 3 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 5 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 6 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 7 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 8 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 11 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 12 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 13 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 14 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 15 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 16 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 17 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 18 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 19 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 20 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 21 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 22 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 23 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 24 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 25 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 26 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 27 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 28 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 29 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 30 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 31 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 32 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 33 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 34 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 35 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 36 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 37 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 38 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 41 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 42 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 43 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 46 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 47 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 48 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 49 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 50 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 51 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 52 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 53 ],
		"visible" : false,
		"searchable" : false
	}, {
		"targets" : [ 54 ],
		"visible" : false,
		"searchable" : false
	} ] */
	});
</script>
