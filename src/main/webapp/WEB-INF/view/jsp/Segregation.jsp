<%@include file="header.jsp" %>       
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>	
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/segregation.js"></script>
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
	text-align: center;
}
</style>
		
        <div class="col-xs-12" style="height:30px;"></div>
        <div class="container">
            
  <ul class="nav nav-justified">    
    <li><a style="text-transform:uppercase;background-color:#fff;">SEGREGATION</a></li>
  </ul>

  <div class="tab-content">
    <div id="#menu1" class="tab-pane fade in active">
    <form:form  action="doSegregation" method="post" commandName="segregationDTO">
      <section>
		<div class="container-fluid">
			<div class="row line_item_text">
			<div class="error">
			<c:if test="${not empty error}">
  				 Error: ${error}
			</c:if>
			</div>
				<h3>Reference Data</h3>
			</div>
                <div class='row input_area' >
                    
                   <div class='col-sm-4'>    
                        <div class='form-group'>
                        <label>PO Number</label>
                        <form:input path="vc_po_no" cssClass="form-control" id="user_title" readonly="true"/>                        
                        </div>
                   </div>
                   <div class='col-sm-4'>
                       <div class='form-group'>
                       <label>Product Number</label>
                       <form:input path="vc_product_no" cssClass="form-control" readonly="true"/>                       
                   </div>
                  </div>
                  <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Style No</label>
                   <form:input path="vc_style_no" cssClass="form-control" readonly="true"/>                   
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Sku-No</label>
                   <form:input path="vc_sku_no" cssClass="form-control" readonly="true"/>                  
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Color</label>
                   <form:input path="vc_color" cssClass="form-control" readonly="true"/>                  
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Size</label>
                   <form:input path="vc_size" cssClass="form-control" readonly="true"/>                  
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Total Pieces</label>
                   <form:input path="vc_tot_pcs" cssClass="form-control" readonly="true"/>                  
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>HS Code</label>
                   <form:input path="vc_hs_code" cssClass="form-control" readonly="true"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>No Pcs Ctns</label>  
                    <form:input path="nu_no_pcs_ctns" cssClass="form-control"  readonly="true"/>   
                    <form:hidden path="po_id"/>  
                    <form:hidden path="seg_id"/>     
                    <form:hidden path="paramPoNo"/>
                    <form:hidden path="paramFromDate"/>
                    <form:hidden path="paramToDate"/>                          
                 </div>
                 </div>
             </div>
			</div>
</section>
       
        <section class="input_area">
          <div class="container-fluid">
			<div class="row line_item_text">
				<h3>Line Item Data</h3>
			</div>
                <div style="overflow-x:auto;">
                    
                  <table id="customTable">
	
        <tr>
	        <th>Product No</th>
			<th>SKU No</th>
			<th>Style No</th>
	        <th>Color</th>
	        <th>Size</th>
	        <th>POL</th>
	        <th>POD</th>
	        <th>Total Pieces</th>
	        <th>HS Code</th>
	        <th>Ref Field</th>
	        <th>Article No</th>
        </tr>
	
	<tr id="showtest">
		<td><input type="text" name="dynamicData[0].vc_product_no" id="id" maxlength="10"></td>
		<td><input type="text" name="dynamicData[0].vc_sku_no" maxlength="10"></td>
		<td><input type="text" name="dynamicData[0].vc_style_no" maxlength="15"></td>
        <td><input type="text" name="dynamicData[0].vc_color" maxlength="10"></td>
		<td><input type="text" name="dynamicData[0].vc_size" maxlength="6"></td>
        <td><input type="text" name="dynamicData[0].vc_pol" maxlength="10"></td>
        <td><input type="text" name="dynamicData[0].vc_pod" maxlength="10"></td>
        <td><input type="text" name="dynamicData[0].vc_tot_pcs" value="0" maxlength="8"></td>
        <td><input type="text" name="dynamicData[0].vc_hs_code" maxlength="10"></td>
        <td><input type="text" name="dynamicData[0].vc_ref_field1" maxlength="20"></td>
        <td><input type="text" name="dynamicData[0].vc_article_no" maxlength="10"></td>
        <td>
         <input type="button"  name="addCFName" id="addDynamicrows" value="Add More" style="width: 80px;height: 22px;padding-left: 2px;background-color:#ff9900; color:#000;font-weight: bold;">
        </td>
	  </tr>            
	

    </table>	  
				  
            </div>
			</div>
			<div class="row input_area">
				<button id="line_item_button" type="submit" name="singlebutton" class="btn btn-success btn-lg center-block">Submit</button>
			</div>     
</section>
</form:form>
      </div>
    </div>
        </div>
		
	<!-- ----------------------------- -->		
		<%@include file="footer.jsp" %>
