<section class="footer no-print">
<%-- 			<div class="container-fluid">
				<div class="row" >
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" class=""/>
						
						<p class="footer-para">Copyright &copy Myshipment.com</p>
					</div>
					 <div class="col-lg-3 col-md-3 footer-img">
						<img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="" class=""/>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 footer_nav">
						
					</div>
				</div>
			</div> --%>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<p class="footer-para">Copyright &copy Myshipment.com</p>
			</div>			
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 footer_nav">
			</div>
		</div>
	</div>
</section>
<%-- 		<link href='${pageContext.request.contextPath}/resources/css/footer/styles.css' type='text/css' rel="stylesheet">
		
        <section id="footer">
            <div class="container wow animated fadeInUp" data-wow-delay="0.4s" data-wow-duration="1.5s">
                <div class="row footer-top">
                    <div class="col-sm-4 footer-follow">
                        <h5>Follow Us :</h5>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="https://www.facebook.com/mghgroupglobal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.linkedin.com/company/mghgroup" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 footer-copy">
                        <p class="">
                            &copy; myshipment.com
                            <script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
                        </p>
                    </div>
                    <div class="col-sm-5 text-xs-center footer-app">
                        <a href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" target="_blank">
                        	<!-- <img src="img/Android-App-Store-AF.png" class="img-fluid" title="Get On App Store"> -->
                        	<img src="${pageContext.request.contextPath}/resources/images/Android-App-Store-AF.png" class="img-fluid" title="Get On App Store">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.mgh.shipment" target="_blank">
<!--                         	<img src="img/Android-App-Store-GF.png" class="img-fluid" title="Get On Google Play"> -->
                        	<img src="${pageContext.request.contextPath}/resources/images/Android-App-Store-GF.png" class="img-fluid" title="Get On Google Play">
                        </a>
                    </div>
                </div>
            </div>
        </section> --%>


<script
	src="${pageContext.request.contextPath }/resources/js/bootbox.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/main.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/app.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/myshipment.js"></script>
<script>
		$(document).ready( function() {
		
		 $("#to_date").datepicker({ dateFormat: "dd-mm-yy",changeYear: true}).datepicker("setDate", new Date());
		 var currentDate=new Date();
		 currentDate.setMonth(currentDate.getMonth()-2);
		 $("#from_date").datepicker({ dateFormat: "dd-mm-yy",changeYear: true}).datepicker("setDate", currentDate);
		$("#btn-assgn-carr").attr("disabled","disabled"); 
	
})

		</script>
</body>
</html>
