<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html, body {
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Cargo Acknowledgement Certificate</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Cargo Acknowledgement Certificate</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">

	</div>

</div>
<c:if test="${empty cargoAckJson.itHeaderList }">
	<div
		style="height: 50px; width: 200px; color: red; font-weight: bold; margin-left: 43%; padding-top: 10px;">No
		Record found!</div>
	<div class="row row-without-margin">
		No Record found!
	</div>
</c:if>
<c:if test="${ not empty cargoAckJson.itHeaderList }">
	<section>
		<div class="container">
		<form:form action="cargoAck" method="post" commandName="req"
						cssClass="form-incline" target="_blank">
						<div class='row'>
							<div class='col-xs-2'>
								<div class='form-group' style="display: none">
									<form:input path="req1" id="number" cssClass="form-control"
										required="true" />
								</div>
							</div>
							<div class='col-xs-8'></div>
							<div class='col-xs-2'>
								<div class='form-group'>
									<label style="display: block; color: #f5f5f5">. </label>
									<!-- added for alignment purpose, dont delete -->
									<input type="submit" name="download" id="download"
										class="btn btn-primary form-control" value="Download" />
								</div>
							</div>
						</div>

					</form:form>
		</div>
	</section>
	<div class="container-fluid" style="margin-left: 2%;">

		<section class="main_body_wraper1" id="print-con"
			style="margin-left: 10px;">
			<div class="row nopadding">
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-3 nopadding shipper_info">
							<h6>FCR No.</h6>
							<h6>Shipper</h6>
							<h6>Com Inv No.</h6>
							<h6>Total QTY.</h6>
							<h6>FCR Date</h6>
						</div>
						<div class="col-xs-6 nopadding">
							<h6>: ${cargoAckJson.itHeaderList[0].zzHblHawbNo }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].name1 }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].zzCommInvNo }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].zzSoQuantity }</h6>
							<h6>
								:
								<fmt:formatDate pattern="yyyy-MM-dd" type="date"
									value="${cargoAckJson.itHeaderList[0].buDat }" />
							</h6>
						</div>

					</div>
				</div>
				<div class="col-xs-6"></div>
			</div>
			<div class="col-xs-12 row nopadding">
				<h4 style="padding: 1% 0%;">This is to certify that the
					following orders have been received from the above shipper.</h4>
			</div>

			<div class="col-xs-12 row table_wraper">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Order No</th>
								<th>Carton Quantity</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${cargoAckJson.itItemList}" var="item">
								<tr>
									<td>${item.poSnr}</td>
									<td>${item.zzPoNumber}</td>
									<td>${item.menge}</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>

			</div>
			<div class="col-xs-6 row nopadding">
				<h6>${cargoAckJson.itAddList[0].name1}</h6>
				<h6>${cargoAckJson.itAddList[0].name2}</h6>
				<h6>${cargoAckJson.itAddList[0].name3}</h6>
				<h6>${cargoAckJson.itAddList[0].name4}</h6>
			</div>

		</section>
















	</div>

</c:if>






<!-- jquery
		============================================ -->
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS
		============================================ -->
<script src="js/bootstrap.min.js"></script>
<!-- plugins JS
		============================================ -->
<script src="js/plugins.js"></script>
<!-- main JS
		============================================ -->

<script src="js/canvasjsmin.js"></script>
<script src="js/main.js"></script>

<script>
	function printContent(el) {
		/* var restorepage = $('body').html();
		var printcontent = $('#' + el).clone();
		$('body').empty().html(printcontent);
		window.print();
		$('body').html(restorepage); */

		var printContents = document.getElementById(el).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
</script>
<%@include file="footer_v2_fixed.jsp"%>
