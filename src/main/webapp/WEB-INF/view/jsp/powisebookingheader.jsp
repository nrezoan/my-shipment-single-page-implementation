<%@page import="com.google.gson.Gson"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>
<script>
	localStorage.setItem("directBooking", new Date());
	var commInvDate = '${itemList[0].comm_inv_date}';
	var cargoHandOverDate = '${itemList[0].cargo_handover_date}';
	
	/* var choosen = ${itemList[0].buyer_id}; */
/* 	var modeChoosen = ${itemList[0].freight_mode};
	var termChoosen = ${itemList[0].terms_of_shipment}; */
	//var processStartdata = localStorage.getItem('directBooking');
	//alert(processStartdata);
</script>

<style>
	html, body {
		background-color: #fff;
		margin-bottom: 20px;
	}
	
	form .orderBooking-row-height {
		padding-bottom: 20px;
	}
	
	#orderBookingHeader {
		font-size: 25px;
    	font-weight: 500;
	}
	
	.select2 {
		width: 100% !important;
		
	}
	
	.form-group.required .text-center:after {
	    content: "*";
	    color: red;
	}

	.order-booking-tab-panel {
		background-color: #3c8dbc !important;
		color: #fff !important;
		font-size: 16px !important;
		border: 1px #eaeaea solid;
	}
	
	.order-booking-tab-main-row {
		margin-left: 1%;
		margin-right: 1%;
	}
	
	.order-booking-tab-panel a {
		color: #fff !important;
	}
	
	.active a {
		color: #252525 !important;
	}
	
	.order-booking-tab-header {
		padding-top: 5px;
		padding-left: 1%;
		font-size: 20px;
		font-weight: 500;
		color: #fff;
	}
	
	select {
		height: 33px;
		width: 100% !important;
		padding-left: 10px;
	}
	
	#frm-direct-booking input {
		width: 100% !important;
		border: 1px solid rgb(169, 169, 169) !important;
	}
	
	.predictable-select {
		width: 237px !important;
	}
	
	.input-group-addon {
		border: unset !important;
	}
	
	.booking-information-header {
		font-size: 16px;
		padding-left: 5px;
		font-weight: bold;
	}
	
	.order-booking-select-lc {
		height: 25px !important;
    	width: 15% !important;
	}
</style>
 <!-- <script
	src="${pageContext.request.contextPath}/resources/js/directBookingHeader.js"></script> -->
	<script
	src="${pageContext.request.contextPath}/resources/js/poWiseBookingHeader.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/jquery.autocorrect.js"
	type="text/javascript"></script>
<script type="text/javascript">
	// Bind events when document is ready
	//console.log(loginDetails);
	
	$(document).ready(function() {
		// Attach auto-correct plugin with default options
		$("#disccriptonOfGoods").autocorrect();
		// Attach auto-correct plugin with new option alongwith option to over-write old one
		/*$("#textbox2").autocorrect({
		    corrections: {
		        arent: "aren't",
		        aboutit: "about it"
		    }
		});*/
		// Attach auto-correct plugin to textarea
		// $("#textarea").autocorrect({ corrections: { aboutit: "about it" } });
	});
</script>
<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">



<section>
	<div class="row order-booking-tab-main-row">
		<div class="col-md-12">
			<div class="nav-tabs-custom-949974">
				<section class="" style="display: block;">
					
					<span id="orderBookingHeader"></span>
					
					<hr>
					
				</section>
				<br>
				<form:form action="${pageContext.request.contextPath}/savePoWiseBooking"
					modelAttribute="orderHeader" method="post" id="frm-direct-booking">
					<div class="">
						<div class="row">
							<div class="col-md-12">
								<span class="booking-information-header">Partner Information</span>
								<hr>
							</div>
							<div class="col-md-12 orderBooking-row-height">
								
								<div class="col-md-4">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Consignee/Buyer</label>
										</div>
										<div class="col-md-12">
											<form:select style="" class="select2 predictable-select"
												data-live-search="true" required="true" path="buyer"
												id="buyers" tabindex="1"
												onchange="getTemplateDataAndfillInComponent(this.value);">
												<option value="">Please Select</option>
												<%--<c:forEach
													items="${c }"
													var="buyers">
													<option value="${buyers.key }">${buyers.value }</option>
												</c:forEach>--%>
												<form:option value="" label="Select Buyer"></form:option>
												<form:options items="${supplierDetails.buyersSuppliersmap.buyers}"></form:options>
											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Local Buying House</label>
										</div>
										<div class="col-md-12">
											<form:select class=" drop" id="lbuyingHouse"
												path="localBuyingHouse"  tabindex="4">
												<option selected value="">Please Select</option>
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.localBuyingHouse }"
													var="localBuyingHouse">
													<option value="${localBuyingHouse.key }">${localBuyingHouse.value }</option>
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Notify Party</label>
										</div>
										<div class="col-md-12">
											<form:select class="" path="notifyParty" id="notifyParty"
												tabindex="7" >
												<option selected value="">Please Select</option>
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.notifyParty }"
													var="notifyParty">
													<option value="${notifyParty.key }">${notifyParty.value }</option>
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="col-md-12 orderBooking-row-height">								
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Shipper's Bank</label>
										</div>
										<div class="col-md-12">
											<form:select class=" drop" id="shippersBank"
												path="shippersBank"  tabindex="6">
												<option selected value="">Please Select</option>
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.shippersBank }"
													var="shippersBank">
													<option value="${shippersBank.key }">${shippersBank.value }</option>
												</c:forEach>
											</form:select>
										</div>
									</div>
									
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Buyer's Bank</label>
										</div>
										<div class="col-md-12">
											<form:select class="" path="buyersBank" id="buyersBank"
												tabindex="5" >
												<option value="" selected>Please Select</option>
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.buyersBank }"
													var="buyersBank">
													<option value="${buyersBank.key }">${buyersBank.value }</option>
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
								<span class="booking-information-header">B/L Information</span>
								<hr>
							</div>
							
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Order Type</label>
										</div>
										<div class="col-md-12">
											<form:select class="drop color-change "
												data-live-search="true" id="orderType" required="true"
												path="docType" tabindex="2" >
	
												<!--  <option selected value="">Please Select</option>-->
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.docTypes }"
													var="docTypes">
													<c:choose>
														<c:when
															test="${docTypes.key=='ZSCM' || docTypes.key=='ZMSE' || docTypes.key=='ZBKT' || docTypes.key=='ZSCA' || docTypes.key=='ZMAE' || docTypes.key=='ZBKA'}">
															<option value="${docTypes.key }" selected>${docTypes.value }</option>
														</c:when>
														<c:otherwise>
															<option value="${docTypes.key }">${docTypes.value }</option>
														</c:otherwise>
													</c:choose>
	
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">HBL/AWB Initials</label>
										</div>
										<div class="col-md-12">
											<form:select class=" color-change" path="hblInit"
												required="true" id="hblInitial" tabindex="9"
												>
												<option value="">Please Select</option>
												<c:forEach
													items="${supplierDetails.buyersSuppliersmap.hblInit }"
													var="hblInit">
													<option value="${hblInit.key }">${hblInit.value }</option>
												</c:forEach>
											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Terms of Shipment</label>
										</div>
										<div class="col-md-12">
											<form:select path="tos" class=" drop color-change "
												required="required" id="termOfShipment" tabindex="2"
												>
												<form:option value="" label="Select Terms of Shipment"></form:option>
												<form:options items="${supplierDetails.buyersSuppliersmap.tos}"></form:options>
											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Freight Mode</label>
										</div>
										<div class="col-md-12">
											<form:select class="drop color-change" required="true"
												id="freightMode"  path="freightMode"
												tabindex="10">
												<form:option value="" label="Select Freight Mode"></form:option>
												<form:options items="${supplierDetails.buyersSuppliersmap.freightMode}"></form:options>
											</form:select>
										</div>
									</div>
								</div>

							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Commercial Invoice Number</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="comInvNo" tabindex="11"
											class="form-control color-change" required="true"
											 />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Commercial Invoice Date</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="comInvDate" tabindex="12"
											 id="lcomInvDate" onkeydown="return false"
											class="form-control color-change date-picker" required="true" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Exp. Number</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="expNumber" 
												class="form-control" path="expNo" tabindex="13" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Exp. Date</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="expDate" onkeydown="return false" 
												id="expdt" class="form-control date-picker" tabindex="14" />
										</div>
									</div>
								</div>
								
							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<form:select class="order-booking-select-lc" path="transType">
												<option value="sc">SC</option>
												<option selected value="LC">LC</option>
												<option value="TT">TT</option>
												<option value="PO">PO</option>
												<option value="CO">CO</option>
												<option value="DC">DC</option>
											</form:select>
											<label class="text-center">Number</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="lcTtPono" class="form-control"
												tabindex="16"  />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">LC Expiry Date</label>
										</div>
										<div class="col-md-12">
											<form:input id="lcexpdt" type="text" path="lcExpiryDate"
												tabindex="18" onkeydown="return false" 
												class="form-control date-picker" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">LC/TT/PO/Date</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="lcTtPoDate"
												 id="lcttpodt" onkeydown="return false"
												class="form-control date-picker" tabindex="17" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Freight Payable at</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="tosDes" 
												class="form-control color-change" required="true"
												tabindex="19" />
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-6">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Description of Goods</label>
										</div>
										<div class="col-md-12">
											<form:textarea path="description" tabindex="34"
												style="" id="disccriptonOfGoods"
												class="textarea color-change" required="true" cols="40"
												rows="7" onkeypress="checkCharacterLimit()" placeholder="Description of Goods must be completed within 440 characters/ 5 lines (max.)"></form:textarea>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Shipping Marks</label>
										</div>
										<div class="col-md-12">
											<form:textarea path="shippingMark" tabindex="35"
												style="" id="shippingMark" rows="7" onkeypress="checkCharacterLimitShippingMark()"
												class="textarea color-change" required="true"
												placeholder="Shipping marks must be completed within 200 characters (max.)"></form:textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<span class="booking-information-header">Route Information</span>
								<hr>
							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Place of Receipt</label>
										</div>
										<div class="col-md-12">
											<form:select style="" class="select2 predictable-select"
												data-live-search="true" required="true" path="portLink"
												id="placeOfReceipt">
												<option value="">Please Select</option>
												<c:forEach var="lstStorageLocation"
													items="${lstStorageLocation }">
													<option value="${lstStorageLocation.storageId }">${lstStorageLocation.storageDescription}</option>
												</c:forEach>


											</form:select>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Port of Loading</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="portLoad" tabindex="27"
												 id="portOfLoading"
												class="form-control auto-complete color-change"
												required="true" onkeyup="fillData(this)"
												onKeyDown="return DisableCopyPaste(event)"
												onMouseDown="return DisableCopyPaste (event)" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Port of Discharge</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="portOfDischarge"
												path="portOfDischarge" tabindex="31" 
												class="form-control auto-complete color-change"
												required="true" onkeyup="fillData(this)" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Place of Delivery</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" path="placeOfDelivery" tabindex="32"
												 id="plcOfDel"
												class="form-control auto-complete color-change"
												required="true" onkeyup="fillData(this)" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Place of Delivery Address</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="placeOfDel"
												path="placeOfDeliveryText" 
												class="form-control auto-complete color-change"
												required="true" tabindex="33" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group required'>
										<div class="col-md-12">
											<label class="text-center">Cargo Handover Date</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="cargoHandoverDate"
												path="cargoHandoverDate" tabindex="28" onkeydown="return false"
												class="form-control date-picker color-change"
												required="true" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<span class="booking-information-header">Other Information</span>
								<hr>
							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Vendor Reference</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="venderRef" path="vendorRef"
												 class="form-control" tabindex="20" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">ACC Number</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="accNumber" path="accNumber"
												 class="form-control" tabindex="22" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Swift Transfer</label>
										</div>
										<div class="col-md-12">
											<form:input id="swiftTransfer" type="text"
												path="swiftTransfer" 
												class="form-control" tabindex="24" />
										</div>
									</div>
								</div>
								
							</div>
							<div class="col-md-12 orderBooking-row-height">
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Bank Code</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="bankCode" path="bankCode"
												 class="form-control" tabindex="21" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Intermediate Bank</label>
										</div>
										<div class="col-md-12">
											<form:input type="text" id="intermediateBank"
												path="intermediateBank" 
												class="form-control" tabindex="23" />
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Remarks1</label>
										</div>
										<div class="col-md-12">
											<form:textarea id="remarks1" path="remarks1"
												style="" class="textarea" tabindex="36"
												maxlength="500" autocapitalize="sentences" autocorrect="on"></form:textarea>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class='form-group'>
										<div class="col-md-12">
											<label class="text-center">Remarks2</label>
										</div>
										<div class="col-md-12">
											<form:textarea id="remarks2" path="remarks2"
												style="" class="textarea" tabindex="37"
												maxlength=""></form:textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row"
						style="float: right; margin-right: 2%; margin-top: 2%;">
						<button type="button" id="submitBtn" class="btn btn-primary">Submit Booking</button>
						<button type="button" class="btn btn-danger btn_addmore">Cancel</button>
					</div>
				</form:form>
</section>
<section>
	<div class="container-fluid">
		

		<div id="myModal" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<h2 style="text-align: center;">Template</h2>
				</div>
				<div class="modal-body">
					<div style="text-align: center;">
						<label style="font-weight: bold;">Template Name: </label> <input
							type="text" id="tempName"
							style="height: 30px; width: 70%; padding-left: 10px; border: 1px solid gray; border-radius: 5px; margin-top: 30px;">
					</div>

					<div class="row"
						style="float: right; margin-top: 2%; margin-right: 2%;">
						<input type="submit" id="tempSave" class="btn btn-primary submit"
							value="Submit"
							style="background-color: #337ab7; height: 30px; margin-right: 5px;">
						<input type="submit" class="btn btn-danger close" value="Cancel"
							style="background-color: #d9534f !important; height: 30px;">
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<script>
	$(function() {
		/* $('#buyers option[value= ' + choosen + ']').attr('selected', true); */
	/* 	$('#freightMode option[value= ' + modeChoosen + ']').attr('selected', true);
		$('#termOfShipment option[value= ' + termChoosen + ']').attr('selected', true); */
	});
</script>
<script>
$.LoadingOverlay("show");
	if(loginDetails.divisionSelected == "SE" && loginDetails.disChnlSelected == "EX") {
		$("#orderBookingHeader").text("SEA EXPORT ORDER BOOKING");
	} else if(loginDetails.divisionSelected == "AR" && loginDetails.disChnlSelected == "EX"){
		$("#orderBookingHeader").text("AIR EXPORT ORDER BOOKING");
	}
	$(function () {
	    //Initialize Select2 Elements
	    $('.select2').select2();
	});
	/* var modal = document.getElementById('myModal');
	 //Get the button that opens the modal
	 var btn = document.getElementById("myBtnSv");
	 //Get the <span> element that closes the modal
	 var span = document.getElementsByClassName("close")[0];
	 //When the user clicks the button, open the modal
	 btn.onclick = function() {
	 modal.style.display = "block";
	 }
	 //When the user clicks on <span> close the modal
	 span.onclick = function() {
	 modal.style.display = "none";
	 }
	 //When the user clicks anywhere outside of the modal, close it
	 window.onclick = function(event) {
	 if (event.target == modal) {
	 modal.style.display = "none";
	 }
	 } */
	
	$(document).ready(function() {
		$(".date-picker").datepicker({
			dateFormat : "dd-mm-yy"
		}).datepicker("setDate", "");
		$("#lcexpdt").datepicker("option", "minDate", new Date());
		if(commInvDate != null && commInvDate != "") {
			$("#lcomInvDate").datepicker("setDate", new Date(commInvDate));
		}
		if(cargoHandOverDate != null && cargoHandOverDate != "") {
            $("#cargoHandoverDate").datepicker("setDate", new Date(cargoHandOverDate));
        }
		
		
		$("#submitBtn").click(function() {
			handleBookingTemplate();
		});
		/*  $("#buyers").chosen();
		 $("#orderType").chosen(); */
		$.LoadingOverlay("hide");
		
		var lines = 5;
		var linesUsed = $('#linesUsed'); 
	    $('#disccriptonOfGoods').keydown(function(e) {
	        
	        newLines = $(this).val().split("\n").length;
	        linesUsed.text(newLines);
	        
	        if(e.keyCode == 13 && newLines >= lines) {
	            linesUsed.css('color', 'red');
	            return false;
	        }
	        else {
	            linesUsed.css('color', '');
	        }
	    });
	});

	function orderFieldValidation() {
		var consigneeBuyer = $("#buyers").val();
		var termOfShipment = $('#termOfShipment').val();
		var orderType = $('#orderType').val();
		var freightMode = $('#freightMode').val();
		var hblInitial = $('#hblInitial').val();
		var comInvNo = $('#comInvNo').val();
		var lcomInvDate = $('#lcomInvDate').val();
		var tosDes = $('#tosDes').val();
		var cargoHandoverDate = $('#cargoHandoverDate').val();
		var placeOfReceipt = $("#placeOfReceipt").val();
		var portOfLoading = $('#portOfLoading').val();
		var portOfDischarge = $('#portOfDischarge').val();
		var plcOfDel = $('#plcOfDel').val();
		var placeOfDel = $('#placeOfDel').val();
		var disccriptonOfGoods = $('#disccriptonOfGoods').val();
		var shippingMark = $('#shippingMark').val();

		if (consigneeBuyer == "") {
			swal("Consignee/Buyer", "Please select a Consignee/Buyer", "error");
			return false;
		} else if (termOfShipment == "") {
			swal("Terms of Shipment", "Please select Terms of Shipment",
					"error");
			return false;
		} else if (orderType == "") {
			swal("Order Type", "Please select an Order Type", "error");
			return false;
		} else if (freightMode == "") {
			swal("Freight Mode", "Please select a Freight Mode", "error");
			return false;
		} else if (hblInitial == "") {
			swal("HBL/AWB Initials", "Please select HBL/AWB Initials", "error");
			return false;
		} else if (comInvNo == "") {
			swal("Commercial Invoice Number",
					"Please enter Commercial Invoice Number", "error");
			return false;
		} else if (lcomInvDate == "") {
			swal("Commercial Invoice Date",
					"Please select Commercial Invoice Date", "error");
			return false;
		} else if (tosDes == "") {
			swal("Freight Payable at", "Please enter a Freight Payable",
					"error");
			return false;
		} else if (cargoHandoverDate == "") {
			$("#nav-tabs-custom").tabs({
				active : 1
			});
			swal("Cargo Handover Date", "Please select Cargo Handover Date",
					"error");
			return false;
		} else if (placeOfReceipt == "") {

			swal("Port Of Receipt", "Please select Port Of Receipt", "error");
			return false;
		} else if (portOfLoading == "") {
			swal("Port Of Loading", "Please mention Port Of Loading", "error");
			return false;
		} else if (portOfDischarge == "") {
			swal("Port Of Discharge", "Please mention Port Of Discharge",
					"error");
			return false;
		} else if (plcOfDel == "") {
			swal("Place of Delivery", "Please mention Place of Delivery",
					"error");
			return false;
		} else if (placeOfDel == "") {
			swal("Place of Delivery Address",
					"Please mention Place of Delivery Address", "error");
			return false;
		} else if (disccriptonOfGoods == "") {
			swal("Description of Goods", "Please add Description of Goods",
					"error");
			return false;
		} else if (shippingMark == "") {
			swal("Shipping Mark", "Please add Shipping Mark", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<%@include file="footer_v2.jsp"%>