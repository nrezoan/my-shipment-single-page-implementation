
<%@include file="header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	
		</script>

<section class="main-body">
	<div class="well assign-po-search">
		<form:form action="shipmentdetaildatabyhbl" method="post" commandName="myshipmentTrackParams">
			<fieldset>
				<legend>HBL Tracking</legend>
				<div class='row'>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>HBL No.</label>
							<form:input path="doc_no" id="doc_no" cssClass="form-control" required="true"/>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" id="btn-appr-po-search"
								class="btn btn-success btn-lg form-control" value="Submit" />
						</div>
					</div>
				</div>
				<c:if test="${errorDialogue == 'FAILURE'}">
					<div class='row'>
						<div class='col-sm-4'>							
							<div class='form-group'>
								<label style="color: red;margin-top: 2px;">Please provide a valid HBL No.</label>								
							</div>				
						</div>
					</div>
				</c:if>

			</fieldset>
		</form:form>
	</div>
</section>

<!-- ----------------------------- -->
<%@include file="footer.jsp"%>