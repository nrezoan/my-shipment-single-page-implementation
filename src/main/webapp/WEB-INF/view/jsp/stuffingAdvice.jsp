<%@ include file="header_v2.jsp"%>
<%@ include file="navbar.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>
html, body {
	background-color: #fff;
}
</style>
<div class="container-fluid container-fluid-441222" >
	<div class="row row-without-margin">
		<h1 class="h1-report">Stuffing Advice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Stuffing Advice</li>
		</ol>
	</div>
	<c:if test="${headeritems ==null || fn:length(headeritems) lt 1 }">
		<div class="row row-without-margin" style="height: 50px; width: 200px; font-weight: bold; color: red; margin-left: 43%; padding-top: 10px;">
			No	Record found!
		</div>
	</c:if>
	<c:if test="${headeritems !=null &&  fn:length(headeritems) gt 0 }">
		<div class="row row-without-margin">
			<div class="col-md-2 col-sm-12 col-xs-12">
				<!-- <button type="button" class="btn btn-success btn-block" id="" onclick="printContent('stuffing_report')">Print</button> -->
				<form:form action="stuffingAdviceReport" method="post" commandName="req" target="_blank">
					<div class='form-group' style="display: none">
							<form:input path="req1" id="number" cssClass="form-control" />
					</div>					
					<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" name="download" id="download" class="btn btn-success btn-block" value="Download" />
					</div>					
				</form:form>
			</div>	
		</div>
		<br>
		<div id="stuffing_report">
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<c:forEach items="${itAddress}" var="itAddress">
						<span style="font-weight: bold; font-size: 16px;">${itAddress.name1}</span><br>
                     	<span>${itAddress.name2} </span> <br>
                     	<span>${itAddress.name3} </span> <br>
                     	<span>${itAddress.name4} </span>
                     	<c:set var="now" value="<%=new java.util.Date()%>" />
						<%-- <span style="font-size: 15px; font-weight: bold; float: right;">Date:<fmt:formatDate
								type="date" value="${now}" /></span> --%> 
                 </c:forEach>
			</div>
		</div>
		<br>
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tbody>
							<c:forEach items="${headeritems}" var="headeritems">
								<tr>
									<td>Shipper Name</td>
									<td>${headeritems.name1}</td>
									<td>Buyer Name</td>
									<td>${headeritems.name2}</td>
									<td>HBL No.</td>
									<td>${headeritems.zzHblHawbNo}</td>
								</tr>
								<tr>
									<td>L/C No</td>
									<td>${headeritems.zzLcpottNo}</td>
									<td>L/C.Date</td>
									<td><fmt:formatDate type="date"
											value="${headeritems.zzLcDt}" /></td>
									<%--                      <td>${headeritems.zzLcDt}</td> --%>
									<td>TOS</td>
									<td>${headeritems.zzFreightModeDes}</td>


								</tr>

								<tr>
									<td>C/Inv No.</td>
									<td>${headeritems.zzComInvNo}</td>
									<td>C/Inv.Date</td>
									<td><fmt:formatDate type="date"
											value="${headeritems.zzCommInvDt}" /></td>
									<%--                      <td>${headeritems.zzCommInvDt}</td> --%>
									<td>Booking Date</td>
									<td><fmt:formatDate type="date"
											value="${headeritems.auDat}" /></td>
									<%--                      <td>${headeritems.auDat}</td> --%>


								</tr>

								<tr>
									<td>POL</td>
									<td>${headeritems.zzPortOfLoading}</td>
									<td>POD</td>
									<td>${headeritems.zzPortOfDest}</td>
									<td>Place Of Delivery</td>
									<td>${headeritems.zzPlacedelivery}</td>

								</tr>

								<tr>
									<td>Proj. F/Vsl.</td>
									<td>${headeritems.zzAirlineName1}
										${headeritems.zzAirLineNo1}</td>
									<td>Proj. M/Vsl.</td>
									<%--                      <td>${headeritems.zzAirLineNo1}</td> --%>
									<td>${headeritems.zzAirlineNam21}
										${headeritems.zzAirLineNo2}</td>
									<td>ETD-${headeritems.zzPortOfLoading}</td>
									<!-- <td> <fmt:formatDate type="date"  value="${headeritems.fvEtDport}"/></td> -->
									<td><fmt:formatDate type="date"
											value="${headeritems.zzDepartureDt}" /></td>
								</tr>

								<tr>
									<td>FETD</td>
									<!-- <td><fmt:formatDate type="date"  value="${headeritems.fvEtDport}"/></td> -->
									<td><fmt:formatDate type="date"
											value="${headeritems.zzDepartureDt}" /></td>
									<td>META</td>
									<!--  <td><fmt:formatDate type="date"  value="${headeritems.fvEtAport}"/></td> -->
									<td><fmt:formatDate type="date"
											value="${headeritems.zzEta}" /></td>
									<td>ETA -
										${headeritems.zzPortOfDest}</td>
									<!-- <td><fmt:formatDate type="date"  value="${headeritems.fvEtAport}"/></td>  -->
									<td><fmt:formatDate type="date"
											value="${headeritems.zzEta}" /></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<td>Order No</td>
								<td>Style No</td>
								<!-- <td>Color</td> -->
								<td>Cont. No</td>
								<td>Size</td>
								<td>Seal</td>
								<td>Stuffing Dt</td>
								<td>Mode</td>
								<td>Total Pcs</td>
								<td>CBM</td>
								<td>Carton Qty</td>
								<td>Gross Wt(Kgs)</td>
							</tr>
						</thead>

						<tbody>
							<c:forEach items="${detailItem}" var="detailItems">
								<tr>
									<td>${detailItems.zzPoNumber}</td>
									<td>${detailItems.zzStyleNo}</td>
									<%-- <td>${detailItems.zzColour}</td> --%>
									<td>${detailItems.vhIlm}</td>
									<td>${detailItems.wgbEZ60}</td>
									<td>${detailItems.vhIlmKu}</td>
									<td><fmt:formatDate type="date"
											value="${detailItems.zzStuffingDate}" /></td>
									<td>${detailItems.zzMode}</td>
									<td>${detailItems.zzTotalNoPcs}</td>
									<td>${detailItems.zzVolume}</td>
									<td>${detailItems.zzQuantity}</td>
									<td>${detailItems.zzGrWt}</td>
								</tr>
							</c:forEach>
							<tr>
								<td colspan="7">Total:</td>
								<td>${stuffingReportTotalData.totPcs}</td>
								<td>${stuffingReportTotalData.totVol}</td>
								<td>${stuffingReportTotalData.totQty}</td>
								<td>${stuffingReportTotalData.totKgs}</td>
							</tr>
						</tbody>
					</table>
				</div>	
			</div>
		</div>
		<br>
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center">
					<span><strong>NOTE: ABOVE SCHEDULE IS SUBJECT TO
							CHANGE. WE WILL CONFIRM YOU THE M/VSL IN DUE COURSE. FOR FURTHER
							INFORMATION PLEASE DO NOT HESITATE TO CONTACT US.</strong> </span>
				</div>
		</div>
		<br>
		</div>
	</c:if>	
</div>


<script>
	function printContent(el) {
		var printContents = document.getElementById(el).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>
<%@include file="footer_v2_fixed.jsp"%>
<!-- new design ends -->

<%-- <div class="entire-content-c-white">
	<div class="content">
		<div class="row">
			<section class="content-header" style="display: display;">
				<h1>Stuffing Advice</h1>
				<ol class="breadcrumb" style="position: unset; float: none;">
					<li><i class="fa fa-dashboard"></i>Report Management</li>
					<li class="active">Stuffing Advice</li>
				</ol>
			</section>
			<hr>
			<br>
		</div>



		<c:if test="${headeritems ==null || fn:length(headeritems) lt 1 }">
			<div
				style="height: 50px; width: 200px; font-weight: bold; color: red; margin-left: 43%; padding-top: 10px;">No
				Record found!</div>
		</c:if>
		<c:if test="${headeritems !=null &&  fn:length(headeritems) gt 0 }">

			<section>
				<div class="container">
					<div style="float: right;">
						<button type="button" class="btn btn-default no-print" id="print" onclick="printContent('stuffing_report')">Print</button>
						<!--   <button type="button" class="btn btn-primary no-print" id="pdf">PDF</button> -->
					</div>
				</div>
			</section>
			<div class="col-xs-12" style="height: 20px;"></div>
			<section>
				<div class="container">
					<div class="row">
						<div class="col-xs-12"></div>
					</div>
				</div>
			</section>

			<div class="row">
				<section id="stuffing_report">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<table class="table commercial-table container-table1"
									style="margin-top: 1px;">
									<thead>
										<tr>
											<td
												style="font-size: 16px; text-align: left; border: 1px solid #337ab7;">Stuffing
												Advice</td>
											                   <td style="font-size:15px;text-align:right;padding-top:15px;width:809px;border: 1px solid #337ab7;">Date:
                   <c:set var="now" value="<%=new java.util.Date()%>" />
                   <fmt:formatDate type="date"  value="${now}"/></td>
										</tr>
									</thead>

									<tbody>

										<tr>
											<c:forEach items="${itAddress}" var="itAddress">
												<span style="font-weight: bold; font-size: 16px;">${itAddress.name1}</span>
												</br>
                     	${itAddress.name2} </br>
                     	${itAddress.name3} </br>
                     	${itAddress.name4} 
                     	</c:forEach>
											<c:set var="now" value="<%=new java.util.Date()%>" />
											<span
												style="font-size: 15px; font-weight: bold; float: right;">Date:<fmt:formatDate
													type="date" value="${now}" /></span>
										</tr>
									</tbody>
								</table>

								<table class="table commercial-table container-table2"
									style="margin-top: -20px;">
									<tbody>
										<c:forEach items="${headeritems}" var="headeritems">
											<tr>
												<td>Shipper Name</td>
												<td>${headeritems.name1}</td>
												<td>Buyer Name</td>
												<td>${headeritems.name2}</td>
												<td>HBL No.</td>
												<td>${headeritems.zzHblHawbNo}</td>


											</tr>

											<tr>
												<td>L/C No</td>
												<td>${headeritems.zzLcpottNo}</td>
												<td>L/C.Date</td>
												<td><fmt:formatDate type="date"
														value="${headeritems.zzLcDt}" /></td>
												                     <td>${headeritems.zzLcDt}</td>
												<td>TOS</td>
												<td>${headeritems.zzFreightModeDes}</td>


											</tr>

											<tr>
												<td>C/Inv No.</td>
												<td>${headeritems.zzComInvNo}</td>
												<td>C/Inv.Date</td>
												<td><fmt:formatDate type="date"
														value="${headeritems.zzCommInvDt}" /></td>
												                     <td>${headeritems.zzCommInvDt}</td>
												<td>Booking Date</td>
												<td><fmt:formatDate type="date"
														value="${headeritems.auDat}" /></td>
												                     <td>${headeritems.auDat}</td>


											</tr>

											<tr>
												<td>POL</td>
												<td>${headeritems.zzPortOfLoading}</td>
												<td>POD</td>
												<td>${headeritems.zzPortOfDest}</td>
												<td>Place Of
													Delivery</td>
												<td>${headeritems.zzPlacedelivery}</td>

											</tr>

											<tr>
												<td>Proj. F/Vsl.</td>
												<td>${headeritems.zzAirlineName1}
													${headeritems.zzAirLineNo1}</td>
												<td>Proj. M/Vsl.</td>
												                     <td>${headeritems.zzAirLineNo1}</td>
												<td>${headeritems.zzAirlineNam21}
													${headeritems.zzAirLineNo2}</td>
												<td>ETD-${headeritems.zzPortOfLoading}</td>
												<!-- <td> <fmt:formatDate type="date"  value="${headeritems.fvEtDport}"/></td> -->
												<td><fmt:formatDate type="date"
														value="${headeritems.zzDepartureDt}" /></td>
											</tr>

											<tr>
												<!--                      <td>Proj.M/vsl</td> -->
												                     <td>${headeritems.zzAirlineNam21} ${headeritems.zzAirLineNo2}</td>
												<!--                      <td>Voyage</td> -->
												                     <td>${headeritems.zzAirLineNo2}</td>
												<td>FETD</td>
												<!-- <td><fmt:formatDate type="date"  value="${headeritems.fvEtDport}"/></td> -->
												<td><fmt:formatDate type="date"
														value="${headeritems.zzDepartureDt}" /></td>
												<td>META</td>
												<!--  <td><fmt:formatDate type="date"  value="${headeritems.fvEtAport}"/></td> -->
												<td><fmt:formatDate type="date"
														value="${headeritems.zzEta}" /></td>
												<td>ETA -
													${headeritems.zzPortOfDest}</td>
												<!-- <td><fmt:formatDate type="date"  value="${headeritems.fvEtAport}"/></td>  -->
												<td><fmt:formatDate type="date"
														value="${headeritems.zzEta}" /></td>

											</tr>
											<!--                    <tr> -->
											<!--                      <td>FETD</td> -->
											<!--                      <td><fmt:formatDate type="date"  value="${headeritems.fvEtDport}"/></td>  -->
											                     <td><fmt:formatDate type="date"  value="${headeritems.zzDepartureDt}"/></td>
											<!--                      <td>META</td> -->
											<!--                       <td><fmt:formatDate type="date"  value="${headeritems.fvEtAport}"/></td> -->
											                     <td><fmt:formatDate type="date"  value="${headeritems.zzEta}"/></td>
											<!--                      <td>Date of Del</td> -->
											                     <td><fmt:formatDate type="date"  value="${headeritems.zzPodDt}"/></td>

											<!--                    </tr> -->

										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</section>

				<section>
					<div class="container">

						<table class="table commercial-table container-table2">
							<thead>
								<tr>
									<td>Order No.</td>
									<td>Style No.</td>
									<td>Color</td>
									<td>Container No.</td>
									<td>Size</td>
									<td>Seal</td>
									<td>Stuffing Date</td>
									<td>Mode</td>
									<td>No. of Pieces</td>
									<td>CBM</td>
									<td>Carton Quantity</td>
									<td>Gross Wt.(Kgs)</td>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${detailItem}" var="detailItems">
									<tr>
										<td>${detailItems.zzPoNumber}</td>
										<td>${detailItems.zzStyleNo}</td>
										<td>${detailItems.zzColour}</td>
										<td>${detailItems.vhIlm}</td>
										<td>${detailItems.wgbEZ60}</td>
										<td>${detailItems.vhIlmKu}</td>
										<td><fmt:formatDate type="date"
												value="${detailItems.zzStuffingDate}" /></td>
										<td>${detailItems.zzMode}</td>
										<td>${detailItems.zzTotalNoPcs}</td>
										<td>${detailItems.zzVolume}</td>
										<td>${detailItems.zzQuantity}</td>
										<td>${detailItems.zzGrWt}</td>
									</tr>
								</c:forEach>
								<tr>
									<td colspan="8">Total:</td>
									<td>${stuffingReportTotalData.totPcs}</td>
									<td>${stuffingReportTotalData.totVol}</td>
									<td>${stuffingReportTotalData.totQty}</td>
									<td>${stuffingReportTotalData.totKgs}</td>
								</tr>
							</tbody>
						</table>
					</div>

				</section>

				<section>
					<div class="container">

						<table class="table commercial-table container-table2">
							<thead>
								<tr>
									<!--                    <td style="border-right: 1px solid #337ab7;">NOTE: THE ABOVE SCHEDULE IS SUBJECT TO CHANGE. WE WILL CONFIRM YOU THE M/VSL IN DUE COURSE FOR FURTHERINFORMATION PLS DO NOT HESITATE TO CONTACT US</td> -->
									<td>NOTE: THE ABOVE
										SCHEDULE IS SUBJECT TO CHANGE. WE WILL CONFIRM YOU THE M/VSL
										IN DUE COURSE FOR FURTHERINFORMATION PLS DO NOT HESITATE TO
										CONTACT US</td>
								</tr>
							</thead>
						</table>
					</div>
				</section>
			</div>
		</c:if>
	</div>
</div>

 --%>