<!-- FOOTER -->

<div id="footer">
			<div class="new-footer">
				<p>
					&copy; <script type="text/javascript">
	                                var today = new Date()
	                                var year = today.getFullYear()
	                                document.write(year)
	
	                            </script> my<span class="shipment-footer-panel">shipment</span> | All
					Rights Reserved
				</p>
			</div>
		</div>

<section id="footer" style="background-color: #252525; display:none;">
	<div class="container wow animated fadeInUp" data-wow-delay="0.4s"
		data-wow-duration="1.5s">
		<div class="row footer-top">
			<div class="col-sm-4 footer-follow">
				<h5>Follow Us :</h5>
				<ul class="list-inline">
					<li class="list-inline-item"><a
						href="https://www.facebook.com/mghgroupglobal" target="_blank"><i
							class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li class="list-inline-item"><a
						href="https://www.linkedin.com/company/mghgroup" target="_blank"><i
							class="fa fa-linkedin" aria-hidden="true"></i></a></li>
				</ul>
			</div>
			<div class="col-sm-3 footer-copy">
				<p class="">
					&copy; myshipment.com
					<script type="text/javascript">
						var today = new Date()
						var year = today.getFullYear()
						document.write(year)
					</script>
				</p>
			</div>
			<div class="col-sm-5 text-xs-center footer-app">
				<a
					href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8"
					target="_blank"><img
					src="${pageContext.request.contextPath}/resources/img/Android-App-Store-AF.png"
					class="img-fluid" title="Get On App Store"></a> <a
					href="https://play.google.com/store/apps/details?id=com.mgh.shipment"
					target="_blank"><img
					src="${pageContext.request.contextPath}/resources/img/Android-App-Store-GF.png"
					class="img-fluid" title="Get On Google Play"></a>
			</div>
		</div>
	</div>
</section>

<!-- END FOOTER -->

<%--  <!-- jQuery -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery.js"></script>
 --%>
<!-- bootstrap JS -->
<script
	src="${pageContext.request.contextPath}/resources/js/myshipment_v2/bootstrap.min.js"></script>

<!-- jquery ui js -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>

<!-- WOW JS -->
<script
	src="${pageContext.request.contextPath}/resources/js/myshipment_v2/wow.min.js"></script>

<!-- select2 -->
<script src="${pageContext.request.contextPath}/resources/dist/js/select2/select2.js"></script>

<!-- custom JS -->
<script
	src="${pageContext.request.contextPath}/resources/js/myshipment_v2/custom-login.js"></script>

<%-- <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery-1.8.0.min.js"></script> --%>

<!-- easing -->
<script
	src="${pageContext.request.contextPath}/resources/js/myshipment_v2/easing/jquery.easing.1.3.js"></script>

<!--fontawesome script-->
<script src="https://use.fontawesome.com/fcba9d8315.js"></script>


<!-- jQuery 2.2.3 -->
<!-- <script src="plugins/jQuery/jquery-2.2.3.min.js"></script> -->

<!-- FastClick -->
<script
	src="${pageContext.request.contextPath}/resources/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script
	src="${pageContext.request.contextPath}/resources/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script
	src="${pageContext.request.contextPath}/resources/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script
	src="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script
	src="${pageContext.request.contextPath}/resources/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script
	src="${pageContext.request.contextPath}/resources/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script
	src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/myshipment.js"></script>	
<script>	
	$(document).ready(function() {
			$.LoadingOverlay("hide");
		});
</script>
<!-- </div> -->
</body>

</html>
