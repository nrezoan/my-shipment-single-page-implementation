<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html, body {
	background-color: #fff;
}
}
</style>
<script type="text/javascript">
</script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">HBL/HAWB Tracking</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-map-marker"></i>&nbsp;Tracking</li>
			<li class="active">HBL wise Tracking</li>
		</ol>
	</div>
	<hr>
	<form:form action="getTrackingDetailInfo" method="post"
		commandName="trackingRequestParam">			
			<div class='row row-without-margin'>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class='form-group'>
						<label>HBL/HAWB No.</label>
						<form:input path="blNo" id="blNo" cssClass="form-control" />
					</div>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">					
					<label style="display: block; color: #f5f5f5">. </label>
					<!-- added for alignment purpose, dont delete -->
					<input type="submit" id="btn-appr-po-search" class="btn btn-success form-control" value="Submit" onclick="return reportSearch();"/>					
				</div>
			</div>		
	</form:form>
</div>
	<script>
	function reportSearch() {
		var number = $("#blNo").val();
		if(number == "") {
			swal ( "HBL Number" ,  "HBL number cannot be empty!" ,  "error" );
			return false;
		} else {
			return true;
		}
	}
</script>
<%@include file="footer_v2_fixed.jsp"%>

<%-- <section class="main-body">
	<div class="well assign-po-search">
		<form:form action="getTrackingDetailInfo" method="post"	commandName="trackingRequestParam">
			<fieldset>
				<legend></legend>
				<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>HBL/HAWB No.</label>
							<form:input path="blNo" id="blNo" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" id="btn-appr-po-search"
								class="btn btn-success form-control" value="Submit" />
						</div>
					</div>
				</div>

			</fieldset>
		</form:form>
	</div>
</section>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<!-- ----------------------------- --> --%>