<%@page import="java.util.Date"%>
<%@page import="com.myshipment.util.DateUtil"%>
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- DataTables -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<style>
html {
	background-color: #eaeaea;
}

.ship_desti {
	margin-left: 1%;
}

th {
	text-align: center;
}

.main-body {
	background-color: #eaeaea;
	margin-bottom: 5%;
}

.commercial h3 {
	background-color: #222534;
	padding-top: 7px;
	font-weight: 400;
	height: 40px;
	margin-top: 10px;
	color: #fff;
}
</style>

<section class="main-body">

	<div class="container-fluid">
		<div class="row-without-margin" style="width: 101%;">
			<%-- <div class="col-md-12">
				<h3 class="text-center">${name }</h3>
			</div> --%>

			<div class="col-sm-12">
				<div class="commercial">
					<c:choose>
						<c:when test="${status == 'In Transit' }">

							<h3 class="text-center">${status }(Estimated)</h3>

						</c:when>
						<c:when test="${status == 'Delivered' }">

							<h3 class="text-center">Arrived(Estimated)</h3>

						</c:when>
						<c:otherwise>
							<h3 class="text-center">${status }</h3>
						</c:otherwise>

					</c:choose>
				</div>
			</div>

		</div>
		<!--/row-->
		<!-- <hr> -->
		<div class="row" style="margin-left: 0px;">


			<div class="col-md-12 table-responsive">
				<table class="table table-bordered table-striped" id="shipmentTable">
					<thead style="background-color: #222534; color: #fff;">
						<tr id="shipmentHeader">
							
							
							<c:choose>
									<c:when test="${status == 'Order Booked' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>${nameBuyerSupplier }</th>
										<th>${weightHeader }</th>
										<th>Total CBM</th>
										<th>Total Quantity</th>
									</c:when>
									<c:when test="${status == 'Goods Received' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>${nameBuyerSupplier }</th>
										<th>GR Date</th>
										<th>Place of Receipt</th>
										<th>${weightHeader }</th>
										<th>Total CBM</th>
										<th>Total Quantity</th>
									</c:when>
									<c:when test="${status == 'Stuffing Done' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>${nameBuyerSupplier }</th>
										<th>Stuffing Date</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POL</th>
										<th>ETD</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
									<c:when test="${status == 'In Transit' }">
										<th>BL. No</th>
										<th>${nameBuyerSupplier }</th>
										<th>POL</th>
										<th>ETD</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POD</th>
										<th>ETA</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
									<c:when test="${status == 'Delivered' }">
										<th>BL. No</th>
										<th>${nameBuyerSupplier }</th>
										<th>POL</th>
										<th>ETD</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POD</th>
										<th>ETA</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
								</c:choose>
							
							<!-- <th>POL</th>
							<th>POD</th>
							<th>ETA</th>
							<th>ETD</th> -->



						</tr>
					</thead>
					<tbody>
						<c:forEach items="${statusWiseList}" var="sodetails">
							<tr class="text-center">
								
								
								<c:choose>
									<c:when test="${status == 'Order Booked' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td>${sodetails.nameBuyerSupplier }</td>
										<td>${sodetails.totalWeight }</td>
										<td>${sodetails.totalCBM }</td>
										<td>${sodetails.totalQuantity }</td>
										<%-- <td>${sodetails.volume }</td>
										<td>${sodetails.tot_qty }</td> --%>
									</c:when>
									<c:when test="${status == 'Goods Received' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td>${sodetails.nameBuyerSupplier }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.gr_date}" /></td>
										<td>${sodetails.placeOfReceipt }</td>
										<td>${sodetails.totalWeight }</td>
										<td>${sodetails.totalCBM }</td>
										<td>${sodetails.totalQuantity }</td>
									</c:when>
									<c:when test="${status == 'Stuffing Done' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td>${sodetails.nameBuyerSupplier }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.stuffingDate}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									<c:when test="${status == 'In Transit' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td>${sodetails.nameBuyerSupplier }</td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pod }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.eta}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									<c:when test="${status == 'Delivered' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td>${sodetails.nameBuyerSupplier }</td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pod }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.eta}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<script>
	$(function() {
		$('#shipmentTable').DataTable();
	});
</script>

<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<%@include file="footer_v2_fixed.jsp"%>