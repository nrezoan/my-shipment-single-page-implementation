<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">
<!-- <script
	src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

	




<style>
html, body {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Performance Report</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Performance Report</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="performanceReport" method="post" commandName="dsrParams"
			cssClass="form-incline">
			<div class='row'>

				<div class='col-sm-2'>
					<div class='form-group'>
						<label>From Date</label>
						<form:input path="fromDate" id="fromDate"
							class="date-picker form-control glyphicon glyphicon-calendar" />
					</div>
				</div>
				<div class='col-sm-2'>
					<div class='form-group'>
						<label>TO Date</label>
						<form:input path="toDate" id="toDate"
							class="date-picker form-control glyphicon glyphicon-calendar" />
					</div>
				</div>

				<div class='col-sm-2'>
					<div class='form-group'>
						<label style="display: block; color: #f5f5f5">. </label> <input
							type="submit" id="datesearch"
							class="btn btn-success form-control" name="datesearch"
							value="Search" />
					</div>
				</div>

			</div>
		</form:form>
	</div>
	<hr>
		<div class="row row-without-margin">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h3 style="margin-top: 0px; margin-bottom: 20px;">Search Result</h3>
		</div>
	</div>
	<div class="row row-without-margin">
		<div class="container-fluid table-responsive">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover display"
						id="report">
						<thead style="background-color: #222534; color: #fff;">
							<tr>
								<th>Actual TOD week</th>
								<th style="display: none;">Year</th>
								<th style="display: none;">TSP</th>
								<th style="display: none;">Freight Service</th>
								<th>Actual Receiver</th>
								<th style="display: none;">Incoterms</th>
								<th style="display: none;">Origin</th>
								<th style="display: none;">Destination</th>
								<th>MAWB/MBL</th>
								<th>HAWB/HBL</th>
								<!-- 10 -->
								<th style="display: none;">Gross Weight(kg)</th>
								<th style="display: none;">Volume(m3)</th>
								<th style="display: none;">Ch.Weight(kg)</th>
								<th>ATD Origin</th>
								<th>ATA Destination</th>
								<th style="display: none;">Cut-off Date</th>
								<th style="display: none;">ETA Deadline</th>
								<th style="display: none;">Airport to Airport Transit time met</th>
								<th style="display: none;">Airport to DC Transit time met</th>
								<th style="display: none;">Difference in Days</th>
								<!-- 20 -->


							</tr>
						</thead>
						<tbody>

							<c:forEach items="${searchList}" var="i">
								<tr>
									<td>${i.bl_no}</td>
									<td style="display: none;">${i.bl_date}</td>
									<td style="display: none;">${i.shipper_name}</td>
									<td>${i.buyer_name}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.comm_invoice_no}</td>
									<td style="display: none;">${i.lc_tt_po_no}</td>
									<td style="display: none;">${i.freight_mode}</td>
									<td style="display: none;">${i.po_no}</td>
									<td>${i.material_desc}</td>
									<!-- 10 -->
									<td style="display: none;">${i.style_no}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.article_no}</td>
									<td style="display: none;">${i.sku_no}</td>
									<td style="display: none;">${i.size1}</td>
									<td style="display: none;">${i.color}</td>
									<td style="display: none;">${i.indent_no}</td>
									<td style="display: none;">${i.tot_qty}</td>
									<td style="display: none;">${i.gross_wt}</td>
									<td style="display: none;">${i.net_wt}</td>
									<!-- 20 -->
								</tr>
							</c:forEach>


						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>
<jsp:include page="footer_v2.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});	
		    var table = $('#report').DataTable( {
		        lengthChange: false,
		        buttons: [ 'excel']
		    } );
		 
		    table.buttons().container()
		        .appendTo( '#report_wrapper .col-sm-6:eq(0)' );
	});
</script>
