<jsp:include page="header.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        
<%-- <c:set var="sap" value="${bookedPoApprovalBeanList}"></c:set>
<c:out value="${sap}"></c:out> --%>        
<section id="bill-of-landing">

	<div class="col-xs-12" style="height: 20px;"></div>
	
	<div class="container-fluid">
			<div class="row">
				<div class="col-md-1"></div>				
				<div class="col-md-10">
					<table class="table">
						<thead>
							<tr>
								<th colspan="12" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center; font-size: 17px;">Approval Summary</th>
							</tr>
						</thead>
						<tbody style="text-align: center;">
							<tr class="info">
								<td style="font-weight: bold;">Sl</td>
								<td style="font-weight: bold;">HBL No.</td>
								<td style="font-weight: bold;">PO No.</td>
								<td style="font-weight: bold;">Status</td>								
							</tr>
							<% int sl=1; %>
							<c:forEach items="${bookedPoApprovalBeanList}" var="approvedPO" >								
             					<tr>
									<td><% out.print(sl);%></td>
									<% sl++; %>
									<td>${approvedPO.bl_no}</td>
					                <td>${approvedPO.po_no}</td>
					                <td>${approvedPO.status}</td>
             					</tr>
      						</c:forEach>

						</tbody>
					</table>
				</div>
				<div class="col-md-1"></div>
			</div>
</div>			
			
</section>        
        <script>
            function myFunction2(){
                      document.getElementById("ship").style.backgroundColor = "#a5a5a5";
                      document.getElementById("plane").style.backgroundColor = "#fff";
                      document.getElementById("sea-text").innerHTML = "SEA";
                      document.getElementById("air-text").innerHTML = "AIR";
          }
            
             function myFunction1(){
                      document.getElementById("plane").style.backgroundColor = "#a5a5a5";
                      document.getElementById("ship").style.backgroundColor = "#fff";
                      document.getElementById("air-text").innerHTML = "AIR";
                      document.getElementById("sea-text").innerHTML = "SEA";
          }
        </script>
        <jsp:include page="footer.jsp"></jsp:include>
    </body>
</html>