<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/poTracking.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
<style>
	html {
		background-color: #f5f5f5;
	}
	body {
		background-color: #f5f5f5;
	}
	
	tbody td {
		font-size: 14px;
	}
	
/* 	.nav > li > a:hover,
.nav > li > a:active,
.nav > li > a:focus {
  color: #444;
   background: #f7f7f7;
}
.nav > li > a:focus {
  color: #444;
  background: #000;
} */
div.tab-content
{
padding: 10px;
}
.active a {

  background-color:#222534 !important;
}
.tab-content{
	min-height:120px !important;
	border: 0px;
}
.foc
{
 background-color:#DCDCDC;
}
.foc:hover {
    background-color: #999999;
} 

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
	color:#fff !important;
}

.nav-tabs.nav-justified>li>a {
    border-style: solid;
    border-radius: 0 0 0 0; 
}

.nav>li>a:hover {
	color: #fff !important;
}

</style>


	

<%-- <section class="main-body">
	<div class="well assign-po-search">
		<form:form action="getPoTrackingDetail" method="post"
			commandName="poTrackingParams" id="poTrack">
			<legend>PO Tracking</legend>
			<div class='row'>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>PO Number</label>
							<form:input path="doc_no" id="poNo" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label>
							<form:input path="fromDate" id="fromDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>TO Date</label>
							<form:input path="toDate" id="toDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="statusType" id="sType" cssClass="form-control">
								<form:option value="NONE">--- Select ---</form:option>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div>
					<!--  
					<div class='col-sm-2' style="margin-top: 2%;">
						<div class='form-group'>

							<button type="submit" class="btn btn-success btn-lg form-control">Search</button>
						</div>
					</div>
					-->
					<div class='col-sm-2'>
                  <div class='form-group'>
                   <label style="display:block;color:#f5f5f5">. </label> <!-- added for alignment purpose, dont delete -->       
                   <input type="submit" id="btn-appr-po-search"class="btn btn-success form-control" value="Search"/>
                 </div>
                 </div>
                 
				</div>
		</form:form>
	</div> --%>
	<section class="content-header" style="display: display;margin-bottom:10px">
	<legend>PO Tracking</legend>
</section>
<section class="main-body">


		<ul class="nav nav-tabs nav-justified nav-book-update"
			style="padding:10px;" id=myTab>
			<li id="posearch" class="active foc" ><a data-toggle="tab"
				href="#powise">PO-Wise Tracking</a></li>
			<li id="datesearch" class="foc"><a data-toggle="tab"
				href="#datewise">Date-Wise Tracking</a></li>

		</ul>

		<div class="tab-content">
			<div id="powise" class="tab-pane fade in active">
				<form:form action="getPoTrackingDetail" method="post"
					commandName="poTrackingParams" id="powiseTrack">
					
					

					<div class='row'>
						<div class='col-sm-2'>
							<div class='form-group'>
								<label>PO Number</label>
								<form:input path="doc_no" id="poNo" cssClass="form-control" />
							</div>
						</div>


						<div class='col-sm-2'>
							<div class='form-group'>
								<label style="display: block; color: #f5f5f5">. </label> 
								<input type="submit" id="posearch" class="btn btn-success form-control" name="posearch" value="Search" onclick="return reportSearch();" />
							</div>
						</div>
					</div>

				</form:form>
			</div>
			<div id="datewise" class="tab-pane fade">
				<form:form action="getPoTrackingDetail" method="post"
					commandName="poTrackingParams" id="datewiseTrack">
					<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label>
							<form:input path="fromDate" id="fromDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>TO Date</label>
							<form:input path="toDate" id="toDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="statusType" id="sType" cssClass="form-control">
								<form:option value="NONE">--- Select ---</form:option>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div>

					<div class='col-sm-2'>
							<div class='form-group'>
								<label style="display: block; color: #f5f5f5">. </label>
								<input type="submit" id="datesearch"
									class="btn btn-success form-control" name="datesearch" value="Search" onclick="return dateSearch();" />
							</div>
						</div>

					</div>
				</form:form>
			</div>
		</div>


<c:if test="${ not empty poTrackingResultBeanLst}">

	<div class="container-fluid table-responsive">
		
		<div class="row search-result-datatable">
			
				<table class="table table-bordered table-striped" id="po_table">
					<thead style="background-color:#222534;color:#fff;">
						<tr>

							<th>PO No</th>
							<th>Comm. Invoice No</th>
							<th>Goods Receive Date</th>
							<th>PO Status</th>
							<th>Exception Status</th>
							
						</tr>
					</thead>
					<tbody id="accordion">

						<c:forEach items="${poTrackingResultBeanLst}" var="i" varStatus="index">
							<tr class="">
								<td style="font-size:12px;">${i.po_no} <a style="color:#3c8dbc !important;" data-toggle="collapse" href="#${index.count}"  onclick="hideexpand(this);"><span id="#${index.count}"  class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
								<td style="font-size:12px;">${i.comm_invoice_no}</td>
								<fmt:parseDate value="${i.gr_date}" var="grDate" pattern="yyyyMMdd" />
								<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${grDate}" /></td>
								<fmt:parseDate value="${i.shipment_date}" var="shipmentDate" pattern="yyyyMMdd" />
								<!--  <td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${shipmentDate}" /></td>-->
								<td style="font-size:12px;">${i.poStatus}</td>
								<td style="font-size:12px;">${i.shipmentStatus}</td>
								
							</tr>
							
							<tr class="hide">
	 <td colspan="7">
	 <div class="collapse" id="${index.count}">
	 <table class="table table-striped" style="width:80%;margin-left: 8%;">
	 <tr>
	 	<th>HBL/AWB No</th>
	 	<th>Shipper</th>
	 	<th>Consignee</th>
	 	<th>Delivery Agent</th>
	 	<th>Carrier</th>
	 </tr>
	 	<tr >
        	<td style="font-size:12px;"><a href="getTrackingInfoFrmUrl?searchString=${i.bl_no}">${i.bl_no}</a></td>
        	<td style="font-size:12px;" >${i.shipper_name}</td>
        	<td style="font-size:12px;">${i.buyer_name}</td>
			<td style="font-size:12px;">${i.agent_name}</td>
			<td style="font-size:12px;">${i.carrier_name}</td>
			
		</tr>
	    <tr>
			<th>Comm. Invoice No</th>
		 	<th>POL</th>
		 	<th>POD</th>
		 	<th>Total Pcs.</th>
		 	<th>Gross Wt.</th>
	    </tr>	
		 <tr>
        	<td style="font-size:12px;">${i.comm_invoice_no}</td>
        	<td style="font-size:12px;">${i.pol_name}</td>
        	<td style="font-size:12px;">${i.pod_name}</td>
			<td style="font-size:12px;">${i.tot_pcs}</td>
			<td style="font-size:12px;">${i.gross_wt}</td>
			
		</tr>
		<tr>
			<th>Booking Date</th>
		 	<th>ETD</th>
		 	<th>ATD</th>
		 	<th>ETA</th>
		 	<th>ATA</th>
	 	</tr>
		 <tr>
		    <fmt:parseDate value="${i.booking_date}" var="bookingDate" pattern="yyyyMMdd" />
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${bookingDate }"/></td>
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.etd}"/></td>
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.atd}"/></td>
			<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.eta}"/></td>
			<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.ata}"/></td>
			
		</tr>
		</table>
		</div>
		</td>
	 </tr>
						</c:forEach>


					</tbody>
				</table>
			
		</div>

	</div>
</c:if>

</section>

<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>



<script>


var checking = JSON.parse('${itemTabActive}');
console.log(checking);
if (checking) {
	$('#posearch').removeClass('active');
	$('#datesearch').addClass('active');		
	$('#powise').removeClass('tab-pane fade in active').addClass('tab-pane fade');
	$('#datewise').removeClass('tab-pane fade').addClass('tab-pane fade in active');
}


</script>

<script>
	function reportSearch() {
		var number = $("#poNo").val();
		if(number == "") {
			swal ( "PO Search" ,  "PO Number Field can not be Empty!" ,  "error" );
			return false;
		} else {
			return true;
		}
	}
</script>

<script>
	function dateSearch() {
		var fdate = $("#fromDate").val();
		var tdate = $("#toDate").val();
		if(fdate == "") {
			swal ( "PO Search" ,  "Form Date Field can not be Empty!" ,  "error" );
			return false;
		}
		if(tdate=="")
			{
			swal ( "PO Search" ,  "TO Date Field can not be Empty!" ,  "error" );
			return false;
			
			}
		else {
			return true;
		}
	}
</script>
<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
	});
	
	  $('.date-picker').each(function() {
	      $(this).removeClass('hasDatepicker').datepicker({
	          dateFormat: "dd.mm.yy"
	      });
	  });
	
	$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
		localStorage.setItem('activeTab', $(e.target).attr('href'));
	});
	var activeTab = localStorage.getItem('activeTab');
	if (activeTab) {
		$('#myTab a[href="' + activeTab + '"]').tab('show');
	}

	function hideexpand(varobj) {
		var res = varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');

		//var toggleicon = $myGroup.children('.collapsed');		   
		//alert(toggleicon);

		$($myGroup)
				.find('.collapse')
				.each(
						function() {
							//alert($(this).attr('id'));
							if (res[1] == $(this).attr('id')) {

								$(this).closest("tr").toggleClass("hide");
								$(this).toggle();

								//alert("entering");
								//alert(document.getElementById("myicon"));
								//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
								//alert(document.getElementById("myicon"));
								if (document.getElementById("#" + res[1]).className == "glyphicon glyphicon-chevron-down") {
									//alert("entering");
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-up";
								} else {
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-down";
								}

							} else {
								//alert($(this).attr('id'))
								//$(this).slideUp( "slow", "linear" )
								//$(this).closest("tr").addClass("hide");
								//$(this).hide();
							}

							//toggleicon.html("<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>");

						});

	}
</script>



