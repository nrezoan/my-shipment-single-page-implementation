    <jsp:include page="header.jsp"></jsp:include>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
 <script src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
 
        <style>
        	.panel-group .panel {
		border-radius: 0;
		box-shadow: none;
		border-color: #EEEEEE;
	}

	.panel-default > .panel-heading {
		padding: 0;
		border-radius: 0;
		color: #212121;
		background-color: #FAFAFA;
		border-color: #EEEEEE;
	}

	.panel-title {
		font-size: 14px;
	}

	.panel-title > a {
		display: block;
		text-decoration: none;
	}

	.more-less {
		color: #212121;
	}

	.panel-default > .panel-heading + .panel-collapse > .panel-body {
		border-top-color: #EEEEEE;
	}
            
            .booking-update1 li {
                float: left;
                border-right: 1px solid black;
            }
            .message-success{
            color: green; 
            }
            .message-error{
            color: red; 
            }
            .table .table-bordered .minimal_cell{
            background: none;
    border-width: 0;
    border: none;
    	}
        </style>
   <script type="text/javascript">
   var directBookingParams=${directBookingParamsJson};
       

   
</script>
<!--Start of the collapse-->
        <div class="col-xs-12" style="height:40px;"></div>
        
        <div class="container">
          <div class="row">
          <span class="" id="span-message"></span>
          <c:if test="${updateStatus==true }">
           <span class="message-success" id="span-message">${updateMessage }</span>			    
          </c:if>
           <c:if test="${updateStatus==false }">
           <span class="message-error" id="span-message">${updateMessage }</span>
          </c:if>
          <div class="col-md-12">
          <div class="panel-heading">
          <h4 class="panel-title" style="font-weight: bold; width: 100px; margin-top: 25px; text-align: center;padding: 5px;border-radius: 10px;background-color: #04afef; color: #fff;">
           Header Item
          </h4>
          </div>
          </div>
          
          	        
          
          
          
          
          </div>
          
	  
	          
	     </div>
     
        
        <!--DEMO-->
        
        <div class="container demo">
	
	    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne" style="margin-bottom: -20px;">
				<h4 class="panel-title">
					
						<table class="table table-bordered">
                           <thead>
                               <th style="margin-right: 0px; width: 20px;"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#One" aria-expanded="true" aria-controls="collapseOne">Expand</a></th>
                               <th style="text-align: center;">HBL. No.</th>
                               <th style="text-align: center;">Exp. No.</th>
                               <th style="text-align: center;">Exp. Date</th>
                               <th style="text-align: center;">Carton Quantity</th>
                               <th style="text-align: center;">Total Gross Weight</th>
                               <th style="text-align: center;">Total CBM</th>
                           </thead>
                            <tbody>
                               <tr>
                                   <td colspan="2" style="text-align: center;">${directBookingParams.orderHeader.hblNumber }</td>
                                   <td style="text-align: center;">${directBookingParams.orderHeader.expNo }</td>
                                   <td style="text-align: center;">${directBookingParams.orderHeader.expDate }</td>
                                   <td style="text-align: center;">${totalQuantity}</td>
                                   <td style="text-align: center;">${grossWeight}</td>
                                   <td style="text-align: center;">${totalCBM}</td>
                               </tr>
                            </tbody>
                        </table>
					
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body" style="border:1px solid gray;">
				<div class="row">
                 <div class="col-md-12">
                 
                  <div class="col-md-6">
                    
                        
						
						<div class="forms">
							<div class="form-group has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Exp. Number:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="expNo" class="form-control" id="expNo" value="${directBookingParams.orderHeader.expNo }"/>
									</div>
								</div>
								</div>
							</div>
							</div>
						
						<div class="forms">
							<div class="form-group has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">LC/TT/PO Number:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="lcTtPono" id="lcTtPono" value="${directBookingParams.orderHeader.lcTtPono }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
<%-- 							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipment Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippingDate"  id="shippingDate" value="${directBookingParams.orderHeader.shippingDate }"class="form-control date-picker"/>
									</div>
								</div>
								</div>
							</div>
							</div> --%>
							
							<div class="forms">
							<div class="form-group1 has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Description Of Goods:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="description"  id="description" class="textarea color-change"  required>${directBookingParams.orderHeader.description }</textarea>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper Name:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperName" class="form-control" id="shipperName" value="${directBookingParams.orderHeader.shipperName }"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper Address 1:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperAddress1" class="form-control" id="shipperAddress1" value="${directBookingParams.orderHeader.shipperAddress1 }"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper Address 2:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperAddress2" id="shipperAddress2" class="form-control" value="${directBookingParams.orderHeader.shipperAddress2 }"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper Address 3:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperAddress3" id="shipperAddress3" value="${directBookingParams.orderHeader.shipperAddress3 }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper City:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperCity" id="shipperCity" value="${directBookingParams.orderHeader.shipperCity }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>							
							<div class="forms" style="display: none;">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper Country:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shipperCountry" id="shipperCountry" value="${directBookingParams.orderHeader.shipperCountry }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Notify Party:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
<%-- 									<input type="text" name="notifyParty"  id="notifyParty" value="${directBookingParams.orderHeader.notifyPartyName}" class="form-control color-change" required/> --%>
									<input type="text" name="notifyParty"  id="notifyParty" value="${fn:escapeXml(directBookingParams.orderHeader.notifyPartyName)}" class="form-control" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
									
										<span class="control-label">Notify Party's Address 1:</span>
									
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="notifyPartyAddress1" id="notifyPartyAddress1" value="${directBookingParams.orderHeader.notifyPartyAddress1 }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
									
										<span class="control-label">Notify Party's Address 2:</span>
									
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="notifyPartyAddress2" id="notifyPartyAddress2" value="${directBookingParams.orderHeader.notifyPartyAddress2 }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
									
										<span class="control-label">Notify Party's Address 3:</span>
									
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="notifyPartyAddress3" id="notifyPartyAddress3" value="${directBookingParams.orderHeader.notifyPartyAddress3 }" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Notify Party's City:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="notifyPartyCity"  id="notifyPartyCity" value="${directBookingParams.orderHeader.notifyPartyCity }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms" style="display: none;">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Notify Party's Country:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="notifyPartyCountry"  id="notifyPartyCountry" value="${directBookingParams.orderHeader.notifyPartyCountry }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Remarks1:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="remarks1" id="remarks1" class="textarea">${directBookingParams.orderHeader.remarks1 }</textarea>
									</div>
								</div>
								</div>
							</div>
							</div>
						
							
                  </div>
                     
                  <div class="col-md-6">
                    
                        	<div class="forms">
							<div class="form-group has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Exp. Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="expDate" id="expDate" value="${directBookingParams.orderHeader.expDate }"class="form-control date-picker"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							
							<div class="forms">
							<div class="form-group has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">LC/TT/PO/Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="lcTtPoDate" id="lcTtPoDate" value="${directBookingParams.orderHeader.lcTtPoDate }"class="form-control date-picker"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
<!-- 							<div class="forms">
							<div class="form-group required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label"  ></span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="textarea" class="textarea color-change" required style="display: none;"></textarea>
									</div>
								</div>
								</div>
							</div>
							</div> -->
							
							<div class="forms">
							<div class="form-group has-error required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Shipping Mark:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="shippingMark"  id="shippingMark" class="textarea color-change" required>${directBookingParams.orderHeader.shippingMark }</textarea>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Buyer Name:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">									
<%-- 									<input type="text" name="buyerName" id="buyerName" value="${directBookingParams.orderHeader.buyerName }"class="form-control"/> --%>
									<input type="text" name="buyerName" id="buyerName" value="${fn:escapeXml(directBookingParams.orderHeader.buyerName)}" class="form-control"/>
									</div>											
																				
								</div>
								</div>
							</div>
							</div>
                        
                        <!--SHIPPER'S BANK ADDRESS-->
                        <div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Buyer Address 1:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="buyerAddress1" id="buyerAddress1" value="${directBookingParams.orderHeader.buyerAddress1 }"class="form-control"/>
									</div>
                                    <div class="input-group-addon">
								
									</div>
								</div>
								</div>
							</div>
							</div>
							 <div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Buyer Address 2:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="buyerAddress2" id="buyerAddress2" value="${directBookingParams.orderHeader.buyerAddress2 }" class="form-control"/>
									</div>
                                    <div class="input-group-addon">
								
									</div>
								</div>
								</div>
							</div>
							</div>
							 <div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Buyer Address 3:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="buyerAddress3" id="buyerAddress3" value="${directBookingParams.orderHeader.buyerAddress3 }" class="form-control"/>
									</div>
                                    <div class="input-group-addon">
								
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Buyer City:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="buyerCity" id="buyerCity" value="${directBookingParams.orderHeader.buyerCity }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>							
							<div class="forms" style="display: none;">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Buyer Country:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="buyerCountry" id="buyerCountry" value="${directBookingParams.orderHeader.buyerCountry }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper's Bank Name:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankName" id="shippersBankName" value="${directBookingParams.orderHeader.shippersBankName }"class="form-control " required/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper's Bank Address 1:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankAddress1" id="shippersBankAddress1" value="${directBookingParams.orderHeader.shippersBankAddress1 }"class="form-control "/>
									</div>
								</div>
								</div>
							</div>
							</div>							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper's Bank Address 2:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankAddress2" id="shippersBankAddress2" value="${directBookingParams.orderHeader.shippersBankAddress2 }"class="form-control " />
									</div>
								</div>
								</div>
							</div>
							</div>							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipper's Bank Address 3:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankAddress3" id="shippersBankAddress3" value="${directBookingParams.orderHeader.shippersBankAddress3 }"class="form-control " />
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper's Bank City:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankCity" id="shippersBankCity" value="${directBookingParams.orderHeader.shippersBankCity }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms" style="display: none;">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Shipper's Bank Country:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="shippersBankCountry" id="shippersBankCountry" value="${directBookingParams.orderHeader.shippersBankCountry }"class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Remarks2:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="remarks2" id="remarks2" class="textarea">${directBookingParams.orderHeader.remarks2 }</textarea>
									</div>
								</div>
								</div>
							</div>
                            </div>
							
							
                  </div>
                 </div>
                </div>
                   
                    <div class="col-xs-12" style="height:10px;"></div> 

                
                   
                    <div class="row" style="float: right;margin-right: 2%;margin-top: 2%;">
								<input type="submit" class="btn btn-primary" value="Update" id="update">
								<button type="button" class="btn btn-danger" id="cancel-head">Cancel</button>
				    </div>
               
                </div>
            </div>
            </div>
            </div>
        </div>
        
        <!--demo-->
 <!--End of the collapse--> 
        
        
 <!--Start of the collapse-->
<!--       <c:if test="${directBookingParams.orderItemLst[0].grFlag!=x}"> -->
        <div class="container">
          <div class="row">
          <div class="col-md-12">
          	<div class="panel-heading">
          	<h4 class="panel-title" style="font-weight: bold; width: 100px; text-align: center;padding: 5px;border-radius: 10px;background-color: #04afef; color: #fff;">
           		Line Item
          	</h4>
          		<c:choose>
          			<c:when test="${grDone eq 'show'}">
					<a href="#addItem" class="btn btn-primary" id="addItemBtn"
						style="float: right;"> <span class="glyphicon glyphicon-plus"></span>
						Add Item
					</a>
					</c:when>
					<c:when test="${grDone eq 'hide'}">
					<a href="#addItem" class="btn btn-primary" id="addItemBtn"
						style="float: right; display: none;"> <span class="glyphicon glyphicon-plus"></span>
						Add Item
					</a>
					</c:when>
					<c:otherwise>
					<a href="#addItem" class="btn btn-primary" id="addItemBtn"
						style="float: right;"> <span class="glyphicon glyphicon-plus"></span>
						Add Item
					</a>
					</c:otherwise>
				</c:choose>
			</div>
          </div>
          </div>
        </div>
        
        <div class="container demo">

	
	    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingTwo" style="margin-bottom: -20px;">
				<h4 class="panel-title">
					
						<table class="table table-bordered">
                           <thead>
                           	<tr>
                               <th style="text-align: center;"></th>
                               <th style="text-align: center;">Item Number</th>
                               <th style="text-align: center;">PO. Number</th>
                               <th style="text-align: center;">Style</th>
                               <th style="text-align: center;">Size</th>
                               <th style="text-align: center;">Color</th>
                               <th style="text-align: center;">HS Code</th>
                               <th style="text-align: center;">Material</th>
<!--                                <th class="minimal_cell"></th> -->
                               <th style="text-align: center;" class="minimal_cell">Action</th>
                               </tr>
                           </thead>
                            <tbody>
                            <c:forEach items="${directBookingParams.orderItemLst }" var="item">
                            <c:if test="${item.grFlag ne 'x'}">
                               <tr>
                                   <td style="margin-right: 0px; width: 20px;"><input type="hidden" name="${item.itemNumber }" value="${item.itemNumber }"><strong><a role="button" data-toggle="collapse" data-parent="#accordion" href="#Two" aria-expanded="true" aria-controls="collapseTwo">Expand</a></strong></td>
                                   <td style="text-align: center;">${item.itemNumber }</td>
                                   <td style="text-align: center;">${item.poNumber }</td>
                                   <td style="text-align: center;">${item.styleNo }</td>
                                   <td style="text-align: center;">${item.sizeNo }</td>
                                   <td style="text-align: center;">${item.color }</td>
                                   <td style="text-align: center;">${item.hsCode}</td>
                                   <td style="text-align: center;">${item.materialText}</td>
                                   <td style="text-align: center;">
								   	<a href="${pageContext.request.contextPath}/deleteitem?itemId=${item.itemNumber }">
								   		<span class="glyphicon glyphicon-trash"></span>
								   	</a>
                            	   </td>
                               </tr>
                               </c:if>
                               <c:if test="${item.grFlag eq 'x'}">
                               	<tr>
    								<td style="text-align: center;" colspan=9>GOODS ALREADY RECEIVED, LINE ITEM CANNOT BE UPDATED</td>
    								</tr>
    							</c:if>
<%--                            <a href="<c:url value='/deleteitem-${item.itemNumber}' />" >
                                	<span class="glyphicon glyphicon-trash"></span>
                            	</a>
								href="<c:url value='/deleteUser-${listuser.id}' />" --%>
                               </c:forEach>
                            </tbody>
                            </table>
					
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body" style="border:1px solid gray;">
				<div class="row">
                  <div class="col-md-12">
                  <div class="col-xs-12" style="height:20px;"></div>
                      <input type="hidden" name="itemNumber">
                      
                  <!--START OF ITEM DETAILS-->
                      
                      <div class="input-group-addon">
								<h4 style="background-color: #337ab7;padding-top:7px;padding-left:10px; text-align:left;font-weight:bold;height:35px;margin-top:10px; color:#fff;">Item Details</h4>
				            </div>
                  <div class="col-xs-12" style="height:20px;"></div>
                  
                  
                  <div class="col-md-4">							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">PO No:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="poNumber"  class="form-control color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Total Gross Weight:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="grossWeight" class="form-control color-change" required onkeypress="return validateDecimalDataType(this,event);"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>
                      		<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Carton Length:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="cartonLength" class="form-control" onchange="return calculateCBM();"/>
									</div>
								</div>
								</div>
							</div>
							</div>
                      		<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Carton Width:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="cartonWidth" class="form-control" onchange="return calculateCBM();"/>
									</div>
								</div>
								</div>
							</div>
                      	 	</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Carton Height:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="cartonHeight" class="form-control" onchange="return calculateCBM();"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>                      	 	
                      	 	<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Carton Unit:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<select class="form-control" name="cartonMeasurementUnit" id="cartonMeasurementUnit" onchange="return calculateCBM();">
										<option value="">Select</option>
										<option value="IN">IN</option>
										<option selected value="CM">CM</option>
									</select>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Total CBM:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="totalVolume" id="totalVolume" class="form-control color-change" required onkeypress="return validateDecimalDataType(this,event);"/>
									</div>
								</div>
								</div>
							</div>
							</div>
					</div> <!--  class="col-md-4" -->
					<div class="col-md-4">                   									
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Carton Quantity:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="curtonQuantity" class="form-control color-change" required onkeypress="return validateDataType(this,event);" onchange="return calculateCBM();"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group   required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Total Pieces:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="totalPieces" class="form-control color-change" required onkeypress="return validateDataType(this,event);"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Total Net Weight:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="netWeight" class="form-control" onkeypress="return validateDecimalDataType(this,event);"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>                      		
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Style No:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="styleNo" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>
                      		
                      		<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>HS Code:</span><span class="required-alone">*</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="hsCode" class="form-control color-change" required/>
									</div>
								</div>
							</div>
							</div>
							</div>

					</div> <!--  class="col-md-4" -->
					<div class="col-md-4">	
							<div class="forms" style="display:none">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Unit:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<select class="form-control drop" name="unit" id="add_order_frm_orderItem_unit" style="border: 1px solid gray; border-radius: 5px;">
										<option selected value="KG">Kilogram</option>
    <option value="MOK">Mol/kilogram</option>
    <option value="KGM">Kilogram/Mol</option>
    <option value="MOL">Mol</option>
    <option value="MON">Months</option>
    <option value="EA">Each</option>
    <option value="FT2">Square foot</option>
    <option value="FT3">Cubic foot</option>
    <option value="KGS">Kilogram/second</option>
    <option value="KGV">Kilogram/cubic meter</option>
    <option value="TM3">1/cubic meter</option>
    <option value="DZ">Dozen</option>
    <option value="JAR">JAR</option>
    <option value="DM">Decimeter</option>
    <option value="PWC">PLYWOOD CASE</option>
    <option value="?GQ">Microgram/cubic meter</option>
    <option value="?GL">Microgram/liter</option>
    <option value="MPB">Mass parts per billion</option>
    <option value="DR">Drum</option>
    <option value="MPA">Megapascal</option>
    <option value="KGF">Kilogram/Square meter</option>
    <option value="ROL">Role</option>
    <option value="&quot;3">Cubic inch</option>
    <option value="&quot;2">Square inch</option>
    <option value="KGK">Kilogram/Kilogram</option>
    <option value="&quot;">Inch</option>
    <option value="MNM">Millinewton/meter</option>
    <option value="%">Percentage</option>
    <option value="EU">Enzyme Units</option>
    <option value="KVA">Kilovoltampere</option>
    <option value="V%">Percent volume</option>
    <option value="C3S">Cubic centimeter/second</option>
    <option value="D">Days</option>
    <option value="ACR">Acre</option>
    <option value="KWH">Kilowatt hours</option>
    <option value="F">Farad</option>
    <option value="G">Gram</option>
    <option value="MMO">Millimol</option>
    <option value="BDR">Board Drums</option>
    <option value="MMH">Millimeter/hour</option>
    <option value="A">Ampere</option>
    <option value="MMK">Millimol/kilogram</option>
    <option value="L">Liter</option>
    <option value="M">Meter</option>
    <option value="N">Newton</option>
    <option value="QML">Kilomol</option>
    <option value="MMG">Millimol/gram</option>
    <option value="H">Hour</option>
    <option value="MMA">Millimeter/year</option>
    <option value="J">Joule</option>
    <option value="K">Kelvin</option>
    <option value="W">Watt</option>
    <option value="V">Volt</option>
    <option value="A/V">Siemens per meter</option>
    <option value="FT">Foot</option>
    <option value="P">Points</option>
    <option value="S">Second</option>
    <option value="MMS">Millimeter/second</option>
    <option value="MLK">Milliliter/cubic meter</option>
    <option value="R-U">Nanofarad</option>
    <option value="MLI">Milliliter act. ingr.</option>
    <option value="EML">Enzyme Units / Milliliter</option>
    <option value="HA">Hectare</option>
    <option value="BUN">BUNDLES</option>
    <option value="WK">Weeks</option>
    <option value="LHK">Liter per 100 km</option>
    <option value="SKD">SKIDS</option>
    <option value="GM">Gram/Mol</option>
    <option value="GJ">Gigajoule</option>
    <option value="MM3">Cubic millimeter</option>
    <option value="MM2">Square millimeter</option>
    <option value="NAM">Nanometer</option>
    <option value="RF">Millifarad</option>
    <option value="M-2">1 / square meter</option>
    <option value="VPB">Volume parts per billion</option>
    <option value="GHG">Gram/hectogram</option>
    <option value="%O">Per mille</option>
    <option value="PKG">Package</option>
    <option value="VPM">Volume parts per million</option>
    <option value="QT">Quart, US liquid</option>
    <option value="VPT">Volume parts per trillion</option>
    <option value="STR">String</option>
    <option value="AU">Activity unit</option>
    <option value="PT">Pint, US liquid</option>
    <option value="KJM">Kilojoule/Mol</option>
    <option value="JMO">Joule/Mol</option>
    <option value="KJK">Kilojoule/kilogram</option>
    <option value="PS">Picosecond</option>
    <option value="LMS">Liter/Molsecond</option>
    <option value="BT">Bottle</option>
    <option value="NMM">Newton/Square millimeter</option>
    <option value="TO">Tonne</option>
    <option value="PMI">1/minute</option>
    <option value="MIJ">Millijoule</option>
    <option value="TS">Thousands</option>
    <option value="KIK">kg act.ingrd. / kg</option>
    <option value="ST">items</option>
    <option value="SET">SETS</option>
    <option value="MIS">Microsecond</option>
    <option value="MIN">Minute</option>
    <option value="LMI">Liter/Minute</option>
    <option value="UNI">Unit</option>
    <option value="TES">Tesla</option>
    <option value="?C">Degrees Celsius</option>
    <option value="KHZ">Kilohertz</option>
    <option value="CV">Case</option>
    <option value="RLS">ROLLS</option>
    <option value="?F">Fahrenheit</option>
    <option value="GKG">Gram/kilogram</option>
    <option value="000">Meter/Minute</option>
    <option value="MI2">Square mile</option>
    <option value="JKK">Spec. Heat Capacity</option>
    <option value="CD">Candela</option>
    <option value="MHZ">Megahertz</option>
    <option value="JKG">Joule/Kilogram</option>
    <option value="CM">Centimeter</option>
    <option value="OHM">Ohm</option>
    <option value="MHV">Megavolt</option>
    <option value="CL">Centiliter</option>
    <option value="GPH">Gallons per hour (US)</option>
    <option value="KOH">Kiloohm</option>
    <option value="TC3">1/cubic centimeter</option>
    <option value="M3">Cubic meter</option>
    <option value="M2">Square meter</option>
    <option value="MG">Milligram</option>
    <option value="COI">COIL</option>
    <option value="ONE">One</option>
    <option value="DAY">Days</option>
    <option value="ML">Milliliter</option>
    <option value="MI">Mile</option>
    <option value="IDR">Iron Drum</option>
    <option value="GOH">Gigaohm</option>
    <option value="MA">Milliampere</option>
    <option value="KPA">Kilopascal</option>
    <option value="MV">Millivolt</option>
    <option value="MGW">Megawatt</option>
    <option value="MW">Milliwatt</option>
    <option value="M/M">Mol per cubic meter</option>
    <option value="MWH">Megawatt hour</option>
    <option value="M/L">Mol per liter</option>
    <option value="MN">Meganewton</option>
    <option value="MGQ">Milligram/cubic meter</option>
    <option value="MM">Millimeter</option>
    <option value="MGO">Megohm</option>
    <option value="M/H">Meter/Hour</option>
    <option value="COL">COLLI</option>
    <option value="MGL">Milligram/liter</option>
    <option value="MGK">Milligram/kilogram</option>
    <option value="CON">Container</option>
    <option value="VAM">Voltampere</option>
    <option value="VAL">Value-only material</option>
    <option value="NI">Kilonewton</option>
    <option value="MGG">Milligram/gram</option>
    <option value="MGE">Milligram/Square centimeter</option>
    <option value="NM">Newton/meter</option>
    <option value="NA">Nanoampere</option>
    <option value="M/S">Meter/second</option>
    <option value="V%O">Permille volume</option>
    <option value="KMH">Kilometer/hour</option>
    <option value="CM2">Square centimeter</option>
    <option value="M2S">Square meter/second</option>
    <option value="PAL">Pallet</option>
    <option value="BLK">BULK</option>
    <option value="MVA">Megavoltampere</option>
    <option value="NS">Nanosecond</option>
    <option value="KMN">Kelvin/Minute</option>
    <option value="PAA">Pair</option>
    <option value="NO">NOS</option>
    <option value="KMK">Cubic meter/Cubic meter</option>
    <option value="PPT">Parts per trillion</option>
    <option value="PAC">Pack</option>
    <option value="OM">Spec. Elec. Resistance</option>
    <option value="TRS">TRUSSES</option>
    <option value="CMH">Centimeter/hour</option>
    <option value="YD2">Square Yard</option>
    <option value="PPM">Parts per million</option>
    <option value="KMS">Kelvin/Second</option>
    <option value="PPB">Parts per billion</option>
    <option value="YD3">Cubic yard</option>
    <option value="WDP">Wooden Pallet</option>
    <option value="CMS">Centimeter/second</option>
    <option value="?A">Microampere</option>
    <option value="OZ">Ounce</option>
    <option value="MEJ">Megajoule</option>
    <option value="WDC">Wooden Case</option>
    <option value="LPH">Liter per hour</option>
    <option value="WDB">WOODEN BOX</option>
    <option value="KM2">Square kilometer</option>
    <option value="GM2">Gram/square meter</option>
    <option value="GM3">Gram/Cubic meter</option>
    <option value="?M">Micrometer</option>
    <option value="?L">Microliter</option>
    <option value="GLI">Gram/liter</option>
    <option value="WCR">WOODEN CRATE</option>
    <option value="PA">Pascal</option>
    <option value="?F">Microfarad</option>
    <option value="RHO">Gram/cubic centimeter</option>
    <option value="WMK">Heat Conductivity</option>
    <option value="CRT">Crate</option>
    <option value="OCM">Spec. Elec. Resistance</option>
    <option value="HL">Hectoliter</option>
    <option value="DEG">Degree</option>
    <option value="PRS">Number of Persons</option>
    <option value="HR">Hours</option>
    <option value="FOZ">Fluid Ounce US</option>
    <option value="MTE">Millitesla</option>
    <option value="HZ">Hertz (1/second)</option>
    <option value="PRC">Group proportion</option>
    <option value="MBA">Millibar</option>
    <option value="CAR">Carton</option>
    <option value="KBK">Kilobecquerel/kilogram</option>
    <option value="HPA">Hectopascal</option>
    <option value="IB">Pikofarad</option>
    <option value="CAN">Canister</option>
    <option value="BOX">BOXES</option>
    <option value="GAU">Gram Gold</option>
    <option value="M3H">Cubic meter/Hour</option>
    <option value="PBG">POLY BAG</option>
    <option value="M3S">Cubic meter/second</option>
    <option value="GAL">US gallon</option>
    <option value="MSC">Microsiemens per centimeter</option>
    <option value="YD">Yards</option>
    <option value="FDR">FIBER DRUM</option>
    <option value="MSE">Millisecond</option>
    <option value="GAI">Gram act. ingrd.</option>
    <option value="WKY">Evaporation Rate</option>
    <option value="PAS">Pascal second</option>
    <option value="GRO">Gross</option>
    <option value="MBZ">Meterbar/second</option>
    <option value="KD3">Kilogram/cubic decimeter</option>
    <option value="22S">Square millimeter/second</option>
    <option value="MS2">Meter/second squared</option>
    <option value="G/L">gram act.ingrd / liter</option>
    <option value="YR">Years</option>
    <option value="CCM">Cubic centimeter</option>
    <option value="KA">Kiloampere</option>
    <option value="M%O">Permille mass</option>
    <option value="BQK">Becquerel/kilogram</option>
    <option value="REL">REEL</option>
    <option value="KJ">Kilojoule</option>
    <option value="CD3">Cubic decimeter</option>
    <option value="KG">Kilogram</option>
    <option value="MPZ">Meterpascal/second</option>
    <option value="BAL">BALES</option>
    <option value="MPS">Millipascal seconds</option>
    <option value="MPT">Mass parts per trillion</option>
    <option value="KM">Kilometer</option>
    <option value="BAG">Bag</option>
    <option value="KW">Kilowatt</option>
    <option value="KT">Kilotonne</option>
    <option value="KV">Kilovolt</option>
    <option value="BRL">Barrel</option>
    <option value="MPG">Miles per gallon (US)</option>
    <option value="LB">US pound</option>
    <option value="GPM">Gallons per mile (US)</option>
    <option value="PCS">Pieces</option>
    <option value="KAI">Kilogram act. ingrd.</option>
    <option value="JCN">JERRICANS</option>
    <option value="MPL">Millimol per liter</option>
    <option value="BAR">bar</option>
    <option value="M%">Percent mass</option>
    <option value="TOM">Ton/Cubic meter</option>
    <option value="MPM">Mass parts per million</option>
    <option value="TON">US ton</option>
									</select>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Refer No:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="refNo" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>
                      		<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Product Code/SKU:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="productCode" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
                      		</div> 
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Project No/Article No:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="projectNo" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Size:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="sizeNo" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Color:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="color" class="form-control"/>
									</div>
								</div>
								</div>
							</div>
                      		</div>
							</div> <!-- class="col-md-4" --> 
                  			<div class="col-md-8">
								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
													<div class="input-group-addon book">
														<span>Commodity:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-9 col-lg-9 col-sm-6 col-xs-6">
													<!-- <input type="text" name="material" class="form-control"/> -->
													<select name="material"
														class="form-control color-change auto-complete selectpicker"
														data-live-search="true" required>
														<option value="">--Please Select--</option>
														<%
															try {
														%>
														<c:forEach var="materials" items="${materials }">
															<c:choose>
																<c:when
																	test="${materials.value == 000000000001000106 }">
																	<option value="${materials.value }" selected>${materials.label}</option>
																</c:when>
																<c:otherwise>
																	<option value="${materials.value }">${materials.label}</option>
																</c:otherwise>
															</c:choose>
															<%-- <option value="${materials.value }" >${materials.label}</option> --%>
														</c:forEach>
														<%
															} catch (Exception e) {
																	e.printStackTrace();
																}
														%>
													</select>
												</div>
											</div>
										</div>
									</div>
                      			</div>	
                      		</div>
                  <!-- edit end -->
                  
                  </div>
                  </div>
                    
                    
                    <!--START, COMMERCIAL INVOICE DETAILS-->
                    
                    <div class="col-xs-12" style="height:10px;"></div> 
                    <div class="row">
                    <div class="col-md-12">
<!--                      		<div class="input-group-addon" style="width: 50%;"> -->
                            <div class="input-group-addon" >
								<h4 style="background-color: #337ab7;padding-top:7px;padding-left:10px; text-align:left;font-weight:bold;height:35px;margin-top:10px; color:#fff;">Commercial Invoice Details</h4>
				            </div>
                     <div class="col-xs-12" style="height:20px;"></div> 
                        <div class="col-md-4">
                         <div class="forms">
							<div class="form-group   required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Pay Conday Cond.:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="payCond" class="form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
                        </div>
                        
                        <div class="col-md-4">
                         <div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Unit Price/Pcs:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="unitPrice" class="form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
                        </div>
                        
                        <div class="col-md-4">
                         <div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Currency Unit:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="currency" class="form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
                        </div>
                        
                   <!--END, COMMERCIAL INVOICE DETAILS-->    
                   <!--END OF ITEM DETAILS-->
                        
                    <div class="col-xs-12" style="height:10px;"></div> 
                    <div class="row">    
                    <div class="col-md-12">
                    <div class="col-xs-12" style="height:10px;"></div>
                     
                            <div class="input-group-addon">
								<h4 style="background-color: #337ab7;padding-top:7px;padding-left:10px; text-align:left;font-weight:bold;height:35px;margin-top:10px; color:#fff;">Item Extra</h4>
				            </div>
                     
                        <div class="col-xs-12" style="height:20px;"></div>                        
                        <div class="col-md-4">
                        <div class="forms">
                          <div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Comm. Invoice No.:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="commInvoice" class="form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
                            </div>							

							<div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">GWeight/Carton:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="grossWeightPerCarton" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>
                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Net Cost:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="netCost" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>							
							
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">QC Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="qcDate" class="date-picker form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Release Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="releaseDate" class="date-picker form-control auto-complete color-change" required/>
									</div>
								</div>
								</div>
							</div>
							</div>											                            

                         
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Shipping Mark:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="shippingMark" class="textarea color-change" required></textarea>
									</div>
								</div>
								</div>
							</div>
							</div>
							
							</div>
							<div class="col-md-4">
							<div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Comm. Inv. Date:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="commInvoiceDate" class="form-control date-picker" />
									</div>
								</div>
								</div>
							</div>
							</div>
                            
                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Net Weight/Carton:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="netWeightPerCarton" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>
                            
							<div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Reference#1:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="reference1" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>
                            
                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Reference#3:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="reference3" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>
                            
							<div class="forms">
							<div class="form-group  required">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="">Description of Goods:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="description" class="textarea"></textarea>
									</div>
								</div>
								</div>
							</div>
                            </div>
							

                        </div>                        
                        
                        <div class="col-md-4">
						<div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Carton Serial No.:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="cartonSerNo" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>                            

                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Pieces/Carton:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="pcsPerCarton" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>

                            
                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Reference#2:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="reference2" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>   
							
                            <div class="forms">
							<div class="form-group">
								<div class="input-group1 required">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span class="control-label">Reference#4:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="reference4" class="form-control" />
									</div>
								</div>
								</div>
							</div>
							</div>	
							
							<div class="forms">
							<div class="form-group">
								<div class="input-group1">
								<div class="row">
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<div class="input-group-addon book">
										<span>Item Description:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<textarea type="textarea" name="itemDescription" class="textarea"></textarea>
									</div>
								</div>
								</div>
							</div>
                            </div>									
							

                        </div>  
                        
                    </div>
                </div>
                        
                    <div class="col-xs-12" style="height:10px;"></div> 
                    <div class="row">    
                    <div class="col-md-12">
                    <div class="col-xs-12" style="height:10px;"></div>
                     
                        </div>
                        </div>
                        
                 
                        </div>
                    </div>
                    
                        
                    <div class="row" style="float: right;margin-right: 2%;margin-top: 2%;">
								<input type="submit" class="btn btn-primary" value="Save" id="addItem" style="display: none;" onclick="saveLineItem('this')">
								<input type="submit" class="btn btn-primary" value="Update" id="update-2">
								<button type="button" class="btn btn-danger" id="cancel-item">Cancel</button>
				    </div>
               
                </div>
            </div>
            </div>
            
        
            </div>
            
            
             <!-- container demo -->
            
            
            <div>
             <div class="container">
          <div class="row">
          <div class="col-md-12">
          <div class="panel-heading">
          <h4 class="panel-title" style="font-weight: bold; width: 100px; text-align: center;padding: 5px;border-radius: 10px;background-color: #04afef; color: #fff;">
           Packing List
          </h4>
          </div>
          </div>
          </div>
        </div>
            <strong><a role="button" data-toggle="collapse" data-parent="#accordion" href="#Three" aria-expanded="true" aria-controls="collapseThree">Expand</a></strong>
            			<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body" style="border:1px solid gray;">
				<div class="row">
                 
                        <div class="col-xs-12" style="height:20px;"></div> 
                        <div class="col-md-6">
                         <div class="forms">
							<div class="form-group required">
								
								<div class="input-group1">
								<div  style="color: black;" ><span  style="  z-index: 1;margin-top: -30px;display: block;" >Please add sizes (e.g. L, M XL etc) first before adding packing list items !</span></div>
								<div id="errorMsg-pkl" style="color: red;" ><span  style=" position: absolute; z-index: 1;margin-bottom: 50px;" > </span></div>
								<div class="row">
								
								<div class="col-md-12">
								
									<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2" >
									
									<div class="input-group-addon book" style="background-color: #334d4d; color: #fff;">
										<span class="control-label" style="margin-left: 10px;">Size:</span>
									</div>
									</div>
									<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
									<input type="text" name="text" class="form-control auto-complete color-change" id="txt-add-size" />
									</div>
                                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                                    <button type="button" class="btn btn-primary" id="btn-add-size">Add Size</button>
                                    </div>
								</div>
								</div>
								</div>
							</div>
							</div>
                        </div>
                       
                        <div class="col-md-12" id="table-container">
                         <div class="table-responsive">
                        <table class="table table-bordered" id="tbl-packing-list" style="background-color: #5c8a8a; color: #fff;" id="tbl-pack-list">
    <thead style="background-color: #334d4d;">
    <tr>
    	 <th rowspan="2" style="width: 100px;">Ctn No</th>
      <th rowspan="2">Ctn</th>
      <th rowspan="2">Colour</th>
      <th colspan="0" style="text-align: center;">Size</th>
       <th rowspan="2">Pcs/Ctn</th>
      <th rowspan="2">Total pcs</th>
      <th rowspan="2">Remarks</th>
      <th rowspan="2" style="width: 20px;"> <button type="button" class="btn btn-primary" id="btn-add-item">Add item</button></th>
    </tr>
    <tr>
   
    </tr>
  </thead>
  <tbody style="color: #000;">
    
      
  </tbody>
</table>
                        </div>
                 </div>
                </div>
                   
                    <div class="col-xs-12" style="height:10px;"></div> 

                
                   
                    <div class="row" style="float: right;margin-right: 2%;margin-top: 2%;">
								<input type="submit" class="btn btn-primary" value="Update" id="btn-update-pck-lst">
								<button type="button" class="btn btn-danger" id="cancel-packlist">Cancel</button>
				    </div>
               
                </div>
            </div>
            
            </div>
            
            
            
            <section>
               <div class="row">
                <div class="col-md-12">
                   <div class="col-lg-8 col-md-8">

       <div class="col-xs-12" style="height:20px;"></div>
                 <c:choose>
          			<%-- <c:when test="${grDone eq 'show'}">
					<a href="#addItem" class="btn btn-primary" id="addItemBtn"
						style="float: right;"> <span class="glyphicon glyphicon-plus"></span>
						Add Item
					</a>
					</c:when> --%>
					<c:when test="${grDone eq 'hide'}">
						   <form:form action="${pageContext.request.contextPath }/upload/uploadExcel" commandName="" type="POST" enctype="multipart/form-data">
						    <div class="form-group">
						     <input type="file" name="file" class="file" id="uploadXL">
						     <div class="input-group col-xs-4">
						      <span class="input-group-addon"></span> 
						      <input type="text" class="form-control input-lg" disabled="" placeholder="Upload" style="height: 35px; border-top-right-radius: 0 !important;
						      border-bottom-right-radius: 0 !important;text-align: center;padding-top: 8px; margin-top: 1px !important; display:none;"> 
						      <span
						       class="input-group-btn">
						       <button class="browse btn btn-primary input-lg" type="button" style="height: 35px;border-top-right-radius:5px;border-bottom-right-radius:5px; display:none;">
						        <i class="glyphicon glyphicon-search"></i> Browse
						       </button>
						       <input type="submit" class="btn btn-success" value="Upload" style="margin-left: 8%; display:none;" id="btn-upload">
						      </span>
						      
						      
						     </div>
						    </div>
						    <span style="display: inline;"><a href="${pageContext.request.contextPath}/download/excel/booking-update.xlsx"><img src="${pageContext.request.contextPath}/resources/images/excel.png" style="height: 45px; width: auto; margin-left: 40%;margin-top: -60px;display:none;"/></a> </span>
						   </form:form>
					</c:when>
					<c:otherwise>
					       <form:form action="${pageContext.request.contextPath }/upload/uploadExcel" commandName="" type="POST" enctype="multipart/form-data">
						    <div class="form-group">
						     <input type="file" name="file" class="file" id="uploadXL">
						     <div class="input-group col-xs-4">
						      <span class="input-group-addon"></span> 
						      <input type="text" class="form-control input-lg" disabled="" placeholder="Upload" style="height: 35px; border-top-right-radius: 0 !important;
						      border-bottom-right-radius: 0 !important;text-align: center;padding-top: 8px; margin-top: 1px !important;"> 
						      <span
						       class="input-group-btn">
						       <button class="browse btn btn-primary input-lg" type="button" style="height: 35px;border-top-right-radius:5px;border-bottom-right-radius:5px;">
						        <i class="glyphicon glyphicon-search"></i> Browse
						       </button>
						       <input type="submit" class="btn btn-success" value="Upload" style="margin-left: 8%;" id="btn-upload">
						      </span>
						      
						      
						     </div>
						    </div>
						    <span style="display: inline;"><a href="${pageContext.request.contextPath}/download/excel/booking-update.xlsx"><img src="${pageContext.request.contextPath}/resources/images/excel.png" style="height: 45px; width: auto; margin-left: 40%;margin-top: -60px;"/></a> </span>
						   </form:form>
					</c:otherwise>
				</c:choose>
  </div>
                </div>
               </div>
            </section>
 

 
 </c:if> <!-- line 758 -->  
 
<%--  <c:forEach items="${directBookingParams.orderItemLst }" var="item">
	<c:if test="${item.grFlag eq 'x'}">
    	<p align="center" >GR DONE, LINE ITEM UPDATE IS NOT ALLOWED</p>
    </c:if>
</c:forEach> --%>
 
 <%-- <c:if test="${directBookingParams..orderItemLst(0).grFlag eq 'x'}">
 <p align="center" >GR DONE, LINE ITEM UPDATE IS NOT ALLOWED</p>
 </c:if>   --%>       
            
                      <div class="row" style="float: right;margin-top: 2%;margin-right: 2%;">
								<input type="submit" class="btn btn-primary" value="Finish Update" id="btn-submit">
								<input type="submit" class="btn btn-danger" value="Cancel">
				      </div>
             </div>
        
  
 <!--End of the collapse-->
        <jsp:include page="footer.jsp"></jsp:include>
       <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/booking-update.js"></script>
 