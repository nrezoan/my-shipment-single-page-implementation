<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html, body {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">OK To Ship Certificate</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">OK To Ship Certificate</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getshipCerForGermany" method="post"
			cssClass="form-incline" commandName="req">
			<div class='col-md-4 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label><b> Select Germany Store Location:</b></label>
					<form:select class="form-control" id="zone" path="zone">
						<option value="">Please Select</option>
						<option value="1">STORES_HM</option>
						<option value="2">STORES/ONLINE_COS</option>
						<option value="3">STORES/ONLINE_MONKI</option>
						<option value="4">STORES/ONLINE_P10</option>
						<option value="5">STORES/ONLINE_CHEAP MONDAY</option>
						<option value="6">WM-43</option>
						<option value="7">STORES_WEEKDAY</option>
					</form:select>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" name="generate" id="generate"
					onclick="return reportSearch();" class="btn btn-success btn-block"
					value="Generate" />
			</div>
		</form:form>
	</div>
</div>
<script>
	function reportSearch() {
		var number = $("#zone").val();
		if (number == "") {
			swal("Zone", "Please select a Zone!", "error");
			return false;
		} else {
			return true;
		}
	}
</script>
<%@include file="footer_v2_fixed.jsp"%>