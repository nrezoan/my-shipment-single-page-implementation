<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>

<style>
html, body {
	background-color: #fff;
}

tbody td {
	font-size: 14px;
}
</style>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/inspectionTracking.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>


<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Inspection Tracking</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Inspection Tracking</li>
		</ol>
	</div>
	<hr>

	<div class="row row-without-margin">
		<form:form action="getInspectionTrackingDetail" method="post"
			commandName="insTrackingParams" id="insTrack">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>Inspection Date</label>
					<form:input path="fromDate" id="fromDate"
						class="date-picker form-control glyphicon glyphicon-calendar" reuired="true"/>
				</div>
			</div>
			<%-- 					<div class='col-sm-2'>
						<div class='form-group'>
							<label>TO Date</label>
							<form:input path="toDate" id="toDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Inspection Date</label>
							<form:input path="inspectionDate" id="inspectionDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div> --%>

			<%-- 					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="statusType" id="sType" cssClass="form-control">
								<form:option value="NONE">--- Select ---</form:option>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div> --%>
			<!--  
					<div class='col-sm-2' style="margin-top: 2%;">
						<div class='form-group'>

							<button type="submit" class="btn btn-success btn-lg form-control">Search</button>
						</div>
					</div>
					-->
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label style="display: block; color: #f5f5f5">. </label>
					<!-- added for alignment purpose, dont delete -->
					<input type="submit" id="btn-appr-po-search"
						class="btn btn-success form-control" value="Search" />
				</div>
			</div>


		</form:form>
	</div>
	<hr>

	<c:if test="${ not empty insTrackingResultBeanLst}">
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h3 style="margin-top: 0px; margin-bottom: 20px;">Search Result</h3>
			</div>
		</div>
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="container-fluid table-responsive">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-hover display"
							id="tableInsTrack">
							<thead style="background-color: #222534; color: #fff;">
								<tr>
									<th>PO No</th>
									<th>SAP No</th>
									<th>Inspection Qty</th>
									<th>Order Qty</th>
									<th>Merchandiser</th>
									<th>Mode</th>
									<th>Location</th>
									<th>Adv. Order</th>
									<th>Remarks</th>
									<th>Ins. Date</th>
									<th>Del. Date</th>
									<th>Entry Time</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${insTrackingResultBeanLst}" var="i">
									<tr class="">
										<td>${i.PO_NUMBER}</td>
										<td>${i.SAP_NO}</td>
										<td>${i.INSPECTION_QTY}</td>
										<td>${i.ORDER_QTY}</td>
										<td>${i.INSPECTOR_CODE}</td>
										<td>${i.SHIPMENT_MODE}</td>
										<td>${i.LOCATION}</td>
										<td>${i.ADV_ORDER}</td>
										<td>${i.REMARKS}</td>
										<td>${i.INSPECTION_DATE}</td>
										<td>${i.DELIVERY_DATE}</td>
										<td>${i.ENTRY_TIME}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</div>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>


<script>
	$(document).ready(function() {
		//.datepicker("setDate", new Date());
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-M-y"
		});
		//.datepicker("setDate", new Date());
		$('#tableInsTrack').DataTable({
			responsive : true,
			dom : 'Bfrtip',
			buttons : [ 'csv', 'excel' ]
		});
	});
</script>


<%@include file="footer_v2.jsp"%>