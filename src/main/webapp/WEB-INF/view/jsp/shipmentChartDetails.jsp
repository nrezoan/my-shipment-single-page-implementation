<%@page import="java.util.Date"%>
<%@page import="com.myshipment.util.DateUtil"%>
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<style>
	html {
		background-color: #eaeaea;
	}
	.ship_desti {
		margin-left: 1%;
	}
	th {
		text-align: center;
	}
	.main-body {
		background-color: #eaeaea;
		margin-bottom: 5%;
	}
	.commercial h3 {
	    background-color: #222534;
	    padding-top: 7px;
	    font-weight: 400;
	    height: 40px;
	    margin-top: 10px;
	    color: #fff;
	}
</style>

<section class="main-body">

	<div class="container-fluid">
		<div class="row-without-margin" style="width: 101%;">
			<%-- <div class="col-md-12">
				<h3 class="text-center">${name }</h3>
			</div> --%>
			
			<div class="col-sm-12">
				<div class="commercial">
				<c:choose>
						<c:when test="${requestStatus == 'In Transit' }">

							<h3 class="text-center">${name } (${requestStatus }(Estimated))</h3>

						</c:when>
						<c:when test="${requestStatus == 'Arrived' }">

							<h3 class="text-center">${name } (${requestStatus }(Estimated))</h3>

						</c:when>
						<c:otherwise>
							<h3 class="text-center">${name } (${requestStatus })</h3>
						</c:otherwise>

					</c:choose>
					
				</div>
			</div>
			
		</div>
		<!--/row-->
		<!-- <hr> -->
		<div class="row" style="margin-left: 0px;">


			<%-- <div class="col-md-12 table-responsive" style="display: none;">
				<table class="table table-bordered table-striped" id="shipmentTable">
					<thead style="background-color: #222534; color: #fff;">
						<tr id="shipmentHeader">
							<th>BL. No</th>
							<th>BL. Date</th>
							<!-- <th>Booking Date</th> -->
							<!-- <th>Buyer Name</th> -->
							<th>POL</th>
							<th>POD</th>
							<th>
							<%
								if(loginDto.getDivisionSelected().equals("SE")) {
									request.setAttribute("weightHeader", "Gross Weight");
								} else if(loginDto.getDivisionSelected().equals("AR")) {
									request.setAttribute("weightHeader","Charge Weight");
								}
							%>
							${weightHeader }
							</th>
							<th>Total Quantity</th>
							<th>Total Volume</th>
							<th>GR Date</th>
							<th>Shipment Date</th>


						</tr>
					</thead>
					<tbody>
						<c:forEach items="${shipmentList}"
							var="sodetails">
							<tr class="text-center">
								<td>
									<a href="getTrackingInfoFrmUrl?searchString=${sodetails.bl_no }">${sodetails.bl_no }</a>
								</td>
								<td>
									<c:set var="bl_date" value="${sodetails.bl_bt }"/>
									
									<% 
									if(pageContext.getAttribute("bl_date") != null && !pageContext.getAttribute("bl_date").equals("")) {
										Date blDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("bl_date") + "000000");
										request.setAttribute("blDate", blDate); 
									}
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${blDate}" />
								</td>
								<td>${sodetails.so_dt }</td>
								<td>${sodetails.buyer}</td>
								<td>${sodetails.pol }</td>
								<td>${sodetails.pod }</td>
								<td>
									<c:set var="weightGross" value="${sodetails.grs_wt }"/>
									<c:set var="weightCharge" value="${sodetails.crg_wt }"/>
									<%
									if(loginDto.getDivisionSelected().equals("SE")) {
										request.setAttribute("weightData", pageContext.getAttribute("weightGross"));
									} else if(loginDto.getDivisionSelected().equals("AR")) {
										request.setAttribute("weightData", pageContext.getAttribute("weightCharge"));
									}
									%>
									${weightData }
								</td>
								<td>${sodetails.tot_qty }</td>
								<td>${sodetails.volume }</td>
								<td>
									<c:set var="gr_dt" value="${sodetails.gr_dt }"/>
									<% 
										if(pageContext.getAttribute("gr_dt") != null && !pageContext.getAttribute("gr_dt").equals("")) {
											Date grDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("gr_dt") + "000000");
											request.setAttribute("grDate", grDate);
										}
										 
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${grDate}" />
								</td>
								<td>
									<c:set var="dep_dt" value="${sodetails.dep_dt }"/>
									
									<% 
										if(pageContext.getAttribute("dep_dt") != null && !pageContext.getAttribute("dep_dt").equals("")) {
											Date depDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("dep_dt") + "000000");
											request.setAttribute("depDate", depDate);
										}
										 
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${depDate}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div> --%>
			
			<!-- ------------------------------------------------------------ -->
			<div class="col-md-12 table-responsive">
				<table class="table table-bordered table-striped" id="shipmentTable">
					<thead style="background-color: #222534; color: #fff;">
						<tr id="shipmentHeader">
							
							<c:choose>
									<c:when test="${status == 'openBooking' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>${weightHeader }</th>
										<th>Total CBM</th>
										<th>Total Quantity</th>
									</c:when>
									<c:when test="${status == 'goodsReceived' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>GR Date</th>
										<th>Place of Receipt</th>
										<th>${weightHeader }</th>
										<th>Total CBM</th>
										<th>Total Quantity</th>
									</c:when>
									<c:when test="${status == 'stuffingDone' }">
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>Stuffing Date</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POL</th>
										<th>ETD</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
									<c:when test="${status == 'inTransit' }">
										<th>BL. No</th>
										<th>POL</th>
										<th>ETD</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POD</th>
										<th>ETA</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
									<c:when test="${status == 'arrived' }">
										<th>BL. No</th>
										<th>POL</th>
										<th>ETD</th>
										<!-- <th>Shipping Line</th> -->
										<th>${shippingLineHeader }</th>
										<th>POD</th>
										<th>ETA</th>
										<th>${nameAgentSupplier }</th>
									</c:when>
									<c:otherwise>
										<th>BL. No</th>
										<th>BL. Date</th>
										<th>${weightHeader }</th>
										<th>Total CBM</th>
										<th>Total Quantity</th>
										<th>POL</th>
										<th>POD</th>
										<th>ETD</th>
										<th>ETA</th>
									</c:otherwise>
								</c:choose>
							
							<!-- <th>POL</th>
							<th>POD</th>
							<th>ETA</th>
							<th>ETD</th> -->



						</tr>
					</thead>
					<tbody>
						<c:forEach items="${shipmentChartDetailsList}" var="sodetails">
							<tr class="text-center">
								
								
								<c:choose>
									<c:when test="${status == 'openBooking' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td>${sodetails.totalWeight }</td>
										<td>${sodetails.totalCBM }</td>
										<td>${sodetails.totalQuantity }</td>
										<%-- <td>${sodetails.volume }</td>
										<td>${sodetails.tot_qty }</td> --%>
									</c:when>
									<c:when test="${status == 'goodsReceived' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.gr_date}" /></td>
										<td>${sodetails.placeOfReceipt }</td>
										<td>${sodetails.totalWeight }</td>
										<td>${sodetails.totalCBM }</td>
										<td>${sodetails.totalQuantity }</td>
									</c:when>
									<c:when test="${status == 'stuffingDone' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.stuffingDate}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									<c:when test="${status == 'inTransit' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pod }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.eta}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									<c:when test="${status == 'arrived' }">
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td>${sodetails.pol }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td>${sodetails.shippingLine }</td>
										<td>${sodetails.pod }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.eta}" /></td>
										<td>${sodetails.nameAgentSupplier }</td>
									</c:when>
									<c:otherwise>
										<td><a href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.blDate}" /></td>
										<td>${sodetails.totalWeight }</td>
										<td>${sodetails.totalCBM }</td>
										<td>${sodetails.totalQuantity }</td>
										<td>${sodetails.pol }</td>
										<td>${sodetails.pod }</td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.etd}" /></td>
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sodetails.eta}" /></td>
									</c:otherwise>
									
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<!-- ------------------------------------------------------------ -->
		</div>
	</div>
</section>

<script>

	$(function() {
		$('#shipmentTable').DataTable();
	});
	
	
</script>

<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<%@include file="footer_v2_fixed.jsp"%>