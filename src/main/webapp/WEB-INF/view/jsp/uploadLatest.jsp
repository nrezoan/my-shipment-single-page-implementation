
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/po-upload.js"></script>

<script
	src="${pageContext.request.contextPath}/resources/js/POGenerate.js"></script>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">

<style>
html {
	background-color: #fff;
	font-family: sans-serif;
}

.input-group-addon {
	border: unset !important;
}

.nav-tabs.nav-justified>.active>a, .nav-tabs.nav-justified>.active>a:focus,
	.nav-tabs.nav-justified>.active>a:hover {
	background-color: #007aa7;
	color: #fff;
}

.tab-direct-booking-well {
	background-color: #fff !important;
}

.ul-unset-margin {
	margin-left: unset !important;
}
</style>

<div class="col-xs-12" style="height: 30px;"></div>

<div class="col-xs-12" style="height: 10px;"></div>

<!-- shammi -->
<!-- <div class="container"> -->
<div class="row">
	<div class="col-md-12">
		<!-- Tabs for both options: PO Form and Excel -->
		<ul class="nav nav-tabs nav-justified nav_hov ul-unset-margin"
			role="tablist">

			<li role="presentation" class="active"><a href="#home"
				aria-controls="home" role="tab" data-toggle="tab">PO Generation
					Form</a></li>
			<li role="presentation"><a href="#POExcel"
				aria-controls="POExcel" role="tab" data-toggle="tab">Upload PO
					Excel</a></li>
		</ul>

	</div>
</div>


<input type="hidden" name="POGenerateID" id="POGenerateID"
	value="${approvedPurchaseOrderId }">
<div class="tab-content">


	<div role="tabpanel" class="tab-pane active" id="home">

		<section class="">
			<div class="container-fluid">
				<div id="itemdetail">
					<div id="first-tab">

						<div class="row" style="margin-top: 15px">
							<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
								<div class="forms">
									<div class="form-group">
										<div class="input-group2">
											<div class="row">
												<div class="col-md-6 col-lg-3 col-sm-6 col-xs-6"
													style="margin-left: 51px">
													<div class="input-group-addon">
														<span style="font-weight: bold">SUPPLIER</span><span
															class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-7 col-sm-6 col-xs-6"
													style="margin-left: 13px">
													

													<select name="text"
														class="form-control auto-complete select2"
														data-live-search="true" id="SupplierNo">
														<option value="">--Please Select--</option>
														<%
															try {
														%>
														<c:forEach var="shipper" items="${shipper}">
																	<option value="${shipper.value}">${shipper.value}</option>
															<%-- <option value="${materials.value }" >${materials.label}</option> --%>
														</c:forEach>

														<%-- 													<c:forEach var="materials" items="${materials }">
														<c:choose>
															<c:when test="${materials.value == ${directBookingParams.  }">
																<option value="${materials.value }" selected>${materials.label}</option>
															</c:when>
															<c:otherwise>
																<option value="${materials.value }">${materials.label}</option>
															</c:otherwise>
														</c:choose>
														<option value="${materials.value }" >${materials.label}</option>
														</c:forEach> --%>
														<%
															} catch (Exception e) {
																e.printStackTrace();
															}
														%>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>



						</div>

						<div class="row first-tab-clone tab-direct-booking-well"
							style="margin-top: 15px">
							<!-- Starting of first column -->
							<div class="col-md-3 col-lg-4 col-sm-6 col-xs-12">
								<input type="hidden" name="itemId" id="itemId">


								<div class="forms">
									<div class="form-group">
										<div class="input-group2">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>PO No:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														id="PONumber" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Total Gross Weight:</span><span
															class="required-alone">*</span>

													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														required id="GrossWeight"
														onkeypress="return validateDecimalDataTypeStrict(this,event);" />


												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Carton Length:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														id="CartonLength"
														onkeypress="return validateDecimalDataTypeStrict(this,event);"
														onchange="return calculateCBM();" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Carton Height:</span><span class="required-alone">*</span>

													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														id="CartonHeight"
														onkeypress="return validateDecimalDataTypeStrict(this,event);"
														onchange="return calculateCBM();" />


													<!-- <input type="text" name="text" class="form-control" id="orderItem_car_number" onkeyup="calculateCBM();" onkeypress="return validateDataType(this,event);"/> -->
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Carton Width:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														id="CartonWidth"
														onkeypress="return validateDecimalDataTypeStrict(this,event);"
														onchange="return calculateCBM();" />


												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row row-margin-unset">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Carton Unit:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<select class="form-control drop"
														id="CartonMeasurementUnit"
														onchange="return calculateCBM();">
														<!-- 														onchange="return calculateCBM();" onfocus="return checkMandatoryForCBM()" > -->
														<option value="">Select</option>
														<option value="IN">IN</option>
														<option selected value="CM">CM</option>

													</select>
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								

							</div>


							<!-- End of first column -->

							<!-- Start of Second column -->
							<div class="col-md-3 col-lg-4 col-sm-6 col-xs-12">

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Total Pieces:</span><span class="required-alone">*</span>

													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														title="Only Numbers Allowed!"
														onkeypress="return validateDataType(this,event);"
														id="TotalPcs" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group2">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Carton Quantity:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														id="TotalCarton" data-toggle="tooltip"
														title="Only Numbers Allowed!"
														onkeypress="return validateDataType(this,event);"
														onchange="return calculateCBM();" />


												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>



								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Total CBM:</span><span class="required-alone">*</span>

													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text"
														class="form-control auto-complete" id="TotalCBM"
														onkeypress="return validateDecimalDataTypeStrict(this,event);" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>HS Code:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														id="HSCode" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group2">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Unit:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<select class="form-control drop select2"
														data-live-search="true" id="ItemUoM" style="">
														<option value="">Please Select</option>
														<option value="KG" selected>Kilogram</option>
														<option value="DZ">Dozen</option>
														<option value="JAR">JAR</option>
														<option value="DM">Decimeter</option>
														<option value="PWC">PLYWOOD CASE</option>
														<option value="?GQ">Microgram/cubic meter</option>
														<option value="?GL">Microgram/liter</option>
														<option value="MPB">Mass parts per billion</option>
														<option value="DR">Drum</option>
														<option value="MPA">Megapascal</option>
														<option value="KGF">Kilogram/Square meter</option>
														<option value="ROL">Role</option>
														<option value="&quot;3">Cubic inch</option>
														<option value="&quot;2">Square inch</option>
														<option value="KGK">Kilogram/Kilogram</option>
														<option value="&quot;">Inch</option>
														<option value="MNM">Millinewton/meter</option>
														<option value="%">Percentage</option>
														<option value="EU">Enzyme Units</option>
														<option value="KVA">Kilovoltampere</option>
														<option value="V%">Percent volume</option>
														<option value="C3S">Cubic centimeter/second</option>
														<option value="D">Days</option>
														<option value="ACR">Acre</option>
														<option value="KWH">Kilowatt hours</option>
														<option value="F">Farad</option>
														<option value="G">Gram</option>
														<option value="MMO">Millimol</option>
														<option value="BDR">Board Drums</option>
														<option value="MMH">Millimeter/hour</option>
														<option value="A">Ampere</option>
														<option value="MMK">Millimol/kilogram</option>
														<option value="L">Liter</option>
														<option value="M">Meter</option>
														<option value="N">Newton</option>
														<option value="QML">Kilomol</option>
														<option value="MMG">Millimol/gram</option>
														<option value="H">Hour</option>
														<option value="MMA">Millimeter/year</option>
														<option value="J">Joule</option>
														<option value="K">Kelvin</option>
														<option value="W">Watt</option>
														<option value="V">Volt</option>
														<option value="A/V">Siemens per meter</option>
														<option value="FT">Foot</option>
														<option value="P">Points</option>
														<option value="S">Second</option>
														<option value="MMS">Millimeter/second</option>
														<option value="MLK">Milliliter/cubic meter</option>
														<option value="R-U">Nanofarad</option>
														<option value="MLI">Milliliter act. ingr.</option>
														<option value="EML">Enzyme Units / Milliliter</option>
														<option value="HA">Hectare</option>
														<option value="BUN">BUNDLES</option>
														<option value="WK">Weeks</option>
														<option value="LHK">Liter per 100 km</option>
														<option value="SKD">SKIDS</option>
														<option value="GM">Gram/Mol</option>
														<option value="GJ">Gigajoule</option>
														<option value="MM3">Cubic millimeter</option>
														<option value="MM2">Square millimeter</option>
														<option value="NAM">Nanometer</option>
														<option value="RF">Millifarad</option>
														<option value="M-2">1 / square meter</option>
														<option value="VPB">Volume parts per billion</option>
														<option value="GHG">Gram/hectogram</option>
														<option value="%O">Per mille</option>
														<option value="PKG">Package</option>
														<option value="VPM">Volume parts per million</option>
														<option value="QT">Quart, US liquid</option>
														<option value="VPT">Volume parts per trillion</option>
														<option value="STR">String</option>
														<option value="AU">Activity unit</option>
														<option value="PT">Pint, US liquid</option>
														<option value="KJM">Kilojoule/Mol</option>
														<option value="JMO">Joule/Mol</option>
														<option value="KJK">Kilojoule/kilogram</option>
														<option value="PS">Picosecond</option>
														<option value="LMS">Liter/Molsecond</option>
														<option value="BT">Bottle</option>
														<option value="NMM">Newton/Square millimeter</option>
														<option value="TO">Tonne</option>
														<option value="PMI">1/minute</option>
														<option value="MIJ">Millijoule</option>
														<option value="TS">Thousands</option>
														<option value="KIK">kg act.ingrd. / kg</option>
														<option value="ST">items</option>
														<option value="SET">SETS</option>
														<option value="MIS">Microsecond</option>
														<option value="MIN">Minute</option>
														<option value="LMI">Liter/Minute</option>
														<option value="UNI">Unit</option>
														<option value="TES">Tesla</option>
														<option value="?C">Degrees Celsius</option>
														<option value="KHZ">Kilohertz</option>
														<option value="CV">Case</option>
														<option value="RLS">ROLLS</option>
														<option value="?F">Fahrenheit</option>
														<option value="GKG">Gram/kilogram</option>
														<option value="000">Meter/Minute</option>
														<option value="MI2">Square mile</option>
														<option value="JKK">Spec. Heat Capacity</option>
														<option value="CD">Candela</option>
														<option value="MHZ">Megahertz</option>
														<option value="JKG">Joule/Kilogram</option>
														<option value="CM">Centimeter</option>
														<option value="OHM">Ohm</option>
														<option value="MHV">Megavolt</option>
														<option value="CL">Centiliter</option>
														<option value="GPH">Gallons per hour (US)</option>
														<option value="KOH">Kiloohm</option>
														<option value="TC3">1/cubic centimeter</option>
														<option value="M3">Cubic meter</option>
														<option value="M2">Square meter</option>
														<option value="MG">Milligram</option>
														<option value="COI">COIL</option>
														<option value="ONE">One</option>
														<option value="DAY">Days</option>
														<option value="ML">Milliliter</option>
														<option value="MI">Mile</option>
														<option value="IDR">Iron Drum</option>
														<option value="GOH">Gigaohm</option>
														<option value="MA">Milliampere</option>
														<option value="KPA">Kilopascal</option>
														<option value="MV">Millivolt</option>
														<option value="MGW">Megawatt</option>
														<option value="MW">Milliwatt</option>
														<option value="M/M">Mol per cubic meter</option>
														<option value="MWH">Megawatt hour</option>
														<option value="M/L">Mol per liter</option>
														<option value="MN">Meganewton</option>
														<option value="MGQ">Milligram/cubic meter</option>
														<option value="MM">Millimeter</option>
														<option value="MGO">Megohm</option>
														<option value="M/H">Meter/Hour</option>
														<option value="COL">COLLI</option>
														<option value="MGL">Milligram/liter</option>
														<option value="MGK">Milligram/kilogram</option>
														<option value="CON">Container</option>
														<option value="VAM">Voltampere</option>
														<option value="VAL">value-only material</option>
														<option value="NI">Kilonewton</option>
														<option value="MGG">Milligram/gram</option>
														<option value="MGE">Milligram/Square centimeter</option>
														<option value="NM">Newton/meter</option>
														<option value="NA">Nanoampere</option>
														<option value="M/S">Meter/second</option>
														<option value="V%O">Permille volume</option>
														<option value="KMH">Kilometer/hour</option>
														<option value="CM2">Square centimeter</option>
														<option value="M2S">Square meter/second</option>
														<option value="PAL">Pallet</option>
														<option value="BLK">BULK</option>
														<option value="MVA">Megavoltampere</option>
														<option value="NS">Nanosecond</option>
														<option value="KMN">Kelvin/Minute</option>
														<option value="PAA">Pair</option>
														<option value="NO">NOS</option>
														<option value="KMK">Cubic meter/Cubic meter</option>
														<option value="PPT">Parts per trillion</option>
														<option value="PAC">Pack</option>
														<option value="OM">Spec. Elec. Resistance</option>
														<option value="TRS">TRUSSES</option>
														<option value="CMH">Centimeter/hour</option>
														<option value="YD2">Square Yard</option>
														<option value="PPM">Parts per million</option>
														<option value="KMS">Kelvin/Second</option>
														<option value="PPB">Parts per billion</option>
														<option value="YD3">Cubic yard</option>
														<option value="WDP">Wooden Pallet</option>
														<option value="CMS">Centimeter/second</option>
														<option value="?A">Microampere</option>
														<option value="OZ">Ounce</option>
														<option value="MEJ">Megajoule</option>
														<option value="WDC">Wooden Case</option>
														<option value="LPH">Liter per hour</option>
														<option value="WDB">WOODEN BOX</option>
														<option value="KM2">Square kilometer</option>
														<option value="GM2">Gram/square meter</option>
														<option value="GM3">Gram/Cubic meter</option>
														<option value="?M">Micrometer</option>
														<option value="?L">Microliter</option>
														<option value="GLI">Gram/liter</option>
														<option value="WCR">WOODEN CRATE</option>
														<option value="PA">Pascal</option>
														<option value="?F">Microfarad</option>
														<option value="RHO">Gram/cubic centimeter</option>
														<option value="WMK">Heat Conductivity</option>
														<option value="CRT">Crate</option>
														<option value="OCM">Spec. Elec. Resistance</option>
														<option value="HL">Hectoliter</option>
														<option value="DEG">Degree</option>
														<option value="PRS">Number of Persons</option>
														<option value="HR">Hours</option>
														<option value="FOZ">Fluid Ounce US</option>
														<option value="MTE">Millitesla</option>
														<option value="HZ">Hertz (1/second)</option>
														<option value="PRC">Group proportion</option>
														<option value="MBA">Millibar</option>
														<option value="CAR">Carton</option>
														<option value="KBK">Kilobecquerel/kilogram</option>
														<option value="HPA">Hectopascal</option>
														<option value="IB">Pikofarad</option>
														<option value="CAN">Canister</option>
														<option value="BOX">BOXES</option>
														<option value="GAU">Gram Gold</option>
														<option value="M3H">Cubic meter/Hour</option>
														<option value="PBG">POLY BAG</option>
														<option value="M3S">Cubic meter/second</option>
														<option value="GAL">US gallon</option>
														<option value="MSC">Microsiemens per centimeter</option>
														<option value="YD">Yards</option>
														<option value="FDR">FIBER DRUM</option>
														<option value="MSE">Millisecond</option>
														<option value="GAI">Gram act. ingrd.</option>
														<option value="WKY">Evaporation Rate</option>
														<option value="PAS">Pascal second</option>
														<option value="GRO">Gross</option>
														<option value="MBZ">Meterbar/second</option>
														<option value="KD3">Kilogram/cubic decimeter</option>
														<option value="22S">Square millimeter/second</option>
														<option value="MS2">Meter/second squared</option>
														<option value="G/L">gram act.ingrd / liter</option>
														<option value="YR">Years</option>
														<option value="CCM">Cubic centimeter</option>
														<option value="KA">Kiloampere</option>
														<option value="M%O">Permille mass</option>
														<option value="BQK">Becquerel/kilogram</option>
														<option value="REL">REEL</option>
														<option value="KJ">Kilojoule</option>
														<option value="CD3">Cubic decimeter</option>
														<option value="KG">Kilogram</option>
														<option value="MPZ">Meterpascal/second</option>
														<option value="BAL">BALES</option>
														<option value="MPS">Millipascal seconds</option>
														<option value="MPT">Mass parts per trillion</option>
														<option value="KM">Kilometer</option>
														<option value="BAG">Bag</option>
														<option value="KW">Kilowatt</option>
														<option value="KT">Kilotonne</option>
														<option value="KV">Kilovolt</option>
														<option value="BRL">Barrel</option>
														<option value="MPG">Miles per gallon (US)</option>
														<option value="LB">US pound</option>
														<option value="GPM">Gallons per mile (US)</option>
														<option value="PCS">Pieces</option>
														<option value="KAI">Kilogram act. ingrd.</option>
														<option value="JCN">JERRICANS</option>
														<option value="MPL">Millimol per liter</option>
														<option value="BAR">bar</option>
														<option value="M%">Percent mass</option>
														<option value="TOM">Ton/Cubic meter</option>
														<option value="MPM">Mass parts per million</option>
														<option value="TON">US ton</option>
													</select>
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>


								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-2 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<div class="input-group-addon">
														<span>Total Net Weight:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														id="NetWeight"
														onkeypress="return validateDecimalDataTypeStrict(this,event);" />

													<!-- <input type="text" name="text" class="form-control" id="orderItem_netWeight" onkeypress="return validateDataType(this,event);"/> -->
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End of second column -->



							<!-- Start of Third column -->
							<div class="col-md-3 col-lg-4 col-sm-6 col-xs-12">

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Ref. No/WH Code:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">
													<input type="text" name="text" class="form-control"
														id="ReferenceNo1" />
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Style No./PMA-PMC:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														id="Style" />


													<!-- <input type="text" name="text" class="form-control" id="orderItem_styleNo"/> -->
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>


								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Size No:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text" class="form-control"
														id="Size" />
													<!-- onkeypress="return validateDataType(this,event);" /> -->



												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group2">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Prod. Code/SKU/Season:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">

													<input type="text" name="text" class="form-control"
														id="ProductCode" />

												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Proj./Art./Del. No:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">


													<input type="text" name="text"
														class="form-control auto-complete" id="ProjectNo" />


												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>
								</div>

								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6"
													style="margin-left: 15px">
													<div class="input-group-addon">
														<span>Color:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-5 col-sm-6 col-xs-6">




													<input type="text" name="text" class="form-control"
														id="Color" />

													<!-- <input type="text" name="text" class="form-control" id="orderItem_color"/> -->
												</div>
												<div class="col-md-6 col-lg-1 col-sm-6 col-xs-6"></div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
								<div class="forms">
									<div class="form-group">
										<div class="input-group1">
											<div class="row">
												<div class="col-md-6 col-lg-3 col-sm-6 col-xs-6"
													style="margin-left: 51px">
													<div class="input-group-addon">
														<span>Commodity:</span><span class="required-alone">*</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-7 col-sm-6 col-xs-6"
													style="margin-left: 13px">


													<select name="text"
														class="form-control auto-complete select2"
														data-live-search="true" id="MaterialNo">
														<option value="">--Please Select--</option>
														<%
															try {
														%>
														<c:forEach var="materials" items="${materials}">
															<c:choose>
																<c:when test="${materials.value == 000000000001000106}">
																	<option value="${materials.value }" selected>${materials.label}</option>
																</c:when>
																<c:otherwise>
																	<option value="${materials.value }">${materials.label}</option>
																</c:otherwise>
															</c:choose>
															<%-- <option value="${materials.value }" >${materials.label}</option> --%>
														</c:forEach>

														<%-- 													<c:forEach var="materials" items="${materials }">
														<c:choose>
															<c:when test="${materials.value == ${directBookingParams.  }">
																<option value="${materials.value }" selected>${materials.label}</option>
															</c:when>
															<c:otherwise>
																<option value="${materials.value }">${materials.label}</option>
															</c:otherwise>
														</c:choose>
														<option value="${materials.value }" >${materials.label}</option>
														</c:forEach> --%>
														<%
															} catch (Exception e) {
																e.printStackTrace();
															}
														%>
													</select>

													<!-- <input type="text" name="text" class="form-control auto-complete" id="orderItem_commodity"/> -->
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

							<!-- End of Fourth column -->
						</div>


					</div>




				</div>
			</div>


		</section>


		<div class="row">

			<div class="col-xs-6 col-md-4"></div>
			<div class="col-xs-6 col-md-4"></div>
			<div class="col-xs-6 col-md-4">
				<div class="row" style="float: right; margin-right: 2%;">
					<button type="submit" data-type="horizontal"
						class="btn btn-primary btn_addmore" id="edit-item"
						name="singlebutton" style="border-radius: 5px; height: 35px;"
						onclick="handleEdit();">Edit</button>
					<button type="submit" data-type="horizontal"
						class="btn btn-primary btn_addmore" id="add-item"
						name="singlebutton" style="border-radius: 5px; height: 35px;"
						onclick="createBookingObj();">Add Item</button>
					<button type="submit" data-type="horizontal"
						class="btn btn-primary btn_addmore" id="update-item"
						name="singlebutton" style="border-radius: 5px; height: 35px;"
						onclick="bookingUpdate();">Update</button>
					<button type="submit" data-type="horizontal"
						class="btn btn-danger btn_addmore" id="cancel-update"
						name="singlebutton" style="border-radius: 5px; height: 35px;"
						onclick="handleEdit();">Cancel</button>
				</div>
			</div>
		</div>

		<hr>

		<div class="row row-margin-unset">
			<table style="" class="table">
				<thead style="">
					<tr>
						<th>Supplier</th>
						<th>PO No</th>
						<th>Material</th>
						<th>Colour</th>
						<th>Product Code/SKU</th>
						<th>HS Code</th>
						<th>Carton Qty</th>
						<th>Total Pcs</th>
						<th>Net Weight</th>
						<th>Gross Weight</th>
						<th>CBM</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="booking_table">


				</tbody>
			</table>
		</div>
		<div class="row" style="float: right; margin-right: 2%;">
			<button type="submit" class="btn btn-primary btn_addmore"
				id="btn-submit" name="singlebutton"
				style="border-radius: 5px; height: 35px; padding-top: 8px;">Submit</button>
			<!-- <button type="button" class="btn btn-danger btn_addmore"
				name="singlebutton"
				style="border-radius: 5px; height: 35px; padding-top: 8px;">Cancel</button> -->
		</div>
		<div style="clear: both;"></div>

	</div>

	<div role="tabpanel" class="tab-pane" id="POExcel">
		<section class="">
			<div class="container-fluid">

				<div id="second-tab">
					<div class="row second-tab-clone tab-direct-booking-well">
						<div class="row">
							<form:form action="processExcelData" method="post"
								commandName="fileData" enctype="multipart/form-data">
								<div class="col-md-12">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<div class="card">
											<span style="display: block; color: red;"
												id="fileUploadError">${fileUploadError }</span>
											<div class="card-body">

												<div>
													<div class="form-group">
														<input type="file" name="file" class="file">
														<div class="input-group col-xs-12">
															<span class="input-group-addon"><i class=""></i></span>
															<!--<form:input path="fileName" id="some-text"
												cssClass="form-control input-lg" />-->
															<input type="text" class="form-control input-lg" disabled
																placeholder="Upload PO Excel"
																style="border: 1px solid gray; border-top-right-radius: 0px !important; border-bottom-right-radius: 0px !important; margin-top: 4px !important; height: 38px; text-align: center;">
															<span class="input-group-btn">
																<button class="browse btn btn-primary input-lg"
																	type="button" style="height: 38px; text-align: center;">
																	<i class="glyphicon glyphicon-search"></i> Browse
																</button>
															</span><span style="display: inline;"><a
																href="${pageContext.request.contextPath}/download/xls/PO_UploadC.xlsx"
																data-toggle="tooltip" title="Download Sample File"><img
																	src="${pageContext.request.contextPath}/resources/images/excel.png"
																	style="height: 48px; width: auto; margin-left: 40%; margin-top: -35px;" /></a>
															</span>
														</div>
														<div style="margin-top: 20px;">


															<form:checkbox path="approved"
																cssStyle="display: inline-block; margin-left: 120px;" />
															<span>&nbsp;&nbsp;Upload as approved </span><input
																type="submit" class="btn btn-success" value="Upload"
																style="margin-left: 17px; width: 80px !important;">

														</div>
													</div>
												</div>

											</div>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>

							</form:form>
						</div>
						<div style="margin-bottom: 40px" class="col-md-9"></div>
					</div>
					<%-- 		</form:form> --%>
				</div>
			</div>



		</section>
	</div>

</div>

<!-- </div> -->





<script>
	$(document).on('click', '.browse', function() {
		var file = $(this).parent().parent().parent().find('.file');
		file.trigger('click');
	});
	$(document).on(
			'change',
			'.file',
			function() {
				$(this).parent().find('.form-control').val(
						$(this).val().replace(/C:\\fakepath\\/i, ''));
			});

	$('#myTab a').click(function(e) {

		if ($(this).attr('href').trim() == "#home") {

			$("#menu1").hide();
			$("#home").show();

		} else {

			$("#menu1").show();
			$("#home").hide();

		}
	});
</script>
<!-- ----------------------------- -->
<%@include file="footer_v2.jsp"%>