<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>

<!-- DataTables -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/poTracking.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>

<style>
html, body {
	background-color: #fff;
}
</style>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">PO Exception</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">PO Exception Report</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getPoExceptionReport" method="post"
			commandName="poTrackingParams" id="poTrack">
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>PO Number</label>
					<form:input path="doc_no" id="poNo" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>From Date</label>
					<form:input path="fromDate" id="fromDate"
						class="date-picker form-control glyphicon glyphicon-calendar" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>TO Date</label>
					<form:input path="toDate" id="toDate"
						class="date-picker form-control glyphicon glyphicon-calendar" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>Status Type</label>
					<form:select path="expStatusType" id="sType"
						cssClass="form-control">
						<form:option value="NONE">--- Select ---</form:option>
						<form:options items="${exceptionStatusTypes}" />
					</form:select>
				</div>
			</div>
			<!-- 		<div class="col-md-2 col-sm-12 col-xs-12" style="margin-top: 2%;">
							<div class='form-group'>
								<button type="submit" class="btn btn-success btn-lg form-control">Search</button>
							</div>
						</div>			-->
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label style="display: block; color: transparent">. </label>
					<!-- added for alignment purpose, dont delete -->
					<input type="submit" id="btn-appr-po-search"
						class="btn btn-success form-control" value="Search" />
				</div>
			</div>
		</form:form>
	</div>
	<hr>

	<div class="container-fluid table-responsive">
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<table class="table table-striped" id="poExceptionReportTable">
					<thead style="background-color: #222534; color: #fff;">
						<tr>
							<th>PO No</th>
							<th>Comm. Invoice No</th>
							<th>Goods Receive Date</th>
							<!--  <th>Shipment date</th>-->
							<th>PO Status</th>
							<th>Exception Status</th>
						</tr>
					</thead>
					<tbody id="accordion">
						<c:forEach items="${poTrackingResultBeanLst}" var="i"
							varStatus="index">
							<tr class="">
								<td style="font-size: 12px;">${i.po_no}<a
									data-toggle="collapse" href="#${index.count}"
									onclick="hideexpand(this);"><span id="#${index.count}"
										class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
								<td style="font-size: 12px;">${i.comm_invoice_no}</td>
								<fmt:parseDate value="${i.gr_date}" var="grDate"
									pattern="yyyyMMdd" />
								<td style="font-size: 12px;"><fmt:formatDate
										pattern="yyyy-MM-dd" type="date" value="${grDate}" /></td>
								<fmt:parseDate value="${i.shipment_date}" var="shipmentDate"
									pattern="yyyyMMdd" />
								<!--  <td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${shipmentDate}" /></td>-->
								<td style="font-size: 12px;">${i.poStatus}</td>
								<td style="font-size: 12px;">${i.shipmentStatus}</td>
							</tr>
							<tr class="hide">
								<td colspan="7">
									<div class="collapse" id="${index.count}">
										<table class="table table-striped"
											style="width: 80%; margin-left: 8%;">
											<th>HBL/AWB No</th>
											<th>Shipper</th>
											<th>Consignee</th>
											<th>Delivery Agent</th>
											<th>Carrier</th>

											<tr>
												<td style="font-size: 12px;"><a
													href="getTrackingInfoFrmUrl?searchString=${i.bl_no}">${i.bl_no}</a></td>
												<td style="font-size: 12px;">${i.shipper_name}</td>
												<td style="font-size: 12px;">${i.buyer_name}</td>
												<td style="font-size: 12px;">${i.agent_name}</td>
												<td style="font-size: 12px;">${i.carrier_name}</td>

											</tr>
											<th>Comm. Invoice No</th>
											<th>POL</th>
											<th>POD</th>
											<th>Total Pcs.</th>
											<th>Gross Wt.</th>

											<tr>
												<td style="font-size: 12px;">${i.comm_invoice_no}</td>
												<td style="font-size: 12px;">${i.pol_name}</td>
												<td style="font-size: 12px;">${i.pod_name}</td>
												<td style="font-size: 12px;">${i.tot_pcs}</td>
												<td style="font-size: 12px;">${i.gross_wt}</td>

											</tr>

											<th>Booking Date</th>
											<th>ETD</th>
											<th>ATD</th>
											<th>ETA</th>
											<th>ATA</th>

											<tr>
												<fmt:parseDate value="${i.booking_date}" var="bookingDate"
													pattern="yyyyMMdd" />
												<td style="font-size: 12px;"><fmt:formatDate
														pattern="yyyy-MM-dd" type="date" value="${bookingDate }" /></td>
												<td style="font-size: 12px;"><fmt:formatDate
														pattern="yyyy-MM-dd" type="date" value="${i.etd}" /></td>
												<td style="font-size: 12px;"><fmt:formatDate
														pattern="yyyy-MM-dd" type="date" value="${i.atd}" /></td>
												<td style="font-size: 12px;"><fmt:formatDate
														pattern="yyyy-MM-dd" type="date" value="${i.eta}" /></td>
												<td style="font-size: 12px;"><fmt:formatDate
														pattern="yyyy-MM-dd" type="date" value="${i.ata}" /></td>

											</tr>

										</table>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<%@include file="footer_v2_fixed.jsp"%>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<%-- <script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script> --%>
<%-- <script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script> --%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>


<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
	});
	 $('.date-picker').each(function() {
	      $(this).removeClass('hasDatepicker').datepicker({
	          dateFormat: "dd.mm.yy"
	      });
	  });
	/* $('#po_tables').DataTable({
		responsive : true,
		dom : 'Bfrtip',
		buttons : [ 'csv', 'excel'

		]
	}); */
	
	$(function() {
		$('#poExceptionReportTable').DataTable();
	});

	function hideexpand(varobj) {
		var res = varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');

		//var toggleicon = $myGroup.children('.collapsed');		   
		//alert(toggleicon);

		$($myGroup)
				.find('.collapse')
				.each(
						function() {
							//alert($(this).attr('id'));
							if (res[1] == $(this).attr('id')) {

								$(this).closest("tr").toggleClass("hide");
								$(this).toggle();

								//alert("entering");
								//alert(document.getElementById("myicon"));
								//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
								//alert(document.getElementById("myicon"));
								if (document.getElementById("#" + res[1]).className == "glyphicon glyphicon-chevron-down") {
									//alert("entering");
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-up";
								} else {
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-down";
								}

							} else {
								//alert($(this).attr('id'))
								//$(this).slideUp( "slow", "linear" )
								//$(this).closest("tr").addClass("hide");
								//$(this).hide();
							}

							//toggleicon.html("<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>");

						});

	}
</script>


