<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="description" content="Track your shipment">
    <meta name="keywords" content="MGH Logistics, Logistics, Freight Forwarding, MGH Group, Track your shipment, myshipment, shipment">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>myshipment</title>

    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700" rel="stylesheet">

    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/bootstrap/bootstrap.min.css">

    <!-- animate CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/styles.css">

    <!-- responsive CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">

	<!--fontawesome script-->
    <script src="https://use.fontawesome.com/fcba9d8315.js"></script>

</head>

<body data-spy="scroll" data-target=".navbar-fixed-top" data-offset="65">
    <div class="entire-content">
        <header>
            <nav class="navbar navbar-fixed-top">
                <div class="container-fluid">
                    <div class="myshipment-nav-wrapper">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myshipment-menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                       </button>
                            <a class="navbar-brand" href="#header">
                                <img src="${pageContext.request.contextPath}/resources/img/logo-xs.png" alt="logo">
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="myshipment-menu">
                            <ul class="nav navbar-nav">
                                <li><a class="child-0 smooth-scroll" href="#header">Home</a></li>
                                <li><a class="smooth-scroll" href="#order">Track</a></li>
                                <li><a class="smooth-scroll" href="#services">Services</a></li>
                                <li><a class="smooth-scroll" href="#accord">FAQ</a></li>
                                <li><a class="smooth-scroll" href="#app">App</a></li>
                                <li><a class="smooth-scroll" href="#contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>


        <!--HEADER-->

        <section id="header" style="background: url('${pageContext.request.contextPath}/resources/img/Login-background-23.jpg') center center no-repeat; background-size: cover;">
            <div class="container">
                <div class="row header-row">
                    <div class="col-sm-12">
                        <div class="col-sm-6 header-row-1">
                            <h2>DRIVEN BY INNOVATION...</h2>
                            <p class="lead">A cut above the rest, <strong>MGH</strong> offers a range of services like online booking, purchase-order management, tracking and tracing of goods, creation of reports etc. &mdash; all glued into one business framework called <strong>myshipment</strong>.</p>
                        </div>
                        <div class="col-sm-6 header-right">
                            <!--<img src="${pageContext.request.contextPath}/resources/img/logo-small.png" class="img-fluid second-img" />-->
                            <div class="col-sm-12">
                                <h4>Log in to <strong class="my">my<span class="shipment">shipment</span></strong></h4>
                                <h6><span class="incorrect-u-p text-xs-center;">&nbsp;Incorrect Username or Password!</span></h6>
                                <form:form action="${pageContext.request.contextPath}/selectedorg" modelAttribute="loginMdl" method="post">
                                    <label for="" class="errorMsg" style="color:red;">${error}</label>
                                    <fieldset class="form-group">
                                        <label class="sr-only">Username</label>
                                        <input type="text" class="form-control" id="customerCode" name="customerCode" placeholder="Case sensitive username...">
                                    </fieldset>
                                    <fieldset class="form-group">
                                        <label class="sr-only">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Case sensitive password...">
                                    </fieldset>
                                    <button type="submit" id="trackButton" name="trackButton" class="btn btn-info btn-block">Log In</button>
                                    <fieldset>
                                        <input type="checkbox" name="remember" id="remember" value="on"> <span class="check">Remember me!</span>
                                        <span class="checking">
                                        	<a href="${pageContext.request.contextPath}/forgetPasswordPage">Forgot Password</a>
                                        </span>
                                    </fieldset>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow animated bounce" data-wow-delay="0.2s">
                    <div class="col-md-12">
                        <a href="#order"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- END HEADER-->


        <!-- TRACK ORDER -->

        <section id="order">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s">
                        <h2>Track your shipment</h2>
                        <div class="content-title-underline-body"></div>
                        <h4>Want to track your order? Fill up the details in the panel below and search!</h4>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6 order-img wow fadeInLeft" data-wow-delay="0.4s">
                            <img class="img-responsive" src="${pageContext.request.contextPath}/resources/img/track.jpg">
                        </div>

                        <div class="col-md-6 order-lg track wow fadeInRight" data-wow-delay="0.4s">
                            <div class="col-md-12">
                                <p>In order to maintain transparency and provide flexibilty to its customers, myshipment facilitates real-time tracking of goods.Users can have an overview of their shipment status on the fly.</p>
                            </div>
                            <div class="col-md-12" id="track-form" style="background: url('${pageContext.request.contextPath}/resources/img/track-bg.jpg') center center no-repeat; background-size: cover;">
                                <h4>Track your order in <strong class="my">my<span class="shipment">shipment</span></strong></h4>
                                <form:form action="${pageContext.request.contextPath}/getCommonTrackingDetailInfo" method="post" commandName="trackingRequestParam" >
                                    <div class="form-group">
                                        <select name="action" class="form-control">
                     						<option value="01">Sea Export HBL</option>
											<option value="02">Sea Import HBL</option>
											<option value="04">Air Import HAWB</option>
                			 			</select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="blNo" placeholder="HBL Number" />
                                    </div>
                                    <button type="submit" id="trackButton" name="trackButton" class="btn btn-info btn-block">Track</button>
                                </form:form>
                            </div>
                        </div>
                        <p class="order-p wow fadeInRight" data-wow-delay="0.4s"><strong><i>For detailed view kindly <a href="#">login</a> and access the advanced  features.</i></strong></p>
                    </div>
                </div>
                
            </div>
            <div class="row wow animated bounce" data-wow-delay="1s">
            	<div class="col-md-11"></div>
            	<div class="col-md-1">
            		<a href="#why"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
            	</div>
            </div>
        </section>
        <!-- END TRACK ORDER -->


        <!-- WHY -->
        <section id="why">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.2s">
                        <h2>Why choose our service?</h2>
                        <div class="content-title-underline-body"></div>
                        <h4>Because we are the best at what we do!</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 wow fadeIn" data-wow-delay="0.4s">
                        <div class="col-md-12 why-fa">
                            <i class="fa fa-thumbs-up fa-3x" aria-hidden="true"></i>
                        </div>
                        <h3>Customer Satisfaction</h3>
                        <p>Delivering your load safe and on time is not our goal but our standard. Our customers can count on the same standard wherever they are, in terms of service quality, innovative and flexible solutions, transparency, and a smart and safe logistical process.</p>
                    </div>
                    <div class="col-md-6 wow fadeIn" data-wow-delay="0.6s">
                        <div class="col-md-12 why-fa">
                            <i class="fa fa-trophy fa-3x" aria-hidden="true"></i>
                        </div>
                        <h3>Clientele Range</h3>
                        <p>Trusted by more than 20,000 shippers, buyers, and delivery agents, we are one of the leading online logistics management services. Expanding our reach in more than eight nations around the globe, we have evolved into the de facto expert in this industry.</p>
                    </div>
                </div>
            </div>
            <div class="row wow animated bounce" data-wow-delay="0.2s">
            	<div class="col-md-11"></div>
            	<div class="col-md-1">
            		<a href="#services"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
            	</div>
            </div>
        </section>
        <!-- END WHY -->


        <!-- OUR SERVICES -->
        <section id="services" style="background: url('${pageContext.request.contextPath}/resources/img/Login-background.jpg') center center no-repeat; background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.2s">
                        <h2>Our services</h2>
                        <div class="content-title-underline-body"></div>
                        <h4>Read what our services have to offer...</h4>
                    </div>
                </div>
                <div class="row wow fadeInLeft" data-wow-delay="0.4s">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h3 class="card-title">Hassel-Free Booking</h3>
                                <hr width="50%">
                                <p class="card-text">To best support your ever changing logistics needs, we are continuously evolving our services starting from loading to unloading and leave nothing to chance. For your convenience, we have made the booking order and approval procedure easier and kept transparency throughout.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h3 class="card-title">Track & Trace</h3>
                                <hr width="50%">
                                <p class="card-text">We use state of the art computer technology to keep real-time track of your shipment for security and transparency from booking till delivery. You can retrieve the current status of your consignment simply by providing the MBL/HBL number, PO number or Commercial Invoice number.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12" style="height:20px;"></div>
                <div class="row wow fadeInRight" data-wow-delay="0.4s">
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h3 class="card-title">PO Management</h3>
                                <hr width="50%">
                                <p class="card-text">Provide multi-modal logistics solution designed for easier management of POs and invoices with options to assign your preferred carrier and schedule for product delivery and efficient management of the entire supply chain by authorising total end-to-end visibility.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h3 class="card-title">Reports & Dashboard</h3>
                                <hr width="50%">
                                <p class="card-text">With our easy to use consolidated dashboard, you can graphically visualize all your operations with clickable tabs and identify statistics, correlations and BL status. You can even generate and download several reports concerning vital stages of whole process.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row wow animated bounce" data-wow-delay="0.2s">
            	<div class="col-md-11"></div>
            	<div class="col-md-1">
            		<a href="#accord"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
            	</div>
            </div>
        </section>
        <!-- END SERVICES -->






        <!--ACCORDION-->
        <section id="accord">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInDown" data-wow-delay="0.2s">
                        <h2>FREQUENTLY ASKED QUESTIONS</h2>
                        <div class="content-title-underline-body"></div>
                        <h4>Answers to some common questions...<i class="fa fa-question-circle fa-x" aria-hidden="true"></i></h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist">
                            <div class="panel panel-default wow fadeInUp" data-wow-delay="0.2s">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                        <h4 class="panel-title">
                                            <span>Login</span>
                                            <i class="fa fa-plus-circle more-less bd-plus" aria-hidden="true"></i>
                                            <i class="fa fa-minus-circle more-less bd-minus" aria-hidden="true"></i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-content">
                                            <h4>How to create membership in myshipment?</h4>
                                            <p>myshipment user creation is a facility given only to the companies and individuals who have communicated to our sales/CRM personnel and have a business profile created within our system. Hence this is not a functionality supported by myshipment.</p>
                                        </div>
                                        <div class="panel-content">
                                            <h4>How to change password of your account?</h4>
                                            <p>After logging in click the settings icon on top right corner of the homepage and select the change password option from the settings list.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default wow fadeInUp" data-wow-delay="0.3s">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                        <h4 class="panel-title">
                                            <span>Booking</span>
                                            <i class="fa fa-plus-circle more-less" aria-hidden="true"></i>
                                            <i class="fa fa-minus-circle more-less" aria-hidden="true"></i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-content">
                                            <h4>How can I ensure my booking is confirmed?</h4>
                                            <p>Whenever a booking is successfully completed a success message is shown, furthermore an auto mail is sent to the user’s previously set email address. myshipment application also give a notification to the parties affiliated with the order.</p>
                                        </div>
                                        <div class="panel-content">
                                            <h4>How can I know my HBL number?</h4>
                                            <p>After successful completion of the order HBL number is provided in the success page. Moreover you can also find in the email sent for booking confirmation.</p>
                                        </div>
                                        <div class="panel-content">
                                            <h4>Why can’t I find a previously created order?</h4>
                                            <p>There could be two possible scenarios :</p>
                                            <ul>
                                                <li>You have not selected the proper company after logging in, simply change the company name where you made the order from the option bar in top right corner of the page.</li>
                                                <li>Another reason could be the wrong distribution channel (sea/air) and/or wrong division (export/import).</li>
                                            </ul>
                                        </div>
                                        <div class="panel-content">
                                            <h4>How can the previous shipments be found?</h4>
                                            <p>All the pervious orders made by the user can be found in Last N shipment under the Report management tab in the menu.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default wow fadeInUp" data-wow-delay="0.4s">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                        <h4 class="panel-title">
                                            <span>Tracking</span>
                                            <i class="fa fa-plus-circle more-less" aria-hidden="true"></i>
                                            <i class="fa fa-minus-circle more-less" aria-hidden="true"></i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-content">
                                            <h4>How can I track a shipment without the HBL number?</h4>
                                            <p>myshipment facilitates four different ways of tracking a shipment :</p>
                                            <ul>
                                                <li>PO tracking</li>
                                                <li>Commercial Invoice tracking</li>
                                                <li>HBL number-wise tracking</li>
                                                <li>MBL number-wise tracking</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default wow fadeInUp" data-wow-delay="0.5s">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                        <h4 class="panel-title">
                                            <span>Reports</span>
                                            <i class="fa fa-plus-circle more-less" aria-hidden="true"></i>
                                            <i class="fa fa-minus-circle more-less" aria-hidden="true"></i>
                                        </h4>
                                    </a>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-content">
                                            <h4>How can I print a report?</h4>
                                            <p>When the report is shown in the browser you will find download, print icons. You can either choose to directly download from the browser or you can download it in your local file system and then print</p>
                                        </div>
                                        <div class="panel-content">
                                            <h4>Why can’t I find the Non-negotiable bill of lading?</h4>
                                            <p>Non-negotiable bill of lading is not created until the shipment is done if your distribution channel is sea. If your channel is air Non-negotiable bill is created after the shipping order is made.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row wow animated bounce" data-wow-delay="0.2s">
            	<div class="col-md-11"></div>
            	<div class="col-md-1">
            		<a href="#app"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
            	</div>
            </div>
        </section>
        <!--END ACCORDION-->


        <!-- APP-->
        <section id="app" style="background: url('${pageContext.request.contextPath}/resources/img/testi-bg.png') center center no-repeat;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInDown" data-wow-delay="0.4s">
                        <h2>Get The App</h2>
                        <div class="content-title-underline-body"></div>
                        <h4>We care for users on the go; which means apps at finger tips!</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 app-img wow fadeInUp" data-wow-delay="0.6s">
                        <img src="${pageContext.request.contextPath}/resources/img/iphone-img-2.png" class="phone-img">
                    </div>
                    <div class="col-sm-8 wow fadeInRight" data-wow-delay="0.8s">
                        <div class="col-sm-12 height-1"></div>
                        <div class="col-sm-12">
                            <p><i class="fa fa-cogs fa-2x" aria-hidden="true"></i></p>
                            <div class="app" style="background: url('${pageContext.request.contextPath}/resources/img/testi-bg.png') center center no-repeat; background-size: cover;">
                                <h4>All-Round Tracking App</h4>
                                <p>Considering the convenience of customers, we have brought myshipment at arm’s length. myshipment Mobile Application provides a more flexible experience with 24/7 availability irrespective of location.</p>
                            </div>
                        </div>
                        <div class="col-sm-12 height-2"></div>
                        <div class="col-sm-12">
                            <p><i class="fa fa-location-arrow fa-2x" aria-hidden="true"></i></p>
                            <div class="app">
                                <h4>Your Own Handy Tool</h4>
                                <p>By creating visibility, no matter where you are, presently it is easy as ever to track shipment with our new global mobile device application.</p>
                            </div>
                        </div>
                        <div class="col-sm-12 height-2"></div>
                        <div class="col-sm-12">
                            <a href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-A.png" class="img-fluid" title="Get On App Store"></a>
                            <a href="https://play.google.com/store/apps/details?id=com.mgh.shipment" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-G.png" class="img-fluid" title="Get On Google Play"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row wow animated bounce" data-wow-delay="0.2s">
            	<div class="col-md-11"></div>
            	<div class="col-md-1">
            		<a href="#contact"><i class="fa fa-chevron-down fa-3x" aria-hidden="true"></i></a>
            	</div>
            </div>
        </section>


        <!-- CONTACT -->
        <section id="contact">
            <div class="container wow zoomIn" data-wow-delay="0.3s">
                <div class="row">
                    <div class="col-md-12">
                        <h2>Contact us</h2>
                        <div class="content-title-underline-body"></div>
                        <h5>Need to get in touch? Fill up the form below for more information:</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form>
                                <fieldset class="form-group">
                                    <input type="text" class="form-control form-control-sm" id="name" placeholder="Name..." required>
                                </fieldset>
                                <fieldset class="form-group">
                                    <input type="email" class="form-control form-control-sm" id="email" placeholder="E-mail..." required>
                                </fieldset>
                                <fieldset class="form-group">
                                    <textarea class="form-control form-control-sm" id="message" placeholder="Message..." rows="3" required></textarea>
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </form>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END CONTACT -->


        <!-- FOOTER -->
        <section id="footer" style="background: url('${pageContext.request.contextPath}/resources/img/topheaderBG.jpg') center center no-repeat; background-size: cover;">
            <div class="container wow animated fadeInUp" data-wow-delay="0.4s" data-wow-duration="1.5s">
                <div class="row footer-top">
                    <div class="col-sm-4 footer-follow">
                        <h5>Follow Us :</h5>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="https://www.facebook.com/mghgroupglobal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.linkedin.com/company/mghgroup" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 footer-copy">
                        <p class="">
                            &copy; myshipment.com
                            <script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
                        </p>
                    </div>
                    <div class="col-sm-5 text-xs-center footer-app">
                        <a href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-AF.png" class="img-fluid" title="Get On App Store"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.mgh.shipment" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-GF.png" class="img-fluid" title="Get On Google Play"></a>
                    </div>
                </div>
            </div>
        </section>

        <!-- END FOOTER -->

        <!-- jQuery -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery.js"></script>

        <!-- bootstrap JS -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/bootstrap.min.js"></script>



        <!-- WOW JS -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/wow.min.js"></script>


        <!-- custom JS -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/custom.js"></script>

        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery.js"></script>
        
        <!-- easing -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/easing/jquery.easing.1.3.js"></script>

        

    </div>
</body>

</html>
