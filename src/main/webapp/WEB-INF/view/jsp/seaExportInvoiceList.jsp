
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
html {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>


<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Freight Invoice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Freight Invoice</li>
		</ol>
	</div>
	<hr>
	<div class="content">
		<div class="row">
			

			<div class="row">
				<section class="main-body">
					<!-- <div class="container-fluid table-responsive"> -->
					<div class="container">
						<!-- 		<div class="row table_header">

			<h3>Sea Export Invoice List</h3>

		</div> -->
					<c:if test="${empty invoiceListJsonData.invoice}">
							<!-- <div style="height: 50px; width: 200px; font-weight: bold; color: red; 
							margin-left: 43%; padding-top: 10px;">No Record found!</div> -->
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="text-center" style="height: 50px; color: red; font-weight: bold; margin-top: 10%;">
										No Record found!
									</div>
									<div class="text-center" style="">
										<a class="btn btn-primary" href="${pageContext.request.contextPath}/getSeaExportInvoice" class="">Search Again</a>
									</div>
									<br><br><br><br><br>
								</div>
								<div class="col-md-4"></div>
								
							</div>
						</c:if>
					<c:if test="${not empty invoiceListJsonData.invoice}">
					
						<div class="row" style="height: 20px;"></div>
						<div class="panel-group">
							<div class="panel panel-primary">
								<div class="panel-heading" style="font-size: 20px;">Sea
									Export Invoice List</div>

								<div class="panel-body">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Invoice Number</th>
												<th>Type</th>
											</tr>
										</thead>
										<tbody>
											<tr class="">
												<c:forEach items="${invoiceListJsonData.invoice}" var="i">
													<tr>
														<td><c:url value="/getSeaExportInvoiceDetail"
																var="invoiceDetail">
																<c:param name="param" value="${i.vbeln}" />
															</c:url> <a href="<c:out value="${invoiceDetail}"/>"
															target="blank">${i.vbeln}</a></td>
														<c:if test="${i.knumv =='ZSCM'}">
															<td>Invoice</td>
														</c:if>
														<c:if test="${i.knumv !='ZSCM'}">
															<td>Debit Note</td>
														</c:if>
													</tr>
												</c:forEach>
											</tr>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</c:if>
						<%-- 		<div class="row col-xs-6">
			<table class="table table-striped">
				<thead style="">
					<tr>
						<th>Invoice Number</th>
						<th>Type</th>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<c:forEach items="${invoiceListJsonData.invoice}" var="i">
							<tr>
								<td><c:url value="/getSeaExportInvoiceDetail"
										var="invoiceDetail">
										<c:param name="param" value="${i.vbeln}" />
									</c:url> <a href="<c:out value="${invoiceDetail}"/>" target="blank">${i.vbeln}</a></td>
								<c:if test="${i.knumv =='ZSCM'}">
									<td>Invoice</td>
								</c:if>
								<c:if test="${i.knumv !='ZSCM'}">
									<td>Debit Note</td>
								</c:if>
							</tr>
						</c:forEach>
					</tr>

				</tbody>
			</table>
			<c:if test="${invoiceListJsonData ==null }">
				<div
					style="height: 50px; width: 200px; color: red; margin-left: 43%; padding-top: 10px;">No
					Record found!</div>
			</c:if>
		</div> --%>

					</div>

				</section>
			</div>
			<br>
		</div>
	</div>
</div>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>


