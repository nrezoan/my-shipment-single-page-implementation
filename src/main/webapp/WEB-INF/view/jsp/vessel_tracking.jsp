<%@include file="header.jsp" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/shiptracking.css">
<style type="text/css">
            #map {height:75vH;}
            .icon-flipped {
              transform: scaleX(-1);
              -moz-transform: scaleX(-1);
              -webkit-transform: scaleX(-1);
              -ms-transform: scaleX(-1);
          }
        </style>
                       
<!--START OF THE SHIP TRACKING CODE-->       
 <div class="container ship-container-one">
        <div class="row">
        <div class="col-md-12">
            <img src="img/Flag_of_Bangladesh.png" height="50" width="50" alt="Flag of Bangladesh" title="Bangladesh" style="padding-top: 0px;"/>
            <h1 style="color: #fff;margin-top:10px;">CMA CGM CHRISTOPHER BANGLADESH</h1>
        </div>
     </div>
    </div>
        
 <div class="container ship-container-two">
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
            <div class="col-md-12 ship-container-three">
            <div class="col-md-6" style="border-right: 1px dotted #000;width: 50%;float:left;">
			<div>
				    <span>IMO: </span>
					<b>9453559</b>
            </div>

            <div>
					<span>MMSI: </span>
					<b>228315600</b>
            </div>
			
			<div>
				<span>Call Sign: </span>
				<b>FNUY</b>
			</div>
                
			<div>
				<span>Flag: </span>
				<b>Bangladesh [BD]</b>
			</div>
                
			<div>
				<span>AIS Vessel Type: </span>
				<b>Cargo - Hazard A (Major)</b>
			</div>
		</div>
		<div class="col-md-6">
			<div>
				<span>Gross Tonnage: </span>
				<b>153022</b>
			</div>
			<div>
				<span>Deadweight: </span>
				<b>165375 t</b>
			</div>
			<div>
				<span>Length Overall x Breadth Extreme: </span>
				<b>365.5m &times; 51.2m</b>
			</div>
			<div>
				<span>Year Built: </span>
				<b>2009</b>
			</div>
			<div>
				<span>Status: </span>
				<b>Active</b>
			</div>
		</div>
	</div>
            
            <div class="col-md-12 ship-container-four">
                <header class="panel-heading">
				<h2 class="text-left">Voyage Info</h2>
			    </header>

			<div class="panel-body text-left">
            <div class="action-and-more">
			<div class="padding-10">
				<div class="row vertical-offset-7">
				<div class="col-xs-6">
				<div >
                    <span style="font-weight: bold">JEA</span>
                    <span>AE</span>
                </div>
                <div>
                    <span style="font-weight: bold">ATD :</span>
                    <span>2017-1-18</span>
                </div>
				
				</div>
				<div class="col-xs-6">
                <div style="float:right;">
				<div style="text-align: right;">
                    <span>CN</span>
                    <span style="font-weight: bold">YAN</span>
                </div>
                    <div class="clear:left;"></div>
                <div>
                    <span style="font-weight: bold;">ETA :</span>
                    <span>2017-1-26</span>
                </div>
                </div>
				
				</div>
				</div>
			</div>
                
                <div class="col-md-12">
                <div class="w3-container-fluid">
                <span class="glyphicon glyphicon-flag" style="color:green;"></span>
                <span class="glyphicon glyphicon-flag icon-flipped" style="color:green;float: right;"></span>
                <div class="w3-progress-container w3-light-blue">
                <div class="w3-progressbar w3-blue" style="width:75%">
                <div class="w3-center w3-text-white">75%</div>
                </div>
                </div>
                </div>
                </div>
                
                </div>
                </div>
               
            </div>
                
                <div class="col-md-12 ship-container-five">
                <div class="col-md-6" style="border-right: 1px dotted #000;">
                 <p>Distance Travelled</p>
                </div>
                     <div class="col-md-6">
                        <p>1500 <span style="font-weight: bold;">km</span></p>
                    </div>
                </div>
                <div class="col-md-12 ship-container-six">
                    <div class="col-md-6" style="border-right: 1px dotted #000;">
                 <p>Distance to Go</p>
                    </div>
                    <div class="col-md-6">
                        <p>2500 <span style="font-weight: bold;">km</span></p>
                    </div>
                </div>
                <div class="col-md-12 ship-container-five">
                    <div class="col-md-6" style="border-right: 1px dotted #000;">
                 <p>Total Voyage Distance</p>
                    </div>
                     <div class="col-md-6">
                        <p>4000 <span style="font-weight: bold;">km</span></p>
                    </div>
                </div>
                <div class="col-md-12 ship-container-six">
                    <div class="col-md-6" style="border-right: 1px dotted #000;">
                 <p>Time to Destination</p>
                    </div>
                     <div class="col-md-6">
                        <p>20 <span style="font-weight: bold;">Days</span></p>
                    </div>
                </div>
                <div class="col-md-12" style="background-color: #000;">
                 <p></p>
                </div>
                <div class="col-md-12 ship-container-seven">
                    <div class="col-md-6" style="border-right: 1px dotted #000;">
                 <p>Draught</p>
                    </div>
                     <div class="col-md-6">
                        <p><span style="font-weight: bold;">15m</span></p>
                    </div>
                </div>
        </div>
            
            <div class="col-md-6">
            <div class="col-md-12 map-div">
            <div id="map"></div>

	<script src="script.js"></script>
	<script async defer 
					src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOQZgqHOTsyNF75N_MaINhyawixCjAiXQ&callback=initMap"></script>
            </div>
            </div>
     </div>
    </div>
    </div>
    
     <script>
      function initMap() {
       var arr = [
[22.3475365, 91.81233240000006, '0'],
[21.572003743013017, 91.02272730320692, '0'],
[21.27541465041965, 89.97902613133192, '0'],
[20.639326938255227, 88.84743433445692, '0'],
[19.8766347445236, 88.22121363133192, '0'],
[19.01680020506702, 86.71608667820692, '0'],
[18.03762262003212, 85.16701441258192, '0'],
[17.619274257849632, 83.27736597508192, '0'],
[16.663941332895853, 83.3103246241808, '0'],
[15.587445225055001, 82.71706357598305, '0'],
[14.217915888294934, 81.95900693535805, '0'],
[13.032777113598527, 80.28908506035805, '0'],
[12.035413909199692, 81.70632138848305, '0'],
[11.088246008404077, 83.33229795098305, '0'],
[10.040651706594932, 84.84841123223305, '0'],
[8.674829266358788, 86.34255185723305, '0'],
[7.641705072416416, 87.81471982598305, '0'],
[6.922460837366598, 90.58327451348305, '0'],
[6.7479290063577935, 93.82424131035805, '0'],
[6.049188405817341, 97.84523740410805, '0'],
[3.7396846404360518, 100.0425023585558, '0'],
[2.280558679789955, 101.65749326348305, '0'],
[1.3252323745117338, 103.86574521660805, '0'],
]
      function initialize() {
        var mapOptions = {
          zoom: 5,
          center: new google.maps.LatLng(22.3475365, 91.81233240000006),
          mapTypeId: google.maps.MapTypeId.OCEANMAP
        };
        var map = new google.maps.Map(document.getElementById('map'),
            mapOptions);
        var markerB=new google.maps.Marker({icon:'img/Ship13.png'});
        var line=new google.maps.Polyline(
              {
                map:map,
                icons: [{
                          icon: {
                                  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                                  strokeColor:'#009933',
                                  fillColor:'#009933',
                                  fillOpacity:1
                                },
                          //repeat:'100px'
                       }]
                 });
          
          //simulate incoming new positions every 2 seconds
          timer=setInterval(function(){
              var item=arr.shift();
              if(!arr.length){clearInterval(timer);}
              var path=line.getPath().getArray(),
              latLng=new google.maps.LatLng(item[0],item[1]);
              path.push(latLng);
                   map.setCenter(latLng);
                  line.setPath(path);
                  if(path.length==1){
                     new google.maps.Marker({map:map,position:latLng,icon:'img/Location_marker_pin_map_gps.png'});
                  }else{
                     markerB.setOptions({map:map,position:latLng})
                  }
          },2000);
      }

      google.maps.event.addDomListener(window, 'load', initialize);
      }
    </script>   
        
        
        <!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
		<!-- main JS
		============================================ -->		
        <script src="js/main.js"></script>
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            function myFunction2(){
                      document.getElementById("ship").style.backgroundColor = "#a5a5a5";
                      document.getElementById("plane").style.backgroundColor = "#fff";
                      document.getElementById("sea-text").innerHTML = "SEA";
                      document.getElementById("air-text").innerHTML = "AIR";
          }
            
             function myFunction1(){
                      document.getElementById("plane").style.backgroundColor = "#a5a5a5";
                      document.getElementById("ship").style.backgroundColor = "#fff";
                      document.getElementById("air-text").innerHTML = "AIR";
                      document.getElementById("sea-text").innerHTML = "SEA";
          }
        </script>
        <%@include file="footer.jsp"%>
    </body>
</html>