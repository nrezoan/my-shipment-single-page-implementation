<!doctype html>
<%@page import="com.myshipment.dto.LoginDTO"%>
<%@page import="com.google.gson.Gson"%>
<%@page errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html class="no-js" lang="">
    <head>
    <%-- <% LoginDTO loginDto1=(LoginDTO) session.getAttribute("loginDetails"); %> --%>
    <style>
 		@media print {		.no-print {display: none !important;}} 
</style> 

	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sweetalert.css">  
	
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-2.2.2.min.js"></script>
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/sweetalert.min.js"></script>
     <script type="text/javascript">
     var myContextPath = "${pageContext.request.contextPath}";
     var selectedColor = "#30c5ef";
     var notSelectedColor = "#fff";
     </script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
    var loginDetails = <%= new Gson().toJson(session.getAttribute("loginDetails")) %>;
    $(window).load(function(){
    	//$.LoadingOverlay("show");
		
		if(loginDetails != undefined || loginDetails != null || loginDetails != "") {
			menu(loginDetails);
		}
		

    	/* $.ajax({
    		url: myContextPath + '/headerdetails',
    		type: "GET",
    		dataType : "json",
    		success:function(data1),
    	error:function( jqXHR, textStatus, errorThrown ){
    		$.LoadingOverlay("hide");
    	}
    	}) */
    	
    	$("button[name='btnChnage']").click(function(){
    		var salesOrg=$("#salesOrgdrpDwn").val();
    		var distChannel=$(".btn_radio").find('input:checked').val(); 
    		var division=null;
    		//alert($("#air-text").text());
    		//alert(rgb2hex(document.getElementById("ship").style.backgroundColor));
    		//alert(rgb2hex(document.getElementById("plane").style.backgroundColor));
    		
    		if($("#air-text").text()=='AIR' && rgb2hex(document.getElementById("plane").style.backgroundColor)=='#ffffff')
    			division=$("#air-text").text();
    		else
    			division=$("#sea-text").text();
    		$.ajax({
    			url: myContextPath + '/changebusiness',
        		type: "POST",
        		data:{salesOrg:salesOrg,
        			distChannel:distChannel,
        			division:division
        			},
        		
        		success:function(data, textStatus, xhr){
        			window.location=xhr.getResponseHeader("Location");
        		}
    		})
    		
    	})
    	
    	
    	//sea click
    	$("#ship").click(function(){
    		document.getElementById("plane").style.backgroundColor = notSelectedColor;
            document.getElementById("ship").style.backgroundColor = selectedColor;
            document.getElementById("air-text").innerHTML = "AIR";
            document.getElementById("sea-text").innerHTML = "SEA";
            
    		var salesOrg=$("#salesOrgdrpDwn").val();
    		var distChannel=$(".btn_radio").find('input:checked').val(); 
    		var division=null;
    		division=$("#sea-text").text();
    		$.ajax({
    			url: myContextPath + '/changebusiness',
        		type: "POST",
        		data:{salesOrg:salesOrg,
        			distChannel:distChannel,
        			division:division
        			},
        		
        		success:function(data, textStatus, xhr){
        			$.LoadingOverlay("show");
        			//window.location=xhr.getResponseHeader("Location");
        			window.location.reload();
        			$.LoadingOverlay("hide");
        		}
    		})
    		
    	})
    	

    	//air click
    	$("#plane").click(function(){
    		document.getElementById("ship").style.backgroundColor = notSelectedColor;
            document.getElementById("plane").style.backgroundColor = selectedColor;
            document.getElementById("sea-text").innerHTML = "SEA";
            document.getElementById("air-text").innerHTML = "AIR";
    		var salesOrg=$("#salesOrgdrpDwn").val();
    		var distChannel=$(".btn_radio").find('input:checked').val(); 
    		var division=null;
    		division=$("#air-text").text();
    		$.ajax({
    			url: myContextPath + '/changebusiness',
        		type: "POST",
        		data:{salesOrg:salesOrg,
        			distChannel:distChannel,
        			division:division
        			},
        		
        		success:function(data, textStatus, xhr){
        			$.LoadingOverlay("show");
        			//window.location=xhr.getResponseHeader("Location");
        			window.location.reload();
        			$.LoadingOverlay("hide");
        		}
    		})
    		
    	})
    	

    	//
    	
    	
    })
    
    function changeCompany() {
    	var salesOrg=$("#salesOrgdrpDwn").val();
		var distChannel=$(".btn_radio").find('input:checked').val(); 
		var division=null;
		//alert($("#air-text").text());
		//alert(rgb2hex(document.getElementById("ship").style.backgroundColor));
		//alert(rgb2hex(document.getElementById("plane").style.backgroundColor));
		
		if($("#air-text").text()=='AIR' && rgb2hex(document.getElementById("plane").style.backgroundColor)==selectedColor)
			division=$("#air-text").text();
		else
			division=$("#sea-text").text();
		$.ajax({
			url: myContextPath + '/changebusiness',
    		type: "POST",
    		data:{salesOrg:salesOrg,
    			distChannel:distChannel,
    			division:division
    			},
    		
    		success:function(data, textStatus, xhr){
    			//window.location=xhr.getResponseHeader("Location");
    			window.location.reload();
    		}
		})
    }
    
    function menu(data) {
		<%-- var loginData = document.getElementById("loginData");
		console.log("Login Data : " + loginData);
		var newData = '<%= loginDto %>';
		console.log("Data : " + newData.mpSalesOrgTree); --%>
		var division='';
		var $option='';
	$.each(data.mpSalesOrgTree,function(index,value){
		//company name list-->which one is selected and add the other list to the options
		var salesOrg=value;
		if(salesOrg.salesOrg==data.salesOrgSelected)
		 $option=$option+'<option value="'+salesOrg.salesOrg+'" selected >'+salesOrg.salesOrgText+'</option>';
		else
			$option=$option+'<option value="'+salesOrg.salesOrg+'" >'+salesOrg.salesOrgText+'</option>';	
			
	})
	$("#salesOrgdrpDwn").append($option);
	$.each(data.mpSalesOrgTree,function(index,value){
		
		if(value.salesOrg==data.salesOrgSelected)
			{
			var distChnlLst=value.lstDistChnl;
			var Ex='';
			var Im='';
			var cha=''
			$.each(distChnlLst,function(index,value)
					{
				if(value.distrChan==data.disChnlSelected)
					division=value.lstDivision;
				var disChnl=value;
				if(disChnl.distrChan=='EX')
					{
					Ex=disChnl.distrChan;
					}
				if(disChnl.distrChan=='IM')
				{
				Im=disChnl.distrChan;
				}
				if(disChnl.distrChan=='CHA')
				{
					cha=disChnl.distrChan;
				}
				
					})
					
				if(Ex=='EX')
					{
					if(data.disChnlSelected=='EX')
						$(".btn_radio").append('<p class="exp">Export</p><input class="export-radio" type="radio" name="radio" value="'+Ex+'" checked/><br>');
					else
						$(".btn_radio").append('<p class="exp">Export</p><input class="export-radio" type="radio" name="radio" value="'+Ex+'" /><br>');
					}
				else 
					{
					$(".btn_radio").append('<p class="exp">Export</p><input class="export-radio" type="radio" name="radio" value="" disabled/><br>');
					}
			
			if(Im=='IM')
			{
			if(data.disChnlSelected=='IM')
				$(".btn_radio").append('<p class="imp">Import</p><input class="import-radio" type="radio" name="radio" value="'+Im+'" checked/><br>');
			else
				$(".btn_radio").append('<p class="imp">Import</p><input class="import-radio" type="radio" name="radio" value="'+Im+'" /><br>');
			}
			else
			{
				$(".btn_radio").append('<p class="imp">Import</p><input class="import-radio" type="radio" name="radio" value="" disabled/><br>');
				}
			//$(".btn_radio").append('<div class="vertical-line"></div><br>');
			if(cha=='CHA')
			{
			if(data.disChnlSelected=='CHA')
				$(".btn_radio").append('<p class="cha">Cha</p><input class="cha-radio" type="radio" name="radio" value="'+cha+'" checked/><br>');
			else
				$(".btn_radio").append('<p class="cha">Cha</p><input class="cha-radio" type="radio" name="radio" value="'+cha+'" /><br>');
			}
			else
			{
				$(".btn_radio").append('<p class="cha">Cha</p><input class="cha-radio" type="radio" name="radio" value="" disabled /><br>');
				}
			}
	})
	if(division.length==1){
		$.each(division,function(index,value){
		if(value.division=='SE')
		{
		$("#division-air").removeAttr("style");
		if(data.divisionSelected==value.division)
		myFunction3();
		}
	if(value.division=='AR')
		{
		$("#division-sea").removeAttr("style");
		if(data.divisionSelected==value.division)
		myFunction4();
		}
		})}
	if(division.length>1){
		
	
	$.each(division,function(index,value){
		if(value.division=='SE')
			{
			$("#division-sea").removeAttr("style");
			if(data.divisionSelected==value.division)
			myFunction1();
			}
		if(value.division=='AR')
			{
			$("#division-air").removeAttr("style");
			if(data.divisionSelected==value.division)
			myFunction2();
			}
		if(data.divisionSelected==null && division.length==2)
			myFunction1();
	})
	}
	$(".user-text").find("b").text(data.peAddress.name);
	renderMenuItems(data);
	//$.LoadingOverlay("hide");
}
    
   function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
    
    </script>
    <script>
  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  		ga('create', 'UA-90249127-1', 'auto');
  		ga('send', 'pageview');
	</script>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>My Shipment</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       	<link href='${pageContext.request.contextPath}/resources/css/fonts.css' type='text/css' rel="stylesheet">
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dashboard.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
		<!-- modernizr JS
		============================================ -->	
		 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/graph.css">
		<!-- modernizr JS
		============================================	-->	
                	
        <script src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
                
               
    </head>
    <body>
    <%-- <input type="hidden" id="loginData" value="${loginDto} /> --%>
		<header class="header-area no-print">
			<!-- <div class="container"> -->
			<div class="header">
				<!-- div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="logo">-->
						<div class="header-1">
							<a href="${pageContext.request.contextPath}\homepage"><img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="Logo"></a>
						</div>
					<!-- </div> -->
					<!-- <div class="col-sm-12 col-md-3 col-lg-3 search-form"> -->
					<div class="header-2">
						<!-- <div class="first"> -->
							 <form action="getTrackingDetailInfo" method="post" > 
								<!-- <div class="form-group">
									<div class="input-group"> -->
										<input type="text" class="form-control" id="exampleInputAmount" name="blNo" placeholder="Track Shipment By HBL No">
									<!-- 	<div class="input-group-addon"> -->
											<button type="submit" class="btn btn-primary">Go</button>
									<!-- 	</div> -->
									<!-- </div>
								</div> -->
							 </form> 
						</div>
					<!-- </div> -->
				<!-- 	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 second"> -->
					<div class="header-3">
					
						<div class="btn_radio header-radio radio_text"  style="margin-left:0%;" id="radio_text">
						</div>
						
                        <div class="form-field-change">
                <p style="display: inline;"><img src="${pageContext.request.contextPath}/resources/images/arrow-header-image.png" style=""></p>
						<!-- <div class="search-form"> -->
							<form action="#"   id="form_search" style="margin-top:5px;">
								<div class="form-group form-field-change-group">
									<div class="input-group" class="header-4">
										<select class="form-control header-control" name="salesOrgdrpDwn" id="salesOrgdrpDwn" onchange="changeCompany()">
									
									
											
										</select>
										
											<!-- <button type="button" class="btn btn-primary button-change" name="btnChnage">Change</button> -->
										
									</div>
								</div>
							</form>
						 </div> 
						
					</div>
				
			</div>
		</header>
		<div class="welcome-area">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
                     <section class="container">
                        <div class="row-fluid">
                        <div class="row-fluid">
                        <div class="span6" id="division-air" style="display:none;">
                        <div id="ship" onclick="myFunction1()">
                        <img src="${pageContext.request.contextPath}/resources/images/ship.png" class="img-responsive" /><p id="sea-text"><strong>SEA</strong></p>
                        </div>
                        </div>
                        
                        <div class="span6" id="division-sea" style="display:none;">
                        <div id="plane" onclick="myFunction2()">
                        <img src="${pageContext.request.contextPath}/resources/images/plane.png" class="img-responsive" /><p id="air-text"><strong>AIR</strong></p>
                        </div>
                        </div>
                        </div>
                        </div>
                     </section>
					</div>
					<div class="col-md-8">
						<div class="user-text">
							<p style="text-transform:capitalize; font-size: small;"> welcome <b ></b> <a href="${pageContext.request.contextPath}/editPasswordPage"><img src="${pageContext.request.contextPath}/resources/images/question.png" alt="Help" title="Help"></a> <a href=""><img src="${pageContext.request.contextPath}/resources/images/settings.png" alt="Settings" title="Settings" ></a>
							<a href="${pageContext.request.contextPath}/logout"><img src="${pageContext.request.contextPath}/resources/images/log_off.png" alt="Logout" title="Logout" style="width: 40px; height: auto; cursor: pointer;margin-left: 5px;"></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="menu.jsp"></jsp:include>
		<script>
            function myFunction2(){
                      document.getElementById("ship").style.backgroundColor = notSelectedColor;
                      document.getElementById("plane").style.backgroundColor = selectedColor;
                      document.getElementById("sea-text").innerHTML = "SEA";
                      document.getElementById("air-text").innerHTML = "AIR";
                  	
          }
            
             function myFunction1(){
                      document.getElementById("plane").style.backgroundColor = notSelectedColor;
                      document.getElementById("ship").style.backgroundColor = selectedColor;
                      document.getElementById("air-text").innerHTML = "AIR";
                      document.getElementById("sea-text").innerHTML = "SEA";
          }
             function myFunction3(){
                 document.getElementById("plane").style.backgroundColor = notSelectedColor;
                 document.getElementById("ship").style.backgroundColor = selectedColor;
                 document.getElementById("air-text").innerHTML = "";
                 document.getElementById("sea-text").innerHTML = "SEA";
     	 }
             function myFunction4(){
                 document.getElementById("plane").style.backgroundColor = notSelectedColor;
                 document.getElementById("ship").style.backgroundColor = selectedColor;
                 document.getElementById("air-text").innerHTML = "";
                 document.getElementById("sea-text").innerHTML = "AIR";
     }


             /* function myFunction2(){
                 document.getElementById("ship").style.backgroundColor = "#a5a5a5";
                 document.getElementById("plane").style.backgroundColor = "#fff";
                 document.getElementById("sea-text").innerHTML = "SEA";
                 document.getElementById("air-text").innerHTML = "AIR";
                 
                 
                 
               
	     }
	       
	        function myFunction1(){
	                 document.getElementById("plane").style.backgroundColor = "#a5a5a5";
	                 document.getElementById("ship").style.backgroundColor = "#fff";
	                 document.getElementById("air-text").innerHTML = "AIR";
	                 document.getElementById("sea-text").innerHTML = "SEA";
	     }
	        function myFunction3(){
	            document.getElementById("plane").style.backgroundColor = "#a5a5a5";
	            document.getElementById("ship").style.backgroundColor = "#fff";
	            document.getElementById("air-text").innerHTML = "";
	            document.getElementById("sea-text").innerHTML = "SEA";
		 }
	        function myFunction4(){
	            document.getElementById("plane").style.backgroundColor = "#a5a5a5";
	            document.getElementById("ship").style.backgroundColor = "#fff";
	            document.getElementById("air-text").innerHTML = "";
	            document.getElementById("sea-text").innerHTML = "AIR";
	} */
        </script>