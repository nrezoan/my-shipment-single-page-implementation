<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">
<!-- <script
	src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>






<style>
html, body {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Buyer wise Shipper List</h1>
	</div>
	<div class="row row-without-margin">
		<div class="container-fluid table-responsive">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x">
					<table class="table table-striped table-hover display"
						id="shipperTable">
						<thead style="background-color: #222534; color: #fff;">
							<tr>
								<th>Shipper Name</th>
								<th>Shipper Value</th>

							</tr>
						</thead>
						<tbody>

							<c:forEach items="${shipper}" var="i">
								<tr>
									<td>${i.value}</td>
									<td>${i.key}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="footer_v2.jsp"></jsp:include>
<script>
	$(document).ready(
			function() {
				var table2 = $('#shipperTable').DataTable({
					lengthChange : false,
					buttons : [ 'excel' ]
				});

				table2.buttons().container().appendTo(
						'#shipperTable_wrapper .col-sm-6:eq(0)');
			});
</script>
