<div id="mbl" class="tab-pane fade in active">
    <!-- <div id="headerItem" class="tab-pane fade"> -->
    <!-- hamid -->
    <div class="row">
        <div class="col-md-12">
            <span class="booking-update-information-header">Performance Report Log</span>
            <hr>
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">HBL/HAWB No.</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" value="2500" class="form-control" id="hbl" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group'>
                    <div class="col-md-12">
                        <label class="text-center">GHA</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="gha" value="2500" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group'>
                    <div class="col-md-12">
                        <label class="text-center">Origin</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="origin" value="2500" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group'>
                    <div class="col-md-12">
                        <label class="text-center">Destination</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="destination" value="2500" class="form-control" readonly/>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Total Pieces</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="totalPieces" value="2500" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Weight</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="weight" value="12" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Chargeable Weight</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="chargeableWeight" value="120" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">CBM</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="cbm" value="34" class="form-control" readonly/>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Type of Shipment</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="typeOfShipment" value="Standard" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Cargo Handover Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="cargoHandoverDate" onkeydown="return false" class="form-control date-picker"
                            readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Pre-alert Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="preAlertDate" onkeydown="return false" class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div>

    <!-- Sanjana Khan...CommInv No and Date Edit -->

    <div class="row">

        <div class="col-md-12">
            <span class="booking-update-information-header">ETA/ETD</span>
            <hr>
        </div>

        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ETA</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="eta" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ETD</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="etd" onkeydown="return false" class="form-control date-picker"
                        />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Difference between ETA & ETD </label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" value="8" class="form-control" readonly/>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ATA Airport </label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" onkeydown="return false" class="form-control date-picker" />
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-12">

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">C/I WH Date </label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" onkeydown="return false" class="form-control date-picker" />
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Type of Delivery </label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" readonly/>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">P/U Time</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" value="34" class="form-control" readonly/>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Slot Request Date</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Slot Received Date</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Cargo Status</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Exception</label>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" readonly/>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Remarks</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Weekend Hours</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 booking-update-padding-column">
        </div>

        <!-- buyer -->
        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Pre-notice Req H&M</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" name="buyerName" id="buyerName" value="${directBookingParams.orderHeader.buyerName}" class="form-control"
                            readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Delivery After Arrival Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" value="${directBookingParams.orderHeader.buyerAddress1 }" class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Arrival to delivery</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Arrival to truck Depo</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 booking-update-padding-column">
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Truck Type</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Truck Booking Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Truck Cancelation Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">License Plate</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
        </div>
        <!-- notify party -->
        <div class="col-md-12 booking-update-padding-column">

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Driver Details</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">GHA to SOV ATA</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">GHA to SOV ATA</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Actual TOD Week</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 booking-update-padding-column">

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Year</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">TSP</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Freight Service</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ATD Origin</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control " />
                    </div>
                </div>
            </div>
        </div>
        <!-- shipper bank -->
        <div class="col-md-12 booking-update-padding-column">

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ATA Destnation</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control " />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Cutoff Date</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">ETA Deadline</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control date-picker" />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Airport To Airport</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 booking-update-padding-column">

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Transit time met</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly />
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Airport To DC</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class='form-group required'>
                    <div class="col-md-12">
                        <label class="text-center">Difference in Days</label>
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>