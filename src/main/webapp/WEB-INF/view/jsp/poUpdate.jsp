<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
html, body {
	background-color: #FFF;
}
</style>	
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/powisebooking.js"></script>

	<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/poWiseBookingUpdate.js"></script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">PO Update</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-edit"></i>&nbsp;Purchase Orders</li>
			<li class="active">PO Update</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getPOForUpdate" method="post" commandName="poModel"
			cssClass="form-incline">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">PO NUMBER</label>
					<form:input path="po_no" id="po_no" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" name="search" id="search"
					class="btn btn-success btn-block" value="Search" />
			</div>
		</form:form>
	</div>
<c:if test="${orderList !=null }">
	<div class="container-fluid table-responsive">
				<div class="row search-result-datatable">
					
					<table class="table table-bordered table-striped" id="po_table">
						<thead>
							<tr>
								<th>PO Number</th>
								<th>Shipper ID</th>
								<th></th>
								<th></th>
								<th></th>

							</tr>
						</thead>
						<tbody id="accordion">

						<c:forEach items="${orderList}" var="po" varStatus="index">
							<tr class="" id= "po_search_${index.count}">
								<td style="font-size:12px;" id="search_id"><b>${po.po_no}</b> <a style="color:#3c8dbc !important;" data-toggle="collapse" href="#${index.count}"  onclick="hideexpand(this);"><span id="#${index.count}"  class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
								<td style="font-size:12px;"><b>${po.shipper}</b></td>
								<td></td>
								<td></td>
								<td style="font-size: 12px; text-align:right;"><a class='btn btn-danger btn-sm' onclick="deletePO(${po.app_id},${index.count});"><i class='fa fa-trash-o' aria-hidden='true'></i>&nbsp;Delete</a></td>
								<td style="display:none">${po.app_id}</td>
							</tr>
<tr class="hide" id="expand_${index.count}">
	 <td colspan="7">
	 <div class="collapse" id="${index.count}">
	 <table class="table table-striped" style="width:80%;margin-left: 8%;">
	    <tr>
			<th>PO Number</th>
		 	<th>Shipper</th>
		 	<th>Commercial Invoice No</th>
		 	<th>Commercial Invoice Date</th>
		 	<th>Cargo Handover Date</th>
		 	
	    </tr>	
		 <tr>
        	<td style="font-size:12px; width: 16%;"><input id="po_no_${index.count}" value="${po.po_no}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id ="shipper_${index.count}" value="${po.shipper}" class="form-control"/></td>
        	<td style="font-size:12px; width: 16%;"><input id="comm_inv_${index.count}" value="${po.comm_inv}" class="form-control"  /></td>
			<td style="font-size:12px; width: 16%;"><input id="comm_inv_date_${index.count}" value="${po.comm_inv_date}" class="form-control" /></td>
			<td style="font-size:12px; width: 16%;"><input id="cargo_handover_date_${index.count}" value="${po.cargo_handover_date}" class="form-control" /></td>
			
			
		</tr>
	    <tr>
			<th>Total Gross Weight</th>
		 	<th>Carton Length</th>
		 	<th>Carton Height</th>
		 	<th>Carton Width</th>
		 	<th>Carton Quantity</th>
		 	
	    </tr>	
		 <tr>
        	<td style="font-size:12px; width: 16%;"><input id="total_gw_${index.count}" value="${po.total_gw}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id ="carton_length_${index.count}" value="${po.carton_length}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="carton_height_${index.count}" value="${po.carton_height}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
			<td style="font-size:12px; width: 16%;"><input id="carton_width_${index.count}" value="${po.carton_width}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
			<td style="font-size:12px; width: 16%;"><input id="carton_quantity_${index.count}" value="${po.carton_quantity}" class="form-control" onchange="return calculateCBM(${index.count})"  /></td>
			
			
		</tr>
		<tr>
		<th>Total CBM</th>
		<th>Total Pieces</th>
	 	<th>Style No/PMA-PMC</th>
		<th>Color</th>
		<th>H.S. Code</th>
		
	 </tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="total_cbm_${index.count}" value="${po.total_cbm}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="total_pieces_${index.count}" value="${po.total_pieces}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="style_no_${index.count}" value="${po.style_no}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="color_${index.count}" value="${po.color}" class="form-control" /></td>
			<td style="font-size:12px; width: 16%;"><input id="hs_code_${index.count}" value="${po.hs_code}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Carton Unit</th>
		 	<th>WH Code/Ref. No</th>
		 	<th>SKU/Season/Prod. Code</th>
		 	<th>Total Net Weight</th>
		 	<th>Commodity</th>
	 	</tr>
		 <tr>
		  	<td style="font-size:12px; width: 16%;"><input id="carton_unit_${index.count}" value="${po.carton_unit}" class="form-control" onchange="return calculateCBM(${index.count})"/></td>
        	<td style="font-size:12px; width: 16%;"><input id="wh_code_${index.count}" value="${po.wh_code}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="sku_${index.count}" value="${po.sku}" class="form-control" /></td>
			<td style="font-size:12px; width: 16%;"><input id="total_nw_${index.count}" value="${po.total_nw}" class="form-control"/></td>
			<td style="font-size:12px; width: 16%;"><input id="commodity_${index.count}" value="${po.commodity}" class="form-control" /></td>
			
		</tr> 
		<tr>
		<th>Freight Mode</th>
		<th>Terms of shipment</th>
	 	<th>Ref.#1</th>
		<th>Ref.#2</th>
		<th>Ref.#3</th>
		
	 </tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="freight_mode_${index.count}" value="${po.freight_mode}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="terms_of_shipment_${index.count}" value="${po.terms_of_shipment}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="reference_1_${index.count}" value="${po.reference_1}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="reference_2_${index.count}" value="${po.reference_2}" class="form-control" /></td>
			<td style="font-size:12px; width: 16%;"><input id="reference_3_${index.count}" value="${po.reference_3}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Unit</th>
			<th>Size No</th>
		 	<th>Proj./Art./Del. No</th>
		 	<th>Ref.#4/Department</th>
		 	<th>Division</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="unit_${index.count}" value="${po.unit}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="size_no_${index.count}" value="${po.size_no}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="article_no_${index.count}" value="${po.article_no}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="department_${index.count}" value="${po.department}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="division_${index.count}" value="${po.division}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Fedder Vessel Name</th>
			<th>Mother Vessel 1 Name</th>
		 	<th>Mother Vessel 2 Name</th>
		 	<th>Mother Vessel 3 name</th>
		 	<th>Voyage 1/Flight No 1</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="fvsl_${index.count}" value="${po.fvsl}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl1_${index.count}" value="${po.mvsl1}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl2_${index.count}" value="${po.mvsl2}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl3_${index.count}" value="${po.mvsl3}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="voyage1_${index.count}" value="${po.voyage1}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Voyage 2/Flight No 2</th>
			<th>Voyage 3/Flight No 3</th>
		 	<th>Expected Departure Date</th>
		 	<th>Actual Departure Date</th>
		 	<th>Transhipment 1 ETD</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="voyage2_${index.count}" value="${po.voyage2}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="voyage3_${index.count}" value="${po.voyage3}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="etd_${index.count}" value="${po.etd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="atd_${index.count}" value="${po.atd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment1etd_${index.count}" value="${po.transhipment1etd}" class="form-control" /></td>
		</tr>
			<tr>
			<th>Transhipment 2 ETD</th>
			<th>Transhipment 1 ETA</th>
		 	<th>Transhipment 2 ETA</th>
		 	<th>Expected Arrival Date</th>
		 	<th>Actual Arrival Date</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="transhipment2etd_${index.count}" value="${po.transhipment2etd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment1eta_${index.count}" value="${po.transhipment1eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment2eta_${index.count}" value="${po.transhipment2eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="eta_${index.count}" value="${po.eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="ata_${index.count}" value="${po.ata}" class="form-control" /></td>

		</tr>
		
		<tr style="display:none">
			<th></th>
		 	
	 	</tr>
	 	<tr style="display:none">
        	<td style="font-size:12px; width: 16%;display: none"><input id="app_id_${index.count}" value="${po.app_id}"/></td>
		</tr>
		
		</table>
		</div>
		</td>
</tr>
		</c:forEach>
					</table>
					</div>
					<!-- <div style="text-align: center">
						<button type="button" style="width: 19%; margin-bottom: 20px;"
							class="btn btn-primary" onclick="check()">Proceed
							to Booking</button>
					</div>	 -->	
					<div style="text-align: right">
						<button type="button" style="width: 10%; margin-bottom: 20px;"
							class="btn btn-primary" onclick="finishUpdate()">Finish Update</button>
							
						<button type="button" style="width: 10%; margin-bottom: 20px;"
							class="btn btn-danger" onclick="window.location.href='${pageContext.request.contextPath}/poUpdatePage'">Cancel Update</button>
					</div>			
				</div>
			</c:if>	
	  
</div>

<div class="modal fade" id="poUpdateStatus" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">PO Update</h4>
			</div>
			<div class="modal-body" style="text-align:center;">
				<p id=msgBody>PO Updated Successfully!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.href='${pageContext.request.contextPath}/poUpdatePage'">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="poDeleteModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">PO Update</h4>
			</div>
			<div class="modal-body" style="text-align:center;">
				<p id=podeleted>PO Deleted Successfully!</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<jsp:include page="footer_v2.jsp"></jsp:include>

<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
	
<script>
	function hideexpand(varobj) {
		var res = varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');

		$($myGroup)
				.find('.collapse')
				.each(
						function() {
							//alert($(this).attr('id'));
							if (res[1] == $(this).attr('id')) {

								$(this).closest("tr").toggleClass("hide");
								$(this).toggle();

								//alert("entering");
								//alert(document.getElementById("myicon"));
								//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
								//alert(document.getElementById("myicon"));
								if (document.getElementById("#" + res[1]).className == "glyphicon glyphicon-chevron-down") {
									//alert("entering");
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-up";
								} else {
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-down";
								}

							} else {
							}

						});

	}
</script> 