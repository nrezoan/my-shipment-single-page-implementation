<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@page import="com.myshipment.dto.LoginDTO"%>
<%@page import="com.google.gson.Gson"%>
<%@page errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

	<script>
  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  		ga('create', 'UA-90249127-1', 'auto');
  		ga('send', 'pageview');
	</script>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>myshipment</title>
    
    <script type="text/javascript">
    	var myContextPath = "${pageContext.request.contextPath}";
    </script>
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700" rel="stylesheet">
    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- material icon-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- bootstrap CSS -->
    <%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/bootstrap/bootstrap-afterlogin.min.css"> --%>
	<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate-dashboard-custom.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/navbar-menu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/report-format.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/afterlogin.css">

    <!-- responsive CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">

	<!-- sweetalert css -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sweetalert.css">
	
	<!-- jquery ui css -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
	<!-- sweetalert js -->
	<script src="${pageContext.request.contextPath}/resources/js/sweetalert.min.js"></script>
	<!-- jquery -->
	<script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery.js"></script>
	
	<!-- overlay js -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
	<!-- ChartJS 1.0.1 -->
    <script src="${pageContext.request.contextPath}/resources/plugins/chartjs/Chart.min.js"></script>
    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    
    <!-- select2 -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/select2/select2.css">
    
    <!-- Theme style -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
    

	<!-- font  -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet"> -->
	<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
</head>