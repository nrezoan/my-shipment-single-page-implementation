<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
.nav-tabs-custom>.nav-tabs>li.active {
	border-top-color: #252525;
}

.nav-tabs-custom>.nav-tabs>li>a {
	font-size: 15px;
	font-weight: bold;
}

.canvasjs-chart-credit {
	display: none;
}
.active #chartContainerCBMCRGWT {
	display: block;
}
.commercial h3 {
	background-color: #222534;
	padding-top: 7px;
	font-weight: 400;
	height: 40px;
	margin-top: 10px;
	color: #fff;
}

.row .col-md-6 {
	margin-top: 1%;
}
</style>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dashboardInfoDetails.js"></script>




<c:if test="${not empty error}">
	<style>
		html {
			background-color: #fff;
		}
	</style>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="text-center" style="height: 50px; color: red; font-weight: bold; margin-top: 20%;">
				${error}
			</div>
			<div class="text-center" style="">
				<a class="btn btn-primary" href="${pageContext.request.contextPath}/homepage" class="">Return to Dashboard</a>
			</div>
			<br><br><br><br><br>
		</div>
		<div class="col-md-4"></div>
		
	</div>
	
	
</c:if>


<c:if test="${not empty totalShipmentList}">
	<div class="entire-content-c">
		<div class="content"
			style="background-color: #eaeaea; display: block;">
			<section class="content">
				<div class="row-without-margin" style="margin-left: -1%; width: 102%;">
					<div class="col-sm-12">
						<div class="commercial">
							<h3 class="text-center">${status }</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

						<div class="box box-myshipment">
							<div class="box-header with-border">
								<h3 class="box-title">${chartHeader}</h3>
							</div>
							<div class="box-body no-padding" style="display: block;">
								<div id="chartContainer" style="width: 95%; height: 300px"></div>
							</div>
						</div>
					</div>

					<div class="col-md-6">

						<div class="box box-myshipment">
							<div class="box-header with-border">
								<h3 class="box-title">${listHeader}</h3>
							</div>
							<div class="box-body no-padding" style="display: block;">
								<table class="table table-hover">
									<thead style="">
										<tr>
											<th>${name}</th>
											<th class="text-center">Total Shipment</th>
											<th class="text-center">% of Shipment</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${totalShipmentList}" var="ship"
											varStatus="index">

											<tr>
												<td>${ship.name}</td>
												<td class="text-center">${ship.shipment}</td>
												<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ship.shipmentPercentage}"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<script type="text/javascript">
		var details = ${totalShipmentListJson};
		$(document).ready(function() {
			var dataArray = createTotalShipmentDataArray(details);
			drawChart(dataArray, "chartContainer");
		});
	</script>
</c:if>

<c:if test="${not empty totalGWTList}">
	<div class="entire-content-c">
		<div class="content"
			style="background-color: #eaeaea; display: block;">
			<section class="content">
				<div class="row-without-margin" style="margin-left: -1%; width: 102%;">
					<div class="col-sm-12">
						<div class="commercial">
							<h3 class="text-center">${status }</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="box box-myshipment">
							<div class="box-header with-border">
								<h3 class="box-title">${chartHeader}</h3>
							</div>
							<div class="box-body no-padding" style="display: block;">
								<div id="chartContainerGWT" style="width: 95%; height: 300px"></div>
							</div>
						</div>
						<%-- <div class="nav-tabs-custom">
							<ul class="nav nav-tabs nav-justified">
								<li class="active"><a href="#tab_1" data-toggle="tab">${chartHeader}</a></li>
								<li class=""><a href="#tab_2" data-toggle="tab">${chartHeaderCBMCRG}</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="tab_1">
									<div class="row">
										<div id="chartContainerGWT" style="width: 95%; height: 300px;"></div>
									</div>
									
								</div>
								
								<div class="tab-pane active" id="tab_2">
									<div class="row">
										<div id="chartContainerCBMCRGWT" style="width: 95%; height: 300px;"></div>
									</div>
								</div>
							</div>
						</div> --%>
						<div class="box box-myshipment" style="display: none;">
							<div class="box-header with-border">
								<h3 class="box-title"></h3>
							</div>
							<div class="box-body no-padding" style="display: block;">
								<div id="chartContainer" style="width: 95%; height: 300px"></div>
							</div>
						</div>
					</div>

					<div class="col-md-6">

						<div class="box box-myshipment">
							<div class="box-header with-border">
								<h3 class="box-title">${listHeader}</h3>
							</div>
							<div class="box-body no-padding" style="display: block;">
								<table class="table table-hover">
									<thead style="">
										<tr>
											<th>${name}</th>
											<th class="text-center">${value}</th>
											<th class="text-center">${percentage}</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${totalGWTList}" var="ship"
											varStatus="index">

											<tr>
												<td>${ship.name}</td>
												<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ship.totalValue}"/></td>
												<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${ship.valuePercentage}"/></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
	<script type="text/javascript">
		var details = ${totalGWTListJson};
		$(document).ready(function() {
			var dataArrayGWT = createGWTDataArray(details);
			//var dataArrayCBMCRGWT = createCBMCRGWTDataArray(details);
			drawChart(dataArrayGWT, "chartContainerGWT");
			//drawChart(dataArrayCBMCRGWT, "chartContainerCBMCRGWT");
			
		});
	</script>
</c:if>

<script>
	$(document).ready(function() {
		
	});
</script>

<jsp:include page="footer_v2_fixed.jsp"></jsp:include>