
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html {
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Commercial Invoice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Commercial Invoice</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="findCommercialInvoice" method="post" commandName="commercialInvoice" name="commInvoiceFrm" cssClass="form-incline">
		
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">HBL Number</label>
					<form:input path="hblnumber" id="hblnumber" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">PO Number</label>
					<form:input path="ponumber"  id="ponumber" cssClass="form-control"/>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">				
				<label style="display: block; color: #f5f5f5">. </label>
									<!-- added for alignment purpose, dont delete -->
									<input type="submit" id="btn-appr-po-search reportSearchBtn" onclick="return reportSearch();"
										class="btn btn-primary form-control" value="Submit" />
			</div>	
			
			<div class='col-md-2 col-xs-6'>
				<div class='form-group'>
					<label style="color: red; margin-top: 16%;">${message}</label>
				</div>
			</div> 
			
		</form:form>
	</div>

	<hr>
	<br>
</div>



<script>
	function reportSearch() {
		var hblnumber = $("#hblnumber").val();
		var ponumber = $("#ponumber").val();
		if(hblnumber == "") {
			if(ponumber == "") {
				swal ( "HBL Number" ,  "HBL number cannot be empty!" ,  "error" );
				return false;
			}
		} else {
			return true;
		}
		if(ponumber == "") {
			swal ( "PO Number" ,  "PO number cannot be empty!" ,  "error" );
			return false;
		} else {
			return true;
		}
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript">

	$(document).ready(function() {
		$("#commInvoiceFrm").validate({

			rules : {
				"kunnr"    	: "required",
				"hblnumber" : "required",
				"ponumber" 	: "required"				
			},
			messages : {
				"kunnr"    	: "KUNNR is required",
				"hblnumber" : "HBL No is required",
				"ponumber" 	: "PO Number is required"				
			}
		});
	});
</script>
