<!-- Update Modal -->
<style>
	@media screen and (max-width: 767px) {
		#preAlertUploadModal {
			width: 90% !important;
		}
		#preAlertIplViewModal {
			width: 90% !important;
		}
		iframe {
		    zoom: 0.15;
		    -moz-transform:scale(0.75);
		    -moz-transform-origin: 0 0;
		    -o-transform: scale(0.75);
		    -o-transform-origin: 0 0;
		    -webkit-transform: scale(0.75);
		    -webkit-transform-origin: 0 0;
		}
	}
</style>

<!-- Upload HBL/HAWB modal -->
<div class="modal fade" id="updateFormModal" role="dialog" style="">
	<div class="modal-dialog" id="preAlertUploadModal" style="width: 65%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- <h4 class="modal-title">Modal Header</h4> -->
				<span class="preAlertModalHeader">Pre-Alert Documents</span>
			</div>
			<div class="modal-body">
				<div class="row row-no-margin-331932">
					<div class="col-md-2">
						<h5 class="text-center"><b>HBL/HAWB: </b></h5><h5 class="text-center" id="blNo"></h5><input type="hidden" id="bl-number">
					</div>
					<div class="col-md-5">
						<h5 class="text-center"><b>Consignee: </b></h5><h5 class="text-center" id="consignee"></h5>
					</div>
					<div class="col-md-5">
						<h5 class="text-center"><b>Destination Agent: </b></h5><h5 class="text-center" id="agent"></h5>
					</div>
				</div>
				
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<input type="hidden" name="itemNumber" id="id_itemNumber">
						<span class="pre-alert-modal-info-header">Upload Commercial Invoice and Packing List</span>
						<hr class="hr-update-modal">

						<div class="row row-no-margin-331932">
							<div class="col-md-6">
								<h5 class="text-center"><b>File Status:</b></h5>
								<h5 id="preAlertDocStatus" style="text-align: center;"></h5>
								<input type="hidden" id="isPreAlertDocExists">
							</div>
							<div class="col-md-6">
								<h5 class="text-center"><b>Action:</b></h5>
								<div class="form-inline">
									<div class="col-md-12 col-xs-12 col-sm-12">
										<form id="commercialInvoiceFileUploadForm" class="form-group" method="post">
											<div class="input-group">
												<input type="file" name="commercialInvoiceFile" class="file" id="commercialInvoiceFile">
												<input type="text" class="form-control" disabled placeholder="Upload" id="commercialInvoiceFileText" style="height: 35px; text-align: center;">
												<span class="input-group-btn">
													<button class="browse btn btn-primary" type="button" style="height: 35px;">
														<i class="glyphicon glyphicon-search"></i> Browse
													</button>
												</span>
											</div>
	
	
										</form>
									</div>
									
									
								</div>

							</div>
						</div>
						<hr>
					</div>
				</div>
				
				<div class="row row-no-margin-331932">
					
					<div class="col-md-8 col-xs-12 col-sm-12">
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						<input type="submit" class="btn btn-success btn-block" value="Upload" onclick="return uploadCommercialInvoice();" id="uploadCommercialInvoice">
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						<button type="button" class="btn btn-danger btn-block" id="cancel-item"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- View HBL/HAWB modal -->
<div class="modal fade" id="viewIplModal" role="dialog" style="">
	<div class="modal-dialog" id="preAlertIplViewModal" style="width: 90%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- <h4 class="modal-title">Modal Header</h4> -->
				<span class="preAlertModalHeader">Commercial Invoice and Packing List</span>
			</div>
			<div class="modal-body">
				<div class="row row-no-margin-331932">
					<div class="col-md-2">
						<h5 class="text-center">
							<b>HBL/HAWB: </b>
							<span class="text-center" id="blNoIpl"></span>
						</h5>
						<input type="hidden" id="bl-number-ipl">
					</div>
					<div class="col-md-3">
						<h5 class="text-center">
							<b>Commercial Invoice: </b> 
							<span class="text-center" id="commercialInvoiceNumber"></span>
						</h5>
					</div>
					<div class="col-md-7">
						<h5 class="text-center"><b>N.B: </b>Please check the commercial Invoice Number and make sure the uploaded commercial invoice is correct.</h5>
					</div>
				</div>
				<hr>
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<iframe id="displayIplFrame" style="width: 100%; height: 600px;"></iframe><!-- width="800" height="500" -->
					</div>
				</div>
				
				<div class="row row-no-margin-331932">
					
					<div class="col-md-8 col-xs-12 col-sm-12">
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						<button type="button" class="btn btn-danger btn-block" id="cancel-item"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Upload MBL/MAWB modal -->
<div class="modal fade" id="updateFormModalMBL" role="dialog" style="">
	<div class="modal-dialog" id="preAlertUploadModalMBL" style="width: 65%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- <h4 class="modal-title">Modal Header</h4> -->
				<span class="preAlertModalHeader">Pre-Alert Documents</span>
			</div>
			<div class="modal-body">
				<div class="row row-no-margin-331932">
					<div class="col-md-2">
						<h5 class="text-center"><b>MBL/MAWB: </b></h5><h5 class="text-center" id="mblNo"></h5><input type="hidden" id="mbl-number">
					</div>
					<div class="col-md-5">
						<h5 class="text-center"><b>Consignee: </b></h5><h5 class="text-center" id="mbl-consignee"></h5>
					</div>
					<div class="col-md-5">
						<h5 class="text-center"><b>Destination Agent: </b></h5><h5 class="text-center" id="mbl-agent"></h5>
					</div>
				</div>
				
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<input type="hidden" name="itemNumber" id="id_itemNumber">
						<span class="pre-alert-modal-info-header">Upload: Master Bill of Lading (MBL) / Master Airway Bill (MAWB)</span>
						<hr class="hr-update-modal">

						<div class="row row-no-margin-331932">
							<div class="col-md-6">
								<h5 class="text-center"><b>File Status:</b></h5>
								<h5 id="preAlertMblStatus" style="text-align: center;"></h5>
								<input type="hidden" id="isPreAlertMblExists">
							</div>
							<div class="col-md-6">
								<h5 class="text-center"><b>Action:</b></h5>
								<div class="form-inline">
									<div class="col-md-12 col-xs-12 col-sm-12">
										<form id="commercialInvoiceFileUploadForm" class="form-group" method="post">
											<div class="input-group">
												<input type="file" name="masterBlFile" class="file" id="masterBlFile">
												<input type="text" class="form-control" disabled placeholder="Upload" id="masterBlFileText" style="height: 35px; text-align: center;">
												<span class="input-group-btn">
													<button class="browse btn btn-primary" type="button" style="height: 35px;">
														<i class="glyphicon glyphicon-search"></i> Browse
													</button>
												</span>
											</div>
	
	
										</form>
									</div>
									
									
								</div>

							</div>
						</div>
						<hr>
					</div>
				</div>
				
				<div class="row row-no-margin-331932">
					
					<div class="col-md-8 col-xs-12 col-sm-12">
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						<input type="submit" class="btn btn-success btn-block" value="Upload" onclick="return uploadMasterBL();" id="uploadCommercialInvoice">
					</div>
					<div class="col-md-2 col-xs-6 col-sm-6">
						<button type="button" class="btn btn-danger btn-block" id="cancel-item"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>