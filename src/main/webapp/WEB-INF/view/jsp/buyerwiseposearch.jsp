
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<script type="text/javascript">
	var choosen = ${selectedValue};
	 var modelArray = []; 
	console.log(choosen);
</script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/powisebooking.js"></script>
<style>
/* 	td{
		width: 16%;
	} */
	.container-fluid {
    padding-left: 15px;
    } 
</style>
<c:forEach items="${approvedpo}" var="item"> 
    <script type="text/javascript"> 
      model = { 
        po_no : "${item.po_no}", 
        division : "${item.division}", 
        pol:  "${item.pol}", 
        pod:  "${item.pod}", 
        place_of_delivery:  "${item.place_of_delivery}", 
        place_of_delivery_address:  "${item.place_of_delivery_address}", 
        cargo_handover_date:  "${item.cargo_handover_date}", 
        comm_inv:  "${item.comm_inv}", 
        comm_inv_date: "${item.comm_inv_date}" ,
        freight_mode:"${item.freight_mode}",
        terms_of_shipment: "${item.terms_of_shipment}",
        buyer_id: "${selectedValue}",
        fvsl: "${item.fvsl}",
        mvsl1: "${item.mvsl1}",
        mvsl2: "${item.mvsl2}",
        mvsl3: "${item.mvsl3}",
        voyage1: "${item.voyage1}",
        voyage2: "${item.voyage2}",
        voyage3: "${item.voyage3}",
        etd: "${item.etd}",
        eta: "${item.eta}",
        ata: "${item.ata}",
        atd: "${item.atd}",
        transhipment1etd: "${item.transhipment1etd}",
        transhipment2etd: "${item.transhipment2etd}",
        transhipment1eta: "${item.transhipment1eta}",
        transhipment2eta: "${item.transhipment2eta}",
        
      }; 
   
      modelArray.push(model); 
      console.log(modelArray); 
    </script> 
  </c:forEach> 
<form action="${pageContext.request.contextPath}/approvednotbookedpo"
	method="get">
	<section class="main-body">
		<div class="well assign-po-search">

			<fieldset>
				<legend>PO Search</legend>
				<div class='row'>
					<div class='col-sm-4'>
						<div class='form-group'>
							<label>Buyer</label> <select
								class="form-control auto-complete selectpicker"
								data-live-search="true" id="buyer" name="buyer"
								required="required">
								<option value="">Select</option>
								<c:if test="${buyer != null}">
									<c:forEach items="${buyer}" var="buyer">
										<option value="${buyer.key }">${buyer.value}</option>
									</c:forEach>
								</c:if>

							</select>
						</div>
					</div>
					<div class='col-sm-4'>
						<div class='form-group'>
							<label>PO Number</label> <input class="form-control"
								id="po_number" name="po-number" type="text" />
						</div>
					</div>
					<!-- 					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label> <input id="from_date" name="from_date"
								type="text"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>TO Date</label> <input id="to_date" name="to_date"
								type="text"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div> -->

					<!--  <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>Sku No</label>
                   <input class="form-control" id="user_lastname" name="user[lastname]" required="true" size="30" type="text" />
                 </div>
                 </div> -->
					<div class='col-sm-2' style="margin-top: 2%;">
						<div class='form-group'>

							<input type="submit" id="btn-appr-po-search"
								class="btn btn-success form-control" value="Search" />
						</div>
					</div>

				</div>

			</fieldset>
		</div>
		<div class="container-fluid table-responsive">
		<c:if test="${not empty approvedpo}">
			<div class="row table_header">
				<!-- <div class="col-lg-11 col-md-11 col-sm-10 col-xs-9"> -->
					<div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
						<span class="glyphicon glyphicon-triangle-bottom"></span>
						<h3>Search Results</h3>
						<span id="display-message" class=""></span>
					</div>
				<div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
					<div class='form-group'>
						<input class="form-control" type="text" id="inputfilter"
							 placeholder="Search for purchase orders.." />
					</div>
				</div> 
			<!-- 	</div> -->

			</div>
				<div class="row search-result-datatable">
					
					<table class="table table-bordered table-striped" id="po_table">
						<thead style="background-color: #222534; color: #fff;">
							<tr>
								<th style="width:8%;">Select</th>
								<th>PO No</th>
								<th>PO Status</th>

							</tr>
						</thead>
						<tbody id="accordion">

						<c:forEach items="${approvedpo}" var="po" varStatus="index">
							<tr class="" id= "po_search">
								<td style="font-size:12px;"><input type="hidden" id="app_id_${index.count}"
											value="${po.app_id }" />
											<div class="checkbox">
												<label><input type="checkbox" name="checking" id="checkbox_${index.count}"/></label>
											</div></td>
								<td style="font-size:12px;" id="search_id"><b>${po.po_no}</b> <a style="color:#3c8dbc !important;" data-toggle="collapse" href="#${index.count}"  onclick="hideexpand(this);"><span id="#${index.count}"  class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
								<td style="font-size: 12px;"><b>Pending</b></td>
								
							</tr>
												<tr class="hide">
	 <td colspan="7">
	 <div class="collapse" id="${index.count}">
	 <table class="table table-striped" style="width:80%;margin-left: 8%;">
	    <tr>
			<th>Total Gross Weight<span class="required-alone">*</span></th>
		 	<th>Carton Length<span class="required-alone">*</span></th>
		 	<th>Carton Height<span class="required-alone">*</span></th>
		 	<th>Carton Width<span class="required-alone">*</span></th>
		 	<th>Carton Quantity<span class="required-alone">*</span></th>
		 	
	    </tr>	
		 <tr>
        	<td style="font-size:14px; width: 16%;"><input id="total_gw_${index.count}" value="${po.total_gw}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id ="carton_length_${index.count}" value="${po.carton_length}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="carton_height_${index.count}" value="${po.carton_height}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
			<td style="font-size:14px; width: 16%;"><input id="carton_width_${index.count}" value="${po.carton_width}" class="form-control" onchange="return calculateCBM(${index.count})" /></td>
			<td style="font-size:14px; width: 16%;"><input id="carton_quantity_${index.count}" value="${po.carton_quantity}" class="form-control" onchange="return calculateCBM(${index.count})"  /></td>
			
			
		</tr>
		<tr>
		<th>Total CBM<span class="required-alone">*</span></th>
		<th>Total Pieces<span class="required-alone">*</span></th>
	 	<th>Style No/PMA-PMC</th>
		<th>Color</th>
		<th>H.S. Code<span class="required-alone">*</span></th>
		
	 </tr>
	 	<tr >
        	<td style="font-size:14px; width: 16%;"><input id="total_cbm_${index.count}" value="${po.total_cbm}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="total_pieces_${index.count}" value="${po.total_pieces}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="style_no_${index.count}" value="${po.style_no}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="color_${index.count}" value="${po.color}" class="form-control" /></td>
			<td style="font-size:14px; width: 16%;"><input id="hs_code_${index.count}" value="${po.hs_code}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Carton Unit<span class="required-alone">*</span></th>
		 	<th>WH Code/Ref. No</th>
		 	<th>SKU/Season/Prod. Code</th>
		 	<th>Total Net Weight</th>
		 	<th>Commodity<span class="required-alone">*</span></th>
	 	</tr>
		 <tr>
		  	<td style="font-size:14px; width: 16%;"><%-- <input id="carton_unit_${index.count}" value="${po.carton_unit}" class="form-control"/> --%>
		  	<select class="form-control" name="cartonMeasurementUnit"
													id="carton_unit_${index.count}" onchange="return calculateCBM(${index.count})">
													<option value="">Select</option>
													<option value="IN">IN</option>
													<option selected value="CM">CM</option>
												</select></td>
        	<td style="font-size:14px; width: 16%;"><input id="wh_code_${index.count}" value="${po.wh_code}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="sku_${index.count}" value="${po.sku}" class="form-control" /></td>
			<td style="font-size:14px; width: 16%;"><input id="total_nw_${index.count}" value="${po.total_nw}" class="form-control"/></td>
			<td style="font-size:14px;"><%-- <input id="commodity_${index.count}" value="${po.commodity}" class="form-control" /> --%>
					<select name="material" id="commodity_${index.count}" class="form-control drop">
													<!-- <option value="" selected>-- Please Select --</option> -->
													<option value="A75" selected>RMG</option>
													<%-- <%
														try {
													%>
														<c:forEach var="materials" items="${materials }">
															<c:choose>
																<c:when test="${materials.value == 000000000001000106}">
																	<option value="${materials.value }" selected>${materials.label}</option>
																</c:when>
															<c:otherwise>
																<option value="${materials.value }" >${materials.label}</option>
															</c:otherwise>
															</c:choose>
														</c:forEach>
													
													<%
														} catch (Exception e) {
															e.printStackTrace();
														}
													%> --%>
					</select>
			</td>
			
		</tr> 
			<tr>
			<th>Unit</th>
			<th>Size No</th>
		 	<th>Proj./Art./Del. No</th>
		 	<th>Ref.#4/Department</th>
		 	<th></th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:14px; width: 16%;"><%-- <input id="unit_${index.count}" value="${po.unit}" class="form-control" /> --%>
        	<select class="form-control drop" name="unit"
													id="unit_${index.count}">
													<option selected value="KG">Kilogram</option>
													<option value="MOK">Mol/kilogram</option>
													<option value="KGM">Kilogram/Mol</option>
													<option value="MOL">Mol</option>
													<option value="MON">Months</option>
													<option value="EA">Each</option>
													<option value="FT2">Square foot</option>
													<option value="FT3">Cubic foot</option>
													<option value="KGS">Kilogram/second</option>
													<option value="KGV">Kilogram/cubic meter</option>
													<option value="TM3">1/cubic meter</option>
													<option value="DZ">Dozen</option>
													<option value="JAR">JAR</option>
													<option value="DM">Decimeter</option>
													<option value="PWC">PLYWOOD CASE</option>
													<option value="?GQ">Microgram/cubic meter</option>
													<option value="?GL">Microgram/liter</option>
													<option value="MPB">Mass parts per billion</option>
													<option value="DR">Drum</option>
													<option value="MPA">Megapascal</option>
													<option value="KGF">Kilogram/Square meter</option>
													<option value="ROL">Role</option>
													<option value="&quot;3">Cubic inch</option>
													<option value="&quot;2">Square inch</option>
													<option value="KGK">Kilogram/Kilogram</option>
													<option value="&quot;">Inch</option>
													<option value="MNM">Millinewton/meter</option>
													<option value="%">Percentage</option>
													<option value="EU">Enzyme Units</option>
													<option value="KVA">Kilovoltampere</option>
													<option value="V%">Percent volume</option>
													<option value="C3S">Cubic centimeter/second</option>
													<option value="D">Days</option>
													<option value="ACR">Acre</option>
													<option value="KWH">Kilowatt hours</option>
													<option value="F">Farad</option>
													<option value="G">Gram</option>
													<option value="MMO">Millimol</option>
													<option value="BDR">Board Drums</option>
													<option value="MMH">Millimeter/hour</option>
													<option value="A">Ampere</option>
													<option value="MMK">Millimol/kilogram</option>
													<option value="L">Liter</option>
													<option value="M">Meter</option>
													<option value="N">Newton</option>
													<option value="QML">Kilomol</option>
													<option value="MMG">Millimol/gram</option>
													<option value="H">Hour</option>
													<option value="MMA">Millimeter/year</option>
													<option value="J">Joule</option>
													<option value="K">Kelvin</option>
													<option value="W">Watt</option>
													<option value="V">Volt</option>
													<option value="A/V">Siemens per meter</option>
													<option value="FT">Foot</option>
													<option value="P">Points</option>
													<option value="S">Second</option>
													<option value="MMS">Millimeter/second</option>
													<option value="MLK">Milliliter/cubic meter</option>
													<option value="R-U">Nanofarad</option>
													<option value="MLI">Milliliter act. ingr.</option>
													<option value="EML">Enzyme Units / Milliliter</option>
													<option value="HA">Hectare</option>
													<option value="BUN">BUNDLES</option>
													<option value="WK">Weeks</option>
													<option value="LHK">Liter per 100 km</option>
													<option value="SKD">SKIDS</option>
													<option value="GM">Gram/Mol</option>
													<option value="GJ">Gigajoule</option>
													<option value="MM3">Cubic millimeter</option>
													<option value="MM2">Square millimeter</option>
													<option value="NAM">Nanometer</option>
													<option value="RF">Millifarad</option>
													<option value="M-2">1 / square meter</option>
													<option value="VPB">Volume parts per billion</option>
													<option value="GHG">Gram/hectogram</option>
													<option value="%O">Per mille</option>
													<option value="PKG">Package</option>
													<option value="VPM">Volume parts per million</option>
													<option value="QT">Quart, US liquid</option>
													<option value="VPT">Volume parts per trillion</option>
													<option value="STR">String</option>
													<option value="AU">Activity unit</option>
													<option value="PT">Pint, US liquid</option>
													<option value="KJM">Kilojoule/Mol</option>
													<option value="JMO">Joule/Mol</option>
													<option value="KJK">Kilojoule/kilogram</option>
													<option value="PS">Picosecond</option>
													<option value="LMS">Liter/Molsecond</option>
													<option value="BT">Bottle</option>
													<option value="NMM">Newton/Square millimeter</option>
													<option value="TO">Tonne</option>
													<option value="PMI">1/minute</option>
													<option value="MIJ">Millijoule</option>
													<option value="TS">Thousands</option>
													<option value="KIK">kg act.ingrd. / kg</option>
													<option value="ST">items</option>
													<option value="SET">SETS</option>
													<option value="MIS">Microsecond</option>
													<option value="MIN">Minute</option>
													<option value="LMI">Liter/Minute</option>
													<option value="UNI">Unit</option>
													<option value="TES">Tesla</option>
													<option value="?C">Degrees Celsius</option>
													<option value="KHZ">Kilohertz</option>
													<option value="CV">Case</option>
													<option value="RLS">ROLLS</option>
													<option value="?F">Fahrenheit</option>
													<option value="GKG">Gram/kilogram</option>
													<option value="000">Meter/Minute</option>
													<option value="MI2">Square mile</option>
													<option value="JKK">Spec. Heat Capacity</option>
													<option value="CD">Candela</option>
													<option value="MHZ">Megahertz</option>
													<option value="JKG">Joule/Kilogram</option>
													<option value="CM">Centimeter</option>
													<option value="OHM">Ohm</option>
													<option value="MHV">Megavolt</option>
													<option value="CL">Centiliter</option>
													<option value="GPH">Gallons per hour (US)</option>
													<option value="KOH">Kiloohm</option>
													<option value="TC3">1/cubic centimeter</option>
													<option value="M3">Cubic meter</option>
													<option value="M2">Square meter</option>
													<option value="MG">Milligram</option>
													<option value="COI">COIL</option>
													<option value="ONE">One</option>
													<option value="DAY">Days</option>
													<option value="ML">Milliliter</option>
													<option value="MI">Mile</option>
													<option value="IDR">Iron Drum</option>
													<option value="GOH">Gigaohm</option>
													<option value="MA">Milliampere</option>
													<option value="KPA">Kilopascal</option>
													<option value="MV">Millivolt</option>
													<option value="MGW">Megawatt</option>
													<option value="MW">Milliwatt</option>
													<option value="M/M">Mol per cubic meter</option>
													<option value="MWH">Megawatt hour</option>
													<option value="M/L">Mol per liter</option>
													<option value="MN">Meganewton</option>
													<option value="MGQ">Milligram/cubic meter</option>
													<option value="MM">Millimeter</option>
													<option value="MGO">Megohm</option>
													<option value="M/H">Meter/Hour</option>
													<option value="COL">COLLI</option>
													<option value="MGL">Milligram/liter</option>
													<option value="MGK">Milligram/kilogram</option>
													<option value="CON">Container</option>
													<option value="VAM">Voltampere</option>
													<option value="VAL">Value-only material</option>
													<option value="NI">Kilonewton</option>
													<option value="MGG">Milligram/gram</option>
													<option value="MGE">Milligram/Square centimeter</option>
													<option value="NM">Newton/meter</option>
													<option value="NA">Nanoampere</option>
													<option value="M/S">Meter/second</option>
													<option value="V%O">Permille volume</option>
													<option value="KMH">Kilometer/hour</option>
													<option value="CM2">Square centimeter</option>
													<option value="M2S">Square meter/second</option>
													<option value="PAL">Pallet</option>
													<option value="BLK">BULK</option>
													<option value="MVA">Megavoltampere</option>
													<option value="NS">Nanosecond</option>
													<option value="KMN">Kelvin/Minute</option>
													<option value="PAA">Pair</option>
													<option value="NO">NOS</option>
													<option value="KMK">Cubic meter/Cubic meter</option>
													<option value="PPT">Parts per trillion</option>
													<option value="PAC">Pack</option>
													<option value="OM">Spec. Elec. Resistance</option>
													<option value="TRS">TRUSSES</option>
													<option value="CMH">Centimeter/hour</option>
													<option value="YD2">Square Yard</option>
													<option value="PPM">Parts per million</option>
													<option value="KMS">Kelvin/Second</option>
													<option value="PPB">Parts per billion</option>
													<option value="YD3">Cubic yard</option>
													<option value="WDP">Wooden Pallet</option>
													<option value="CMS">Centimeter/second</option>
													<option value="?A">Microampere</option>
													<option value="OZ">Ounce</option>
													<option value="MEJ">Megajoule</option>
													<option value="WDC">Wooden Case</option>
													<option value="LPH">Liter per hour</option>
													<option value="WDB">WOODEN BOX</option>
													<option value="KM2">Square kilometer</option>
													<option value="GM2">Gram/square meter</option>
													<option value="GM3">Gram/Cubic meter</option>
													<option value="?M">Micrometer</option>
													<option value="?L">Microliter</option>
													<option value="GLI">Gram/liter</option>
													<option value="WCR">WOODEN CRATE</option>
													<option value="PA">Pascal</option>
													<option value="?F">Microfarad</option>
													<option value="RHO">Gram/cubic centimeter</option>
													<option value="WMK">Heat Conductivity</option>
													<option value="CRT">Crate</option>
													<option value="OCM">Spec. Elec. Resistance</option>
													<option value="HL">Hectoliter</option>
													<option value="DEG">Degree</option>
													<option value="PRS">Number of Persons</option>
													<option value="HR">Hours</option>
													<option value="FOZ">Fluid Ounce US</option>
													<option value="MTE">Millitesla</option>
													<option value="HZ">Hertz (1/second)</option>
													<option value="PRC">Group proportion</option>
													<option value="MBA">Millibar</option>
													<option value="CAR">Carton</option>
													<option value="KBK">Kilobecquerel/kilogram</option>
													<option value="HPA">Hectopascal</option>
													<option value="IB">Pikofarad</option>
													<option value="CAN">Canister</option>
													<option value="BOX">BOXES</option>
													<option value="GAU">Gram Gold</option>
													<option value="M3H">Cubic meter/Hour</option>
													<option value="PBG">POLY BAG</option>
													<option value="M3S">Cubic meter/second</option>
													<option value="GAL">US gallon</option>
													<option value="MSC">Microsiemens per centimeter</option>
													<option value="YD">Yards</option>
													<option value="FDR">FIBER DRUM</option>
													<option value="MSE">Millisecond</option>
													<option value="GAI">Gram act. ingrd.</option>
													<option value="WKY">Evaporation Rate</option>
													<option value="PAS">Pascal second</option>
													<option value="GRO">Gross</option>
													<option value="MBZ">Meterbar/second</option>
													<option value="KD3">Kilogram/cubic decimeter</option>
													<option value="22S">Square millimeter/second</option>
													<option value="MS2">Meter/second squared</option>
													<option value="G/L">gram act.ingrd / liter</option>
													<option value="YR">Years</option>
													<option value="CCM">Cubic centimeter</option>
													<option value="KA">Kiloampere</option>
													<option value="M%O">Permille mass</option>
													<option value="BQK">Becquerel/kilogram</option>
													<option value="REL">REEL</option>
													<option value="KJ">Kilojoule</option>
													<option value="CD3">Cubic decimeter</option>
													<option value="KG">Kilogram</option>
													<option value="MPZ">Meterpascal/second</option>
													<option value="BAL">BALES</option>
													<option value="MPS">Millipascal seconds</option>
													<option value="MPT">Mass parts per trillion</option>
													<option value="KM">Kilometer</option>
													<option value="BAG">Bag</option>
													<option value="KW">Kilowatt</option>
													<option value="KT">Kilotonne</option>
													<option value="KV">Kilovolt</option>
													<option value="BRL">Barrel</option>
													<option value="MPG">Miles per gallon (US)</option>
													<option value="LB">US pound</option>
													<option value="GPM">Gallons per mile (US)</option>
													<option value="PCS">Pieces</option>
													<option value="KAI">Kilogram act. ingrd.</option>
													<option value="JCN">JERRICANS</option>
													<option value="MPL">Millimol per liter</option>
													<option value="BAR">bar</option>
													<option value="M%">Percent mass</option>
													<option value="TOM">Ton/Cubic meter</option>
													<option value="MPM">Mass parts per million</option>
													<option value="TON">US ton</option>
												</select></td>
        	<td style="font-size:14px; width: 16%;"><input id="size_no_${index.count}" value="${po.size_no}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="article_no_${index.count}" value="${po.article_no}" class="form-control" /></td>
        	<td style="font-size:14px; width: 16%;"><input id="department_${index.count}" value="${po.department}" class="form-control" /></td>
        	<td style="font-size:14px;"><input id="po_no_${index.count}" value="${po.po_no}" class="form-control" type="hidden" /></td>
		</tr>
		<tr>
			<th>Fedder Vessel Name</th>
			<th>Mother Vessel 1 Name</th>
		 	<th>Mother Vessel 2 Name</th>
		 	<th>Mother Vessel 3 name</th>
		 	<th>Voyage 1/Flight No 1</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="fvsl_${index.count}" value="${po.fvsl}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl1_${index.count}" value="${po.mvsl1}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl2_${index.count}" value="${po.mvsl2}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="mvsl3_${index.count}" value="${po.mvsl3}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="voyage1_${index.count}" value="${po.voyage1}" class="form-control" /></td>
		</tr>
		<tr>
			<th>Voyage 2/Flight No 2</th>
			<th>Voyage 3/Flight No 3</th>
		 	<th>Expected Departure Date</th>
		 	<th>Actual Departure Date</th>
		 	<th>Transhipment 1 ETD</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="voyage2_${index.count}" value="${po.voyage2}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="voyage3_${index.count}" value="${po.voyage3}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="etd_${index.count}" value="${po.etd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="atd_${index.count}" value="${po.atd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment1etd_${index.count}" value="${po.transhipment1etd}" class="form-control" /></td>
		</tr>
			<tr>
			<th>Transhipment 2 ETD</th>
			<th>Transhipment 1 ETA</th>
		 	<th>Transhipment 2 ETA</th>
		 	<th>Expected Arrival Date</th>
		 	<th>Actual Arrival Date</th>
		 	
	 	</tr>
	 	<tr >
        	<td style="font-size:12px; width: 16%;"><input id="transhipment2etd_${index.count}" value="${po.transhipment2etd}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment1eta_${index.count}" value="${po.transhipment1eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="transhipment2eta_${index.count}" value="${po.transhipment2eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="eta_${index.count}" value="${po.eta}" class="form-control" /></td>
        	<td style="font-size:12px; width: 16%;"><input id="ata_${index.count}" value="${po.ata}" class="form-control" /></td>

		</tr>
		</table>
		</div>
		</td>
		</c:forEach>
					</table>
					</div>
					<div style="text-align: center">
						<button type="button" style="width: 19%; margin-bottom: 20px;"
							class="btn btn-primary" onclick="check()">Proceed
							to Booking</button>
					</div>
				</c:if>				
			</div>

	</section>
</form>
<script>

	//id="btn-proceed-to-book"
	$(document).ready(function() {
		$("#to_date").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy",
			defaultDate : ''
		});
		//.datepicker("setDate", new Date());
		$("#from_date").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
		$("#inputfilter").keyup(function(){
			filter = new RegExp($(this).val(),'i');
			$("#po_table tbody #po_search").filter(function(){
				$(this).each(function(){
					found = false;
					$(this).children().each(function(){
						content = $(this).html();
						if(content.match(filter))
						{
							found = true
						}
					});
					if(!found)
					{
						$(this).hide();
					}
					else
					{
						$(this).show();
					}
				});
			});
		});
	});
</script>
<script>
	$(function() {
		$('#buyer option[value= ' + choosen + ']').attr('selected', true);
	});
</script>
<script>
	function hideexpand(varobj) {
		var res = varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');

		$($myGroup)
				.find('.collapse')
				.each(
						function() {
							//alert($(this).attr('id'));
							if (res[1] == $(this).attr('id')) {

								$(this).closest("tr").toggleClass("hide");
								$(this).toggle();

								//alert("entering");
								//alert(document.getElementById("myicon"));
								//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
								//alert(document.getElementById("myicon"));
								if (document.getElementById("#" + res[1]).className == "glyphicon glyphicon-chevron-down") {
									//alert("entering");
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-up";
								} else {
									//document.getElementById("#"+res[1]).removeClass();
									document.getElementById("#" + res[1]).className = "glyphicon glyphicon-chevron-down";
								}

							} else {
							}

						});

	}
</script> 
<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>