
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>


<div class="container-fluid container-fluid-441222">
	<div class="content">
		<div class="row">
			<div class="row row-without-margin">
				<h1 class="h1-report">Commercial Invoice</h1>
				<ol class="breadcrumb">
					<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
					<li class="active">Commercial Invoice</li>
				</ol>
			</div>
			<hr>

			<div class="row">
				<section>
					<div class="container">
						<div style="float: right;">
							<button type="button" class="btn btn-default no-print"
								onclick="printContent('commercial')" id="print">Print</button>
							<!--  <button type="button" class="btn btn-primary" id="pdf">PDF</button> -->
						</div>
					</div>
				</section>
				<br>
				<section id="commercial">
				
					<!--         <section>
            <div class="container">             
             <div class="row"> -->

	<%-- 				<div class="col-xs-12">
					<div class="col-xs-4" style="width: 380px;">
						<div class="input-group-addon commercial">
							<h4>Invoice Details</h4>
						</div>

						<div class="bs-example form-horizontal">

							<div class="form-group">
								<label class="control-label col-xs-4">Invoice No.:</label>
								<div class="col-xs-7 div-small">
									<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvno}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-4">Invoice Date:</label>
								<div class="col-xs-7 div-small">
									<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvdt}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-4">SO Number:</label>
								<div class="col-xs-7 div-small">
									<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].vbeln}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-4">Vendor's Ref.:</label>
								<div class="col-xs-7 div-small">
									<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z028}" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-xs-4">HBL Number:</label>
								<div class="col-xs-7 div-small">
									<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zhblhawno}" />
								</div>
							</div>

						</div>
					</div>
					<div class="col-xs-4" style="width:;">
						<div class="input-group-addon commercial">
							<h4>Vendor</h4>
							<div class="div-large" style="border: 1px solid #000;">
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].name1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputD280pxata.itVendor[0].name2}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].name3}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].name4}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].city1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].country}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].landx}" />
							</div>
						</div>
					</div>

					<div class="col-xs-4" style="width: 380px;">
						<div class="input-group-addon commercial">
							<h4>Maker</h4>
							<div class="div-large">
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].name1}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].name2}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].name3}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].name4}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].city1}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].country}" />
								</br>
								<c:out
									value="${commercialInvoiceJsonOutputData.itMaker[0].landx}" />
							</div>
						</div>
					</div>
				</div> --%>

	<!-- 				

			</div>             
            </div>
           
        </section> -->
					
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="col-xs-4">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Vendor</td>
											</tr>
										</thead>

										<tbody>
											<tr style="background-color: #fff;">
												<td style="border-top: 1px solid #337ab7;">
													<div style="text-align: center; height: 140px;">
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendor[0].name1}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendor[0].name2}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendor[0].name3}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendor[0].name4}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendor[0].city1}" />
														</br>
														<%-- 									<c:out value="${commercialInvoiceJsonOutputData.itVendor[0].country}" /></br> --%>
														<c:out
															value="${commercialInvoiceJsonOutputData.itVendorcountry[0].landx}" />
													</div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-xs-4">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Maker</td>
											</tr>
										</thead>

										<tbody>
											<tr style="background-color: #fff;">
												<td style="border-top: 1px solid #337ab7;">
													<div style="text-align: center; height: 140px;">
														<c:out
															value="${commercialInvoiceJsonOutputData.itMaker[0].name1}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itMaker[0].name2}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itMaker[0].name3}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itMaker[0].name4}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itMaker[0].city1}" />
														</br>
														<%-- 								<c:out value="${commercialInvoiceJsonOutputData.itMaker[0].country}" /></br> --%>
														<c:out
															value="${commercialInvoiceJsonOutputData.itMakercountry[0].landx}" />
													</div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-xs-4">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Invoice Details</td>
											</tr>
										</thead>

										<tbody>
											<tr style="background-color: #fff;">
												<td style="border-top: 1px solid #337ab7;">
													<%-- 									<div class="bs-example form-horizontal">
										<div class="form-group">
											<label class="control-label col-xs-4">Invoice No.:</label>
											<div class="col-xs-7 div-small">
												<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvno}" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-xs-4">Invoice Date:</label>
											<div class="col-xs-7 div-small">
												<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvdt}" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-xs-4">SO Number:</label>
											<div class="col-xs-7 div-small">
												<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].vbeln}" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-xs-4">Vendor's Ref.:</label>
											<div class="col-xs-7 div-small">
												<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z028}" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-xs-4">HBL Number:</label>
											<div class="col-xs-7 div-small">
												<c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zhblhawno}" />
											</div>
										</div>		
									</div> --%>

													<div style="height: 140px;">
														Invoice No.:
														<c:out
															value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvno}" />
														</br>
														<c:set var="zzcomminvdt"
															value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvdt}" />
														Invoice Date :
														<fmt:formatDate pattern="dd-MM-yyyy"
															value="${zzcomminvdt}" />
														</br>
														<%-- 										Invoice Date: <c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzcomminvdt}" /></br> --%>
														SO Number:
														<c:out
															value="${commercialInvoiceJsonOutputData.waHeader[0].vbeln}" />
														</br> Vendor's Ref.:
														<c:out
															value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z028}" />
														</br>
														<%-- 										HBL Number: <c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zhblhawno}" /> --%>
														HBL Number:
														<c:out
															value="${commercialInvoiceJsonOutputData.waHeader[0].zzhblhawbno}" />
													</div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>

								<!--                  <div class="col-xs-12">					 -->
								<%-- 					<div class="col-xs-6" style="width: 550px;">
						<div class="input-group-addon commercial">
							<h4>Buyer</h4>
							<div class="div-large1">
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].name1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].name2}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].name3}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].name4}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].city1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].country}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].landx}" />
							</div>
						</div>
					</div> --%>
								<%-- 					<div class="col-xs-6" style="width: 550px; margin-left: 20px;">
						<div class="input-group-addon commercial">
							<h4>Send To (Consignee)</h4>
							<div class="div-large1">
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].name1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].name2}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].name3}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].name4}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].city1}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].country}" />
								</br>
								<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].landx}" />
							</div>
						</div>
					</div> --%>
								<div class="col-xs-6">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Buyer</td>
											</tr>
										</thead>

										<tbody>
											<tr style="background-color: #fff;">
												<td style="border-top: 1px solid #337ab7;">
													<div class="" style="text-align: center;">
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsignee[0].name1}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsignee[0].name2}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsignee[0].name3}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsignee[0].name4}" />
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsignee[0].city1}" />
														</br>
														<%-- 									<c:out value="${commercialInvoiceJsonOutputData.itConsignee[0].country}" /> --%>
														<c:out
															value="${commercialInvoiceJsonOutputData.itConsigneecountry[0].landx}" />
													</div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-xs-6">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Send To (Consignee)</td>
											</tr>
										</thead>

										<tbody>
											<tr style="background-color: #fff;">
												<td style="border-top: 1px solid #337ab7;">
													<div class="" style="text-align: center;">
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyer[0].name1}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyer[0].name2}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyer[0].name3}" />
														</br>
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyer[0].name4}" />
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyer[0].city1}" />
														</br>
														<%-- 									<c:out value="${commercialInvoiceJsonOutputData.itBuyer[0].country}" /> --%>
														<c:out
															value="${commercialInvoiceJsonOutputData.itBuyercountry[0].landx}" />
													</div>
												</td>

											</tr>
										</tbody>
									</table>
								</div>


								<div class="col-xs-12">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Country of Origin</td>
												<td>Country of Destination</td>
												<td>Incoterm</td>
												<td>Place Of Receipt</td>
												<td>LC Number</td>
												<td>Expiry Date</td>
												<td>Issuing Bank</td>
												<td>Invoice Currency</td>
											</tr>
										</thead>

										<tbody>

											<tr style="background-color: #fff;">
												<td><c:out
														value="${commercialInvoiceJsonOutputData.itCntryorig[0].landx}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.itCntrydest[0].landx}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.itVbkd[0].inco1}, ${commercialInvoiceJsonOutputData.itVbkd[0].inco2}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].zzplacereceipt}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcpottno}" /></br>
													<c:set var="zzlcdt"
														value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcdt}" />
													Date: <fmt:formatDate pattern="dd-MM-yyyy"
														value="${zzlcdt}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcexpdt}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.itIssueBank[0].name1}" /></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.itCostUnit[0].zzunit_cost_unit}" /></td>
											</tr>
										</tbody>
									</table>
								</div>

								<!-- 				</div>    -->


								<div class="col-xs-12">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td>Order Number</td>
												<td>Item Ref.</td>
												<td>Product Description</td>
												<td>H.S. Code</td>
												<td>Set</td>
												<td>Quantity</td>
												<td>Unit Price</td>
												<td>Amount</td>
											</tr>
										</thead>

										<tbody>
											<c:forEach items="${commercialInvoiceJsonOutputData.waItem}"
												var="i">
												<tr style="background-color: #fff;">
													<td>${i.zzponumber}</td>
													<td>${i.zzstyleno}</td>
													<td>${commercialInvoiceJsonOutputData.waHeader[0].desc_z023}</td>
													<td>${i.zzhscode}</td>
													<td></td>
													<td>${i.zztotalnopcs}</td>
													<td>${i.zzunitCost}</td>
													<td>${i.zamount}</td>
												</tr>
											</c:forEach>


											<tr style="background-color: #fff;">
												<td colspan="4"
													style="text-align: right; font-weight: bold;">Total</td>
												<td></td>
												<td><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].ztot_qty}" /></td>
												<td colspan="2" style="padding-left: 90px;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].zztot}" /></td>
											</tr>

											<tr style="background-color: #fff;">
												<td colspan="4"
													style="text-align: right; font-weight: bold;">Remarks</td>
												<td colspan="4"></td>
											</tr>


										</tbody>
									</table>
								</div>

								<div class="col-xs-12">
									<table class="table commercial-table">
										<thead>
											<tr>
												<td style="text-align: left; border: none;">Beneficiary
													Bank, Company Chop and Signature</td>
											</tr>
										</thead>

										<tbody class="beneficiary">
											<tr>
												<td rowspan="6"
													style="background-color: #fff; border: 1px solid #337ab7; color: black">
													<c:out
														value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name1}" />,
													<c:out
														value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name2}" /></br>
													<c:out
														value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name3}" />
													<c:out
														value="${commercialInvoiceJsonOutputData.itSupplierBank[0].city1}" />,
													<c:out
														value="${commercialInvoiceJsonOutputData.itSupplierBank[0].landx}" />

												</td>
												<td
													style="border-bottom: 1px solid #337ab7; border-top: 1px solid #337ab7;">Bank
													Code:</td>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z029}" /></td>
											</tr>

											<tr>
												<td style="border-bottom: 1px solid #337ab7;">EXP NO:</td>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpno}" /></td>

											</tr>

											<tr>
												<td style="border-bottom: 1px solid #337ab7;">EXP Date:</td>
												<%--                    	 <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpdt}" /></td> --%>
												<c:set var="zzexpdt"
													value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpdt}" />
												<%-- 						<fmt:formatDate pattern="dd-MM-yyyy" value="${zzexpdt}" /></br> --%>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><fmt:formatDate
														pattern="dd-MM-yyyy" value="${zzexpdt}" /></td>
											</tr>

											<tr>

												<td style="border-bottom: 1px solid #337ab7;">Account
													Number:</td>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z030}" /></td>
											</tr>

											<tr>

												<td style="border-bottom: 1px solid #337ab7;">Swift
													Transfer:</td>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z032}" /></td>
											</tr>

											<tr>

												<td style="border-bottom: 1px solid #337ab7;">Intermeditory
													Bank:</td>
												<td
													style="background-color: #fff; width: 300px; border: 1px solid #337ab7; color: black;"><c:out
														value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z031} - ${commercialInvoiceJsonOutputData.waHeader[0].desc_z005}" /></td>
											</tr>

											<tr>
												<!--                     <td colspan="4" style="text-align:center;font-weight:bold;">THE TOTAL AMOUNT OF THIS INVOICES INCLUDES A BUYING COMMISSION 6% PAID TO CARREFOUR GLOBAL SOURCING ASIA LTD.</td> -->
												<td colspan="4" style="border: 1px solid #337ab7;"></td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</div>
					</div>
					<%--         <section>
            <div class="container">
            
            <table class="table commercial-table">
             <thead>
               <tr>
                   <td>Country of Origin</td>
                   <td>Country of Destination</td>
                   <td>Incoterm</td>
                   <td>Place Of Receipt</td>
                   <td>LC Number</td>
                   <td>Expiry Date</td>
                   <td>Issuing Bank</td>
                   <td>Invoice Currency</td>
               </tr>
            </thead>
               
               <tbody>
                   
                   <tr style="background-color:#fff;">
                     <td><c:out value="${commercialInvoiceJsonOutputData.itCntryorig[0].landx}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.itCntrydest[0].landx}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.itVbkd[0].inco1}, ${commercialInvoiceJsonOutputData.itVbkd[0].inco2}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzplacereceipt}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcpottno}" /></br>
                     <c:set var="zzlcdt" value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcdt}" /> 						
                     Date: <fmt:formatDate pattern="dd-MM-yyyy" value="${zzlcdt}" /> </td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzlcexpdt}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.itIssueBank[0].name1}" /></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.itCostUnit[0].zzunit_cost_unit}" /></td>
                 </tr>
               </tbody>
            </table>
            </div>
            
        </section> --%>

	<%--         <section>
            <div class="container">
            
            <table class="table commercial-table">
             <thead>
               <tr>
                   <td>Order Number</td>
                   <td>Item Ref.</td>
                   <td>Product Description</td>
                   <td>H.S. Code</td>
                   <td>Set</td>
                   <td>Quantity</td>
                   <td>Unit Price</td>
                   <td>Amount</td>
               </tr>
            </thead>
               
               <tbody>
                   <c:forEach items="${commercialInvoiceJsonOutputData.waItem}" var="i"  >
                   <tr style="background-color:#fff;">
                     <td>${i.zzponumber}</td>
                     <td>${i.zzstyleno}</td>
                     <td>${commercialInvoiceJsonOutputData.waHeader[0].desc_z023}</td>
                     <td>${i.zzhscode}</td>
                     <td></td>
                     <td>${i.zztotalnopcs}</td>
                     <td>${i.zzunitCost}</td>
                     <td>${i.zamount}</td>
                 </tr>
                 </c:forEach>  
                   
                   
                   <tr style="background-color:#fff;">
                     <td colspan="4" style="text-align:right;font-weight:bold;">Total</td>
                     <td></td>
                     <td><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].ztot_qty}" /></td>
                     <td colspan="2" style="padding-left: 90px;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zztot}" /></td>
                 </tr>
                   
                   <tr style="background-color:#fff;">
                     <td colspan="4" style="text-align:right;font-weight:bold;">Remarks</td>
                     <td colspan="4"></td>
                 </tr>
               
               
               </tbody>
            </table>
            </div>
            
        </section> --%>



	<%--         <section>
            <div class="container">
            <div class="row">
            <div class="col-xs-12">   
                
            <table class="table commercial-table">
             <thead>
               <tr>
                   <td style="text-align:left;border:none;">Beneficiary Bank, Company Chop and Signature</td>
                     </tr>
            </thead>
               
               <tbody class="beneficiary">
                   <tr>
                     <td rowspan="6" style="background-color:#fff;border: 1px solid #337ab7;color:black">
                     <c:out value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name1}" />,
                   <c:out value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name2}" /></br>
                   <c:out value="${commercialInvoiceJsonOutputData.itSupplierBank[0].name3}" />
                   <c:out value="${commercialInvoiceJsonOutputData.itSupplierBank[0].city1}" />,
               	   <c:out value="${commercialInvoiceJsonOutputData.itSupplierBank[0].landx}" />
            
                     </td>
                     <td>Bank Code:</td>
                     <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z029}" /></td>
                    </tr>
                   
                   <tr>
                     <td>EXP NO:</td>
                   	 <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpno}" /></td>
                    
                   </tr>
                   
                   <tr>
                     <td>EXP Date:</td>
                   	 <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpdt}" /></td>
						<c:set var="zzexpdt" value="${commercialInvoiceJsonOutputData.waHeader[0].zzexpdt}" />
						<fmt:formatDate pattern="dd-MM-yyyy" value="${zzexpdt}" /></br>
                   	 <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><fmt:formatDate pattern="dd-MM-yyyy" value="${zzexpdt}" /></td>
                    </tr>
                   
                   <tr>
                     
                     <td>Account Number:</td>
                     <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z030}" /></td>
                   </tr>
                   
                   <tr>
                     
                     <td>Swift Transfer:</td>
                     <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z032}" /></td>
                   </tr>
                   
                    <tr>
                     
                     <td>Intermeditory Bank:</td>
                     <td style="background-color: #fff;width:300px;border: 1px solid #337ab7;color:black;"><c:out value="${commercialInvoiceJsonOutputData.waHeader[0].desc_z031} - ${commercialInvoiceJsonOutputData.waHeader[0].desc_z005}" /></td>
                   </tr>
                   
                   <tr>
<!--                     <td colspan="4" style="text-align:center;font-weight:bold;">THE TOTAL AMOUNT OF THIS INVOICES INCLUDES A BUYING COMMISSION 6% PAID TO CARREFOUR GLOBAL SOURCING ASIA LTD.</td> -->
					<td colspan="4"></td>
                   </tr>
                </tbody>
                </table>
                </div>
             </div>
            </div>
            </section> --%>
				</section>
			</div>

			<hr>
		</div>
	</div>
</div>

<!-- ----------------------------- -->

<script>
	function printContent(el) {
		/* var restorepage = $('body').html();
		var printcontent = $('#' + el).clone();
		$('body').empty().html(printcontent);
		window.print();
		$('body').html(restorepage); */
		
		var printContents = document.getElementById(el).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
</script>

<%@include file="footer_v2_fixed.jsp"%>