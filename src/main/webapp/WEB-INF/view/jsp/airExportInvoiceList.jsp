<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
html {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>


<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Freight Invoice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Freight Invoice</li>
		</ol>
	</div>
	<hr>
	<c:if test="${empty invoiceListJsonData.invoice}">		
		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px;">
				<div class="text-center" style="color: red; font-weight: bold;">No Record found!</div>
			</div>	
			<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:10px;">
				<div class="text-center">
					<a class="btn btn-primary" href="${pageContext.request.contextPath}/getAirExportInvoice">Search Again</a>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${not empty invoiceListJsonData.invoice}">
		<div class="row">
			<!-- Left col -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<!-- MAP & BOX PANE -->
				<div class="box box-myshipment">
					<div class="box-header with-border">
						<h1 class="box-title"
							style="font-size: 26px; font-family: 'Quicksand', sans-serif; padding-bottom: 0px !important;">Air Export Invoice List</h1>
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding" style="display: block;">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div class="pad">



									<table class="table table-striped">
										<thead>
											<tr>
												<th>Invoice Number</th>
												<th>Type</th>
											</tr>
										</thead>
										<tbody>
											<tr class="">
												<c:forEach items="${invoiceListJsonData.invoice}" var="i">
													<tr>
														<td><c:url value="/getAirExportInvoiceDetail"
																var="invoiceDetail">
																<c:param name="param" value="${i.vbeln}" />
															</c:url> <a href="<c:out value="${invoiceDetail}"/>" target="blank">${i.vbeln}</a></td>
														<c:if test="${i.knumv =='ZSCM'}">
															<td>Invoice</td>
														</c:if>
														<c:if test="${i.knumv !='ZSCM'}">
															<td>Debit Note</td>
														</c:if>
													</tr>
												</c:forEach>
											</tr>

										</tbody>
									</table>

								</div>
							</div>
						</div>
						<!-- /.row -->
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>
	</c:if>
</div>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>
