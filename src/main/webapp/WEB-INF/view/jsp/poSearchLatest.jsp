<%@include file="header_v2.jsp" %>
<%@include file="navbar.jsp" %> 
		<section class="main-body">
		<div class="well assign-po-search">
		<form:form action="searchPo" method="post" commandName="searchPoDTO">
		<div class="row">
          <div class="col-sm-12">
          <h4 style="text-align: center; color: green;"><i>${message}</i></h4>
          </div>
         </div>
      <fieldset>
          <legend>PO Search</legend>
          
                <div class='row' >
                    
                   <div class='col-sm-2'>    
                        <div class='form-group'>
                        <label>PO Number</label>
                        <form:input path="poNo"  id="user_title" cssClass="form-control"/>
                        
                       <!--  <input class="form-control" id="user_title" name="user[title]"  type="text" /> -->
                        </div>
                   </div>
                   <div class='col-sm-2'>
                       <div class='form-group'>
                       <label>From Date</label>
                       <form:input path="fromDate" id="" cssClass="date-picker form-control glyphicon glyphicon-calendar" />
                       
                       <!-- <input id="" type="text" class="date-picker form-control glyphicon glyphicon-calendar" /> -->
                   </div>
                  </div>
                  <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>To Date</label>
                   <form:input path="toDate" id="" cssClass="date-picker form-control glyphicon glyphicon-calendar" />
                   
                  <!--  <input id="" type="text" class="date-picker form-control glyphicon glyphicon-calendar" /> -->
                 </div>
                 </div>
                  <!--    <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>Supplier Code</label>
                   <input class="form-control" id="user_lastname" name="user[lastname]" required="true" size="30" type="text" />
                 </div> 
                 </div> -->
                  <!-- <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>Sku No</label>
                   <input class="form-control" id="user_lastname" name="user[lastname]" required="true" size="30" type="text" />
                 </div>
                 </div> -->
				  <div class='col-sm-2' style="margin-top: 2%;">
                  <div class='form-group'>
                   
                   <button type="submit" class="btn btn-success form-control">Search</button>
                 </div>
                 </div>
                    
                 </div>
             
</fieldset>
</form:form>
		</div>
		<div class="container-fluid table-responsive">
			<div class="row table_header">
				<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
					<span class="glyphicon glyphicon-triangle-bottom"></span><h3>Search Result</h3>
				</div>
				
			</div>
			<div class="row">
		<table class="table table-striped">
		  <thead style="">
      <tr>
	    
       <!--  <th>Po No</th>
        <th>Client Name</th>
        <th>Product Number</th>
		<th>Sku-No</th>
		<th>Color</th>
		<th>Size</th>
		<th>Sales Org</th>	
		<th>PO Created On</th>	
		<th>Action</th> -->
		<th style="padding-left:25px;">Po No</th>
        <th>Product Number</th>
        <th>Style No</th>
		<th>Sku-No</th>
		<th>Color</th>
		<th>Size</th>
		<th>Total Pieces</th>	
		<th>PO Created On</th>	
		<th>
		<table>
		<tbody>
		 <tr><td colspan="3" style="padding-left: 25%;">Action</td></tr>
		 <tr>
		 <td style="padding-right: 5px;">Update</td>
		 <td>|</td>
		 <td style="padding-left: 5px;">Segregate</td>
		 </tr>
		</tbody>
		</table>
		</th>
      </tr>
    </thead>
    <tbody>
      <tr class="">
	  <c:forEach items="${purchaseOrderLst}" var="i"  >
	<tr>
		<td style="padding-left:25px;">${i.vc_po_no}</td>
    		<td>${i.vc_product_no}</td>
    		<td>${i.vc_style_no}</td>
    		<td>${i.vc_sku_no}</td>
    		<td>${i.vc_color}</td>
    		<td>${i.vc_size}</td>
    		<td>${i.vc_tot_pcs}</td>
    		 <td>${i.formattedPoCreationDate}</td> 
    		<td>
    			  <table> 
    			  <tbody>
    			  <tr>
    			   
    			   <td style="padding-right: 10px;"><a href="getUpdatePoPage?seg_id=${i.seg_id}&param1=${searchPoDTO.poNo}&param2=${searchPoDTO.fromDate}&param3=${searchPoDTO.toDate}" >
    			   <span class="glyphicon glyphicon-edit"></span>
    			   </a></td>
    			   
    			   <td style="padding-left: 40px;"><a href="getSegregationPage?seg_id=${i.seg_id}&param1=${searchPoDTO.poNo}&param2=${searchPoDTO.fromDate}&param3=${searchPoDTO.toDate}" >
    			   <span class="glyphicon glyphicon-adjust"></span>
    			   </a></td>
    			   
    			  </tr>
    			   
    			   <!-- <input type="submit" value="Segregate"></a> -->
    			   </tbody>
    			</table>
    			
    		</td>
	</tr>
	</c:forEach>
      </tr>
      
    </tbody>
  </table>
  <!-- <div style="text-align:center">
  <button type="button" style="width: 19%;" class="btn btn-primary btn-lg">Approve</button>
  </div> -->
  </div>

		</div>
		</section>
	
	<%@include file="footer_v2_fixed.jsp" %>
	
	<script>
		$(document).ready( function() {
		$(".date-picker").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", "");
})

		</script>	
		
