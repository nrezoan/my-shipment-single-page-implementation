<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>

<style>
	html {
		background-color: #fff;
	}
	
	.input-group-addon {
		border: unset !important;
	}
</style>
<div class="col-xs-12" style="height: 10px;"></div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="col-xs-12" style="height: 10px;"></div>

			<div class="input-group-addon">
				<h4
					style="background-color: #337ab7; padding-top: 7px; padding-left: 10px; text-align: center; font-weight: bold; height: 35px; margin-top: 10px; color: #fff;">PO
					Update</h4>
			</div>
		</div>
	</div>
</div>
<div class="col-xs-12" style="height: 20px;"></div>
<section class="container uploadForm">
	<form:form action="updateThePo" method="post"
		commandName="segPurchaseOrder" cssClass="form-horizontal">
		<div>
			<div class="col-md-4">
				<label>PO Number</label>
				<form:input path="vc_po_no" cssClass="form-control" id="user_title"
					readonly="true" />
			</div>
			<div class="col-md-4">
				<label>Division</label>
				<form:input path="vc_division" cssClass="form-control" />
			</div>
			<div class="col-md-4">
				<label>Product Number</label>
				<form:input path="vc_product_no" cssClass="form-control" />
			</div>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>Style No</label>
				<form:input path="vc_style_no" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Sku-No</label>
				<form:input path="vc_sku_no" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Color</label>
				<form:input path="vc_color" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>Size</label>
				<form:input path="vc_size" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Total Pieces</label>
				<form:input path="vc_tot_pcs" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>HS Code</label>
				<form:input path="vc_hs_code" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>No Pcs Ctns</label>
				<form:input path="nu_no_pcs_ctns" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Article No</label>
				<form:input path="vc_article_no" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>POL</label>
				<form:input path="vc_pol" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>POD</label>
				<form:input path="vc_pod" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Commodity</label>
				<form:input path="vc_commodity" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Quantity</label>
				<form:input path="vc_quan" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>Length</label>
				<form:input path="nu_length" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Width</label>
				<form:input path="nu_width" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Height</label>
				<form:input path="nu_height" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>CBM Sea</label>
				<form:input path="vc_cbm_sea" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Volume</label>
				<form:input path="vc_volume" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Gross Weight</label>
				<form:input path="vc_gr_wt" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>Net Weight</label>
				<form:input path="vc_nt_wt" cssClass="form-control" />
			</div>
			<div class="col-md-4">
				<label>Ref Field1</label>
				<form:input path="vc_ref_field1" cssClass="form-control" />
			</div>
			<div class="col-md-4">
				<label>Ref Field2</label>
				<form:input path="vc_ref_field2" cssClass="form-control" />
			</div>
			<br>
		</div>
		<div class="col-xs-12" style="height: 15px;"></div>
		<div>
			<div class="col-md-4">
				<label>Ref Field3</label>
				<form:input path="vc_ref_field3" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Ref Field4</label>
				<form:input path="vc_ref_field4" cssClass="form-control" />
			</div>
			<br>
			<div class="col-md-4">
				<label>Ref Field5</label>
				<form:input path="vc_ref_field5" cssClass="form-control" />
				<form:hidden path="seg_id" />
				<form:hidden path="paramPoNo" />
				<form:hidden path="paramFromDate" />
				<form:hidden path="paramToDate" />
			</div>
			<br>
		</div>
		<!-- <div class="col-xs-12" style="height:15px;"></div>
	   <div>
          <div class="col-md-4">
          <label>Input-1</label>
          <input type="text" class="form-control">
          </div><br>
          <div class="col-md-4">
          <label>Input-1</label>
          <input type="text" class="form-control">
          </div><br>
          <div class="col-md-4">
          <label>Input-1</label>
          <input type="text" class="form-control">
          </div><br>
      </div> -->
		<div class="col-xs-12" style="height: 10px;"></div>
		<div class="centered">
			<input type="submit" class="btn btn-info center-block" value="Submit"
				style="font-weight: bold; font-size: 14px;">
		</div>
		<!--<div class="row input_area">
				<button id="line_item_button" type="submit" name="singlebutton" class="btn btn-success btn-lg center-block">Submit</button>
			</div>-->
	</form:form>
</section>
<%@include file="footer_v2_fixed.jsp"%>
