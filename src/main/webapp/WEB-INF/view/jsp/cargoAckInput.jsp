
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html {
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Cargo Acknowledgement Certificate</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Cargo Acknowledgement Certificate</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form  action="cargoAck" method="post" commandName="req"
			cssClass="form-incline">
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">HBL Number</label>
					<form:input path="req1" id="number" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" id="btn-appr-po-search reportSearchBtn"
					name="btn-appr-po-search" onclick="return reportSearch();"
					class="btn btn-primary btn-block" value="View" />
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" name="download" id="download"
					onclick="return reportSearch();" class="btn btn-success btn-block"
					value="Download" />
			</div>
			<div class='col-md-6 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label style="color: red; margin-top: 5%;">${message}</label>
				</div>
			</div>
		</form:form>
	</div>

	<hr>
	<br>
</div>

<script>
	function reportSearch() {
		var hblnumber = $("#number").val();
		if (hblnumber == "") {
			swal("HBL Number", "HBL number cannot be empty!", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>