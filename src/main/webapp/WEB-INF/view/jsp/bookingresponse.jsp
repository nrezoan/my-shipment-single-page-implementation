<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html {
	background-color: #fff;
}
</style>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/pre-alert-management.css"></link>

<script src="${pageContext.request.contextPath}/resources/js/pre-alert-management.js"></script>

<script>
$(document).ready(function(){

	// Get saved data from sessionStorage
	var processStartdata = localStorage.getItem('directBooking');
	var processEnddata = new Date();
	//alert(processStartdata);
	//alert(processEnddata);
	var processingTime = Math.abs(processEnddata.getTime() - processStartdata.getTime());
	var hours   = Math.floor(processingTime / 3600);
    var minutes = Math.floor((processingTime - (hours * 3600)) / 60);
    var secons = processingTime - (hours * 3600) - (minutes * 60);
	//alert(hours+":"+minutes+":"+secons);
	var out=document.getElementById("processingTime");
	out.innerHTML=hours+":"+minutes+":"+secons; 
	// Remove saved data from sessionStorage
	localStorage.removeItem('directBooking');

	// Remove all saved data from sessionStorage
	localStorage.clear();
	
})
</script>

<section>
	<div class="container" style="height: 20px;">
		<!-- <div style="float:right;">
             <button type="button" class="btn btn-default" id="print">Print</button>
             <button type="button" class="btn btn-primary" id="pdf">PDF</button>
             </div> -->
	</div>
</section>
<c:if test="${not empty jsonResponse.zsalesdocument}">
	<section>
		<div class="container">
			<div class="panel-group">
				<div class="panel panel-success">
					<div class="panel-heading"
						style="font-size: 20px; font-weight: bold; text-align: center;">
						<i class="fa fa-check-circle-o" aria-hidden="true"></i> You Have
						Successfully Placed Booking
					</div>
					<div class="panel-body">
						<div
							style="font-size: 18px; font-weight: bold; text-align: center; height: 38px;">Booking
							Information</div>
<%-- 						<table class="table ">
							<tbody>
								<tr>
									
									<td style="text-align: center;">HBL NO.</td>
									<td><a href="shippingOrderFromUrl?searchString=${ jsonResponse.hblnumber}">${ jsonResponse.hblnumber}</a>
										<span style="color: darkorange;"> (Please Click to Download/View Shipping Order)</span>
									</td>
								</tr>
								<tr>
									
									<td style="text-align: center;">DOCUMENT NO.</td>
									<td>${ jsonResponse.zsalesdocument}</td>

								</tr>
							</tbody>
						</table> --%>
						<div class="row">
							<div class="col-md-5">
								<p style="float: right">HBL/HAWB NO.</p>
							</div>
							<div class="col-md-7">
								<p>
								<a href="shippingOrderFromUrl?searchString=${ jsonResponse.hblnumber}">${ jsonResponse.hblnumber}</a>
										<span style="color: darkorange;"> (Please Click to Download/View Shipping Order)</span>
								</p>		
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<p style="float: right">DOCUMENT NO.</p>
							</div>
							<div class="col-md-7">
								<p>${jsonResponse.zsalesdocument}</p>
							</div>
						</div>
						<%-- <c:if test="${jsonResponse.division == 'SE'}">
							<div class="row">
								<div class="col-md-5">
									<p style="float: right">Vessel Information</p>
								</div>
								<div class="col-md-7">
									<p>${jsonResponse.fvsl}</p>
								</div>
							</div>
						</c:if> --%>
						
						<%-- <c:if test="${jsonResponse.hblnumber != ''}">
							<div class="row">
								<div class="col-md-5">
									<p style="float: right">Upload Files(Pre-Alert Documents)</p>
								</div>
								<div class="col-md-7">
									<c:if test="${preAlertSaveBLStatus != null}">
										<c:choose>
											<c:when test="${preAlertSaveBLStatus.saveBLStatusError == ''}">
												<a data-toggle="modal" data-target="#updateFormModal" 
													class="button btn btn-warning btn-sm" 
													onclick="setUploadFilesValue('${jsonResponse.hblnumber}', '${preAlertSaveBLStatus.bookingSupplier}', '${preAlertSaveBLStatus.bookingBuyer}', '${preAlertSaveBLStatus.bookingNotify}', '')">
													<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload
												</a>
												<span id="preAlertDocUploadHints" style="color: red;"></span>
												<script>
													var directBookingIsPreAlertDocExist = '${preAlertSaveBLStatus.isPreAlertDoc}';
													if(directBookingIsPreAlertDocExist == '0') {
														$('#preAlertDocUploadHints').text('(Please Upload Scanned Copy of Commercial Invoice and Packing List)');
													} else if(directBookingIsPreAlertDocExist == '1') {
														$('#preAlertDocUploadHints').text('File Uploaded!');
													}
												</script>
											</c:when>
											<c:when test="${saveBLStatusError != ''}">
												<span style="color: red;">${preAlertSaveBLStatus.saveBLStatusError}</span>
											</c:when>
										</c:choose>
									</c:if>
									<c:if test="${preAlertSaveBLStatus == null}">
										<span style="color: red;">Something went Wrong! Please Contact with your respective CRM</span>
									</c:if>
									
									
								</div>
							</div>
						</c:if> --%>
						<div class="alert alert-info-248435">
  							<strong>NB:</strong> Please note without printed copy of Shipping Order, No Trucks will be allowed to enter CFS/Warehouse.
						</div>						
					</div>
				</div>
			</div>
		</div>
	</section>
</c:if>

<c:if test="${empty jsonResponse.zsalesdocument}">
	<section>
		<div class="container">
			<div class="panel-group">
				<div class="panel panel-danger">
					<div class="panel-heading"
						style="font-size: 20px; font-weight: bold; text-align: center;">
						<i class="fa fa-times-circle-o" aria-hidden="true"></i> Error
						Occured, Booking Was Not Placed
					</div>
					<div class="panel-body">

						<table class="table ">
							<thead>
								<tr style="font-weight: bold;">
									<td>Id</td>
									<td>Type</td>
									<td>Code</td>
									<td>Message</td>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${ jsonResponse.bapiReturn2}" var="bapiReturn">
									<tr>
										<td>${bapiReturn.id }</td>
										<td>${bapiReturn.type }</td>
										<td>${bapiReturn.number }</td>
										<td>${bapiReturn.message }</td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</c:if>

<jsp:include page="preAlertUploadModal.jsp"></jsp:include>

<%@include file="footer_v2_fixed.jsp"%>