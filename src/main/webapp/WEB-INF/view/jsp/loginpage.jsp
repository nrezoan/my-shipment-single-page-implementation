<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>myshipment login</title>
<!-- Bootstrap CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/transparent-login.css">
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700'
	rel='stylesheet' type='text/css'>
<!-- <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"	rel="stylesheet" type="text/css" /> -->

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.2/modernizr.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
	sessionStorage.setItem('login_dashboard', new Date());

	// Get saved data from sessionStorage
	var data = sessionStorage.getItem('login_dashboard');
	//alert(data);
	// Remove saved data from sessionStorage
	sessionStorage.removeItem('key');

	// Remove all saved data from sessionStorage
	sessionStorage.clear();
</script>

<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script',
			'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-90249127-1', 'auto');
	ga('send', 'pageview');
</script>

</head>
<body>
	<%-- 	<div>
		<nav class="navbar credentials-nav-356369">
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4"></div>
					<div class="col-md-4" style="text-align: center;">
						<a class="navbar-brand-295154" href="#"> <img
							class="nav-img-447749"
							src="${pageContext.request.contextPath}/resources/images/logo-xs.png" /></a>
					</div>
					<div class="col-md-4"></div>
				</div>
			</div>
		</nav>
	</div> --%>
	<form:form action="${pageContext.request.contextPath}/selectedorg"
		modelAttribute="loginMdl" method="post">
		<div class="login-wrap">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-10">

							<div class="login-html">
								<div class="row">
									<div class="col-md-12" style="text-align: center;">
										<a href="${pageContext.request.contextPath}\homepage"> <%-- <img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="Logo"> --%>
											<img
											src="${pageContext.request.contextPath}/resources/images/logo-xs.png"
											alt="myshipment logo"></img>
										</a><br>
									</div>
								</div>
								<div class="hr-blue"></div>
								<div class="row">
									<div class="col-md-12" style="text-align: center; color: #FFF;">
										<h4>login to continue to myshipment</h4>
									</div>
								</div>

								<input id="tab-1" type="radio" name="tab" class="sign-in"
									checked> <label for="tab-1" class="tab"
									style="display: none;">login to continue to myshipment
								</label> <input id="tab-2" type="radio" name="tab" class="sign-up">
								<label for="tab-2" class="tab" style="display: none;">Sign
									Up </label>


								<div class="login-form">
									<div class="sign-in-htm">
										<div class="group">
											<label for="user" class="label">Customer ID</label>
											<!-- <input id="user" type="text" class="input"> -->
											<input type="text" class="input" id="customerCode"
												name="customerCode" required="required"
												title="Please Enter Customer ID" maxlength="10">
										</div>
										<div class="group">
											<label for="pass" class="label">Password</label>
											<!-- <input id="pass" type="password" class="input" data-type="password"> -->
											<input type="password" class="input" id="password"
												name="password" required="required"
												title="Please Enter Password">
										</div>
										<div class="group">
											<!-- <input id="check" type="checkbox" class="check"> -->
											<input type="checkbox" class="check" name="check" id="check">
											<label for="check"> <span class="icon"></span>
												Remember me
											</label>
										</div>
										<div class="row">
											<div class="col-md-6">
												<!-- <input type="submit" class="button-589814" value="Sign In"> -->
												<button type="submit" class="button-589814">Login</button>
											</div>
											<div class="col-md-6"
												style="text-align: right; padding-top: 12px;">
												<a href="#forgot" style="text-decoration: none;">Forgot
													Password?</a>
											</div>
										</div>
										<div class="hr"></div>
										<div class="foot-lnk">
											<p>
												Copyright &copy;
												<script type="text/javascript">
													var today = new Date()
													var year = today
															.getFullYear()
													document.write(year)
												</script>
												<a href="www.myshipment.com" style="text-decoration: none;">myshipment</a>
											</p>
										</div>
									</div>

								</div>

							</div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>

		</div>
	</form:form>
</body>

<%-- <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MyShipment</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
	  
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/style.css">
		 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/graph.css">
		 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main-shipping.css">
		<link rel="stylesheet" type="text/css" media="print" href="${pageContext.request.contextPath}/resources/css/print.css">
		 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> 
		<!-- modernizr JS
		============================================ -->		
        <script src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
		<style>
		.@media (min-width: 992px) {
                .modal-dialog {
    width: 65%;
    margin-top: 5%;
  }}
  .right-li{
  font-size: 15pt;
  }
  .lead{
  font-size: 24pt;
  }
  .modal-header{
  background-color: #222534;
    padding: 12px 0;
  }
  .modal-title{
  padding:1.3%;
  }
  .modal-content{
   margin-top: 7%;}
		</style>
		<script>
		sessionStorage.setItem('login_dashboard', new Date());

		// Get saved data from sessionStorage
		var data = sessionStorage.getItem('login_dashboard');
		//alert(data);
		// Remove saved data from sessionStorage
		sessionStorage.removeItem('key');

		// Remove all saved data from sessionStorage
		sessionStorage.clear();
		</script>
		
		<script>
  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  		ga('create', 'UA-90249127-1', 'auto');
  		ga('send', 'pageview');
	</script>
    </head>
    <body onload="zoom();" background="${pageContext.request.contextPath}/resources/images/bodyBG3.jpg">
     <div id="login-overlay" class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
             
              <h4 class="modal-title" id="myModalLabel"><img style="max-width:40%;" src="${pageContext.request.contextPath}/resources/images/logo.png" /></h4>
          </div>
          <div class="modal-body">
              <div class="row">
               <div class="col-xs-2"></div>
                  <div class="col-xs-8">
                      <div class="well" style="margin-top: 45px;">
                         <!--   <form id="loginForm" method="POST" action="/login/" novalidate="novalidate">-->
                           <form:form class="middle-section-form"  action="${pageContext.request.contextPath}/selectedorg" modelAttribute="loginMdl" method="post">
                              <div class="form-group">
                                  <label for="username" class="errorMsg" style="color:red;">${error}</label>
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="username" class="control-label">Customer ID</label>
                                  <input type="text" class="form-control" id="customerCode" name="customerCode" value="" required="required" title="Please enter your customer id" placeholder="Customer Id" maxlength="10">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">Password</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="required" title="Please enter your password" placeholder="Password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember" value="on"> Stay signed in
                                  </label>
                                  
                              </div>
                              <button type="submit" class="btn btn-success btn-block" style="background-color: #222534;">Login</button>
                              
                          </form:form>
                      </div>
                  </div>
                  <div class="col-xs-2"></div>
                  </br>
                  </br>
                  </br>
                 
				  
              </div>
			 
                   	<form:form action="${pageContext.request.contextPath}/getCommonTrackingDetailInfo" method="post" commandName="trackingRequestParam" >
					 <div class="row" style="padding: 0 2% 0 2%;margin-top: 60px;">
					 
					 <div class="col-md-12 well">
					 <div class="col-md-5" style="margin-top: 1%;font-size: 1.3em;">
					 <strong>Shipment Tracking : </strong> 
					 </div>
                      <div class="col-md-5">
					 <input type="text" class="form-control"  title="Please enter your HBL Number" name="blNo" placeholder="HBL Number">
					 </div>
					 <div class="col-md-2">
					 <button type="submit" class="btn btn-success" style="background-color: #222534;">Track</button>
					 </div>
					 </div>
					 </div>
					 
               		</form:form>
          </div>
      </div>
  </div>
		

		<!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
		<!-- main JS
		============================================ -->
		
<script src="js/canvasjsmin.js"></script>		
        <script src="js/main.js"></script>
		
	
    </body>
</html>
 --%>