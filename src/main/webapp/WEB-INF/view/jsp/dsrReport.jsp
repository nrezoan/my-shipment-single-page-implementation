<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> --%>
<style>
	html, body {
		background-color: #fff;
	}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Daily Sales Report</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Daily Sales Report</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getDSRdetails" method="post" commandName="dsrParam" target="_blank" 
			cssClass="form-incline">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class='form-group'>
							<label>Invoice Date</label>
							<form:input path="searchDate" id="searchDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">				
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" id="btn-appr-po-search reportSearchBtn" name="btn-appr-po-search" onclick="return reportSearch();"	class="btn btn-primary btn-block" value="Search" />
			</div>	 
		</form:form>
	</div>
	<div class="container-fluid table-responsive">
		<div class="row search-result-datatable">
			<div class="col-md-12">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover display"
						id="hawb_tables">
						<thead style="background-color: #222534; color: #fff;">
							<tr>
								<th>Invoice No</th>
								<th>Invoice Date</th>
								<th>MAWB 1</th>
								<th>MAWB 2</th>
								<th>Status</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="footer_v2_fixed.jsp"%>
<%-- <script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script> --%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>
<script>
	function reportSearch() {
		var date = $("#searchDate").val();
		if (date == "") {
			swal("Report Date", "Date field cannot be empty!", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<script>
	$(document).ready(function() {
		$("#searchDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});

		$('.date-picker').each(function() {
			$(this).removeClass('hasDatepicker').datepicker({
				dateFormat : "dd.mm.yy"
			});
		});
	});
	$('#hawb_tables').DataTable({
		responsive : true,
		dom : 'Bfrtip',
		buttons : [ 'csv', 'excel'

		]
	});
</script>
<!-- ----------------------------- -->
