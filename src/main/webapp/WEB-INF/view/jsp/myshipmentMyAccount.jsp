<!-- header -->
<%@include file="header_v2.jsp" %>
<!-- navbar -->
<%@include file="navbar.jsp" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- Content Starts -->

<style>

	html {
		background-color: #fff;
	}
	
	.btn {
		border-radius: 0px !important;
	}
	
	/* .swal-overlay {
	  background-color: rgba(43, 165, 137, 0.45);
	} */
	
	.sweet-alert button.cancel {
		background-color: #ea6239;
		color: white
	}
	
	.sweet-alert button.cancel:active {
	    background-color: #f44330;
	}
	
	.sweet-alert button.cancel:hover {
	    background-color: #f44330;
	}
	
	@media (min-width: 320px) and (max-width: 1000px) {
		.tabs-content-right {
			width: 100% !important;
		}
		.nav-tabs.nav-justified-custom {
		  width: 100%;
		  border-bottom: 0;
		}
		.nav-tabs.nav-justified-custom > li {
		  float: left !important;
		}
		.nav-tabs.nav-justified-custom > li > a {
		  margin-bottom: 5px;
		  text-align: center;
		}
		.nav-tabs.nav-justified-custom > .dropdown .dropdown-menu {
		  top: auto;
		  left: auto;
		}
		@media (min-width: 768px and min-width: 981px) {
		  .nav-tabs.nav-justified-custom > li {
		    display: table-cell;
		    width: 1%;
		  }
		  .nav-tabs.nav-justified-custom > li > a {
		    margin-bottom: 0;
		  }
		}
		.nav-tabs.nav-justified-custom > li > a {
		  margin-right: 0;
		  border-radius: 4px;
		}
		.nav-tabs.nav-justified-custom > .active > a,
		.nav-tabs.nav-justified-custom > .active > a:hover,
		.nav-tabs.nav-justified-custom > .active > a:focus {
		  border: 1px solid #ddd;
		}
		@media (min-width: 768px and min-width: 981px) {
		  .nav-tabs.nav-justified-custom > li > a {
		    border-bottom: 1px solid #ddd;
		    border-radius: 4px 4px 0 0;
		  }
		  .nav-tabs.nav-justified-custom > .active > a,
		  .nav-tabs.nav-justified-custom > .active > a:hover,
		  .nav-tabs.nav-justified-custom > .active > a:focus {
		    border-bottom-color: #fff;
		  }
		}
		
		.tabs-left .fa {
			font-size: 15px !important;
		}
		.account-tab-text {
			font-size: 12px !important;
		}
		.nav-tabs.tabs-left > li {
			margin-right: 0% !important;
		}
	}
	
	.tabs-left {
		width: 15%;
		padding-top: 1%;
	}
	
	.tabs-content-right {
		width: 85%;
	}
	
	.nav-tabs.tabs-left {
		/* border: 1px solid #eaeaea; */
		border-right: 1px solid #eaeaea;
		border-radius: 0px;
		/* border-right: 1px solid #ddd; */
	}
	.nav-tabs.tabs-left > li {
		float: none;
		margin-bottom: 0;
		/* margin-right: -1px; */
		margin-right: -8%;
	}
	.nav-tabs.tabs-left > li > a {
		margin-right: 0;
		margin-bottom: 2px;
		border: 1px solid transparent;
		border-radius: 4px 0 0 4px;
	}
	.nav-tabs.tabs-left > li.active > a {
		border: 1px solid #ddd;
		border-right-color: transparent;
	}
	.tabs-left li {
		text-align: center;
	}
	.tabs-left .fa {
		/* font-size: 50px; */
		font-size: 25px;
	}
	.account-tab-text {
		font-size: 14px;
	}
	
	.nav-tabs-custom > .nav-tabs.tabs-left > li.active {
	    border-top-color: #252525;
	    background-color: #eaeaea;
	    border: 0px solid #252525;
	    /* border: 5px solid #252525; */
	    /* border: 5px solid #d2d6de; */
	}
	
	.nav-tabs-custom > .nav-tabs.tabs-left > li.active > a {
		background-color: #eaeaea !important;
	}
	
	.nav-tabs-custom > .nav-tabs > li > a, .nav-tabs-custom > .nav-tabs > li > a {
	    margin: 0;
	}
	
	.tab-content.tabs-content-right {
		border: unset;
	}
	
	.box {
		border-radius: 0px !important;
	    /* border-top: 3px solid #d2d6de; */
	    border-top: 0px solid #fff;
	    box-shadow: 5px 5px 10px 5px rgba(0, 0, 0, 0.2);
	}
	
	.bg-myshipment-active {
	    background-color: #252525 !important;
	}
	
	.wallet-header {
		font-size: 20px !important;
	}
	
	.default-company-active-dropdown {
		background-color: #fff !important;
	}
	
	.default-company-active-dropdown li a:hover {
	    /* background-color: #252525 !important; */
	    background-color: rgba(37, 37, 37, 0.38) !important;
	    color: #fff !important;
	}
	
	.default-company-active-dropdown li a.active {
	    background-color: #252525 !important;
	    color: #fff !important;
	}
	
	.default-company-active-dropdown>li>a {
	    color: #252525 !important;
	}
	
	.dropdown-menu {
	    min-width: 80px !important;
	}
	
	.dropdown-menu>li>a {
	    padding: 4px 15px !important;
	}

</style>

		<div class="nav-tabs-custom row row-without-margin" style="min-height: 901px;">
		<hr>
            <ul class="nav nav-tabs tabs-left nav-justified-custom col-md-2" style="min-height: 100%; height: fit-content;">
              <li class="active">
              	<a href="#tab_1" data-toggle="tab">
              		<i class="fa fa-user" aria-hidden="true"></i>&nbsp;<span class="account-tab-text">My Account</span>
              	</a>
              </li>
              <li>
              	<a href="#tab_2" data-toggle="tab">
              		<i class="fa fa-key" aria-hidden="true"></i>&nbsp;<span class="account-tab-text">Security</span>
              	</a>
              </li>
              <li>
              	<a href="#tab_3" data-toggle="tab">
              		<i class="fa fa-briefcase" aria-hidden="true"></i>&nbsp;<span class="account-tab-text">Company</span>
              	</a>
              </li>
            </ul>
            <div class="tab-content tabs-content-right col-md-10 col-sm-12 col-xs-12	">
              <div class="tab-pane active" id="tab_1">
              	<div class="row row-without-margin" style="display: none;">
					<h1 class="h1-report"></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-cogs"></i>&nbsp;Settings</li>
						<li><i class="fa fa-user active"></i>&nbsp;My Account</li>
					</ol>
				</div>
                <div class="row">
					<div class="col-md-6">
						<div class="box box-widget widget-user">
							<!-- Add the bg color to the header using any of the bg-* classes -->
							<div class="widget-user-header bg-aqua-active bg-myshipment-active">
								<h3 class="widget-user-username text-center" id="userCompany"></h3>
								<h5 class="widget-user-desc"></h5>
							</div>
							<div class="widget-user-image">
								<img class="img-circle" style="border: 0px solid #fff !important;" src="${pageContext.request.contextPath}/resources/images/myaccount-avatar.png"
									alt="User Avatar">
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-sm-12">
										<div class="description-block">
											<h3 class="description-header wallet-header">myshipment Wallet</h3>
											<span class="description-text">coming soon</span>
										</div>
										<!-- /.description-block -->
									</div>
									
								</div>
								<!-- /.row -->
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="box box-myshipment">
							<div class="box-header">
								<h3 class="box-title">Most Used Company in myshipment</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th class="text-center">Sales Organization</th>
											<th class="text-center">Distribution Channel</th>
											<th class="text-center">Division</th>
											<th class="text-center">% of Usage</th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${fn:length(companyUsagePercentageList) gt 0}">
												<c:if test="${not empty companyUsagePercentageList}">
													<c:forEach items="${companyUsagePercentageList}" var="company">
														<tr>
															<td class="text-center">${company.companyName}</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${company.distributionChannel == 'EX'}">Export</c:when>
																	<c:when test="${company.distributionChannel == 'IM'}">Import</c:when>
																	<c:when test="${company.distributionChannel == 'CH'}">CHA</c:when>
																</c:choose>
															</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${company.division == 'SE'}">SEA</c:when>
																	<c:when test="${company.division == 'AR'}">AIR</c:when>
																</c:choose>
															</td>
															<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${company.totalHitPercentage}"/></td>
														</tr>
													</c:forEach>
												</c:if>
											</c:when>
											<c:otherwise>
												<td class="text-center" colspan="4"><a class="not-found">No usage Found</a></td>
											</c:otherwise>
										</c:choose>
									</tbody>
			
			
			
								</table>
							</div>
						</div>
					</div>
				</div>
              </div>
              <div class="tab-pane" id="tab_2">
                <div class="row row-without-margin" style="display: none;">
					<h1 class="h1-report" id="userCompany"></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-cogs"></i>&nbsp;Settings</li>
						<li><i class="fa fa-key active"></i>&nbsp;Change Password</li>
					</ol>
				</div>

			<div class="row row-without-margin">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box box-myshipment">
						<div class="box-header with-border" style="text-align: center;">
							<h1 class="box-title"
								style="font-size: 26px; font-family: 'Quicksand', sans-serif; padding-bottom: 0px !important;">Change
								Password</h1>
						</div>
						<div class="box-body no-padding" style="display: block;">
							<div class="row row-without-margin">
								<div class="col-md-12 col-sm-12 col-sx-12">
									<div class="pad">
										<form:form method="post" id="changePasswordForm"
											commandName="editparam">
											<fieldset>
												<legend
													style="color: #00a65a; margin-top: 2px; text-align: center; border-bottom: 0px solid;">
													<%-- ${message} --%> </legend>
												<div class='row'>
													<div class="col-md-2"></div>
													<div class="col-md-8">
														<div class='col-md-12'>
															<div class='form-group'>
																<label>Old Password</label>
																<form:input path="password" id="pass"
																	cssClass="form-control" required="required"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label>New Password</label>
																<form:input path="newPassword" id="pass1"
																	required="required" cssClass="form-control"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label>Verify Password</label>
																<form:input path="verifyPassword" id="pass2"
																	required="required" cssClass="form-control"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label style="display: block; color: #f5f5f5">.
																</label> <input type="submit" id="btn-appr-po-search"
																	class="btn btn-success form-control" value="SUBMIT"
																	onclick="return Validate()" />
															</div>
														</div>
													</div>
													<div class="col-md-2"></div>
												</div>
											</fieldset>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>

		</div>
              <div class="tab-pane" id="tab_3">
                <div class="row" style="display: none;">
					<h1 class="h1-report" id="userCompany"></h1>
					<ol class="breadcrumb">
						<li><i class="fa fa-cogs"></i>&nbsp;Settings</li>
						<li><i class="fa fa-briefcase active"></i>&nbsp;Preferred Sales Organization</li>
					</ol>
				</div>
				<div class="row">
					<div class="col-md-8">
						<div class="box box-myshipment">
							<div class="box-header">
								<h3 class="box-title">Default Company</h3>
							</div>
							<div class="box-body no-padding">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th class="text-center">Sales Organization</th>
											<th class="text-center">Distribution Channel</th>
											<th class="text-center">Division</th>
											<th class="text-center"></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">
												<div class="form-group">
													<select class="form-control" id="defaultCompanyList" onchange="defaultDistributionChannelOnChange(this.value);">
														
													</select>
												</div>
											</td>
											<td class="text-center">
												<div class="form-group">
													<select class="form-control" id="defaultDistributionChannelList" onchange="defaultDivisionOnChange(this.value);">
														
													</select>
												</div>
											</td>
											<td class="text-center">
												<div class="form-group">
													<select class="form-control" id="defaultDivisionList">
														
													</select>
												</div>
											</td>
											<td>
												<a class="btn btn-primary" id="saveDefaultCompany" style="width: 100%;">Save</a>
											</td>
										</tr>
									</tbody>
			
			
			
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="box box-myshipment">
							<div class="box-header">
								<h3 class="box-title">Default Company Status</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th class="text-center"></th>
											<th class="text-center">Status</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="text-center">
												Click Here to Activate Default Company
											</td>
											<td class="text-center">
												<div class="input-group-btn">
								                  <button type="button" id="default-company-active-state-text" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								                    <span class="fa fa-caret-down"></span></button>
								                  <ul class="dropdown-menu default-company-active-dropdown" style="width: 100%;">
								                    <li><a id="default-company-active">Active</a></li>
								                    <li><a id="default-company-inactive">Inactive</a></li>
								                  </ul>
								                </div>
											</td>
										</tr>
										
									</tbody>
			
			
			
								</table>
								<br>
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="display: none;">
					<div class="col-md-12">
						<div class="box box-myshipment">
							<div class="box-header">
								<h3 class="box-title">Alternative Company Selection (maximum usage in myshipment)</h3>
							</div>
							<div class="box-body no-padding">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th class="text-center">Sales Organization</th>
											<th class="text-center">Distribution Channel</th>
											<th class="text-center">Division</th>
											<th class="text-center">% of Usage</th>
											<th class="text-center"></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${fn:length(companyUsagePercentageList) gt 0}">
												<c:if test="${not empty companyUsagePercentageList}">
													<c:forEach items="${companyUsagePercentageList}" var="company" begin="0" end="0">
														<tr>
															<td class="text-center">${company.companyName}</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${company.distributionChannel == 'EX'}">Export</c:when>
																	<c:when test="${company.distributionChannel == 'IM'}">Import</c:when>
																	<c:when test="${company.distributionChannel == 'CH'}">CHA</c:when>
																</c:choose>
															</td>
															<td class="text-center">
																<c:choose>
																	<c:when test="${company.division == 'SE'}">SEA</c:when>
																	<c:when test="${company.division == 'AR'}">AIR</c:when>
																</c:choose>
															</td>
															<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${company.totalHitPercentage}"/></td>
															<td class="text-center">
																<div class="input-group-btn">
												                  <button type="button" id="default-company-active-state-text" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
												                    <span class="fa fa-caret-down"></span></button>
												                  <ul class="dropdown-menu default-company-active-dropdown" style="width: 100%;">
												                    <li><a id="default-company-active">Active</a></li>
												                    <li><a id="default-company-inactive">Inactive</a></li>
												                  </ul>
												                </div>
															</td>
														</tr>
													</c:forEach>
												</c:if>
											</c:when>
											<c:otherwise>
												<td class="text-center" colspan="4"><a class="not-found">No usage Found</a></td>
											</c:otherwise>
										</c:choose>
									</tbody>
			
			
			
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-4" style="display: none;">
						<div class="box box-myshipment">
							<div class="box-header">
								<h3 class="box-title">How it Works!</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body no-padding">
								
							</div>
						</div>
					</div>
				</div>
              </div>
            </div>
          </div>

<div class="container-fluid container-fluid-441222" style="background-color: #fffafa; display: none;">
	<div class="row row-without-margin">
		<h1 class="h1-report" id="userCompany"></h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-user "></i>&nbsp;My Account</li>
			<li class="active"></li>
		</ol>
	</div>
	<hr>
	
	<div class="row row-without-margin">
		<div class="col-md-6">
			<div class="box box-myshipment">
				<div class="box-header">
					<h3 class="box-title">Default Company</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-responsive">
						<thead>
							<tr>
								<th class="text-center">Sales Organization</th>
								<th class="text-center">Distribution Channel</th>
								<th class="text-center">Division</th>
								<th class="text-center"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-center">
									<div class="form-group">
										<select class="form-control" id="defaultCompanyList" onchange="defaultDistributionChannelOnChange(this.value);">
											
										</select>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group">
										<select class="form-control" id="defaultDistributionChannelList" onchange="defaultDivisionOnChange(this.value);">
											
										</select>
									</div>
								</td>
								<td class="text-center">
									<div class="form-group">
										<select class="form-control" id="defaultDivisionList">
											
										</select>
									</div>
								</td>
								<td>
									<a class="btn btn-primary" class="" id="saveDefaultCompany">Save</a>
								</td>
							</tr>
						</tbody>



					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="box box-myshipment">
				<div class="box-header">
					<h3 class="box-title">Most Used Company in myshipment</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-responsive">
						<thead>
							<tr>
								<th class="text-center">Sales Organization</th>
								<th class="text-center">Distribution Channel</th>
								<th class="text-center">Division</th>
								<th class="text-center">% of Usage</th>
							</tr>
						</thead>
						<tbody>
							<c:choose>
								<c:when test="${fn:length(companyUsagePercentageList) gt 0}">
									<c:if test="${not empty companyUsagePercentageList}">
										<c:forEach items="${companyUsagePercentageList}" var="company">
											<tr>
												<td class="text-center">${company.companyName}</td>
												<td class="text-center">
													<c:choose>
														<c:when test="${company.distributionChannel == 'EX'}">Export</c:when>
														<c:when test="${company.distributionChannel == 'IM'}">Import</c:when>
														<c:when test="${company.distributionChannel == 'CH'}">CHA</c:when>
													</c:choose>
												</td>
												<td class="text-center">
													<c:choose>
														<c:when test="${company.division == 'SE'}">SEA</c:when>
														<c:when test="${company.division == 'AR'}">AIR</c:when>
													</c:choose>
												</td>
												<td class="text-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${company.totalHitPercentage}"/></td>
											</tr>
										</c:forEach>
									</c:if>
								</c:when>
								<c:otherwise>
									<td class="text-center" colspan="4"><a class="not-found">No usage Found</a></td>
								</c:otherwise>
							</c:choose>
						</tbody>



					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>
<!-- Content Ends -->
<script>
	$("#userCompany").text(loginDetails.peAddress.name);
	var defaultCompanyJson = ${defaultCompanyJson};
	
	function openCity(evt, cityName) {
	    // Declare all variables
	    var i, tabcontent, tablinks;

	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }

	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(cityName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
</script>

<script src="${pageContext.request.contextPath}/resources/js/myaccount.js"></script>

<!-- footer -->
<%@include file="footer_v2.jsp" %>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/passwordChange.js"></script>
