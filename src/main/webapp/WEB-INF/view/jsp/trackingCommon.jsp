
<%@include file="headerCommon.jsp" %>       

        <!--END OF HEADER-->
        
        
        
        <section id="bill-of-landing">
          <section>
            <div class="container-fluid">
             <div class="row">
                <div class="input-group-addon commercial">
                    <h4 style="text-align:left;">Tracking</h4>
				</div>
             </div>
            </div>
        </section>
        
        <c:if test="${not empty documentHeaderBeanLst}">
        <div class="col-xs-12" style="height:20px;"></div>
            
<div class="container" style="width:80%; margin: 0 20% 0 12%;">       
<!-- Nav tabs -->
<ul class="tracking-items">
    <li id="booking-conf" class="stateC">
        <a class="tracking-item active">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Booking Confirm</a>
    </li>
    <li id="goods-rec" class="stateA">
        <a class="tracking-item">Goods Receive</a>
    </li>
    <li id="stuffing-done" class="stateB">
        <a class="tracking-item">Stuffing Done</a>
    </li>
    <!-- <li id="shipped-on" class="stateB">
        <a class="tracking-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shipped Onboard</a>
    </li> -->
    <li id="delivered" class="stateB">
        <a class="tracking-item">Delivered</a>
    </li>
   <!--  <li id="invoice-do" class="stateB">
        <a class="tracking-item">Invoice Done</a>
    </li> -->
</ul>



      <div style="clear:both;"></div>

     
</div> 
</section>

        <section>
            <div class="container" style="width:75%; margin: 0 auto;">
              <div class="col-xs-12" style="height:40px;"></div>
              <div class="row">          
            <div class="col-md-12">
            <table class="table commercial-table package-detail" style="margin-top:-10px;">
             <thead>
               <tr>
                   <td colspan="3" style="font-size:14px;">Package Details</td>
               </tr>
            </thead>
               <!-- Package Details will have only one row. so documentHeaderBeanLst size should be one only.-->
               <tbody>
                   <c:forEach items="${documentHeaderBeanLst}" var="docHeader"  >
                   <tr>
                       <td>BL No :<p style="font-weight:normal;">${docHeader.bl_no}</p></td>
                       <td>Document No :<p style="font-weight:normal;">${docHeader.document_no}</p></td>
                       <td>Comm. Invoice No :<p style="font-weight:normal;">${docHeader.comm_invoice_no}</p></td>
                       
                   </tr>
                   <tr>
                     <td>Shipper Name :<p style="font-weight:normal;">${docHeader.shipper_name}</p></td>
                     <td>Buyer Name :<p style="font-weight:normal;">${docHeader.buyer_name}</p></td>
                     <td>Carrier Name :<p style="font-weight:normal;">${docHeader.carrier_name}</p></td>
                     
                   </tr>
                   <tr>
                     <td>POL Name :<p style="font-weight:normal;">${docHeader.pol_name}</p></td>
                     <td>POD Name :<p style="font-weight:normal;">${docHeader.pod_name}</p></td>
                     <td>Place Of Discharge :<p style="font-weight:normal;">${docHeader.place_of_discharge}</p></td>
                     
                     
                   </tr>
                   
                    <tr>
                     <td>Comm. Invoice Date: <p style="font-weight:normal;">${docHeader.comm_invoice_date}</p></td>
                     <td>LC No: <p style="font-weight:normal;">${docHeader.lc_tt_po_no}</p></td>
                     <td>LC Date :<p style="font-weight:normal;">${docHeader.lc_tt_po_date}</p></td>
                     
                     
                   </tr>
                   
                   <tr>
                     <td>Booking Date:<p style="font-weight:normal;">${docHeader.booking_date}</p></td>
                     <td>GR Date :<p style="font-weight:normal;">${docHeader.gr_date}</p></td>
                     <td>Stuffing Date:<p style="font-weight:normal;">${docHeader.shipment_date}</p></td>
                     
                     
                   </tr>
                   <tr>
                     <td>Total Volume :<p style="font-weight:normal;">${docHeader.tot_volume}</p></td>
                     <td>Charge Weight :<p style="font-weight:normal;">${docHeader.charge_wt}</p></td>
                     <td>Gross Weight :<p style="font-weight:normal;">${docHeader.gross_wt}</p></td>
                   </tr>
                   
                  <!--   <tr>
                     <td>Discharge Port Name :<p style="font-weight:normal;">${docHeader.discharge_port_name}</p></td>
                     <td>Total Pieces :<p style="font-weight:normal;">${docHeader.tot_pcs}</p></td>
                     <td>Net Weight :<p style="font-weight:normal;">${docHeader.net_wt}</p></td>
                   </tr>-->
                   </c:forEach>
               </tbody>
            </table>
            </div>
            </div>
        </div>
        
        <!--START OF TABLE-->
         <div class="container" style="width:70%; margin: 0 auto;">
         <div class="row">
         <div class="col-md-12">
         <div class="table-responsive">
          <table class="table table-bordered">
          <thead>
          <tr>
          <th colspan="8" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center;">Line Items</th> 
          </tr>
          </thead>
          <tbody>
             <tr>
                 <td style="font-weight: bold;">PO Number</td>
                 <td style="font-weight: bold;">Material Desc</td>
                 <td style="font-weight: bold;">Style No</td>
                 <td style="font-weight: bold;">Total No Of Pcs </td>
                 <td style="font-weight: bold;">Net Weight</td>
                 <td style="font-weight: bold;">Gross Weight </td>
                 <td style="font-weight: bold;">Color </td>
                 <td style="font-weight: bold;">Size </td>
             </tr>
      		<c:forEach items="${documentDetailBeanLst}" var="docDetail"  >
             <tr>
                 <td>${docDetail.po_no}</td>
                 <td>${docDetail.material_desc}</td>
                 <td>${docDetail.style_no}</td>
                 <td>${docDetail.tot_no_of_pcs}</td>
                 <td>${docDetail.net_weight}</td>
                 <td>${docDetail.gr_weight}</td>
                 <td>${docDetail.color}</td>
                 <td>${docDetail.size1}</td>
             </tr>
      		</c:forEach>
      
         </tbody>
             
         </table>   
         </div>
         </div>
         </div>
         </div>
         <!--END OF TABLE-->
        </section>
</c:if>
<c:if test="${empty documentHeaderBeanLst}">
 <div style="clear:both;">

<p align="center" style="color:red;font-size:18px">No Record Found</p>
</div>
</c:if>
<script type="text/javascript">
	

	
	$(document).ready(function() {
		//var trackingObject = "${trackingDto.zzmblmawbno}";
		var bookingConfirmed = "${trackingStatusParams.bookingDate}";
		var goodsReceived = "${trackingStatusParams.goodsReceivedDate}";
		var stuffingDone = "${trackingStatusParams.stuffingDate}";
		var shippedOnboard = "${trackingStatusParams.shippedOnboardDate}";
		var delivered = "${trackingStatusParams.deliveredDate}";

		
		/*if (bookingConfirmed != null) {			
			$('#booking-conf').removeClass('stateC').addClass('stateC');
		}*/
		if (bookingConfirmed == null || bookingConfirmed == '') {
			$('#booking-conf').removeClass('stateC').addClass('stateA');
		}
		if (goodsReceived != null && goodsReceived != '') {
			$('#goods-rec').removeClass('stateA').addClass('stateC');
		}
		if (goodsReceived == null || goodsReceived == '') {
			if (bookingConfirmed != null && bookingConfirmed != '') {
				//$('#goods-rec').removeClass('stateA').addClass('stateC');
			}
			if (bookingConfirmed == null || bookingConfirmed == '') {
				$('#goods-rec').removeClass('stateA').addClass('stateB');
			}

		}
		//Stuffing done
		if (stuffingDone != null && stuffingDone != '') {
			
			$('#stuffing-done').removeClass('stateB').addClass('stateC');
		}
		if (stuffingDone == null || stuffingDone == '') {
			
			if (goodsReceived != null && goodsReceived != '') {
				$('#stuffing-done').removeClass('stateB').addClass('stateA');
			}
			if (goodsReceived == null || goodsReceived == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Shipped Onboard
		if (shippedOnboard != null && shippedOnboard != '') {
			//alert("in sh")
			$('#shipped-on').removeClass('stateB').addClass('stateC');
		}
		if (shippedOnboard == null || shippedOnboard == '') {
			if (stuffingDone != null && stuffingDone != '') {
				$('#shipped-on').removeClass('stateB').addClass('stateA');
			}
			if (stuffingDone == null || stuffingDone == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Delivered
		if (delivered != null && delivered != '') {
			$('#delivered').removeClass('stateB').addClass('stateC');
		}
		if (delivered == null || delivered == '') {
			if (shippedOnboard != null && shippedOnboard != '') {
				$('#delivered').removeClass('stateB').addClass('stateA');
			}
			if (shippedOnboard == null || shippedOnboard == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

	});
</script> 
 <%@include file="footer.jsp" %>