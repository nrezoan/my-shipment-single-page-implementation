		<%@include file="header_v2.jsp" %>
		<%@include file="navbar.jsp" %>
		<section >
		<div class="well assign-po-search">
	
      <fieldset>
          <legend>Approve PO: PO Search</legend>
                <div class='row' >
                    
                   <div class='col-sm-2'>    
                        <div class='form-group'>
                        <label>PO Number</label>
                        <input class="form-control" id="po_number" name="user[title]"  type="text" />
                        </div>
                   </div>
                   <div class='col-sm-2'>
                       <div class='form-group'>
                       <label>From Date</label>
                       <input id="from_date" type="text" class="date-picker form-control glyphicon glyphicon-calendar" />
                   </div>
                  </div>
                  <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>TO Date</label>
                   <input id="to_date" type="text" class="date-picker form-control glyphicon glyphicon-calendar" />
                 </div>
                 </div>
                    <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>Supplier Code</label>
                   <input class="form-control" id="supplier_code" name="user[lastname]" required="true" size="30" type="text" />
                 </div>
                 </div>
                   <!--  <div class='col-sm-2'>
                  <div class='form-group'>
                   <label>Sku No</label>
                   <input class="form-control" id="user_lastname" name="user[lastname]" required="true" size="30" type="text" />
                 </div>
                 </div> -->
				  <div class='col-sm-2' style="margin-top: 2%;">
                  <div class='form-group'>
                   
                   <button type="button" id="btn-aprv-search"class="btn btn-success form-control">Search</button>
                 </div>
                 </div>
                    
                 </div>
             
</fieldset>
		</div>
		<div class="container-fluid table-responsive">
			<div class="row table_header">
				<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
					<span class="glyphicon glyphicon-triangle-bottom"></span><h3>Search Result From <label id="from_date_lbl"></label>  to  <label id="to_date_lbl"></label> </h3><span id="display-message" class="" ></span>
				</div>
				
			</div>
			<div class="row" style="margin-left:5px;">
		<table class="table table-striped" id="tbl-aprv-po">
		  <thead style="">
      <tr>
	    <th>Select</th>
        <th>PO No</th>
    <!--      <th>Client Name</th>-->
       <th>Sku No</th>
		<th>Color</th>
		<th>Size</th>
		<th>Total Pcs</th>
		<th>Select Carrier</th>
		<th>Carrier Schedule</th>
		<th>Comments</th>
	
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
  <div style="text-align:center">
  <button type="button" style="width: 19%;margin-bottom:20px;" class="btn btn-primary" id="btn-approve">Approve</button>
  </div>
  </div>

		</div>	
		
		</section>
	<!-- ----------------------------- -->		
		<%@include file="footer_v2_fixed.jsp" %>