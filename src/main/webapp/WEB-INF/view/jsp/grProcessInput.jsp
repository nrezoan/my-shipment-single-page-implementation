<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html, body {
	background-color: #fff;
}
tbody td {
	font-size: 14px;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/grprocess.js"></script>
<script type="text/javascript">
</script>

<section class="main-body">
	<div class="well well-white-269490">
		<form:form action="testDisplayGoodsReceive" method="post" commandName="grWSParams">
			<fieldset>
				<legend>GR With Reference To Sales Order</legend>
				<div class='row'>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>soNumber</label>
							<form:input path="soNumber" id="soNumber" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>grDocDate</label>
							<form:input path="grDocDate" id="grDocDate"
								class="date-picker form-control glyphicon glyphicon-calendar"
								required="true"/>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>						
							<label>grPostingDate</label>
							<form:input path="grPostingDate" id="grPostingDate"
								class="date-picker form-control glyphicon glyphicon-calendar"
								required="true" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>storLocation</label>
							<form:input path="storLocation" id="storLocation" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>storUnitType</label>
							<form:input path="storUnitType" id="storUnitType" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>destStorType</label>
							<form:input path="destStorType" id="destStorType" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>storSect</label>
							<form:input path="storSect" id="storSect" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>bin</label>
							<form:input path="bin" id="bin" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: transparent">. </label>
							<input type="submit" id="btn-goods-receive"
								class="btn btn-success form-control" value="Submit" />
						</div>
					</div>
				</div>

			</fieldset>
		</form:form>
	</div>
</section>

<script>
	$(document).ready(function() {
		$('.date-picker').each(function() {
		    $(this).removeClass('hasDatepicker').datepicker({
		        dateFormat: "dd.mm.yy"
		    }).datepicker("setDate", new Date());
		});
		$("#btn-goods-receive").click(function() {
			validateDateFieldsGR();			
		})
	});	
</script>

<%@include file="footer_v2_fixed.jsp"%>