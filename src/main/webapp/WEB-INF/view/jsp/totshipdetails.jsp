<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>DataBAse</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="style.css">
		 <link rel="stylesheet" href="css/graph.css">
		<!-- modernizr JS
		============================================ -->		
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body onload="zoom();">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

		<!-- Prelaoding Screen -->
		<header class="header-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="logo">
							<a href=""><img src="img/logo.png" alt="Logo"></a>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 col-lg-3 search-form">
						<div class="first">
							<form action="#">
								<div class="form-group">
									<div class="input-group">
										<input type="text" class="form-control" id="exampleInputAmount" placeholder="Track Your Shipment">
										<div class="input-group-addon">
											<button type="submit" class="btn btn-primary">Go</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 second">
						<div class="radio_text">
							<p style="color:#fff;">Export</p>
							<p style="color:#fff;">Import</p>
							<p style="color:#fff;">Cha</p>
						</div>
					
						<div class="btn_radio">
							<input type="radio" name="radio"/><br>
							<input type="radio" name="radio"/><br>
							<input type="radio" name="radio"/>
						</div>
						<div class="border-img">
							<img src="img/s-border.png" alt="">
						</div>
                        <!--<div class="radio_text">
                            <label>Export</label><input type="radio" name="radio">
                            <label>Import</label><input type="radio" name="radio">
                            <label>CHA</label><input type="radio" name="radio">
                        </div>-->
						<div class="search-form">
							<form action="#"   id="form_search">
								<div class="form-group">
									<div class="input-group">
										<select class="form-control">
											<option selected>ABC Company ltd</option>
											<option>Companny 7</option>
											<option>Companny 2</option>
											<option>Companny 5</option>
											<option>Companny 2</option>
											<option>Companny 8</option>
										</select>
										<div class="input-group-addon">
											<button type="submit" class="btn btn-primary">Change</button>
										</div>
									</div>
								</div>
							</form>
						</div>
						
					</div>
					
				</div>
			</div>
		</header>
		<div class="welcome-area">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-6">
						<div class="left-1">
							<form action="#">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">
											<button type="submit" class="btn btn-primary">
												<i class="fa fa-send"></i>
											</button>
										</div>
										<select class="form-control">
											<option selected>Sea</option>
											<option>Companny 7</option>
											<option>Companny 2</option>
											<option>Companny 5</option>
											<option>Companny 2</option>
											<option>Companny 8</option>
										</select>
									</div>
								</div>
							</form>
						</div>
						<div class="left-2">
							<form action="#">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon">
											<button type="submit" class="btn btn-primary">
												<i class="fa fa-send"></i>
											</button>
										</div>
										<select class="form-control">
											<option selected>Switch to Air</option>
											<option>Companny 7</option>
											<option>Companny 2</option>
											<option>Companny 5</option>
											<option>Companny 2</option>
											<option>Companny 8</option>
										</select>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div class="user-text">
							<p>welcome <b>user name</b> <a href=""><img src="img/question.png" alt=""></a> <a href=""><img src="img/settings.png" alt=""></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section class="booking-table-heading-area">
			<div class="container-fluid">
				<div class="row" style="padding: 0px;margin-left: 0%;">
					<div class="col-md-12">
						<ul class="table-h-nav">
							<li><a href="">Booking</a></li>
							<li><a href="" id="nav_slash_one">|</a></li>
							<li><a href="">booking template</a></li>
							<li><a href="" id="nav_slash_two">|</a></li>
							<li><a href="">frequently used form</a></li>
							<li><a href="" id="nav_slash_three">|</a></li>
							<li><a href="">other reports</a></li>
							<li><a href="" id="nav_slash_four">|</a></li>
							<li><a href="">Mis</a></li>
							<li><a href="" id="nav_slash_five">|</a></li>
							<li><a href="">Update</a></li>
							<li><a href="" id="nav_slash_six">|</a></li>
							<li role="presentation" class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
      Po Management <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
      <li><a href="#">Action</a></li> <li><a href="#">Another action</a></li> <li><a href="#">Something else here</a></li> <li role="separator" class="divider"></li> <li><a href="#">Separated link</a></li> 
    </ul>
  </li>							 
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="main-body">
		
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    	<h3 class="ship_desti">Buyer wise Shipment</h3>  
    </div>
  </div><!--/row-->
  <hr>
  <div class="row">
    
   
  
	 <div class="col-md-4">
        
	
<div id="chartContainer" style="width: 100%; height: 300px"></div>
    </div>
	  <div class="col-md-8">
      <table class="table table-striped">
		  <thead style="">
      <tr>
        <th>Buyer Name</th>
        <th>Total Shipment</th>
        <th>% of Shipment</th>
		
		
      </tr>
    </thead>
    <tbody>
      <tr class="">
        <td>Buyer 1</td>
        <td>30</td>
        <td>18%</td>
		
		
      </tr>
      <tr class="">
        <td>Buyer 2</td>
        <td>40</td>
        <td>24%</td>
		
		
      </tr>
      <tr class="">
        <td>Buyer 3</td>
        <td>20</td>
        <td>12%</td>
	 </tr>
	    <tr class="">
        <td>Buyer 4</td>
        <td>80</td>
        <td>46%</td>
		
		
      </tr>
	</tbody>
  </table>
    </div>
  </div>
</div>
		</section>
		
		<section class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<p class="footer-para">Copyright &copy MyShipment.com</p>
						
					</div>
					<div class="col-lg-3 col-md-3 footer-img">
						<img src="img/logo.png" alt="" class=""/>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 footer_nav">
						<ul class="table-h-nav-footer">
							<li><a href="">Booking</a></li>
							<li><a href="">booking template</a></li>
							<li><a href="">Mis</a></li>
							<li><a href="">frequently used form</a></li>
							<li><a href="">other reports</a></li>
							
							<li><a href="">Update</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		
	
			
		
		

		<!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
		<!-- main JS
		============================================ -->
		
<script src="js/canvasjsmin.js"></script>		
        <script src="js/main.js"></script>
		<script src="js/buyer-wise-shipment.js"></script>
		<script>
	 $(document).ready( 
                function () {
                   

                    $("#chartContainer").click( 
                        function(evt){
                            
                          window.location.assign("shippingdetails.html");
                        }
                    );                  
                }
            );	
			</script>
    </body>
</html>
