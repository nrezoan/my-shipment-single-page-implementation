<div id="hblForm" class="tab-pane fade">
    <!-- <div id="lineItem" class="tab-pane fade in active"> -->
    <div class="row row-no-margin-331932" style="padding-top: 10px;">
        <div class="col-xs-6 col-md-5" style="padding-left: 20px;">
            <div class="form-inline">
                <span class="" id="span-message"></span>
                <c:if test="${updateStatus==true }">
                    <span class="message-success" id="span-message">${updateMessage }</span>
                </c:if>
                <c:if test="${updateStatus==false }">
                    <span class="message-error" id="span-message">${updateMessage }</span>
                </c:if>
            </div>
        </div>
    </div>
    <div class="row row-no-margin-331932" style="padding-top: 10px;">

        <div class="col-xs-6 col-md-5" style="padding-left: 20px;">

        </div>
        <div class="col-xs-6 col-md-3">

        </div>
        <div class="col-xs-6 col-md-4">
        </div>
    </div>
    <!-- table part -->
    <div class="row row-no-margin-331932" style="padding-top: 10px;">
        <div class="col-xs-12 col-md-12">
            <div class="table-responsive" id="headingTwo">
                <table class="table table-hover" id="valueTable">
                    <thead>
                        <tr>
                            <th style="text-align: center;">HBL/HAWB</th>
                            <th style="text-align: center;">Incoterms</th>
                            <th style="text-align: center;">Origin</th>
                            <th style="text-align: center;">Destination</th>
                            <th style="text-align: center;">Receiver</th>
                            <th style="text-align: center;">Gross Weight</th>
                            <th style="text-align: center;">Volume</th>
                            <th style="text-align: center;">Chargeable Weight</th>
                        </tr>
                    </thead>
                    <tbody id="valueTableBody" style="text-align: center;">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>