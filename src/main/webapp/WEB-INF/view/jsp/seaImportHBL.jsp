<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sea Import HBL Tracking Details</title>
    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/header-footer.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/credentials.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">
    
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
	<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate.css"> --%>
		
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/sea-export.css" />
		
	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700' rel='stylesheet' type='text/css'>
	<!-- <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"	rel="stylesheet" type="text/css" /> -->
	<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/footer-before-login.css"
	rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/timeline.css" />	
	<script	src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.2/modernizr.js" type="text/javascript"></script>
	<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script>
	$(document)
			.ready(
					function() {
 						var status = "${seaImportHBLData.myshipmentTrackHeader[0].status}"; 
						/* var status = "Shipment Released"; */
						if (status === "Order Booked") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
						} else if (status === "Shipment In Transit") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
							document.getElementById("shipment-in-transit").className = "li-shipment-in-transit-263318 complete";
						} else if (status === "Shipment Arrived") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
							document.getElementById("shipment-in-transit").className = "li-shipment-in-transit-263318 complete";
							document.getElementById("shipment-arrived").className = "li-shipment-arrived-588411 complete";
						} else if (status === "Shipment Released") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
							document.getElementById("shipment-in-transit").className = "li-shipment-in-transit-263318 complete";
							document.getElementById("shipment-arrived").className = "li-shipment-arrived-588411 complete";
							document.getElementById("shipment-released").className = "li-shipment-released-553226 complete";
						}
						$('.collapse').on('shown.bs.collapse', function(){
							$(this).parent().find(".glyphicon-plus-sign").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
							})
						.on('hidden.bs.collapse', function(){
							$(this).parent().find(".glyphicon-minus-sign").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
						});
					});

	</script>
	
	<!-- hamid styles -->
	<style>
		.table > thead > tr > td, 
		.table > tbody > tr > td,
		.table > thead > tr > th,
		.table > tbody > tr > th {
	    	border: none;
	}
	.panel {
    	border-color: #FFF;
    }
	</style>    
</head>

<body>
    <div>
		<nav class="navbar credentials-nav-356369">
			<div class="row row-margin-unset-all">
				<div class="col-md-12">					
					<div class="col-md-3">
						
								<div class=""></div>
								<div class="">
									<a class="navbar-brand-295154" href="/">
										<img class="nav-img-447749" src="${pageContext.request.contextPath}/resources/img/logo-xs.png" /></a>
								</div>
								<div class=""></div>
						
					</div>
					<div class="col-md-6"></div>
					<div class="col-md-3">
						<div id="navigation">
							<ul class="nav navbar-nav" id="top-nav">
								<li class="nav-item"><a class="nav-link" href="/"> <%-- <img src="${pageContext.request.contextPath}/resources/images/home-1.png" title="Back to Home"> --%>
										HOME
								</a></li>
								<li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/signin"> <%-- <img src="${pageContext.request.contextPath}/resources/images/home-1.png" title="Back to Home"> --%>
										LOGIN
								</a></li>
							</ul>
						</div>
					</div>
					<div class=""></div>

					<%-- 					<a class="navbar-brand" href="#"><img
						src="${pageContext.request.contextPath}/resources/images/logo-xs.png" /></a> --%>
					
				</div>
			</div>
		</nav>
		<!--HEADER-->


        <!-- CREDENTIALS -->
<!--         <section id="credential" style="height:auto;">
            <div class="container text-xs-center">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>credentials</h2>
                        <div class="content-title-underline-body"></div>
                        <h5>Forgot Username/Password? <br> <br>Fill-up the details below to receive the credentials on your registered email-id</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <form>
                                <fieldset class="form-group">
                                    <input type="text" class="form-control form-control-sm myName" id="name" placeholder="Customer ID...">
                                </fieldset>
                                <fieldset class="form-group">
                                    <input type="email" class="form-control form-control-sm myEmail" id="email" placeholder="E-mail...">
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </form>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- END CONTACT -->
        
		<section>
			<div class="container" id="track-ship-header">
				<div class="page-header"
					style="margin: 10px 0 10px; padding-bottom: 0px;">
					<!-- <h1>Sea Export HBL Tracking Details</h1> -->
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Sea Import HBL Tracking</h2>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div id="company-name">
									<h4>${seaImportHBLData.myshipmentTrackHeader[0].sales_org_desc}</h4>
								</div>
							</div>	
							<div class="col-md-2 col-sm-6 col-xs-6">
								<!-- <h4> -->
									<img id="img-comp-logo"
										src="${pageContext.request.contextPath}/resources/images/${seaImportHBLData.myshipmentTrackHeader[0].comp_code}.jpg"
										alt="Company Logo" />
								<!-- </h4> -->
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="milestone-track-section-254196">
			<div class="container" id="track-ship-summary">			
				<div class="panel-group">
					<div class="panel panel-default"
						style="border-bottom: 3px solid #007AA7; border-bottom: 2px solid #007AA7; margin-bottom: 0px; border-radius: 0px;">
						<div class="panel-heading-217226" >
							<div class="row">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="col-md-3 col-xs-12 col-sm-12">
										<h5>HBL NO. :
											${seaImportHBLData.myshipmentTrackHeader[0].bl_no }</h5>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-12">
										<h5>MBL NO. :
											${seaImportHBLData.myshipmentTrackHeader[0].mbl_no }</h5>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-12">
										<h5>STATUS :
											${seaImportHBLData.myshipmentTrackHeader[0].status}</h5>
									</div>
									<div class="col-md-3 col-xs-12 col-sm-12">
										<h5>
											ATA :
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt}"
												var="ata_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${ata_date}" />
										</h5>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="panel-heading" style="background-color: #00ADF1; color: #FFF;margin-bottom: 0px;border-radius: 0px;">						
					</div> -->
						<div class="panel-body panel-body-422432">
							<ul class="timeline-591271" id="timeline-591271">

								<li id="order-booked" class="li-order-booked-003439">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaImportHBLData.myshipmentTrackHeader[0].bl_bt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaImportHBLData.myshipmentTrackHeader[0].bl_bt}"
														var="bl_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${bl_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Order Booked</h4>
									</div>
								</li>
								<li id="shipment-in-transit" class="li-shipment-in-transit-263318">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaImportHBLData.myshipmentTrackHeader[0].dep_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaImportHBLData.myshipmentTrackHeader[0].dep_dt}"
														var="dep_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${dep_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Shipment On Board</h4>
									</div>
								</li>
								<li id="shipment-arrived" class="li-shipment-arrived-588411 ">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaImportHBLData.myshipmentTrackHeader[0].ata_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt}"
														var="ata_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${ata_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Shipment Arrived</h4>
									</div>
								</li>
								<li id="shipment-released" class="li-shipment-released-553226 ">
									<div class="timestamp-194858">
										<span> <c:choose>
												<c:when
													test="${empty seaImportHBLData.myshipmentTrackHeader[0].noc_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaImportHBLData.myshipmentTrackHeader[0].noc_dt}"
														var="noc_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${noc_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Shipment Released</h4>
									</div>
								</li>

							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- hamid collapse toggle -->
		<div class="container">
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a class="" data-toggle="collapse"
							data-parent="#accordion" href="#collapseOne" >
							
								<span>CONSIGNMENT DETAILS</span> <!-- <span
									class="glyphicon glyphicon-minus-sign" style="float: right;"></span> -->
							
						</a></h4>
					</div>
 					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover table-inverse">

									<tbody>
										<tr>
											<th scope="row">HBL No.</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].bl_no }</td>
											<th scope="row">MBL No.</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].mbl_no }</td>
										</tr>

										<tr>
											<th scope="row">Shipper Name</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].shipper }</td>
											<th scope="row">Consignee Name</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].buyer }</td>
										</tr>
										<tr>
											<th scope="row">Origin Agent</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].agent }</td>
											<th scope="row">Place of Receipt</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].por }</td>
										</tr>
										<tr>
											<th scope="row">Port Of Loading</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].pol }</td>
											<th scope="row">Port of Discharge</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].pod }</td>
										</tr>
										<tr>
											<th scope="row">Shipment Mode</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].ship_mode }</td>
											<th scope="row">TOS</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].frt_mode_ds }</td>
										</tr>
										<tr>
											<th scope="row">Shipping Line</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].shipper }</td>
											<th scope="row">Co-Loader</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].coloader }</td>
										</tr>
										<tr>
											<th scope="row">L/C No.</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].lc_no }</td>
											<th scope="row">L/C Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].lc_dt_openTracking}" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].lc_dt}"
												var="lc_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${lc_date}" /></td>

											<!-- <td>${seaImportHBLData.myshipmentTrackHeader[0].lc_dt }</td> -->
										</tr>
										<tr>
											<th scope="row">ETA</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].eta_dt_openTracking }" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${eta_date}" /></td>

											<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].eta_dt }</td>-->
											<th scope="row">Shipped On Board</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].dep_dt}"
												var="dep_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${dep_date}" /></td>
										</tr>
										<tr>
											<th scope="row">ATA</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt_openTracking }" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt}"
												var="ata_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${ata_date}" /></td>

											<!-- <td>${seaImportHBLData.myshipmentTrackHeader[0].ata_dt }</td> -->
											<th scope="row">Import Rotation No</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].rot_no }</td>
										</tr>
										<tr>
											<th scope="row">NOC / DO Issue Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].noc_dt_openTracking }" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].noc_dt}"
												var="noc_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${noc_date}" /></td>

											<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].noc_dt }</td>-->
											<th scope="row">De-Stuffing Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt_openTracking }" /></td> --%>
											<fmt:parseDate
												value="${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt}"
												var="unstf_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${unstf_date}" /></td>

											<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt }</td>-->
										</tr>
										<tr>
											<th scope="row">Total Quantity</th>
											<%-- <td>${seaImportHBLData.myshipmentTrackHeader[0].tot_qty }</td> --%>
											<fmt:parseNumber var="totqty" integerOnly="true"
												type="number"
												value="${seaImportHBLData.myshipmentTrackHeader[0].tot_qty }" />
											<td>${totqty}</td>
											<th scope="row">Total Volume (CBM)</th>
											<td>${seaImportHBLData.myshipmentTrackHeader[0].volume }</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title"><a class="collapsed" data-toggle="collapse"
							data-parent="#accordion" href="#collapseTwo" >
							
								<span>ITEM &amp; CONTAINER DETAILS</span> <!-- <span
									class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->
							
						</a></h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive ">
								<table class="table ">
									<thead>
										<tr>
											<th>Sl</th>
											<th>Container No</th>
											<th>Size</th>
											<th>Seal No</th>
											<th>Mode</th>
											<th>Commodity</th>
											<th>Quantity</th>
											<th>Vol (CBM)</th>
											<th>Weight</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${seaImportHBLData.myshipmentTrackItem}"
											var="item" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${item.cont_no}</td>
												<td>${item.cont_size}</td>
												<td>${item.seal_no}</td>
												<td>${item.cont_mode}</td>
												<td>${item.commodity}</td>
												<%-- <td>${item.item_qty}</td> --%>
												<fmt:parseNumber var="itmqty" integerOnly="true"
													type="number" value="${item.item_qty}" />
												<td>${itmqty}</td>
												<td>${item.item_vol}</td>
												<td>${item.item_grwt}</td>
											</tr>
										</c:forEach>


									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel ">
					<div class="panel-heading">
						<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse"
							data-parent="#accordion" href="#collapseThree" >
							
								<span>VESSEL SCHEDULE</span> <!-- <span
									class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->
							
						</a></h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table ">

									<thead>
										<tr>
											<th>Leg</th>
											<th>Vessel Name</th>
											<th>Vessel Type</th>
											<th>Voyage</th>
											<th>POL</th>
											<th>POD</th>
											<th>ETD</th>
											<th>ETA</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${seaImportHBLData.myshipmentTrackSchedule}"
											var="vSchedule" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${vSchedule.car_name}</td>
												<td>${vSchedule.car_type}</td>
												<td>${vSchedule.car_no}</td>
												<td>${vSchedule.pol}</td>
												<td>${vSchedule.pod}</td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.etd_openTracking}" /></td> --%>
												<fmt:parseDate value="${vSchedule.etd}" var="etd_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${etd_date}" /></td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.eta_openTracking}" /></td> --%>
												<fmt:parseDate value="${vSchedule.eta}" var="eta_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${eta_date}" /></td>
												<!-- <td>${vSchedule.etd}</td> -->
												<!-- <td>${vSchedule.eta}</td> -->
											</tr>
										</c:forEach>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
<!-- 				<div class="panel ">
					<div class="panel-heading">
						<a class="accordion-toggle" data-toggle="collapse"
							data-parent="#accordion" href="#collapseFour"
							style="color: #2F4F4F; text-decoration: none;">
							<h4 class="panel-title">
								<span>CONTAINER DETAILS</span> <span
									class="glyphicon glyphicon-plus-sign" style="float: right;"></span>
							</h4>
						</a>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							
						</div>
					</div>
				</div> -->
			</div>
		</div>

		<!-- hamid edit ends -->

		<!-- FOOTER -->
		<%-- <section id="footer-673168" class=""	>
			<div class="container wow fadeInUp" data-wow-delay="0.4s"
				data-wow-duration="1.5s">
				<div class="row footer-top">
					<div class="col-md-4 footer-follow">
						<h5>Follow Us :</h5>
						<ul class="list-inline">
							<li class="list-inline-item"><a style="color: #FFF"
								href="https://www.facebook.com/mghgroupglobal" target="_blank"><i
									class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="list-inline-item"><a style="color: #FFF"
								href="https://www.linkedin.com/company/mghgroup" target="_blank"><i
									class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-copy">
						<p class="text-xs-center">
							&copy; myshipment.com
							<script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
						</p>
					</div>
					<div class="col-md-4 text-xs-center footer-app">
						<a
							href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/images/Android-App-Store-AF.png"
							class="img-fluid" title="Get On App Store"></a> <a
							href="https://play.google.com/store/apps/details?id=com.mgh.shipment"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/images/Android-App-Store-GF.png"
							class="img-fluid" title="Get On Google Play"></a>
					</div>
					<div id="footer">
						<div class="copy">
							<p>
								&copy;
								<script type="text/javascript">
									var today = new Date()
									var year = today.getFullYear()
									document.write(year)
								</script>
								my<span class="shipment-footer-panel">shipment</span> | All
								Rights Reserved
							</p>
						</div>
					</div>
				</div>
			</div>
		</section> --%>
				<div id="footer">
			<div class="copy">
				<p>
					&copy; <script type="text/javascript">
	                                var today = new Date()
	                                var year = today.getFullYear()
	                                document.write(year)
	
	                            </script> my<span class="shipment-footer-panel">shipment</span> | All
					Rights Reserved
				</p>
			</div>
		</div>

         <!-- FOOTER -->
        <section id="footer" style="background-color: #252525; display: none;<%-- background: url('${pageContext.request.contextPath}/resources/img/topheaderBG.jpg') --%> center center no-repeat;">
            <div class="container wow animated fadeInUp" data-wow-delay="0.4s" data-wow-duration="1.5s">
                <div class="row footer-top">
                    <div class="col-sm-4 footer-follow">
                        <h5>Follow Us :</h5>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="https://www.facebook.com/mghgroupglobal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.linkedin.com/company/mghgroup" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 footer-copy">
                        <p class="">
                            &copy; myshipment.com
                            <script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
                        </p>
                    </div>
                    <div class="col-sm-5 text-xs-center footer-app">
                        <a href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-AF.png" class="img-fluid" title="Get On App Store"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.mgh.shipment" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-GF.png" class="img-fluid" title="Get On Google Play"></a>
                    </div>
                </div>
            </div>
        </section>

		<!-- END FOOTER -->

    </div>
    
</body>

<%-- 
<!-- OLD DESIGN -->
<body>
	<header class="header-area">
		<div class="header">
			<div class="header-1">
				<a href="${pageContext.request.contextPath}/MyShipmentFrontApp/"><img
					src="${pageContext.request.contextPath}/MyShipmentFrontApp/resources/images/logo.png"
					alt="Logo" class="img-responsive"></a>
			</div>
			<div class="header-2">
				<a href="${pageContext.request.contextPath}/signin" target="_blank"><button
						type="button" class="btn btn-primary">Login</button></a>
			</div>
			<div class="clearfix"></div>
		</div>
	</header>
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12" style="margin: 0; padding: 0;">
					<p class="sea-export-1">Sea Import HBL Tracking Details</p>
				</div>
			</div>
		</div>
	</section>


	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12" style="margin: 0; padding: 0;">
					<p class="sea-export-2">Consignment Details</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4 sea-export-3">${seaImportHBLData.myshipmentTrackHeader[0].sales_org_desc}</div>
					<div class="col-md-4"></div>
					<div class="col-md-4 img-global">
						<img
							src="${pageContext.request.contextPath}/MyShipmentFrontApp/resources/images/${seaImportHBLData.myshipmentTrackHeader[0].comp_code}.jpg"
							alt="Company Logo" />
					</div>

				</div>
			</div>
		</div>
	</section>
	<section style="background-color: #fff; padding: 5px 15px;">
		<div class="table-responsive">
			<table class="table table-hover table-inverse">

				<tbody>
					<tr>
						<th scope="row">HBL No.</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].bl_no }</td>
						<th scope="row">MBL Number</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].mbl_no }</td>
					</tr>

					<tr>
						<th scope="row">Shipper Name</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].shipper }</td>
						<th scope="row">Consignee Name</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].buyer }</td>
					</tr>
					<tr>
						<th scope="row">Origin Agent</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].agent }</td>
						<th scope="row">Place of Receipt</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].por }</td>
					</tr>
					<tr>
						<th scope="row">Port Of Loading</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].pol }</td>
						<th scope="row">Port of Discharge</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].pod }</td>
					</tr>
					<tr>
						<th scope="row">Shipment Mode</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].ship_mode }</td>
						<th scope="row">TOS</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].frt_mode_ds }</td>
					</tr>
					<tr>
						<th scope="row">Shipping Line</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].shipper }</td>
						<th scope="row">Co-Loader1</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].coloader }</td>
					</tr>
					<tr>
						<th scope="row">L/C No.</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].lc_no }</td>
						<th scope="row">L/C Date</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].lc_dt_openTracking}" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].lc_dt}"
							var="lc_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${lc_date}" /></td>

						<!-- <td>${seaImportHBLData.myshipmentTrackHeader[0].lc_dt }</td> -->
					</tr>
					<tr>
						<th scope="row">ETA</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].eta_dt_openTracking }" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].eta_dt}"
							var="eta_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${eta_date}" /></td>

						<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].eta_dt }</td>-->
						<th scope="row">Shipped on Board</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].dep_dt}"
							var="dep_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${dep_date}" /></td>
					</tr>
					<tr>
						<th scope="row">ATA</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt_openTracking }" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].ata_dt}"
							var="ata_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${ata_date}" /></td>

						<!-- <td>${seaImportHBLData.myshipmentTrackHeader[0].ata_dt }</td> -->
						<th scope="row">Import Rotation No</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].rot_no }</td>
					</tr>
					<tr>
						<th scope="row">NOC / DO Issue Date</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].noc_dt_openTracking }" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].noc_dt}"
							var="noc_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${noc_date}" /></td>

						<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].noc_dt }</td>-->
						<th scope="row">De-Stuffing Date</th>
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt_openTracking }" /></td>
						<fmt:parseDate
							value="${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt}"
							var="unstf_date" pattern="yyyyMMdd" />
						<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
								value="${unstf_date}" /></td>

						<!--  <td>${seaImportHBLData.myshipmentTrackHeader[0].unstf_dt }</td>-->
					</tr>
					<tr>
						<th scope="row">Total Quantity</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].tot_qty }</td>
						<fmt:parseNumber var="totqty" integerOnly="true" type="number"
							value="${seaImportHBLData.myshipmentTrackHeader[0].tot_qty }" />
						<td>${totqty}</td>
						<th scope="row">Total Volume (CBM)</th>
						<td>${seaImportHBLData.myshipmentTrackHeader[0].volume }</td>
					</tr>
				</tbody>
			</table>
		</div>
	</section>

	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12" style="margin: 0; padding: 0;">
					<p class="sea-export-2">Item and Container Details</p>
				</div>
			</div>
		</div>
	</section>
	<section style="background-color: #fff; padding: 5px 15px;">
		<div class="table-responsive">
			<table class="table table-hover table-inverse table-bordered">

				<thead style="background-color: #26acf5;">
					<tr class="info">
						<th>Sl</th>
						<th>Container No</th>
						<th>Size</th>
						<th>Seal No</th>
						<th>Mode</th>
						<th>Commodity</th>
						<th>Quantity</th>
						<th>Vol (CBM)</th>
						<th>Weight</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${seaImportHBLData.myshipmentTrackItem}"
						var="item" varStatus="loopCounter">
						<tr>
							<th scope="row">${loopCounter.count}</th>
							<td>${item.cont_no}</td>
							<td>${item.cont_size}</td>
							<td>${item.seal_no}</td>
							<td>${item.cont_mode}</td>
							<td>${item.commodity}</td>
							<td>${item.item_qty}</td>
							<fmt:parseNumber var="itmqty" integerOnly="true" type="number"
								value="${item.item_qty}" />
							<td>${itmqty}</td>
							<td>${item.item_vol}</td>
							<td>${item.item_grwt}</td>
						</tr>
					</c:forEach>


				</tbody>
			</table>
		</div>
	</section>


	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12" style="margin: 0; padding: 0;">
					<p class="sea-export-2">Vessel Schedule</p>
				</div>
			</div>
		</div>
	</section>
	<section style="background-color: #fff; padding: 5px 15px;">
		<div class="table-responsive">
			<table class="table table-hover table-inverse table-bordered">

				<thead style="background-color: #26acf5;">
					<tr class="info">
						<th>Sl</th>
						<th>Vessel Name</th>
						<th>Vessel Type</th>
						<th>Voyage</th>
						<th>POL</th>
						<th>POD</th>
						<th>ETD</th>
						<th>ETA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${seaImportHBLData.myshipmentTrackSchedule}"
						var="vSchedule" varStatus="loopCounter">
						<tr>
							<th scope="row">${loopCounter.count}</th>
							<td>${vSchedule.car_name}</td>
							<td>${vSchedule.car_type}</td>
							<td>${vSchedule.car_no}</td>
							<td>${vSchedule.pol}</td>
							<td>${vSchedule.pod}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.etd_openTracking}" /></td>
							<fmt:parseDate value="${vSchedule.etd}" var="etd_date"
								pattern="yyyyMMdd" />
							<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
									value="${etd_date}" /></td>
							<td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.eta_openTracking}" /></td>
							<fmt:parseDate value="${vSchedule.eta}" var="eta_date"
								pattern="yyyyMMdd" />
							<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
									value="${eta_date}" /></td>
							<!-- <td>${vSchedule.etd}</td> -->
							<!-- <td>${vSchedule.eta}</td> -->
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</section>
	<section class="footer">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<p class="footer-para">Copyright &copy MyShipment.com</p>

				</div>
			</div>
		</div>
	</section>
</body>





 --%>