<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">
	
</script>
<style>
	html {
		background-color: #FFF;
	}
</style>

<!-- Theme style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/air-sea-export-track.css" />


<section style="background-color: #fff;">
	<div class="container-fluid" >
<!-- 		<div class="row" style="height: 30px;"></div> -->
		<div class="row">
			<div class="col-md-12" >
			
				<p class="air-sea-export-4">Your search yielded no results</p>
				<ul>
					<li>- Check if entered HBL/HAWB is correct.</li>
					<li>- Check if selected company/sales organization is correct.</li>
					<li>- Check if selected shipment mode(SEA or AIR) is correct.</li>
				</ul>
			</div>
		</div>
<!-- 		<div class="row" style="height: 30px;"></div> -->
<%-- 		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 air-sea-export-3">${seaExportHBLData.myshipmentTrackHeader[0].sales_org_desc}</div>
				<div class="col-md-4"></div>
				<div class="col-md-4 img-global">
					<img
						src="${pageContext.request.contextPath}/resources/images/${seaExportHBLData.myshipmentTrackHeader[0].comp_code}.jpg"
						alt="Company Logo" class="imgLogo"/>
				</div>

			</div>
		</div> --%>
	</div>
</section>

<%@include file="footer_v2.jsp"%>