
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	
</script>

<style>
	html, .non-nego-header-section {
		background-color: #fff;
	}
	
	.seaBill-fieldset {
		margin-top: 2%;
	}
	
	.seaBill-form {
		margin-top: 1%;
	}
</style>


<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Non Nego Bill of Lading</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Non Nego Bill of Lading</li>
		</ol>
	</div>
	<div class="assign-po-search">
		<fieldset class="seaBill-fieldset">
			<legend>Sea Bill of Lading</legend>
			<form:form action="NonNegoBillOfLading" method="post" target="_blank"
				commandName="reqParams" class="seaBill-form">
				
				<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>HBL No</label>
							<form:input path="req1" id="number" cssClass="form-control" />
						</div>
					</div>


					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" id="btn-appr-po-search" onclick="return checkHblNumber();"
								class="btn btn-success form-control" value="Submit" />
						</div>
					</div>
				</div>
				<div class='row'>
					<div class='col-sm-4'>
						<div class='form-group'>
							<label style="color: red; margin-top: 2px;">${message}</label>
						</div>
					</div>
				</div>


			</form:form>
		</fieldset>
	</div>
</div>

<script>
	function checkHblNumber() {
		var hblNumber = $("#number").val();
		
		if(hblNumber == "") {
			swal("HBL Number", "Please Enter HBL Number", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>