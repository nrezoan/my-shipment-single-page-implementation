package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.ApproOrderCarrierDtl;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.BuyerDTO;
import com.myshipment.model.CarrierMaster;
import com.myshipment.model.Materials;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.PackingListMdl;
import com.myshipment.model.PurchaseOrderMdl;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.SegPurchaseOrderMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.UpdateEvent;
import com.myshipment.model.WarehouseCode;

public interface IPoMgmtDAO {
	public List<PurchaseOrderMdl> getPoDashBoardData();
	public PurchaseOrderMdl getPoAndLineItemById(Long id);
	public Long updatePoDetails(ApprovedPurchaseOrder appPoOrder);
	public void updateApprovedPoBookingStatusById(ApprovedPurchaseOrder appPoOrder);
	
	public SegPurchaseOrderMdl getLineItemByItemId(Long lineItemId);
	public void updatepoUpdateEvent(UpdateEvent updateEvent);
	public List<ApprovedPurchaseOrder> getApprovedPoLineItemById(Long poId);
	public List<CarrierMaster> getAllCarriers();
	public List<PurchaseOrderMdl> searchPo(String poNumber,String fromDate,String toDate,String supplierCode,String buyerCode);
	public Long updateLineItem(SegPurchaseOrderMdl segPo);
	public Long updateLineItemCarrierIdAndScheduleById(SegPurchaseOrderMdl segPo);
	public Long insertAppCarrDtl(ApproOrderCarrierDtl appOrdCarrDtl);
	public List<ApprovedPurchaseOrder> searchApprovedPo(String poNumber, String fromDate, String toDate, String supplierCode,String buyerCode);
	public ApprovedPurchaseOrder getApprovedItemById(Long appId);
	public void updateApprovedPOById(ApprovedPurchaseOrder appPo);
	public List<ApprovedPurchaseOrder> getApprovedPoNotBookedList(String poNumber,String buyer,String fromDate,String toDate);
	/*public List<ApprovedPurchaseOrder> getPOforBooking(String buyer,String poNumber,String salesOrg,String division,String shipper);*/
	public List<PurchaseOrdersModel> getPOforBooking(String buyer,String poNumber,String shipper);
	
	public void savePackListOrderBooking(PackingListMdl packingListMdl);
	public void saveBookingInfoAsTemplate(SaveAsTemplateMdl bookingInfoAstemplate);
	public List<SaveAsTemplateMdl> getTemplateInfoByLoginCred(SaveAsTemplateMdl saveAsTemplateMdl);
	public SaveAsTemplateMdl getTemplateInfoByTemplateId(String templateId,String customerCode,String company,String operationType,String seaOrAir);
	public void updateTemplateInfo(String temp_Id,String tempInfo);
	public List<Materials> getMaterialsDao();
	public List<StorageLocation> getStorageLocationsDao();
	
	
	public List<BuyerDTO> getHMBuyers(String buyer,String disChnlSelected, String divisionSelected, String salesOrgSelected);
	public List<WarehouseCode> getWHList(String countryCode);
}
