package com.myshipment.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.mappers.PoSearchMapper;
import com.myshipment.mappers.PurchaseOrderMapper;
import com.myshipment.mappers.SegPurchaseOrderMapper;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public class ManualPoCreationDAO implements IManualPoCreationDAO{

	@Autowired
	private PurchaseOrderMapper purchaseOrderMapper;
	@Autowired
	private SegPurchaseOrderMapper segPurchaseOrderMapper;
	@Autowired
	private PoSearchMapper poSearchMapper;
	
	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public Long createManualPO(HeaderPOData headerPOData, List<SegPurchaseOrder> segPurchaseOrders) 
	{

		purchaseOrderMapper.insert(headerPOData);
		for(SegPurchaseOrder segPurchaseOrder : segPurchaseOrders)
		{
			segPurchaseOrder.setPo_id(headerPOData.getPo_id());
			segPurchaseOrder.setCurrentDate(getCurrentDate());
			segPurchaseOrder.setVc_po_no(headerPOData.getVc_po_no());
			segPurchaseOrderMapper.insert(segPurchaseOrder);
		}
		
		return headerPOData.getPo_id();
	}

	private Date getCurrentDate()
	{
		Date currentDate = new Date();
		
		return currentDate;
	}

	@Override
	public List<SegPurchaseOrder> getCurrentlyManuallyUploadedRecords(Long poId) {
		
		List<SegPurchaseOrder> uploadedData = poSearchMapper.getCurrentUploadedDataNew(poId);
		return uploadedData;
	}
}
