package com.myshipment.dao;

import com.myshipment.dto.FileDTO;
import com.myshipment.mappers.PoSearchMapper;
import com.myshipment.mappers.PurchaseOrderMapper;
import com.myshipment.mappers.SegPurchaseOrderMapper;
import com.myshipment.mappers.UploadDataMapper;
import com.myshipment.model.ApprovedPOBean;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.HeaderAndLineitemData;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.POFileDTO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.model.SegPurchaseOrderMdl;
import com.myshipment.model.UpdateEvent;
import com.myshipment.model.ZemailJsonData;
import com.myshipment.model.ZemailParams;
import com.myshipment.service.IEmailService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.RestUtil;
import com.sun.mail.util.MailSSLSocketFactory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class UploadPODataDAO implements IUploadPODataDAO {
	static final Logger logger = Logger.getLogger(UploadPODataDAO.class);
	@Autowired
	private UploadDataMapper uploadDataMapper;
	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	@Autowired
	private PurchaseOrderMapper purchaseOrderMapper;
	@Autowired
	private SegPurchaseOrderMapper segPurchaseOrderMapper;
	@Autowired
	private PoSearchMapper poSearchMapper;
	@Autowired
	private IPoMgmtDAO poMgmtDao;

	public UploadPODataDAO() {
	}

	public void insertPODataToDatabase(List<POFileDTO> poFileDTOLst) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		try {
			for (POFileDTO poFileDTO : poFileDTOLst) {
				uploadDataMapper.insert(poFileDTO);
			}
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
		} finally {
			sqlSession.close();
		}
	}

	@Transactional(rollbackFor = { Exception.class }, propagation = Propagation.REQUIRED)
	public List<SegPurchaseOrder> insertDataInPurchaseOrderAndSegregatedTable(
			HeaderAndLineitemData headerAndLineitemData, FileDTO fileData) {
		logger.info(" Inside UploadPODataDAO.insertDataInPurchaseOrderAndSegregatedTable method.");

		List<SegPurchaseOrder> segPurchaseOrderLst = new ArrayList();
		Map<String, Long> poIdLst = new HashMap();
		Map<String, HeaderPOData> headerItem = null;
		List<POFileDTO> lineItems = null;
		Set<String> poNos = null;

		if (headerAndLineitemData != null) {
			headerItem = headerAndLineitemData.getHeaderItem();
			lineItems = headerAndLineitemData.getLineItems();
			poNos = headerAndLineitemData.getUniquePoNo();
		}

		for (String poNo : poNos) {
			HeaderPOData headerPOData = (HeaderPOData) headerItem.get(poNo);
			purchaseOrderMapper.insert(headerPOData);
			poIdLst.put(poNo, headerPOData.getPo_id());
		}

		for (POFileDTO item : lineItems) {
			SegPurchaseOrder segPurchaseOrder = convertPOFileDTOToSegPurchaseOrder(item);
			segPurchaseOrder.setPo_id((Long) poIdLst.get(item.getVc_po_no()));
			segPurchaseOrderMapper.insert(segPurchaseOrder);
			segPurchaseOrderLst.add(segPurchaseOrder);
		}

		if (fileData.isApproved()) {
			ApprovedPOBean appPurchaseOrder = prepateDataForPOArroved(segPurchaseOrderLst);
			List<ApprovedPurchaseOrder> appPurchaseOrderLst = appPurchaseOrder.getApprovedPO();
			List<UpdateEvent> updateEventLst = appPurchaseOrder.getUpdateEvent();
			List<SegPurchaseOrderMdl> segPurOrderLst = appPurchaseOrder.getSegPurOrder();

			for (ApprovedPurchaseOrder approvedPurchaseOrder : appPurchaseOrderLst) {
				poMgmtDao.updatePoDetails(approvedPurchaseOrder);
			}
			for (UpdateEvent updateEvent : updateEventLst) {
				poMgmtDao.updatepoUpdateEvent(updateEvent);
			}
			for (SegPurchaseOrderMdl segPurchaseOrderMdl : segPurOrderLst) {
				poMgmtDao.updateLineItem(segPurchaseOrderMdl);
			}
		}
		logger.info(" Method UploadPODataDAO.insertDataInPurchaseOrderAndSegregatedTable ends.");

		return segPurchaseOrderLst;
	}

	private SegPurchaseOrder convertPOFileDTOToSegPurchaseOrder(POFileDTO poFileDTO) {
		SegPurchaseOrder segPurchaseOrder = new SegPurchaseOrder();
		segPurchaseOrder.setVc_po_no(poFileDTO.getVc_po_no());
		segPurchaseOrder.setNu_client_code(poFileDTO.getNu_client_code());
		segPurchaseOrder.setDt_etd(poFileDTO.getDt_etd());
		segPurchaseOrder.setFile_upload_date(poFileDTO.getFile_upload_date());
		segPurchaseOrder.setNu_height(poFileDTO.getNu_height());
		segPurchaseOrder.setNu_length(poFileDTO.getNu_length());
		segPurchaseOrder.setNu_no_pcs_ctns(poFileDTO.getNu_no_pcs_ctns());
		segPurchaseOrder.setNu_width(poFileDTO.getNu_width());
		segPurchaseOrder.setSales_org(poFileDTO.getSales_org());
		segPurchaseOrder.setSap_quotation(poFileDTO.getSap_quotation());
		segPurchaseOrder.setVc_article_no(poFileDTO.getVc_article_no());
		segPurchaseOrder.setVc_buy_house(poFileDTO.getVc_buy_house());
		segPurchaseOrder.setVc_buyer(poFileDTO.getVc_buyer());
		segPurchaseOrder.setVc_cbm_sea(poFileDTO.getVc_cbm_sea());
		segPurchaseOrder.setVc_color(poFileDTO.getVc_color());
		segPurchaseOrder.setVc_commodity(poFileDTO.getVc_commodity());
		segPurchaseOrder.setVc_division(poFileDTO.getVc_division());
		segPurchaseOrder.setVc_gr_wt(poFileDTO.getVc_gr_wt());
		segPurchaseOrder.setVc_gw_car(poFileDTO.getVc_gw_car());
		segPurchaseOrder.setVc_hs_code(poFileDTO.getVc_hs_code());
		segPurchaseOrder.setVc_in_hcm(poFileDTO.getVc_in_hcm());
		segPurchaseOrder.setVc_nt_wt(poFileDTO.getVc_nt_wt());
		segPurchaseOrder.setVc_pod(poFileDTO.getVc_pod());
		segPurchaseOrder.setVc_pol(poFileDTO.getVc_pol());
		segPurchaseOrder.setVc_qc_dt(poFileDTO.getVc_qc_dt());
		segPurchaseOrder.setVc_qua_uom(poFileDTO.getVc_qua_uom());
		segPurchaseOrder.setVc_quan(poFileDTO.getVc_quan());
		segPurchaseOrder.setVc_ref_field1(poFileDTO.getVc_ref_field1());
		segPurchaseOrder.setVc_ref_field2(poFileDTO.getVc_ref_field2());
		segPurchaseOrder.setVc_ref_field3(poFileDTO.getVc_ref_field3());
		segPurchaseOrder.setVc_ref_field4(poFileDTO.getVc_ref_field4());
		segPurchaseOrder.setVc_ref_field5(poFileDTO.getVc_ref_field5());
		segPurchaseOrder.setVc_rel_dt(poFileDTO.getVc_rel_dt());
		segPurchaseOrder.setVc_size(poFileDTO.getVc_size());
		segPurchaseOrder.setVc_sku_no(poFileDTO.getVc_sku_no());
		segPurchaseOrder.setVc_style_no(poFileDTO.getVc_style_no());
		segPurchaseOrder.setVc_tot_pcs(poFileDTO.getVc_tot_pcs());
		segPurchaseOrder.setVc_volume(poFileDTO.getVc_volume());
		segPurchaseOrder.setVc_product_no(poFileDTO.getVc_product_no());
		segPurchaseOrder.setCurrentDate(getCurrentDate());

		return segPurchaseOrder;
	}

	private Date getCurrentDate() {
		Date currentDate = new Date();
		return currentDate;
	}

	public List<SegPurchaseOrder> getUploadedRecords(String currentDate) {
		logger.info("Inside UploadPODataDAO.getUploadedRecords method.");

		List<SegPurchaseOrder> uploadedData = poSearchMapper.getCurrentUploadedData(currentDate);

		logger.info("Method UploadPODataDAO.getUploadedRecords ends.");

		return uploadedData;
	}

	public List<List<SegPurchaseOrder>> getCurrentlyUploadedRecordThroughFileUpload(List<Long> poIdLst) {
		List<List<SegPurchaseOrder>> uploadedDataLst = new ArrayList();
		for (Long poId : poIdLst) {
			List<SegPurchaseOrder> uploadedData = poSearchMapper.getCurrentUploadedDataNew(poId);
			uploadedDataLst.add(uploadedData);
		}

		return uploadedDataLst;
	}

	private ApprovedPOBean prepateDataForPOArroved(List<SegPurchaseOrder> segPurchaseOrderLst) {
		ApprovedPOBean approvedPOBean = new ApprovedPOBean();
		List<ApprovedPurchaseOrder> approvedPurchaseOrderLst = new ArrayList();
		List<UpdateEvent> updateEventLst = new ArrayList();
		List<SegPurchaseOrderMdl> segPurchOrderLst = new ArrayList();
		double totalPcs = 0.0D;

		for (SegPurchaseOrder segPurchaseOrder : segPurchaseOrderLst) {
			ApprovedPurchaseOrder apo = new ApprovedPurchaseOrder();

			apo.setDt_etd(segPurchaseOrder.getDt_etd() == null ? "" : segPurchaseOrder.getDt_etd());
			apo.setFile_name(segPurchaseOrder.getFile_name());
			apo.setFile_upload_date(segPurchaseOrder.getFile_upload_date());
			apo.setNu_client_code(segPurchaseOrder.getNu_client_code());
			apo.setNu_hieght(segPurchaseOrder.getNu_height());
			apo.setNu_length(segPurchaseOrder.getNu_length());
			apo.setNu_no_pcs_ctns(segPurchaseOrder.getNu_no_pcs_ctns());
			apo.setNu_width(segPurchaseOrder.getNu_width());
			apo.setSales_org(segPurchaseOrder.getSales_org());
			apo.setSap_quotation(segPurchaseOrder.getSap_quotation());
			apo.setVc_article_no(segPurchaseOrder.getVc_article_no());
			apo.setVc_buy_house(segPurchaseOrder.getVc_buy_house());
			apo.setVc_buyer(segPurchaseOrder.getVc_buyer());
			apo.setVc_cbm_sea(segPurchaseOrder.getVc_cbm_sea());
			apo.setVc_color(segPurchaseOrder.getVc_color());
			apo.setVc_commodity(segPurchaseOrder.getVc_commodity());
			apo.setVc_division(segPurchaseOrder.getVc_division());
			apo.setVc_gr_wt(segPurchaseOrder.getVc_gr_wt());
			apo.setVc_gw_car(segPurchaseOrder.getVc_gw_car());
			apo.setVc_hs_code(segPurchaseOrder.getVc_hs_code());
			apo.setVc_in_hcm(segPurchaseOrder.getVc_in_hcm());
			apo.setVc_nt_wt(segPurchaseOrder.getVc_nt_wt());
			apo.setVc_nw_car(segPurchaseOrder.getVc_nw_car());
			apo.setVc_po_no(segPurchaseOrder.getVc_po_no());
			apo.setVc_pod(segPurchaseOrder.getVc_pod());
			apo.setVc_pol(segPurchaseOrder.getVc_pol());
			apo.setVc_product_no(segPurchaseOrder.getVc_product_no());
			apo.setVc_qc_dt(segPurchaseOrder.getVc_qc_dt());
			apo.setVc_qua_uom(segPurchaseOrder.getVc_qua_uom());
			apo.setVc_quan(segPurchaseOrder.getVc_quan());
			apo.setVc_ref_field1(segPurchaseOrder.getVc_ref_field1());
			apo.setVc_ref_field2(segPurchaseOrder.getVc_ref_field2());
			apo.setVc_ref_field3(segPurchaseOrder.getVc_ref_field3());
			apo.setVc_ref_field4(segPurchaseOrder.getVc_ref_field4());
			apo.setVc_ref_field5(segPurchaseOrder.getVc_ref_field5());
			apo.setVc_rel_dt(segPurchaseOrder.getVc_rel_dt());
			apo.setVc_size(segPurchaseOrder.getVc_size());
			apo.setVc_sku_no(segPurchaseOrder.getVc_sku_no());
			apo.setVc_style_no(segPurchaseOrder.getVc_style_no());
			apo.setVc_volume(segPurchaseOrder.getVc_volume());
			apo.setBooking_status("");
			apo.setSeg_id(segPurchaseOrder.getSeg_id());
			apo.setVc_tot_pcs(segPurchaseOrder.getVc_tot_pcs());
			apo.setApprove_comment("");
			apo.setCarrier_id("");
			apo.setCarrier_schedule("");

			approvedPurchaseOrderLst.add(apo);

			UpdateEvent updateEvent = new UpdateEvent();

			updateEvent.setAction("POAPPROVE");
			updateEvent.setVc_po_no(segPurchaseOrder.getVc_po_no());
			updateEvent.setSeg_id(segPurchaseOrder.getSeg_id());
			updateEvent.setItems(segPurchaseOrder.getVc_tot_pcs());
			updateEventLst.add(updateEvent);

			SegPurchaseOrderMdl segPurchaseOrderMdl = new SegPurchaseOrderMdl();
			segPurchaseOrderMdl.setVc_tot_pcs(totalPcs);
			segPurchaseOrderMdl.setSeg_id(segPurchaseOrder.getSeg_id());
			segPurchOrderLst.add(segPurchaseOrderMdl);
		}

		approvedPOBean.setApprovedPO(approvedPurchaseOrderLst);
		approvedPOBean.setSegPurOrder(segPurchOrderLst);
		approvedPOBean.setUpdateEvent(updateEventLst);

		return approvedPOBean;
	}
	
	/*Sanjana Khan*/

	@Override
	public List<ApprovedPurchaseOrder> savePOGeneratedInfo(List<ApprovedPurchaseOrder> appPOList) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		List<ApprovedPurchaseOrder> approvedPOList = new ArrayList<ApprovedPurchaseOrder>();
		try {
			for (ApprovedPurchaseOrder appPO: appPOList) {
				uploadDataMapper.insertPOGeneratedInfo(appPO);
				
				
				//email sending
				/*ZemailParams zemailParams=new ZemailParams();
				 zemailParams.setBuyer(orderHeader.getBuyer());
				 //zemailParams.setHblNumber(directBookingJsonData.getHblnumber());
				
				 zemailParams.setCustomerNumber(loginDTO.getLoggedInUserName());
				 zemailParams.setDistChannel(loginDTO.getDisChnlSelected());
				 zemailParams.setDivision(loginDTO.getDivisionSelected());
				 zemailParams.setSalesOrg(loginDTO.getSalesOrgSelected());
				 ZemailJsonData zemailJsonData=restService.postForObject(RestUtil.prepareUrlForService(new StringBuffer(RestUtil.EMAIL)).toString(), zemailParams, ZemailJsonData.class);
				
				Properties properties=new Properties();
				InputStream inputStream=null;
				try {
					inputStream=new FileInputStream(new File("/WEB-INF/classes/email/emailconfig.properties"));
					inputStream=this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
					properties.load(inputStream);
					//properties.put(key, value);
					properties.put("mail.smtp.EnableSSL.enable","true");
		            
		            MailSSLSocketFactory sf=new MailSSLSocketFactory();
		            sf.setTrustAllHosts(true);
		            
		              properties.put("mail.smtp.ssl.socketFactory", sf);
					emailService=(IEmailService)serviceLocator.findService("emailService");
					emailService.creatDirectbookingMailParamsAndSendMail(zemailJsonData, properties.getProperty(CommonConstant.DIRECT_BOOKING_EMAIL_TEMPLATE_NAME), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_TO), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_SUBJECT));
				} catch (Exception e) {
					e.printStackTrace();
					logger.debug(""+e.getMessage());
				}finally
				{
					if(inputStream!=null)
					{
						try {
							inputStream.close();
						} catch (Exception e2) {
							// TODO: handle exception
						}
					}
				}
				
				
				
				
				*/
				
				
				
				approvedPOList.add(appPO);
			}
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
		} finally {
			sqlSession.close();
		}
		
		return approvedPOList;

	}
}