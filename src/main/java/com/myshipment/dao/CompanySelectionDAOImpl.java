/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.mappers.CompanySelectionMapper;
import com.myshipment.model.DefaultCompany;
import com.myshipment.model.IntelligentCompanyUsage;

public class CompanySelectionDAOImpl implements ICompanySelectionDAO {

	private Logger logger = Logger.getLogger(CompanySelectionDAOImpl.class);
	
	@Autowired
	private CompanySelectionMapper companySelectionMapper;
	
	@Override
	public List<IntelligentCompanyUsage> getCompanyUsageList(String userCode) throws Exception {
		
		logger.info(this.getClass() + "DAO for Company Usage List Called");
		List<IntelligentCompanyUsage> result = new ArrayList<IntelligentCompanyUsage>();
		
		result = companySelectionMapper.intelligentCompanyUsageList(userCode);
		
		return result;
	}

	@Override
	public DefaultCompany getDefaultCompany(String userCode) throws Exception {
		logger.info(this.getClass() + "DAO for Selected Default Company Called");
		DefaultCompany defaultCompany = new DefaultCompany();
		
		defaultCompany = companySelectionMapper.selectedDefaultCompany(userCode);
		
		return defaultCompany;
	}

	@Override
	public int saveDefaultCompany(DefaultCompany defaultCompany) throws Exception {
		
		return companySelectionMapper.insertDefaultCompany(defaultCompany.getUserId(), defaultCompany.getPreferredCompanyId(), defaultCompany.getDistributionChannel(), defaultCompany.getDivision(), defaultCompany.getActive(), defaultCompany.getUpdatedDate());
	}

	@Override
	public int updateDefaultCompany(DefaultCompany defaultCompany) throws Exception {
		
		return companySelectionMapper.updateDefaultCompany(defaultCompany.getUserId(), defaultCompany.getPreferredCompanyId(), defaultCompany.getDistributionChannel(), defaultCompany.getDivision(), defaultCompany.getActive(), defaultCompany.getUpdatedDate());
	}

	@Override
	public int setActiveDefaultCompany(DefaultCompany defaultCompany) throws Exception{
		
		return companySelectionMapper.setActiveDefaultCompany(defaultCompany.getUserId(), defaultCompany.getActive(), defaultCompany.getUpdatedDate());
	}

}
