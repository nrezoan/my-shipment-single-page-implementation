package com.myshipment.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.mappers.BookedPOApprovalMapper;
import com.myshipment.model.BookedPOApproval;

public class BookedPOApprovalDao implements IBookedPOApprovalDao{
	
	@Autowired
	private BookedPOApprovalMapper bookedPOApprovalMapper; 

	@Override
	@Transactional
	public void savePostBookingPoApprovalData(List<BookedPOApproval> bpoapplstdao) {
		// TODO Auto-generated method stub
		int count = 0;
		for(BookedPOApproval bookedPOApproval : bpoapplstdao) {
			count = bookedPOApprovalMapper.getApprovedBookedPOByPrimaryKeys(bookedPOApproval);
			if(count > 0) {
				bookedPOApprovalMapper.updateApprovedBookedPO(bookedPOApproval);
			}else {
				bookedPOApprovalMapper.saveApprovedBookedPO(bookedPOApproval);
			}
		}
	}
	
	@Override
	@Transactional
	public List<BookedPOApproval> getApprovedBookedPOByDate(Date startDate, Date endDate, String buyer) {
		List<BookedPOApproval> bookPoApproveList = new ArrayList<BookedPOApproval>();
		try {
			bookPoApproveList = bookedPOApprovalMapper.fetchDataByDateParams(startDate, endDate, buyer);
			return bookPoApproveList;
		}catch(Exception ex) {
			ex.printStackTrace();
			return bookPoApproveList;
		}
	}

	@Override
	public List<BookedPOApproval> getApprovedBookedPOByStatus(Date startDate, Date endDate, String status, String buyer) {
		List<BookedPOApproval> bookPoApproveList = new ArrayList<BookedPOApproval>();
		try {
			bookPoApproveList = bookedPOApprovalMapper.fetchDataByStatus(startDate, endDate, status, buyer);
			return bookPoApproveList;
		}catch(Exception ex) {
			ex.printStackTrace();
			return bookPoApproveList;
		}
	}

}
