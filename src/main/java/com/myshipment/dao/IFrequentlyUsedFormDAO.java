package com.myshipment.dao;


import java.util.List;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
/*
 * @ Gufranur Rahman
 */
public interface IFrequentlyUsedFormDAO {

	//public SegPurchaseOrder getSegPurchaseOrderBasedOnSegId(int segId);
	public int insetFrequentlyUsedFormIntoTable(FrequentlyUsedFormMdl obj);
	public List <FrequentUsedMenu> getFrequentlyUsedMenu(LoginDTO loginDTO);
}
