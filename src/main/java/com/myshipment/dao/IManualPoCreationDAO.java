package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface IManualPoCreationDAO {

	public Long createManualPO(HeaderPOData headerPOData, List<SegPurchaseOrder> egPurchaseOrder);
	
	public List<SegPurchaseOrder> getCurrentlyManuallyUploadedRecords(Long poId);
}
