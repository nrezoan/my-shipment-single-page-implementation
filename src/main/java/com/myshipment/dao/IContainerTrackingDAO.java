/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.ContainerInformation;
import com.myshipment.model.ContainerLocationDetails;
import com.myshipment.model.ContainerTrackingSummary;
import com.myshipment.model.ContainerTypeInformation;

public interface IContainerTrackingDAO {

	public List<ContainerInformation> getContainerInfoData(String containerNumber) throws Exception;
	public List<ContainerLocationDetails> getContainerDetailsData(String containerNumber) throws Exception;
	public List<ContainerTrackingSummary> getContainerSummaryData(String containerNumber) throws Exception;
	public List<ContainerTypeInformation> getContainerTypeInfo(String containerNumber) throws Exception;
}
