package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.POFileInfo;
/*
 * @Ranjeet Kumar
 */
public interface IPOFileReaderDAO {

	public void savePOFileData(List<POFileInfo> fileInfoLst);
	public POFileInfo getFileInfoForDuplicateCheck(String fileName);
}
