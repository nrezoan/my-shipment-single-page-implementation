package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.SegPurchaseOrder;
/*
 * @Ranjeet Kumar
 */
public interface IXMLFileReaderDAO {

	public void saveXMLDataToTheDatabase(List<List<SegPurchaseOrder>> segPurchaseOrderLst);
}
