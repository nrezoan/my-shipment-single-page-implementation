package com.myshipment.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.dto.SegregationDTO;
import com.myshipment.mappers.SegregationMapper;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public class SegregationDAO implements ISegregationDAO{

	private static final Logger logger = Logger.getLogger(SegregationDAO.class);
	
	@Autowired
	private SegregationMapper segregationMapper;

	@Override
	public SegPurchaseOrder getSegPurchaseOrderBasedOnSegId(int segId) {
		logger.info("Method SegregationDAO.getSegPurchaseOrderBasedOnSegId starts.And segreation id is : "+segId);
		
		SegPurchaseOrder segPurchaseOrder = segregationMapper.getSegregatePurchaseOrderBasedOnSegId(segId);
		
		logger.info("Method SegregationDAO.getSegPurchaseOrderBasedOnSegId starts.And segPurchaseOrder is : "+segPurchaseOrder);
		
		return segPurchaseOrder;
	}

	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public int insetSegregattedDataIntoSegPurchaseOrderTable(SegregationDTO segregationDTO) {

		logger.info("Method SegregationDAO.insetSegregattedDataIntoSegPurchaseOrderTable starts.And segregationDTO is : "+segregationDTO);
		
		int updateStatus = 0;
		
		List<SegPurchaseOrder> segPurOrderLst = segregationDTO.getDynamicData();

		if(segPurOrderLst != null && segPurOrderLst.size() > 0){
			for(SegPurchaseOrder segPurchaseOrder : segPurOrderLst)
			{
				segPurchaseOrder.setPo_id(segregationDTO.getPo_id());
				segPurchaseOrder.setCurrentDate(getCurrentDate());
				segPurchaseOrder.setSegregattedAgainst(segregationDTO.getSeg_id());
				segPurchaseOrder.setVc_po_no(segregationDTO.getVc_po_no());
				segregationMapper.insertData(segPurchaseOrder);
				
			}

			updateStatus = segregationMapper.updateData(segregationDTO.getSeg_id());
		}
		logger.info("Method SegregationDAO.insetSegregattedDataIntoSegPurchaseOrderTable starts.And updateStatus is : "+updateStatus);
		
		return updateStatus;
	}

	private Date getCurrentDate()
	{
		Date currentDate = new Date();
		return currentDate;
	}

}
