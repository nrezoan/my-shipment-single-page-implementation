package com.myshipment.dao;

import java.util.List;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface ISearchPoDAO {

	public List<HeaderPOData> getPosBasedOnPoNo(SearchPoDTO searchPoDto);
	
	public List<HeaderPOData> getPosBasedOnPoNoAndDate(SearchPoDTO searchPoDto);
	
	//public List<HeaderPOData> getPosBasedOnTodateAndFromdate(SearchPoDTO searchPoDto);
	
	public List<SegPurchaseOrder> getAllLineItemPoData(String poNo);
	
	public List<SegPurchaseOrder> getPosBasedOnTodateAndFromdate(SearchPoDTO searchPoDto);
	
	public List<SegPurchaseOrder> getPosBasedOnTodateAndFromdateAndPoNo(SearchPoDTO searchPoDto);
	
	public List<SegPurchaseOrder> getAllDataForNoInput();
	
	public Long updatePoBasedOnSegId(SegPurchaseOrder segPurchaseOrder);
}
