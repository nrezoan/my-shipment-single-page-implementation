/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.mappers.ContainerTrackingMapper;
import com.myshipment.model.ContainerInformation;
import com.myshipment.model.ContainerLocationDetails;
import com.myshipment.model.ContainerTrackingSummary;
import com.myshipment.model.ContainerTypeInformation;

public class ContainerTrackingDAOImpl implements IContainerTrackingDAO {
	
	Logger logger = Logger.getLogger(ContainerTrackingDAOImpl.class);

	@Autowired
	private ContainerTrackingMapper containerTrackingMapper;
	
	@Override
	public List<ContainerLocationDetails> getContainerDetailsData(String containerNumber) throws Exception {
		List<ContainerLocationDetails> result = new ArrayList<ContainerLocationDetails>();
		
		result = containerTrackingMapper.containerTrackDetails(containerNumber);
		
		return result;
	}

	@Override
	public List<ContainerTrackingSummary> getContainerSummaryData(String containerNumber) throws Exception {
		List<ContainerTrackingSummary> result = new ArrayList<ContainerTrackingSummary>();
		
		result = containerTrackingMapper.containerTrackSummary(containerNumber);
		
		return result;
	}

	@Override
	public List<ContainerInformation> getContainerInfoData(String containerNumber)
			throws Exception {
		List<ContainerInformation> result = new ArrayList<ContainerInformation>();
		
		result = containerTrackingMapper.containerInformation(containerNumber);
		
		return result;
	}

	@Override
	public List<ContainerTypeInformation> getContainerTypeInfo(String containerNumber) {
		
		logger.info("Container Type Info DAO called...");
		List<ContainerTypeInformation> result = new ArrayList<ContainerTypeInformation>();
		
		result = containerTrackingMapper.containerTypeInformation(containerNumber);
		
		logger.info("Result Set Found : " + result);
		
		return result;
	}

}
