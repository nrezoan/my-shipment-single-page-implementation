/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.mappers.PreAlertManagementMapper;
import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertMBL;
import com.myshipment.util.MyshipmentDatabaseComponent;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

public class PreAlertManagementDAOImpl implements IPreAlertManagementDAO {
	
	private Logger logger = Logger.getLogger(PreAlertManagementDAOImpl.class);
	
	@Autowired
	private PreAlertManagementMapper preAlertManagementMapper;
	
	@Override
	public int updatePreAlertDocumentPath(PreAlertHBL preAlertHBL) throws Exception {
		return preAlertManagementMapper.updatePreAlertDocumentPath(preAlertHBL);
	}
	
	@Override
	public int updateMblDownloadStatus(PreAlertHBL preAlertHBL) throws Exception {
		return preAlertManagementMapper.updateMblDownloadStatus(preAlertHBL);
	}

	@Override
	public List<PreAlertHBL> getPreAlertByHBL(String supplierId) throws Exception {
		return preAlertManagementMapper.selectPreAlertHblList(supplierId);
	}

	@Override
	public int insertPreAlertHBL(String hblNumber, String supplierId, String buyerId, String agentId, String distributionChannel, String division, String salesOrg) throws Exception {
		return preAlertManagementMapper.insertPreAlertHBLafterBooking(hblNumber, supplierId, buyerId, agentId, distributionChannel, division, salesOrg);
	}

	@Override
	public PreAlertHBL getPreAlertBySingleHBL(PreAlertHBL preAlertHbl, String myshipmentUid) {
		return preAlertManagementMapper.selectPreAlertHbl(preAlertHbl, myshipmentUid);
	}

	@Override
	public List<PreAlertHBL> getPreAlertToDownload(String myshipmentUID, PreAlertHBL preAlertHbl) throws Exception {
		return preAlertManagementMapper.selectPreAlertReadyToDownload(myshipmentUID, preAlertHbl);
	}
	
	@Override
	public List<PreAlertHBL> getPreAlertBuyerWiseList(PreAlertHBL preAlertHbl, String status) throws Exception {
		List<PreAlertHBL> preAlertHblList = new ArrayList<PreAlertHBL>();
		if(status.equalsIgnoreCase("ALL")) {
			preAlertHblList = preAlertManagementMapper.selectBuyerPreAlertHblListALL(preAlertHbl);
		} else if(status.equalsIgnoreCase("PENDING")) {
			preAlertHblList = preAlertManagementMapper.selectBuyerPreAlertHblListPENDING(preAlertHbl);
		} else if(status.equalsIgnoreCase("COMPLETED")) {
			preAlertHblList = preAlertManagementMapper.selectBuyerPreAlertHblListCOMPLETED(preAlertHbl);
		}
		return preAlertHblList;
	}

	@Override
	public PreAlertHBL checkAuthForHBL(PreAlertHBL preAlertHBL) throws Exception {
		return preAlertManagementMapper.checkAuthHBL(preAlertHBL);
	}

	@Override
	public List<PreAlertHBL> getMblwisePreAlertList(String myshipmentUid, PreAlertHBL preAlertHbl) throws Exception {
		return preAlertManagementMapper.mblWisePreAlert(myshipmentUid, preAlertHbl);
	}

	@Override
	public int saveMBLDocument(PreAlertHBL preAlertHbl, String[] hblNumber) throws Exception {
		MyshipmentDatabaseComponent db = new MyshipmentDatabaseComponent();
		
		int status = 0;
		try {
			logger.info(this.getClass() + ": Call procedure SAVE_OR_UPDATE_MBL...");
			CallableStatement callableStatement = db.connection.prepareCall("{call SAVE_OR_UPDATE_MBL(?, ?, ?, ?, ?, ?, ?, ?)}");
			
			ArrayDescriptor arrayDesc = ArrayDescriptor.createDescriptor("T_VARCHAR2_ARRAY", db.connection);
			
			Array array = new ARRAY(arrayDesc, db.connection, hblNumber);
			
			callableStatement.setArray(1, array);
			callableStatement.setString(2, preAlertHbl.getMblNumber());
			callableStatement.setString(3, preAlertHbl.getFileMBL());
			callableStatement.setString(4, preAlertHbl.getIsMBL());
			callableStatement.setString(5, "1");
			callableStatement.setString(6, preAlertHbl.getCrmId());
			callableStatement.setString(7, preAlertHbl.getAgentId());
			callableStatement.registerOutParameter(8, java.sql.Types.VARCHAR);
			
			callableStatement.execute();
			
			String procedureStatus = callableStatement.getString(8);
			
			if(procedureStatus.equals("SUCCESS")) {
				status = 1;
			} else if(procedureStatus.equals("ERROR")) {
				status = 0;
			}
			
			logger.info(this.getClass() + ": Successful procedure call...");
		} catch (SQLException e) {
			e.printStackTrace();
			status = 0;//failure
			db.rollBackTransaction();
		}
		db.commitTransaction();
		
		return status;
	}

	@Override
	public PreAlertMBL checkMBLExist(PreAlertHBL preAlertHBL) throws Exception {
		return null;
	}

	@Override
	public int updateMBLDocument(PreAlertHBL preAlertHbl) throws Exception {
		return 0;
	}

}
