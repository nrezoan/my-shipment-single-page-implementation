package com.myshipment.dao;

import com.myshipment.dto.SegregationDTO;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface ISegregationDAO {

	public SegPurchaseOrder getSegPurchaseOrderBasedOnSegId(int segId);
	public int insetSegregattedDataIntoSegPurchaseOrderTable(SegregationDTO segregationDTO);
}
