package com.myshipment.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.mappers.UpdateLoggerMapper;
import com.myshipment.model.CRMLogger;
import com.myshipment.model.ReportParams;

public class UpdateLoggerDAOImpl implements IUpdateLoggerDAO {
	private Logger logger = Logger.getLogger(UpdateLoggerDAOImpl.class);

	@Autowired
	UpdateLoggerMapper updateLoggerMapper;

	@Override
	public int getUpdateLogger(List<CRMLogger> updateLoggerList) {

		int result = 0;
		for (CRMLogger updateLogger : updateLoggerList) {
			result = updateLoggerMapper.insertUpdateLogger(updateLogger);
		}

		return result;
	}

	@Override
	public List<CRMLogger> getLoggerHblWise(CRMLogger req) {
		if (req.getHblNumber().isEmpty()) {
			return updateLoggerMapper.allLogger(req);
		} else {
			return updateLoggerMapper.hblwiseLogger(req);
		}
	}

}
