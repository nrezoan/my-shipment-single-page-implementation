package com.myshipment.dao;

import java.util.Date;
import java.util.List;

import com.myshipment.model.BookedPOApproval;
/*
 * @ Hamid
 */
public interface IBookedPOApprovalDao {
	
	public void savePostBookingPoApprovalData(List<BookedPOApproval> bpoapplstdao);

	public List<BookedPOApproval> getApprovedBookedPOByDate(Date startDate, Date endDate, String buyer);

	public List<BookedPOApproval> getApprovedBookedPOByStatus(Date startDate, Date endDate, String status, String buyer);
	
}
