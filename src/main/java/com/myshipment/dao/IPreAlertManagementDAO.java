/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertMBL;

public interface IPreAlertManagementDAO {

	public int insertPreAlertHBL(String hblNumber, String supplierId, String buyerId, String agentId, String distributionChannel, String division, String salesOrg) throws Exception;
	public int updatePreAlertDocumentPath(PreAlertHBL preAlertHBL) throws Exception;
	
	/**
	 * Returns the status of database transaction (failed-0, success-1)
	 * @param 
	 * 			(Save MBL/MAWB with File location file)
	 * @return
	 */
	public int saveMBLDocument(PreAlertHBL preAlertHbl, String[] hblNumber) throws Exception;
	
	/**
	 * Returns the status of database transaction (failed-0, success-1)
	 * @param 
	 * 			(Update MBL/MAWB with File location file)
	 * @return
	 */
	public int updateMBLDocument(PreAlertHBL preAlertHbl) throws Exception;
	
	/**
	 * Returns if MBL exists or not
	 * @param MBL number, sales organization, distribution channel, division and CRM ID
	 * @return
	 */
	
	public int updateMblDownloadStatus(PreAlertHBL preAlertHbl) throws Exception;
	
	
	public PreAlertMBL checkMBLExist(PreAlertHBL preAlertHBL) throws Exception;
	public List<PreAlertHBL> getPreAlertByHBL(String supplierId) throws Exception;
	public PreAlertHBL getPreAlertBySingleHBL(PreAlertHBL preAlertHbl, String myshipmentUid) throws Exception;
	public PreAlertHBL checkAuthForHBL(PreAlertHBL preAlertHBL) throws Exception;
	public List<PreAlertHBL> getPreAlertToDownload(String myshipmentUID, PreAlertHBL preAlertHbl) throws Exception;
	public List<PreAlertHBL> getPreAlertBuyerWiseList(PreAlertHBL preAlertHbl, String status) throws Exception;
	public List<PreAlertHBL> getMblwisePreAlertList(String myshipmentUid, PreAlertHBL preAlertHbl) throws Exception;
	
}
