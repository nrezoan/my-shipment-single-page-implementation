package com.myshipment.service;

import java.util.List;

import com.myshipment.model.OAGRequest;
import com.myshipment.model.OAG;

public interface IOAGService {

	public List<OAG> getFlightDetails(OAGRequest oagRequest);
}
