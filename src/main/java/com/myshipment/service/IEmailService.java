package com.myshipment.service;

import javax.mail.MessagingException;

import org.thymeleaf.context.Context;

import com.myshipment.model.ZemailJsonData;

public interface IEmailService {

	 public void sendMailWithInlineImageAndWithoutAttachment(
	            final String templateName, final String recipientEmail,final String[] recipents,final String mailFrom,final String subject,final Context ctx,final String headerImage,final String footerImage,final byte[] headerBytes,final byte[] footerBytes)
	            throws MessagingException;
	 public  void creatDirectbookingMailParamsAndSendMail(ZemailJsonData zemailJsonData,String templateName,String mailTo,String mailFrom,String subject);
	 public void recoverPasswordMail(String mail, String property, String subject, String password) throws MessagingException;

}
