package com.myshipment.service;

import java.util.Date;
import java.util.List;

import com.myshipment.model.BookedPOApproval;
/*
 * @ Hamid
 */
public interface IBookedPOApprovalService {
	
	public void saveBookedPoApprovalData(List<BookedPOApproval> bpoapplstserv);

	public List<BookedPOApproval> getBookedPoApprovalDataByDate(Date start, Date end, String buyer);

	public List<BookedPOApproval> getBookedPoApprovalDataByStatus(Date start, Date end, String status, String buyer);
	
}
