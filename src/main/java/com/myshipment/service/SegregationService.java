package com.myshipment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.ISegregationDAO;
import com.myshipment.dto.SegregationDTO;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @Ranjeet Kumar
 */
@Service
public class SegregationService implements ISegregationService{

	@Autowired
	private ISegregationDAO segregationDAO;
	
	@Override
	public SegPurchaseOrder getLineItemForSegregation(int segId) {
		SegPurchaseOrder segPurchaseOrder = segregationDAO.getSegPurchaseOrderBasedOnSegId(segId);
		return segPurchaseOrder;
	}

	@Override
	public int doSegregation(SegregationDTO segregationDTO) {
		
		int status = segregationDAO.insetSegregattedDataIntoSegPurchaseOrderTable(segregationDTO);
		return status;
	}

}
