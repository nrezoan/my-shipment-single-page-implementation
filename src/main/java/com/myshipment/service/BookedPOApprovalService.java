package com.myshipment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IBookedPOApprovalDao;
import com.myshipment.model.BookedPOApproval;

@Service
public class BookedPOApprovalService implements IBookedPOApprovalService{
	
	@Autowired
	private IBookedPOApprovalDao bookedPoApprovalDao;
	

	public IBookedPOApprovalDao getBookedPoApprovalDao() {
		return bookedPoApprovalDao;
	}
	public void setBookedPoApprovalDao(IBookedPOApprovalDao bookedPoApprovalDao) {
		this.bookedPoApprovalDao = bookedPoApprovalDao;
	}
	@Override
	public void saveBookedPoApprovalData(List<BookedPOApproval> bpoapplstserv) {
		// TODO Auto-generated method stub
		bookedPoApprovalDao.savePostBookingPoApprovalData(bpoapplstserv);
	}
	@Override
	public List<BookedPOApproval> getBookedPoApprovalDataByDate(Date start, Date end, String buyer) {
		// TODO Auto-generated method stub
		List<BookedPOApproval> bookAppList = new ArrayList<BookedPOApproval>();
		bookAppList = bookedPoApprovalDao.getApprovedBookedPOByDate(start, end, buyer);
		return bookAppList;
	}
	@Override
	public List<BookedPOApproval> getBookedPoApprovalDataByStatus(Date start, Date end, String status, String buyer) {
		// TODO Auto-generated method stub
		List<BookedPOApproval> bookAppList = new ArrayList<BookedPOApproval>();
		bookAppList = bookedPoApprovalDao.getApprovedBookedPOByStatus(start, end, status, buyer);
		return bookAppList;
	}

}
