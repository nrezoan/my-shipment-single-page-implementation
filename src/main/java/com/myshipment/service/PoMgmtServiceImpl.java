package com.myshipment.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.dao.IPoMgmtDAO;
import com.myshipment.dto.ApprovedPoDTO;
import com.myshipment.dto.PurchaseOrderDTO;
import com.myshipment.model.ApproOrderCarrierDtl;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.CarrierMaster;
import com.myshipment.model.PurchaseOrderMdl;
import com.myshipment.model.SegPurchaseOrderMdl;
import com.myshipment.model.UpdateEvent;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;

public class PoMgmtServiceImpl implements IPoMgmtService {
	private Logger logger = Logger.getLogger(PoMgmtServiceImpl.class);
	@Autowired
	private IPoMgmtDAO poMgmtDao;

	public IPoMgmtDAO getPoMgmtDao() {
		return poMgmtDao;
	}

	public void setPoMgmtDao(IPoMgmtDAO poMgmtDao) {
		this.poMgmtDao = poMgmtDao;
	}
	
	//load  dash board data
	public List<PurchaseOrderDTO> getPoDashBoardData()
	{
		logger.debug(this.getClass().getName()+"getPoDashBoardData(): get Po Dash Board data");
		List<PurchaseOrderMdl> listOfPurchaseOrder=poMgmtDao.getPoDashBoardData();
		logger.debug(this.getClass().getName()+"getPoDashBoardData():"+listOfPurchaseOrder);
		List<PurchaseOrderDTO> purchaseOrderDtoLst=new ArrayList<PurchaseOrderDTO>();
		for(PurchaseOrderMdl purchaseOrder:listOfPurchaseOrder)
		{
			for(SegPurchaseOrderMdl segPo:purchaseOrder.getLineItems())
			{
			PurchaseOrderDTO poDto=new PurchaseOrderDTO();
			poDto.setVc_size(segPo.getVc_size());
			poDto.setVc_color(segPo.getVc_color());
			poDto.setVc_sku_no(segPo.getVc_sku_no());
			poDto.setVc_product_no(purchaseOrder.getVc_product_no());
			poDto.setVc_po_no(purchaseOrder.getVc_po_no());
			poDto.setSeg_id(segPo.getSeg_id());
			poDto.setSupplier_code(purchaseOrder.getSupplier_code());
			poDto.setSales_org(purchaseOrder.getSales_org());
			poDto.setPo_id(purchaseOrder.getPo_id());
			poDto.setNu_client_code(purchaseOrder.getNu_client_code());
		
			poDto.setPo_creation_date(DateUtil.formatDateToString(purchaseOrder.getPo_creation_date()));
			purchaseOrderDtoLst.add(poDto);
			}
		}
		logger.debug(this.getClass().getName()+"getPoDashBoardData():"+purchaseOrderDtoLst);
		return purchaseOrderDtoLst;
	}

	public int approvePo(List<Map<String,Object>> listOfMap)
	
	{
		Long poId=0l;
		Long segId=0l;
		String carrierSch="";
		String carrierId="";
		Double totalPcs=0.0;
		String comment="";
		int noOfSuccess=0;
	
		for(Map<String,Object> map:listOfMap)
		{
			if(map.containsKey("seg_id"))
			{
				segId=(Long.parseLong((String)map.get("seg_id")));
			}
			if(map.containsKey("vc_tot_pcs"))
			{
				totalPcs=Double.parseDouble((String)map.get("vc_tot_pcs"));
			}
			if(map.containsKey("approve_comment"))
			{
				comment=(String)map.get("approve_comment");
			}
			if(map.containsKey("po_id"))
			{
				poId=Long.parseLong((String)map.get("po_id"));
			}
			if(map.containsKey("carrier_schedule"))
			{
				carrierSch=(String)map.get("carrier_schedule");
			}
			if(map.containsKey("carrier_id"))
			{
				carrierId=((String)map.get("carrier_id"));
			}
			logger.debug(this.getClass().getName()+"approvePo(): Parameters are: poId="+poId+", segId="+segId+",carrierSch="+carrierSch+",carrierId="+carrierId+",totalPcs="+totalPcs+",comment="+comment);
			SegPurchaseOrderMdl poLineItem=poMgmtDao.getLineItemByItemId(segId);
			//logger.debug(this.getClass().getName()+"approvePo()"+poLineItem);
			ApprovedPurchaseOrder apo=new ApprovedPurchaseOrder();
			//apo.setDt_etd(DateUtil.formatStringToDatenew(DateUtil.YYYYMMDDToDD_MM_YYYY(poLineItem.getDt_etd()), CommonConstant.DATE_FORMAT_DD_MM_YYYY));
			apo.setDt_etd(poLineItem.getDt_etd()==null?"":poLineItem.getDt_etd());
			
			apo.setFile_name(poLineItem.getFile_name());
			apo.setFile_upload_date(poLineItem.getFile_upload_date());
			apo.setNu_client_code((poLineItem.getNu_client_code()!=null && !poLineItem.getNu_client_code().equalsIgnoreCase(""))?poLineItem.getNu_client_code():poLineItem.getPurchaseOrder().getNu_client_code());
			apo.setNu_hieght(poLineItem.getNu_hieght());
			apo.setNu_length(poLineItem.getNu_length());
			apo.setNu_no_pcs_ctns(poLineItem.getNu_no_pcs_ctns());
			apo.setNu_width(poLineItem.getNu_width());
			apo.setSales_org(poLineItem.getSales_org()!=null && !poLineItem.getSales_org().equals("")?poLineItem.getSales_org():poLineItem.getPurchaseOrder().getSales_org());
			apo.setSap_quotation(poLineItem.getSap_quotation());
			apo.setVc_article_no(poLineItem.getVc_article_no());
			apo.setVc_buy_house(poLineItem.getVc_buy_house()!=null && !poLineItem.getVc_buy_house().equals("")?poLineItem.getVc_buy_house():poLineItem.getPurchaseOrder().getVc_buy_house());
			apo.setVc_buyer(poLineItem.getVc_buyer()!=null && !poLineItem.getVc_buyer().equals("")?poLineItem.getVc_buyer():poLineItem.getPurchaseOrder().getVc_buyer());
			apo.setVc_cbm_sea(poLineItem.getVc_cbm_sea());
			apo.setVc_color(poLineItem.getVc_color());
			apo.setVc_commodity(poLineItem.getVc_commodity());
			apo.setVc_division(poLineItem.getVc_division());
			apo.setVc_gr_wt(poLineItem.getVc_gr_wt());
			apo.setVc_gw_car(poLineItem.getVc_gw_car());
			apo.setVc_hs_code(poLineItem.getVc_hs_code());
			apo.setVc_in_hcm(poLineItem.getVc_in_hcm());
			apo.setVc_nt_wt(poLineItem.getVc_nt_wt());
			apo.setVc_nw_car(poLineItem.getVc_nw_car());
			apo.setVc_po_no(poLineItem.getVc_po_no()!=null && !poLineItem.getVc_po_no().equals("")?poLineItem.getVc_po_no():poLineItem.getPurchaseOrder().getVc_po_no());
			apo.setVc_pod(poLineItem.getVc_pod());
			apo.setVc_pol(poLineItem.getVc_pol());
			apo.setVc_product_no(poLineItem.getVc_product_no());
			apo.setVc_qc_dt(poLineItem.getVc_qc_dt());
			apo.setVc_qua_uom(poLineItem.getVc_qua_uom());
			apo.setVc_quan(poLineItem.getVc_quan());
			apo.setVc_ref_field1(poLineItem.getVc_ref_field1());
			apo.setVc_ref_field2(poLineItem.getVc_ref_field2());
			apo.setVc_ref_field3(poLineItem.getVc_ref_field3());
			apo.setVc_ref_field4(poLineItem.getVc_ref_field4());
			apo.setVc_ref_field5(poLineItem.getVc_ref_field5());
			apo.setVc_rel_dt(poLineItem.getVc_rel_dt());
			apo.setVc_size(poLineItem.getVc_size());
			apo.setVc_sku_no(poLineItem.getVc_sku_no());
			apo.setVc_style_no(poLineItem.getVc_style_no());
			apo.setVc_volume(poLineItem.getVc_volume());
			apo.setBooking_status("");
			if(totalPcs!=0.0 && totalPcs<=poLineItem.getVc_tot_pcs())
			{
				logger.debug(this.getClass().getName()+"approvePo(): total pcs are less than actual total pcs");
				UpdateEvent updateEvent=new UpdateEvent();
				apo.setSeg_id(poLineItem.getSeg_id());
				apo.setVc_po_no(poLineItem.getVc_po_no());
				apo.setApprove_comment(comment);
				apo.setCarrier_id(carrierId);
				apo.setCarrier_schedule(carrierSch);
				
				updateEvent.setVc_po_no(poLineItem.getVc_po_no());
				updateEvent.setSeg_id(apo.getSeg_id());
				 
				/*if(0l!=carrierId )
				{
					logger.debug(this.getClass().getName()+"approvePo(): Carrier id is not 0 so will be assigning carrier as well  ");
				ApproOrderCarrierDtl appCrrDtl=new ApproOrderCarrierDtl();
				apo.setVc_tot_pcs(0.0);
				poLineItem.setVc_tot_pcs(poLineItem.getVc_tot_pcs()-totalPcs);
				Long appId=poMgmtDao.updatePoDetails(apo);
				
				appCrrDtl.setApp_id(appId);
				appCrrDtl.setCaarrier_schedule(carrierSch);
				appCrrDtl.setCarr_ass_comment(comment);
				appCrrDtl.setCarrier_name("");
				appCrrDtl.setVc_po_no(apo.getVc_po_no());
				appCrrDtl.setNo_of_items_approved(totalPcs);
				poMgmtDao.insertAppCarrDtl(appCrrDtl);
				logger.debug(this.getClass().getName()+"approvePo(): po approved data updated ");
				
				 updateEvent.setAction(CommonConstant.PO_APPROVE_ACTION_NAME);
				 updateEvent.setItems(totalPcs);
				 poMgmtDao.updatepoUpdateEvent(updateEvent);
				 updateEvent.setAction(CommonConstant.PO_ASSIGNCARRIER_ACTION_NAME);
				 updateEvent.setItems(totalPcs);
				 poMgmtDao.updatepoUpdateEvent(updateEvent);
				 logger.debug(this.getClass().getName()+"approvePo(): history updated");
				}*/
				//else
				//{
					logger.debug(this.getClass().getName()+"approvePo(): Not assigning carrier");
					apo.setVc_tot_pcs(totalPcs);
					poLineItem.setVc_tot_pcs(poLineItem.getVc_tot_pcs()-totalPcs);
					poMgmtDao.updatePoDetails(apo);
					updateEvent.setAction(CommonConstant.PO_APPROVE_ACTION_NAME);
					updateEvent.setItems(totalPcs);
					poMgmtDao.updatepoUpdateEvent(updateEvent);
					logger.debug(this.getClass().getName()+"approvePo(): history updated");
				//}
					poLineItem.setCarrier_id(carrierId);
					poLineItem.setCarrier_schedule(carrierSch);
					poMgmtDao.updateLineItem(poLineItem);
					logger.debug(this.getClass().getName()+"approvePo(): Updated original data");
				noOfSuccess++;
				}
			
				}
		
		return noOfSuccess;
		
	}
	
	
	// Load po approve search page
	public PurchaseOrderDTO loadPoLineItemById(Long id)
	{
		PurchaseOrderMdl po=poMgmtDao.getPoAndLineItemById(id);
		PurchaseOrderDTO LineItemtoApprove=new PurchaseOrderDTO();
		BeanUtils.copyProperties(po, LineItemtoApprove);
		return LineItemtoApprove;
	}
	
	public ApprovedPoDTO loadApprovedPoLineItemById(Long poId)
	{
		List<ApprovedPurchaseOrder> listPOs=poMgmtDao.getApprovedPoLineItemById(poId);
		List<CarrierMaster> listOfCarrier=poMgmtDao.getAllCarriers();
		ApprovedPoDTO lineItemApproved=new ApprovedPoDTO();
		PurchaseOrderMdl po=new PurchaseOrderMdl();
		//po=listPOs.get(0).getPurchaseOrder();
		
		
		
		//BeanUtils.copyProperties(purchaseOrder, LineItemtoApprove);
		return lineItemApproved;
	}

	public List<PurchaseOrderDTO> searchPo(String poNumber, String fromDate, String toDate, String supplierCode,String buyerCode) {

		List<PurchaseOrderMdl> lstPo = poMgmtDao.searchPo(poNumber == "" ? null : poNumber,
				fromDate == "" ? null : fromDate, toDate == "" ? null : toDate,
						supplierCode == "" ? null : supplierCode,buyerCode);
		List<CarrierMaster> listOfCarrier = poMgmtDao.getAllCarriers();
		List<PurchaseOrderDTO> lstPODto = new ArrayList<PurchaseOrderDTO>();
		for (Iterator iterator = lstPo.iterator(); iterator.hasNext();) {
			PurchaseOrderMdl purchaseOrder = (PurchaseOrderMdl) iterator.next();
			for (Iterator iterator2 = purchaseOrder.getLineItems().iterator(); iterator2.hasNext();) {
				SegPurchaseOrderMdl segPurchaseOrder = (SegPurchaseOrderMdl) iterator2.next();
				PurchaseOrderDTO poDto = new PurchaseOrderDTO();
				poDto.setCarrierSchedule(segPurchaseOrder.getCarrier_schedule()==null?"":segPurchaseOrder.getCarrier_schedule());
				poDto.setCarrierId(segPurchaseOrder.getCarrier_id()==null?"":segPurchaseOrder.getCarrier_id());
				
				poDto.setVc_tot_pcs(segPurchaseOrder.getVc_tot_pcs());
				poDto.setCarrierMaster(null != listOfCarrier ? listOfCarrier : null);
				poDto.setVc_size(null == segPurchaseOrder.getVc_size() ? "" : segPurchaseOrder.getVc_size());
				poDto.setVc_color(null == segPurchaseOrder.getVc_color() ? "" : segPurchaseOrder.getVc_color());
				poDto.setVc_sku_no(null == segPurchaseOrder.getVc_sku_no() ? "" : segPurchaseOrder.getVc_sku_no());
				poDto.setSeg_id(segPurchaseOrder.getSeg_id() == null ? 0l : segPurchaseOrder.getSeg_id());
				poDto.setNu_client_code(
						purchaseOrder.getNu_client_code() == null ? "" : purchaseOrder.getNu_client_code());
				poDto.setPo_creation_date(purchaseOrder.getPo_creation_date() == null ? ""
						: DateUtil.formatDateToString(purchaseOrder.getPo_creation_date()));
				poDto.setPo_id(purchaseOrder.getPo_id() == null ? 0l : purchaseOrder.getPo_id());
				poDto.setSales_org(purchaseOrder.getSales_org() == null ? "" : purchaseOrder.getSales_org());
				poDto.setSupplier_code(
						purchaseOrder.getSupplier_code() == null ? "" : purchaseOrder.getSupplier_code());
				poDto.setVc_po_no(purchaseOrder.getVc_po_no() == null ? "" : purchaseOrder.getVc_po_no());
				poDto.setVc_product_no(
						purchaseOrder.getVc_product_no() == null ? "" : purchaseOrder.getVc_product_no());
				lstPODto.add(poDto);
			}
		}
		return lstPODto;
	}
	
	
	
	public List<ApprovedPoDTO> searchApprovedPo(String poNumber, String fromDate, String toDate, String supplierCode,String buyerCode)
	{
		List<ApprovedPurchaseOrder> lstPo = poMgmtDao.searchApprovedPo(poNumber == "" ? null : poNumber,
				fromDate == "" ? null : fromDate, toDate == "" ? null : toDate,
						supplierCode == "" ? null : supplierCode,buyerCode);
		List<CarrierMaster> listOfCarrier = poMgmtDao.getAllCarriers();
		List<ApprovedPoDTO> lstPODto = new ArrayList<ApprovedPoDTO>();
		for (Iterator iterator = lstPo.iterator(); iterator.hasNext();) {
			ApprovedPurchaseOrder approvedPurchaseOrder = (ApprovedPurchaseOrder) iterator.next();
			ApprovedPoDTO  apoDto=new ApprovedPoDTO();
			apoDto.setApp_id(approvedPurchaseOrder.getApp_id()==null?0l:approvedPurchaseOrder.getApp_id());
			apoDto.setCarrierMaster(listOfCarrier);
			apoDto.setCarrierSchedule("");
			apoDto.setNu_client_code(approvedPurchaseOrder.getNu_client_code()==null?"":approvedPurchaseOrder.getNu_client_code());
			//apoDto.setPo_creation_date(DateUtil.formatDateToString(approvedPurchaseOrder.getPurchaseOrder().getPo_creation_date()));
			apoDto.setPo_id(approvedPurchaseOrder.getPo_id()==null?0l:approvedPurchaseOrder.getPo_id());
			apoDto.setSales_org(approvedPurchaseOrder.getSales_org()==null?"":approvedPurchaseOrder.getSales_org());
			apoDto.setSupplier_code(approvedPurchaseOrder.getNu_client_code()==null?"":approvedPurchaseOrder.getNu_client_code());
			apoDto.setVc_color(approvedPurchaseOrder.getVc_color()==null?"":approvedPurchaseOrder.getVc_color());
			apoDto.setVc_tot_pcs(approvedPurchaseOrder.getVc_tot_pcs()==0.0?0.0:approvedPurchaseOrder.getVc_tot_pcs());
			apoDto.setVc_po_no(approvedPurchaseOrder.getVc_po_no()==null?"":approvedPurchaseOrder.getVc_po_no());
			apoDto.setVc_product_no(approvedPurchaseOrder.getVc_product_no()==null?"":approvedPurchaseOrder.getVc_product_no());
			apoDto.setVc_size(approvedPurchaseOrder.getVc_size()==null?"":approvedPurchaseOrder.getVc_size());
			apoDto.setVc_sku_no(approvedPurchaseOrder.getVc_sku_no()==null?"":approvedPurchaseOrder.getVc_sku_no());
			lstPODto.add(apoDto);
			
			
			
		}
		return lstPODto;
	}

	@Override
	public boolean assignCarrier(List<Map<String, Object>> assCarrLst) {
		
		Long poId;
		Long appId=0l;
		Long segId=0l;
		String carrierSch="";
		String carrierId="";
		Double totalPcs=0.0;
		String comment="";
		boolean success=true;
	
		for(Map<String,Object> map:assCarrLst)
		{
			if(map.containsKey("seg_id"))
			{
				segId=(Long.parseLong((String)map.get("seg_id")));
			}
			if(map.containsKey("vc_total_pcs"))
			{
				totalPcs=Double.parseDouble((String)map.get("vc_total_pcs"));
			}
			if(map.containsKey("comments"))
			{
				comment=(String)map.get("comments");
			}
			if(map.containsKey("po_id"))
			{
				poId=Long.parseLong((String)map.get("po_id"));
			}
			if(map.containsKey("carrier_schedule"))
			{
				carrierSch=(String)map.get("carrier_schedule");
			}
			if(map.containsKey("carrier_id"))
			{
				carrierId=((String)map.get("carrier_id"));
			}
			//ApprovedPurchaseOrder poAppItem=poMgmtDao.getApprovedItemById(appId);
			//ApproOrderCarrierDtl appOrderCrrDtl=new ApproOrderCarrierDtl();
			/*if(totalPcs<=poAppItem.getVc_tot_pcs())
			{
				if(totalPcs!=0.0)
				{
					appOrderCrrDtl.setNo_of_items_approved(totalPcs);
					poAppItem.setVc_tot_pcs(poAppItem.getVc_tot_pcs()-totalPcs);
				}
				else
				{
					appOrderCrrDtl.setNo_of_items_approved(poAppItem.getVc_tot_pcs());	
					poAppItem.setVc_tot_pcs(poAppItem.getVc_tot_pcs()-poAppItem.getVc_tot_pcs());
				}
				appOrderCrrDtl.setApp_id(poAppItem.getApp_id());
				appOrderCrrDtl.setCaarrier_schedule(carrierSch);
				appOrderCrrDtl.setCarr_ass_comment(comment);
				appOrderCrrDtl.setCarrier_name("");
				appOrderCrrDtl.setVc_po_no(null==poAppItem.getVc_po_no()?"":poAppItem.getVc_po_no());
				poMgmtDao.insertAppCarrDtl(appOrderCrrDtl);
				poMgmtDao.updateApprovedPOById(poAppItem);
				UpdateEvent updateEvent=new UpdateEvent();
				updateEvent.setSeg_id(poAppItem.getSeg_id());
				updateEvent.setVc_po_no(null==poAppItem.getVc_po_no()?"":poAppItem.getVc_po_no());
				updateEvent.setItems(appOrderCrrDtl.getNo_of_items_approved());
				updateEvent.setAction(CommonConstant.PO_ASSIGNCARRIER_ACTION_NAME);
				poMgmtDao.updatepoUpdateEvent(updateEvent);
				
			}*/
			
			//poMgmtDao.updateLineItemCarrierIdAndScheduleById 
			SegPurchaseOrderMdl segPo=new SegPurchaseOrderMdl();
			segPo.setCarrier_id(carrierId);
			segPo.setCarrier_schedule(carrierSch);
			segPo.setSeg_id(segId);
			poMgmtDao.updateLineItemCarrierIdAndScheduleById(segPo);
			
			UpdateEvent updateEvent=new UpdateEvent();
			updateEvent.setSeg_id(segId);
			updateEvent.setVc_po_no("");
			updateEvent.setItems(0);
			updateEvent.setAction(CommonConstant.PO_ASSIGNCARRIER_ACTION_NAME);
			poMgmtDao.updatepoUpdateEvent(updateEvent);
			
		}
		 return success;
		
		
	}
}
