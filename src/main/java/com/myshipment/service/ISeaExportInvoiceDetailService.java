package com.myshipment.service;

import com.myshipment.model.InvoiceParam;
import com.myshipment.model.SexInvJsonData;



public interface ISeaExportInvoiceDetailService {

	public SexInvJsonData getSeaExportInvoiceDetail(InvoiceParam param);
}
