package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.myshipment.model.InvoiceListJsonData;
import com.myshipment.model.InvoiceParam;
import com.myshipment.model.ReportParams;
import com.myshipment.model.SexInvJsonData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class SeaExportInvoiceDetailServiceImpl implements ISeaExportInvoiceDetailService{

	private Logger logger = Logger.getLogger(SeaExportInvoiceDetailServiceImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public SexInvJsonData getSeaExportInvoiceDetail(InvoiceParam req) {
		SexInvJsonData sexInvJsonData = null;
		StringBuffer url=new StringBuffer(RestUtil.SEA_EXPORT_INVOICE_DETAIL);
		sexInvJsonData = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, SexInvJsonData.class);
		return sexInvJsonData;
	}


}
