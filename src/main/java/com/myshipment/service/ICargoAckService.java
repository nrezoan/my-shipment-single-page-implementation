package com.myshipment.service;

import com.myshipment.model.CargoAckReportJsonData;
import com.myshipment.model.ReportParams;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingOrderReportJsonData;


public interface ICargoAckService {

	public CargoAckReportJsonData getCargoAckDetail(ReportParams req);
}
