package com.myshipment.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.myshipment.model.EmailItems;
import com.myshipment.model.ZbapiPacListH;
import com.myshipment.model.ZbapiPacListI;
import com.myshipment.model.ZemailJsonData;
import com.myshipment.model.zmail;

import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.*;
import com.sun.mail.util.MailSSLSocketFactory;

@Service
public class EmailService implements IEmailService {

	//private static final String HEADER_LOGO = "email/htmltemplate/images/Email-Logo.png";
	private static final String HEADER_LOGO = "email/htmltemplate/images/Email-Logo.bmp";
	private static final String FOOTER_LOGO = "email/htmltemplate/images/mgh-logo.bmp";
	private static final String TEMPLATEPATH = "email/htmltemplate";

	private static final String PNG_MIME = "image/png";
	private static final String BMP_MIME = "image/bmp";

	@SuppressWarnings("unused")
	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private TemplateEngine templateEngine;

	/*
	 * Send HTML mail with attachment.
	 */
	/*
	 * public void sendMailWithInlineImageAndAttachment( final String
	 * templateName, final String recipientEmail, final String
	 * attachmentFileName, final String attachementPath) throws
	 * MessagingException {
	 * 
	 * // Prepare the evaluation context final Context ctx = new
	 * Context(locale); ctx.setVariable("name", recipientName);
	 * ctx.setVariable("subscriptionDate", new Date());
	 * ctx.setVariable("hobbies", Arrays.asList("Cinema", "Sports", "Music"));
	 * 
	 * // Prepare message using a Spring helper final MimeMessage mimeMessage =
	 * this.mailSender.createMimeMessage(); final MimeMessageHelper message =
	 * new MimeMessageHelper(mimeMessage, true multipart , "UTF-8");
	 * message.setSubject("Example HTML email with attachment");
	 * message.setFrom("thymeleaf@example.com"); message.setTo(recipientEmail);
	 * 
	 * // Create the HTML body using Thymeleaf final String htmlContent =
	 * this.htmlTemplateEngine.process(EMAIL_WITHATTACHMENT_TEMPLATE_NAME, ctx);
	 * message.setText(htmlContent, true isHtml );
	 * 
	 * // Add the attachment final InputStreamSource attachmentSource = new
	 * ByteArrayResource(attachmentBytes); message.addAttachment(
	 * attachmentFileName, attachmentSource, attachmentContentType);
	 * 
	 * // Send mail this.mailSender.send(mimeMessage); }
	 */

	/*
	 * Send HTML mail with inline image
	 */
	public void sendMailWithInlineImageAndWithoutAttachment(
	            final String templateName, final String recipientEmail,String[] recipents,final String mailFrom,final String subject,final Context ctx,final String headerImage,final String footerImage,final byte[] headerBytes,final byte[] footerBytes)
	            throws MessagingException {
		try {
			ctx.setVariable("headerLogoName", headerImage);
	        //ctx.setVariable("footerLogoName", footerImage);
	        final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
	            
	        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true /* multipart */, "UTF-8");
	        message.setSubject(subject);
	        message.setFrom(mailFrom);
	        if(recipents==null || recipents.length<=0)
	            message.setTo(recipientEmail);
	        else {
	            	Address [] internetAddress=new InternetAddress[recipents.length];
	            	int counter=0;
	            	for (String  address : recipents) {
	            		internetAddress[counter]=new InternetAddress(address);
	            		counter++;
					}
	            	mimeMessage.addRecipients(RecipientType.TO, internetAddress);
	            	
	        }
	        // Create the HTML body using Thymeleaf
	        final String htmlContent = this.templateEngine.process(TEMPLATEPATH+File.separator+templateName, ctx);
	        message.setText(htmlContent, true /* isHtml */);

	        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
	        final InputStreamSource imageSourceHeader = new ByteArrayResource(headerBytes);
	        message.addInline(headerImage , imageSourceHeader, EmailService.BMP_MIME);
	        final InputStreamSource imageSourceFooter=new ByteArrayResource(footerBytes);
	        message.addInline(footerImage, imageSourceFooter, EmailService.BMP_MIME);
	                    
	        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	        Properties props = new Properties();
			try {
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.host", "117.120.5.149");
				props.put("mail.debug", "true");
				props.put("mail.smtp.EnableSSL.enable", "true");
				props.put("mail.smtp.port", "587");
				props.put("mail.smtp.starttls.enable", "true");
		
				MailSSLSocketFactory sf = new MailSSLSocketFactory();
				sf.setTrustAllHosts(true);
		
				props.put("mail.smtp.ssl.socketFactory", sf);
			} catch (Exception ex) {
					ex.printStackTrace();
			}
	            
	        mailSender.setJavaMailProperties(props);
	        mailSender.setHost("117.120.5.149");
	        mailSender.setPort(587);
	        mailSender.setProtocol("smtp");
	        mailSender.setUsername("no-reply@myshipment.com");
	        mailSender.setPassword("A5vu23#x");	            
	        mailSender.send(mimeMessage);	            
	           
		}catch(Exception mailEx) {
			Address[] validUnsentAddresses = null;
			if(mailEx instanceof MailSendException) {
				Exception[] exArray = ((MailSendException) mailEx).getMessageExceptions();
				SendFailedException sfe =  (SendFailedException) exArray[0];
				validUnsentAddresses = sfe.getValidUnsentAddresses();
			}
			
			ctx.setVariable("headerLogoName", headerImage);
			//ctx.setVariable("footerLogoName", footerImage);
	        final MimeMessage mimeMessage_again = this.mailSender.createMimeMessage();
	            
	        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage_again, true /* multipart */, "UTF-8");
	        message.setSubject(subject);
	        message.setFrom(mailFrom);
	        if(validUnsentAddresses != null && validUnsentAddresses.length > 0)
	        	mimeMessage_again.addRecipients(RecipientType.TO, validUnsentAddresses);
	        final String htmlContent = this.templateEngine.process(TEMPLATEPATH+File.separator+templateName, ctx);
	        message.setText(htmlContent, true /* isHtml */);

	        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
	        final InputStreamSource imageSourceHeader = new ByteArrayResource(headerBytes);
	        message.addInline(headerImage , imageSourceHeader, EmailService.BMP_MIME);
	        final InputStreamSource imageSourceFooter=new ByteArrayResource(footerBytes);
	        message.addInline(footerImage, imageSourceFooter, EmailService.BMP_MIME);
	                    
	        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	        Properties props = new Properties();
			try {
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.host", "117.120.5.149");
				props.put("mail.debug", "true");
				props.put("mail.smtp.EnableSSL.enable", "true");
				props.put("mail.smtp.port", "587");
				props.put("mail.smtp.starttls.enable", "true");
		
				MailSSLSocketFactory sf = new MailSSLSocketFactory();
				sf.setTrustAllHosts(true);
		
				props.put("mail.smtp.ssl.socketFactory", sf);
			} catch (Exception ex) {
					ex.printStackTrace();
			}
	            
	        mailSender.setJavaMailProperties(props);
	        mailSender.setHost("117.120.5.149");
	        mailSender.setPort(587);
	        mailSender.setProtocol("smtp");
	        mailSender.setUsername("no-reply@myshipment.com");
	        mailSender.setPassword("A5vu23#x");	            
	        mailSender.send(mimeMessage_again);
			
	    }		
	}

	public void creatDirectbookingMailParamsAndSendMail(ZemailJsonData zemailJsonData, String templateName,
			String mailTo, String mailFrom, String subject) {
		Context context = new Context();
		// context.setVariable("hblNumber", "GFL06663987777");
		context = createContextVariables(context, zemailJsonData);
		File headerImage = new File(getClass().getClassLoader().getResource(EmailService.HEADER_LOGO).getFile());
		File footerImage = new File(getClass().getClassLoader().getResource(EmailService.FOOTER_LOGO).getFile());
		byte[] headerBytes = new byte[(int) headerImage.length()];
		byte[] footerBytes = new byte[(int) footerImage.length()];
		String address[] = null;
		if (zemailJsonData.getEmailAddress().size() > 0) {
			address = new String[zemailJsonData.getEmailAddress().size()];
			int i = 0;
			for (Iterator<zmail> iterator = zemailJsonData.getEmailAddress().iterator(); iterator.hasNext();) {
				zmail zmail = (zmail) iterator.next();
				address[i] = zmail.getEmailAddress();
				i++;
			}
		}
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(headerImage);
			fileInputStream.read(headerBytes);
			fileInputStream.close();
			fileInputStream = new FileInputStream(footerImage);
			fileInputStream.read(footerBytes);
			fileInputStream.close();
			sendMailWithInlineImageAndWithoutAttachment(templateName, mailTo, address, mailFrom, subject, context,
					headerImage.getName(), footerImage.getName(), headerBytes, footerBytes);
		} catch (MessagingException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
    public void recoverPasswordMail(String mailTo,String mailFrom,String subject,String text)throws MessagingException
    {
  	   final MimeMessage mimeMessage = this.mailSender.createMimeMessage(); 
          final MimeMessageHelper message	= new MimeMessageHelper(mimeMessage, true /* multipart */, "UTF-8");
          message.setSubject(subject);
          message.setFrom(mailFrom);
          message.setTo(mailTo);
          message.setText(text);
          JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
          Properties props = new Properties();
          try{
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.host", "117.120.5.149");
          props.put("mail.debug", "true");
          props.put("mail.smtp.EnableSSL.enable","true");
          props.put("mail.smtp.port", "587");
          props.put("mail.smtp.starttls.enable", "true");
          
          MailSSLSocketFactory sf=new MailSSLSocketFactory();
            sf.setTrustAllHosts(true);
          
          props.put("mail.smtp.ssl.socketFactory", sf);
                  
           }catch(Exception ex){
                ex.printStackTrace();
          }
          
          mailSender.setJavaMailProperties(props);
          mailSender.setHost("117.120.5.149");
          mailSender.setPort(587);
          mailSender.setProtocol("smtp");
          mailSender.setUsername("no-reply@myshipment.com");
          mailSender.setPassword("A5vu23#x");
          mailSender.send(mimeMessage);
          
      	
    }
	private Context createContextVariables(Context context, ZemailJsonData zemailJsonData) {
		List<ZbapiPacListH> headeritem = zemailJsonData.getHeaderitem();
		List<ZbapiPacListI> lineItem = zemailJsonData.getLineItem();
		ZbapiPacListH zbapiPacListH = headeritem.get(0);

		context.setVariable("soNumber", zbapiPacListH.getVbeln());
		context.setVariable("hblDate", zbapiPacListH.getZzhblhawbdt());
		context.setVariable("shipperName", zbapiPacListH.getNameAg());
		context.setVariable("commInvDt", zbapiPacListH.getZzcomminvdt());
		context.setVariable("lcNumber", zbapiPacListH.getZzlcpottno());
		context.setVariable("totalQty", zbapiPacListH.getZzsoquantity());
		context.setVariable("pol", zbapiPacListH.getZzportloading());

		context.setVariable("hblNumber", zbapiPacListH.getZhblhawno());
		context.setVariable("buyerName", zbapiPacListH.getNameWe());
		context.setVariable("commInvNo", zbapiPacListH.getZzcomminvno());
		context.setVariable("cargoDeliveryDate", zbapiPacListH.getZzexpdt());
		context.setVariable("lcDate", zbapiPacListH.getZzlcdt());
		context.setVariable("bookingDate", zbapiPacListH.getAudat());
		context.setVariable("pod", zbapiPacListH.getZzportdest());

		List<EmailItems> lstEmailItems = new ArrayList<>();
		for (Iterator<ZbapiPacListI> iterator = lineItem.iterator(); iterator.hasNext();) {
			ZbapiPacListI zbapiPacListI = (ZbapiPacListI) iterator.next();
			EmailItems emailItems = new EmailItems();
			emailItems.poNumber = zbapiPacListI.getZzponumber();
			emailItems.quantity = zbapiPacListI.getZzquantity();
			// emailItems.grossWeight=zbapiPacListI.getZzwtpercartoon();
			emailItems.grossWeight = zbapiPacListI.getZzgr_wt() + "";
			emailItems.totalPcs = zbapiPacListI.getZztotalnopcs();
			emailItems.volume = zbapiPacListI.getZzcbmSea();
			lstEmailItems.add(emailItems);

		}

		context.setVariable("lineItem", lstEmailItems);

		return context;
	}

}
