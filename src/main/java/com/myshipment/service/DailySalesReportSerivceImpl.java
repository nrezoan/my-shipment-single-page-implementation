package com.myshipment.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.myshipment.model.DailySaleReportParams;
import com.myshipment.model.DailySalesReportHawbParams;;

@Service
public class DailySalesReportSerivceImpl implements IDailySalesReportService{

	@Override
	public DailySaleReportParams findMawb(String mawb) {

		return buildDailySalesReport();
	}
	
	public DailySaleReportParams buildDailySalesReport() {
		
		DailySaleReportParams dsrp = new DailySaleReportParams();
		List<DailySalesReportHawbParams> hawbList = new ArrayList<>();
		
		DailySalesReportHawbParams hbl1 = new DailySalesReportHawbParams();
		hbl1.setHawb("HBL1234");
		hbl1.setMawb("MAHWB1234569");
		hbl1.setReceiver("Receiver");
		hbl1.setIncoterms("Incoterms 1");
		hbl1.setOrigin("Origin");
		hbl1.setDestination("Destination");
		hbl1.setGrossWeight(BigDecimal.valueOf(250));
		hbl1.setVolume(BigDecimal.valueOf(30));
		hbl1.setChargeableWeight(BigDecimal.valueOf(870));
		
		hawbList.add(hbl1);
		
		DailySalesReportHawbParams hbl2 = new DailySalesReportHawbParams();
		hbl2.setHawb("HBL1235");
		hbl2.setMawb("MAHWB1234569");
		hbl2.setReceiver("Receiver");
		hbl2.setIncoterms("Incoterms 1");
		hbl2.setOrigin("Origin");
		hbl2.setDestination("Destination");
		hbl2.setGrossWeight(BigDecimal.valueOf(250));
		hbl2.setVolume(BigDecimal.valueOf(30));
		hbl2.setChargeableWeight(BigDecimal.valueOf(870));
		
		hawbList.add(hbl2);
		
		
		DailySalesReportHawbParams hbl3 = new DailySalesReportHawbParams();
		hbl3.setHawb("HBL1234");
		hbl3.setMawb("MAHWB1234569");
		hbl3.setReceiver("Receiver");
		hbl3.setIncoterms("Incoterms 1");
		hbl3.setOrigin("Origin");
		hbl3.setDestination("Destination");
		hbl3.setGrossWeight(BigDecimal.valueOf(250));
		hbl3.setVolume(BigDecimal.valueOf(30));
		hbl3.setChargeableWeight(BigDecimal.valueOf(870));
		
		hawbList.add(hbl3);
		
		dsrp.setMawb("MAHWB1234569");
		dsrp.setGha("GHA");
		dsrp.setOrigin("Origin");
		dsrp.setDestination("Destination");
		dsrp.setTotalPieces(BigDecimal.valueOf(25000));
		dsrp.setWeight(BigDecimal.valueOf(550));
		dsrp.setCwt(BigDecimal.valueOf(750));
		dsrp.setCbm(BigDecimal.valueOf(850));
		dsrp.setTypeOfShipment("Type Of Shipment");
		dsrp.setGrDate(new Date());
		dsrp.setPreAlertDate(new Date());
		dsrp.setEta(new Date());
		dsrp.setEtd(new Date());
		dsrp.setDiff(8);
		dsrp.setAtaPort(new Date());
		dsrp.setWhDate(new Date());
		dsrp.setTypeOfDelivery("Type of Delivery");
		dsrp.setPuTime(1200);
		dsrp.setSlotReqDt(new Date());
		dsrp.setSlotRecDt(new Date());
		dsrp.setCargoStatus("CargoStatus");
		dsrp.setException("Exception");
		dsrp.setRemarks("Remarks");
		dsrp.setWeekendHours(24);
		dsrp.setPreNoticeReq(2);
		dsrp.setDlvDate(3);
		dsrp.setArrToTruckDep(4);
		dsrp.setTruckType("Truck Type");
		dsrp.setTruckBookingDate("Trcuk Booking Date");
		dsrp.setTruckCancelDate("Truck Cancel Date");
		dsrp.setLicensePlate("License Plate");
		dsrp.setDriverDetails("Driver Details");
		dsrp.setGhaToSovAta(new Date());
		dsrp.setGhaToSovAtd(new Date());
		dsrp.setWeek(43);
		dsrp.setYear(2018);
		dsrp.setTsp("TSP");
		dsrp.setFreightService("Freight Service");
		dsrp.setAtd(new Date());
		dsrp.setAta(new Date());
		dsrp.setCutOffDate(new Date());
		dsrp.setEtaDeadline(new Date());
		dsrp.setPortToPort("Port to Port");
		dsrp.setPortToDc("Port to Dc");
		dsrp.setDayDifference(4);
		dsrp.setHawbList(hawbList);
		
		return dsrp;
	}
}
