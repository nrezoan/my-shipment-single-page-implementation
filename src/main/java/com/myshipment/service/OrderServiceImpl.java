package com.myshipment.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.dao.IPoMgmtDAO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.mappers.POUploadMapper;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.BookingByPoDTO;
import com.myshipment.model.BuyerDTO;
import com.myshipment.model.DirectBookingJsonData;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.Materials;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.OrderItem;
import com.myshipment.model.PackListColor;
import com.myshipment.model.PackListSize;
import com.myshipment.model.PackingListMdl;
import com.myshipment.model.PortJsonData;
import com.myshipment.model.PortListBean;
import com.myshipment.model.PortofRecieptParams;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.StoreLocJsonData;
import com.myshipment.model.StoreLocListBean;
import com.myshipment.model.WarehouseCode;
import com.myshipment.model.ZemailJsonData;
import com.myshipment.model.ZemailParams;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.ServiceLocator;
import com.myshipment.util.StringUtil;
import com.sun.mail.util.MailSSLSocketFactory;
/**
 * @author virendra
 *
 */
public class OrderServiceImpl implements IOrderService{
	
	
	private Logger logger = Logger.getLogger(OrderServiceImpl.class);
	@Autowired
	private RestService restService;
	
	private ServiceLocator  serviceLocator;
	public RestService getRestService() {
		return restService;
	}
	@Autowired
	private IPoMgmtDAO poMgmtDao;
	
	@Autowired
	private POUploadMapper poUploadMapper;

	public IPoMgmtDAO getPoMgmtDao() {
		return poMgmtDao;
	}

	public void setPoMgmtDao(IPoMgmtDAO poMgmtDao) {
		this.poMgmtDao = poMgmtDao;
	}
	
	/*@Autowired*/
	private IEmailService emailService;
	

	public void setRestService(RestService restService) {
		this.restService = restService;
	}
	public List<StringBuffer> getPortList(String params)
	{
		StringBuffer url=new StringBuffer(RestUtil.PORT_DETAILS);
		//RestTemplate restTemplate=new RestTemplate();
		//PortJsonData lstListBean= restTemplate.postForObject(RestUtil.REST_URL+"/getPortDetails", "CHITT", PortJsonData.class);
		PortJsonData lstListBean=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), params, PortJsonData.class);
		List<StringBuffer> lstPort=new ArrayList<>();
		StringBuffer data=null; 
		if(null!=lstListBean){
		for (Iterator<PortListBean> iterator = lstListBean.getPortList().iterator(); iterator.hasNext();) {
			PortListBean portListBean =  iterator.next();
			data=new StringBuffer();
			data.append(portListBean.getZzPortName()).append("-").append(portListBean.getZzPort());
			lstPort.add(data);
		}
		}
		return lstPort;
	}
	
	
	public DirectBookingJsonData saveOrderDetailsAndPAckList(List<Map<String, Object>> orderDetails,OrderHeader orderHeader,LoginDTO loginDTO)
	{
		List<OrderItem> lstOderItem = new ArrayList<OrderItem>();
		DirectBookingJsonData directBookingJsonData=null;
		/*directBookingJsonData=new DirectBookingJsonData();
		directBookingJsonData.setHblnumber("123456");
		directBookingJsonData.setZsalesdocument("fdsfdafdsf");*/
		if (null != orderDetails) {
			int posNumber = 10;
			List<Object> lstPackingList=null;
			for (Iterator iterator = orderDetails.iterator(); iterator.hasNext();) {
				OrderItem item = null;
				
				
				Map<String, Object> map = (Map<String, Object>) iterator.next();
				List<Object> lstObject = (List) map.get("itemDetail");
				List<Object> lstExtraObject = (List) map.get("itemExtra");
				lstPackingList=(List)map.get("packinklist");
				if(null!=lstObject && null!=lstExtraObject)
					item=new OrderItem();
				if(null!=lstObject)
				for (Iterator iterator2 = lstObject.iterator(); iterator2.hasNext();) {
					Map<String, Object> itemMap = (Map<String, Object>) iterator2.next();

/*					item.setPoNumber((String) itemMap.get("orderItem_poNumber"));
					item.setProductCode((String) itemMap.get("orderItem_productCode"));
					item.setCartonLength((Double.parseDouble((String) itemMap.get("orderItem_car_length")==null||itemMap.get("orderItem_car_length").equals("")?"0":(String) itemMap.get("orderItem_car_length"))));
					item.setCurtonQuantity(Integer.parseInt((String) itemMap.get("orderItem_car_number")==null||itemMap.get("orderItem_car_number").equals("")?"0":(String) itemMap.get("orderItem_car_number")));
					item.setCartonUnit((String) itemMap.get("orderItem_car_unit"));
					item.setStyleNo((String) itemMap.get("orderItem_styleNo"));
					item.setColor((String) itemMap.get("orderItem_color"));
					item.setCartonWidth(Double.parseDouble((String) itemMap.get("orderItem_car_width")==null|| itemMap.get("orderItem_car_width").equals("")?"0":(String) itemMap.get("orderItem_car_width")));
					item.setNetWeight(Double.parseDouble((String) itemMap.get("orderItem_netWeight")==null|| itemMap.get("orderItem_netWeight").equals("")?"0":(String) itemMap.get("orderItem_netWeight")));
					item.setMaterial((String) itemMap.get("orderItem_commodity"));
					item.setRefNo((String) itemMap.get("orderItem_refNo"));
					item.setSizeNo((String) itemMap.get("orderItem_sizeNo"));
					item.setCartonHeight(Double.parseDouble((String) itemMap.get("orderItem_car_height")==null|| itemMap.get("orderItem_car_height").equals("")?"0":(String) itemMap.get("orderItem_car_height")));
					item.setGrossWeight(Double.parseDouble((String) itemMap.get("orderItem_grossWeight")==null|| itemMap.get("orderItem_grossWeight").equals("")?"0":(String) itemMap.get("orderItem_grossWeight")));
					item.setHsCode((String) itemMap.get("orderItem_hsCode"));
					item.setProjectNo((String) itemMap.get("orderItem_projectNo"));
					item.setUnit((String) itemMap.get("orderItem_unit"));
					item.setTotalCbm(Double.parseDouble((String) itemMap.get("orderItem_total_cbm")==null|| itemMap.get("orderItem_total_cbm").equals("")?("0"):(String) itemMap.get("orderItem_total_cbm")));					
					item.setTotalVolume(Double.parseDouble((String) itemMap.get("orderItem_total_cbm")==null|| itemMap.get("orderItem_total_cbm").equals("")?("0"):(String) itemMap.get("orderItem_total_cbm")));					
					item.setPayCond((String) itemMap.get("orderItem_payCond"));
					item.setUnitPrice(Double.parseDouble((String) itemMap.get("orderItem_unitPrice")==null|| itemMap.get("orderItem_unitPrice").equals("")?"0":(String) itemMap.get("orderItem_unitPrice")));
					item.setCurrency((String) itemMap.get("orderItem_currency"));
					item.setTotalPieces(Integer.parseInt((String) itemMap.get("orderItem_totalPieces")==null|| itemMap.get("orderItem_totalPieces").equals("")?"0":(String) itemMap.get("orderItem_totalPieces")));
					item.setPlant(loginDTO.getSalesOrgSelected().equals("1900")?"1100":loginDTO.getSalesOrgSelected());*/
					
					
					//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					item.setPoNumber((String) itemMap.get("PONumber"));
					item.setProductCode((String) itemMap.get("ProductCode"));
					item.setCartonLength((Double.parseDouble((String) itemMap.get("CartonLength")==null||itemMap.get("CartonLength").equals("")?"0":(String) itemMap.get("CartonLength"))));
					item.setCurtonQuantity(Integer.parseInt((String) itemMap.get("TotalCarton")==null||itemMap.get("TotalCarton").equals("")?"0":(String) itemMap.get("TotalCarton")));
					item.setCartonUnit((String) itemMap.get("CartonMeasurementUnit"));
					item.setStyleNo((String) itemMap.get("Style"));
					item.setColor((String) itemMap.get("Color"));
					item.setCartonWidth(Double.parseDouble((String) itemMap.get("CartonWidth")==null|| itemMap.get("CartonWidth").equals("")?"0":(String) itemMap.get("CartonWidth")));
					item.setNetWeight(Double.parseDouble((String) itemMap.get("NetWeight")==null|| itemMap.get("NetWeight").equals("")?"0":(String) itemMap.get("NetWeight")));
					item.setMaterial((String) itemMap.get("MaterialNo"));
					item.setRefNo((String) itemMap.get("ReferenceNo1"));
					item.setSizeNo((String) itemMap.get("Size"));
					item.setCartonHeight(Double.parseDouble((String) itemMap.get("CartonHeight")==null|| itemMap.get("CartonHeight").equals("")?"0":(String) itemMap.get("CartonHeight")));
					item.setGrossWeight(Double.parseDouble((String) itemMap.get("GrossWeight")==null|| itemMap.get("GrossWeight").equals("")?"0":(String) itemMap.get("GrossWeight")));
					item.setHsCode((String) itemMap.get("HSCode"));
					item.setProjectNo((String) itemMap.get("ProjectNo"));
					item.setUnit((String) itemMap.get("ItemUoM"));
					item.setTotalCbm(Double.parseDouble((String) itemMap.get("TotalCBM")==null|| itemMap.get("TotalCBM").equals("")?("0"):(String) itemMap.get("TotalCBM")));
					item.setTotalVolume(Double.parseDouble((String) itemMap.get("TotalCBM")==null|| itemMap.get("TotalCBM").equals("")?("0"):(String) itemMap.get("TotalCBM")));
					item.setPayCond((String) itemMap.get("PaymentCondition"));
					item.setUnitPrice(Double.parseDouble((String) itemMap.get("UnitPrice")==null|| itemMap.get("UnitPrice").equals("")?"0":(String) itemMap.get("UnitPrice")));
					item.setCurrency((String) itemMap.get("OrderCurrency"));
					item.setTotalPieces(Integer.parseInt((String) itemMap.get("TotalPcs")==null|| itemMap.get("TotalPcs").equals("")?"0":(String) itemMap.get("TotalPcs")));
					item.setPlant(loginDTO.getSalesOrgSelected().equals("1900")?"1100":loginDTO.getSalesOrgSelected());
					item.setVolumeWeight((Double.parseDouble((String) itemMap.get("TotalCBM")==null|| itemMap.get("TotalCBM").equals("")?("0"):(String) itemMap.get("TotalCBM")))*166.67);
					item.setReference5((String) itemMap.get("Line_ReferenceNo5"));
					//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					
				}
				if(null!=lstExtraObject)
				for (Iterator iterator3 = lstExtraObject.iterator(); iterator3.hasNext();) {
					Map<String, Object> extraMap = (Map<String, Object>) iterator3.next();
					
/*					item.setCommInvoice((String) extraMap.get("itemExtra_commInvoice"));
					item.setCartonSerNo((String) extraMap.get("itemExtra_cartonSerNo"));
					item.setReference1((String) extraMap.get("itemExtra_reference1"));
					//item.setQcDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_qcdate")));
					item.setQcDate("");					
					item.setDescription((String) extraMap.get("itemExtra_description"));
					item.setItemDescription((String) extraMap.get("itemExtra_itemDescription"));
					//item.setCommInvoiceDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_com_invoice_date")));
					item.setCommInvoiceDate("");					
					item.setPcsPerCarton((extraMap.get("itemExtra_pcsPerCarton")!=null && !(extraMap.get("itemExtra_pcsPerCarton").toString().equals("")))?Integer.parseInt((String) extraMap.get("itemExtra_pcsPerCarton")):new Integer(0));
					item.setReference2((String) extraMap.get("itemExtra_reference2"));
					//item.setReleaseDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_release_date")));
					item.setReleaseDate("");					
					item.setNetCost(Double.parseDouble((String) extraMap.get("itemExtra_netCost")==null|| extraMap.get("itemExtra_netCost").equals("")?"0":(String) extraMap.get("itemExtra_netCost")));
					item.setGrossWeightPerCarton(Double.parseDouble((String) extraMap.get("itemExtra_grossWeightPerCarton")==null|| extraMap.get("itemExtra_grossWeightPerCarton").equals("")?"0":(String) extraMap.get("itemExtra_grossWeightPerCarton")));
					item.setReference3((String) extraMap.get("itemExtra_reference3"));
					item.setShippingMark((String) extraMap.get("itemExtra_shippingMark"));
					item.setNetWeightPerCarton(Double.parseDouble((String) extraMap.get("itemExtra_netWeightPerCarton")==null|| extraMap.get("itemExtra_netWeightPerCarton").equals("")?"0":(String) extraMap.get("itemExtra_netWeightPerCarton")));
					item.setReference4((String) extraMap.get("itemExtra_reference4"));*/
					
					//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					item.setCommInvoice((String) extraMap.get("Line_CommInvNo"));
					item.setCartonSerNo((String) extraMap.get("Line_CartonSlNo"));
					
					//item.setQcDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_qcdate")));
					item.setQcDate("");
					
					item.setDescription((String) extraMap.get("Line_Description"));
					item.setItemDescription((String) extraMap.get("Line_ExtraDescription"));
					//item.setCommInvoiceDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_com_invoice_date")));
					item.setCommInvoiceDate("");
					
					item.setPcsPerCarton((extraMap.get("Line_Pcs/Carton")!=null && !(extraMap.get("Line_Pcs/Carton").toString().equals("")))?Integer.parseInt((String) extraMap.get("Line_Pcs/Carton")):new Integer(0));
					
					//item.setReleaseDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD((String) extraMap.get("itemExtra_release_date")));
					item.setReleaseDate("");
					
					item.setNetCost(Double.parseDouble((String) extraMap.get("Line_NetCost")==null|| extraMap.get("Line_NetCost").equals("")?"0":(String) extraMap.get("Line_NetCost")));
					item.setGrossWeightPerCarton(Double.parseDouble((String) extraMap.get("Line_GrossWt/Carton")==null|| extraMap.get("Line_GrossWt/Carton").equals("")?"0":(String) extraMap.get("Line_GrossWt/Carton")));
					
					item.setShippingMark((String) extraMap.get("Line_ShippingMarks"));
					item.setNetWeightPerCarton(Double.parseDouble((String) extraMap.get("Line_NetWt/Carton")==null|| extraMap.get("Line_NetWt/Carton").equals("")?"0":(String) extraMap.get("Line_NetWt/Carton")));
					
					//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					//item.setRefNo((String) extraMap.get("ReferenceNo1"));ReferenceNo1
					item.setReference1((String) extraMap.get("Line_ReferenceNo2"));
					//item.setReference2((String) extraMap.get("Line_ReferenceNo3"));
					item.setReference3((String) extraMap.get("Line_ReferenceNo3"));
					item.setReference4((String) extraMap.get("Line_ReferenceNo4"));
					/*item.setReference5((String) extraMap.get("Line_ReferenceNo5"));*/
					//-------------------------------------------------------------
				}
				if(item!=null)
				{
					item.setPosnerNumber(posNumber);
				lstOderItem.add(item);
				posNumber = posNumber + 10;
				}
			}
			DirectBookingParams directBookingParams = new DirectBookingParams();
			
			orderHeader.setCargoHandoverDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getCargoHandoverDate()));
			orderHeader.setComInvDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getComInvDate()));
			//orderHeader.setDeptDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getDeptDate()));
			orderHeader.setExpDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getExpDate()));
			orderHeader.setLcExpiryDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcExpiryDate()));
			orderHeader.setLcTtPoDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcTtPoDate()));
			//orderHeader.setLoadingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLoadingDate()));
			orderHeader.setShipperRefDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShipperRefDate()));
			//orderHeader.setShippingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShippingDate()));
			orderHeader.setPortLink(StringUtil.splitCodefromString(orderHeader.getPortLink(), "-"));
			orderHeader.setPortLoad(StringUtil.splitCodefromString(orderHeader.getPortLoad(), "-"));
			orderHeader.setPortOfDischarge(StringUtil.splitCodefromString(orderHeader.getPortOfDischarge(), "-"));
			orderHeader.setPlaceOfDelivery(StringUtil.splitCodefromString(orderHeader.getPlaceOfDelivery(), "-"));
			
			
			directBookingParams.setOrderHeader(orderHeader);
			directBookingParams.setOrderItemLst(lstOderItem);
			directBookingParams.setGoodsDesc(breakStringInLine(orderHeader.getDescription()));
			directBookingParams.setShippingMark(breakStringInLine(orderHeader.getShippingMark()));
		StringBuffer url=new StringBuffer(RestUtil.DIRECT_BOOKING);
		 directBookingJsonData=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), directBookingParams, DirectBookingJsonData.class);
		 Boolean isSuccess=false;
		 if((directBookingJsonData!=null && directBookingJsonData.getHblnumber()!=null) && (directBookingJsonData.getZsalesdocument()!=""))
			if(null!=lstPackingList && lstPackingList.size()>1)
			 isSuccess=savePackingList(lstPackingList,directBookingJsonData);
			else
				isSuccess=true;
		 if(isSuccess)
		 {
			 ZemailParams zemailParams=new ZemailParams();
			 zemailParams.setBuyer(orderHeader.getBuyer());
			 zemailParams.setHblNumber(directBookingJsonData.getHblnumber());
			
			 zemailParams.setCustomerNumber(loginDTO.getLoggedInUserName());
			 zemailParams.setDistChannel(loginDTO.getDisChnlSelected());
			 zemailParams.setDivision(loginDTO.getDivisionSelected());
			 zemailParams.setSalesOrg(loginDTO.getSalesOrgSelected());
			 ZemailJsonData zemailJsonData=restService.postForObject(RestUtil.prepareUrlForService(new StringBuffer(RestUtil.EMAIL)).toString(), zemailParams, ZemailJsonData.class);
			
			Properties properties=new Properties();
			InputStream inputStream=null;
			try {
				/*inputStream=new FileInputStream(new File("/WEB-INF/classes/email/emailconfig.properties"));*/
				inputStream=this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
				properties.load(inputStream);
				//properties.put(key, value);
				properties.put("mail.smtp.EnableSSL.enable","true");
	            
	            MailSSLSocketFactory sf=new MailSSLSocketFactory();
	            sf.setTrustAllHosts(true);
	            
	              properties.put("mail.smtp.ssl.socketFactory", sf);
				emailService=(IEmailService)serviceLocator.findService("emailService");
				emailService.creatDirectbookingMailParamsAndSendMail(zemailJsonData, properties.getProperty(CommonConstant.DIRECT_BOOKING_EMAIL_TEMPLATE_NAME), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_TO), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_SUBJECT));
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(""+e.getMessage());
			}finally
			{
				if(inputStream!=null)
				{
					try {
						inputStream.close();
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			}
			// prepareAndSendEmail(zemailJsonData);
			
		 }
		}
		
		
		
		 return directBookingJsonData;
	
	}
	
	
	public DirectBookingJsonData saveOrderDetailsAndPAckListByPo(LoginDTO loginDTO,OrderHeader orderHeader,BookingByPoDTO bookingByPoDTO)
	{
		List<OrderItem> lstOderItem = new ArrayList<OrderItem>();
		DirectBookingJsonData directBookingJsonData=null;
		
		if (null != bookingByPoDTO) {
			int posNumber = 10;
			List<Object> lstPackingList=null;
			for (Iterator iterator = bookingByPoDTO.getLstAppPurchaseOrder().iterator(); iterator.hasNext();) {
				ApprovedPurchaseOrder approvedPurchaseOrder=(ApprovedPurchaseOrder)iterator.next();
				OrderItem item=new OrderItem();

					item.setPoNumber(approvedPurchaseOrder.getVc_po_no());
					item.setProductCode(approvedPurchaseOrder.getVc_product_no());
					item.setCurtonQuantity(new Double(approvedPurchaseOrder.getVc_quan()).intValue());
					item.setCartonUnit(approvedPurchaseOrder.getVc_in_hcm());
					item.setStyleNo(approvedPurchaseOrder.getVc_style_no());
					item.setColor(approvedPurchaseOrder.getVc_color());
					item.setCartonWidth(approvedPurchaseOrder.getNu_width());
					item.setCartonHeight(approvedPurchaseOrder.getNu_hieght());
					item.setCartonLength(approvedPurchaseOrder.getNu_length());
					if(null!=approvedPurchaseOrder.getVc_nt_wt() && !approvedPurchaseOrder.getVc_nt_wt().trim().equalsIgnoreCase(""))
					item.setNetWeight(Double.parseDouble(approvedPurchaseOrder.getVc_nt_wt()));
					else
						item.setNetWeight(0.0);
					item.setMaterial(approvedPurchaseOrder.getVc_commodity());
					item.setRefNo(approvedPurchaseOrder.getVc_ref_field1());
					item.setSizeNo(approvedPurchaseOrder.getVc_size());
					item.setGrossWeight(approvedPurchaseOrder.getNu_hieght());
					item.setHsCode(approvedPurchaseOrder.getVc_hs_code());
					item.setProjectNo(approvedPurchaseOrder.getSap_quotation());
					item.setUnit(approvedPurchaseOrder.getVc_qua_uom());
					if(null!=approvedPurchaseOrder.getVc_cbm_sea() && !approvedPurchaseOrder.getVc_cbm_sea().trim().equalsIgnoreCase(""))
					item.setTotalCbm(Double.parseDouble(approvedPurchaseOrder.getVc_cbm_sea()));
					else
						item.setTotalCbm(0.0);
					item.setPayCond("");
					item.setUnitPrice(0.0);
					item.setCurrency("");
					item.setTotalPieces(new Double(approvedPurchaseOrder.getVc_tot_pcs()).intValue());
					item.setPlant(loginDTO.getSalesOrgSelected().equals("1900")?"1100":loginDTO.getSalesOrgSelected());
				
					item.setCommInvoice("");
					item.setCartonSerNo("");
					item.setArticleNo(approvedPurchaseOrder.getVc_article_no());
					item.setReference1(approvedPurchaseOrder.getVc_ref_field2());
					item.setQcDate(approvedPurchaseOrder.getVc_qc_dt());
					item.setDescription("");
					item.setItemDescription("");
					item.setCommInvoiceDate("");//approvedPurchaseOrder.getVc_rel_dt());
					item.setPcsPerCarton(new Double(approvedPurchaseOrder.getNu_no_pcs_ctns()).intValue());
					item.setReference2(approvedPurchaseOrder.getVc_ref_field3());
					item.setReleaseDate((approvedPurchaseOrder.getVc_rel_dt()));
					item.setNetCost(0.0);
					if(null!=approvedPurchaseOrder.getVc_gw_car() && !approvedPurchaseOrder.getVc_gw_car().trim().equalsIgnoreCase(""))
					item.setGrossWeightPerCarton(Double.parseDouble(approvedPurchaseOrder.getVc_gw_car()));
					else
						item.setGrossWeightPerCarton(0.0);
					item.setReference3(approvedPurchaseOrder.getVc_ref_field4());
					item.setShippingMark("");
					if(null!=approvedPurchaseOrder.getVc_nw_car() && !approvedPurchaseOrder.getVc_nw_car().trim().equalsIgnoreCase(""))
					item.setNetWeightPerCarton(Double.parseDouble(approvedPurchaseOrder.getVc_nw_car()));
					else
						item.setNetWeightPerCarton(0.0);	
					item.setReference4(approvedPurchaseOrder.getVc_ref_field5());
				
				if(item!=null)
				{
					item.setPosnerNumber(posNumber);
				lstOderItem.add(item);
				posNumber = posNumber + 10;
				}
			}
			DirectBookingParams directBookingParams = new DirectBookingParams();
			
			orderHeader.setCargoHandoverDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getCargoHandoverDate()));
			orderHeader.setComInvDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getComInvDate()));
			orderHeader.setDeptDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getDeptDate()));
			orderHeader.setExpDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getExpDate()));
			orderHeader.setLcExpiryDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcExpiryDate()));
			orderHeader.setLcTtPoDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcTtPoDate()));
			orderHeader.setLoadingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLoadingDate()));
			orderHeader.setShipperRefDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShipperRefDate()));
			orderHeader.setShippingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShippingDate()));
			orderHeader.setPortLink(StringUtil.splitCodefromString(orderHeader.getPortLink(), "-"));
			orderHeader.setPortLoad(StringUtil.splitCodefromString(orderHeader.getPortLoad(), "-"));
			orderHeader.setPortOfDischarge(StringUtil.splitCodefromString(orderHeader.getPortOfDischarge(), "-"));
			orderHeader.setPlaceOfDelivery(StringUtil.splitCodefromString(orderHeader.getPlaceOfDelivery(), "-"));
			
			
			directBookingParams.setOrderHeader(orderHeader);
			directBookingParams.setOrderItemLst(lstOderItem);
			directBookingParams.setGoodsDesc(breakStringInLine(orderHeader.getDescription()));
			directBookingParams.setShippingMark(breakStringInLine(orderHeader.getShippingMark()));
		StringBuffer url=new StringBuffer(RestUtil.DIRECT_BOOKING);
		 directBookingJsonData=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), directBookingParams, DirectBookingJsonData.class);
		 Boolean isSuccess=false;
		 if((directBookingJsonData!=null && directBookingJsonData.getHblnumber()!=null) && (directBookingJsonData.getZsalesdocument()!=""))
			if(null!=lstPackingList && lstPackingList.size()>1)
				isSuccess=savePackingList(lstPackingList,directBookingJsonData);
			else
				isSuccess=true;
		 if(isSuccess)
		 {
			 //update PO booking status
			
			 for (Iterator iterator = bookingByPoDTO.getLstAppPurchaseOrder().iterator(); iterator.hasNext();) {
					ApprovedPurchaseOrder approvedPurchaseOrder=(ApprovedPurchaseOrder)iterator.next();
					approvedPurchaseOrder.setBooking_status("Y");
					poMgmtDao.updateApprovedPoBookingStatusById(approvedPurchaseOrder);
					
			 }
			 ZemailParams zemailParams=new ZemailParams();
			 zemailParams.setBuyer(orderHeader.getBuyer());
			 zemailParams.setHblNumber(directBookingJsonData.getHblnumber());
			
			 zemailParams.setCustomerNumber(loginDTO.getPeAddress().getName());
			 zemailParams.setDistChannel(loginDTO.getDisChnlSelected());
			 zemailParams.setDivision(loginDTO.getDivisionSelected());
			 zemailParams.setSalesOrg(loginDTO.getSalesOrgSelected());
			 ZemailJsonData zemailJsonData=restService.postForObject(RestUtil.prepareUrlForService(new StringBuffer(RestUtil.EMAIL)).toString(), zemailParams, ZemailJsonData.class);
			/*prepareAndSendEmail(zemailJsonData);*/
			 Properties properties=new Properties();
				InputStream inputStream=null;
				try {
					/*inputStream=new FileInputStream(new File("/WEB-INF/classes/email/emailconfig.properties"));*/
					inputStream=this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
					properties.load(inputStream);
					emailService=(IEmailService)serviceLocator.findService("emailService");
					emailService.creatDirectbookingMailParamsAndSendMail(zemailJsonData, properties.getProperty(CommonConstant.DIRECT_BOOKING_EMAIL_TEMPLATE_NAME), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_TO), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_SUBJECT));
				} catch (Exception e) {
					logger.debug(""+e.getMessage());
				}finally
				{
					if(inputStream!=null)
					{
						try {
							inputStream.close();
						} catch (Exception e2) {
							// TODO: handle exception
						}
					}
				}
			
		 }
		}
		
		
		
		 return directBookingJsonData;
	
	}
	
	
	public void prepareAndSendEmail(ZemailJsonData zemailJsonData)
	{
		/*Map<String,Object> map=new HashMap<>();
		map.put("emailData", zemailJsonData);
		//map.put("directBookingJsonData", bookingJsonData);
		
		try {
			mailer.sendMail(bookingMailDtls, map);
		} catch (ResourceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	private  boolean savePackingList(List<Object> lstPackingList,DirectBookingJsonData directBookingJsonData)
	{
		try{
		List<String> sizes=new ArrayList<>();
		PackingListMdl packingList=new PackingListMdl();
		PackListColor packListColor=null;
		PackListSize packListSize=null;
		if(null!=lstPackingList && lstPackingList.size()>1){
		Map<String,Object> map=(Map<String,Object>)lstPackingList.get(0);
		
		if(map.get("size[1]_1")!=null && !map.get("size[1]_1").equals(""))
			sizes.add("size[1]_");
		if(map.get("size[2]_1")!=null && !map.get("size[2]_1").equals(""))
			sizes.add("size[2]_");
		if(map.get("size[3]_1")!=null && !map.get("size[3]_1").equals(""))
			sizes.add("size[3]_");
		if(map.get("size[4]_1")!=null && !map.get("size[4]_1").equals(""))
			sizes.add("size[4]_");
		if(map.get("size[5]_1")!=null && !map.get("size[5]_1").equals(""))
			sizes.add("size[5]_");
		if(map.get("size[6]_1")!=null && !map.get("size[6]_1").equals(""))
			sizes.add("size[6]_");
		if(map.get("size[7]_1")!=null && !map.get("size[7]_1").equals(""))
			sizes.add("size[7]_");
		if(map.get("size[8]_1")!=null && !map.get("size[8]_1").equals(""))
			sizes.add("size[8]_");
		if(map.get("size[9]_1")!=null && !map.get("size[9]_1").equals(""))
			sizes.add("size[9]_");
		if(map.get("size[10]_1")!=null && !map.get("size[10]_1").equals(""))
			sizes.add("size[10]_");
		if(map.get("size[11]_1")!=null && !map.get("size[11]_1").equals(""))
			sizes.add("size[11]_");
		if(map.get("size[12]_1")!=null && !map.get("size[12]_1").equals(""))
			sizes.add("size[12]_");
		if(map.get("size[13]_1")!=null && !map.get("size[13]_1").equals(""))
			sizes.add("size[13]_");
		List<PackListColor> lstPackListColor=new ArrayList<>(); 
		for (int i = 1; i < lstPackingList.size(); i++) {
			Map<String,Object> map1=(Map<String,Object>)lstPackingList.get(i);
			if(packingList.getHblNumber()==null || packingList.getHblNumber()=="")
			{
				packingList.setHblNumber(directBookingJsonData.getHblnumber());
			}
			if(packingList.getzSalesDocument()==null || packingList.getzSalesDocument()=="")
			{
				packingList.setzSalesDocument(directBookingJsonData.getZsalesdocument());
			}
			if(packingList.getCtnSrlNo()==null || packingList.getCtnSrlNo()=="")
			{
				if(map1.get("serial-no_"+(i+1))!=null)
					packingList.setCtnSrlNo((String)map1.get("serial-no_"+(i+1)));
			}
			if(packingList.getNoOfCtnls()==0l)
			{
				if(map1.get("no-of-cartoon_"+(i+1))!=null)
					packingList.setNoOfCtnls(Long.parseLong((String)map1.get("no-of-cartoon_"+(i+1))));
			}
			packListColor=new PackListColor();
			
			packListColor.setName((map1.get("color_"+(i+1))==null || map1.get("color_"+(i+1)).equals(""))?"":(String)map1.get("color_"+(i+1)));
			packListColor.setPcsPerCartoon((map1.get("pcs-crtn_"+(i+1))==null || map1.get("pcs-crtn_"+(i+1)).equals(""))?new Long(0):Long.parseLong((String)map1.get("pcs-crtn_"+(i+1))));  
			packListColor.setTotalPcs((map1.get("total-pcs_"+(i+1))==null || map1.get("total-pcs_"+(i+1)).equals(""))?new Long(0):Long.parseLong((String)map1.get("total-pcs_"+(i+1))));
			packListColor.setRemarks(map1.get("remarks_"+(i+1))==null?"":(String)map1.get("remarks_"+(i+1)));
			List<PackListSize> lstPckList=new ArrayList<>();
			for (Iterator iterator = sizes.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				packListSize=new PackListSize();
				packListSize.setName((String)map.get(string+(i)));
				packListSize.setNoOfPcs(Long.parseLong((String)map1.get(string+(i+1))));
				lstPckList.add(packListSize);
			}
			packListColor.setPackListSize(lstPckList);
			lstPackListColor.add(packListColor);
			packingList.setPackListColor(lstPackListColor);
		}
		}
		poMgmtDao.savePackListOrderBooking(packingList);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return true;
	}
	

	public List<ApprovedPurchaseOrder> getApprovedPONotBooked(String poNumber,String buyer,String fromDate,String toDate)
	{
		
		return poMgmtDao.getApprovedPoNotBookedList(poNumber==""?null:poNumber, buyer==""?null:buyer, (fromDate==null||fromDate=="")?null:fromDate, (toDate==null || toDate=="")?null:toDate);
		//return poMgmtDao.getApprovedPoNotBookedList(poNumber, buyer, fromDate, toDate);	
	}
	
	/*public List<ApprovedPurchaseOrder> getPOforBooking(String buyer,String poNumber,String salesOrg, String division,String shipper)
	{
		return poMgmtDao.getPOforBooking(buyer,poNumber,salesOrg,division,shipper);
	}*/
	public List<PurchaseOrdersModel> getPOforBooking(String buyer,String poNumber,String shipper)
	{
		return poMgmtDao.getPOforBooking(buyer,poNumber,shipper);
	}
	
	public ApprovedPurchaseOrder getAppPurchaseOrderById(Long Id)
	{
		return poMgmtDao.getApprovedItemById(Id);
	}

	@Override
	public List<StringBuffer> placeOfReciept(String searchString, String salesOrg) {
		
		logger.info(this.getClass().getName()+"placeOfReciept(): searchString="+searchString+" salesOrg="+salesOrg);
		StringBuffer url=new StringBuffer(RestUtil.PLACE_OF_RECIEPT);
		PortofRecieptParams portofRecieptParams=new PortofRecieptParams();
		portofRecieptParams.setSearchString(searchString);
		portofRecieptParams.setSalesOrg(salesOrg);
		StoreLocJsonData storeLocJsonData=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), portofRecieptParams, StoreLocJsonData.class);
		logger.info(this.getClass().getName()+"placeOfReciept(): Json Response="+storeLocJsonData);
		List<StringBuffer> lstPort=new ArrayList<>();
		StringBuffer data=null; 
		if(null!=storeLocJsonData){
		for (Iterator<StoreLocListBean> iterator = storeLocJsonData.getPorList().iterator(); iterator.hasNext();) {
			StoreLocListBean storeLocListBean =  iterator.next();
			data=new StringBuffer();
			data.append(storeLocListBean.getLgObe()).append("-").append(storeLocListBean.getLgOrt());
			lstPort.add(data);
		}
		}
		return lstPort;
		
	}
	
	public void saveBookingHeaderAsTemplate(SaveAsTemplateMdl bookingInfoMdl)
	{	
		poMgmtDao.saveBookingInfoAsTemplate(bookingInfoMdl); 
	}
	public List<SaveAsTemplateMdl> getTemplateInfoByCred(SaveAsTemplateMdl saveAsTemplateMdl)
	{
		
		return poMgmtDao.getTemplateInfoByLoginCred(saveAsTemplateMdl);
		//return poMgmtDao.getApprovedPoNotBookedList(poNumber, buyer, fromDate, toDate);	
	}
	public SaveAsTemplateMdl getTemplateInfoById(String templateId,String customer,String company,String operationType,String seaOrAir)
	{
		SaveAsTemplateMdl asTemplateMdl = null;
		asTemplateMdl = poMgmtDao.getTemplateInfoByTemplateId(templateId==""?null:templateId,customer==""?null:customer, company==""?null:company,operationType==""?null:operationType,seaOrAir==""?null:seaOrAir );
		return asTemplateMdl;	
	}
	public void updateTemplateInfo(String templateId,String tempInfo)
	{	
		poMgmtDao.updateTemplateInfo(templateId,tempInfo); 
	}

	
	@Override
	public List<MaterialsDto> getMaterialService(String param) {
		List<MaterialsDto> lstMaterialsDto=null;
		List<Materials> lstMaterials=poMgmtDao.getMaterialsDao();
		
		if(null!=lstMaterials && lstMaterials.size()>0)
		{
			lstMaterialsDto=new ArrayList<>();
			for (Iterator iterator = lstMaterials.iterator(); iterator.hasNext();) {
				Materials materials = (Materials) iterator.next();
				MaterialsDto materialsDto=new MaterialsDto();
				materialsDto.setLabel(materials.getMaterialDescription());
				materialsDto.setValue(materials.getMaterialCode());
				lstMaterialsDto.add(materialsDto);
			}
		}
		return lstMaterialsDto;
		
	}
	
	@Override
	public List<StorageLocation> getStorageLocationService(String param) {
		List<StorageLocation> lstStorageLocationDto=null;
		List<StorageLocation> lstStorageLocations=poMgmtDao.getStorageLocationsDao();
		
		if(null!=lstStorageLocations && lstStorageLocations.size()>0)
		{
			lstStorageLocationDto=new ArrayList<>();
			for (Iterator iterator = lstStorageLocations.iterator(); iterator.hasNext();) {
				StorageLocation storageLocation = (StorageLocation) iterator.next();
				StorageLocation storageLoc=new StorageLocation();
				storageLoc.setStorageId(storageLocation.getStorageDescription()+"-"+storageLocation.getStorageId());
				storageLoc.setStorageDescription(storageLocation.getStorageDescription());
				lstStorageLocationDto.add(storageLoc);
			}
		}
		return lstStorageLocationDto;
		
	}

	
	
	public List<BuyerDTO> getHMBuyers(String buyer,String disChnlSelected, String divisionSelected, String salesOrgSelected){
		List<BuyerDTO> buyerLst = poMgmtDao.getHMBuyers(buyer,disChnlSelected,divisionSelected,salesOrgSelected);
		return buyerLst;
	}

	@Override
	public List<WarehouseCode> getWHList(String countryCode) {
		List<WarehouseCode> whList = poMgmtDao.getWHList(countryCode);
		return whList;
	}
	
	
	public DirectBookingJsonData saveOrderDetailsByPo(LoginDTO loginDTO,OrderHeader orderHeader,List<PurchaseOrdersModel> itemList)
	{
		List<OrderItem> lstOderItem = new ArrayList<OrderItem>();
		DirectBookingJsonData directBookingJsonData=null;
		
		if (null != itemList) {
			int posNumber = 10;
			List<Object> lstPackingList=null;
			for (PurchaseOrdersModel purchaseOrder:itemList) {
				OrderItem item=new OrderItem();

					try {
						item.setPoNumber(purchaseOrder.getPo_no());
						item.setProductCode(purchaseOrder.getProductNo());
						item.setCurtonQuantity(Integer.parseInt(purchaseOrder.getCarton_quantity()));
						item.setCartonUnit(purchaseOrder.getCarton_unit());
						item.setStyleNo(purchaseOrder.getStyle_no());
						item.setColor(purchaseOrder.getColor());
						if(purchaseOrder.getCarton_width()!= null && !purchaseOrder.getCarton_width().trim().equalsIgnoreCase("")) {
						item.setCartonWidth(Double.parseDouble(purchaseOrder.getCarton_width()));
						}
						else {
							item.setCartonWidth(0.0);
						}
						if(purchaseOrder.getCarton_height()!= null && !purchaseOrder.getCarton_height().trim().equalsIgnoreCase("")) {
						item.setCartonHeight(Double.parseDouble(purchaseOrder.getCarton_height()));
						}
						else {
							item.setCartonHeight(0.0);
						}
						if(purchaseOrder.getCarton_length()!= null && !purchaseOrder.getCarton_length().trim().equalsIgnoreCase("")) {
						item.setCartonLength(Double.parseDouble(purchaseOrder.getCarton_length()));
						}
						else {
							item.setCartonLength(0.0);
						}
						if(null!=purchaseOrder.getTotal_nw() && !purchaseOrder.getTotal_nw().trim().equalsIgnoreCase(""))
						item.setNetWeight(Double.parseDouble(purchaseOrder.getTotal_nw()));
						else
							item.setNetWeight(0.0);
						item.setMaterial(purchaseOrder.getCommodity());
						//item.setRefNo(purchaseOrder.get);
						item.setSizeNo(purchaseOrder.getSize_no());
						item.setGrossWeight(Double.parseDouble(purchaseOrder.getTotal_gw()));
						item.setHsCode(purchaseOrder.getHs_code());
						item.setProjectNo(purchaseOrder.getProjectNo());
						item.setUnit(purchaseOrder.getUnit());
						if(null!=purchaseOrder.getTotal_cbm() && !purchaseOrder.getTotal_cbm().trim().equalsIgnoreCase(""))
						item.setTotalCbm(Double.parseDouble(purchaseOrder.getTotal_cbm()));
						else
							item.setTotalCbm(0.0);
						item.setPayCond("");
						item.setUnitPrice(0.0);
						item.setCurrency("");
						item.setTotalPieces(Integer.parseInt(purchaseOrder.getTotal_pieces()));
						item.setPlant(loginDTO.getSalesOrgSelected().equals("1900")?"1100":loginDTO.getSalesOrgSelected());

						item.setCommInvoice(purchaseOrder.getComm_inv());
						item.setCartonSerNo("");
						item.setArticleNo(purchaseOrder.getArticle_no());
						item.setReference1(purchaseOrder.getReference_1());
						item.setQcDate(purchaseOrder.getQcDate());
						item.setDescription("");
						item.setItemDescription("");
						item.setCommInvoiceDate("");//approvedPurchaseOrder.getVc_rel_dt());
						/*item.setPcsPerCarton(Integer.parseInt(purchaseOrder.getPcsPerCarton()));*/
						/*if(null!=purchaseOrder.getPcsPerCarton() && !purchaseOrder.getPcsPerCarton().trim().equalsIgnoreCase(""))
							item.setPcsPerCarton(Integer.parseInt(purchaseOrder.getPcsPerCarton()));
							else
								item.setPcsPerCarton(0);*/
						item.setPcsPerCarton(0);
						item.setReference2(purchaseOrder.getReference_3());
						item.setReleaseDate("");
						item.setNetCost(0.0);
						if(null!=purchaseOrder.getGwPerCarton() && !purchaseOrder.getGwPerCarton().trim().equalsIgnoreCase(""))
						item.setGrossWeightPerCarton(Double.parseDouble(purchaseOrder.getGwPerCarton()));
						else
							item.setGrossWeightPerCarton(0.0);
						item.setReference3(purchaseOrder.getReference_4());
						item.setShippingMark("");
						if(null!=purchaseOrder.getNwPerCarton() && !purchaseOrder.getNwPerCarton().trim().equalsIgnoreCase(""))
						item.setNetWeightPerCarton(Double.parseDouble(purchaseOrder.getNwPerCarton()));
						else
							item.setNetWeightPerCarton(0.0);	
						item.setReference4(purchaseOrder.getDepartment());
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				if(item!=null)
				{
					item.setPosnerNumber(posNumber);
				lstOderItem.add(item);
				posNumber = posNumber + 10;
				}
			}
			DirectBookingParams directBookingParams = new DirectBookingParams();
			
			orderHeader.setCargoHandoverDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getCargoHandoverDate()));
			orderHeader.setComInvDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getComInvDate()));
			orderHeader.setDeptDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getDeptDate()));
			orderHeader.setExpDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getExpDate()));
			orderHeader.setLcExpiryDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcExpiryDate()));
			orderHeader.setLcTtPoDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcTtPoDate()));
			orderHeader.setLoadingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLoadingDate()));
			orderHeader.setShipperRefDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShipperRefDate()));
			orderHeader.setShippingDate(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShippingDate()));
			orderHeader.setPortLink(StringUtil.splitCodefromString(orderHeader.getPortLink(), "-"));
			orderHeader.setPortLoad(StringUtil.splitCodefromString(orderHeader.getPortLoad(), "-"));
			orderHeader.setPortOfDischarge(StringUtil.splitCodefromString(orderHeader.getPortOfDischarge(), "-"));
			orderHeader.setPlaceOfDelivery(StringUtil.splitCodefromString(orderHeader.getPlaceOfDelivery(), "-"));
			
			
			directBookingParams.setOrderHeader(orderHeader);
			directBookingParams.setOrderItemLst(lstOderItem);
			directBookingParams.setGoodsDesc(breakStringInLine(orderHeader.getDescription()));
			directBookingParams.setShippingMark(breakStringInLine(orderHeader.getShippingMark()));
		StringBuffer url=new StringBuffer(RestUtil.DIRECT_BOOKING);
		 directBookingJsonData=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), directBookingParams, DirectBookingJsonData.class);
		 Boolean isSuccess=false;
		 if((directBookingJsonData!=null && directBookingJsonData.getHblnumber()!=null) && (directBookingJsonData.getZsalesdocument()!=""))
			if(null!=lstPackingList && lstPackingList.size()>1)
				isSuccess=savePackingList(lstPackingList,directBookingJsonData);
			else
				isSuccess=true;
		 if(isSuccess)
		 {
			 directBookingJsonData.setFvsl(itemList.get(0).getFvsl());
			 directBookingJsonData.setMvsl1(itemList.get(0).getMvsl1());
			 directBookingJsonData.setMvsl2(itemList.get(0).getMvsl2());
			 directBookingJsonData.setMvsl3(itemList.get(0).getMvsl3());
			 directBookingJsonData.setVoyage1(itemList.get(0).getVoyage1());
			 directBookingJsonData.setVoyage2(itemList.get(0).getVoyage2());
			 directBookingJsonData.setVoyage3(itemList.get(0).getVoyage3());
			 directBookingJsonData.setEtd(itemList.get(0).getEtd());
			 directBookingJsonData.setEta(itemList.get(0).getEta());
			 directBookingJsonData.setAtd(itemList.get(0).getAtd());
			 directBookingJsonData.setAta(itemList.get(0).getAta());
			 directBookingJsonData.setTranshipment1eta(itemList.get(0).getTranshipment1eta());
			 directBookingJsonData.setTranshipment2eta(itemList.get(0).getTranshipment2eta());
			 directBookingJsonData.setTranshipment1etd(itemList.get(0).getTranshipment1etd());
			 directBookingJsonData.setTranshipment2etd(itemList.get(0).getTranshipment2etd());
			 directBookingJsonData.setDivision(loginDTO.getDivisionSelected());
			 //update PO booking status
			
/*			 for (Iterator iterator = bookingByPoDTO.getLstAppPurchaseOrder().iterator(); iterator.hasNext();) {
					ApprovedPurchaseOrder approvedPurchaseOrder=(ApprovedPurchaseOrder)iterator.next();
					approvedPurchaseOrder.setBooking_status("Y");
					poMgmtDao.updateApprovedPoBookingStatusById(approvedPurchaseOrder);
					
			 }*/
			 for(PurchaseOrdersModel po:itemList) {
				 
				 poUploadMapper.updateFlag(po.getApp_id(),new Date());
			 }
			 ZemailParams zemailParams=new ZemailParams();
			 zemailParams.setBuyer(orderHeader.getBuyer());
			 zemailParams.setHblNumber(directBookingJsonData.getHblnumber());
			
			 zemailParams.setCustomerNumber(loginDTO.getPeAddress().getName());
			 zemailParams.setDistChannel(loginDTO.getDisChnlSelected());
			 zemailParams.setDivision(loginDTO.getDivisionSelected());
			 zemailParams.setSalesOrg(loginDTO.getSalesOrgSelected());
			 ZemailJsonData zemailJsonData=restService.postForObject(RestUtil.prepareUrlForService(new StringBuffer(RestUtil.EMAIL)).toString(), zemailParams, ZemailJsonData.class);
			/*prepareAndSendEmail(zemailJsonData);*/
			 Properties properties=new Properties();
				InputStream inputStream=null;
				try {
					/*inputStream=new FileInputStream(new File("/WEB-INF/classes/email/emailconfig.properties"));*/
					inputStream=this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
					properties.load(inputStream);
					emailService=(IEmailService)serviceLocator.findService("emailService");
					emailService.creatDirectbookingMailParamsAndSendMail(zemailJsonData, properties.getProperty(CommonConstant.DIRECT_BOOKING_EMAIL_TEMPLATE_NAME), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_TO), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM), properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_SUBJECT));
				} catch (Exception e) {
					logger.debug(""+e.getMessage());
				}finally
				{
					if(inputStream!=null)
					{
						try {
							inputStream.close();
						} catch (Exception e2) {
							// TODO: handle exception
						}
					}
				}
			
		 }
		}
		
		
		
		 return directBookingJsonData;
	
	}
	
	public String breakStringInLine(String line) {
		
		String finalString ="";
		line = line.replaceAll("[\\n\\r\\t]+", " ");
		String [] splittedLine = line.split(" ");
		int totalWordLengthCount = 0;
		int totalLineCount = 0;
		
		for(int count = 0; count<splittedLine.length; count++) {
			
			
			String word = splittedLine[count];
			int wordLength = word.length();
			totalWordLengthCount = totalWordLengthCount + wordLength;
			
			if(totalWordLengthCount < 34) {
				finalString = finalString+" "+ word;
				totalWordLengthCount++;// Because of added space;
			}else {
				finalString = finalString+"\n"+ word;
				totalWordLengthCount = 0;
				totalWordLengthCount = wordLength + 1; // Because of one added  new line;
				//totalLineCount ++;
			}
			
//			if(totalLineCount>6) {
//				break;
//			}
			
		}
		
		
		return finalString.trim();
	}
	
	
}