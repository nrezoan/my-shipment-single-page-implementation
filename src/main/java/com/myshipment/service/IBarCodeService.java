package com.myshipment.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.google.zxing.WriterException;
import com.myshipment.model.BarCodeImage;

public interface IBarCodeService {
	
	public BarCodeImage generateCode128toImage(String codeToGenerate,String pathOfImage,String imageType,int width,int height) throws WriterException,FileNotFoundException, IOException;
}
