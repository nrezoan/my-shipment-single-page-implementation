package com.myshipment.service;

import java.util.List;
import java.util.Map;

import com.myshipment.dto.ApprovedPoDTO;
import com.myshipment.dto.PurchaseOrderDTO;

public interface IPoMgmtService {
	public List<PurchaseOrderDTO> getPoDashBoardData();
	public int approvePo(List<Map<String,Object>> listOfMap);
	public PurchaseOrderDTO loadPoLineItemById(Long id);
	public ApprovedPoDTO loadApprovedPoLineItemById(Long poId);
	public List<PurchaseOrderDTO> searchPo(String poNumber,String fromDate,String toDate,String supplierCode,String buyerCode);
	public List<ApprovedPoDTO> searchApprovedPo(String poNumber, String fromDate, String toDate, String supplierCode,String buyerCode);
	public boolean assignCarrier(List<Map<String,Object>> assCarrLst);
	

}
