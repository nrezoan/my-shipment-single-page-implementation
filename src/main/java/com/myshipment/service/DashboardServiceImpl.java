package com.myshipment.service;

import java.net.ConnectException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BLStatusDetailsParamBuyer;
import com.myshipment.model.BlStatusDetailsParam;
import com.myshipment.model.BuyerWiseCBMdetails;
import com.myshipment.model.BuyerWiseGWTdetails;
import com.myshipment.model.BuyerWiseShipDetails;
import com.myshipment.model.BuyerWiseShipDetailsbyPOD;
import com.myshipment.model.BuywiseShipDetailsbyBLstatus;
import com.myshipment.model.DashboardBuyerParams;
import com.myshipment.model.DashboardShipperParams;
import com.myshipment.model.ItHeaderBean;
import com.myshipment.model.LastNShipmentParams;
import com.myshipment.model.LastNShipmentsJsonOutputData;
import com.myshipment.model.SODashboardBuyerParams;
import com.myshipment.model.SODashboardShipperParams;
import com.myshipment.model.SOWiseShipDetailsBuyerJson;
import com.myshipment.model.SOWiseShipmentSummaryJson;
import com.myshipment.model.SalesOrgWiseShipmentJson;
import com.myshipment.model.ShipDetailsBuyerParams;
import com.myshipment.model.ShipDetailsByPodParams;
import com.myshipment.model.ShipDetailsByPodParamsBuyer;
import com.myshipment.model.ShipDetailsShipperParams;
import com.myshipment.model.ShipmentDetailsBuyerJson;
import com.myshipment.model.ShipmentDetailsShipperJson;
import com.myshipment.model.SowiseShipDetailsBuyer;
import com.myshipment.model.SowiseShipDetailsShipper;
import com.myshipment.model.SuppWiseShipSummaryJson;
import com.myshipment.model.SupplierWiseCBMDetailJson;
import com.myshipment.model.SupplierWiseGWTDetailJson;
import com.myshipment.model.SupplierWiseShipmentDetailJson;
import com.myshipment.model.SuppwiseShipDetailsbyBLstatus;
import com.myshipment.model.TopFiveShipBuyerJson;
import com.myshipment.model.TopFiveShipment;
import com.myshipment.util.DateUtil;
import com.myshipment.util.ErrorCode;
import com.myshipment.util.MyShipApplicationException;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

/**
 * @author Gufranur Rahman
 *
 */
@Service
public class DashboardServiceImpl implements IDashboardService {
	// @Autowired
	private Logger logger = Logger.getLogger(DashboardServiceImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public SOWiseShipmentSummaryJson soWiseShipSummaryForShipperService(String fromDate,String toDate,LoginDTO loginDto ) throws MyShipApplicationException {

		logger.debug(this.getClass().getName() + " soWiseShipSummaryForShipperService()  start-- ");
		
		SOWiseShipmentSummaryJson soWiseShipmentSummaryJson = null;
		StringBuffer url=null;
		try {
		
		
			url=new StringBuffer(RestUtil.SO_WISE_SHIP_SUMMARY_SHIPPER);
			DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
			dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
			dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
			dashboardShipperParams.setShipperNo(loginDto.getLoggedInUserName());
			dashboardShipperParams.setDistChannel(loginDto.getDisChnlSelected());
			dashboardShipperParams.setDivision(loginDto.getDivisionSelected());
			logger.debug(this.getClass().getName() + " soWiseShipSummaryForShipperService()  Calling Service-- Parameters:"+dashboardShipperParams);
			soWiseShipmentSummaryJson = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardShipperParams,
					SOWiseShipmentSummaryJson.class);
			logger.debug(this.getClass().getName() + " soWiseShipSummaryForShipperService()  Service Called-- Parameters:"+dashboardShipperParams);
		} catch (Exception e) {
			if (e instanceof ConnectException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
			if (e instanceof ResourceAccessException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
		}
		
		return soWiseShipmentSummaryJson;
	}

	@Override
	public TopFiveShipment topFiveShipmentService(String fromDate, String toDate, LoginDTO loginDTO) throws MyShipApplicationException {
			logger.debug(this.getClass().getName() + " topFiveShipmentService()  start-- ");
		
		TopFiveShipment topFiveShipment = null;
		StringBuffer url=null;
		try {
		
		
			url=new StringBuffer(RestUtil.TOP_FIVE_SHIPMENT_SHIPPER);
			DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
			dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
			dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
			dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
			dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
			dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
			logger.debug(this.getClass().getName() + " topFiveShipmentService()  Calling Service-- "+RestUtil.TOP_FIVE_SHIPMENT_SHIPPER+" : Parameters: "+dashboardShipperParams);
			topFiveShipment = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardShipperParams,
					TopFiveShipment.class);
			logger.debug(this.getClass().getName() + " topFiveShipmentService()  Called Service-- Parameters: "+dashboardShipperParams);
		} catch (Exception e) {
			if (e instanceof ConnectException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
			if (e instanceof ResourceAccessException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
		}
		
		return topFiveShipment;
	}

	@Override
	public SuppwiseShipDetailsbyBLstatus blStatusBuyer(String fromDate, String toDate, String blStatus,LoginDTO loginDTO	) {
		BLStatusDetailsParamBuyer blStatusDetailsParamBuyer=new BLStatusDetailsParamBuyer();
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		blStatusDetailsParamBuyer.setDashboardBuyerParams(dashboardBuyerParams);
		if(blStatus.equalsIgnoreCase("Delivered"))
		blStatusDetailsParamBuyer.setStatus("Released");
		else
			blStatusDetailsParamBuyer.setStatus("Pending");
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_SHIP_DETAILS_BLSTATUS);
		SuppwiseShipDetailsbyBLstatus suppwiseShipDetailsbyBLstatus=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), blStatusDetailsParamBuyer, SuppwiseShipDetailsbyBLstatus.class);
		if(null!=suppwiseShipDetailsbyBLstatus)
		{
			for (Iterator iterator = suppwiseShipDetailsbyBLstatus.getLstSalesOrgWiseShipmentJson().iterator(); iterator.hasNext();) {
				SalesOrgWiseShipmentJson salesOrgWiseShipmentJson = (SalesOrgWiseShipmentJson) iterator.next();
				salesOrgWiseShipmentJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBldate()));
				salesOrgWiseShipmentJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBookingDate()));
				salesOrgWiseShipmentJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getGrDate()));
				salesOrgWiseShipmentJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getShipmentDate()));
				
			}
		}
		
		return suppwiseShipDetailsbyBLstatus;
	}
	
	
	@Override
	public SuppWiseShipSummaryJson suppWiseShipSummaryForBuyerService(String fromDate,String toDate,LoginDTO loginDto ) throws MyShipApplicationException {

		logger.debug(this.getClass().getName() + " soWiseShipSummaryForShipperService()  start-- ");
		
		SuppWiseShipSummaryJson soWiseShipmentSummaryJson = null;
		StringBuffer url=null;
		try {
		
		
			url=new StringBuffer(RestUtil.SUPP_WISE_SHIP_SUMMARY_BUYER);
			DashboardBuyerParams dashboardBuyerParams =new DashboardBuyerParams();
			dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
			dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
			dashboardBuyerParams.setBuyerNo(loginDto.getLoggedInUserName());
			dashboardBuyerParams.setDistChannel(loginDto.getDisChnlSelected());
			dashboardBuyerParams.setDivision(loginDto.getDivisionSelected());
			soWiseShipmentSummaryJson = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardBuyerParams,
					SuppWiseShipSummaryJson.class);
		
		} catch (Exception e) {
			if (e instanceof ConnectException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
			if (e instanceof ResourceAccessException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
		}
		
		return soWiseShipmentSummaryJson;
	}

	@Override
	public TopFiveShipBuyerJson topFiveShipmentServiceBuyer(String fromDate, String toDate, LoginDTO loginDTO) throws MyShipApplicationException {
			logger.debug(this.getClass().getName() + " topFiveShipmentService()  start-- ");
		
			TopFiveShipBuyerJson topFiveShipBuyerJson = null;
		StringBuffer url=null;
		try {
		
		
			url=new StringBuffer(RestUtil.TOP_FIVE_SHIPMENT_BUYER);
			DashboardBuyerParams dashboardBuyerParams =new DashboardBuyerParams();
			dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
			dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
			dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
			dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
			dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
			topFiveShipBuyerJson = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardBuyerParams,
					TopFiveShipBuyerJson.class);
		
		} catch (Exception e) {
			if (e instanceof ConnectException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
			if (e instanceof ResourceAccessException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
		}
		
		return topFiveShipBuyerJson;
	}

	@Override
	public BuywiseShipDetailsbyBLstatus blStatusShipper(String fromDate, String toDate, String blStatus,
			LoginDTO loginDTO) {
		BlStatusDetailsParam blStatusDetailsParam =new BlStatusDetailsParam();
		DashboardShipperParams dashboardShipperParams =new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		blStatusDetailsParam.setDashboardShipperParams(dashboardShipperParams);
		blStatusDetailsParam.setStatus(blStatus);
		StringBuffer url=new StringBuffer(RestUtil.BUY_WISE_SHIP_DETAILS_BY_BLSTATUS);
		BuywiseShipDetailsbyBLstatus buywiseShipDetailsbyBLstatus =restService.postForObject(RestUtil.prepareUrlForService(url).toString(), blStatusDetailsParam, BuywiseShipDetailsbyBLstatus.class);
		if(null!=buywiseShipDetailsbyBLstatus)
		{
			for (Iterator iterator = buywiseShipDetailsbyBLstatus.getLstSOWiseShipDetailsBuyerJson().iterator(); iterator.hasNext();) {
				SOWiseShipDetailsBuyerJson  soWiseShipDetailsBuyerJson= (SOWiseShipDetailsBuyerJson) iterator.next();
				soWiseShipDetailsBuyerJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getBldate()));
				soWiseShipDetailsBuyerJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getBookingDate()));
				soWiseShipDetailsBuyerJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getGrDate()));
				soWiseShipDetailsBuyerJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getShipmentDate()));
				
			}
		}
		
		return buywiseShipDetailsbyBLstatus;
	}

	@Override
	public BuyerWiseShipDetails totalShipDetShipperService(String fromDate, String toDate,
			LoginDTO loginDTO) {

		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		StringBuffer url=new StringBuffer(RestUtil.BUYER_WISE_SHIP_DETAILS);
		BuyerWiseShipDetails buyerWiseShipDetails=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardShipperParams, BuyerWiseShipDetails.class);
		
		return buyerWiseShipDetails;
	}

	@Override
	public BuyerWiseCBMdetails totCbmDetailsShipperService(String fromDate, String toDate, LoginDTO loginDTO) {
		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		StringBuffer url=new StringBuffer(RestUtil.BUYER_WISE_CBM_DETAILS);
		BuyerWiseCBMdetails buyerWiseShipDetails=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardShipperParams, BuyerWiseCBMdetails.class);
		
		return buyerWiseShipDetails;
	}

	@Override
	public BuyerWiseGWTdetails totGwtDetailsShipperService(String fromDate, String toDate, LoginDTO loginDTO) {
		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		StringBuffer url=new StringBuffer(RestUtil.BUYER_WISE_GWT_DETAILS);
		BuyerWiseGWTdetails buyerWiseShipDetails=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardShipperParams, BuyerWiseGWTdetails.class);
		
		return buyerWiseShipDetails;
	}

	@Override
	public SupplierWiseShipmentDetailJson totalShipmentDetBuyerService(String fromDate, String toDate,LoginDTO loginDTO)
	{
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_SHIP_DETAILS);
		SupplierWiseShipmentDetailJson supplierWiseShipmentDetailJson=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardBuyerParams, SupplierWiseShipmentDetailJson.class);
		return supplierWiseShipmentDetailJson;
	}

	@Override
	public SupplierWiseCBMDetailJson totalCBMDetBuyerService(String fromDate, String toDate, LoginDTO loginDTO) {
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_CBM_DETAILS);
		SupplierWiseCBMDetailJson supplierWiseCBMDetailJson=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardBuyerParams, SupplierWiseCBMDetailJson.class);
		return supplierWiseCBMDetailJson;
	}

	@Override
	public SupplierWiseGWTDetailJson totalGWTDetBuyerService(String fromDate, String toDate, LoginDTO loginDTO) {
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_GWT_DETAILS);
		SupplierWiseGWTDetailJson supplierWiseGWTDetailJson =restService.postForObject(RestUtil.prepareUrlForService(url).toString(), dashboardBuyerParams, SupplierWiseGWTDetailJson.class);
		return supplierWiseGWTDetailJson;
	}

	@Override
	public SowiseShipDetailsShipper sowiseShipDetailsShipperService(String fromDate, String toDate, String salesOrg,
			LoginDTO loginDTO) {
		SODashboardShipperParams soDashboardShipperParams =new SODashboardShipperParams();
		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		soDashboardShipperParams.setDashboardShipperParams(dashboardShipperParams);
		soDashboardShipperParams.setSalesOrg(salesOrg);
		StringBuffer url=new StringBuffer(RestUtil.SO_WISE_SHIP_DETAILS_SHIPPER);
		SowiseShipDetailsShipper sowiseShipDetailsShipper=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), soDashboardShipperParams, SowiseShipDetailsShipper.class);
		return sowiseShipDetailsShipper;
	}

	@Override
	public BuyerWiseShipDetailsbyPOD buyerWiseShipDetailsByPodService(String fromDate, String toDate, String pod,
			LoginDTO loginDTO) {
		ShipDetailsByPodParams  shipDetailsByPodParams=new ShipDetailsByPodParams();
		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		shipDetailsByPodParams.setDashboardShipperParams(dashboardShipperParams);
		shipDetailsByPodParams.setPod(pod);
		StringBuffer url=new StringBuffer(RestUtil.BUYER_WISE_SHIP_DETAILS_BY_POD);
		BuyerWiseShipDetailsbyPOD buyerWiseShipDetailsbyPOD=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), shipDetailsByPodParams, BuyerWiseShipDetailsbyPOD.class);
		return buyerWiseShipDetailsbyPOD;
	}

	@Override
	public SowiseShipDetailsBuyer soWiseShipDetailsBuyerService(String fromDate, String toDate, String shipperNo,
			LoginDTO loginDTO) {
		SODashboardBuyerParams soDashboardBuyerParams=new SODashboardBuyerParams();
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		soDashboardBuyerParams.setDashboardBuyerParams(dashboardBuyerParams);
		soDashboardBuyerParams.setShipperNo(shipperNo);
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_SHIP_DETAILS_BUYER);
		SowiseShipDetailsBuyer sowiseShipDetailsBuyer=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), soDashboardBuyerParams, SowiseShipDetailsBuyer.class);
		for (Iterator iterator = sowiseShipDetailsBuyer.getLstSalesOrgWiseShipmentJson().iterator(); iterator.hasNext();) {
			SalesOrgWiseShipmentJson salesOrgWiseShipmentJson = (SalesOrgWiseShipmentJson) iterator.next();
			salesOrgWiseShipmentJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBldate()));
			salesOrgWiseShipmentJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBookingDate()));
			salesOrgWiseShipmentJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getGrDate()));
			salesOrgWiseShipmentJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getShipmentDate()));
			
		}
		
		
		return sowiseShipDetailsBuyer;
	}

	@Override
	public SuppwiseShipDetailsbyBLstatus suppWiseShipDetailsByPOOService(String fromDate, String toDate, String poo,
			LoginDTO loginDTO) {
		ShipDetailsByPodParamsBuyer shipDetailsByPodParamsBuyer=new ShipDetailsByPodParamsBuyer();
		shipDetailsByPodParamsBuyer.setPoo(poo);
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(fromDate));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(toDate));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		shipDetailsByPodParamsBuyer.setDashboardBuyerParams(dashboardBuyerParams);
		StringBuffer url=new StringBuffer(RestUtil.SUPP_WISE_SHIP_DETAILS_BY_POO);
		SuppwiseShipDetailsbyBLstatus suppwiseShipDetailsbyBLstatus=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), shipDetailsByPodParamsBuyer, SuppwiseShipDetailsbyBLstatus.class);
		for (Iterator iterator = suppwiseShipDetailsbyBLstatus.getLstSalesOrgWiseShipmentJson().iterator(); iterator.hasNext();) {
			SalesOrgWiseShipmentJson salesOrgWiseShipmentJson = (SalesOrgWiseShipmentJson) iterator.next();
			salesOrgWiseShipmentJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBldate()));
			salesOrgWiseShipmentJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBookingDate()));
			salesOrgWiseShipmentJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getGrDate()));
			salesOrgWiseShipmentJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getShipmentDate()));
		}
		return suppwiseShipDetailsbyBLstatus;
	}

	@Override
	public ShipmentDetailsShipperJson shipDetailsShipperService( String buyerNo,
			LoginDTO loginDTO) {
		
		ShipDetailsShipperParams shipDetailsShipperParams=new ShipDetailsShipperParams();
		DashboardShipperParams dashboardShipperParams=new DashboardShipperParams();
		dashboardShipperParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardFromDate()));
		dashboardShipperParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardToDate()));
		dashboardShipperParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardShipperParams.setDivision(loginDTO.getDivisionSelected());
		dashboardShipperParams.setShipperNo(loginDTO.getLoggedInUserName());
		shipDetailsShipperParams.setBuyerNo(buyerNo);
		shipDetailsShipperParams.setDashboardShipperParams(dashboardShipperParams);
		StringBuffer url=new StringBuffer(RestUtil.SHIP_DET_SHIPPER);
		ShipmentDetailsShipperJson shipmentDetailsShipperJson=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), shipDetailsShipperParams, ShipmentDetailsShipperJson.class);
		for (Iterator iterator = shipmentDetailsShipperJson.getLstSOWiseShipDetailsBuyerJson().iterator(); iterator.hasNext();) {
			SOWiseShipDetailsBuyerJson soWiseShipDetailsBuyerJson = (SOWiseShipDetailsBuyerJson) iterator.next();
			soWiseShipDetailsBuyerJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getBldate()));
			soWiseShipDetailsBuyerJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getBookingDate()));
			soWiseShipDetailsBuyerJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getGrDate()));
			soWiseShipDetailsBuyerJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(soWiseShipDetailsBuyerJson.getShipmentDate()));
			
		}
		return shipmentDetailsShipperJson;
	}

	@Override
	public ShipmentDetailsBuyerJson shipDetailsBuyerService(String shipperNo, LoginDTO loginDTO) {
		ShipDetailsBuyerParams shipDetailsBuyerParams=new ShipDetailsBuyerParams();
		shipDetailsBuyerParams.setShipperNo(shipperNo);
		DashboardBuyerParams dashboardBuyerParams=new DashboardBuyerParams();
		dashboardBuyerParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardFromDate()));
		dashboardBuyerParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardToDate()));
		dashboardBuyerParams.setBuyerNo(loginDTO.getLoggedInUserName());
		dashboardBuyerParams.setDistChannel(loginDTO.getDisChnlSelected());
		dashboardBuyerParams.setDivision(loginDTO.getDivisionSelected());
		shipDetailsBuyerParams.setDashboardBuyerParams(dashboardBuyerParams);
		StringBuffer url=new StringBuffer(RestUtil.SHIP_DET_BUYER);
		ShipmentDetailsBuyerJson shipmentDetailsBuyerJson=restService.postForObject(RestUtil.prepareUrlForService(url).toString(), shipDetailsBuyerParams, ShipmentDetailsBuyerJson.class);
		for (Iterator iterator = shipmentDetailsBuyerJson.getLstSalesOrgWiseShipmentJson().iterator(); iterator.hasNext();) {
			SalesOrgWiseShipmentJson salesOrgWiseShipmentJson = (SalesOrgWiseShipmentJson) iterator.next();
			salesOrgWiseShipmentJson.setBldate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBldate()));
			salesOrgWiseShipmentJson.setBookingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getBookingDate()));
			salesOrgWiseShipmentJson.setGrDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getGrDate()));
			salesOrgWiseShipmentJson.setShipmentDate(DateUtil.YYYYMMDDToDD_MM_YYYY(salesOrgWiseShipmentJson.getShipmentDate()));
		}
		return shipmentDetailsBuyerJson;
	}

	@Override
	public LastNShipmentsJsonOutputData getRecentShipmentShipper(LoginDTO loginDTO,int noOfRecords) {
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.LAST_N_SHIPMENT);
		LastNShipmentParams lastNShipmentParams=new LastNShipmentParams();
		lastNShipmentParams.setKunnr(loginDTO.getLoggedInUserName());
		lastNShipmentParams.setSpart(loginDTO.getDivisionSelected());
		lastNShipmentParams.setVkorg(loginDTO.getSalesOrgSelected());
		lastNShipmentParams.setVtweg(loginDTO.getDisChnlSelected());
		lastNShipmentParams.setNumber(noOfRecords);
		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), lastNShipmentParams, LastNShipmentsJsonOutputData.class);
		List<ItHeaderBean> lstItHeaderBean=lastNShipmentsJsonOutputData.getItHeaderBeanLst();
		for (ItHeaderBean itHeaderBean : lstItHeaderBean) {
			itHeaderBean.setBookingdate(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getBookingdate()));
			itHeaderBean.setFvEtaPort(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getFvEtaPort()));
			itHeaderBean.setFvEtdPort(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getFvEtdPort()));
			itHeaderBean.setHblDate(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getHblDate()));
			itHeaderBean.setMvEtaaPort(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getMvEtaaPort()));
			itHeaderBean.setMvEtdPort(DateUtil.YYYYMMDDToDD_MM_YYYY(itHeaderBean.getMvEtdPort()));
		}
		lastNShipmentsJsonOutputData.setItHeaderBeanLst(lstItHeaderBean);
		return lastNShipmentsJsonOutputData;
	}

	

}
