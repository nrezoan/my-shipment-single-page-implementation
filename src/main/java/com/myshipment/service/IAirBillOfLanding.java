package com.myshipment.service;

import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.RequestParams;




public interface IAirBillOfLanding {

	public BillOfLandingDetails getAirBillLandingDetail(RequestParams req);
}
