package com.myshipment.service;

import java.util.List;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface ISearchPOSerivce {

	public List<HeaderPOData> getPosBasedOnParameter(SearchPoDTO searchPoDto);
	
	public List<SegPurchaseOrder> getLineItemPosBasedOnParameter(SearchPoDTO searchPoDto);
	
	public List<SegPurchaseOrder> getAllLineItemPo(String poNo);
	
	public Long updatePo(SegPurchaseOrder segPurchaseOrder);
}
