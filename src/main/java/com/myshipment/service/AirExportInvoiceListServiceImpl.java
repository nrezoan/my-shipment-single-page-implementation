package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.model.InvoiceListJsonData;
import com.myshipment.model.InvoiceTrackingParams;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class AirExportInvoiceListServiceImpl implements IAirExportInvoiceListService{

	private Logger logger = Logger.getLogger(AirExportInvoiceListServiceImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public InvoiceListJsonData getAirExportInvoiceList(InvoiceTrackingParams req) {
		InvoiceListJsonData invoiceListJsonData = null;
		StringBuffer url=new StringBuffer(RestUtil.AIR_EXPORT_INVOICE_LIST);
		invoiceListJsonData = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, InvoiceListJsonData.class);
		return invoiceListJsonData;
	}


}
