package com.myshipment.service;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.metadata.IIOInvalidTreeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.CRMLogger;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.OrderItem;
import com.myshipment.util.SessionUtil;

@Service
public class FileUploadService implements IFileUploadService {

	private static final Logger logger = Logger.getLogger(FileUploadService.class);
	
//	private DirectBookingParams directBookingParamsIfNotUpdated = new DirectBookingParams();
	
	
	@Override
	public DirectBookingParams processFileUploaded(MultipartFile multipartFile, String filename,
			DirectBookingParams directBookingParams) {
		
//		setDirectBookingParamsIfNotUpdated(directBookingParams);
/*		List<OrderItem> tempItemList = directBookingParams.getOrderItemLst();
		List<CRMLogger> updateLoggerList = new ArrayList<CRMLogger>();*/

		logger.info("");

		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(multipartFile.getBytes());

			// fis = new FileInputStream(new File(name));
			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (filename.toLowerCase().endsWith("xlsx")) {
				logger.info("File type is xlsx.");
				workbook = new XSSFWorkbook(bis);
			} else if (filename.toLowerCase().endsWith("xls")) {
				logger.info("File type is xls.");
				workbook = new HSSFWorkbook(bis);
			}

			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();

			for (int i = 0; i < numberOfSheets; i++) {

				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);

				// every sheet has rows, iterate over them
				String sheetName = sheet.getSheetName();
				if (sheetName.equalsIgnoreCase("item_detail"));
				{
					// List<OrderItem>
					// lstOrderItem=directBookingParams.getOrderItemLst();
					List<OrderItem> lstOrderItemNew = new ArrayList<>();
					Iterator<Row> rowIterator = sheet.iterator();
					int itemNumber = 10;
					// int index=0;
					while (rowIterator.hasNext()) {

						OrderItem orderItem = new OrderItem();
						Row row = rowIterator.next();
						if (row.getRowNum() == 0)
							continue;
						Iterator<Cell> cellIterator = row.cellIterator();
						Cell nextCell;
						int columnIndex = 0;
						while (cellIterator.hasNext()) {
							nextCell = cellIterator.next();
							columnIndex = (nextCell.getColumnIndex() + 1);
							switch (columnIndex) {
							case 1:
								orderItem.setPoNumber(getCellValue(nextCell).toString());
								break;
							case 2:
								orderItem.setCurtonQuantity(Integer.parseInt(getCellValue(nextCell).toString()));								
								break;
							case 3:
								orderItem.setTotalPieces(Integer.parseInt(getCellValue(nextCell).toString()));								
								break;
							case 4:
								orderItem.setCartonLength(Double.parseDouble(getCellValue(nextCell).toString()));								
								break;
							case 5:
								orderItem.setCartonWidth(Double.parseDouble(getCellValue(nextCell).toString()));								
								break;
							case 6:
								orderItem.setCartonHeight(Double.parseDouble(getCellValue(nextCell).toString()));								
								break;
							case 7:
								orderItem.setCartonUnit(getCellValue(nextCell).toString());
								break;
							case 8:
								orderItem.setTotalVolume(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 9:
								orderItem.setGrossWeight(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 10:
								orderItem.setHsCode(getCellValue(nextCell).toString());
								break;
							case 11:
								orderItem.setMaterial(getCellValue(nextCell).toString());
								break;
							case 12:
								orderItem.setUnit(getCellValue(nextCell).toString());
								break;
							case 13:
								orderItem.setStyleNo(getCellValue(nextCell).toString());
								break;
							case 14:
								orderItem.setColor(getCellValue(nextCell).toString());
								break;
							case 15:
								orderItem.setSizeNo(getCellValue(nextCell).toString());
								break;
							case 16:
								orderItem.setPcsPerCarton(Integer.parseInt(getCellValue(nextCell).toString()));
								break;
							case 17:
								orderItem.setGrossWeightPerCarton(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 18:
								orderItem.setNetWeightPerCarton(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 19:
								orderItem.setNetWeight(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 20:
								orderItem.setProductCode(getCellValue(nextCell).toString());
								break;
							case 21:
								orderItem.setProjectNo(getCellValue(nextCell).toString());
								break;
							case 22:
								orderItem.setCartonSerNo(getCellValue(nextCell).toString());
								break;
							case 23:
								orderItem.setCommInvoice(getCellValue(nextCell).toString());
								break;
							case 24:
								orderItem.setCommInvoiceDate(getCellValue(nextCell).toString());
								break;
							case 25:
								orderItem.setQcDate(getCellValue(nextCell).toString());
								break;
							case 26:
								orderItem.setReleaseDate(getCellValue(nextCell).toString());
								break;
							case 27:
								orderItem.setDescription(getCellValue(nextCell).toString());
								break;
							case 28:
								orderItem.setItemDescription(getCellValue(nextCell).toString());
								break;
							case 29:
								orderItem.setShippingMark(getCellValue(nextCell).toString());
								break;
							case 30:
								orderItem.setPayCond(getCellValue(nextCell).toString());
								break;
							case 31:
								orderItem.setCurrency(getCellValue(nextCell).toString());
								break;
							case 32:
								orderItem.setUnitPrice(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 33:
								orderItem.setNetCost(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 34:
								orderItem.setRefNo(getCellValue(nextCell).toString());
								break;
							case 35:
								orderItem.setReference1(getCellValue(nextCell).toString());
								break;
							case 36:
								orderItem.setReference3(getCellValue(nextCell).toString());
								break;
							case 37:
								orderItem.setReference4(getCellValue(nextCell).toString());
								break;
							case 38:
								orderItem.setReference5(getCellValue(nextCell).toString());
								break;
								
/*							case 1:
								orderItem.setPoNumber(getCellValue(nextCell).toString());
								break;
							case 2:
								orderItem.setStyleNo(getCellValue(nextCell).toString());
								break;
							case 3:
								orderItem.setRefNo(getCellValue(nextCell).toString());
								break;
							case 4:
								orderItem.setProjectNo(getCellValue(nextCell).toString());
								break;
							case 5:
								orderItem.setProductCode(getCellValue(nextCell).toString());
								break;
							case 6:
								orderItem.setColor(getCellValue(nextCell).toString());
								break;
							case 7:
								orderItem.setSizeNo(getCellValue(nextCell).toString());
								break;
							case 8:
								orderItem.setTotalPieces(
										new Double(Double.parseDouble(getCellValue(nextCell).toString())).intValue());
								break;
							case 9:
								orderItem.setCartonLength(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 10:
								orderItem.setCartonWidth(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 11:
								orderItem.setCartonHeight(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 12:
								orderItem.setCartonUnit(getCellValue(nextCell).toString());
								break;
							case 13:
								orderItem.setCurtonQuantity(
										new Double(Double.parseDouble(getCellValue(nextCell).toString())).intValue());
								break;
							case 14:
								orderItem.setGrossWeight(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 15:
								if ((getCellValue(nextCell).toString()).equals(""))
									orderItem.setNetWeight(Double.parseDouble("0.0"));
								else
									orderItem.setNetWeight(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 16:
								// orderItem.setTotalCbm(Double.parseDouble(getCellValue(nextCell).toString()));
								orderItem.setTotalVolume(Double.parseDouble(getCellValue(nextCell).toString()));
								break;
							case 17:
								orderItem.setUnit((String) getCellValue(nextCell));
								break;
							case 18:
								orderItem.setMaterial((String) getCellValue(nextCell));
								break;
							case 19:
								orderItem.setPayCond((String) getCellValue(nextCell));
								break;
							case 20:
								if (getCellValue(nextCell).equals(""))
									orderItem.setUnitPrice(0.0);
								else
									orderItem.setUnitPrice((Double) getCellValue(nextCell));
								break;
							case 21:
								orderItem.setCurrency((String) getCellValue(nextCell));
								break;
							case 22:
								orderItem.setCommInvoice((String) getCellValue(nextCell));
								break;
							case 23:
								orderItem.setCommInvoiceDate((String) getCellValue(nextCell));
								break;
							case 24:
								if (getCellValue(nextCell).equals(""))
									orderItem.setNetCost(0.0);
								else
									orderItem.setNetCost((Double) getCellValue(nextCell));
								break;
							case 25:
								if (getCellValue(nextCell).toString().equals(""))
									orderItem.setPcsPerCarton(0);
								else
									orderItem.setPcsPerCarton(
											new Double(Double.parseDouble(getCellValue(nextCell).toString()))
													.intValue());
								break;
							case 26:
								if (getCellValue(nextCell).equals(""))
									orderItem.setNetWeightPerCarton(0.0);
								else
									orderItem.setGrossWeightPerCarton((Double) getCellValue(nextCell));
								break;
							case 27:
								if (getCellValue(nextCell).equals(""))
									orderItem.setNetWeightPerCarton(0.0);
								else
									orderItem.setNetWeightPerCarton((Double) getCellValue(nextCell));
								break;
							case 28:
								orderItem.setReference1((String) getCellValue(nextCell));
								break;
							case 29:
								orderItem.setReference2((String) getCellValue(nextCell));
								break;
							case 30:
								orderItem.setReference3((String) getCellValue(nextCell));
								break;
							case 31:
								orderItem.setReference4((String) getCellValue(nextCell));
								break;
							case 32:
								orderItem.setReference5((String) getCellValue(nextCell));
								break;
							case 33:
								orderItem.setShippingMark((String) getCellValue(nextCell));
								break;
							case 34:
								orderItem.setDescription((String) getCellValue(nextCell));
								break;
							case 35:
								orderItem.setItemDescription((String) getCellValue(nextCell));
								break;
							case 36:
								orderItem.setHsCode((String) getCellValue(nextCell));
								break;

							case 37:
								if (getCellValue(nextCell).toString() != null && getCellValue(nextCell).toString() != ""
										&& !getCellValue(nextCell).toString().isEmpty())
									orderItem.setPosnerNumber(
											new Double(Double.parseDouble(getCellValue(nextCell).toString()))
													.intValue());
								break;*/	
/*							case 40:
								orderItem.setReleaseDate((String) getCellValue(nextCell));
								break;
							case 41:
								orderItem.setQcDate((String) getCellValue(nextCell));
								break;
							case 42:
								orderItem.setCartonSerNo((String) getCellValue(nextCell));
								break;*/

							}

						}
						//nextCell = cellIterator.next();
						//orderItem.setReference5(getCellValue(nextCell).toString());
						orderItem.setItemNumber(Integer.toString(itemNumber));
						/*
						 * try{ OrderItem oitem=lstOrderItem.get(index);
						 * orderItem.setTotalVolume(oitem.getTotalVolume());
						 * }catch(Exception ex){ orderItem.setTotalVolume(new
						 * Double(0)); } index=index+1;
						 */
						itemNumber += 10;
						lstOrderItemNew.add(orderItem);

					}
					directBookingParams.setOrderItemLst(lstOrderItemNew);
				}
			}

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		//Boolean check = validateMandatory(directBookingParams);
		//if(check) {
			return directBookingParams;
		//}

		//return getDirectBookingParamsIfNotUpdated();
	}

	private Object getCellValue(Cell cell) {
		if (cell == null)
			return "";
		switch (cell.getCellType()) {
			case Cell.CELL_TYPE_STRING: 
				//System.out.println("Cell value: " + cell.getStringCellValue());
				return cell.getStringCellValue() == null ? "" : cell.getStringCellValue();
			
			case Cell.CELL_TYPE_BOOLEAN:
				return cell.getBooleanCellValue();
	
			case Cell.CELL_TYPE_NUMERIC:
				String numericCell = NumberToTextConverter.toText(cell.getNumericCellValue());
				return numericCell;
	
			case Cell.CELL_TYPE_BLANK:
				return cell.getStringCellValue();
				
			case Cell.CELL_TYPE_FORMULA:
				return cell.getCellFormula();
			
			default :	
				return cell.getStringCellValue() == null ? "" : cell.getStringCellValue();
		}
	}
	
/*	private Boolean validateMandatory(DirectBookingParams directBookingParamsUpdated) {
		// TODO Auto-generated method stub
		Boolean validateUpdate = false;
		int count = 0;
		List<OrderItem> orderItemLst = directBookingParamsUpdated.getOrderItemLst();
		for(OrderItem orderItem : orderItemLst) {
			
			System.out.println(orderItem.getPoNumber());
			if(orderItem.getPoNumber() != null) {
				if(orderItem.getCurtonQuantity() != null) {
					if(orderItem.getTotalPieces() != null) {
						if(orderItem.getTotalVolume() != null) {
							if(orderItem.getGrossWeight() != null || orderItem.getPoNumber() != "") {
								if(orderItem.getHsCode() != null || orderItem.getPoNumber() != "") {
									if(orderItem.getMaterial() != null || orderItem.getPoNumber() != "") {
										count++;
									}
								}
							}							
						}
					}
				}				
			}
			
		}//for loop
		
		if(count == orderItemLst.size())
			validateUpdate = true;
		else
			validateUpdate = false;
		
		return validateUpdate;
	
	}*/
	
/*	public DirectBookingParams getDirectBookingParamsIfNotUpdated() {
		return directBookingParamsIfNotUpdated;
	}

	public void setDirectBookingParamsIfNotUpdated(DirectBookingParams directBookingParamsIfNotUpdated) {
		this.directBookingParamsIfNotUpdated = directBookingParamsIfNotUpdated;
	}*/

}
