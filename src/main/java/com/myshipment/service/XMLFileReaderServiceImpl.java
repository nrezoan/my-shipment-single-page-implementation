package com.myshipment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IXMLFileReaderDAO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.xmlpobean.CarfourPOXMLBean;
import com.myshipment.xmlpobean.Produit;
import com.myshipment.xmlpobean.QteProduit;
import com.myshipment.xmlpobean.Tranche;
/*
 * @Ranjeet Kumar
 */
@Service
public class XMLFileReaderServiceImpl implements IXMLFileReaderService{

	@Autowired
	private IXMLFileReaderDAO iXMLFileReaderDAO;
	
	@Override
	public void processXMLData(List<CarfourPOXMLBean> carfourPOXMLBeanLst) {
		
		List<List<SegPurchaseOrder>> segPurchaseOrderLst = new ArrayList<List<SegPurchaseOrder>>();
		
		for(CarfourPOXMLBean carfourPOXMLBean : carfourPOXMLBeanLst){
			List<SegPurchaseOrder> segPurOrd = prepareDataAsPerLineItemPO(carfourPOXMLBean);
			segPurchaseOrderLst.add(segPurOrd);
		}
		
		iXMLFileReaderDAO.saveXMLDataToTheDatabase(segPurchaseOrderLst);
	}

	
private List<SegPurchaseOrder> prepareDataAsPerLineItemPO(CarfourPOXMLBean xmlData){
		
		List<SegPurchaseOrder> segPurchaseOrderLst = new ArrayList<SegPurchaseOrder>();		
		Map<String, Produit> codProProduitMap = new HashMap<String, Produit>();
		
		
		List<Produit> proditLst =  xmlData.getProduitLst();
		List<Tranche> trancheLst = xmlData.getTrancheLst();
		
		//prepare a map for Produit
		for(Produit produit : proditLst){
			String codPro = produit.getCod_pro();
			codProProduitMap.put(codPro, produit);
		}
		
		//get the QteProduit list. Based on the this line item is defined.
		//The no of QteProduit is equal to the no of line item to be created in the database.
		
		for(Tranche tranche : trancheLst){
			List<QteProduit> qtes = tranche.getQteProduit();
			for(QteProduit qteProduit : qtes){
				SegPurchaseOrder segPurchaseOrder = new SegPurchaseOrder();
				segPurchaseOrder.setVc_po_no(xmlData.getNum_uat());
				segPurchaseOrder.setVc_pol(xmlData.getCod_vil_inc());

				//get the respective Produit
				if(codProProduitMap != null){
					Produit produit = codProProduitMap.get(qteProduit.getCod_pro_qte_produit());

					if(produit != null){
						//Setting the values from the Produit
						segPurchaseOrder.setVc_sku_no(produit.getCod_pro());
						segPurchaseOrder.setVc_style_no(produit.getCod_pro_pro());
						segPurchaseOrder.setVc_product_no(produit.getCod_ray());
						segPurchaseOrder.setNu_length(Double.valueOf(produit.getCol_lon()));
						segPurchaseOrder.setNu_width(Double.valueOf(produit.getCol_lar()));
						segPurchaseOrder.setNu_height(Double.valueOf(produit.getCol_hau()));
						segPurchaseOrder.setVc_ref_field3(produit.getCod_unl_ach());
						segPurchaseOrder.setNu_no_pcs_ctns(Double.valueOf(produit.getPcb()));

						//Setting the values from the Tranche
						segPurchaseOrder.setDt_etd(tranche.getDat_etd());
						segPurchaseOrder.setVc_ref_field2(tranche.getCod_trs_cli());
						segPurchaseOrder.setVc_pod(tranche.getCod_vil_prc2());
						segPurchaseOrder.setVc_ref_field5(tranche.getCod_trs_dep());

						//Setting the values from the QteProduit
						segPurchaseOrder.setVc_color(qteProduit.getCod_cou_qte_produit());
						segPurchaseOrder.setVc_size(qteProduit.getCod_tai_qte_produit()==null?"" : qteProduit.getCod_tai_qte_produit());
						segPurchaseOrder.setVc_quan(Double.valueOf(qteProduit.getNbr_col()));
						segPurchaseOrder.setVc_tot_pcs(Double.valueOf(qteProduit.getNbr_uvc()));
						
						//Some common fields
						segPurchaseOrder.setCurrentDate(new Date());
						
						segPurchaseOrderLst.add(segPurchaseOrder);
					}
				}
			}
		}
		
		return segPurchaseOrderLst;
	}

}
