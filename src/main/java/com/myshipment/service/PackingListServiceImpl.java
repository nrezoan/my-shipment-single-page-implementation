package com.myshipment.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.model.BuyerDetails;
import com.myshipment.model.CartoonDimension;
import com.myshipment.model.ConsigneeDetails;
import com.myshipment.model.MakerDetails;
import com.myshipment.model.PackListParams;
import com.myshipment.model.PackListReportMainPage;
import com.myshipment.model.PackListReportTrailPage;
import com.myshipment.model.PackingListItems;
import com.myshipment.model.PackingListJson;
import com.myshipment.model.PackingListOrderHeader;
import com.myshipment.model.PackingListOrderLineItem;
import com.myshipment.model.PackingListReportJsonOutputData;
import com.myshipment.model.PackingListReportOutputDto;
import com.myshipment.model.PoBookingPacklistColorBean;
import com.myshipment.model.PoBookingPacklistDetailBean;
import com.myshipment.model.PoBookingPacklistSizeBean;
import com.myshipment.model.SellerDetails;
import com.myshipment.model.TotalOfPackingList;
import com.myshipment.model.ZbapiPacListHDTO;
import com.myshipment.model.ZbapiPacListIDTO;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

import javassist.bytecode.stackmap.BasicBlock.Maker;

@Service
public class PackingListServiceImpl implements IPackingListReport{
	@Autowired
	private RestService restService; 

	@Override
	public PackingListReportOutputDto getPackingListReportDataService(PackListParams packListParams) {
		
		PackingListReportOutputDto packingListReportOutputDto=null;
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.PACKING_LIST_REPORT);
		PackingListReportJsonOutputData packingListReportJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), packListParams, PackingListReportJsonOutputData.class);
		
		webServiceUrl=new StringBuffer(RestUtil.PACKING_LIST_BY_HBL);
		PackingListJson packingListJson=restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), packListParams, PackingListJson.class);
		if(packingListReportJsonOutputData!=null && (packingListJson!=null && packingListJson.getPoBookingPacklistDetailBeans().size()>0))
			packingListReportOutputDto= preparePackingListDto(packingListReportJsonOutputData,packingListJson);
		return packingListReportOutputDto;
	}
	private PackingListReportOutputDto preparePackingListDto(PackingListReportJsonOutputData packingListReportJsonOutputData,PackingListJson packingListJson)
	{
		
		 List<ZbapiPacListHDTO> waHeader=packingListReportJsonOutputData.getWaHeader();	
		 List<ZbapiPacListHDTO> itCntryorig=packingListReportJsonOutputData.getItCntryorig();
		 List<ZbapiPacListHDTO> itCntrydest=packingListReportJsonOutputData.getItCntrydest();	
		 List<ZbapiPacListHDTO> itVendor=packingListReportJsonOutputData.getItVendor();
		 List<ZbapiPacListHDTO> itVendorcountry=packingListReportJsonOutputData.getItVendorcountry();
		 List<ZbapiPacListHDTO> itBuyer=packingListReportJsonOutputData.getItBuyer();	
		 List<ZbapiPacListHDTO> itBuyercountry=packingListReportJsonOutputData.getItBuyercountry();
		 List<ZbapiPacListHDTO> itPol=packingListReportJsonOutputData.getItPol();	
		 List<ZbapiPacListHDTO> itPod=packingListReportJsonOutputData.getItPod();	
		 List<ZbapiPacListHDTO> itPodel=packingListReportJsonOutputData.getItPodel();	
		 List<ZbapiPacListIDTO> waItem=packingListReportJsonOutputData.getWaItem();
		 List<ZbapiPacListHDTO> itVbkd=packingListReportJsonOutputData.getItVbkd();	
		 List<ZbapiPacListHDTO> itMaker=packingListReportJsonOutputData.getItMaker();	
		 List<ZbapiPacListHDTO> itConsignee=packingListReportJsonOutputData.getItConsignee();	
		 List<ZbapiPacListHDTO> itMakercountry=packingListReportJsonOutputData.getItMakercountry();
		 List<ZbapiPacListHDTO> itConsigneecountry=packingListReportJsonOutputData.getItConsigneecountry();
		 /*if(packingListJson.getPoBookingPacklistDetailBeans().size()==waItem.size())
		 {
			 
			 
		 }*/
		 
		 //Create Seller Object
		 ZbapiPacListHDTO zbapiPacListHDTO=null;
		 SellerDetails sellerDetails=new SellerDetails();
		 if(itVendor!=null && itVendor.size()>0)
		 {
			 zbapiPacListHDTO=itVendor.get(0);
		 sellerDetails.setSellerName(zbapiPacListHDTO.getName1());
		 sellerDetails.setSellerAddress(zbapiPacListHDTO.getName2()+" "+zbapiPacListHDTO.getName3());
		 sellerDetails.setSellerCity(zbapiPacListHDTO.getCity1());
		 }
		 if(itVendorcountry!=null && itVendorcountry.size()>0)
		 {
		 zbapiPacListHDTO=itBuyercountry.get(0);
		 sellerDetails.setSellerCountry(zbapiPacListHDTO.getLandx());
		 }
		 //Create Buyer Object
		 BuyerDetails buyerDetails=new BuyerDetails();
		 if(itBuyer!=null && itBuyer.size()>0)
		 {
		 zbapiPacListHDTO=itBuyer.get(0);
		 buyerDetails.setBuyerName(zbapiPacListHDTO.getName1());
		 buyerDetails.setAddress(zbapiPacListHDTO.getName2()+" "+zbapiPacListHDTO.getName3()+" "+zbapiPacListHDTO.getName4());
		 buyerDetails.setBuyerCity(zbapiPacListHDTO.getCity1());
		 }
		 if(itBuyercountry!=null && itBuyercountry.size()>0)
		 {
		 zbapiPacListHDTO=itBuyercountry.get(0);
		 buyerDetails.setBuyerCountry(zbapiPacListHDTO.getLandx());
		 }
		 
		 //Create Maker Object
		 MakerDetails makerDetails=new MakerDetails();
		 if(itMaker!=null && itMaker.size()>0)
		 {
		 zbapiPacListHDTO=itMaker.get(0);
		 makerDetails.setMakerName(zbapiPacListHDTO.getName1());
		 makerDetails.setMakerAddress(zbapiPacListHDTO.getName2()+" "+zbapiPacListHDTO.getName3()+" "+zbapiPacListHDTO.getName4());
		 makerDetails.setMakerCity(zbapiPacListHDTO.getCity1());
		 }
		 if(itMakercountry!=null && itMakercountry.size()>0)
		 {
		 zbapiPacListHDTO=itMakercountry.get(0);
		 makerDetails.setMakerCountry(zbapiPacListHDTO.getLandx());
		 }
		 
		 //Create Consignee Object
		 ConsigneeDetails consigneeDetails=new ConsigneeDetails();
		 if(itConsignee!=null && itConsignee.size()>0)
		 {
			 zbapiPacListHDTO=itConsignee.get(0);
			 consigneeDetails.setConsigneeName(zbapiPacListHDTO.getName1());
			 consigneeDetails.setConsigneeAddress(zbapiPacListHDTO.getName2()+" "+zbapiPacListHDTO.getName3()+" "+zbapiPacListHDTO.getName4());
			 consigneeDetails.setConsigneeCity(zbapiPacListHDTO.getCity1());
			 
		 }
		 if(itConsigneecountry!=null && itConsigneecountry.size()>0)
		 {
			 zbapiPacListHDTO=itConsigneecountry.get(0);
			 consigneeDetails.setConsigneeCountry(zbapiPacListHDTO.getLandx());
		 }
		 PackingListOrderHeader packingListOrderHeader=new PackingListOrderHeader();
		 ZbapiPacListHDTO hdto=waHeader.get(0);
		 packingListOrderHeader.setCountryOfDestination(hdto.getZzcntryDest());
		 packingListOrderHeader.setCountryOfOrigin(hdto.getZzcntryOrig());
		 packingListOrderHeader.setFinalDestination(hdto.getZzportoffinaldst());
		 packingListOrderHeader.setIncoterm(hdto.getInco1()+""+hdto.getInco2());
		 packingListOrderHeader.setModeOfShipment(hdto.getZzmode_shipment());
		 packingListOrderHeader.setPlaceOfReciept(hdto.getZzplacereceipt());
		 packingListOrderHeader.setPortOfDischarge(hdto.getZzportdest());
		 packingListOrderHeader.setPortOfLoading(hdto.getZzportloading());
		 
		 
		 int count=0;
		 PackingListReportOutputDto packingListReportOutputDto=createPageWiseListDataForPackListReport(packingListOrderHeader,waItem, packingListJson);
		 List<PackListReportMainPage> lstPackingListReportMainPage=packingListReportOutputDto.getLstPackListReportMainPage();
		 for (Iterator iterator = lstPackingListReportMainPage.iterator(); iterator.hasNext();) {
			PackListReportMainPage packListReportMainPage = (PackListReportMainPage) iterator.next();
			if(count==0)
			{
				packListReportMainPage.setHblNumber(waHeader.get(0).getZzhblhawbno());
				packListReportMainPage.setSoNumber(waHeader.get(0).getVbeln());
				
			}
			List<PackingListItems> lstPackingListItems=packListReportMainPage.getLstPackingListItems();
			TotalOfPackingList totalOfPackingList=new TotalOfPackingList();
			for (Iterator iterator2 = lstPackingListItems.iterator(); iterator2.hasNext();) {
				PackingListItems packingListItems = (PackingListItems) iterator2.next();
				totalOfPackingList.setTotalCtns(totalOfPackingList.getTotalCtns()+packingListItems.getNoOfCartons());
				
			}
			packListReportMainPage.setTotalOfPackingList(totalOfPackingList);
			
			
		}
		 
		
		 
		 
		 
		return packingListReportOutputDto;
	}
	
private PackingListReportOutputDto createPageWiseListDataForPackListReport(PackingListOrderHeader packingListOrderHeader,List<ZbapiPacListIDTO> lstwaItems,PackingListJson packingListJson)
{
	PackingListReportOutputDto packingListReportOutputDto=new PackingListReportOutputDto();
	List<PackListReportMainPage> lstPackListReportMainPage=new ArrayList<>();
	PackListReportTrailPage packListReportTrailPage=new PackListReportTrailPage(); 
	List<PackingListOrderLineItem> lstPackingListOrdereLineItem=parseLineItemData(lstwaItems);
	List<PackingListItems> lstPackingListItem=createPackingListObject(packingListJson,lstPackingListOrdereLineItem.get(0).getStyleNumber());
	if(lstwaItems.size()==lstPackingListItem.size())
	{
		int i=0;
		for (Iterator iterator = lstPackingListOrdereLineItem.iterator(); iterator.hasNext();) {
			PackingListOrderLineItem packingListOrderLineItem = (PackingListOrderLineItem) iterator.next();
			PackListReportMainPage packListReportMainPage=new PackListReportMainPage();
			packListReportMainPage.setPackingListOrderHeader(packingListOrderHeader);
			packListReportMainPage.setPackingListOrderLineItem(packingListOrderLineItem);
			List<PackingListItems> lstlist=new ArrayList<>();
					lstlist.add(lstPackingListItem.get(i));
			packListReportMainPage.setLstPackingListItems(lstlist);
			
			packingListReportOutputDto.setLstPackListReportMainPage(lstPackListReportMainPage);
			i++;
		}
		if(lstPackingListOrdereLineItem.size()<lstPackingListItem.size())
		{
			List<PackListReportMainPage> lstpackListReportMainPage=packingListReportOutputDto.getLstPackListReportMainPage();
			PackListReportMainPage packListReportMainPage=lstpackListReportMainPage.get(0);
			List<PackingListItems> lstlist=packListReportMainPage.getLstPackingListItems();
			
			int difference=lstPackingListItem.size()-lstPackingListOrdereLineItem.size();
			
			for (int j = 0; j < difference; j++) {
				
				PackingListItems packingListItems=lstPackingListItem.get(lstPackingListOrdereLineItem.size()+j);
				lstlist.add(packingListItems);
				
			}
			packListReportMainPage.setLstPackingListItems(lstlist);
			lstpackListReportMainPage.add(0, packListReportMainPage);
			packingListReportOutputDto.setLstPackListReportMainPage(lstPackListReportMainPage);
			
			
			
			
		}
	
	}
	if(lstwaItems.size()<lstPackingListItem.size())
	{
		int i=0;
		for (Iterator iterator = lstPackingListOrdereLineItem.iterator(); iterator.hasNext();) {
			PackingListOrderLineItem packingListOrderLineItem = (PackingListOrderLineItem) iterator.next();
			PackListReportMainPage packListReportMainPage=new PackListReportMainPage();
			packListReportMainPage.setPackingListOrderHeader(packingListOrderHeader);
			packListReportMainPage.setPackingListOrderLineItem(packingListOrderLineItem);
			List<PackingListItems> lstlist=new ArrayList<>();
					lstlist.add(lstPackingListItem.get(i));
			packListReportMainPage.setLstPackingListItems(lstlist);
			
			packingListReportOutputDto.setLstPackListReportMainPage(lstPackListReportMainPage);
			i++;
		}
		if(lstPackingListOrdereLineItem.size()<lstPackingListItem.size())
		{
			List<PackListReportMainPage> lstpackListReportMainPage=packingListReportOutputDto.getLstPackListReportMainPage();
			PackListReportMainPage packListReportMainPage=lstpackListReportMainPage.get(0);
			List<PackingListItems> lstlist=packListReportMainPage.getLstPackingListItems();
			
			int difference=lstPackingListItem.size()-lstPackingListOrdereLineItem.size();
			
			for (int j = 0; j < difference; j++) {
				
				PackingListItems packingListItems=lstPackingListItem.get(lstPackingListOrdereLineItem.size()+j);
				lstlist.add(packingListItems);
				
			}
			packListReportMainPage.setLstPackingListItems(lstlist);
			lstpackListReportMainPage.add(0, packListReportMainPage);
			packingListReportOutputDto.setLstPackListReportMainPage(lstPackListReportMainPage);
			
			
			
			
		}
		
	}
	if(lstwaItems.size()>lstPackingListItem.size())
	{
		int i=0;
		for (Iterator iterator = lstPackingListItem.iterator(); iterator.hasNext();) {
			PackingListItems packingListItems = (PackingListItems) iterator.next();
			PackListReportMainPage packListReportMainPage=new PackListReportMainPage();
			packListReportMainPage.setPackingListOrderHeader(packingListOrderHeader);
			PackingListOrderLineItem packingListOrderLineItem=lstPackingListOrdereLineItem.get(0);
			packListReportMainPage.setPackingListOrderLineItem(packingListOrderLineItem);
			List<PackingListItems> lstlist=new ArrayList<>();
					lstlist.add(packingListItems);
			packListReportMainPage.setLstPackingListItems(lstlist);
			
			packingListReportOutputDto.setLstPackListReportMainPage(lstPackListReportMainPage);
			i++;
		}
		
		
	}
	
	
	return packingListReportOutputDto;
}

private List<PackingListItems> createPackingListObject(PackingListJson packingListJson,String referenceNumber)
{
	List<PackingListItems> lstPackingListItems=new ArrayList<>();
	List<PoBookingPacklistDetailBean> lstpacklist=packingListJson.getPoBookingPacklistDetailBeans();
	PoBookingPacklistDetailBean poBookingPacklistDetailBean=lstpacklist.get(0);
	Set<PoBookingPacklistColorBean> setPoBookingPacklistColorBean=poBookingPacklistDetailBean.getPacklistColors();
	for (Iterator iterator = setPoBookingPacklistColorBean.iterator(); iterator.hasNext();) {
		PoBookingPacklistColorBean poBookingPacklistColorBean = (PoBookingPacklistColorBean) iterator.next();
		PackingListItems packingListItems=new PackingListItems();
		packingListItems.setCartonNumbers(poBookingPacklistDetailBean.getCtnSrlNo());
		packingListItems.setColors(poBookingPacklistColorBean.getColorName());
		packingListItems.setItemReference(referenceNumber);
		packingListItems.setNoOfCartons(poBookingPacklistDetailBean.getNoOfContainers());
		packingListItems.setPcs(0);
		packingListItems.setPcsPerCarton(poBookingPacklistColorBean.getPcsPerCtn());
		packingListItems.setQuantityInPcs(poBookingPacklistColorBean.getTotalPcs());
		Set<PoBookingPacklistSizeBean> lstpoBookingPacklistSizeBean=poBookingPacklistColorBean.getPacklistSizes();
		List<Integer> lstSizes=new ArrayList<>();
		for (Iterator iterator2 = lstpoBookingPacklistSizeBean.iterator(); iterator2.hasNext();) {
			PoBookingPacklistSizeBean poBookingPacklistSizeBean = (PoBookingPacklistSizeBean) iterator2.next();
			lstSizes.add(poBookingPacklistSizeBean.getNoOfPcs());
		}
		packingListItems.setSizes(lstSizes);
		lstPackingListItems.add(packingListItems);
	}
	return lstPackingListItems;
}

private List<PackingListOrderLineItem> parseLineItemData(List<ZbapiPacListIDTO> zbapiPacListIDTO)
{
	Map<String,PackingListOrderLineItem> mapPackingListOrderLineItem=new HashMap<>();
	List<PackingListOrderLineItem> lstPackingListOrderLineItem=new ArrayList<>();
	for (ZbapiPacListIDTO zbapiPacListIDTO2 : zbapiPacListIDTO) {
		PackingListOrderLineItem packingListOrderLineItem=new  PackingListOrderLineItem();
		packingListOrderLineItem.setOrderNumber(zbapiPacListIDTO2.getZzponumber());
		packingListOrderLineItem.setStyleNumber(zbapiPacListIDTO2.getZzstyleno());
		CartoonDimension cartoonDimension=new CartoonDimension();
		cartoonDimension.setLength(zbapiPacListIDTO2.getZzlength());
		cartoonDimension.setWidth(zbapiPacListIDTO2.getZzwidth());
		cartoonDimension.setHeight(zbapiPacListIDTO2.getZzheight());
		packingListOrderLineItem.setCtnDimension(cartoonDimension);
		packingListOrderLineItem.setGrossWgt(zbapiPacListIDTO2.getZzgr_wt());
		packingListOrderLineItem.setNetWgt(zbapiPacListIDTO2.getZzntWt());
		packingListOrderLineItem.setNumberOfCartoons(new Long(0));
		packingListOrderLineItem.setPackType("");
		packingListOrderLineItem.setProductDescription(zbapiPacListIDTO2.getDescZ001()+" "+zbapiPacListIDTO2.getDescZ002()+" "+zbapiPacListIDTO2.getDescZ003()+" "+zbapiPacListIDTO2.getDescZ005());
		packingListOrderLineItem.setTotalQuantity(Long.parseLong(zbapiPacListIDTO2.getZzquantity()));
		packingListOrderLineItem.setUnitVolume(zbapiPacListIDTO2.getZunitVol());
		mapPackingListOrderLineItem.put(packingListOrderLineItem.getStyleNumber(), packingListOrderLineItem);
		lstPackingListOrderLineItem.add(packingListOrderLineItem);
	}
	Set<Map.Entry<String, PackingListOrderLineItem>> entrySet=mapPackingListOrderLineItem.entrySet();
	
	for (Entry<String, PackingListOrderLineItem> entry : entrySet) {
		lstPackingListOrderLineItem.add(entry.getValue());
	}
	
	return lstPackingListOrderLineItem;
	
}
}
