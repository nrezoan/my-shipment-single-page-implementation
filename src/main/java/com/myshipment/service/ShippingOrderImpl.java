package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.myshipment.model.ReportParams;
import com.myshipment.model.ShippingOrderReportJsonData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class ShippingOrderImpl implements IShippingOrder{

	@SuppressWarnings("unused")
	private Logger logger = Logger.getLogger(ShippingOrderImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public ShippingOrderReportJsonData getShippingOrderDetail(ReportParams req) {
		ShippingOrderReportJsonData shippingOrder = null;
		StringBuffer url=new StringBuffer(RestUtil.SHIPPING_ORDER);
		shippingOrder = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, ShippingOrderReportJsonData.class);
		return shippingOrder;
	}


}
