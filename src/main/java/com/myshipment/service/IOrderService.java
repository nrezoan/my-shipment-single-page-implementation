package com.myshipment.service;

import java.util.List;
import java.util.Map;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.BookingByPoDTO;
import com.myshipment.model.BuyerDTO;
import com.myshipment.model.DirectBookingJsonData;
import com.myshipment.model.Materials;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.WarehouseCode;


/**
 * @author virendra
 *
 */
public interface IOrderService {
	public List<StringBuffer> getPortList(String params);
	public DirectBookingJsonData saveOrderDetailsAndPAckList(List<Map<String, Object>> orderDetails,OrderHeader orderHeader,LoginDTO loginDTO);
	public List<ApprovedPurchaseOrder> getApprovedPONotBooked(String poNumber,String buyer,String fromDate,String toDate);
	/*public List<ApprovedPurchaseOrder> getPOforBooking(String buyer,String poNumber,String salesOrg,String division,String shipper);*/
	public List<PurchaseOrdersModel> getPOforBooking(String buyer,String poNumber,String shipper);
	public ApprovedPurchaseOrder getAppPurchaseOrderById(Long Id);
	//public void savePakListOrderBooking(Map<String,Object> packList);
	public List<StringBuffer> placeOfReciept(String searchString,String salesOrg);
	public DirectBookingJsonData saveOrderDetailsAndPAckListByPo(LoginDTO loginDTO,OrderHeader orderHeader,BookingByPoDTO bookingByPoDTO);
	public void saveBookingHeaderAsTemplate(SaveAsTemplateMdl bookingInfoMdl);
	public List<SaveAsTemplateMdl> getTemplateInfoByCred(SaveAsTemplateMdl saveAsTemplateMdl);
	public SaveAsTemplateMdl getTemplateInfoById(String templateId,String customer,String company,String operationType,String seaOrAir);
	public void updateTemplateInfo(String templateId,String tempInfo);
	public List<MaterialsDto> getMaterialService(String param);
	public List<StorageLocation> getStorageLocationService(String param);
	
	//nusrat
	public List<BuyerDTO> getHMBuyers(String buyer,String disChnlSelected, String divisionSelected, String salesOrgSelected);
	public List<WarehouseCode> getWHList(String countryCode);
	public DirectBookingJsonData saveOrderDetailsByPo(LoginDTO loginDto, OrderHeader orderHeader,
			List<PurchaseOrdersModel> itemList);
}
