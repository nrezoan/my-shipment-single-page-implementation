package com.myshipment.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
import com.myshipment.model.Login;
import com.myshipment.model.SupRequestParams;
import com.myshipment.util.MyShipApplicationException;

/**
 * @author Virendra Kumar
 *
 */
public interface ILoginService {

	/**
	 * @return
	 */
	public LoginDTO authenticateAndGetLoginDetails(Login login) throws MyShipApplicationException;
	public SupplierDetailsDTO getSupplierPreLoadedData(SupRequestParams supplierRequestParams,HttpSession session );
	public void saveUserFavouriteForm(FrequentlyUsedFormMdl obj);
	public List <FrequentUsedMenu> getFrequentUsedMenuById(LoginDTO loginDTO);
	


}
