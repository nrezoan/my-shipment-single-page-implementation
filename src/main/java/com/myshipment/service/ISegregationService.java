package com.myshipment.service;

import com.myshipment.dto.SegregationDTO;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface ISegregationService {

	public SegPurchaseOrder getLineItemForSegregation(int segId);
	public int doSegregation(SegregationDTO segregationDTO);
}
