package com.myshipment.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.myshipment.model.CommercialInvoiceDataForPage;
import com.myshipment.model.CommercialInvoiceJsonOutputData;
import com.myshipment.model.ZbapiPacListHDTO;
import com.myshipment.model.ZbapiPacListIDTO;
/*
 * @Ranjeet Kumar
 */
@Service
public class CommercialInvoiceServiceImpl implements ICommercialInvoiceService{

	@Override
	public CommercialInvoiceDataForPage prepareData(CommercialInvoiceJsonOutputData commercialInvoiceJsonOutputData) {

		CommercialInvoiceDataForPage commercialInvoiceDataForPage = new CommercialInvoiceDataForPage();		

		ZbapiPacListHDTO waHeader = commercialInvoiceJsonOutputData.getWaHeader().get(0);	
		ZbapiPacListHDTO itCntryorig = commercialInvoiceJsonOutputData.getItCntryorig().get(0);	
		ZbapiPacListHDTO itCntrydest = commercialInvoiceJsonOutputData.getItCntrydest().get(0);	
		ZbapiPacListHDTO itVendor = commercialInvoiceJsonOutputData.getItVendor().get(0);	
		ZbapiPacListHDTO itVendorcountry = commercialInvoiceJsonOutputData.getItVendorcountry().get(0);	
		ZbapiPacListHDTO itBuyer = commercialInvoiceJsonOutputData.getItBuyer().get(0);	
		ZbapiPacListHDTO itBuyercountry = commercialInvoiceJsonOutputData.getItBuyercountry().get(0);	
		ZbapiPacListHDTO itPol = commercialInvoiceJsonOutputData.getItPol().get(0);	
		ZbapiPacListHDTO itPod = commercialInvoiceJsonOutputData.getItPod().get(0);	
		ZbapiPacListHDTO itPodel = commercialInvoiceJsonOutputData.getItPodel().get(0);	
		List<ZbapiPacListIDTO> waItem = commercialInvoiceJsonOutputData.getWaItem();	
		ZbapiPacListHDTO itVbkd = commercialInvoiceJsonOutputData.getItVbkd().get(0);	
		ZbapiPacListHDTO itIssueBank = commercialInvoiceJsonOutputData.getItIssueBank().get(0);	
		ZbapiPacListHDTO itMaker = commercialInvoiceJsonOutputData.getItMaker().get(0);	
		ZbapiPacListHDTO itConsignee = commercialInvoiceJsonOutputData.getItConsignee().get(0);	
		ZbapiPacListHDTO itMakercountry = commercialInvoiceJsonOutputData.getItMakercountry().get(0);	
		ZbapiPacListHDTO itConsigneecountry = commercialInvoiceJsonOutputData.getItConsigneecountry().get(0);	
		ZbapiPacListHDTO itSupplierBank = commercialInvoiceJsonOutputData.getItSupplierBank().get(0);	
		ZbapiPacListHDTO itSbcountry = commercialInvoiceJsonOutputData.getItSbcountry().get(0);	
		ZbapiPacListHDTO itCostUnit = commercialInvoiceJsonOutputData.getItCostUnit().get(0);

		ZbapiPacListHDTO zbapiPacListHDTODataForPage = prepareZbapiPacListHDTODataForPage(waHeader,itCntryorig,itCntrydest,itVendor,itVendorcountry,itBuyer,itBuyercountry,itPol,itPod,itPodel,itVbkd,itIssueBank,itMaker,itConsignee,itMakercountry,itConsigneecountry,itSupplierBank,itSbcountry,itCostUnit);
		commercialInvoiceDataForPage.setZbapiPacListHDTO(zbapiPacListHDTODataForPage);
		commercialInvoiceDataForPage.setZbapiPacListIDTOLst(waItem);

		return commercialInvoiceDataForPage;
	}


	private ZbapiPacListHDTO prepareZbapiPacListHDTODataForPage(ZbapiPacListHDTO waHeader,ZbapiPacListHDTO itCntryorig, ZbapiPacListHDTO itCntrydest, ZbapiPacListHDTO itVendor,
			ZbapiPacListHDTO itVendorcountry, ZbapiPacListHDTO itBuyer, ZbapiPacListHDTO itBuyercountry, ZbapiPacListHDTO itPol, ZbapiPacListHDTO itPod, ZbapiPacListHDTO itPodel, ZbapiPacListHDTO itVbkd, ZbapiPacListHDTO itIssueBank,ZbapiPacListHDTO itMaker, ZbapiPacListHDTO itConsignee, ZbapiPacListHDTO itMakercountry,
			ZbapiPacListHDTO itConsigneecountry, ZbapiPacListHDTO itSupplierBank, ZbapiPacListHDTO itSbcountry, ZbapiPacListHDTO itCostUnit)
	{
		ZbapiPacListHDTO zbapiPacListHDTO = new ZbapiPacListHDTO();
		/*zbapiPacListHDTO.setAudat();
		zbapiPacListHDTO.setCity1();
		zbapiPacListHDTO.setCountry();
		zbapiPacListHDTO.setKunnr();
		zbapiPacListHDTO.setLandx();
		zbapiPacListHDTO.setName1();
		zbapiPacListHDTO.setName2();
		zbapiPacListHDTO.setName3();
		zbapiPacListHDTO.setName4();
		zbapiPacListHDTO.setNameAg();
		zbapiPacListHDTO.setNameRg();
		zbapiPacListHDTO.setNameWe();
		zbapiPacListHDTO.setNameZa();
		zbapiPacListHDTO.setNameZb();
		zbapiPacListHDTO.setNameZo();
		zbapiPacListHDTO.setNameZs();
		zbapiPacListHDTO.setNameZy();
		zbapiPacListHDTO.setVbeln();
		zbapiPacListHDTO.setVkorg();
		zbapiPacListHDTO.setZhblhawno();
		zbapiPacListHDTO.setZzcntryDest();
		zbapiPacListHDTO.setZzcntryOrig();
		zbapiPacListHDTO.setZzcomminvdt();
		zbapiPacListHDTO.setZzcomminvno();
		zbapiPacListHDTO.setZzexpdt();
		zbapiPacListHDTO.setZzexpno();
		zbapiPacListHDTO.setZzhblhawbdt();
		zbapiPacListHDTO.setZzlcdt();
		zbapiPacListHDTO.setZzlcpottno();
		zbapiPacListHDTO.setZzmblmawbdt();
		zbapiPacListHDTO.setZzmblmawbno();
		zbapiPacListHDTO.setZzportoffinaldst();
		zbapiPacListHDTO.setZzsoquantity();
		zbapiPacListHDTO.setZzsoquanuom();*/

		return zbapiPacListHDTO;
	}
}
