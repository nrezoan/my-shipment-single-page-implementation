package com.myshipment.service;

import com.myshipment.model.DailySaleReportParams;

public interface IDailySalesReportService {

	public DailySaleReportParams findMawb(String mawb);
}
