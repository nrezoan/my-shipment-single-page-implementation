package com.myshipment.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.gson.Gson;
import com.myshipment.model.OAGRequest;
import com.myshipment.model.OAGResponse;
import com.myshipment.model.OAG;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

public class OAGServiceImpl implements IOAGService {

	private Logger logger = Logger.getLogger(OAGServiceImpl.class);

	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}

	@Override
	public List<OAG> getFlightDetails(OAGRequest oagRequest) {

		Gson gson = new Gson();
		String request = gson.toJson(oagRequest);

		List<OAG> oagFlightDetailsResponse = new ArrayList<OAG>();

		logger.info(this.getClass().getName() + " getFlightDetails()  start-- ");
	

		StringBuffer url = new StringBuffer(RestUtil.GET_FLIGHT_DETAILS);

		try {
			logger.info("Posting request to OAG Server");
			//ResponseEntity<Object[]> postForObject = restService.postForObject(RestUtil.prepareUrlForOAGCall(url).toString(), oagRequest, Object[].class);
			
			ResponseEntity<OAG[]> postForObject = restService.postForEntity(RestUtil.prepareUrlForOAGCall(url).toString(), oagRequest, OAG[].class);
			
			OAG[] oagArray = postForObject.getBody();
			oagFlightDetailsResponse = Arrays.asList(oagArray);
			

		} catch (Exception e) {
			logger.error("Exception occuured, can't fetch data " + e);
			e.printStackTrace();
		}

		if (oagFlightDetailsResponse != null) {

			logger.info(this.getClass().getName() + " Flight Details Pulled Successfully");
			logger.info("returning flight details");
		} else {
			logger.info(this.getClass().getName() + "--Returning Null---");
		}

		return oagFlightDetailsResponse;
	}

}
