package com.myshipment.service;

import com.myshipment.model.PackListParams;
import com.myshipment.model.PackingListReportOutputDto;

public interface IPackingListReport {
	public PackingListReportOutputDto getPackingListReportDataService(PackListParams packListParams);

}
