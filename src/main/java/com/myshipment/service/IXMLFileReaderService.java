package com.myshipment.service;

import java.util.List;

import com.myshipment.xmlpobean.CarfourPOXMLBean;

/*
 * @Ranjeet Kumar
 */
public interface IXMLFileReaderService {

	public void processXMLData(List<CarfourPOXMLBean> carfourPOXMLBeanLst);
}
