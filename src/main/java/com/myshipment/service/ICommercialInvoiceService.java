package com.myshipment.service;

import com.myshipment.model.CommercialInvoiceDataForPage;
import com.myshipment.model.CommercialInvoiceJsonOutputData;
/*
 * @Ranjeet Kumar
 */
public interface ICommercialInvoiceService {

	public CommercialInvoiceDataForPage prepareData(CommercialInvoiceJsonOutputData commercialInvoiceJsonOutputData);
}
