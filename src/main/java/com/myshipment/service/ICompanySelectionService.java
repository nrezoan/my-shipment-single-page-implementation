/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.List;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DefaultCompany;
import com.myshipment.model.IntelligentCompanyPercentageUsage;
import com.myshipment.model.IntelligentCompanyUsage;

public interface ICompanySelectionService {

	public List<IntelligentCompanyUsage> getCompanyUsageList(String userCode) throws Exception;
	public List<IntelligentCompanyPercentageUsage> getCompanyUsagePercentageList(LoginDTO loginDTO);
	public DefaultCompany getDefaultCompany(String userCode) throws Exception;
	public DefaultCompany getActiveDefaultCompany(String userCode) throws Exception;
	public DefaultCompany getDefaultCompanyResult(LoginDTO loginDTO);
	public int saveDefaultCompany(DefaultCompany defaultCompany) throws Exception;
	public int activateDefaultCompany(DefaultCompany defaultCompany) throws Exception;
	
}
