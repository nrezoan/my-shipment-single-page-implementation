package com.myshipment.controller;

import java.util.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.AirBillOfLandingJsonData;
import com.myshipment.model.BapiRet1;
import com.myshipment.model.NegoBillLanding;
import com.myshipment.model.NegoBillLandingI;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingReport;
import com.myshipment.model.seaBillOfLadingPdfGeneration;
import com.myshipment.util.DateUtil;
import com.myshipment.util.NumberToWord;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class SeaBillOfLandingController {

	final static Logger logger = Logger.getLogger(SeaBillOfLandingController.class);

	@Autowired
	private RestService restService;

//	@RequestMapping(value = "/getSeaBillOfLandingData", method = RequestMethod.POST)
//	public String getSeaBillOfLandingDetail(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model, HttpSession session){
//
//		logger.info("Method SeaBillOfLandingController.getSeaBillOfLandingDetail starts.");
//		if(reqParams != null){
//			StringBuffer webServiceUrl = new StringBuffer(RestUtil.SEA_BILL_OF_LANDING);
//			
//			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
//			String customerNo = null;
//			String distChannel=null;
//			String division=null;
//			String salesOrg=null;
//			if(loginDto != null){
//				customerNo = loginDto.getLoggedInUserName();
//				distChannel=loginDto.getDisChnlSelected();
//				division=loginDto.getDivisionSelected();
//				salesOrg=loginDto.getSalesOrgSelected();
//				logger.info("Customer No found as : "+ customerNo);
//			}
//			
//			reqParams.setReq2(salesOrg);
//			reqParams.setReq3(distChannel);
//			reqParams.setReq4(division);
//			reqParams.setReq5(customerNo);
//			
//			AirBillOfLandingJsonData seaBillOfLandingJsonData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams, AirBillOfLandingJsonData.class);				
//			ObjectMapper mapper=new ObjectMapper();
//			try{
//			System.out.println(mapper.writeValueAsString(seaBillOfLandingJsonData));
//			}catch(Exception ex){
//				ex.printStackTrace();
//			}
//			if(seaBillOfLandingJsonData != null){
//				model.addAttribute("seaBillOfLandingJsonData", seaBillOfLandingJsonData);
//				logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail ends.");
//
//				return "seaBillOfLanding";
//			}
//			else{
//				logger.info("Value of seaBillOfLandingJsonData is : " +seaBillOfLandingJsonData);
//			}
//		}
//		return null;
//	}
	
	// nusrat 20.6.2017
	@RequestMapping(value = "/NonNegoBillOfLading", method = RequestMethod.POST)
	public ModelAndView getSeaBillOfLandingDetail(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model,
			HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) throws Exception {

		try {
			logger.info("Method SeaBillOfLandingController.getSeaBillOfLandingDetail starts.");
			if (reqParams != null) {
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.SEA_BILL_OF_LANDING);

				LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
				String customerNo = null;
				String distChannel = null;
				String division = null;
				String salesOrg = null;
				if (loginDto != null) {
					customerNo = loginDto.getLoggedInUserName();
					distChannel = loginDto.getDisChnlSelected();
					division = loginDto.getDivisionSelected();
					salesOrg = loginDto.getSalesOrgSelected();
					logger.info("Customer No found as : " + customerNo);
				}

				reqParams.setReq1(reqParams.getReq1().toUpperCase());
				reqParams.setReq2(salesOrg);
				reqParams.setReq3(distChannel);
				reqParams.setReq4(division);
				reqParams.setReq5(customerNo);

				AirBillOfLandingJsonData seaBillOfLandingJsonData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams,
						AirBillOfLandingJsonData.class);
				
				/*if(seaBillOfLandingJsonData.getItHeader().isEmpty() && seaBillOfLandingJsonData.getItShipper().isEmpty() &&
						seaBillOfLandingJsonData.getItConsignee().isEmpty() && seaBillOfLandingJsonData.getItAddress().isEmpty()) {
					modelAndView.setViewName("redirect:/getSeaBillOfLandingPage");
					redirecAttributes.addFlashAttribute("message", "HBL number is not valid! Please enter a valid HBL number");
					return modelAndView;
				}*/
				
				if (seaBillOfLandingJsonData.getBapiReturn1() != null) {
					if (seaBillOfLandingJsonData.getBapiReturn1().getType().equalsIgnoreCase("E")) {
						BapiRet1 bapiRetSeaNonNego = seaBillOfLandingJsonData.getBapiReturn1();
						modelAndView.setViewName("redirect:/getSeaBillOfLandingPage");
						redirecAttributes.addFlashAttribute("message", bapiRetSeaNonNego.getMessage());
						return modelAndView;
					}
				}
				
				ObjectMapper mapper = new ObjectMapper();
				try {
					mapper.writeValueAsString(seaBillOfLandingJsonData);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				String reportType = request.getParameter("type");
				if(salesOrg.equalsIgnoreCase("2200")) {
					reportType = reportType == null ? "seaNNBBNX" : reportType;
				}
				else {
					reportType = reportType == null ? "seaNNB" : reportType;
				}
				seaBillOfLadingPdfGeneration pdfData = new seaBillOfLadingPdfGeneration();
				List<seaBillOfLadingPdfGeneration> actualPdfData = new ArrayList<seaBillOfLadingPdfGeneration>();
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				List<NegoBillLanding> tabledata = seaBillOfLandingJsonData.getItHeader();
				List<NegoBillLandingI> tabledata2 = seaBillOfLandingJsonData.getItItems();
				List<ShippingReport> tabledata3 = seaBillOfLandingJsonData.getItAddress();
				List<NegoBillLanding> tabledata4 = seaBillOfLandingJsonData.getItShipper();
				List<NegoBillLanding> tabledata5 = seaBillOfLandingJsonData.getItConsignee();
				List<NegoBillLanding> tabledata6 = seaBillOfLandingJsonData.getItShipperCountry();
				List<NegoBillLanding> tabledata7 = seaBillOfLandingJsonData.getItConsigneeCountry();
				List<NegoBillLanding> tabledata8 = seaBillOfLandingJsonData.getItNotify();
				List<NegoBillLanding> tabledata9 = seaBillOfLandingJsonData.getItNotifyCountry();
				List<NegoBillLanding> tabledata10 = seaBillOfLandingJsonData.getItPor();
				List<NegoBillLanding> tabledata11 = seaBillOfLandingJsonData.getItPod();
				List<NegoBillLanding> tabledata12 = seaBillOfLandingJsonData.getItPol();
				List<NegoBillLanding> tabledata13 = seaBillOfLandingJsonData.getItPolDel();
				List<NegoBillLanding> tabledata14 = seaBillOfLandingJsonData.getItCountryOfOrig();
				List<NegoBillLanding> tabledata15 = seaBillOfLandingJsonData.getItShipperCountry();
				List<NegoBillLanding> tabledata16 = seaBillOfLandingJsonData.getItDelevery();
				List<NegoBillLanding> tabledata17 = seaBillOfLandingJsonData.getItCompCountry();
				List<NegoBillLanding> tabledata18 = seaBillOfLandingJsonData.getItDeleveryCountry();
				List<NegoBillLanding> tabledata19 = seaBillOfLandingJsonData.getItPlaceOfIssue();
				List<NegoBillLanding> tabledata20 = seaBillOfLandingJsonData.getItTsp();
				List<NegoBillLandingI> tabledata21 = seaBillOfLandingJsonData.getItVbap();

				String text_shipmark, text_desc;
				NegoBillLanding a, a1, a2, a3, a4, a5, a6, a7, a8, a9, b, b1, b2, b3, b4, b6, b9, b20;
				NegoBillLandingI b21;
				ShippingReport b5;
				
				SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				isoFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
				pdfData.setDate(isoFormat.format(date));
				
				NumberToWord obj = new NumberToWord();
				

				if (tabledata != null && !tabledata.isEmpty()) {
					a = tabledata.get(0);// header
					text_shipmark = a.getDesc_Z112().replaceAll("\\t+", "");
					text_desc = a.getDesc_Z023().replaceAll("\\t+", "");
					text_shipmark = text_shipmark.replaceAll("\\n+", " ");
					text_desc = text_desc.replaceAll("\\n+", " ");
					text_shipmark = text_shipmark.replaceAll("\\r+", " ");
					text_desc = text_desc.replaceAll("\\r+", " ");
					pdfData.setMarksNnumber(text_shipmark);
					pdfData.setDescription(text_desc);
					pdfData.setPreCarrige(a.getZzAirlineName1());
					pdfData.setVessel(a.getZzAirlineNo1());
					pdfData.setBillOfLadingNo(a.getZzHblHawbNo());
					pdfData.setInvoiceNo(a.getZzCommInvNo());
					pdfData.setType(a.getZzLcpottNoType());
					pdfData.setNo(a.getZzLcpotNo());
					pdfData.setExpNo(a.getZzExpNo());
					pdfData.setShippingBilNo(a.getZzShipping_Bl_No());
					pdfData.setDepartureDate(epochTimeToDate(a.getZzDepartureDt()));
					pdfData.setShippingAirlineName(a.getZzAirlineName1());
					pdfData.setShippingAirlineNo(a.getZzAirlineNo1()); // description
					pdfData.setMvsl(a.getZzAirline());
					pdfData.setMvsl1(a.getZzAirlineNo2());
					pdfData.setFreightMode(a.getZzFreightModes());
					pdfData.setDate1(epochTimeToDate(a.getZzCommInvDt()));
					pdfData.setDate2(epochTimeToDate(a.getZzLcdt()));
					pdfData.setDate3(epochTimeToDate(a.getZzExpDt()));
					pdfData.setDate4(DateUtil.formatDateToString(a.getZzShipping_Bl_Dt()));
					pdfData.setDateOfissue(epochTimeToDate(a.getZzDepartureDt()));
				}
				if (tabledata4 != null && !tabledata4.isEmpty()) {
					a1 = tabledata4.get(0);// shipper
					pdfData.setShipperName(a1.getName1());
					pdfData.setAddress1(a1.getName2());
					pdfData.setAddress2(a1.getName3());
					pdfData.setAddress3(a1.getName4());
					pdfData.setShipperCity(a1.getCity1());
				}
				if (tabledata5 != null && !tabledata5.isEmpty()) {
					a2 = tabledata5.get(0);// consignee
					pdfData.setConsigneeName(a2.getName1());
					pdfData.setConsigneeAd1(a2.getName2());
					pdfData.setConsigneeAd2(a2.getName3());
					pdfData.setConsigneeAd3(a2.getName4());
					pdfData.setCongineeCity(a2.getCity1());
				}
				if (tabledata6 != null && !tabledata6.isEmpty()) {
					a3 = tabledata6.get(0);// shippercountry
					pdfData.setCountry(a3.getLandX());
				}
				if (tabledata7 != null && !tabledata7.isEmpty()) {
					a4 = tabledata7.get(0);// consignercountry
					pdfData.setConsigneeCountry(a4.getLandX());
				}
				if (tabledata8 != null && !tabledata8.isEmpty()) {
					a5 = tabledata8.get(0);// notify
					pdfData.setNotify1(a5.getName1());
					pdfData.setNotify2(a5.getName2());
					pdfData.setNotify3(a5.getName3());
					pdfData.setNotify4(a5.getName4());
					pdfData.setNotifyCity(a5.getCity1());
				}
				if (!tabledata9.isEmpty()) {
					a6 = tabledata9.get(0);// notifycountry
					pdfData.setNotifyCountry(a6.getLandX());
				}
				if (tabledata10 != null && !tabledata10.isEmpty()) {
					a7 = tabledata10.get(0);// por
					pdfData.setPlaceOfReceipt(a7.getlGobe());
				}
				if (tabledata11 != null && !tabledata11.isEmpty()) {
					a8 = tabledata11.get(0);// pod
					pdfData.setPortOfDischarge(a8.getZzPortName());
				}
				if (tabledata12 != null && !tabledata12.isEmpty()) {
					a9 = tabledata12.get(0);// pol
					pdfData.setShippingPortName(a9.getZzPortName());
					pdfData.setPortOfLoading(a9.getZzPortName());
				}
				if (tabledata13 != null && !tabledata13.isEmpty()) {
					b = tabledata13.get(0);// poldel
					pdfData.setPlaceOfDelivery(b.getZzPortName());
				}
				if (tabledata14 != null && !tabledata14.isEmpty()) {
					b1 = tabledata14.get(0);// countryOforigin
					pdfData.setCountryOfOrigin(b1.getLandX());
				}
				if (tabledata15 != null && !tabledata15.isEmpty()) {
					b2 = tabledata15.get(0);// shippercountry
					pdfData.setShippingPortCountry(b2.getLandX());
				}
				if (tabledata16 != null && !tabledata16.isEmpty()) {
					b3 = tabledata16.get(0);// delivery
					pdfData.setDelName1(b3.getName1());
					pdfData.setDelName2(b3.getName2());
					pdfData.setDelName3(b3.getName3());
					pdfData.setDelName4(b3.getName4());
					pdfData.setDelCity(b3.getCity1());
				}
				if (tabledata18 != null && !tabledata18.isEmpty()) {
					b4 = tabledata18.get(0);// deliverycountry
					pdfData.setDelCountry(b4.getLandX());
				}
				if (tabledata17 != null && !tabledata17.isEmpty()) {
					b6 = tabledata17.get(0);// companycountry
					pdfData.setComCountry(b6.getLandX());
				}
				if (tabledata19 != null && !tabledata19.isEmpty()) {
					b9 = tabledata19.get(0);// placeofissue
					pdfData.setPlaceOfissue(b9.getCity1());
				}
				if (tabledata3 != null && !tabledata3.isEmpty()) {
					b5 = tabledata3.get(0);// address
					pdfData.setCompany(b5.getName1());
					pdfData.setComAdd1(b5.getName2());
					pdfData.setComAdd2(b5.getName3());
					pdfData.setComAdd3(b5.getName4());
					pdfData.setComCity(b5.getCity1());
				}
				if (tabledata20 != null && !tabledata20.isEmpty()) {
					b20 = tabledata20.get(0);
					pdfData.setTransPort(b20.getZzPortName());
				}
				
/*				String HSCODE = null;
				if(tabledata21 != null && !tabledata21.isEmpty()) {
					for(int c=0;c < tabledata12.size(); c++) {
						b21 = tabledata21.get(c);
						HSCODE= HSCODE+ "," + b21.getZzHsCode();
					}
					
					pdfData.setHsCode(HSCODE);
				}*/


				String s = seaBillOfLandingJsonData.getTotalQuantity();
				Double i1 = Double.parseDouble(s);
				pdfData.setPacakgeNo(i1.intValue());
				pdfData.setCtnWords(obj.convertNumberToWords(i1.intValue()));
				pdfData.setGrossWeight(round(seaBillOfLandingJsonData.getTotalAmount()));
				pdfData.setMeasurment(seaBillOfLandingJsonData.getTotalCbm());
				actualPdfData.add(pdfData);

				NegoBillLanding headertable = new NegoBillLanding();
				if (tabledata != null && !tabledata.isEmpty()) {
					headertable = tabledata.get(0);// header
				} else
					headertable.setZzMode_Shipment("");

				if (tabledata2 != null && !tabledata2.isEmpty()) {
					for (int w = 0; w < tabledata2.size(); w++) {
						NegoBillLandingI table = tabledata2.get(w);
						seaBillOfLadingPdfGeneration pdfTable = new seaBillOfLadingPdfGeneration();
						pdfTable.setMode(headertable.getZzMode_Shipment());
						pdfTable.setContainerNo(table.getVhilm());
						pdfTable.setSealNo(table.getVhilm_ku());
						Double i = Double.parseDouble(table.getVemng());
						pdfTable.setQty(i.intValue());
						pdfTable.setSize(table.getWgbez60());
						//pdfTable.setCbm(round(table.getNtvol()));
						pdfTable.setCbm(table.getNtvol());
						pdfTable.setKgs(round(table.getNtGew()));
						actualPdfData.add(pdfTable);
					}
				}

				JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
				parameterMap.put("datasource", pdfDataSource);
				ServletContext context = request.getSession().getServletContext();
				String path = context.getRealPath("/") + "";
				parameterMap.put("Context", path);
				modelAndView = new ModelAndView(reportType, parameterMap);

				return modelAndView;

			}
			return null;
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			modelAndView.setViewName("redirect:/getSeaBillOfLandingPage");
			redirecAttributes.addFlashAttribute("message", "HBL number is not valid! Please enter a valid HBL number");
			return modelAndView;
		}
	}
	
	@RequestMapping(value="/getSeaBillOfLandingPage", method = RequestMethod.GET )
	public String getSeaBillOfLanding(ModelMap model, HttpSession session) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		RequestParams reqParams = new RequestParams();
		model.addAttribute("reqParams", reqParams);
		
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		
		String division = loginDto.getDivisionSelected();
		
		if(division.equals("AR")) {
			return "redirect:/getAirBillOfLandingPage";
		} else if(division.equals("SE")) {
			return "seaBillOfLandingSearch";
		}
		
		return "seaBillOfLandingSearch";

	}
	
	//hamid
	//22.03.2017
	@RequestMapping(value = "/getSeaBillOfLandingFromUrl", method = RequestMethod.GET)
	public ModelAndView getSeaBillOfLandingDetailFromUrl(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model, HttpSession session, @RequestParam("searchString") String searchValue,
			HttpServletResponse response,HttpServletRequest request, ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result) throws Exception{

		logger.info("Method SeaBillOfLandingController.getSeaBillOfLandingDetail starts.");
		if(reqParams != null){
			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
			String customerNo = null;
			String distChannel=null;
			String division=null;
			String salesOrg=null;
			if(loginDto != null){
				customerNo = loginDto.getLoggedInUserName();
				distChannel=loginDto.getDisChnlSelected();
				division=loginDto.getDivisionSelected();
				salesOrg=loginDto.getSalesOrgSelected();
				logger.info("Customer No found as : "+ customerNo);
			}
			
			reqParams.setReq1(searchValue);
			reqParams.setReq2(salesOrg);
			reqParams.setReq3(distChannel);
			reqParams.setReq4(division);
			reqParams.setReq5(customerNo);
			return getSeaBillOfLandingDetail(reqParams, model, session, response, request, modelAndView, redirecAttributes, result);
			
/*			AirBillOfLandingJsonData seaBillOfLandingJsonData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams, AirBillOfLandingJsonData.class);				
			ObjectMapper mapper=new ObjectMapper();
			try{
				System.out.println(mapper.writeValueAsString(seaBillOfLandingJsonData));
			}catch(Exception ex){
				ex.printStackTrace();
			}
/*			if(seaBillOfLandingJsonData != null){
				model.addAttribute("seaBillOfLandingJsonData", seaBillOfLandingJsonData);
				logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail ends.");

				return "seaBillOfLanding";
			}
			else{
				logger.info("Value of seaBillOfLandingJsonData is : " +seaBillOfLandingJsonData);
			}*/
		}
		return null;
	}
	
	private String epochTimeToDate(String epochString) {
		String dateReturn = "";
		long epoch;
		if(epochString != null) {
			epoch = Long.parseLong(epochString);
        	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        	dateReturn = sdf.format(new Date(epoch));
		}
		return dateReturn;
	}
	private String round(String df)
	{
		DecimalFormat r = new DecimalFormat("0.00");
		Double d=Double.valueOf(df);
		String s=r.format(d);
		return s;
	}
	
}
