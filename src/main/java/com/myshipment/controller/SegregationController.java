package com.myshipment.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.dto.SegregationDTO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.service.ISegregationService;

/*
 * @ Ranjeet Kumar
 */
@Controller
public class SegregationController {


	private static final Logger logger = Logger.getLogger(SegregationController.class);
	
	@Autowired
	private ISegregationService segregationService;
	
	@RequestMapping(value="/getSegregationPage", method=RequestMethod.GET)
	public String getSegregationPage(ModelMap model, HttpSession httpSession, HttpServletRequest request, HttpServletResponse response)
	{
		
		logger.info("Method SegregationController.getSegregationPage starts.");
		
		String segId = request.getParameter("seg_id");
		int segregationId = Integer.parseInt(segId);
		String paramPoNo = request.getParameter("param1");
		String paramFromDate = request.getParameter("param2");
		String paramToDate = request.getParameter("param3");
		//SegregationDTO segregationDTO = new SegregationDTO();
		SegPurchaseOrder segPurchaseOrder =  segregationService.getLineItemForSegregation(segregationId);
		SegregationDTO segregationDTO = prepareSegregationDtoData(segPurchaseOrder);
		//new starts
		segregationDTO.setParamPoNo(paramPoNo);
		segregationDTO.setParamFromDate(paramFromDate);
		segregationDTO.setParamToDate(paramToDate);
		//new ends
		model.put("segregationDTO", segregationDTO);
		
		logger.info("Method SegregationController.getSegregationPage ends.");
		
		return "Segregation";
	}
	
	@RequestMapping(value = "/doSegregation", method = RequestMethod.POST)
	public String createManualPO(@ModelAttribute("segregationDTO") SegregationDTO segregationDTO, RedirectAttributes redirectAttributes, HttpSession session, ModelMap model)
	{

		logger.info("Method SegregationController.createManualPO starts.");
		
		//Prepare the data for redirection
		SearchPoDTO searchPoDTO = new SearchPoDTO();
		searchPoDTO.setFromDate(segregationDTO.getParamFromDate());
		searchPoDTO.setPoNo(segregationDTO.getParamPoNo());
		searchPoDTO.setToDate(segregationDTO.getParamToDate());
		searchPoDTO.setRedirectedRequest("yes");

		//Do validation
		String msg = checkForValidSegregationRequest(segregationDTO);
		if(msg != null && !msg.isEmpty()){
			model.addAttribute("error", msg);
			model.put("segregationDTO", segregationDTO);
			return "Segregation";
		}
		boolean checkQuantity = quantityValidation(segregationDTO);
		if(checkQuantity)
		{
			model.addAttribute("error", "Sum of total pieces of line item data is greater than or less than the original quantity!");
			model.put("segregationDTO", segregationDTO);
			return "Segregation";
		}
		//Do segregation
		int status = segregationService.doSegregation(segregationDTO);
		//Redirect to search page
		redirectAttributes.addFlashAttribute("searchPoDTO", searchPoDTO);
		if(status == 1){
			redirectAttributes.addFlashAttribute("message", "Segregation Successful.");
		}
		if(status == 0){
			redirectAttributes.addFlashAttribute("message", "Segregation Unsuccessful.");
		}

		logger.info("Method SegregationController.createManualPO ends.");
		
		return "redirect:/getSearchPoPage";
	}
	
/*	@ModelAttribute("buyers")
    public Map<String, String> getBuyers() {
        Map<String, String> buyerLst = new HashMap<String, String>();
        buyerLst.put("ABC", "ABC");
        buyerLst.put("XYZ", "XYZ");
        

        return buyerLst;
    }
	
	@ModelAttribute("buyerHouses")
    public Map<String, String> getBuyerHouse() {
        Map<String, String> buyerLst = new HashMap<String, String>();
        buyerLst.put("ABC", "ABC");
        buyerLst.put("XYZ", "XYZ");
        

        return buyerLst;
    }
	
	@ModelAttribute("compCodes")
    public Map<String, String> getCompanyCodes() {
        Map<String, String> buyerLst = new HashMap<String, String>();
        buyerLst.put("AAA", "AAA");
        buyerLst.put("XXX", "XXX");
        

        return buyerLst;
    }
*/	
	private SegregationDTO prepareSegregationDtoData(SegPurchaseOrder segPurchaseOrder)
	{
		SegregationDTO segregationDTO = new SegregationDTO();
		segregationDTO.setVc_po_no(segPurchaseOrder.getVc_po_no());
		//segregationDTO.setSales_org(segPurchaseOrder.getSales_org());		
		//segregationDTO.setNu_client_code(segPurchaseOrder.getNu_client_code());
		segregationDTO.setVc_product_no(segPurchaseOrder.getVc_product_no());
		segregationDTO.setNu_no_pcs_ctns(segPurchaseOrder.getNu_no_pcs_ctns());
		segregationDTO.setVc_color(segPurchaseOrder.getVc_color());
		segregationDTO.setVc_hs_code(segPurchaseOrder.getVc_hs_code());
		segregationDTO.setVc_size(segPurchaseOrder.getVc_size());
		segregationDTO.setVc_sku_no(segPurchaseOrder.getVc_sku_no());
		segregationDTO.setVc_tot_pcs(segPurchaseOrder.getVc_tot_pcs());
		segregationDTO.setVc_style_no(segPurchaseOrder.getVc_style_no());
		segregationDTO.setPo_id(segPurchaseOrder.getPo_id());
		segregationDTO.setSeg_id(segPurchaseOrder.getSeg_id());
		List<SegPurchaseOrder> segPurchaseOrders = new ArrayList<SegPurchaseOrder>();
		segPurchaseOrders.add(segPurchaseOrder);
		segregationDTO.setDynamicData(segPurchaseOrders);
		
		
		return segregationDTO;
	}
	

	private boolean quantityValidation(SegregationDTO segregationDTO)
	{
		if(segregationDTO != null){
			List<SegPurchaseOrder> dataLst = segregationDTO.getDynamicData();
			double quantity = 0;
			for(SegPurchaseOrder segPurchaseOrder : dataLst)
			{
				quantity = quantity + segPurchaseOrder.getVc_tot_pcs();
			}
			if(quantity != segregationDTO.getVc_tot_pcs())
			{
				return true;
			}
			
		}
		return false;
	}
	
	private String checkForValidSegregationRequest(SegregationDTO segregationDTO){

		String message = "";
		if(segregationDTO != null){
			List<SegPurchaseOrder> dataLst = segregationDTO.getDynamicData();
			if(dataLst != null && dataLst.size() == 1){
				if(dataLst.get(0).getVc_tot_pcs() == 0){
					message = "Not a valid segregation request.";
				}
				if(dataLst.get(0).getVc_tot_pcs() != 0){
					message = "Not a valid segregation request. You can better choose update action.";
				}
			}
		}

		return message;
	}
}
