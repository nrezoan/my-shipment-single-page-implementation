package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.CargoAckJasper;
import com.myshipment.model.CargoAckReportJsonData;
import com.myshipment.model.CargoItem;
import com.myshipment.model.CargoItemHeader;
import com.myshipment.model.ReportParams;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingReport;
import com.myshipment.service.ICargoAckService;
import com.myshipment.util.DateUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * @author Gufranur Rahman
 *
 */
@Controller
public class CargoAckController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private ICargoAckService cargoAckService;
	
	private Logger logger = Logger.getLogger(CargoAckController.class);

	
	@RequestMapping(value="/getCargoAckPage", method = RequestMethod.GET )
	public String getCargoAckPage(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		RequestParams req = new RequestParams();
		model.addAttribute("req", req);
		return "cargoAckInput";

	}

	@RequestMapping(value = "/cargoAcknowledgementCertificate",params = "btn-appr-po-search", method = RequestMethod.POST)
	public String viewCargoAck(@ModelAttribute("req") RequestParams req, Model model, HttpSession session) { 
			
			
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		CargoAckReportJsonData cargoAckJson = null;
		ReportParams rParam=new ReportParams();
		rParam.setCustomer(req.getReq5());
		rParam.setDistChannel(req.getReq3());
		rParam.setDivision(req.getReq4());
		rParam.setHblNumber(req.getReq1());
		rParam.setInvType("");
		rParam.setSaleOrg(req.getReq2());
		
		cargoAckJson = cargoAckService.getCargoAckDetail(rParam);
		model.addAttribute("cargoAckJson", cargoAckJson);
		
		return "cargoAckDetails";
	}
	
	@RequestMapping(value = "/cargoAcknowledgementCertificate",params = "download", method = RequestMethod.POST)
	public ModelAndView getCargoAck(@ModelAttribute("req") RequestParams req, Model model,  HttpSession session,HttpServletResponse response,HttpServletRequest request,
			ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result) { 
			
			
		try {
			logger.debug(this.getClass().getName() + "performLogin():  Start ");
			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
			String customerNo = null;
			String distChannel=null;
			String division=null;
			String salesOrg=null;
			if(loginDto != null){
				customerNo = loginDto.getLoggedInUserName();
				distChannel=loginDto.getDisChnlSelected();
				division=loginDto.getDivisionSelected();
				salesOrg=loginDto.getSalesOrgSelected();
				logger.info("Customer No found as : "+ customerNo);
			}
			
			req.setReq2(salesOrg);
			req.setReq3(distChannel);
			req.setReq4(division);
			req.setReq5(customerNo);
			
			logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
					+ ":" + req.getReq2());
			CargoAckReportJsonData cargoAckJson = null;
			
			String reportType = request.getParameter("type");
			reportType = reportType == null ? "cargoAck" : reportType;

			Map<String, Object> parameterMap = new HashMap<String, Object>();
			
			ReportParams rParam=new ReportParams();
			rParam.setCustomer(req.getReq5());
			rParam.setDistChannel(req.getReq3());
			rParam.setDivision(req.getReq4());
			rParam.setHblNumber(req.getReq1());
			rParam.setInvType("");
			rParam.setSaleOrg(req.getReq2());
			
			cargoAckJson = cargoAckService.getCargoAckDetail(rParam);
			List<CargoAckJasper> actualPDFData = new ArrayList<CargoAckJasper>();
			CargoAckJasper pdfReport = new CargoAckJasper();
			
			List<CargoItemHeader> headerList = cargoAckJson.getItHeaderList();
			List<CargoItem> itemList = cargoAckJson.getItItemList();
			List<ShippingReport> addressList = cargoAckJson.getItAddList();
			
			CargoItemHeader header = headerList.get(0);

			ShippingReport address = addressList.get(0);
			
			pdfReport.setName1(address.getName1());
			pdfReport.setName2(address.getName2());
			pdfReport.setName3(address.getName3());
			pdfReport.setName4(address.getName4());
			pdfReport.setZzHblHawbNo(header.getZzHblHawbNo());
			pdfReport.setZzCommInvNo(header.getZzCommInvNo());
			pdfReport.setBuDat(DateUtil.formatDateToString(header.getBuDat()));
			pdfReport.setZzSoQuantity(header.getZzSoQuantity());
			actualPDFData.add(pdfReport);	
			
			for(int i = 0;i<itemList.size();i++) {
				
				CargoItem item = itemList.get(i);
				CargoAckJasper itemPdf = new CargoAckJasper();
				itemPdf.setMenge(item.getMenge());
				itemPdf.setZzPoNumber(item.getZzPoNumber());
				itemPdf.setPoSnr(item.getPoSnr());
				actualPDFData.add(itemPdf);
			}
			
			JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);    
			parameterMap.put("datasource", pdfDataSource);
			modelAndView = new ModelAndView(reportType, parameterMap);
			
			
			
			
			return modelAndView;
		} 	catch(IndexOutOfBoundsException ex)
		{
			 modelAndView.setViewName("redirect:/getCargoAckPage");
	    	 redirecAttributes.addFlashAttribute("message","No Records Found");
	    	 return modelAndView;
			
		}
	}
	
	
	//hamid
	//22.03.2017
	@RequestMapping(value = "/cargoAcknowledgementFromUrl", method = RequestMethod.GET)
	public String getCargoAckFromUrl(@ModelAttribute("req") RequestParams req, Model model, HttpSession session, @RequestParam("searchString") String searchValue) { 
			
			
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		
		req.setReq1(searchValue);
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		CargoAckReportJsonData cargoAckJson = null;
		ReportParams rParam=new ReportParams();
		rParam.setCustomer(req.getReq5());
		rParam.setDistChannel(req.getReq3());
		rParam.setDivision(req.getReq4());
		rParam.setHblNumber(req.getReq1());
		rParam.setInvType("");
		rParam.setSaleOrg(req.getReq2());
		
		cargoAckJson = cargoAckService.getCargoAckDetail(rParam);
		model.addAttribute("cargoAckJson", cargoAckJson);
		
		return "cargoAckDetails";
	}


	
	
	
}
