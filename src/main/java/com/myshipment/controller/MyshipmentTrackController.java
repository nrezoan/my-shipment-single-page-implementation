package com.myshipment.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BuSupListBean;
import com.myshipment.model.MyshipmentTrackContainerBean;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.MyshipmentTrackItemBean;
import com.myshipment.model.MyshipmentTrackJsonOutputData;
import com.myshipment.model.MyshipmentTrackParams;
import com.myshipment.model.MyshipmentTrackScheduleBean;
import com.myshipment.model.SupRequestParams;
import com.myshipment.model.SupplierPreLoadJsonData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

/*
 * @Hamid
 */
@Controller
public class MyshipmentTrackController extends BaseController {

	private static final long serialVersionUID = -3438900313595402920L;
	final static Logger logger = Logger.getLogger(MyshipmentTrackController.class);

	@Autowired
	private RestService restService;

	
	@RequestMapping(value = "/getMyshipmentTrackPage", method = RequestMethod.GET)
	public String getMyshipmentTrackSearchPage(Model model, HttpServletRequest request) {
		logger.info("Method MyshipmentTrackController.getMyshipmentTrackPage starts");

		try {
			MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
			myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
			myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
			myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());
			myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
			myshipmentTrackParams.setAction("01");
			myshipmentTrackParams.setDoc_no("GFL001550");
			// myshipmentTrackParams.setErdat1("");
			// myshipmentTrackParams.setErdat2("");
			logger.info("Method MyshipmentTrackController.getMyshipmentTrackPage ends");
			return showShipmentDetailDataByHbl(myshipmentTrackParams, model, request);
		} catch (Exception ex) {
			ex.printStackTrace();
			return "lastNShipment";
		}
	}

	@RequestMapping(value = "/trackshipmentbyhbl", method = RequestMethod.GET)
	public String getTrackShipmentByHbl(ModelMap model) {

		logger.info("Method MyshipmentTrackController.getTrackShipmentByHbl starts");

		MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
		model.addAttribute("myshipmentTrackParams", myshipmentTrackParams);

		logger.info("Method MyshipmentTrackController.getTrackShipmentByHbl ends");

		return "myshipmentTrackByHblPage";
	}
	
	@RequestMapping(value = "/shipmentdetaildatabyhbl", method = RequestMethod.POST)
	public String showShipmentDetailDataByHbl(
			@ModelAttribute("myshipmentTrackParams") MyshipmentTrackParams myshipmentTrackParams, Model model,
			HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.showShipmentDetailDataByHbl starts");

		MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = new MyshipmentTrackJsonOutputData();
		if (myshipmentTrackParams != null) {
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
			myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
			myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
			myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());
			myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
			//myshipmentTrackParams.setErdat1("");
			//myshipmentTrackParams.setErdat2("");
			if (loginDto.getDisChnlSelected().equalsIgnoreCase("EX")
					&& loginDto.getDivisionSelected().equalsIgnoreCase("SE")) {
				myshipmentTrackParams.setAction("01");
			} else if (loginDto.getDisChnlSelected().equalsIgnoreCase("IM")
					&& loginDto.getDivisionSelected().equalsIgnoreCase("SE")) {
				myshipmentTrackParams.setAction("02");
			} else if (loginDto.getDisChnlSelected().equalsIgnoreCase("EX")
					&& loginDto.getDivisionSelected().equalsIgnoreCase("AR")) {
				myshipmentTrackParams.setAction("03");
			} else if (loginDto.getDisChnlSelected().equalsIgnoreCase("IM")
					&& loginDto.getDivisionSelected().equalsIgnoreCase("AR")) {
				myshipmentTrackParams.setAction("04");
			} else {
				myshipmentTrackParams.setAction("01"); // default action set to
														// sea export
			}

			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
			try {
				myshipmentTrackJsonOutputData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
						MyshipmentTrackJsonOutputData.class);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("FAILURE")) {
				logger.info("myshipmentTrackJsonOutputData was returned null, MyshipmentTrackController.getMyshipmentTrackDetail Method");
				model.addAttribute("errorDialogue", myshipmentTrackJsonOutputData.getDialogue());
				return "myshipmentTrackByHblPage";
			} else if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("SUCCESS")) {
				// initialization
				List<MyshipmentTrackHeaderBean> myshipTrackHeaderList = new ArrayList<MyshipmentTrackHeaderBean>();
				List<MyshipmentTrackItemBean> myshipTrackItemList = new ArrayList<MyshipmentTrackItemBean>();
				List<MyshipmentTrackContainerBean> myshipTrackContainerList = new ArrayList<MyshipmentTrackContainerBean>();
				List<MyshipmentTrackScheduleBean> myshipTrackScheduleList = new ArrayList<MyshipmentTrackScheduleBean>();

				if (myshipmentTrackJsonOutputData != null) {
					if (myshipmentTrackJsonOutputData.getMyshipmentTrackHeader() != null) {
						myshipTrackHeaderList = myshipmentTrackJsonOutputData.getMyshipmentTrackHeader();
					}
					if (myshipmentTrackJsonOutputData.getMyshipmentTrackItem() != null) {
						myshipTrackItemList = myshipmentTrackJsonOutputData.getMyshipmentTrackItem();
					}
					if (myshipmentTrackJsonOutputData.getMyshipmentTrackContainer() != null) {
						myshipTrackContainerList = myshipmentTrackJsonOutputData.getMyshipmentTrackContainer();
					}
					if (myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule() != null) {
						myshipTrackScheduleList = myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule();
					}
					
					model.addAttribute("myshipmentTrackParams", myshipmentTrackParams);
					model.addAttribute("myshipTrackHeaderList", myshipTrackHeaderList);
					model.addAttribute("myshipTrackItemList", myshipTrackItemList);
					model.addAttribute("myshipTrackContainerList", myshipTrackContainerList);
					model.addAttribute("myshipTrackScheduleList", myshipTrackScheduleList);
					
					model.addAttribute("companyCode", loginDto.getSalesOrgSelected());

					logger.info("Method MyshipmentTrackController.showShipmentDetailDataByHbl ends");
					return "myshipmentTrackDetailsByHbl";
				}
			}
			else {
				logger.info("myshipmentTrackJsonOutputData was returned null, MyshipmentTrackController.getMyshipmentTrackDetail Method");
				return "myshipmentTrackDetailsByHbl";
			}
		}
		return null;
	}
	
/*	@RequestMapping(value = "/trackshipmentbydate", method = RequestMethod.GET)
	public String getTrackShipmentByDate(ModelMap model, HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.getTrackShipmentByDate starts");
		LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		buyersList = getBuyersList(loginDto);
		
		//MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
		SAPPOSearchParams sapposearchparams = new SAPPOSearchParams();
		model.addAttribute("sapposearchparams", sapposearchparams);
		model.addAttribute("buyersList", buyersList);
		
		logger.info("Method MyshipmentTrackController.getTrackShipmentByDate ends");

		return "myshipmentTrackByDatePage";
	}*/	
	
	//test buyer list populate
	public Map<String, String> getBuyersList(LoginDTO loginDto) {		
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		SupplierPreLoadJsonData supplierpreloadjsondata = new SupplierPreLoadJsonData();
		SupRequestParams supreqparams = new SupRequestParams();
		supreqparams.setCustomerId(loginDto.getLoggedInUserName());
		supreqparams.setDistChan(loginDto.getDisChnlSelected());
		supreqparams.setDivision(loginDto.getDivisionSelected());
		supreqparams.setSalesOrg(loginDto.getSalesOrgSelected());
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.SUPPLIER_DETAILS);
		try {
			supplierpreloadjsondata = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), 
					supreqparams, SupplierPreLoadJsonData.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if(supplierpreloadjsondata != null) {
			List<BuSupListBean> partnerList = supplierpreloadjsondata.getBuSupList();
			if((partnerList != null) && !partnerList.isEmpty()) {
				for(BuSupListBean bslb : partnerList) {
					if(bslb.getZzParvw().equalsIgnoreCase("WE"))
						buyersList.put(bslb.getZzCust(), bslb.getZzCustName());
				}
			}
		}
		return buyersList;
	}	
	
	public MyshipmentTrackHeaderBean getHeaderFromList(List<MyshipmentTrackHeaderBean> myshipTrackHeadList, String bl_no) {
		MyshipmentTrackHeaderBean myshipmentTrackHeaderBean = new MyshipmentTrackHeaderBean();
		for(MyshipmentTrackHeaderBean header : myshipTrackHeadList) {
			if(header.getBl_no().equalsIgnoreCase(bl_no)) {
				myshipmentTrackHeaderBean = header;
				break;
			}
		}
		return myshipmentTrackHeaderBean;
	}
	
	@ModelAttribute("statusTypes")
	public Map<String, String> getCompanyCodes() {
		
		Map<String, String> statusTypes = new LinkedHashMap<String, String>();
		statusTypes.put("PENDING", "PENDING");
		statusTypes.put("APPROVED", "APPROVED");
		statusTypes.put("REJECTED", "REJECTED");
		//statusTypes.put("ALL", "ALL");
		
		return statusTypes;
	}
	
	public Date convertStringToDate(String dateString) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date outputDate;
        try {
            outputDate = formatter.parse(dateString);
            return outputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
	}
	

	
}
