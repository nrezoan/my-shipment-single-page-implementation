package com.myshipment.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myshipment.dto.BuyersSuppliersmap;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.BookingDisplayJsonOutputData;
import com.myshipment.model.BookingDisplayParams;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.HeaderDisplayDTO;
import com.myshipment.model.ItemDTO;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.OrderItem;
import com.myshipment.model.OrderTextDTO;
import com.myshipment.model.PartnerDTO;
import com.myshipment.model.PoBookingPacklistDetailBean;
import com.myshipment.model.SalesOrderJsonOutputData;
import com.myshipment.model.SalesOrderParams;
import com.myshipment.service.IOrderService;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;

/*
 * @Ranjeet Kumar
 */
@Controller
public class BookingDisplayController {

	private static final Logger logger = Logger.getLogger(BookingDisplayController.class);

	@Autowired
	private RestService restService;

	@Autowired
	IOrderService orderService;

	@RequestMapping(value = "/bookingDisplayData", method = { RequestMethod.POST, RequestMethod.GET })

	public String getBookingData(@ModelAttribute("salesOrderParams") SalesOrderParams salesOrderParams, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) {

		logger.info("Method BookingDisplayController.getBookingData starts.");

		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		if (supplierDetailsDTO != null) {
			BuyersSuppliersmap bsm = supplierDetailsDTO.getBuyersSuppliersmap();
			Map<String, String> mapdt = bsm.getDocTypes();
			if (mapdt.containsKey("ZCOR"))
				mapdt.remove("ZCOR");
			bsm.setDocTypes(mapdt);
			supplierDetailsDTO.setBuyersSuppliersmap(bsm);
			// model.addAttribute("supplierDetails", supplierDetailsDTO);
		} else {
			model.addAttribute("supplierDetails", new SupplierDetailsDTO());

		}

		if (salesOrderParams.getBlno() != null && !salesOrderParams.getBlno().equalsIgnoreCase("")) {
			String blUpperCase = salesOrderParams.getBlno().toUpperCase();
			salesOrderParams.setBlno(blUpperCase);
			try {
				// HttpSession session=SessionUtil.getHttpSession(request);
				LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
				if (null != loginDTO) {
					salesOrderParams.setDistChannel(loginDTO.getDisChnlSelected());
					salesOrderParams.setDivision(loginDTO.getDivisionSelected());
					salesOrderParams.setSalesOrg(loginDTO.getSalesOrgSelected());
					salesOrderParams.setPartner(loginDTO.getLoggedInUserName());
				}
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.SO_BY_HBL);
				SalesOrderJsonOutputData salesOrderJsonOutputData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), salesOrderParams,
						SalesOrderJsonOutputData.class);
				if (null != salesOrderJsonOutputData
						&& !salesOrderJsonOutputData.getSalesOrder().equalsIgnoreCase("")) {
					BookingDisplayParams bookingDisplayParams = new BookingDisplayParams();
					bookingDisplayParams.setSalesOrderNumber(salesOrderJsonOutputData.getSalesOrder());
					webServiceUrl = new StringBuffer(RestUtil.BOOKING_DISPLAY);
					BookingDisplayJsonOutputData bookingDisplayJsonOutputData = restService.postForObject(
							RestUtil.prepareUrlForService(webServiceUrl).toString(), bookingDisplayParams,
							BookingDisplayJsonOutputData.class);

					if (bookingDisplayJsonOutputData != null) {
						// HeaderDisplayDTO headerDisplayDTO =
						// bookingDisplayJsonOutputData.getHeaderDisplayDTO();
						// List<OrderTextDTO> headerTextDTOLst =
						// bookingDisplayJsonOutputData.getHeaderTextDtoLst();
						// List<ItemDTO> itemDTOLst =
						// bookingDisplayJsonOutputData.getItemDtoLst();
						// List<OrderTextDTO> itemTextDTO =
						// bookingDisplayJsonOutputData.getItemTextDtoLst();
						// List<PartnerDTO> partnerDTOLSt =
						// bookingDisplayJsonOutputData.getPartnerDtoLst();
						DirectBookingParams directBookingParams = createBookingObject(bookingDisplayJsonOutputData);
						OrderHeader orderHeader = directBookingParams.getOrderHeader();
						orderHeader.setDocNumber(bookingDisplayParams.getSalesOrderNumber());
						directBookingParams.setOrderHeader(orderHeader);
						ObjectMapper objectMapper = new ObjectMapper();

						String jSON = objectMapper.writeValueAsString(directBookingParams);
						List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");

/*						// test purpose
						MaterialsDto testmat = new MaterialsDto();
						testmat.setLabel("AA MATERIAL");
						testmat.setValue("A75");
						MaterialsDto testmat1 = new MaterialsDto();
						testmat1.setLabel("AA SCIENTIFIC EQUIPMENTS");
						testmat1.setValue("A71");
						MaterialsDto testmat2 = new MaterialsDto();
						testmat2.setLabel("AA LABORATORY SUPPLIES");
						testmat2.setValue("A72");
						lstMaterialsDto.add(testmat);
						lstMaterialsDto.add(testmat1);
						lstMaterialsDto.add(testmat2);*/

						// declaring variables
						List<OrderItem> lstOrderItemForDisplay = directBookingParams.getOrderItemLst();
						String grDone = "";
						Integer checkSum = 0;
						Integer curtonQuantity = 0;
						Double totalVolume = 0.0;
						Double grossWeight = 0.0;
						for (OrderItem orderItem2 : lstOrderItemForDisplay) {
							curtonQuantity = curtonQuantity + orderItem2.getCurtonQuantity();
							grossWeight = grossWeight + orderItem2.getGrossWeight();
							totalVolume = totalVolume + orderItem2.getTotalVolume();
							if (orderItem2.getGrFlag() != null && orderItem2.getGrFlag().equals("x"))
								checkSum++;
							else
								orderItem2.setGrFlag("");
						}
						grossWeight = Math.round(grossWeight * 100.0) / 100.0;
						totalVolume = Math.round(totalVolume * 100.0) / 100.0;
						if (checkSum == 0) {
							// goods not received for any line item
							grDone = "show";
						} else {
							// goods received for all/some line items
							grDone = "hide";
						}

						model.addAttribute("totalQuantity", curtonQuantity);
						model.addAttribute("grossWeight", grossWeight);
						model.addAttribute("totalCBM", totalVolume);
						model.addAttribute("grDone", grDone);
						model.addAttribute("materials", lstMaterialsDto);
						model.addAttribute("directBookingParamsJson", jSON);
						model.addAttribute("directBookingParams", directBookingParams);
						session.setAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE, directBookingParams);
						model.addAttribute("supplierDetails", supplierDetailsDTO);

						if (grDone.equals("show")) {
							boolean itemTab = false;
							model.addAttribute("itemTabActive", new Gson().toJson(itemTab));	
							return "booking-update3";
							// return "booking-updateGoodRecieved";

						} else {
							// return "booking-update"; //replace with actual
							boolean itemTab = false;
							model.addAttribute("itemTabActive", new Gson().toJson(itemTab));	
							return "booking-updateGoodRecieved"; // Added by Tahmid Alam - 11 Sept 2017
						}

					} else {
						logger.info("bookingDisplayJsonOutputData value is null.");
						redirectAttributes.addFlashAttribute("displayError", "No Details Found for HBL and SO!");
						return "redirect:getBookingDisplayPage";
					}
				} else {
					redirectAttributes.addFlashAttribute("displayError",
							"No Sales Order Found, Please Provide Valid HBL No.");
					return "redirect:getBookingDisplayPage";
				}

			} catch (Exception e) {
				e.printStackTrace();
				logger.info("Some problem occurred in Method BookingDisplayController.getBookingData " + e);
				return "redirect:getBookingDisplayPage";
			}

		} else {
			redirectAttributes.addFlashAttribute("displayError", "Please provide valid HBL Number");
			return "redirect:getBookingDisplayPage";
		}
	}

	public static DirectBookingParams createBookingObject(BookingDisplayJsonOutputData bookingDisplayJsonOutputData)
			throws IllegalAccessException, InvocationTargetException {
		String soNumber = "";
		String desGoods = "";
		String shipMark = "";
		HeaderDisplayDTO headerDisplayDTO = bookingDisplayJsonOutputData.getHeaderDisplayDTO();
		List<OrderTextDTO> headerTextDTOLst = bookingDisplayJsonOutputData.getHeaderTextDtoLst();
		List<ItemDTO> itemDTOLst = bookingDisplayJsonOutputData.getItemDtoLst();
		// List<OrderTextDTO> itemTextDTO =
		// bookingDisplayJsonOutputData.getItemTextDtoLst();
		List<PartnerDTO> partnerDTOLSt = bookingDisplayJsonOutputData.getPartnerDtoLst();
		OrderHeader orderHeader = new OrderHeader();
		DirectBookingParams directBookingParams = new DirectBookingParams();
		orderHeader.setExpNo(headerDisplayDTO.getZzexpno());
		orderHeader.setExpDate(DateUtil.YYYYMMDDToDD_MM_YYYY(headerDisplayDTO.getZzexpdt()));
		orderHeader.setLcTtPono(headerDisplayDTO.getZzlcpottno());
		orderHeader.setLcTtPoDate(DateUtil.YYYYMMDDToDD_MM_YYYY(headerDisplayDTO.getZzlcdt()));
		orderHeader.setShippingDate(DateUtil.YYYYMMDDToDD_MM_YYYY(headerDisplayDTO.getZzshpmntvldtydt()));

		for (OrderTextDTO orderTextDTO : headerTextDTOLst) {
			if (orderTextDTO.getTextId().equals("Z023")) {
				// orderHeader.setDescription(orderTextDTO.getLine());
				desGoods = desGoods + orderTextDTO.getLine();
			}
			if (orderTextDTO.getTextId().equals("Z112")) {
				// orderHeader.setShippingMark(orderTextDTO.getLine());
				shipMark = shipMark + orderTextDTO.getLine();
			}
		}
		orderHeader.setDescription(desGoods);
		orderHeader.setShippingMark(shipMark);
		orderHeader.setHblNumber(headerDisplayDTO.getHblNo());
		orderHeader.setComInvNo(headerDisplayDTO.getZzcomminvno());
		orderHeader.setComInvDate(DateUtil.YYYYMMDDToDD_MM_YYYY(headerDisplayDTO.getZzcomminvdt()));

		for (Iterator<PartnerDTO> iterator = partnerDTOLSt.iterator(); iterator.hasNext();) {
			PartnerDTO partnerDTO = (PartnerDTO) iterator.next();
			if (partnerDTO.getPartnRole().equalsIgnoreCase("WE")) {
				orderHeader.setBuyer(partnerDTO.getPartnNumb());
				orderHeader.setBuyerName(partnerDTO.getPartnerName());
				orderHeader.setBuyerAddress1(partnerDTO.getPartnerAddr1());
				orderHeader.setBuyerAddress2(partnerDTO.getPartnerAddr2());
				orderHeader.setBuyerAddress3(partnerDTO.getPartnerAddr3());
				orderHeader.setBuyerCity(partnerDTO.getPartnerCity());
				orderHeader.setBuyerCountry(partnerDTO.getPartnerCountry());
				orderHeader.setBuyerCountryDesc(partnerDTO.getPartnerCountryName());
			}
			if (partnerDTO.getPartnRole().equalsIgnoreCase("AG")) {
				orderHeader.setBuyersBank(partnerDTO.getPartnNumb());
				orderHeader.setShipper(partnerDTO.getPartnNumb());
				orderHeader.setShipperName(partnerDTO.getPartnerName());
				orderHeader.setShipperAddress1(partnerDTO.getPartnerAddr1());
				orderHeader.setShipperAddress2(partnerDTO.getPartnerAddr2());
				orderHeader.setShipperAddress3(partnerDTO.getPartnerAddr3());
				orderHeader.setShipperCity(partnerDTO.getPartnerCity());
				orderHeader.setShipperCountry(partnerDTO.getPartnerCountry());
				orderHeader.setShipperCountryDesc(partnerDTO.getPartnerCountryName());
			}
			if (partnerDTO.getPartnRole().equalsIgnoreCase("ZY")) {
				orderHeader.setShippersBank(partnerDTO.getPartnNumb());
				orderHeader.setShippersBankName(partnerDTO.getPartnerName());
				orderHeader.setShippersBankAddress1(partnerDTO.getPartnerAddr1());
				orderHeader.setShippersBankAddress2(partnerDTO.getPartnerAddr2());
				orderHeader.setShippersBankAddress3(partnerDTO.getPartnerAddr3());
				orderHeader.setShippersBankCity(partnerDTO.getPartnerCity());
				orderHeader.setShippersBankCountry(partnerDTO.getPartnerCountry());
				orderHeader.setShippersBankCountryDesc(partnerDTO.getPartnerCountryName());
			}
			if (partnerDTO.getPartnRole().equalsIgnoreCase("ZO")) {
				orderHeader.setNotifyParty(partnerDTO.getPartnNumb());
				orderHeader.setNotifyPartyName(partnerDTO.getPartnerName());
				orderHeader.setNotifyPartyAddress1(partnerDTO.getPartnerAddr1());
				orderHeader.setNotifyPartyAddress2(partnerDTO.getPartnerAddr2());
				orderHeader.setNotifyPartyAddress3(partnerDTO.getPartnerAddr3());
				orderHeader.setNotifyPartyCity(partnerDTO.getPartnerCity());
				orderHeader.setNotifyPartyCountry(partnerDTO.getPartnerCountry());
				orderHeader.setNotifyPartyCountryDesc(partnerDTO.getPartnerCountryName());
			}

		}

		List<OrderItem> lstOrderItem = new ArrayList<>();
		directBookingParams.setOrderHeader(orderHeader);
		// int itemNumber=10;
		for (Iterator<ItemDTO> iterator = itemDTOLst.iterator(); iterator.hasNext();) {
			ItemDTO itemDTO = (ItemDTO) iterator.next();
			OrderItem orderItem = new OrderItem();
			orderItem.setArticleNo(itemDTO.getArticleNo());
			orderItem.setPoNumber(itemDTO.getPoNumber());
			orderItem.setRefNo(itemDTO.getReference2());
			orderItem.setProductCode(itemDTO.getSkuNo());
			orderItem.setSizeNo(itemDTO.getSize());
			orderItem.setCartonLength(itemDTO.getLength().doubleValue());
			orderItem.setItemNumber(itemDTO.getItemNumber());
			orderItem.setDocNumber(itemDTO.getSoNumber());
			orderItem.setRefDoc("");
			orderItem.setStyleNo(itemDTO.getStyleNo());
			orderItem.setProjectNo(itemDTO.getArticleNo());
			orderItem.setColor(itemDTO.getColour());
			orderItem.setTotalPieces(itemDTO.getTotalPcs().intValue());
			orderItem.setCurtonQuantity(itemDTO.getQuantity().intValue());
			orderItem.setUnit(itemDTO.getUnit());
			orderItem.setPcsPerCarton(itemDTO.getPcsPerCartoon().intValue());
			orderItem.setTotalCbm(itemDTO.getCbm().doubleValue());
			orderItem.setTotalVolume(itemDTO.getVolume().doubleValue());
			orderItem.setGrossWeight(itemDTO.getGrWt().doubleValue());
			orderItem.setNetWeight(itemDTO.getNetWt().doubleValue());
			orderItem.setUnitPrice(Double.parseDouble(itemDTO.getUnitCost()));
			orderItem.setCurrency(itemDTO.getUnitCostUnit());
			orderItem.setHsCode(itemDTO.getHsCose());
			orderItem.setCommInvoice(itemDTO.getCommInvoiceNo());
			orderItem.setCommInvoiceDate(DateUtil.YYYYMMDDToDD_MM_YYYY(itemDTO.getInvoiceDate()));
			orderItem.setNetCost(Double.parseDouble(itemDTO.getNetCost()));
			orderItem.setGrossWeightPerCarton(itemDTO.getWtPerCartoon().doubleValue());
			orderItem.setNetWeightPerCarton(itemDTO.getNetWtPerCartoon().doubleValue());
			orderItem.setCartonWidth(itemDTO.getWidth().doubleValue());
			orderItem.setCartonHeight(itemDTO.getHeight().doubleValue());
			orderItem.setCartonUnit(itemDTO.getInHcm());
			orderItem.setCartonSerNo(itemDTO.getCarSerlNo());
			orderItem.setMaterial(itemDTO.getMaretial());
			orderItem.setMaterialText(itemDTO.getMaterialDescription());
			orderItem.setMaterialUnit("");
			orderItem.setReference1(itemDTO.getReference1());
			orderItem.setReference2(itemDTO.getReference3());
			orderItem.setReference3(itemDTO.getReference4());
			orderItem.setReference4(itemDTO.getReference5());
			// orderItem.setReference5(itemDTO.getReference5());
			orderItem.setQcDate(DateUtil.YYYYMMDDToDD_MM_YYYY(itemDTO.getQcDate()));
			orderItem.setReleaseDate(DateUtil.YYYYMMDDToDD_MM_YYYY(itemDTO.getOrderReleaseDate()));
			orderItem.setGrFlag(itemDTO.getGrFlag());
			// orderItem.setItemNumber(itemNumber);
			if (!soNumber.equals(""))
				soNumber = itemDTO.getSoNumber();
			// itemNumber+=10;
			lstOrderItem.add(orderItem);
			/*
			 * OrderItem orderItem2=new OrderItem();
			 * BeanUtils.copyProperties(orderItem2, orderItem);
			 * 
			 * orderItem2.setItemNumber(orderItem2.getItemNumber()+10);
			 * lstOrderItem.add(orderItem2);
			 */
		}
		if (bookingDisplayJsonOutputData.getPackingDetail() != null) {
			PoBookingPacklistDetailBean poBookingPacklistDetailBean = bookingDisplayJsonOutputData.getPackingDetail();
			directBookingParams.setPoBookingPacklistDetailBean(poBookingPacklistDetailBean);

		}

		if (orderHeader.getDocNumber() == null || orderHeader.getDocNumber().equals(""))
			orderHeader.setDocNumber(soNumber);

		directBookingParams.setOrderHeader(orderHeader);
		directBookingParams.setOrderItemLst(lstOrderItem);
		directBookingParams.setOldOrderItemList(lstOrderItem);

		return directBookingParams;
	}

	@RequestMapping(value = "/getBookingDisplayPage", method = RequestMethod.GET)
	public String getBookingDisplayPage(ModelMap model, @ModelAttribute("displayError") String message) {

		logger.info("Method BookingDisplayController.getBookingDisplayPage starts.");

		SalesOrderParams salesOrderParams = new SalesOrderParams();

		model.addAttribute("salesOrderParams", salesOrderParams);

		if (message != null && !message.equalsIgnoreCase("")) {
			model.addAttribute("displayError", message);
		}
		logger.info("Method BookingDisplayController.getBookingDisplayPage ends.");

		return "bookingupdatesearch";
	}
	
	@RequestMapping(value = "/bookingDisplayDataFromUrl", method = RequestMethod.GET)
	public String getBookingDisplayDataFromUrl(@ModelAttribute("salesOrderParams") SalesOrderParams salesOrderParams , Model model,RedirectAttributes redirectAttributes, HttpServletRequest request,
			@RequestParam("searchString") String searchValue) {

		logger.info("Method BookingDisplayController.getBookingDisplayDataFromUrl starts.");

		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		salesOrderParams.setBlno(searchValue);
		if (supplierDetailsDTO != null) {
			BuyersSuppliersmap bsm = supplierDetailsDTO.getBuyersSuppliersmap();
			Map<String, String> mapdt = bsm.getDocTypes();
			if (mapdt.containsKey("ZCOR"))
				mapdt.remove("ZCOR");
			bsm.setDocTypes(mapdt);
			supplierDetailsDTO.setBuyersSuppliersmap(bsm);
			// model.addAttribute("supplierDetails", supplierDetailsDTO);
		} else {
			model.addAttribute("supplierDetails", new SupplierDetailsDTO());

		}

		if (salesOrderParams.getBlno() != null && !salesOrderParams.getBlno().equalsIgnoreCase("")) {
			String blUpperCase = salesOrderParams.getBlno().toUpperCase();
			salesOrderParams.setBlno(blUpperCase);
			try {
				// HttpSession session=SessionUtil.getHttpSession(request);
				LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
				if (null != loginDTO) {
					salesOrderParams.setDistChannel(loginDTO.getDisChnlSelected());
					salesOrderParams.setDivision(loginDTO.getDivisionSelected());
					salesOrderParams.setSalesOrg(loginDTO.getSalesOrgSelected());
					salesOrderParams.setPartner(loginDTO.getLoggedInUserName());
				}
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.SO_BY_HBL);
				SalesOrderJsonOutputData salesOrderJsonOutputData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), salesOrderParams,
						SalesOrderJsonOutputData.class);
				if (null != salesOrderJsonOutputData
						&& !salesOrderJsonOutputData.getSalesOrder().equalsIgnoreCase("")) {
					BookingDisplayParams bookingDisplayParams = new BookingDisplayParams();
					bookingDisplayParams.setSalesOrderNumber(salesOrderJsonOutputData.getSalesOrder());
					webServiceUrl = new StringBuffer(RestUtil.BOOKING_DISPLAY);
					BookingDisplayJsonOutputData bookingDisplayJsonOutputData = restService.postForObject(
							RestUtil.prepareUrlForService(webServiceUrl).toString(), bookingDisplayParams,
							BookingDisplayJsonOutputData.class);

					if (bookingDisplayJsonOutputData != null) {
						// HeaderDisplayDTO headerDisplayDTO =
						// bookingDisplayJsonOutputData.getHeaderDisplayDTO();
						// List<OrderTextDTO> headerTextDTOLst =
						// bookingDisplayJsonOutputData.getHeaderTextDtoLst();
						// List<ItemDTO> itemDTOLst =
						// bookingDisplayJsonOutputData.getItemDtoLst();
						// List<OrderTextDTO> itemTextDTO =
						// bookingDisplayJsonOutputData.getItemTextDtoLst();
						// List<PartnerDTO> partnerDTOLSt =
						// bookingDisplayJsonOutputData.getPartnerDtoLst();
						DirectBookingParams directBookingParams = createBookingObject(bookingDisplayJsonOutputData);
						OrderHeader orderHeader = directBookingParams.getOrderHeader();
						orderHeader.setDocNumber(bookingDisplayParams.getSalesOrderNumber());
						directBookingParams.setOrderHeader(orderHeader);
						ObjectMapper objectMapper = new ObjectMapper();

						String jSON = objectMapper.writeValueAsString(directBookingParams);
						List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");

/*						// test purpose
						MaterialsDto testmat = new MaterialsDto();
						testmat.setLabel("AA MATERIAL");
						testmat.setValue("A75");
						MaterialsDto testmat1 = new MaterialsDto();
						testmat1.setLabel("AA SCIENTIFIC EQUIPMENTS");
						testmat1.setValue("A71");
						MaterialsDto testmat2 = new MaterialsDto();
						testmat2.setLabel("AA LABORATORY SUPPLIES");
						testmat2.setValue("A72");
						lstMaterialsDto.add(testmat);
						lstMaterialsDto.add(testmat1);
						lstMaterialsDto.add(testmat2);*/

						// declaring variables
						List<OrderItem> lstOrderItemForDisplay = directBookingParams.getOrderItemLst();
						String grDone = "";
						Integer checkSum = 0;
						Integer curtonQuantity = 0;
						Double totalVolume = 0.0;
						Double grossWeight = 0.0;
						for (OrderItem orderItem2 : lstOrderItemForDisplay) {
							curtonQuantity = curtonQuantity + orderItem2.getCurtonQuantity();
							grossWeight = grossWeight + orderItem2.getGrossWeight();
							totalVolume = totalVolume + orderItem2.getTotalVolume();
							if (orderItem2.getGrFlag() != null && orderItem2.getGrFlag().equals("x"))
								checkSum++;
							else
								orderItem2.setGrFlag("");
						}
						grossWeight = Math.round(grossWeight * 100.0) / 100.0;
						totalVolume = Math.round(totalVolume * 100.0) / 100.0;
						if (checkSum == 0) {
							// goods not received for any line item
							grDone = "show";
						} else {
							// goods received for all/some line items
							grDone = "hide";
						}

						model.addAttribute("totalQuantity", curtonQuantity);
						model.addAttribute("grossWeight", grossWeight);
						model.addAttribute("totalCBM", totalVolume);
						model.addAttribute("grDone", grDone);
						model.addAttribute("materials", lstMaterialsDto);
						model.addAttribute("directBookingParamsJson", jSON);
						model.addAttribute("directBookingParams", directBookingParams);
						session.setAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE, directBookingParams);
						model.addAttribute("supplierDetails", supplierDetailsDTO);

						if (grDone.equals("show")) {
							boolean itemTab = false;
							model.addAttribute("itemTabActive", new Gson().toJson(itemTab));	
							return "booking-update3";
							// return "booking-updateGoodRecieved";

						} else {
							// return "booking-update"; //replace with actual
							boolean itemTab = false;
							model.addAttribute("itemTabActive", new Gson().toJson(itemTab));	
							return "booking-updateGoodRecieved"; // Added by Tahmid Alam - 11 Sept 2017
						}

					} else {
						logger.info("bookingDisplayJsonOutputData value is null.");
						redirectAttributes.addFlashAttribute("displayError", "No Details Found for HBL and SO!");
						return "redirect:getBookingDisplayPage";
					}
				} else {
					redirectAttributes.addFlashAttribute("displayError",
							"No Sales Order Found, Please Provide Valid HBL No.");
					return "redirect:getBookingDisplayPage";
				}

			} catch (Exception e) {
				e.printStackTrace();
				logger.info("Some problem occurred in Method BookingDisplayController.getBookingData " + e);
				return "redirect:getBookingDisplayPage";
			}

		} else {
			redirectAttributes.addFlashAttribute("displayError", "Please provide valid HBL Number");
			return "redirect:getBookingDisplayPage";
		}
	}
}
