package com.myshipment.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.model.DsrParams;
import com.myshipment.model.ReportParams;


@Controller
public class DailySalesReportController {

	private static final Logger logger = Logger.getLogger(BookingDisplayController.class);
	
	@RequestMapping(value="/dailySalesReport", method = RequestMethod.GET)
	public String getDailySalesReportPage() {
		
		return "dailySalesReportUpdate";
	}
	
	@RequestMapping(value="/dailySalesReportGeneration", method = RequestMethod.GET)
	public String getDailySalesReport(ModelMap model, HttpSession session) {
		
		DsrParams dsrParams = new DsrParams();
		model.addAttribute("dsrParams", dsrParams);
		return "dailySalesReportGeneration";
		
	}
	
}
