package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.mappers.HMMapper;
import com.myshipment.model.HMCountryList;
import com.myshipment.model.ItVbak;
import com.myshipment.model.ItVbap;
import com.myshipment.model.ReportParams;
import com.myshipment.model.ShippingCertificate;
import com.myshipment.model.ShippingOrderReportJsonData;
import com.myshipment.model.ShippingReport;
import com.myshipment.service.IShippingOrder;
import com.myshipment.util.RestService;
import com.myshipment.util.SessionUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class ShipmentCertificateController {
	private Logger logger = Logger.getLogger(ShipmentCertificateController.class);

	@Autowired
	private IShippingOrder shippingOrderService;

	@Autowired
	private HMMapper hmMapper;

	@RequestMapping(value = "/getCertificatePage", method = RequestMethod.GET)
	public String getCertificatePage(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning OK TO SHIP CERTIFICATE Page");

		ReportParams req = new ReportParams();
		model.addAttribute("req", req);
		return "shippingCerInput";
	}

	@RequestMapping(value = "/shippingCer", method = RequestMethod.POST)
	public ModelAndView getShippingCerDetails(@ModelAttribute("req") ReportParams req, Model model, HttpSession session,
			HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) {

		try {
			LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
			String hbl = req.getHblNumber().toUpperCase();
			req.setHblNumber(hbl);
			req.setCustomer(loginDto.getLoggedInUserName());
			req.setDistChannel(loginDto.getDisChnlSelected());
			req.setDivision(loginDto.getDivisionSelected());
			req.setSaleOrg(loginDto.getSalesOrgSelected());
			req.setInvType("");
			ShippingOrderReportJsonData shippingOrderJson = shippingOrderService.getShippingOrderDetail(req);
			List<ItVbak> countryCodeList = shippingOrderJson.getItVbak();
			List<ItVbap> poList = shippingOrderJson.getItVbap();
			List<ShippingReport> shipperList = shippingOrderJson.getItShipperList();
			ShippingCertificate pdfData = new ShippingCertificate();
			pdfData.setHbl(hbl);
			pdfData.setShipperName(shipperList.get(0).getName1());
			String poOrders = poList.get(0).getZzPoNumber();
			String destCountry = countryCodeList.get(0).getDestCountry();
			if (poList.size() > 1) {
				for (int w = 1; w < poList.size(); w++) {
					ItVbap table = poList.get(w);
					poOrders = poOrders + "," + table.getZzPoNumber();
				}
			}
			pdfData.setOrderNo(poOrders);
			if (destCountry.equalsIgnoreCase("DE")) {
				session.setAttribute("pdfData", pdfData);
				modelAndView.setViewName("redirect:/shippingZone");
				return modelAndView;
			} else {
				List<HMCountryList> countryList = hmMapper.allCountry();
				Map<String, Object> parameterMap = new HashMap<String, Object>();
				List<ShippingCertificate> actualPDFData = new ArrayList<ShippingCertificate>();
				actualPDFData.add(pdfData);
				String reportType = request.getParameter("type");
				reportType = reportType == null ? "okShip" : reportType;
				for (int i = 0; i < countryList.size(); i++) {
					HMCountryList data = countryList.get(i);
					ShippingCertificate countryData = new ShippingCertificate();
					countryData.setCountry(data.getCountry());
					countryData.setCountryCode(data.getCode());
					if (data.getCode().contains(destCountry)) {
						countryData.setHbl(hbl);
					}
					actualPDFData.add(countryData);
				}
				JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);
				parameterMap.put("datasource", pdfDataSource);
				ServletContext context = request.getSession().getServletContext();
				String path = context.getRealPath("/") + "";
				parameterMap.put("Context", path);
				modelAndView = new ModelAndView(reportType, parameterMap);
				return modelAndView;
			}

		} catch (IndexOutOfBoundsException ex) {
			// TODO Auto-generated catch block
			modelAndView.setViewName("redirect:/getCertificatePage");
			redirecAttributes.addFlashAttribute("message", "HBL number is not valid!! Please enter a valid HBL number");
			return modelAndView;
		}
	}

	@RequestMapping(value = "/shippingZone", method = RequestMethod.GET)
	public String getshippingZone(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning OK TO SHIP CERTIFICATE Page for Germany");

		HMCountryList req = new HMCountryList();
		model.addAttribute("req", req);
		return "shippingCerZoneGermany";
	}

	@RequestMapping(value = "/getshipCerForGermany", method = RequestMethod.POST)
	public ModelAndView getShippingCerForGermany(@ModelAttribute("req") HMCountryList req, Model model,
			HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) {
		ShippingCertificate pdfData = (ShippingCertificate) session.getAttribute("pdfData");

		Map<String, Object> parameterMap = new HashMap<String, Object>();
		List<ShippingCertificate> actualPDFData = new ArrayList<ShippingCertificate>();
		actualPDFData.add(pdfData);
		List<HMCountryList> countryList = hmMapper.allCountry();
		String reportType = request.getParameter("type");
		reportType = reportType == null ? "okShip" : reportType;
		for (int i = 0; i < countryList.size(); i++) {
			HMCountryList data = countryList.get(i);
			ShippingCertificate countryData = new ShippingCertificate();
			countryData.setCountry(data.getCountry());
			countryData.setCountryCode(data.getCode());
			
			/* if (data.getCode().contains(destCountry)) { countryData.setHbl(hbl); }*/
			 
			if (data.getZone()!= null) {
				if (data.getZone().equalsIgnoreCase(req.getZone())) {
					countryData.setHbl(pdfData.getHbl());
				}
			}
			actualPDFData.add(countryData);
		}
		JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);
		parameterMap.put("datasource", pdfDataSource);
		ServletContext context = request.getSession().getServletContext();
		String path = context.getRealPath("/") + "";
		parameterMap.put("Context", path);
		modelAndView = new ModelAndView(reportType, parameterMap);
		return modelAndView;

	}

}
