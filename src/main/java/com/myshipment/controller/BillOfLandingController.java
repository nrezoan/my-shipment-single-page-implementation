package com.myshipment.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myshipment.dto.DistributionChannel;
import com.myshipment.dto.Division;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SalesOrgTree;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.Login;
import com.myshipment.model.NegoBillLanding;
import com.myshipment.model.RequestParams;
import com.myshipment.model.StuffingDetail;
import com.myshipment.model.StuffingHeader;
import com.myshipment.model.StuffingReportData;
import com.myshipment.model.SupRequestParams;
import com.myshipment.service.IAirBillOfLanding;
import com.myshipment.service.ILoginService;
import com.myshipment.service.IStuffingReport;
import com.myshipment.util.MyShipApplicationException;
import com.myshipment.util.SessionUtil;

/**
 * @author Gufranur Rahman
 *
 */
@Controller
public class BillOfLandingController extends BaseController {

	@Autowired
	IAirBillOfLanding billOfLandingServices;
	
	private Logger logger = Logger.getLogger(BillOfLandingController.class);

	
	@RequestMapping(value="/billOfLanding", method = RequestMethod.GET )
	public String getStuffingAdvice() {
		logger.info(this.getClass().getName() + " Returning Login Page");

		return "billOfLanding";

	}

	/*@RequestMapping(value = "/stuffingAdvice1", method = RequestMethod.GET)
	public StuffingReportData getStuffingDetail(@RequestParam("hblNO") String hblNO,@RequestParam("saleOrg") String saleOrg, 
			@RequestParam("distChannel") String distChannel, @RequestParam("division") String division,
			@RequestParam("customer") String customer,Model model) {
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		RequestParams req = new RequestParams();
		req.setReq1(hblNO);
		req.setReq2(saleOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq4(customer);
		
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		StuffingReportData stuffingReportData = null;
		stuffingReportData = stuffingService.getStuffingDetail(req);
		List<StuffingHeader> headeritems= stuffingReportData.getHeaderitem();
		List<StuffingDetail> detailItems=stuffingReportData.getBapiItem();
		List<NegoBillLanding> itAddress =stuffingReportData.getItAddress();
		model.addAttribute("headeritems",headeritems);
		model.addAttribute("detailItem",detailItems);
		model.addAttribute("itAddress",itAddress);
		return stuffingReportData;
	}
/*
	@RequestMapping(value = "/distibutionchannels", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getDistChnlBySalesOrg(@RequestParam("salesOrg") String salesOrg, HttpServletRequest request) {
		HttpSession session = request.getSession();
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		SalesOrgTree salesOrgTree = loginDto.getMpSalesOrgTree().get(salesOrg);
		List<String> listOfDistChanel = new ArrayList<>();
		Set<DistributionChannel> lstDistChnl = salesOrgTree.getLstDistChnl();
		for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
			DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
			listOfDistChanel.add(distributionChannel.getDistrChan());

		}
		loginDto.setSalesOrgSelected(salesOrg);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		return listOfDistChanel;
	}*/

	/*@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public String getDashBoard(@RequestParam("companyName") String companyName, @RequestParam("options") String options,
			Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();
		loginDto.setSalesOrgSelected(companyName);
		loginDto.setDisChnlSelected(options);
		SupRequestParams supRequestParams = new SupRequestParams();
		Set<Division> lstDivision=null;
		SalesOrgTree salesOrgTree=loginDto.getMpSalesOrgTree().get(loginDto.getSalesOrgSelected());
		for (Iterator iterator = salesOrgTree.getLstDistChnl().iterator(); iterator.hasNext();) {
			DistributionChannel type = (DistributionChannel) iterator.next();
			if(type.getDistrChan().equals(loginDto.getDisChnlSelected()))
				lstDivision=type.getLstDivision();
			
			
		}
		Division division=new Division();
		
		if(lstDivision!=null && lstDivision.size()>0)
		{
			
		}
			
		supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
		supRequestParams.setDistChan(loginDto.getDisChnlSelected());
		supRequestParams.setDivision("SE");
		supRequestParams.setSalesOrg(loginDto.getSalesOrgSelected());
		SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams, session);

		session.setAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA, supplierDetailsDTO);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		
		return "dashboard";
	}
	
	@RequestMapping(value="/headerdetails",method=RequestMethod.GET)
	@ResponseBody
	public LoginDTO getHeaderDetails(Model model,HttpServletRequest request)
	{
		HttpSession session=SessionUtil.getHttpSession(request);
		LoginDTO loginDto=(LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		return loginDto;
	}
	
	

	@RequestMapping(value = "/currentSelectedOrg", method = RequestMethod.GET)
	public String getCurrentSelestedOrgDistChnl(@RequestParam("salesOrg") String salesOrg,
			@RequestParam("distributionChannel") String distributionChannel, HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();
		loginDto.setSalesOrgSelected(salesOrg);
		loginDto.setDisChnlSelected(distributionChannel);
		SupRequestParams supRequestParams = new SupRequestParams();
		supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
		supRequestParams.setDistChan(loginDto.getDisChnlSelected());
		supRequestParams.setDivision(loginDto.getDivisionSelected());

		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		model.addAttribute("loginDetails", loginDto);
		return "dashboard";
	}
*/
}
