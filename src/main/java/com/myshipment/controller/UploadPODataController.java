package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.FileDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.ManualPODTO;
import com.myshipment.dto.OrderHeaderDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.DirectBookingJsonData;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.PreAlertSaveBLStatus;
import com.myshipment.model.RedirectParams;
import com.myshipment.model.RedirectParamsPOForm;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.service.IOrderService;
import com.myshipment.service.IPoMgmtService;
import com.myshipment.service.IPreAlertManagementService;
import com.myshipment.service.IUploadPODataService;
import com.myshipment.util.SessionUtil;

/*
 * @ Ranjeet Kumar
 */
@Controller
public class UploadPODataController {

	final static Logger logger = Logger.getLogger(UploadPODataController.class);

	MultipartFile file;
	@Autowired
	private IUploadPODataService uploadPODataService;

	@Autowired
	IOrderService orderService;

	@Autowired
	private IPoMgmtService poMgmtService;

	@Autowired
	private IPreAlertManagementService preAlertManagementService;

	@RequestMapping(value = "/processExcelData", method = RequestMethod.POST)
	public String readExcelData(@ModelAttribute("fileData") FileDTO fileData, @RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {

		logger.info("UploadPODataController.processExcelData method starts.");

		if (file.getSize() > 0) {
			List<SegPurchaseOrder> poIdLst = null;

			try {
				logger.info("File name is : " + file.getOriginalFilename());

				poIdLst = uploadPODataService.processData(file, fileData);
				if (poIdLst.isEmpty()) {
					return "uploadLatest";
				}
				logger.info("UploadPODataController.processExcelData method ends.");
			} catch (Exception ex) {
				return "uploadLatest";
			}

			RedirectParams redirectParams = new RedirectParams();
			redirectParams.setPoIdLst(poIdLst);
			redirectAttributes.addFlashAttribute("redirectParams", redirectParams);

			return "redirect:/getResults";
		} else {
			redirectAttributes.addFlashAttribute("fileUploadError", "Please select a valid file to upload");

			return "redirect:/getPoUploadPage";
		}
	}

	@RequestMapping(value = "/getPoUploadPage", method = RequestMethod.GET)
	public String getPoUploadPage(Map<String, Object> model, Model model1, HttpServletRequest request,
			@ModelAttribute("fileUploadError") String message) {
		logger.info("UploadPODataController.getPoUploadPage method starts.");

		FileDTO fileData = new FileDTO();
		ManualPODTO manualPODTO = new ManualPODTO();

		model.put("fileData", fileData);
		model.put("manualPODTO", manualPODTO);
		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);

		List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");
		model1.addAttribute("materials", lstMaterialsDto);
		if (supplierDetailsDTO.getBuyersSuppliersmap() == null)
			model.put("shipper", new HashMap<String, String>());
		else {
			Map<String, String> shipper = supplierDetailsDTO.getBuyersSuppliersmap().getBuyers();
			model1.addAttribute("shipper", shipper);
		}
		logger.info("UploadPODataController.getPoUploadPage method ends.");

		if (message != null) {
			model.put("fileUploadError", message);
		}

		return "uploadLatest";

	}

	/* Sanjana Khan Shammi */

	@RequestMapping(value = "/savePOGeneratedInfo", method = RequestMethod.POST)
	@ResponseBody
	public void savePOGenerationInfo(HttpServletRequest request, Model model,
			@RequestBody List<Map<String, Object>> POGenerationInfo, RedirectAttributes redirectAttributes) {

		logger.info("UploadPODataController.savePOGenerationInfo method starts.");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		
		Map<String, String> shipper = supplierDetailsDTO.getBuyersSuppliersmap().getBuyers();
		
		
		
		List<ApprovedPurchaseOrder> approvedPurchaseOrder = null;
		approvedPurchaseOrder = uploadPODataService.savePOGenerationInfo(POGenerationInfo, shipper, loginDto);
		

		RedirectParamsPOForm redirectParams = new RedirectParamsPOForm();
		redirectParams.setPoIdLst(approvedPurchaseOrder);
		redirectParams.setSupplierName(uploadPODataService.supplierName());
		session.setAttribute("redirectParams", redirectParams);

	}
	
	
	/*       */

	@RequestMapping(value = "/poGenerationResponse", method = RequestMethod.GET)
	public String poGenerationResponse(HttpServletRequest request, Model model) {
		logger.info("UploadPODataController.poGenerationResponse method starts.");

		List<ApprovedPurchaseOrder> uploadedDataLst = new ArrayList<ApprovedPurchaseOrder>();

		HttpSession session = SessionUtil.getHttpSession(request);
		RedirectParamsPOForm redirectParams = (RedirectParamsPOForm) session.getAttribute("redirectParams");

		if (redirectParams != null) {
			List<ApprovedPurchaseOrder> appLst = redirectParams.getPoIdLst();
			for (ApprovedPurchaseOrder appPO : appLst) {
				uploadedDataLst.add(appPO);
			}

		}

		model.addAttribute("uploadedDataLst", uploadedDataLst);
		model.addAttribute("supplier", redirectParams.getSupplierName());
		// model.addAllAttributes(attributeValues)("uploadedDataLst", uploadedDataLst);

		logger.info("UploadPODataController.showUploadedData method ends.");

		return "poView";
	}

	

	@RequestMapping(value = "/getResults", method = RequestMethod.GET)
	public String showUploadedData(ModelMap model, @ModelAttribute("redirectParams") RedirectParams redirectParams) {
		logger.info("UploadPODataController.showUploadedData method starts.");

		List<List<SegPurchaseOrder>> uploadedDataLstForPage = null;
		List<SegPurchaseOrder> uploadedDataLst = new ArrayList<SegPurchaseOrder>();

		if (redirectParams != null) {
			List<SegPurchaseOrder> segLst = redirectParams.getPoIdLst();
			for (SegPurchaseOrder seg : segLst) {
				uploadedDataLst.add(seg);
			}

		}

		model.put("uploadedDataLst", uploadedDataLst);

		logger.info("UploadPODataController.showUploadedData method ends.");

		return "poView";
	}
}
