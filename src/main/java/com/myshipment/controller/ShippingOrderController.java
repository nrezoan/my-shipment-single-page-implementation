package com.myshipment.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.gson.Gson;
import com.google.zxing.WriterException;
import com.myshipment.dto.LoginDTO;
/*import com.myshipment.model.BarCodeImage;*/
import com.myshipment.model.ItVbak;
import com.myshipment.model.ItVbap;
import com.myshipment.model.ReportParams;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingOrderReportJsonData;
import com.myshipment.model.ShippingReport;
/*import com.myshipment.service.IBarCodeService;*/
import com.myshipment.service.IShippingOrder;
import com.myshipment.util.DateUtil;
import com.myshipment.model.ShippingOrderPdfGeneration;
/*import com.myshipment.model.ShippingOrderQR;*/

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * @author Gufranur Rahman
 * @author Hamid Bin Zaman
 * @author Nusrat Momtahana
 *
 */
@Controller
public class ShippingOrderController extends BaseController {

	private static final long serialVersionUID = -236829190769333278L;

	@Autowired
	private IShippingOrder shippingOrderService;
	
	private Logger logger = Logger.getLogger(ShippingOrderController.class);
	
	/*@Autowired
	IBarCodeService barCodeService;*/
	
	@RequestMapping(value="/getShippingOrderPage", method = RequestMethod.GET )
	public String getShippingOrder(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		RequestParams req = new RequestParams();
		model.addAttribute("req", req);
		return "shippingOrderInput";

	}

	
	@RequestMapping(value="/getShippingOrder", method = RequestMethod.GET )
	public String getShippingOrderDtl(@RequestParam("searchString") String searchValue,Model model,HttpSession session) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		RequestParams req = new RequestParams();
		req.setReq1(searchValue);
		
		return getShippingOrderDetail(req,model,session);
		

	}
	
	@RequestMapping(value = "/shippingOrder",params = "btn-appr-po-search", method = RequestMethod.POST)
	public String getShippingOrderDetail(@ModelAttribute("req") RequestParams req, Model model, HttpSession session) { 
			
			
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		String hbl = req.getReq1().toUpperCase();
		req.setReq1(hbl);
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		ReportParams rParam=new ReportParams();
		rParam.setCustomer(req.getReq5());
		rParam.setDistChannel(req.getReq3());
		rParam.setDivision(req.getReq4());
		rParam.setHblNumber(req.getReq1());
		rParam.setInvType("");
		rParam.setSaleOrg(req.getReq2());
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		ShippingOrderReportJsonData shippingOrderJson = null;
		
		shippingOrderJson = shippingOrderService.getShippingOrderDetail(rParam);
		List<ItVbak> lstItVbak=shippingOrderJson.getItVbak();
/*		if(lstItVbak.size()>0){
		BarCodeImage barCodeImage=null;
		try {
		 barCodeImage= barCodeService.generateCode128toImage(lstItVbak.get(0).getBstnk(), "", "png", 159, 159);
		} catch (FileNotFoundException e) {
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		} catch (WriterException e) {
			// TODO Auto-generated catch block
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		}
		model.addAttribute("barCodeImage", barCodeImage);
		}*/
		model.addAttribute("shippingOrderJson", shippingOrderJson);
		
		return "shippingOrderDetail";
	}
	
	
	//nusrat
	//13.06.2017

	@RequestMapping(value = "/shippingOrder",params = "download", method = RequestMethod.POST)
	public ModelAndView downloadShippingOrder(@ModelAttribute("req") RequestParams req,Model model, 
			HttpSession session,HttpServletResponse response,HttpServletRequest request,
			ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result)
	{
		try {
			logger.debug(this.getClass().getName() + "performLogin():  Start ");
			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
			String customerNo = null;
			String distChannel=null;
			String division=null;
			String salesOrg=null;
			if(loginDto != null){
				customerNo = loginDto.getLoggedInUserName();
				distChannel=loginDto.getDisChnlSelected();
				division=loginDto.getDivisionSelected();
				salesOrg=loginDto.getSalesOrgSelected();
				logger.info("Customer No found as : "+ customerNo);
			}
			String hbl = req.getReq1().toUpperCase();
			req.setReq1(hbl);
			req.setReq2(salesOrg);
			req.setReq3(distChannel);
			req.setReq4(division);
			req.setReq5(customerNo);
			
			ReportParams rParam=new ReportParams();
			rParam.setCustomer(req.getReq5());
			rParam.setDistChannel(req.getReq3());
			rParam.setDivision(req.getReq4());
			rParam.setHblNumber(req.getReq1());
			rParam.setInvType("");
			rParam.setSaleOrg(req.getReq2());
			logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
					+ ":" + req.getReq2());
			ShippingOrderReportJsonData shippingOrderJson = null;
			
			shippingOrderJson = shippingOrderService.getShippingOrderDetail(rParam);
			
			//ShippingReport pdfData= new ShippingReport();
			
			ShippingOrderPdfGeneration pdfData = new ShippingOrderPdfGeneration();
			Map<String, Object> parameterMap = new HashMap<String, Object>();
			List<ShippingOrderPdfGeneration> actualPDFData = new ArrayList<ShippingOrderPdfGeneration>();
			List<ShippingReport> tableData= shippingOrderJson.getItAddList();
			List<ShippingReport> tableData2= shippingOrderJson.getItShipperList();
			List<ShippingReport> tableDataBuyer= shippingOrderJson.getItCosigneeList();
			List<ShippingReport> tableData5= shippingOrderJson.getItAgentList();
			List<ShippingReport> tableData6= shippingOrderJson.getItPickList();
			List<ItVbak> tableData3=shippingOrderJson.getItVbak();
			List<ItVbap> tableData4=shippingOrderJson.getItVbap();
			String reportType = request.getParameter("type");
			reportType = reportType == null ? "pdfReport" : reportType;
			ShippingReport c= tableData.get(0);
			ShippingReport b= tableData2.get(0);
			ShippingReport buyer= tableDataBuyer.get(0);
			if(tableDataBuyer.isEmpty())
			{
				pdfData.setCnf("N/A");
			}
			else
			{
				ShippingReport d= tableDataBuyer.get(0);
				pdfData.setCnf(d.getName1());
			}
			if(tableData6.isEmpty())
			{
				pdfData.setNotify("N/A");
			}
			else
			{
			ShippingReport e= tableData6.get(0);
			pdfData.setNotify(e.getName1());
			}
			ItVbak a=tableData3.get(0);
			
			//populating instance of the shippingOrderQRClass
			/*ShippingOrderQR shipOrderQR = new ShippingOrderQR();
			shipOrderQR.setBuyer(buyer.getKunNr());
			shipOrderQR.setBuyername(buyer.getName1());
			shipOrderQR.setCompcode(a.getComp_code());
			shipOrderQR.setExpchdt(DateUtil.formatDateToString(a.getZzCargoHandOverDt()));
			shipOrderQR.setFetd("");
			shipOrderQR.setFvsl("");
			shipOrderQR.setHbl(a.getBstnk());
			shipOrderQR.setMeta("");
			shipOrderQR.setMvsl("");
			shipOrderQR.setPod(shippingOrderJson.getItPortDest());
			shipOrderQR.setPol(shippingOrderJson.getItPortLoad());
			shipOrderQR.setSalesorg(c.getVkOrg());
			shipOrderQR.setShipper(b.getKunNr());
			shipOrderQR.setShippername(b.getName1());
			shipOrderQR.setSo(a.getVbeln());
			Gson gson = new Gson();
			String shipOrderQRJson = gson.toJson(shipOrderQR);*/
		/*	String shipOrderQRJson = a.getBstnk();
			
			pdfData.setQrString(shipOrderQRJson);*/
			pdfData.setCompanyName(c.getName1());
	    	pdfData.setAddress1(c.getName2());
			pdfData.setAddress2(c.getName3());
			pdfData.setAddress3(c.getName4());
			pdfData.setTelno(c.getTelNumber());
			pdfData.setSalesOrg(c.getVkOrg());
			pdfData.setPlaceOfReceipt(shippingOrderJson.getItPortLoad());
			pdfData.setDestination(shippingOrderJson.getItPortDest());
			pdfData.setSoNo(a.getVbeln());
			pdfData.setHblNo(a.getBstnk());
			pdfData.setExpectedDate(DateUtil.formatDateToString(a.getZzCargoHandOverDt()));
			pdfData.setShipper(b.getName1());
			pdfData.setPhone1(b.getTelNumber());
			pdfData.setPhone2("N/A");
			pdfData.setPhone3("N/A");
			pdfData.setVesselName(a.getZzAirlineName1());
			pdfData.setVesselNo(a.getZzAirLineNo1());
			pdfData.setMotherVesselName(a.getZzAirlinerName2());
			pdfData.setMotherVesselNo(a.getZzAirlineNo2());
			pdfData.setCarrier(a.getZzPlaceOfReceipt());
			pdfData.setCompanyCode(a.getComp_code());
			if(a.getOrderType().equalsIgnoreCase("ZMSE") || a.getOrderType().equalsIgnoreCase("ZSCM") || a.getOrderType().equalsIgnoreCase("ZBKT") || a.getOrderType().equalsIgnoreCase("ZPSE") ) {
				pdfData.setOrderType("SEA BOOKING");
			}else if(a.getOrderType().equalsIgnoreCase("ZMSA") || a.getOrderType().equalsIgnoreCase("ZSSA") || a.getOrderType().equalsIgnoreCase("ZBSA")) {
				pdfData.setOrderType("SEA-AIR BOOKING");
			}else if(a.getOrderType().equalsIgnoreCase("ZMAE") || a.getOrderType().equalsIgnoreCase("ZBKA") || a.getOrderType().equalsIgnoreCase("ZSCA")) {
				pdfData.setOrderType("AIR BOOKING");
			}else if(a.getOrderType().equalsIgnoreCase("ZMAS") || a.getOrderType().equalsIgnoreCase("ZSAS") || a.getOrderType().equalsIgnoreCase("ZBAS")) {
				pdfData.setOrderType("AIR-SEA BOOKING");
			}
			actualPDFData.add(pdfData);
//		    int tcqty =0;
//		    int ttotalpc=0;
//		    int d=0;
//		    BigDecimal tgw = new BigDecimal(d);
//		    BigDecimal tnw = new BigDecimal(d);
//		    BigDecimal ttcbm = new BigDecimal(d);
			Double tgw = 0.00;
		    
			for(int w=0; w<tableData4.size();w++)
			{
				ItVbap table=tableData4.get(w);
                
				ShippingOrderPdfGeneration pdfTable = new ShippingOrderPdfGeneration();	
				pdfTable.setPo(table.getZzPoNumber());
				pdfTable.setComodity(table.getArktx());
				pdfTable.setCartonQty(table.getZzQuantity().intValue());
				pdfTable.setStyleNo(table.getZzStyleNo());
				pdfTable.setSku(table.getZzSkuNo());
				pdfTable.setTotalPcs(table.getZzTotalNoPcs().intValue());
				pdfTable.setGrossWeight(table.getZzGrWt().doubleValue());
				pdfTable.setNetWeight(table.getZzNtWt().doubleValue());
				pdfTable.setTotalCbm(table.getZzVolume().doubleValue());
				tgw = tgw + table.getZzGrWt().doubleValue();
				pdfTable.setTotalGrossW(round(tgw.toString()));
//				tcqty=tcqty+pdfTable.getCartonQty();
//				ttotalpc=ttotalpc+pdfTable.getTotalPcs();
//				tgw.add(pdfTable.getGrossWeight());
//				tnw.add(pdfTable.getNetWeight());
//				ttcbm.add(pdfTable.getTotalCbm());
				
		        actualPDFData.add(pdfTable);

			}
			
			JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);
            
			parameterMap.put("datasource", pdfDataSource);
			ServletContext context = request.getSession().getServletContext();
			String path =context.getRealPath("/") + "";
			parameterMap.put("Context", path);
			modelAndView = new ModelAndView(reportType, parameterMap);

			return modelAndView;
		} 
	
		catch (IndexOutOfBoundsException ex) {
			// TODO Auto-generated catch block
			 modelAndView.setViewName("redirect://getShippingOrderPage");
	    	 redirecAttributes.addFlashAttribute("message","HBL number is not valid!! Please enter a valid HBL number");
	    	 return modelAndView;
		}
	}
	



	//hamid
	//22.03.2017
	@RequestMapping(value = "/shippingOrderFromUrl", method = RequestMethod.GET)
	public String getShippingOrderDetailFromUrl(@ModelAttribute("req") RequestParams req, Model model, HttpSession session, @RequestParam("searchString") String searchValue) { 
			
			
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		
		req.setReq1(searchValue);
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		ReportParams rParam=new ReportParams();
		rParam.setCustomer(req.getReq5());
		rParam.setDistChannel(req.getReq3());
		rParam.setDivision(req.getReq4());
		rParam.setHblNumber(req.getReq1());
		rParam.setInvType("");
		rParam.setSaleOrg(req.getReq2());
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		ShippingOrderReportJsonData shippingOrderJson = null;
		
		shippingOrderJson = shippingOrderService.getShippingOrderDetail(rParam);
		List<ItVbak> lstItVbak=shippingOrderJson.getItVbak();
		/*if(lstItVbak.size()>0){
		BarCodeImage barCodeImage=null;
		try {
		 barCodeImage= barCodeService.generateCode128toImage(lstItVbak.get(0).getBstnk(), "", "png", 159, 159);
		} catch (FileNotFoundException e) {
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		} catch (WriterException e) {
			// TODO Auto-generated catch block
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.debug(this.getClass().getName()+" getShippingOrderDetail() "+e.getMessage());
		}
		model.addAttribute("barCodeImage", barCodeImage);
		}*/
		model.addAttribute("shippingOrderJson", shippingOrderJson);
		
		return "shippingOrderDetail";
	}
	
	private String round(String bigDecimal) {
		DecimalFormat r = new DecimalFormat("###.##");
		Double d = Double.valueOf(bigDecimal);
		d=Math.round(d * 100.0)/100.0;
		String s = r.format(d);
		return s;
	}

	
}
