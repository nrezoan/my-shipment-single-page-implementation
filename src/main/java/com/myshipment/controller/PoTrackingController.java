package com.myshipment.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.PoTrackingDetailResultBean;
import com.myshipment.model.PoTrackingJsonOutputData;
import com.myshipment.model.PoTrackingParams;
import com.myshipment.model.PoTrackingResultBean;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

/*
 * @Ranjeet Kumar
 */
@Controller
public class PoTrackingController {

	private static final Logger logger = Logger
			.getLogger(PoTrackingController.class);

	@Autowired
	private RestService restService;

	// po detail output
	@RequestMapping(value = "/getPoTrackingReportDetail", method = RequestMethod.POST)
	public String getPoTrackingReportDetail(
			@ModelAttribute("poTrackingParams") PoTrackingParams poTrackingParams,
			Model model, HttpServletRequest request) {

		logger.info("Method PoTrackingController.getPoTrackingDetail starts.");

		PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try {
			if (poTrackingParams != null) {
				LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
				
				/* deprecated */
/*				if (loginDto.getAccgroup().equals("ZMSP")
						|| loginDto.getAccgroup().equals("ZMFF")) {
					poTrackingParams.setShipper_no(loginDto.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
					poTrackingParams.setAgent_no("");
				}
				if (loginDto.getAccgroup().equals("ZMBY")
						|| loginDto.getAccgroup().equals("ZMBH")) {
					poTrackingParams
							.setBuyer_no(loginDto.getLoggedInUserName());
					poTrackingParams.setShipper_no("");
					poTrackingParams.setAgent_no("");
				}
				if (loginDto.getAccgroup().equals("ZMDA")
						|| loginDto.getAccgroup().equals("ZMOA")) {
					poTrackingParams
							.setAgent_no(loginDto.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
					poTrackingParams.setShipper_no("");
				}*/
				
				if(poTrackingParams.getFromDate()==null)
				{
					poTrackingParams.setFromDate("");
				}
				if(poTrackingParams.getToDate()==null)
				{
					poTrackingParams.setToDate("");
				}

/*				if (poTrackingParams.getStatusType().equalsIgnoreCase(
						CommonConstant.STATUS_TYPE_NONE)) {
					poTrackingParams.setStatusType("");
				}*/
				poTrackingParams.setDivision(loginDto.getDivisionSelected());
				poTrackingParams.setDistChan(loginDto.getDisChnlSelected());
				if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("SE")) {
					poTrackingParams.setAction("09");
				} else if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("AR")) {
					poTrackingParams.setAction("10");
				}				
				poTrackingParams.setCustomer(loginDto.getLoggedInUserName());
				poTrackingParams.setSalesOrg(loginDto.getSalesOrgSelected());

				StringBuffer webServiceUrl = new StringBuffer(RestUtil.PO_TRACKING_REPORT);

				poTrackingJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), poTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingDetailResultBean> poTrackingResultBeanLst = poTrackingJsonOutputData
						.getPoTrackingDetailResultData();
				// hamid
				for (PoTrackingDetailResultBean potrackdetailresultbean : poTrackingResultBeanLst) {
					potrackdetailresultbean
							.setGr_date(convertStringToDate(potrackdetailresultbean
									.getGr_date()));
					potrackdetailresultbean
							.setChdt(convertStringToDate(potrackdetailresultbean
									.getChdt()));
					potrackdetailresultbean
							.setBl_date(convertStringToDate(potrackdetailresultbean
									.getBl_date()));
				}
				// end
				if (poTrackingResultBeanLst != null
						&& poTrackingResultBeanLst.size() > 0) {
					model.addAttribute("poTrackingResultBeanLst",
							poTrackingResultBeanLst);
					return "poTrackingReport";
				} else {
					logger.info("poTrackingResultBeanLst size is either null or zero. ");
					return "poTrackingReport";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Some exception occurred : " + e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");

		return "poTrackingReport";
	}

	// po detail input page
	@RequestMapping(value = "/getPoTrackingReportPage", method = RequestMethod.GET)
	public String getPoTrackingReportPage(Model model) {

		try {
			PoTrackingParams poTrackingParams = new PoTrackingParams();

			model.addAttribute("poTrackingParams", poTrackingParams);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
		}
		return "poTrackingReport";
	}

	@RequestMapping(value = "/getPoTrackingDetail", method = RequestMethod.POST)
	public String getPoTrackingDetail(
			@ModelAttribute("poTrackingParams") PoTrackingParams poTrackingParams,
			Model model, HttpServletRequest request) {

		logger.info("Method PoTrackingController.getPoTrackingDetail starts.");

		PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try {
			if (poTrackingParams != null) {

				LoginDTO loginDto = (LoginDTO) request.getSession()
						.getAttribute("loginDetails");
/*				if (loginDto.getAccgroup().equals("ZMSP")
						|| loginDto.getAccgroup().equals("ZMFF")) {
					poTrackingParams.setShipper_no(loginDto
							.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
				}
				if (loginDto.getAccgroup().equals("ZMBY")
						|| loginDto.getAccgroup().equals("ZMBH")) {
					poTrackingParams
							.setBuyer_no(loginDto.getLoggedInUserName());
					poTrackingParams.setShipper_no("");
				}*/

				if (poTrackingParams.getStatusType().equalsIgnoreCase(
						CommonConstant.STATUS_TYPE_NONE)) {
					poTrackingParams.setStatusType("");
				}
				poTrackingParams.setDivision(loginDto.getDivisionSelected());
				poTrackingParams.setDistChan(loginDto.getDisChnlSelected());
				if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("SE")) {
					poTrackingParams.setAction("09");
				} else if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("AR")) {
					poTrackingParams.setAction("10");
				}
				
				poTrackingParams.setCustomer(loginDto.getLoggedInUserName());
				poTrackingParams.setSalesOrg(loginDto.getSalesOrgSelected());


				StringBuffer webServiceUrl = new StringBuffer(
						RestUtil.PO_TRACKING_INFO);

				poTrackingJsonOutputData = restService.postForObject(RestUtil
						.prepareUrlForService(webServiceUrl).toString(),
						poTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingResultBean> poTrackingResultBeanLst = poTrackingJsonOutputData
						.getPoTrackingResultData();

				poTrackingResultBeanLst = getPoTrackingStatus(poTrackingResultBeanLst);
				

				if (poTrackingResultBeanLst != null && poTrackingResultBeanLst.size() > 0) {
					model.addAttribute("poTrackingResultBeanLst",
							poTrackingResultBeanLst);
					model.addAttribute("poTrackingParams", poTrackingParams);
					return "poTracking";
				} else {
					logger.info("poTrackingResultBeanLst size is either null or zero. ");
					model.addAttribute("poTrackingParams", poTrackingParams);

					return "poTracking";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Some exception occurred : " + e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");

		return "poTracking";
	}

	@RequestMapping(value = "/getPoTrackingPage", method = RequestMethod.GET)
	public String getPoTrackingPage(Model model) {

		try {
			PoTrackingParams poTrackingParams = new PoTrackingParams();

			model.addAttribute("poTrackingParams", poTrackingParams);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
		}
		return "poTracking";
	}

	@RequestMapping(value = "/getPoExceptionReport", method = RequestMethod.POST)
	public String getPoExceptionReport(
			@ModelAttribute("poTrackingParams") PoTrackingParams poTrackingParams,
			Model model, HttpServletRequest request) {

		logger.info("Method PoTrackingController.getPoTrackingDetail starts.");

		PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try {
			if (poTrackingParams != null) {

				LoginDTO loginDto = (LoginDTO) request.getSession()
						.getAttribute("loginDetails");
/*				if (loginDto.getAccgroup().equals("ZMSP")
						|| loginDto.getAccgroup().equals("ZMFF")) {
					poTrackingParams.setShipper_no(loginDto
							.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
					poTrackingParams.setAgent_no("");
				}
				if (loginDto.getAccgroup().equals("ZMBY")
						|| loginDto.getAccgroup().equals("ZMBH")) {
					poTrackingParams
							.setBuyer_no(loginDto.getLoggedInUserName());
					poTrackingParams.setShipper_no("");
					poTrackingParams.setAgent_no("");
				}
				if (loginDto.getAccgroup().equals("ZMDA")
						|| loginDto.getAccgroup().equals("ZMOA")) {
					poTrackingParams
							.setAgent_no(loginDto.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
					poTrackingParams.setShipper_no("");
				}*/

				poTrackingParams.setStatusType("");
				poTrackingParams.setDivision(loginDto.getDivisionSelected());
				poTrackingParams.setDistChan(loginDto.getDisChnlSelected());
				if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("SE")) {
					poTrackingParams.setAction("09");
				} else if (loginDto.getDisChnlSelected().equals("EX")
						&& loginDto.getDivisionSelected().equals("AR")) {
					poTrackingParams.setAction("10");
				}
				
				poTrackingParams.setCustomer(loginDto.getLoggedInUserName());
				poTrackingParams.setSalesOrg(loginDto.getSalesOrgSelected());

				StringBuffer webServiceUrl = new StringBuffer(
						RestUtil.PO_TRACKING_INFO);

				poTrackingJsonOutputData = restService.postForObject(RestUtil
						.prepareUrlForService(webServiceUrl).toString(),
						poTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingResultBean> poTrackingResultBeanLst = poTrackingJsonOutputData
						.getPoTrackingResultData();

				poTrackingResultBeanLst = getPoTrackingStatus(poTrackingResultBeanLst);
				if (poTrackingParams.getExpStatusType() != null) {
					poTrackingResultBeanLst = getPoExceptionStatus(
							poTrackingResultBeanLst, poTrackingParams);
				}

				if (poTrackingResultBeanLst != null
						&& poTrackingResultBeanLst.size() > 0) {
					model.addAttribute("poTrackingResultBeanLst",
							poTrackingResultBeanLst);
					model.addAttribute("poTrackingParams", poTrackingParams);
					return "poExceptionReport";
				} else {
					logger.info("poTrackingResultBeanLst size is either null or zero. ");
					model.addAttribute("poTrackingParams", poTrackingParams);

					return "poExceptionReport";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Some exception occurred : " + e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");

		return "poExceptionReport";
	}

	@RequestMapping(value = "/getExceptionReportPage", method = RequestMethod.GET)
	public String getPoExceptionReportPage(Model model,
			HttpServletRequest request) {
		PoTrackingParams poTrackingParams = new PoTrackingParams();

		try {
			
	/*		Date fromDate = new Date();
			fromDate.setMonth(fromDate.getMonth() - 1);*/
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute(
					"loginDetails");

/*			poTrackingParams.setFromDate(DateUtil.formatDateToString(fromDate));
			poTrackingParams.setToDate(DateUtil.formatDateToString(new Date()));*/
			poTrackingParams.setDivision(loginDto.getDivisionSelected());
			//return getPoExceptionReport(poTrackingParams, model, request);

			 model.addAttribute("poTrackingParams", poTrackingParams);
			 return "poExceptionReport";
		} catch (Exception e) {
			model.addAttribute("poTrackingParams",poTrackingParams);
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
			return "poExceptionReport";
		}

	}

	@RequestMapping(value = "/poTracking", method = RequestMethod.POST)
	public String getPoTracking(
			@ModelAttribute("poTrackingParams") PoTrackingParams poTrackingParams,
			Model model, HttpServletRequest request) {

		logger.info("Method PoTrackingController.getPoTracking starts.");

		PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try {
			if (poTrackingParams != null) {

				LoginDTO loginDto = (LoginDTO) request.getSession()
						.getAttribute("loginDetails");
				/*
				 * if(loginDto.getAccgroup().equals("ZMSP")||loginDto.getAccgroup
				 * ().equals("ZMFF")){
				 * poTrackingParams.setShipper_no(loginDto.getLoggedInUserName
				 * ()); poTrackingParams.setBuyer_no(""); }
				 * if(loginDto.getAccgroup
				 * ().equals("ZMBY")||loginDto.getAccgroup().equals("ZMBH")){
				 * poTrackingParams.setBuyer_no(loginDto.getLoggedInUserName());
				 * poTrackingParams.setShipper_no(""); }
				 */

				poTrackingParams.setCustomer(loginDto.getLoggedInUserName());
				poTrackingParams.setDistChan(loginDto.getDisChnlSelected());
				poTrackingParams.setSalesOrg(loginDto.getSalesOrgSelected());
				poTrackingParams.setStatusType("");
				poTrackingParams.setDivision(loginDto.getDivisionSelected());

				StringBuffer webServiceUrl = new StringBuffer(
						RestUtil.PO_TRACKING);

				poTrackingJsonOutputData = restService.postForObject(RestUtil
						.prepareUrlForService(webServiceUrl).toString(),
						poTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingDetailResultBean> poTrackingResultBeanLst = poTrackingJsonOutputData
						.getPoTrackingDetailResultData();

				poTrackingResultBeanLst = getPoDetailTrackingStatus(poTrackingResultBeanLst);
				
				if (poTrackingParams.getExpStatusType() != null) {
					poTrackingResultBeanLst = getPoDetailExceptionStatus(
							poTrackingResultBeanLst, poTrackingParams);
				}

				if (poTrackingResultBeanLst != null
						&& poTrackingResultBeanLst.size() > 0) {
					model.addAttribute("poTrackingResultBeanLst",
							poTrackingResultBeanLst);
					model.addAttribute("poTrackingParams", poTrackingParams);
					return "poTrackingPage";
				} else {
					logger.info("poTrackingResultBeanLst size is either null or zero. ");
					model.addAttribute("poTrackingParams", poTrackingParams);

					return "poTrackingPage";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("Some exception occurred : " + e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");

		return "poTrackingPage";
	}

	/*
	 * @RequestMapping(value = "/poTrackingPage", method = RequestMethod.GET)
	 * public String getPoTrackingPage(Model model, HttpServletRequest request){
	 * 
	 * try{ PoTrackingParams poTrackingParams = new PoTrackingParams(); Date
	 * fromDate = new Date(); fromDate.setMonth(fromDate.getMonth() - 1);
	 * LoginDTO loginDto =
	 * (LoginDTO)request.getSession().getAttribute("loginDetails");
	 * 
	 * poTrackingParams.setFromDate(DateUtil.formatDateToString(fromDate));
	 * poTrackingParams.setToDate(DateUtil.formatDateToString(new Date()));
	 * poTrackingParams.setDivision(loginDto.getDivisionSelected()); return
	 * getPoTracking(poTrackingParams, model, request);
	 * 
	 * //model.addAttribute("poTrackingParams", poTrackingParams);
	 * }catch(Exception e){ e.printStackTrace();
	 * logger.debug("Some exception occurred : ", e); return "poTrackingPage"; }
	 * 
	 * }
	 */

	@RequestMapping(value = "/poTrackingPage", method = RequestMethod.GET)
	public String getPoTrackingPage(Model model, HttpServletRequest request) {

		try {
			PoTrackingParams poTrackingParams = new PoTrackingParams();
			Date fromDate = new Date();
			fromDate.setMonth(fromDate.getMonth() - 1);
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute(
					"loginDetails");

			poTrackingParams.setFromDate(DateUtil.formatDateToString(fromDate));
			poTrackingParams.setToDate(DateUtil.formatDateToString(new Date()));
			poTrackingParams.setDivision(loginDto.getDivisionSelected());
			return getPoTracking(poTrackingParams, model, request);

			// model.addAttribute("poTrackingParams", poTrackingParams);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
			return "poTrackingPage";
		}

	}

	@RequestMapping(value = "/getExceptionReportByStatus", method = RequestMethod.GET)
	public String getPoExceptionReportByStatus(
			@RequestParam("status") String status, Model model,
			HttpServletRequest request) {

		try {
			PoTrackingParams poTrackingParams = new PoTrackingParams();
			Date fromDate = new Date();
			fromDate.setMonth(fromDate.getMonth() - 1);
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute(
					"loginDetails");

			poTrackingParams.setFromDate(DateUtil.formatDateToString(fromDate));
			poTrackingParams.setToDate(DateUtil.formatDateToString(new Date()));
			poTrackingParams.setExpStatusType(status);
			poTrackingParams.setDivision(loginDto.getDivisionSelected());
			return getPoExceptionReport(poTrackingParams, model, request);

			// model.addAttribute("poTrackingParams", poTrackingParams);
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
			return "poExceptionReport";
		}

	}

	@ModelAttribute("statusTypes")
	public Map<String, String> getCompanyCodes(HttpServletRequest request) {
		
		LoginDTO loginDto = (LoginDTO) request.getSession()
				.getAttribute("loginDetails");

		Map<String, String> statusTypes = new LinkedHashMap<String, String>();
		statusTypes.put(CommonConstant.STATUS_TYPE_ORDER_BOOKED,
				CommonConstant.STATUS_TYPE_ORDER_BOOKED);
		statusTypes.put(CommonConstant.STATUS_TYPE_GR_DONE,
				CommonConstant.STATUS_TYPE_GR_DONE);
		if (loginDto.getDisChnlSelected().equals("EX")
				&& loginDto.getDivisionSelected().equals("SE")) {
			statusTypes.put(CommonConstant.STATUS_TYPE_STUFFING_DONE,
					CommonConstant.STATUS_TYPE_STUFFING_DONE);
		}
		if (loginDto.getDisChnlSelected().equals("EX")
				&& loginDto.getDivisionSelected().equals("AR")) {
			statusTypes.put(CommonConstant.STATUS_TYPE_SHIPMENT_DONE,
					CommonConstant.STATUS_TYPE_SHIPMENT_DONE);
		}


		return statusTypes;
	}

	@ModelAttribute("exceptionStatusTypes")
	public Map<String, String> getExceptionStatusCodes() {

		Map<String, String> exceptionStatusTypes = new LinkedHashMap<String, String>();
		exceptionStatusTypes.put(CommonConstant.EXP_STATUS_DELAYED, "DELAY");
		exceptionStatusTypes.put(CommonConstant.EXP_STATUS_ON_TIME,
				CommonConstant.EXP_STATUS_ON_TIME);
		exceptionStatusTypes.put(CommonConstant.EXP_STATUS_ADVANCE,
				"EARLY ARRIVAL");

		return exceptionStatusTypes;
	}

	private List<PoTrackingResultBean> getPoTrackingStatus(
			List<PoTrackingResultBean> poTrackingResultBeanLst) {
		List<PoTrackingResultBean> poTrackingStautsList = new ArrayList<PoTrackingResultBean>();
		for (PoTrackingResultBean poTrackingResult : poTrackingResultBeanLst) {
//			if (poTrackingResult.getBooking_date() != null) {
//				poTrackingResult
//						.setPoStatus(CommonConstant.STATUS_TYPE_ORDER_BOOKED);
//				if (poTrackingResult.getGr_date() != null) {
//					poTrackingResult
//							.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_RECEIVED);
//					if (poTrackingResult.getShipment_date() != null) {
//						poTrackingResult
//								.setPoStatus(CommonConstant.STATUS_TYPE_STUFFING_DONE);
//						if (poTrackingResult.getEtd() != null) {
//							Date toDate = new Date();
//							// SimpleDateFormat dt1 = new
//							// SimpleDateFormat("yyyyy-mm-dd");
//							if (poTrackingResult.getEtd().compareTo(toDate) < 0) {
//								poTrackingResult
//										.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_DEPARTED);
//							}
//						}
//						if (poTrackingResult.getEta() != null) {
//							Date toDate = new Date();
//							if (poTrackingResult.getEta().compareTo(toDate) < 0) {
//								poTrackingResult
//										.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_ARRIVED);
//							}
//						}
//
//					}
//				}
//			}
			if (poTrackingResult.getEta() != null) {
				if (poTrackingResult.getAta() != null) {

					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) > 0) {
						poTrackingResult.setShipmentStatus("DELAYED");
					}
					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) < 0) {
						poTrackingResult.setShipmentStatus("ADVANCE");
					}
					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) == 0) {
						poTrackingResult.setShipmentStatus("ON-TIME");
					}
				} else {
					if (poTrackingResult.getFeta() != null) {

						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) > 0) {
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) < 0) {
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) == 0) {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					} else {
						poTrackingResult.setShipmentStatus("ON-TIME");
					}
				}
			} else {
				if (poTrackingResult.getEtd() != null) {
					if (poTrackingResult.getAtd() != null) {

						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) > 0) {
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) < 0) {
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) == 0) {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					} else {
						if (poTrackingResult.getFetd() != null) {

							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) > 0) {
								poTrackingResult.setShipmentStatus("DELAYED");
							}
							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) < 0) {
								poTrackingResult.setShipmentStatus("ADVANCE");
							}
							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) == 0) {
								poTrackingResult.setShipmentStatus("ON-TIME");
							}
						} else {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					}
				} else {
					poTrackingResult.setShipmentStatus("ON-TIME");
				}
			}
			poTrackingStautsList.add(poTrackingResult);
		}
		return poTrackingStautsList;
	}

	private List<PoTrackingResultBean> getPoExceptionStatus(
			List<PoTrackingResultBean> poTrackingResultBeanLst,
			PoTrackingParams poTrackingParams) {
		List<PoTrackingResultBean> poTrackingStautsList = new ArrayList<PoTrackingResultBean>();
		for (PoTrackingResultBean poTrackingResult : poTrackingResultBeanLst) {
			if (!poTrackingParams.getExpStatusType().equals("NONE")) {
				if (poTrackingResult.getShipmentStatus().equals(
						poTrackingParams.getExpStatusType())) {
					poTrackingStautsList.add(poTrackingResult);
				}
			} else {
				poTrackingStautsList.add(poTrackingResult);
			}
		}
		return poTrackingStautsList;
	}

	private List<PoTrackingDetailResultBean> getPoDetailTrackingStatus(
			List<PoTrackingDetailResultBean> poTrackingResultBeanLst) {
		List<PoTrackingDetailResultBean> poTrackingStautsList = new ArrayList<PoTrackingDetailResultBean>();
		for (PoTrackingDetailResultBean poTrackingResult : poTrackingResultBeanLst) {
			if (poTrackingResult.getBooking_date() != null) {
				poTrackingResult
						.setPoStatus(CommonConstant.STATUS_TYPE_ORDER_BOOKED);
				if (poTrackingResult.getGr_date() != null) {
					poTrackingResult
							.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_RECEIVED);
					if (poTrackingResult.getShipment_date() != null) {
						poTrackingResult
								.setPoStatus(CommonConstant.STATUS_TYPE_STUFFING_DONE);
						if (poTrackingResult.getEtd() != null) {
							Date toDate = new Date();
							// SimpleDateFormat dt1 = new
							// SimpleDateFormat("yyyyy-mm-dd");
							if (poTrackingResult.getEtd().compareTo(toDate) < 0) {
								poTrackingResult
										.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_DEPARTED);
							}
						}
						if (poTrackingResult.getEta() != null) {
							Date toDate = new Date();
							if (poTrackingResult.getEta().compareTo(toDate) < 0) {
								poTrackingResult
										.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_ARRIVED);
							}
						}

					}
				}
			}
			if (poTrackingResult.getEta() != null) {
				if (poTrackingResult.getAta() != null) {

					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) > 0) {
						poTrackingResult.setShipmentStatus("DELAYED");
					}
					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) < 0) {
						poTrackingResult.setShipmentStatus("ADVANCE");
					}
					if (poTrackingResult.getAta().compareTo(
							poTrackingResult.getEta()) == 0) {
						poTrackingResult.setShipmentStatus("ON-TIME");
					}
				} else {
					if (poTrackingResult.getFeta() != null) {

						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) > 0) {
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) < 0) {
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if (poTrackingResult.getFeta().compareTo(
								poTrackingResult.getEta()) == 0) {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					} else {
						poTrackingResult.setShipmentStatus("ON-TIME");
					}
				}
			} else {
				if (poTrackingResult.getEtd() != null) {
					if (poTrackingResult.getAtd() != null) {

						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) > 0) {
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) < 0) {
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if (poTrackingResult.getAtd().compareTo(
								poTrackingResult.getEtd()) == 0) {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					} else {
						if (poTrackingResult.getFetd() != null) {

							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) > 0) {
								poTrackingResult.setShipmentStatus("DELAYED");
							}
							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) < 0) {
								poTrackingResult.setShipmentStatus("ADVANCE");
							}
							if (poTrackingResult.getFetd().compareTo(
									poTrackingResult.getEtd()) == 0) {
								poTrackingResult.setShipmentStatus("ON-TIME");
							}
						} else {
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					}
				} else {
					poTrackingResult.setShipmentStatus("ON-TIME");
				}
			}
			poTrackingStautsList.add(poTrackingResult);
		}
		return poTrackingStautsList;
	}

	private List<PoTrackingDetailResultBean> getPoDetailExceptionStatus(
			List<PoTrackingDetailResultBean> poTrackingResultBeanLst,
			PoTrackingParams poTrackingParams) {
		List<PoTrackingDetailResultBean> poTrackingStautsList = new ArrayList<PoTrackingDetailResultBean>();
		for (PoTrackingDetailResultBean poTrackingResult : poTrackingResultBeanLst) {
			if (!poTrackingParams.getExpStatusType().equals("NONE")) {
				if (poTrackingResult.getShipmentStatus().equals(
						poTrackingParams.getExpStatusType())) {
					poTrackingStautsList.add(poTrackingResult);
				}
			} else {
				poTrackingStautsList.add(poTrackingResult);
			}
		}
		return poTrackingStautsList;
	}

	// hamid
	public String convertStringToDate(String dateString) {
		Date date = null;
		String formatteddate = null;
		DateFormat before = new SimpleDateFormat("yyyyMMdd");
		DateFormat after = new SimpleDateFormat("dd-MMM-yy");
		try {
			date = before.parse(dateString);
			formatteddate = after.format(date);
		} catch (Exception ex) {
			System.out.println(ex);
			formatteddate = dateString;
		}
		return formatteddate;
	}

}
