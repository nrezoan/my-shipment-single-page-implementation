package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.NegoBillLanding;
import com.myshipment.model.RequestParams;
import com.myshipment.model.StuffingDetail;
import com.myshipment.model.StuffingHeader;
import com.myshipment.model.StuffingReportData;
import com.myshipment.model.StuffingReportJasper;
import com.myshipment.model.StuffingReportTotalData;
import com.myshipment.service.IStuffingReport;
import com.myshipment.util.DateUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
/**
 * @author Gufranur Rahman
 * @author Nusrat Momtahana
 *
 */
@Controller
public class StuffingAdviceController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Autowired
	private IStuffingReport stuffingService;
	
	private Logger logger = Logger.getLogger(StuffingAdviceController.class);
	
	@RequestMapping(value="/stuffingAdvice", method = RequestMethod.GET )
	public String getStuffingAdvice(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");		
		RequestParams req = new RequestParams();
		model.addAttribute("req", req);
		return "stuffingSearch";
	}

	@RequestMapping(value = "/stuffingAdviceReport",params = "btn-appr-po-search", method = RequestMethod.POST)
	public String getStuffingDetail(@ModelAttribute("req") RequestParams req, Model model, HttpSession session) {
		
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		String blno = req.getReq1().toUpperCase();
		req.setReq1(blno);
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1() + ":" + req.getReq2());
		StuffingReportData stuffingReportData = null;
		stuffingReportData = stuffingService.getStuffingDetail(req);
		List<StuffingHeader> headeritems= stuffingReportData.getHeaderitem();
		List<StuffingDetail> detailItems=stuffingReportData.getBapiItem();
		List<NegoBillLanding> itAddress =stuffingReportData.getItAddress();
		StuffingReportTotalData stuffingReportTotalData = preparTotalData(stuffingReportData);
		model.addAttribute("headeritems",headeritems);
		model.addAttribute("detailItem",detailItems);
		model.addAttribute("itAddress",itAddress);
		model.addAttribute("stuffingReportTotalData", stuffingReportTotalData);
		
		//return stuffingReportData;
		return "stuffingAdvice";
	}
	
	@RequestMapping(value = "/stuffingAdviceReport",params = "download", method = RequestMethod.POST)
	public ModelAndView getStuffingDetail(@ModelAttribute("req") RequestParams req, Model model, 
			HttpSession session,HttpServletResponse response,HttpServletRequest request,
			ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result) { 

		try {
			logger.debug(this.getClass().getName() + "performLogin():  Start ");
			LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
			String customerNo = null;
			String distChannel = null;
			String division = null;
			String salesOrg = null;
			if (loginDto != null) {
				customerNo = loginDto.getLoggedInUserName();
				distChannel = loginDto.getDisChnlSelected();
				division = loginDto.getDivisionSelected();
				salesOrg = loginDto.getSalesOrgSelected();

				logger.info("Customer No found as : " + customerNo);
			}
			String blno = req.getReq1().toUpperCase();
			req.setReq1(blno);
			req.setReq2(salesOrg);
			req.setReq3(distChannel);
			req.setReq4(division);
			req.setReq5(customerNo);

			logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:"
					+ req.getReq1() + ":" + req.getReq2());
			StuffingReportData stuffingReportData = null;
			stuffingReportData = stuffingService.getStuffingDetail(req);
			List<StuffingReportJasper> actualPDFData = new ArrayList<StuffingReportJasper>();

			StuffingReportJasper pdfData = new StuffingReportJasper();

			String reportType = request.getParameter("type");
			reportType = reportType == null ? "stuffingReport" : reportType;

			Map<String, Object> parameterMap = new HashMap<String, Object>();
			List<StuffingHeader> headeritems = stuffingReportData.getHeaderitem();
			List<StuffingDetail> detailItems = stuffingReportData.getBapiItem();
			List<NegoBillLanding> itAddress = stuffingReportData.getItAddress();

			StuffingHeader table1 = headeritems.get(0);
			StuffingDetail stuffDate = detailItems.get(0);
			NegoBillLanding address = itAddress.get(0);

			pdfData.setAddress1(address.getName1());
			pdfData.setAddress2(address.getName2());
			pdfData.setAddress3(address.getName3());
			pdfData.setAddress4(address.getName4());
			pdfData.setCity(address.getCity1());
			pdfData.setCountry(address.getLandX());
			pdfData.setShipperName(table1.getName1());
			pdfData.setBuyerName(table1.getName2());
			pdfData.setHbl(table1.getZzHblHawbNo());
			pdfData.setBookingDt(DateUtil.formatDateToString(table1.getAuDat()));
			pdfData.setComInvNo(table1.getZzComInvNo());
			pdfData.setComInvDt(DateUtil.formatDateToString(table1.getZzCommInvDt()));
			pdfData.setPol(table1.getZzPortOfLoading());
			pdfData.setDischPort(table1.getZzPortOfDest());
			pdfData.setPod(table1.getZzPlacedelivery());
			pdfData.setEtd(DateUtil.formatDateToString(table1.getZzDepartureDt()));
			pdfData.setEta(DateUtil.formatDateToString(table1.getZzEta()));
			pdfData.setTos(table1.getZzFreightModeDes());
			pdfData.setFvsl(table1.getZzAirlineName1() + " " + table1.getZzAirLineNo1());
			pdfData.setMvsl(table1.getZzAirlineNam21() + " " + table1.getZzAirLineNo2());
			pdfData.setDeliveryDt(DateUtil.formatDateToString(table1.getZzEta()));
			pdfData.setContractNo(table1.getZzLcpottNo());
			pdfData.setContractDt(DateUtil.formatDateToString(table1.getZzLcDt()));
			pdfData.setTransPort(DateUtil.formatDateToString(table1.getMvEtDport()));
			pdfData.setStuffingDt(DateUtil.formatDateToString(stuffDate.getZzStuffingDate()));
			pdfData.setCompanyCode(table1.getCompanyCode());
			pdfData.setCargoDt(DateUtil.formatDateToString(table1.getGrDt()));

			actualPDFData.add(pdfData);

			for (int i = 0; i < detailItems.size(); i++) {
				StuffingDetail table2 = detailItems.get(i);
				StuffingReportJasper pdfDataTable = new StuffingReportJasper();
				pdfDataTable.setOrderNo(table2.getZzPoNumber());
				pdfDataTable.setSkuNo(table2.getZzSkuNo());
				pdfDataTable.setStyleNo(table2.getZzStyleNo());
				pdfDataTable.setColor(table2.getZzColour());
				pdfDataTable.setContainerNo(table2.getVhIlm());
				pdfDataTable.setContainerSize(table2.getWgbEZ60());
				pdfDataTable.setContainerSeal(table2.getVhIlmKu());
				pdfDataTable.setFreightMode(table2.getZzMode());
				pdfDataTable.setCartonQty(table2.getZzQuantity());
				pdfDataTable.setNoOfPcs(table2.getZzTotalNoPcs());
				pdfDataTable.setCbm(table2.getZzVolume());
				pdfDataTable.setGrWt(table2.getZzGrWt());
			//	pdfDataTable.setStuffingDt(DateUtil.formatDateToString(table2.getZzStuffingDate()));
				actualPDFData.add(pdfDataTable);
			}
			JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);
			parameterMap.put("datasource", pdfDataSource);
			ServletContext context = request.getSession().getServletContext();
			String path = context.getRealPath("/") + "";
			parameterMap.put("Context", path);
			modelAndView = new ModelAndView(reportType, parameterMap);

			return modelAndView;
		}

		catch (IndexOutOfBoundsException ex) {
			// TODO Auto-generated catch block
			modelAndView.setViewName("redirect:/stuffingAdvice");
    		redirecAttributes.addFlashAttribute("message","No Records Found");
    		return modelAndView;
	}	
		
	}
	
	
	private StuffingReportTotalData preparTotalData(StuffingReportData stuffingReportData){
		
		StuffingReportTotalData stuffingReportTotalData = new StuffingReportTotalData();
		int endIndex = stuffingReportData.getTotKgs().length() - 1;
		stuffingReportTotalData.setTotKgs(stuffingReportData.getTotKgs().substring(0, endIndex));
        Double totalPieces = Double.parseDouble(stuffingReportData.getTotPcs());
        stuffingReportTotalData.setTotPcs(totalPieces.intValue()+"");
		//stuffingReportTotalData.setTotPcs(stuffingReportData.getTotPcs());
        Double totalQty = Double.parseDouble(stuffingReportData.getTotQty());
        stuffingReportTotalData.setTotQty(totalQty.intValue()+"");
		//stuffingReportTotalData.setTotQty(stuffingReportData.getTotQty());
		stuffingReportTotalData.setTotVol(stuffingReportData.getTotVol());
		return stuffingReportTotalData;
	}
/*
	@RequestMapping(value = "/distibutionchannels", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getDistChnlBySalesOrg(@RequestParam("salesOrg") String salesOrg, HttpServletRequest request) {
		HttpSession session = request.getSession();
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		SalesOrgTree salesOrgTree = loginDto.getMpSalesOrgTree().get(salesOrg);
		List<String> listOfDistChanel = new ArrayList<>();
		Set<DistributionChannel> lstDistChnl = salesOrgTree.getLstDistChnl();
		for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
			DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
			listOfDistChanel.add(distributionChannel.getDistrChan());

		}
		loginDto.setSalesOrgSelected(salesOrg);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		return listOfDistChanel;
	}*/

	/*@RequestMapping(value = "/homepage", method = RequestMethod.GET)
	public String getDashBoard(@RequestParam("companyName") String companyName, @RequestParam("options") String options,
			Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();
		loginDto.setSalesOrgSelected(companyName);
		loginDto.setDisChnlSelected(options);
		SupRequestParams supRequestParams = new SupRequestParams();
		Set<Division> lstDivision=null;
		SalesOrgTree salesOrgTree=loginDto.getMpSalesOrgTree().get(loginDto.getSalesOrgSelected());
		for (Iterator iterator = salesOrgTree.getLstDistChnl().iterator(); iterator.hasNext();) {
			DistributionChannel type = (DistributionChannel) iterator.next();
			if(type.getDistrChan().equals(loginDto.getDisChnlSelected()))
				lstDivision=type.getLstDivision();
			
			
		}
		Division division=new Division();
		
		if(lstDivision!=null && lstDivision.size()>0)
		{
			
		}
			
		supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
		supRequestParams.setDistChan(loginDto.getDisChnlSelected());
		supRequestParams.setDivision("SE");
		supRequestParams.setSalesOrg(loginDto.getSalesOrgSelected());
		SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams, session);

		session.setAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA, supplierDetailsDTO);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		
		return "dashboard";
	}
	
	@RequestMapping(value="/headerdetails",method=RequestMethod.GET)
	@ResponseBody
	public LoginDTO getHeaderDetails(Model model,HttpServletRequest request)
	{
		HttpSession session=SessionUtil.getHttpSession(request);
		LoginDTO loginDto=(LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		return loginDto;
	}
	
	

	@RequestMapping(value = "/currentSelectedOrg", method = RequestMethod.GET)
	public String getCurrentSelestedOrgDistChnl(@RequestParam("salesOrg") String salesOrg,
			@RequestParam("distributionChannel") String distributionChannel, HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();
		loginDto.setSalesOrgSelected(salesOrg);
		loginDto.setDisChnlSelected(distributionChannel);
		SupRequestParams supRequestParams = new SupRequestParams();
		supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
		supRequestParams.setDistChan(loginDto.getDisChnlSelected());
		supRequestParams.setDivision(loginDto.getDivisionSelected());

		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		model.addAttribute("loginDetails", loginDto);
		return "dashboard";
	}
*/
	
	//hamid
	//22.03.2017
	@RequestMapping(value = "/stuffingAdviceFrmUrl", method = RequestMethod.GET)
	public String getStuffingDetailFromUrl(@ModelAttribute("req") RequestParams req, Model model, HttpSession session, @RequestParam("searchString") String searchValue) { 
			
			
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel=null;
		String division=null;
		String salesOrg=null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			distChannel=loginDto.getDisChnlSelected();
			division=loginDto.getDivisionSelected();
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		
		req.setReq1(searchValue);
		req.setReq2(salesOrg);
		req.setReq3(distChannel);
		req.setReq4(division);
		req.setReq5(customerNo);
		
		logger.debug(this.getClass().getName() + "getStuffingDetail():  Call Service with Parameter:" + req.getReq1()
				+ ":" + req.getReq2());
		StuffingReportData stuffingReportData = null;
		stuffingReportData = stuffingService.getStuffingDetail(req);
		List<StuffingHeader> headeritems= stuffingReportData.getHeaderitem();
		List<StuffingDetail> detailItems=stuffingReportData.getBapiItem();
		List<NegoBillLanding> itAddress =stuffingReportData.getItAddress();
		StuffingReportTotalData stuffingReportTotalData = preparTotalData(stuffingReportData);
		model.addAttribute("headeritems",headeritems);
		model.addAttribute("detailItem",detailItems);
		model.addAttribute("itAddress",itAddress);
		model.addAttribute("stuffingReportTotalData", stuffingReportTotalData);
		
		//return stuffingReportData;
		return "stuffingAdvice";
	}
	
}
