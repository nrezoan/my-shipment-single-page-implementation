package com.myshipment.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myshipment.dto.DistributionChannel;
import com.myshipment.dto.Division;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SalesOrgTree;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.DefaultCompany;
import com.myshipment.model.ExceptionBean;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
import com.myshipment.model.IntelligentCompanyUsage;
import com.myshipment.model.LastNShipmentsJsonOutputData;
import com.myshipment.model.Login;
import com.myshipment.model.LoginMdl;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.MyshipmentTrackJsonOutputData;
import com.myshipment.model.MyshipmentTrackParams;
import com.myshipment.model.PoTrackingDetailResultBean;
import com.myshipment.model.PoTrackingJsonOutputData;
import com.myshipment.model.PoTrackingParams;
import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertHBLDetails;
import com.myshipment.model.RecentShipment;
import com.myshipment.model.SOWiseShipmentSummaryJson;
import com.myshipment.model.SalesorgListBean;
import com.myshipment.model.SupRequestParams;
import com.myshipment.model.SuppWiseShipSummaryJson;
import com.myshipment.model.TopFiveDashboardShipment;
import com.myshipment.model.TopFiveShipBuyerJson;
import com.myshipment.model.TopFiveShipment;
import com.myshipment.model.TopFiveShipmentBuyer;
import com.myshipment.service.ICompanySelectionService;
import com.myshipment.service.IDashboardService;
import com.myshipment.service.ILoginService;
import com.myshipment.service.IPreAlertManagementService;
import com.myshipment.service.OrderServiceImpl;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.EncryptDecryptUtil;
import com.myshipment.util.MyShipApplicationException;
import com.myshipment.util.MyshipmentJspMapper;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;

/**
 * @author Virendra Kumar
 *
 */
@Controller
public class LoginController extends BaseController {

	@Autowired
	ILoginService loginService;
	@Autowired
	RestService restService;

	@Autowired
	private IDashboardService dashboardService;

	@Autowired
	private ICompanySelectionService companySelectionService;

	@Autowired
	private IPreAlertManagementService preAlertManagementService;

	@Autowired
	private OrderServiceImpl orderService;
	private Logger logger = Logger.getLogger(LoginController.class);

	/*
	 * @Autowired private Mailer mailer;
	 */

	@RequestMapping("/oldMyshipment")
	public String getOldMyshipment(Model model, @ModelAttribute("error") String message, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return "landingpage_v2";
	}

	@RequestMapping("/myshipmentLogin")
	public String getMyshipmentLogin(Model model, @ModelAttribute("error") String message, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes redirectAttributes) {
		return "login";
	}

	@RequestMapping("/")
	public String getLoginPage(Model model, @ModelAttribute("error") String message, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes redirectAttributes) {

		logger.info(this.getClass().getName() + " Returning Login Page");

		model.addAttribute("error", message);
		// sendMail();
		LoginMdl login = new LoginMdl();
		Cookie[] cookies = request.getCookies();
		int success = 0;
		if (cookies != null && cookies.length > 2) {
			for (Cookie cookie : cookies) {
				Cookie ck = cookie;

				if (ck.getName().equalsIgnoreCase("customerCode")) {
					if (!ck.getValue().equals("") && ck.getValue() != null) {
						login.setCustomerCode(ck.getValue());
						success = success + 1;
					}
				}
				if (ck.getName().equalsIgnoreCase("password")) {
					if (!ck.getValue().equals("") && ck.getValue() != null) {
						login.setPassword(ck.getValue());
						success = success + 1;
					}
				}
			}
			if (success == 2) {
				model.addAttribute("loginMdl", login);
				return getDashBoard(login, model, request, response, redirectAttributes);
			}
			// restService.postForLocation("http://localhost:8080/MyShipmentFrontApp/selectedorg",
			// model);
			// return "redirect:/selectedorgcookies";
		}
		model.addAttribute("loginMdl", new LoginMdl());
		// return "landingpage_v2";
		return MyshipmentJspMapper.LANDING_PAGE;

	}

	/*
	 * @RequestMapping("/signin") public String getSigninPage(Model
	 * model, @ModelAttribute("error") String message, HttpServletRequest request,
	 * HttpServletResponse response, RedirectAttributes redirectAttributes) {
	 * 
	 * logger.info(this.getClass().getName() + " Returning Login Page");
	 * 
	 * model.addAttribute("error", message); // sendMail(); LoginMdl login = new
	 * LoginMdl(); Cookie[] cookies = request.getCookies(); int success = 0; if
	 * (cookies != null && cookies.length > 2) { for (Cookie cookie : cookies) {
	 * Cookie ck = cookie;
	 * 
	 * if (ck.getName().equalsIgnoreCase("customerCode")) { if
	 * (!ck.getValue().equals("") && ck.getValue() != null) {
	 * login.setCustomerCode(ck.getValue()); success = success + 1; } } if
	 * (ck.getName().equalsIgnoreCase("password")) { if (!ck.getValue().equals("")
	 * && ck.getValue() != null) { login.setPassword(ck.getValue()); success =
	 * success + 1; } } } if (success == 2) { model.addAttribute("loginMdl", login);
	 * return getDashBoard(login, model, request, response, redirectAttributes); }
	 * // restService.postForLocation(
	 * "http://localhost:8080/MyShipmentFrontApp/selectedorg", // model); // return
	 * "redirect:/selectedorgcookies"; } model.addAttribute("loginMdl", new
	 * LoginMdl()); return "loginpage";
	 * 
	 * }
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		final String refererUrl = request.getHeader("Referer");
		response.setHeader(refererUrl, "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		SessionUtil.getHttpSession(request).removeAttribute(SessionUtil.LOGIN_DETAILS);
		SessionUtil.getHttpSession(request).removeAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		SessionUtil.getHttpSession(request).invalidate();
		for (Cookie cookie : request.getCookies()) {

			if (cookie.getName().equalsIgnoreCase("customerCode")) {
				cookie.setValue("");
				cookie.setMaxAge(-1);
				response.addCookie(cookie);
			}

			if (cookie.getName().equalsIgnoreCase("password")) {
				cookie.setValue("");
				cookie.setMaxAge(-1);
				response.addCookie(cookie);
			}

		}
		// sendMail();
		return "redirect:/";

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	public LoginDTO performLogin(@RequestParam("customerCode") String customerCode,
			@RequestParam("password") String password, HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "performLogin():  Start ");
		Login login = new Login();
		login.setUserCode(customerCode);
		login.setPassword(password);
		logger.debug(this.getClass().getName() + "performLogin():  Call Service with Parameter:" + login.getUserCode()
				+ ":" + login.getPassword());
		LoginDTO loginDto = null;
		try {
			loginDto = loginService.authenticateAndGetLoginDetails(login);
			EncryptDecryptUtil encryptDecrypt = new EncryptDecryptUtil();
			String encryptedPassword = encryptDecrypt.encrypt(loginDto.getLoggedInUserPassword());
			loginDto.setLoggedInUserPassword(encryptedPassword);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
		if (null != loginDto)
			if (null != loginDto.getBapiReturn2() && loginDto.getBapiReturn2().getMessage() != ""
					&& loginDto.getBapiReturn2().getMessage().trim().equalsIgnoreCase("Login successfull!!".trim())) {
				HttpSession session = SessionUtil.createSession(request);
				Date fromDate = new Date();
				fromDate.setMonth(fromDate.getMonth() - 2);

				loginDto.setDashBoardFromDate(DateUtil.formatDateToString(fromDate));
				loginDto.setDashBoardToDate(DateUtil.formatDateToString(new Date()));
				session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
			}
		return loginDto;
	}

	@RequestMapping(value = "/distibutionchannels", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getDistChnlBySalesOrg(@RequestParam("salesOrg") String salesOrg, HttpServletRequest request) {
		HttpSession session = request.getSession();
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		SalesOrgTree salesOrgTree = loginDto.getMpSalesOrgTree().get(salesOrg);
		List<String> listOfDistChanel = new ArrayList<>();
		Set<DistributionChannel> lstDistChnl = salesOrgTree.getLstDistChnl();
		for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
			DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
			listOfDistChanel.add(distributionChannel.getDistrChan());

		}
		loginDto.setSalesOrgSelected(salesOrg);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		return listOfDistChanel;
	}

	@RequestMapping(value = "/selectedorg", method = RequestMethod.POST)
	public String getDashBoard(@ModelAttribute("loginMdl") LoginMdl loginMdl, Model model, HttpServletRequest request,
			HttpServletResponse response, RedirectAttributes redirectAttributes) {

		String message = validateLoginForm(loginMdl);
		if (message.equalsIgnoreCase("")) {
			logger.debug(this.getClass().getName() + "performLogin():  Start ");
			Login login = new Login();
			login.setUserCode(loginMdl.getCustomerCode());
			login.setPassword(loginMdl.getPassword());
			logger.debug(this.getClass().getName() + "performLogin():  Call Service with Parameter:"
					+ login.getUserCode() + ":" + login.getPassword());
			LoginDTO loginDto = null;
			try {
				loginDto = loginService.authenticateAndGetLoginDetails(login);

				if (loginDto != null) {
					if (loginDto.getLoggedInUserPassword() != null) {
						EncryptDecryptUtil encryptDecrypt = new EncryptDecryptUtil();
						String encryptedPassword = encryptDecrypt.encrypt(loginDto.getLoggedInUserPassword());
						if (encryptedPassword != null) {
							loginDto.setLoggedInUserPassword(encryptedPassword);
						}
					}
				}

			} catch (MyShipApplicationException e) {
				handleMyShipApplicationException(e, response);
				message = e.getMessage();
				if (message.contains("I/O error on POST request")) {
					message = "Error Occured. Please try again.";
				}
				redirectAttributes.addFlashAttribute("error", message);
			}
			logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
			if (null != loginDto)
				if (null != loginDto.getBapiReturn2() && loginDto.getBapiReturn2().getMessage() != "" && loginDto
						.getBapiReturn2().getMessage().trim().equalsIgnoreCase("Login successfull!!".trim())) {

					if (loginMdl.getRemember() != null && loginMdl.getRemember().equals("on")) {
						Cookie customerCode = new Cookie("customerCode", loginDto.getLoggedInUserName());
						customerCode.setMaxAge(1 * 24 * 60 * 60);
						response.addCookie(customerCode);

						Cookie password = new Cookie("password", loginDto.getLoggedInUserPassword());
						password.setMaxAge(1 * 24 * 60 * 60);
						response.addCookie(password);
					}

					HttpSession session = SessionUtil.createSession(request);
					Date fromDate = new Date();
					fromDate.setMonth(fromDate.getMonth() - 1);

					loginDto.setDashBoardFromDate(DateUtil.formatDateToString(fromDate));
					loginDto.setDashBoardToDate(DateUtil.formatDateToString(new Date()));
					session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);

					session = request.getSession();
					loginDto = (LoginDTO) session.getAttribute("loginDetails");

					logger.info(this.getClass() + " : Default Company Exists(Checking)...called from Login Controller");

					DefaultCompany defaultCompany = null;
					try {
						defaultCompany = companySelectionService
								.getActiveDefaultCompany(loginDto.getLoggedInUserName());
					} catch (Exception e) {
						logger.info(this.getClass()
								+ " : Error occured while fetching Data from database for Default Company");
						e.printStackTrace();
					}
					List<IntelligentCompanyUsage> intelligentCompanyUsageList = null;
					if (defaultCompany == null) {
						try {
							intelligentCompanyUsageList = companySelectionService
									.getCompanyUsageList(loginDto.getLoggedInUserName());
						} catch (Exception e) {
							e.printStackTrace();
						}
						IntelligentCompanyUsage companyUsage = null;
						if (intelligentCompanyUsageList != null) {
							if (intelligentCompanyUsageList.size() != 0) {
								companyUsage = intelligentCompanyUsageList.get(0);
							}
						}
						if (companyUsage != null) {
							if (!companyUsage.getCompany().equals("")
									&& !companyUsage.getDistributionChannel().equals("")
									&& !companyUsage.getDivision().equals("")) {

								defaultCompany = new DefaultCompany();

								defaultCompany.setPreferredCompanyId(companyUsage.getCompany());
								defaultCompany.setDistributionChannel(companyUsage.getDistributionChannel());
								defaultCompany.setDivision(companyUsage.getDivision());
							}
						}
					}

					Map<String, SalesOrgTree> treeMap = new TreeMap<String, SalesOrgTree>(loginDto.getMpSalesOrgTree());

					Map.Entry<String, SalesOrgTree> entry = treeMap.entrySet().iterator().next();
					// String key = entry.getKey();
					String key = "";
					String preferredDistributionChannel = "";
					String preferredDivision = "";

					if (defaultCompany == null) {
						key = entry.getKey();
					} else {
						key = defaultCompany.getPreferredCompanyId();
						preferredDistributionChannel = defaultCompany.getDistributionChannel();
						preferredDivision = defaultCompany.getDivision();
					}

					SalesOrgTree salesOrgTree = loginDto.getMpSalesOrgTree().get(key);
					if (salesOrgTree == null) {
						key = entry.getKey();
						salesOrgTree = loginDto.getMpSalesOrgTree().get(key);
					}
					List<String> listOfDistChanel = new ArrayList<>();
					Set<DistributionChannel> lstDistChnl = salesOrgTree.getLstDistChnl();

					for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
						DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
						listOfDistChanel.add(distributionChannel.getDistrChan());

					}

					for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
						DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
						if (preferredDistributionChannel.equalsIgnoreCase("")) {
							if (distributionChannel.getDistrChan().equalsIgnoreCase("EX")) {
								loginDto.setDisChnlSelected("EX");
								break;
							} else {
								loginDto.setDisChnlSelected(distributionChannel.getDistrChan());
							}
						} else {
							if (distributionChannel.getDistrChan().equalsIgnoreCase(preferredDistributionChannel)) {
								loginDto.setDisChnlSelected(preferredDistributionChannel);
							}
						}

					}

					loginDto.setSalesOrgSelected(salesOrgTree.getSalesOrg());
					session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);

					session = SessionUtil.getHttpSession(request);
					loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

					Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();

					loginDto.setSalesOrgSelected(loginDto.getSalesOrgSelected());
					loginDto.setDisChnlSelected(loginDto.getDisChnlSelected());

					SupRequestParams supRequestParams = new SupRequestParams();

					Set<Division> lstDivision = null;
					salesOrgTree = loginDto.getMpSalesOrgTree().get(loginDto.getSalesOrgSelected());

					for (Iterator iterator = salesOrgTree.getLstDistChnl().iterator(); iterator.hasNext();) {
						DistributionChannel type = (DistributionChannel) iterator.next();
						if (type.getDistrChan().equals(loginDto.getDisChnlSelected()))
							lstDivision = type.getLstDivision();

					}
					Division division = new Division();

					if (lstDivision != null && lstDivision.size() > 0) {
						if (!preferredDivision.equals("")) {
							for (Division division1 : lstDivision) {
								if (division1.getDivision().equals(preferredDivision)) {
									loginDto.setDivisionSelected(preferredDivision);
								}
							}
						} else {
							if (lstDivision.size() == 1) {
								for (Division division1 : lstDivision)
									loginDto.setDivisionSelected(division1.getDivision());
							} else if (lstDivision.size() == 2) {
								loginDto.setDivisionSelected(CommonConstant.DEFAULT_DIVISION_SELECTED);
							} else {
								loginDto.setDivisionSelected("SE");
							}
						}

					}
					/*
					 * Map<String,SalesOrgTree> salesOrgDivisionWise = new
					 * HashMap<String,SalesOrgTree>(); for(SalesorgListBean salesOrg :
					 * loginDto.getSalesorgList()) {
					 * if(salesOrg.getDivision().equals(loginDto.getDivisionSelected()) &&
					 * salesOrg.getDistrChan().equals(loginDto.getDisChnlSelected())) {
					 * salesOrgDivisionWise.put(salesOrg.getSalesOrg(), value) } }
					 */

					Map<String, SalesOrgTree> salesOrgTreeLoginDto = loginDto.getMpSalesOrgTree();
					// salesOrgTreeLoginDto.forEach((key, value)->{});
					Map<String, SalesOrgTree> salesOrgDivisionWise = new HashMap<String, SalesOrgTree>();
					for (Map.Entry<String, SalesOrgTree> mapEntry : salesOrgTreeLoginDto.entrySet()) {
						SalesOrgTree salesOrgTemp = (SalesOrgTree) mapEntry.getValue();
						Set<DistributionChannel> distributionChannelList = salesOrgTemp.getLstDistChnl();

						for (DistributionChannel distChannelTemp : distributionChannelList) {
							if (distChannelTemp.getDistrChan().equals(loginDto.getDisChnlSelected())) {
								Set<Division> divisionList = distChannelTemp.getLstDivision();
								for (Division divisionTemp : divisionList) {
									if (divisionTemp.getDivision().equals(loginDto.getDivisionSelected())) {
										salesOrgDivisionWise.put(salesOrgTemp.getSalesOrg(), salesOrgTemp);
									}
								}
							}

						}

					}
					loginDto.setSalesOrgDivisionWise(salesOrgDivisionWise);

					supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
					supRequestParams.setDistChan(loginDto.getDisChnlSelected());
					supRequestParams.setDivision(loginDto.getDivisionSelected());
					supRequestParams.setSalesOrg(loginDto.getSalesOrgSelected());

					SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams,
							session);

					session.setAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA, supplierDetailsDTO);

					session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
					Gson gson = new Gson();

					session.setAttribute("loginDetailsJson", gson.toJson(loginDto));

					return "redirect:/homepage";
				} else {
					message = "Invalid Customer Id or Password";
					redirectAttributes.addFlashAttribute("error", message);
					for (Cookie cookie : request.getCookies()) {

						if (cookie.getName().equalsIgnoreCase("customerCode")) {
							cookie.setValue("");
							cookie.setMaxAge(-1);
							response.addCookie(cookie);
						}

						if (cookie.getName().equalsIgnoreCase("password")) {
							cookie.setValue("");
							cookie.setMaxAge(-1);
							response.addCookie(cookie);
						}

					}
					return "redirect:/";
				}
		} else {

			redirectAttributes.addFlashAttribute("error", message);
			return "redirect:/";
		}
		return "redirect:/";

	}

	private String validateLoginForm(LoginMdl loginMdl) {
		String message = "";
		if (loginMdl.getCustomerCode() == null || loginMdl.getCustomerCode() == ""
		// || loginMdl.getCustomerCode().length() < 10
		) {
			// model.addAttribute("error", "Please enter valid customer code");
			message = "Please enter valid customer code";
		} else if (loginMdl.getSalesOrg() == null || loginMdl.getSalesOrg() == "") {
			// model.addAttribute("error", "Please select Company ");
			// message="Please select Company ";
		} else if (loginMdl.getDistChannel() == null || loginMdl.getDistChannel() == "") {
			// model.addAttribute("error", "Please select distribution
			// channel");
			// message="Please select distribution channel";
		} else
			message = "";
		return message;
	}

	@RequestMapping(value = "/homepage")
	public String getHomePage(HttpServletRequest servletRequest, HttpServletResponse servletResponse, Model model) {
		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		ObjectMapper objectMapper = new ObjectMapper();
		String loginDtoJson = null;
		try {
			loginDtoJson = objectMapper.writeValueAsString(loginDTO);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		model.addAttribute("loginDto", loginDtoJson);
		ExceptionBean eb = null;
		if (null != loginDTO) {
			String userId = loginDTO.getLoggedInUserName();
			List<FrequentUsedMenu> menuList = (List<FrequentUsedMenu>) session
					.getAttribute(SessionUtil.FREQUENT_MENU_LIST);
			if (menuList != null) {
				loginDTO.setFrequentUsedMenuList(menuList);
				session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);
			} else {
				try {
					menuList = loginService.getFrequentUsedMenuById(loginDTO);
				} catch (Exception e) {
					logger.error("Frequent Menu List fetch failed: " + e);
				}
				session.setAttribute(SessionUtil.FREQUENT_MENU_LIST, menuList);
				loginDTO.setFrequentUsedMenuList(menuList);
				session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);
			}

			try {

			} catch (Exception e) {
				logger.error("Frequent Menu List fetch failed: " + e);
			}

			SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
					.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);

			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {

				if (loginDTO.getCrmDashboardBuyerId() == null) {
					Map<String, String> crmBuyersList = null;
					if (supplierDetailsDTO.getBuyersSuppliersmap() != null) {
						crmBuyersList = supplierDetailsDTO.getBuyersSuppliersmap().getBuyers();
						if (crmBuyersList.size() > 0) {
							Map.Entry<String, String> entry = crmBuyersList.entrySet().iterator().next();
							loginDTO.setCrmDashboardBuyerId(entry.getKey());
							session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);
						}
					}
				}
			}

			DashboardInfoBox dashboardInfoBox = getDashboardInfoBoxData(loginDTO);
			model.addAttribute("dashboardInfoBox", new Gson().toJson(dashboardInfoBox));
			session.setAttribute(SessionUtil.DASHBOARD_INFO_DETAILS, dashboardInfoBox);

			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {

				model.addAttribute("loginDtoModel", loginDTO);
				model.addAttribute("supplierDetails", supplierDetailsDTO);
				model.addAttribute("supplierDetailsJSON", new Gson().toJson(supplierDetailsDTO));

				loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
				loginDTO.setSupplierDetailsDTO(supplierDetailsDTO);
				session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);
			} else {
				List<RecentShipment> recentShipmentList = getRecentShipment(dashboardInfoBox, loginDTO);
				model.addAttribute("recentShipmentList", recentShipmentList);

				List<TopFiveDashboardShipment> topFiveShipmentList = getTopFiveShipment(dashboardInfoBox, loginDTO);
				model.addAttribute("topFiveShipmentList", topFiveShipmentList);
				model.addAttribute("topFiveShipmentListJSON", new Gson().toJson(topFiveShipmentList));
			}

			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				PoTrackingParams poTrackingParams = new PoTrackingParams();
				PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
				try {
					if (poTrackingParams != null) {

						if (loginDTO.getAccgroup().equals("ZMSP") || loginDTO.getAccgroup().equals("ZMFF")) {
							poTrackingParams.setShipper_no(loginDTO.getLoggedInUserName());
							poTrackingParams.setBuyer_no("");
						}
						if (loginDTO.getAccgroup().equals("ZMBY") || loginDTO.getAccgroup().equals("ZMBH")) {
							poTrackingParams.setBuyer_no(loginDTO.getLoggedInUserName());
							poTrackingParams.setShipper_no("");
						}
						if (loginDTO.getAccgroup().equals("ZMDA") || loginDTO.getAccgroup().equals("ZMOA")) {
							poTrackingParams.setBuyer_no(loginDTO.getLoggedInUserName());
							poTrackingParams.setShipper_no("");
						}
						Date fromDate = new Date();
						fromDate.setMonth(fromDate.getMonth() - 1);

						/*
						 * poTrackingParams.setFromDate(DateUtil.formatDateToString(fromDate));
						 * poTrackingParams.setToDate(DateUtil.formatDateToString(new Date()));
						 * poTrackingParams.setStatusType("");
						 * poTrackingParams.setDivision(loginDTO.getDivisionSelected()); StringBuffer
						 * webServiceUrl = new StringBuffer(RestUtil.PO_TRACKING);
						 * 
						 * poTrackingJsonOutputData =
						 * restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).
						 * toString(), poTrackingParams, PoTrackingJsonOutputData.class);
						 * 
						 * List<PoTrackingDetailResultBean> poTrackingResultBeanLst =
						 * poTrackingJsonOutputData.getPoTrackingDetailResultData();
						 * 
						 * eb=getExceptionStatusCoint(poTrackingResultBeanLst);
						 */

					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("Some exception occurred : " + e);
				}

				/*
				 * Map<String, Object> buyerDashboardData =
				 * getBuyerDashboardData(servletRequest, servletResponse);
				 * SuppWiseShipSummaryJson suppWiseShipSummaryJson = (SuppWiseShipSummaryJson)
				 * buyerDashboardData.get("suppWiseShipSummaryJson"); TopFiveShipBuyerJson
				 * topFiveShipBuyerJson = (TopFiveShipBuyerJson)
				 * buyerDashboardData.get("topFiveShipment"); LastNShipmentsJsonOutputData
				 * lastNShipmentsJsonOutputData = (LastNShipmentsJsonOutputData)
				 * buyerDashboardData.get("lastNShipmentsJsonOutputData");
				 * 
				 * Gson gson = new Gson(); model.addAttribute("suppWiseShipSummaryJson",
				 * gson.toJson(suppWiseShipSummaryJson));
				 * model.addAttribute("topFiveShipBuyerJson",
				 * gson.toJson(topFiveShipBuyerJson));
				 * model.addAttribute("lastNShipmentsJsonOutputData",
				 * gson.toJson(lastNShipmentsJsonOutputData));
				 */

				generatePreAlertMblList(servletRequest);

				model.addAttribute("eb", eb);
				// return "dashboardbuyer";
				return MyshipmentJspMapper.DASHBOARD_BUYER;
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {

				Map<String, List<PreAlertHBLDetails>> preAlertDetailsMap = getDashboardPreAlertList(dashboardInfoBox,
						loginDTO);

				model.addAttribute("preAlertDetailsPending", preAlertDetailsMap.get("pre-alert-pending"));

				return MyshipmentJspMapper.DASHBOARD_SHIPPER;
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				// || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2))

				/*
				 * Map<String, Object> agentDashboardData =
				 * getAgentDashboardData(servletRequest, servletResponse);
				 * SuppWiseShipSummaryJson suppWiseShipSummaryJson = (SuppWiseShipSummaryJson)
				 * agentDashboardData.get("suppWiseShipSummaryJson");
				 * LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData =
				 * (LastNShipmentsJsonOutputData)
				 * agentDashboardData.get("lastNShipmentsJsonOutputData");
				 * 
				 * Gson gson = new Gson(); model.addAttribute("suppWiseShipSummaryJson",
				 * gson.toJson(suppWiseShipSummaryJson));
				 * model.addAttribute("lastNShipmentsJsonOutputData",
				 * gson.toJson(lastNShipmentsJsonOutputData));
				 */

				generatePreAlertMblList(servletRequest);

				model.addAttribute("eb", eb);

				// return "dashboardda";
				return MyshipmentJspMapper.DASHBOARD_AGENT;
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {
				// || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2))

				/*
				 * Map<String, Object> agentDashboardData =
				 * getAgentDashboardData(servletRequest, servletResponse);
				 * SuppWiseShipSummaryJson suppWiseShipSummaryJson = (SuppWiseShipSummaryJson)
				 * agentDashboardData.get("suppWiseShipSummaryJson");
				 * LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData =
				 * (LastNShipmentsJsonOutputData)
				 * agentDashboardData.get("lastNShipmentsJsonOutputData");
				 * 
				 * Gson gson = new Gson(); model.addAttribute("suppWiseShipSummaryJson",
				 * gson.toJson(suppWiseShipSummaryJson));
				 * model.addAttribute("lastNShipmentsJsonOutputData",
				 * gson.toJson(lastNShipmentsJsonOutputData));
				 */

				getAllPreAlertBuyerWiseList(servletRequest);

				List<PreAlertHBLDetails> preAlertHblDetailsByBuyerList = (List<PreAlertHBLDetails>) session
						.getAttribute("preAlertHblBuyerWise");
				Map<String, List<PreAlertHBLDetails>> mblWiseHbl = (Map<String, List<PreAlertHBLDetails>>) session
						.getAttribute("mblWiseHbl");

				model.addAttribute("preAlertHblDetailsByBuyerList", new Gson().toJson(preAlertHblDetailsByBuyerList));
				model.addAttribute("mblWiseHbl", new Gson().toJson(mblWiseHbl));

				model.addAttribute("eb", eb);

				// return "dashboardda";
				return MyshipmentJspMapper.DASHBOARD_CRM;
			}

		}
		return "redirect:/";
	}

	@RequestMapping(value = "/headerdetails", method = RequestMethod.GET)
	@ResponseBody
	public LoginDTO getHeaderDetails(Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String userId = loginDto.getLoggedInUserName();
		List<FrequentUsedMenu> menuList = (List<FrequentUsedMenu>) session.getAttribute(SessionUtil.FREQUENT_MENU_LIST);
		if (menuList != null) {
			loginDto.setFrequentUsedMenuList(menuList);
		} else {
			menuList = loginService.getFrequentUsedMenuById(loginDto);
			session.setAttribute(SessionUtil.FREQUENT_MENU_LIST, menuList);

			loginDto.setFrequentUsedMenuList(menuList);
		}

		return loginDto;
	}

	@RequestMapping(value = "/currentSelectedOrg", method = RequestMethod.GET)
	public String getCurrentSelestedOrgDistChnl(@RequestParam("salesOrg") String salesOrg,
			@RequestParam("distributionChannel") String distributionChannel, HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		Map<String, SalesOrgTree> salesorgTree = loginDto.getMpSalesOrgTree();
//		Integer pendingPO = orderService.getNumberofPendingPO(loginDto.getIsUserLoggedIn());
		loginDto.setSalesOrgSelected(salesOrg);
		loginDto.setDisChnlSelected(distributionChannel);
//		loginDto.setPendingPO(pendingPO);
		SupRequestParams supRequestParams = new SupRequestParams();
		supRequestParams.setCustomerId(loginDto.getLoggedInUserName());
		supRequestParams.setDistChan(loginDto.getDisChnlSelected());
		supRequestParams.setDivision(loginDto.getDivisionSelected());
		supRequestParams.setSalesOrg(loginDto.getSalesOrgSelected());

		SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams, session);

		session.setAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA, supplierDetailsDTO);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		model.addAttribute("loginDetails", loginDto);

		DashboardInfoBox dashboardInfoBox = getDashboardInfoBoxData(loginDto);
		model.addAttribute("dashboardInfoBox", new Gson().toJson(dashboardInfoBox));
		session.setAttribute(SessionUtil.DASHBOARD_INFO_DETAILS, dashboardInfoBox);

		return "dashboard";
	}

	@RequestMapping(value = "/changebusiness", method = RequestMethod.POST)
	public ResponseEntity<?> changeBusiness(UriComponentsBuilder bd, HttpServletRequest request,
			@RequestParam("salesOrg") String salesOrg, @RequestParam("distChannel") String distChannel,
			@RequestParam("division") String division) {

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String requestedDivision = division.equals("AIR") ? "AR" : "SE";
		loginDTO.setSalesOrgSelected(salesOrg);
		List<SalesorgListBean> lstSalesOrg = loginDTO.getSalesorgList();
		for (SalesorgListBean salesOrgSelected : lstSalesOrg) {
			if (salesOrgSelected.getSalesOrg().equalsIgnoreCase(salesOrg)) {
				if (salesOrgSelected.getDistrChan().equalsIgnoreCase("EX")) {
					loginDTO.setDisChnlSelected(salesOrgSelected.getDistrChan());
					if (salesOrgSelected.getDivision().equals(requestedDivision)) {
						loginDTO.setDivisionSelected(requestedDivision);
						break;
					} else {
						loginDTO.setDivisionSelected(salesOrgSelected.getDivision());
						// continue;
					}

				}
				loginDTO.setDisChnlSelected(salesOrgSelected.getDistrChan());
				loginDTO.setDivisionSelected(salesOrgSelected.getDivision());
			}
		}
		// loginDTO.setDisChnlSelected(distChannel);
		// loginDTO.setDivisionSelected(division.equals("AIR") ? "AR" : "SE");

		Map<String, SalesOrgTree> salesOrgTreeLoginDto = loginDTO.getMpSalesOrgTree();
		// salesOrgTreeLoginDto.forEach((key, value)->{});
		Map<String, SalesOrgTree> salesOrgDivisionWise = new HashMap<String, SalesOrgTree>();
		for (Map.Entry<String, SalesOrgTree> mapEntry : salesOrgTreeLoginDto.entrySet()) {
			SalesOrgTree salesOrgTemp = (SalesOrgTree) mapEntry.getValue();
			Set<DistributionChannel> distributionChannelList = salesOrgTemp.getLstDistChnl();

			for (DistributionChannel distChannelTemp : distributionChannelList) {
				if (distChannelTemp.getDistrChan().equals(loginDTO.getDisChnlSelected())) {
					Set<Division> divisionList = distChannelTemp.getLstDivision();
					for (Division divisionTemp : divisionList) {
						if (divisionTemp.getDivision().equals(loginDTO.getDivisionSelected())) {
							salesOrgDivisionWise.put(salesOrgTemp.getSalesOrg(), salesOrgTemp);
						}
					}
				}

			}

		}
		loginDTO.setSalesOrgDivisionWise(salesOrgDivisionWise);

		SupRequestParams supRequestParams = new SupRequestParams();
		supRequestParams.setCustomerId(loginDTO.getLoggedInUserName());
		supRequestParams.setDistChan(loginDTO.getDisChnlSelected());
		supRequestParams.setDivision(loginDTO.getDivisionSelected());
		supRequestParams.setSalesOrg(loginDTO.getSalesOrgSelected());

		SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams, session);

		loginDTO.setSupplierDetailsDTO(supplierDetailsDTO);
		session.setAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA, supplierDetailsDTO);

		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);

		DashboardInfoBox dashboardInfoBox = getDashboardInfoBoxData(loginDTO);

		session.setAttribute(SessionUtil.DASHBOARD_INFO_DETAILS, dashboardInfoBox);

		UriComponents uriComponents = bd.path("/homepage").build();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.OK);

	}

	@RequestMapping(value = "/filterBuyer", method = RequestMethod.POST)
	public ResponseEntity<?> filerBuyer(HttpServletRequest request, @RequestParam("buyerId") String buyerId) {

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		ResponseEntity<?> responseEntity = null;
		if (buyerId != null && !buyerId.equals("")) {
			loginDTO.setCrmDashboardBuyerId(buyerId);
		} else {
			responseEntity = new ResponseEntity("No Consignee/Buyer Found", new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		/*
		 * DashboardInfoBox dashboardInfoBox = getDashboardInfoBoxData(loginDTO);
		 * 
		 * session.setAttribute(SessionUtil.DASHBOARD_INFO_DETAILS, dashboardInfoBox);
		 */
		return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.OK);

	}

	@RequestMapping(value = "/saveFrequentUsedForm", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void SaveUserFavouriteForm(@RequestParam("pageName") String pageName, @RequestParam("path") String path,
			HttpServletRequest request) {
		FrequentlyUsedFormMdl obj = new FrequentlyUsedFormMdl();
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		obj.setDivison(loginDTO.getDivisionSelected());
		obj.setDistChannel(loginDTO.getDisChnlSelected());
		obj.setCompany(loginDTO.getSalesOrgSelected());
		obj.setCustomerCode(loginDTO.getLoggedInUserName());
		obj.setPageName(pageName);
		obj.setContext(path);
		obj.setCounter(1);
		loginService.saveUserFavouriteForm(obj);

	}

	private ExceptionBean getExceptionStatusCoint(List<PoTrackingDetailResultBean> poTrackingResultBeanLst) {
		ExceptionBean eb = new ExceptionBean();
		if (poTrackingResultBeanLst != null) {
			for (PoTrackingDetailResultBean poTrackingResult : poTrackingResultBeanLst) {

				if (poTrackingResult.getEta() != null) {
					if (poTrackingResult.getAta() != null) {

						if (poTrackingResult.getAta().compareTo(poTrackingResult.getEta()) > 0) {
							poTrackingResult.setShipmentStatus("DELAYED");
							eb.setDelayed(eb.getDelayed() + 1);
						}
						if (poTrackingResult.getAta().compareTo(poTrackingResult.getEta()) < 0) {
							poTrackingResult.setShipmentStatus("ADVANCE");
							eb.setAdvance(eb.getAdvance() + 1);
						}
						if (poTrackingResult.getAta().compareTo(poTrackingResult.getEta()) == 0) {
							poTrackingResult.setShipmentStatus("ON-TIME");
							eb.setOntime(eb.getOntime() + 1);
						}
					} else {
						if (poTrackingResult.getFeta() != null) {

							if (poTrackingResult.getFeta().compareTo(poTrackingResult.getEta()) > 0) {
								poTrackingResult.setShipmentStatus("DELAYED");
								eb.setDelayed(eb.getDelayed() + 1);
							}
							if (poTrackingResult.getFeta().compareTo(poTrackingResult.getEta()) < 0) {
								poTrackingResult.setShipmentStatus("ADVANCE");
								eb.setAdvance(eb.getAdvance() + 1);
							}
							if (poTrackingResult.getFeta().compareTo(poTrackingResult.getEta()) == 0) {
								poTrackingResult.setShipmentStatus("ON-TIME");
								eb.setOntime(eb.getOntime() + 1);
							}
						} else {
							poTrackingResult.setShipmentStatus("ON-TIME");
							eb.setOntime((eb.getOntime() + 1));
						}
					}
				} else {
					if (poTrackingResult.getEtd() != null) {
						if (poTrackingResult.getAtd() != null) {

							if (poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd()) > 0) {
								poTrackingResult.setShipmentStatus("DELAYED");
								eb.setDelayed(eb.getDelayed() + 1);
							}
							if (poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd()) < 0) {
								poTrackingResult.setShipmentStatus("ADVANCE");
								eb.setAdvance(eb.getAdvance() + 1);
							}
							if (poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd()) == 0) {
								poTrackingResult.setShipmentStatus("ON-TIME");
								eb.setOntime(eb.getOntime() + 1);
							}
						} else {
							if (poTrackingResult.getFetd() != null) {

								if (poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd()) > 0) {
									poTrackingResult.setShipmentStatus("DELAYED");
									eb.setDelayed(eb.getDelayed() + 1);
								}
								if (poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd()) < 0) {
									poTrackingResult.setShipmentStatus("ADVANCE");
									eb.setAdvance(eb.getAdvance() + 1);
								}
								if (poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd()) == 0) {
									poTrackingResult.setShipmentStatus("ON-TIME");
									eb.setOntime(eb.getOntime() + 1);
								}
							} else {
								poTrackingResult.setShipmentStatus("ON-TIME");
								eb.setOntime((eb.getOntime() + 1));
							}
						}
					} else {
						poTrackingResult.setShipmentStatus("ON-TIME");
						eb.setOntime((eb.getOntime() + 1));
					}
				}

			}
		}
		return eb;
	}

	public Map<String, Object> getBuyerDashboardData(HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "getDashboard():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		String fromDate = "";
		String toDate = "";

		if ((loginDto.getDashBoardFromDate() != null || loginDto.getDashBoardFromDate().equalsIgnoreCase(""))
				&& (loginDto.getDashBoardToDate() != null || loginDto.getDashBoardToDate().equalsIgnoreCase(""))) {
			fromDate = loginDto.getDashBoardFromDate();
			toDate = loginDto.getDashBoardToDate();
		} else {
			Date tempDate = new Date();
			tempDate.setMonth(tempDate.getMonth() - 1);
			fromDate = DateUtil.formatDateToString(tempDate);
			toDate = DateUtil.formatDateToString(new Date());
		}

		// supplier wise shipper summary service call
		logger.info(this.getClass().getName() + "Calling Supplier Wise Shipment Summary for Buyer...");

		SuppWiseShipSummaryJson suppWiseShipSummaryJson = null;
		try {
			suppWiseShipSummaryJson = dashboardService.suppWiseShipSummaryForBuyerService(fromDate, toDate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Supplier Wise Shipment Summary for Buyer Call Ends");

		// top five shipment service call
		logger.info(this.getClass().getName() + "Calling Top Five Shipment service...");

		TopFiveShipBuyerJson topFiveShipment = null;
		try {
			topFiveShipment = dashboardService.topFiveShipmentServiceBuyer(fromDate, toDate, loginDto);
			Collections.sort(topFiveShipment.getLstTopFiveShipmentBuyer(), new Comparator<TopFiveShipmentBuyer>() {
				public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
					if (o1.getTotalShipment() == o2.getTotalShipment())
						return 0;
					return o1.getTotalShipment() > o2.getTotalShipment() ? -1 : 1;
				}

			});
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Top Five Shipment service Call Ends");

		// recent shipment
		int noOfRecords = 5;
		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = null;
		try {
			lastNShipmentsJsonOutputData = dashboardService.getRecentShipmentShipper(loginDto, noOfRecords);
		} catch (Exception e) {
			logger.error("Error occured while fetching Recent Shipment Shipper Details : " + e);
		}

		Map<String, Object> resultSet = new HashMap<String, Object>();

		resultSet.put("suppWiseShipSummaryJson", suppWiseShipSummaryJson);
		resultSet.put("topFiveShipment", topFiveShipment);
		resultSet.put("lastNShipmentsJsonOutputData", lastNShipmentsJsonOutputData);

		return resultSet;
	}

	public Map<String, Object> getShipperDashboardData(HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "getDashboard():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		String fromDate = "";
		String toDate = "";

		if ((loginDto.getDashBoardFromDate() != null || loginDto.getDashBoardFromDate().equalsIgnoreCase(""))
				&& (loginDto.getDashBoardToDate() != null || loginDto.getDashBoardToDate().equalsIgnoreCase(""))) {
			fromDate = loginDto.getDashBoardFromDate();
			toDate = loginDto.getDashBoardToDate();
		} else {
			Date tempDate = new Date();
			tempDate.setMonth(tempDate.getMonth() - 1);
			fromDate = DateUtil.formatDateToString(tempDate);
			toDate = DateUtil.formatDateToString(new Date());
		}

		// supplier wise shipper summary service call
		logger.info(this.getClass().getName() + "Calling Supplier Wise Shipment Summary for Shipper...");

		SOWiseShipmentSummaryJson sowiseShipmentSummary = null;
		try {
			sowiseShipmentSummary = dashboardService.soWiseShipSummaryForShipperService(fromDate, toDate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Supplier Wise Shipment Summary for Shipper Call Ends");

		// top five shipment service call
		logger.info(this.getClass().getName() + "Calling Top Five Shipment service...");

		TopFiveShipment topFiveShipment = null;
		try {
			topFiveShipment = dashboardService.topFiveShipmentService(fromDate, toDate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Top Five Shipment service Call Ends");

		// recent shipment
		int noOfRecords = 5;
		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = null;
		try {
			lastNShipmentsJsonOutputData = dashboardService.getRecentShipmentShipper(loginDto, noOfRecords);
		} catch (Exception e) {
			logger.error("Error occured while fetching Recent Shipment Shipper Details : " + e);
		}

		Map<String, Object> resultSet = new HashMap<String, Object>();

		resultSet.put("sowiseShipmentSummary", sowiseShipmentSummary);
		resultSet.put("topFiveShipment", topFiveShipment);
		resultSet.put("lastNShipmentsJsonOutputData", lastNShipmentsJsonOutputData);

		return resultSet;
	}

	public Map<String, Object> getAgentDashboardData(HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "getDashboard():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		String fromDate = "";
		String toDate = "";

		if ((loginDto.getDashBoardFromDate() != null || loginDto.getDashBoardFromDate().equalsIgnoreCase(""))
				&& (loginDto.getDashBoardToDate() != null || loginDto.getDashBoardToDate().equalsIgnoreCase(""))) {
			fromDate = loginDto.getDashBoardFromDate();
			toDate = loginDto.getDashBoardToDate();
		} else {
			Date tempDate = new Date();
			tempDate.setMonth(tempDate.getMonth() - 1);
			fromDate = DateUtil.formatDateToString(tempDate);
			toDate = DateUtil.formatDateToString(new Date());
		}

		// supplier wise shipper summary service call
		logger.info(this.getClass().getName() + "Calling Supplier Wise Shipment Summary for Shipper...");

		SuppWiseShipSummaryJson suppWiseShipSummaryJson = null;
		try {
			suppWiseShipSummaryJson = dashboardService.suppWiseShipSummaryForBuyerService(fromDate, toDate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Supplier Wise Shipment Summary for Shipper Call Ends");

		// recent shipment
		int noOfRecords = 5;
		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = null;
		try {
			lastNShipmentsJsonOutputData = dashboardService.getRecentShipmentShipper(loginDto, noOfRecords);
		} catch (Exception e) {
			logger.error("Error occured while fetching Recent Shipment Shipper Details : " + e);
		}

		Map<String, Object> resultSet = new HashMap<String, Object>();

		resultSet.put("suppWiseShipSummaryJson", suppWiseShipSummaryJson);
		resultSet.put("lastNShipmentsJsonOutputData", lastNShipmentsJsonOutputData);

		return resultSet;
	}

	public DashboardInfoBox getDashboardInfoBoxData(LoginDTO loginDto) {

		logger.info(this.getClass().getName() + "getDashboardInfoData() called....");

		MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();

		String fromDateString = "";
		String toDateString = "";

//		Integer pendingPO = orderService.getNumberofPendingPO(loginDto.getLoggedInUserName());
//		loginDto.setPendingPO(pendingPO);

		/*
		 * if((loginDto.getDashBoardFromDate() != null ||
		 * loginDto.getDashBoardFromDate().equalsIgnoreCase("")) &&
		 * (loginDto.getDashBoardToDate() != null ||
		 * loginDto.getDashBoardToDate().equalsIgnoreCase(""))) { fromDateString =
		 * loginDto.getDashBoardFromDate(); toDateString =
		 * loginDto.getDashBoardToDate(); } else { Date tempDate = new Date();
		 * tempDate.setMonth(tempDate.getMonth() - 2); fromDateString =
		 * DateUtil.formatDateToString(tempDate); toDateString =
		 * DateUtil.formatDateToString(new Date()); }
		 */

		Date tempDate = new Date();
		tempDate.setMonth(tempDate.getMonth() - 2);
		fromDateString = DateUtil.formatDateToString(tempDate);
		toDateString = DateUtil.formatDateToString(new Date());

		Date fromDate = null;
		Date toDate = null;

		fromDateString = fromDateString != null ? fromDateString.replaceAll("-", ".") : null;
		toDateString = toDateString != null ? toDateString.replaceAll("-", ".") : null;

		try {
			// fromDate = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2010");
			fromDate = new SimpleDateFormat("dd.MM.yyyy").parse(fromDateString);
			// toDate = new SimpleDateFormat("dd.MM.yyyy").parse("22.10.2017");
			toDate = new SimpleDateFormat("dd.MM.yyyy").parse(toDateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		myshipmentTrackParams.setAction("00");

		if (loginDto.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
				|| loginDto.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {

			myshipmentTrackParams.setCustomer(loginDto.getCrmDashboardBuyerId());
		} else {
			myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
		}

		myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
		myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
		myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());

		myshipmentTrackParams.setErdat1(fromDate);
		myshipmentTrackParams.setErdat2(toDate);

		StringBuffer webServiceUrl = new StringBuffer(RestUtil.DASHBOARD_API);
		// StringBuffer webServiceUrl = new
		// StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
		MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = null;
		DashboardInfoBox dashboardInfoBox = new DashboardInfoBox();

		try {
			myshipmentTrackJsonOutputData = restService.postForObject(
					RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
					MyshipmentTrackJsonOutputData.class);

			// prepareDataCorrespodningToThePage(myshipmentTrackJsonOutputData,
			// trackingRequestParam.getAction());

			if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("FAILURE")) {
				logger.info("myshipmentTrackJsonOutputData was returned null, getDashboardInfoBoxData Method");

				dashboardInfoBox = setEmptyDataDashboardInfoBox();

			} else if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("SUCCESS")) {
				logger.info("SUCCESS : myshipmentTrackJsonOutputData, getDashboardInfoBoxData Method");
				// initialization
				List<MyshipmentTrackHeaderBean> myshipTrackHeaderList = new ArrayList<MyshipmentTrackHeaderBean>();

				if (myshipmentTrackJsonOutputData != null) {
					if (myshipmentTrackJsonOutputData.getMyshipmentTrackHeader() != null) {
						logger.info(
								"Data Found for myshipmentTrackJsonOutputData.getMyshipmentTrackHeader(), getDashboardInfoBoxData Method");
						myshipTrackHeaderList = myshipmentTrackJsonOutputData.getMyshipmentTrackHeader();

						Double totalCBM = 0.0;
						Double totalGWT = 0.0;
						Double totalCRGWT = 0.0;

						List<MyshipmentTrackHeaderBean> goodsReceviedList = new ArrayList<MyshipmentTrackHeaderBean>();
						List<MyshipmentTrackHeaderBean> stuffingList = new ArrayList<MyshipmentTrackHeaderBean>();
						List<MyshipmentTrackHeaderBean> deliveredList = new ArrayList<MyshipmentTrackHeaderBean>();
						List<MyshipmentTrackHeaderBean> inTransitList = new ArrayList<MyshipmentTrackHeaderBean>();
						List<MyshipmentTrackHeaderBean> openOrderList = new ArrayList<MyshipmentTrackHeaderBean>();

						if (loginDto.getDivisionSelected().equals("SE")) {

							for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : myshipTrackHeaderList) {
								Double volume;
								Double weight;
								try {
									volume = myshipmentTrackHeaderBean.getVolume().doubleValue();
									weight = myshipmentTrackHeaderBean.getGrs_wt().doubleValue();

									totalCBM = totalCBM + volume;
									totalGWT = totalGWT + weight;
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								if (myshipmentTrackHeaderBean.getStatus().equals("GR Done")) {
									goodsReceviedList.add(myshipmentTrackHeaderBean);
								}

								if (myshipmentTrackHeaderBean.getStatus().equals("Stuffing Done")) {
									String etd = myshipmentTrackHeaderBean.getDep_dt();
									String eta = myshipmentTrackHeaderBean.getEta_dt();

									SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");

									Date departureDate = etd != "" ? DateUtil.formatStringToDatenew(etd, "dd-MM-yyyy")
											: null;
									Date arrivalDate = eta != "" ? DateUtil.formatStringToDatenew(eta, "dd-MM-yyyy")
											: null;
									Date currentDate = null;
									try {
										currentDate = sf.parse(sf.format(new Date()));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if (departureDate != null && arrivalDate != null) {
										if (departureDate.compareTo(currentDate) <= 0) {
											// if(arrivalDate.compareTo(currentDate) < 0) {
											if (currentDate.compareTo(arrivalDate) < 0) {
												inTransitList.add(myshipmentTrackHeaderBean);
											} else {
												deliveredList.add(myshipmentTrackHeaderBean);
											}
										} else {
											stuffingList.add(myshipmentTrackHeaderBean);
										}
									} else {
										stuffingList.add(myshipmentTrackHeaderBean);
									}

								} else if (myshipmentTrackHeaderBean.getStatus().equals("Order Booked")) {
									openOrderList.add(myshipmentTrackHeaderBean);
								}

							}

						} else if (loginDto.getDivisionSelected().equals("AR")) {

							for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : myshipTrackHeaderList) {
								Double chargeWeight;
								Double weight;
								try {
									chargeWeight = myshipmentTrackHeaderBean.getCrg_wt().doubleValue();
									weight = myshipmentTrackHeaderBean.getGrs_wt().doubleValue();

									totalCRGWT = totalCRGWT + chargeWeight;
									totalGWT = totalGWT + weight;
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								if (myshipmentTrackHeaderBean.getStatus().equals("GR Done")) {
									goodsReceviedList.add(myshipmentTrackHeaderBean);
								}

								if (myshipmentTrackHeaderBean.getStatus().equals("Shipment Done")) {
									String etd = myshipmentTrackHeaderBean.getDep_dt();
									String eta = myshipmentTrackHeaderBean.getEta_dt();

									SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");

									Date departureDate = DateUtil.formatStringToDatenew(etd, "dd-MM-yyyy");
									Date arrivalDate = DateUtil.formatStringToDatenew(eta, "dd-MM-yyyy");
									Date currentDate = null;
									try {
										currentDate = sf.parse(sf.format(new Date()));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									if (departureDate != null && arrivalDate != null) {
										if (departureDate.compareTo(currentDate) <= 0) {
											if (currentDate.compareTo(arrivalDate) < 0) {
												inTransitList.add(myshipmentTrackHeaderBean);
											} else {
												deliveredList.add(myshipmentTrackHeaderBean);
											}
										} else {
											stuffingList.add(myshipmentTrackHeaderBean);
										}
									} else {
										stuffingList.add(myshipmentTrackHeaderBean);
									}

								} else if (myshipmentTrackHeaderBean.getStatus().equals("Order Booked")) {
									openOrderList.add(myshipmentTrackHeaderBean);
								}

							}
						}

						dashboardInfoBox.setTotalCBM(totalCBM != null ? totalCBM : 0.0);
						dashboardInfoBox.setTotalGWT(totalGWT != null ? totalGWT : 0.0);
						dashboardInfoBox.setTotalCRGWT(totalCRGWT != null ? totalCRGWT : 0.0);

						dashboardInfoBox.setTotalShipment(myshipTrackHeaderList.size());
						dashboardInfoBox.setInTransit(inTransitList.size());
						dashboardInfoBox.setOpenOrder(openOrderList.size());

						dashboardInfoBox.setGoodsReceived(goodsReceviedList.size());
						dashboardInfoBox.setDelivered(deliveredList.size());
						dashboardInfoBox.setStuffingDone(stuffingList.size());

						dashboardInfoBox.setGoodsReceivedList(goodsReceviedList);
						dashboardInfoBox.setDeliveredList(deliveredList);
						dashboardInfoBox.setStuffingDoneList(stuffingList);

						dashboardInfoBox.setTotalShipmentList(myshipTrackHeaderList);
						dashboardInfoBox.setInTransitList(inTransitList);
						dashboardInfoBox.setOpenOrderList(openOrderList);

					} else {
						logger.info(
								"No Data Found for myshipmentTrackJsonOutputData.getMyshipmentTrackHeader(), getDashboardInfoBoxData Method");
						dashboardInfoBox = setEmptyDataDashboardInfoBox();
					}
					logger.info("Method getDashboardInfoBoxData ends");

				}

			} else {
				logger.info("myshipmentTrackJsonOutputData was returned null, getDashboardInfoBoxData Method");

			}
		} catch (Exception e) {
			e.printStackTrace();

			dashboardInfoBox = setEmptyDataDashboardInfoBox();

		}

		return dashboardInfoBox;

	}

	public DashboardInfoBox setEmptyDataDashboardInfoBox() {
		DashboardInfoBox dashboardInfoBox = new DashboardInfoBox();

		dashboardInfoBox.setTotalCBM(0.0);
		dashboardInfoBox.setTotalGWT(0.0);
		dashboardInfoBox.setTotalCRGWT(0.0);

		dashboardInfoBox.setTotalShipment(0);
		dashboardInfoBox.setInTransit(0);
		dashboardInfoBox.setOpenOrder(0);

		dashboardInfoBox.setGoodsReceived(0);
		dashboardInfoBox.setDelivered(0);
		dashboardInfoBox.setStuffingDone(0);

		dashboardInfoBox.setGoodsReceivedList(new ArrayList<MyshipmentTrackHeaderBean>());
		dashboardInfoBox.setDeliveredList(new ArrayList<MyshipmentTrackHeaderBean>());
		dashboardInfoBox.setStuffingDoneList(new ArrayList<MyshipmentTrackHeaderBean>());

		dashboardInfoBox.setTotalShipmentList(new ArrayList<MyshipmentTrackHeaderBean>());
		dashboardInfoBox.setInTransitList(new ArrayList<MyshipmentTrackHeaderBean>());
		dashboardInfoBox.setOpenOrderList(new ArrayList<MyshipmentTrackHeaderBean>());

		return dashboardInfoBox;
	}

	public List<RecentShipment> getRecentShipment(DashboardInfoBox dashboardInfoBox, LoginDTO loginDTO) {

		List<MyshipmentTrackHeaderBean> totalShipment = dashboardInfoBox.getTotalShipmentList();

		List<RecentShipment> recentShipmentList = new ArrayList<RecentShipment>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : totalShipment) {
			RecentShipment recentShipment = new RecentShipment();

			String stuffDate = myshipmentTrackHeaderBean.getStuff_dt();
			String loadDate = myshipmentTrackHeaderBean.getDep_dt();
			String eta = myshipmentTrackHeaderBean.getEta_dt();
			String bl_date_string = myshipmentTrackHeaderBean.getBl_bt();

			SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yyyy");

			Date stuffingDate = stuffDate != "" ? DateUtil.formatStringToDatenew(stuffDate, "dd-MM-yyyy") : null;
			Date loadingDate = loadDate != "" ? DateUtil.formatStringToDatenew(loadDate, "dd-MM-yyyy") : null;
			Date etaDate = eta != "" ? DateUtil.formatStringToDatenew(eta, "dd-MM-yyyy") : null;
			Date blDate = bl_date_string != "" ? DateUtil.formatStringToDatenew(bl_date_string, "dd-MM-yyyy") : null;

			Date currentDate = null;
			try {
				currentDate = sf.parse(sf.format(new Date()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (myshipmentTrackHeaderBean.getStatus().equals("Stuffing Done")
					|| myshipmentTrackHeaderBean.getStatus().equals("Shipment Done")) {

				if (stuffingDate != null && loadingDate != null && etaDate != null) {
					if (stuffingDate.compareTo(currentDate) < 0) {
						if (loadingDate.compareTo(currentDate) < 0) {
							if (currentDate.compareTo(etaDate) < 0) {
								myshipmentTrackHeaderBean.setStatus("In Transit");
								;
							} else {
								myshipmentTrackHeaderBean.setStatus("Arrived");
							}
						}
					}
				}

			}

			recentShipment.setHblNo(myshipmentTrackHeaderBean.getBl_no());
			recentShipment.setBookingDate(blDate);
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				recentShipment.setName(myshipmentTrackHeaderBean.getBuyer());
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				recentShipment.setName(myshipmentTrackHeaderBean.getShipper());
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				recentShipment.setName(myshipmentTrackHeaderBean.getShipper());
			}

			recentShipment.setPol(myshipmentTrackHeaderBean.getPol());
			recentShipment.setPod(myshipmentTrackHeaderBean.getPod());
			recentShipment.setEtd(loadingDate);
			recentShipment.setEta(etaDate);
			recentShipment.setStatus(myshipmentTrackHeaderBean.getStatus());

			recentShipmentList.add(recentShipment);

		}
		return recentShipmentList;
	}

	public List<TopFiveDashboardShipment> getTopFiveShipment(DashboardInfoBox dashboardInfoBox, LoginDTO loginDTO) {

		List<MyshipmentTrackHeaderBean> totalShipment = dashboardInfoBox.getTotalShipmentList();

		Map<String, TopFiveDashboardShipment> shipmentCountMap = new HashMap<String, TopFiveDashboardShipment>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : totalShipment) {
			String port = null;
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				port = myshipmentTrackHeaderBean.getPod();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				port = myshipmentTrackHeaderBean.getPol();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				port = myshipmentTrackHeaderBean.getPol();
			}

			Double cbm = myshipmentTrackHeaderBean.getVolume() != null
					? Math.round(myshipmentTrackHeaderBean.getVolume().doubleValue())
					: 0.0;
			Double crgwt = myshipmentTrackHeaderBean.getCrg_wt() != null
					? Math.round(myshipmentTrackHeaderBean.getCrg_wt().doubleValue())
					: 0.0;
			Double gwt = myshipmentTrackHeaderBean.getGrs_wt() != null
					? Math.round(myshipmentTrackHeaderBean.getGrs_wt().doubleValue())
					: 0.0;

			if (shipmentCountMap.containsKey(port)) {
				TopFiveDashboardShipment topFiveDashboardShipment = shipmentCountMap.get(port);
				if (loginDTO.getDivisionSelected().equals("SE")) {
					topFiveDashboardShipment.setTotalCBM(topFiveDashboardShipment.getTotalCBM() + cbm);
				} else if (loginDTO.getDivisionSelected().equals("AR")) {
					topFiveDashboardShipment.setTotalCBM(topFiveDashboardShipment.getTotalCBM() + crgwt);
				}

				topFiveDashboardShipment.setTotalGWT(topFiveDashboardShipment.getTotalGWT() + gwt);
				topFiveDashboardShipment.setTotalShipment(topFiveDashboardShipment.getTotalShipment() + 1);
				shipmentCountMap.put(port, topFiveDashboardShipment);
			} else {
				TopFiveDashboardShipment topFiveDashboardShipment = new TopFiveDashboardShipment();
				topFiveDashboardShipment.setPort(port);
				if (loginDTO.getDivisionSelected().equals("SE")) {
					topFiveDashboardShipment.setTotalCBM(cbm);
				} else if (loginDTO.getDivisionSelected().equals("AR")) {
					topFiveDashboardShipment.setTotalCBM(crgwt);
				}
				topFiveDashboardShipment.setTotalGWT(gwt);
				topFiveDashboardShipment.setTotalShipment(1);
				shipmentCountMap.put(port, topFiveDashboardShipment);
			}

		}

		List<TopFiveDashboardShipment> topFiveShipment = new ArrayList<TopFiveDashboardShipment>(
				shipmentCountMap.values());

		for (int i = 0; i < topFiveShipment.size(); i++) {

			for (int j = i + 1; j < topFiveShipment.size(); j++) {
				TopFiveDashboardShipment prior = topFiveShipment.get(i);
				TopFiveDashboardShipment match = topFiveShipment.get(j);
				if (prior.getTotalShipment() < match.getTotalShipment()) {
					topFiveShipment.set(i, match);
					topFiveShipment.set(j, prior);
				}
			}
			if (i == 4)
				break;
		}

		return topFiveShipment;
	}

	public Map<String, List<PreAlertHBLDetails>> getDashboardPreAlertList(DashboardInfoBox dashboardInfoBox,
			LoginDTO loginDTO) {

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());

		List<PreAlertHBL> preAlertHblBySupplierList = new ArrayList<PreAlertHBL>();

		try {
			preAlertHblBySupplierList = preAlertManagementService.getPreAlertByHBL(loginDTO.getLoggedInUserName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListAll = new ArrayList<PreAlertHBLDetails>();
		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListPending = new ArrayList<PreAlertHBLDetails>();
		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListCompleted = new ArrayList<PreAlertHBLDetails>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblBySupplier : preAlertHblBySupplierList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblBySupplier.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblBySupplier);
					if (preAlertHblBySupplier.getIsPreAlertDoc().equals("0")) {
						preAlertHblDetailsBySupplierListPending.add(preAlertHBLDetails);
					} else if (preAlertHblBySupplier.getIsPreAlertDoc().equals("1")) {
						preAlertHblDetailsBySupplierListCompleted.add(preAlertHBLDetails);
					} else {
						preAlertHblDetailsBySupplierListAll.add(preAlertHBLDetails);
					}
				}
			}
		}

		Map<String, List<PreAlertHBLDetails>> preAlertDetailsMap = new HashMap<String, List<PreAlertHBLDetails>>();

		preAlertDetailsMap.put("pre-alert-all", preAlertHblDetailsBySupplierListAll);
		preAlertDetailsMap.put("pre-alert-pending", preAlertHblDetailsBySupplierListPending);
		preAlertDetailsMap.put("pre-alert-completed", preAlertHblDetailsBySupplierListCompleted);

		return preAlertDetailsMap;
	}

	public void generatePreAlertMblList(HttpServletRequest request) {
		logger.info(this.getClass() + ": Generating Pre-Alert MBL/MAWB List...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());

		List<PreAlertHBL> preAlertHblByBuyerAgentList = new ArrayList<PreAlertHBL>();
		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();

		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUID = loginDTO.getLoggedInUserName();

		try {
			logger.info(this.getClass() + ": Calling Service for PreAlert MBL/MAWB...");
			preAlertHblByBuyerAgentList = preAlertManagementService.getPreAlertForDownload(myshipmentUID,
					preAlertSearchParameter);
		} catch (Exception e) {
			logger.error(this.getClass() + ": Error Occured while fetching Data for PreAlert MBL/MAWB...");
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerAgentList = new ArrayList<PreAlertHBLDetails>();

		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = new HashMap<String, List<PreAlertHBLDetails>>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);

					if (myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.getMblNumber())) {
						if (mblWiseHbl.containsKey(preAlertHblByBuyerAgent.getMblNumber())) {
							List<PreAlertHBLDetails> tempPreAlertList = mblWiseHbl
									.get(preAlertHblByBuyerAgent.getMblNumber());
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						} else {
							List<PreAlertHBLDetails> tempPreAlertList = new ArrayList<PreAlertHBLDetails>();
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						}
					}

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);

					preAlertHblDetailsByBuyerAgentList.add(preAlertHBLDetails);
				}
			}
		}

		session.setAttribute("mblWiseHbl", mblWiseHbl);
		session.setAttribute("preAlertHblDetailsByBuyerAgentList", preAlertHblDetailsByBuyerAgentList);
	}

	public void getAllPreAlertBuyerWiseList(HttpServletRequest request) {
		logger.info(this.getClass() + ": Generating Pre-Alert Buyer-Wise List...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());

		List<PreAlertHBL> preAlertHblByBuyerAgentList = new ArrayList<PreAlertHBL>();
		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();

		// preAlertSearchParameter.setBuyerId("1010000013");
		// hamid
		preAlertSearchParameter.setBuyerId(loginDTO.getCrmDashboardBuyerId());
		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUID = loginDTO.getLoggedInUserName();

		try {
			logger.info(this.getClass() + ": Calling Service for PreAlert Buyer list of MBL/MAWB...");
			preAlertHblByBuyerAgentList = preAlertManagementService.getPreAlertBuyerWiseList(preAlertSearchParameter,
					"ALL");
		} catch (Exception e) {
			logger.error(
					this.getClass() + ": Error Occured while fetching Data for PreAlert Buyer list of MBL/MAWB...");
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerList = new ArrayList<PreAlertHBLDetails>();

		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = new HashMap<String, List<PreAlertHBLDetails>>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					/*
					 * preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					 * preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					 */

					/*
					 * if(myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.
					 * getMblNumber())) {
					 * 
					 * }
					 */
					if (!myshipmentTrackHeaderBean.getMbl_no().equals("")) {
						if (mblWiseHbl.containsKey(myshipmentTrackHeaderBean.getMbl_no())) {
							List<PreAlertHBLDetails> tempPreAlertList = mblWiseHbl
									.get(myshipmentTrackHeaderBean.getMbl_no());
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(myshipmentTrackHeaderBean.getMbl_no(), tempPreAlertList);
						} else {
							List<PreAlertHBLDetails> tempPreAlertList = new ArrayList<PreAlertHBLDetails>();
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(myshipmentTrackHeaderBean.getMbl_no(), tempPreAlertList);
						}
					}

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);

					preAlertHblDetailsByBuyerList.add(preAlertHBLDetails);
				}
			}
		}

		session.setAttribute("preAlertHblBuyerWise", preAlertHblDetailsByBuyerList);
		session.setAttribute("mblWiseHbl", mblWiseHbl);
	}
}
