package com.myshipment.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.CommInvTrackingParams;
import com.myshipment.model.PoTrackingJsonOutputData;
import com.myshipment.model.PoTrackingParams;
import com.myshipment.model.PoTrackingResultBean;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class CommInvTrackingController {

	private static final Logger logger = Logger.getLogger(CommInvTrackingController.class);

	@Autowired
	private RestService restService;

	@RequestMapping(value = "/getCommInvTrackingDetail", method = RequestMethod.POST)
	public String getCommInvTrackingDetail(@ModelAttribute("commInvTrackingParams") CommInvTrackingParams commInvTrackingParams, Model model, HttpServletRequest request){

		logger.info("Method CommInvTrackingController.getCommInvTrackingDetail starts.");
		
		PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try{
			if(commInvTrackingParams != null){
				
				LoginDTO loginDto = (LoginDTO)request.getSession().getAttribute("loginDetails");
				if(loginDto.getAccgroup().equals("ZMSP")||loginDto.getAccgroup().equals("ZMFF")){
					commInvTrackingParams.setShipper_no(loginDto.getLoggedInUserName());
					commInvTrackingParams.setBuyer_no("");
				}
				if(loginDto.getAccgroup().equals("ZMBY")||loginDto.getAccgroup().equals("ZMBH")){
					commInvTrackingParams.setBuyer_no(loginDto.getLoggedInUserName());
					commInvTrackingParams.setShipper_no("");
				}
				
				if(commInvTrackingParams.getStatusType().equalsIgnoreCase(CommonConstant.STATUS_TYPE_NONE)){
					commInvTrackingParams.setStatusType("");
				}
				
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.COMM_INV_TRACKING);

				poTrackingJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), commInvTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingResultBean>  poTrackingResultBeanLst = poTrackingJsonOutputData.getPoTrackingResultData();
				
				poTrackingResultBeanLst=getPoTrackingStatus(poTrackingResultBeanLst);
				
				if(poTrackingResultBeanLst != null && poTrackingResultBeanLst.size() > 0){
					model.addAttribute("poTrackingResultBeanLst", poTrackingResultBeanLst);
					model.addAttribute("commInvTrackingParams",commInvTrackingParams);
					return "commInvTracking";
				}
				else{
					logger.info("poTrackingResultBeanLst size is either null or zero. " );
					model.addAttribute("commInvTrackingParams",commInvTrackingParams);
					
					return "commInvTracking";
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Some exception occurred : "+ e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");
		
		return "poTracking";
	}


	@RequestMapping(value = "/getCommInvTrackingPage", method = RequestMethod.GET)
	public String getCommInvTrackingPage(Model model){

		try{
			CommInvTrackingParams commInvTrackingParams = new CommInvTrackingParams();

			model.addAttribute("commInvTrackingParams", commInvTrackingParams);
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
		}
		return "commInvTracking";
	}
	
		
	@ModelAttribute("statusTypes")
	public Map<String, String> getCompanyCodes() {
		
		Map<String, String> statusTypes = new LinkedHashMap<String, String>();
		statusTypes.put(CommonConstant.STATUS_TYPE_ORDER_BOOKED, CommonConstant.STATUS_TYPE_ORDER_BOOKED);
		statusTypes.put(CommonConstant.STATUS_TYPE_CARGO_RECEIVED, CommonConstant.STATUS_TYPE_CARGO_RECEIVED);
		statusTypes.put(CommonConstant.STATUS_TYPE_STUFFING_DONE, CommonConstant.STATUS_TYPE_STUFFING_DONE);
		statusTypes.put(CommonConstant.STATUS_TYPE_CARGO_DEPARTED, CommonConstant.STATUS_TYPE_CARGO_DEPARTED);
		statusTypes.put(CommonConstant.STATUS_TYPE_CARGO_ARRIVED, CommonConstant.STATUS_TYPE_CARGO_ARRIVED);

		return statusTypes;
	}
	
	private List<PoTrackingResultBean> getPoTrackingStatus(List<PoTrackingResultBean> poTrackingResultBeanLst){
		List<PoTrackingResultBean> poTrackingStautsList=new ArrayList<PoTrackingResultBean>();
		for (PoTrackingResultBean poTrackingResult : poTrackingResultBeanLst){
			if(poTrackingResult.getBooking_date()!=null){
				poTrackingResult.setPoStatus(CommonConstant.STATUS_TYPE_ORDER_BOOKED);
				if(poTrackingResult.getGr_date()!=null){
					poTrackingResult.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_RECEIVED);
					if(poTrackingResult.getShipment_date()!=null){
						poTrackingResult.setPoStatus(CommonConstant.STATUS_TYPE_STUFFING_DONE);
						if(poTrackingResult.getEtd()!=null){
							Date toDate = new Date();
							//SimpleDateFormat dt1 = new SimpleDateFormat("yyyyy-mm-dd");
							if(poTrackingResult.getEtd().compareTo(toDate)<0){
								poTrackingResult.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_DEPARTED);
							}
						}
							if(poTrackingResult.getEta()!=null){
								Date toDate = new Date();
								if(poTrackingResult.getEta().compareTo(toDate)<0){
								poTrackingResult.setPoStatus(CommonConstant.STATUS_TYPE_CARGO_ARRIVED);
								}
							}
						
						
					}
				}
			}
			if(poTrackingResult.getEta()!=null){
				if(poTrackingResult.getAta()!=null){
				
					if(poTrackingResult.getAta().compareTo(poTrackingResult.getEta())>0){
						poTrackingResult.setShipmentStatus("DELAYED");
					}
					if(poTrackingResult.getAta().compareTo(poTrackingResult.getEta())<0){
						poTrackingResult.setShipmentStatus("ADVANCE");
					}
					if(poTrackingResult.getAta().compareTo(poTrackingResult.getEta())==0){
						poTrackingResult.setShipmentStatus("ON-TIME");
					}
				}else{
					if(poTrackingResult.getFeta()!=null){
						
						if(poTrackingResult.getFeta().compareTo(poTrackingResult.getEta())>0){
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if(poTrackingResult.getFeta().compareTo(poTrackingResult.getEta())<0){
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if(poTrackingResult.getFeta().compareTo(poTrackingResult.getEta())==0){
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					}
					else{
						poTrackingResult.setShipmentStatus("NOT AVAILABLE");
					}
				}
			}else{
				if(poTrackingResult.getEtd()!=null){
					if(poTrackingResult.getAtd()!=null){
						
						if(poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd())>0){
							poTrackingResult.setShipmentStatus("DELAYED");
						}
						if(poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd())<0){
							poTrackingResult.setShipmentStatus("ADVANCE");
						}
						if(poTrackingResult.getAtd().compareTo(poTrackingResult.getEtd())==0){
							poTrackingResult.setShipmentStatus("ON-TIME");
						}
					}else{
						if(poTrackingResult.getFetd()!=null){
							
							if(poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd())>0){
								poTrackingResult.setShipmentStatus("DELAYED");
							}
							if(poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd())<0){
								poTrackingResult.setShipmentStatus("ADVANCE");
							}
							if(poTrackingResult.getFetd().compareTo(poTrackingResult.getEtd())==0){
								poTrackingResult.setShipmentStatus("ON-TIME");
							}
						}
						else{
							poTrackingResult.setShipmentStatus("NOT AVAILABLE");
						}
					}
				}else{
					poTrackingResult.setShipmentStatus("NOT AVAILABLE");
				}
			}
			poTrackingStautsList.add(poTrackingResult);
		}
				return poTrackingStautsList;
	}
}
