package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;

import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface SegPurchaseOrderMapper {

public final String insert = "INSERT INTO T_SEG_PURCHASE_ORDER (SEG_ID,PO_ID,VC_PO_NO,NU_CLIENT_CODE,VC_PRODUCT_NO,VC_SKU_NO,VC_STYLE_NO,VC_ARTICLE_NO,VC_COLOR,VC_SIZE,VC_POL,VC_POD,NU_NO_PCS_CTNS,VC_COMMODITY,DT_ETD,VC_TOT_PCS,VC_QUAN,VC_QUA_UOM,NU_LENGTH,NU_WIDTH,NU_HIEGHT,VC_IN_HCM,VC_CBM_SEA,VC_VOLUME,VC_GW_CAR,VC_GR_WT,VC_NW_CAR,VC_NT_WT,VC_REF_FIELD1,VC_REF_FIELD2,VC_REF_FIELD3,VC_REF_FIELD4,VC_REF_FIELD5,VC_HS_CODE,VC_QC_DT,VC_REL_DT,SALES_ORG,SAP_QUOTATION,FILE_UPLOAD_DATE,VC_BUYER,VC_BUY_HOUSE,FILE_NAME,VC_DIVISION,PO_CREATION_DATE,IS_ACTIVE,PROJECT_NO,UNIT,COMMODITY,PAY_CONDAY_COND,UNIT_PRIC_PCS,CURRENCY_UNIT,PIECES_PER_CTN,GWEIGHT_PER_CARTON,NET_WEIGHT_PER_CARTON,ITEM_DESCRIPTION ) VALUES (#{seg_id},#{po_id},#{vc_po_no},#{nu_client_code},#{vc_product_no},#{vc_sku_no},#{vc_style_no},#{vc_article_no},#{vc_color},#{vc_size},#{vc_pol},#{vc_pod},#{nu_no_pcs_ctns},#{vc_commodity},#{dt_etd},#{vc_tot_pcs},#{vc_quan},#{vc_qua_uom},#{nu_length},#{nu_width},#{nu_height},#{vc_in_hcm},#{vc_cbm_sea},#{vc_volume},#{vc_gw_car},#{vc_gr_wt},#{vc_nw_car},#{vc_nt_wt},#{vc_ref_field1},#{vc_ref_field2},#{vc_ref_field3},#{vc_ref_field4},#{vc_ref_field5},#{vc_hs_code},#{vc_qc_dt},#{vc_rel_dt},#{sales_org},#{sap_quotation},#{file_upload_date},#{vc_buyer},#{vc_buy_house},#{file_name},#{vc_division},#{currentDate},#{isRecordActive},#{projectNo},#{unit},#{commodity},#{payCondayCond},#{unitPricePerPcs},#{currencyUnit},#{piecesPerCTN},#{gweightPerCarton},#{netWeightPerCarton},#{itemDescription})";
	
	
	
	@SelectKey(
            keyProperty = "seg_id",
            before = true,
            resultType = Long.class,
            statement = { "SELECT T_SEG_PURCHASE_ORDER_SEQ.nextval AS seg_id FROM dual" })
	@Insert(insert)
	public void insert(SegPurchaseOrder segPurchaseOrder);
	
	
}
