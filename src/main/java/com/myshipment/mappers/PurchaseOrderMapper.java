package com.myshipment.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;

import com.myshipment.model.HeaderPOData;
import com.myshipment.model.PurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface PurchaseOrderMapper {

	//INSERT_PURCHASE_ORDER = "INSERT INTO T_PURCHASE_ORDER (PO_ID,UPLOAD_ID,VC_PO_NO,NU_CLIENT_CODE,VC_HS_CODE,SALES_ORG,VC_BUYER,VC_BUY_HOUSE,COMPANY_CODE,SUPPLIER_CODE) VALUES (#{po_id},#{upload_id},#{vc_po_no},#{nu_client_code},#{vc_hs_code},#{sales_org},#{vc_buyer},#{vc_buy_house},#{company_code},#{supplier_code})";
	public final String INSERT_PURCHASE_ORDER = "INSERT INTO T_PURCHASE_ORDER (PO_ID,UPLOAD_ID,VC_PO_NO,NU_CLIENT_CODE,SALES_ORG,VC_BUYER,VC_BUY_HOUSE,PO_CREATION_DATE,SUPPLIER_CODE) VALUES (#{po_id},#{upload_id},#{vc_po_no},#{nu_client_code},#{sales_org},#{vc_buyer},#{vc_buy_house},#{currentDate},#{supplier_code})";
	
	
	@SelectKey(
            keyProperty = "po_id",
            before = true,
            resultType = Long.class,
            statement = { "SELECT T_PURCHASE_ORDER_SEQ.nextval AS po_id FROM dual" })
	@Insert(INSERT_PURCHASE_ORDER)
	public void insert(HeaderPOData headerPOData);
}

