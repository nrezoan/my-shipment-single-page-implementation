/**
*
*@author Ahmad Naquib
*/
package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.DefaultCompany;
import com.myshipment.model.IntelligentCompanyUsage;

public interface CompanySelectionMapper {

	public final String SELECT_INTELLIGENT_COMPANY_USAGE_LIST = "SELECT SUM(COUNTER) AS totalHit, "
			+ "DIST_CHANNEL AS distributionChannel, DIVISION AS division, COMPANY AS company "
			+ "FROM T_PAGE_FREQUENCY WHERE USER_NAME=#{user_code} group by DIST_CHANNEL, DIVISION, "
			+ "COMPANY ORDER BY totalHit DESC";
	
	public final String SELECT_DEFAULT_COMPANY = "SELECT USER_ID AS userId, PREFERRED_COMPANY AS preferredCompanyId, "
			+ "DISTRIBUTION_CHANNEL  AS distributionChannel, DIVISION AS division, "
			+ "ACTIVE AS active, UPDATED_DATE AS updatedDate "
			+ "FROM T_DEFAULT_COMPANY WHERE USER_ID=#{user_code}";
	
	public final String INSERT_DEFAULT_COMPANY = "INSERT INTO T_DEFAULT_COMPANY"
			+ "(USER_ID, PREFERRED_COMPANY, DISTRIBUTION_CHANNEL, DIVISION, ACTIVE, UPDATED_DATE) "
			+ "VALUES(#{user_code}, #{preferred_company}, #{distribution_channel}, #{division}, #{active}, #{updated_date})";
	
	public final String UPDATE_DEFAULT_COMPANY = "UPDATE T_DEFAULT_COMPANY "
			+ "SET PREFERRED_COMPANY=#{preferred_company}, DISTRIBUTION_CHANNEL=#{distribution_channel}, "
			+ "DIVISION=#{division}, ACTIVE=#{active} "
			+ "WHERE USER_ID=#{user_code}";
	
	public final String UPDATE_ACTIVE_DEFAULT_COMPANY = "UPDATE T_DEFAULT_COMPANY "
			+ "SET ACTIVE=#{active} "
			+ "WHERE USER_ID=#{user_code}";
	
	@Select(SELECT_INTELLIGENT_COMPANY_USAGE_LIST)
	public List<IntelligentCompanyUsage> intelligentCompanyUsageList(@Param("user_code") String userCode);
	
	@Select(SELECT_DEFAULT_COMPANY)
	public DefaultCompany selectedDefaultCompany(@Param("user_code") String userCode);
	
	@Insert(INSERT_DEFAULT_COMPANY)
	public int insertDefaultCompany(@Param("user_code") String userCode, @Param("preferred_company") String preferredCompany, @Param("distribution_channel") String distributionChannel, @Param("division") String division, @Param("active") String active, @Param("updated_date") String updated_date);
	
	@Update(UPDATE_DEFAULT_COMPANY)
	public int updateDefaultCompany(@Param("user_code") String userCode, @Param("preferred_company") String preferredCompany, @Param("distribution_channel") String distributionChannel, @Param("division") String division, @Param("active") String active, @Param("updated_date") String updated_date);

	@Update(UPDATE_ACTIVE_DEFAULT_COMPANY)
	public int setActiveDefaultCompany(@Param("user_code") String userCode, @Param("active") String active, @Param("updated_date") String updated_date);
}
