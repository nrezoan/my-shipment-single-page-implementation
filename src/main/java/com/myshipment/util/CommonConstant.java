package com.myshipment.util;

public class CommonConstant {
	
	public static final String EXPORTDT = "EX";
	public static final String IMPORTDT = "IM";
	public static final String CHADT = "CHA";
	public static final String DATE_FORMAT_DD_MM_YYYY="dd-MM-yyyy";
	public static final String PO_APPROVE_ACTION_NAME="POAPPROVE";
	public static final String PO_ASSIGNCARRIER_ACTION_NAME="ASSIGNCARRIER";
	public static final String DEFAULT_DIVISION_SELECTED="SE";

	public static final String STATUS_TYPE_ORDER_BOOKED = "Order Booked";
	public static final String STATUS_TYPE_CARGO_RECEIVED = "Cargo Received";
	public static final String STATUS_TYPE_STUFFING_DONE = "Stuffing Done";
	public static final String STATUS_TYPE_CARGO_DEPARTED = "Departed";
	public static final String STATUS_TYPE_CARGO_ARRIVED = "Arrived";
	public static final String STATUS_TYPE_GR_DONE = "GR Done";
	public static final String STATUS_TYPE_SHIPMENT_DONE = "Shipment Done";
	
	
	public static final String EXP_STATUS_DELAYED = "DELAYED";
	public static final String EXP_STATUS_ON_TIME = "ON-TIME";
	public static final String EXP_STATUS_ADVANCE = "ADVANCE";
	
	 
	public static final String STATUS_TYPE_NONE = "NONE";
	public static final String BUYER_CODE1="ZMBY";
	public static final String SUPPLIER_CODE1="ZMSP";
	public static final String BUYER_CODE2="ZMBH";
	public static final String SUPPLIER_CODE2="ZMFF";
	public static final String DA_CODE1="ZMDA";
	public static final String DA_CODE2="ZMOA";
	public static final String CRM_CODE1="ZEMP";
	public static final String CRM_CODE2="ZEMP";
	public static final String NO_BUYER_SELECTED = "NONE";
	public static final String DIRECT_BOOKING_EMAIL_TEMPLATE_NAME="mail.directbooking.templateName";
	public static final String DIRECT_BOOKING_MAIL_TO="mail.directbooking.mailTo";
	public static final String DIRECT_BOOKING_MAIL_FROM="mail.directbooking.mailFrom";
	public static final String DIRECT_BOOKING_MAIL_SUBJECT="mail.directbooking.subject";
	public static final String PO_GENERATING_EMAIL_TEMPLATE_NAME="mail.generatingPO.templateName";
	public static final String PO_GENERATING_MAIL_TO="mail.generatingPO.mailTo";
	public static final String PO_GENERATING_MAIL_FROM="mail.generatingPO.mailFrom";
	public static final String PO_GENERATING_MAIL_SUBJECT="mail.generatingPO.subject";
	public static final String BOOKING_BY_PO_MAIL_SUBJECT="mail.bookingbypo.subject"; 
	public static final String FILE_SEPERATOR = "/";
	
	//For open tracking
	
	public static final String SEA_EXPORT_HBL  = "01";
	public static final String SEA_IMPORT_HBL  = "02";
	public static final String AIR_EXPORT_HAWB = "03";
	public static final String AIR_IMPORT_HAWB = "04";
	public static final String SEA_EXPORT_MBL  = "05";
	public static final String SEA_IMPORT_MBL  = "06";
	public static final String AIR_EXPORT_MAWB = "07";
	public static final String AIR_IMPORT_MAWB = "08";
	
	
	//OAG Flight Status
	public static final String IN_AIR = "INAIR";
	public static final String OUT_GATE = "OUTGATE";
	public static final String SCHEDULED = "SCHEDULED";
	public static final String ARRIVED = "ARRIVED";
	
	
	//For PO upload automation
	
		public static final String PO_UPLOAD_FILE_LOCATION = "/Users/RanjeetKumar/Documents/C4XMLFiles";
}
