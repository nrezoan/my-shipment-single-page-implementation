package com.myshipment.util;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.sun.mail.util.MailSSLSocketFactory;


public class SendMail {
	
	//private static final String SMTP_HOST_NAME = "117.120.1.26";
	private static final String SMTP_HOST_NAME = "202.65.11.11";
	//private static final String SMTP_HOST_NAME = "mail.mghgroup.com";
    private static final String SMTP_AUTH_USER = "user";
    private static final String SMTP_AUTH_PWD  = "smtp123";

    private String emailSubject;
    private String emailTo;
    private String emailFrom;
    private String emailCC;
    private String attachFilePath;
    private String bodyContent;
    
    /*
    public static void main(String[] args) throws Exception{
       new SendMail().javaSendMailWithAttachent("edi.inditex@mghgroup.com","ifta.khirul@mghgroup.com","ifticse_kuet@hotmail.com","Email From Inditex EDI System","aa","C:\\lock.txt");
    }
    */

    public boolean javaSendMailWithAttachent(String from,String to,String cc,String subject,String body,String filePath) throws Exception{
    	
    	String[] ccArr=cc.split(";");
    	InternetAddress[] addressCC = new InternetAddress[ccArr.length];
    	for(int i=0;i<ccArr.length;i++)
    		addressCC[i]=new InternetAddress(ccArr[i]); 
    	
    	
    	boolean res=false;
    	emailFrom=from;
    	emailTo=to;
    	emailCC=cc;
    	emailSubject=subject;
    	bodyContent=body;
    	attachFilePath=filePath;
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session mailSession=null;
        try{
        	mailSession = Session.getDefaultInstance(props, auth);
        }
        catch(Exception ex)
        {
        	mailSession=Session.getInstance(props, auth);
        }
        // uncomment for debugging infos to stdout
         //mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);

        message.setSubject(emailSubject);
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(bodyContent, "text/html");
        MimeBodyPart attachFilePart = new MimeBodyPart();
        
        if(attachFilePath!=null)
        {
        FileDataSource fds = new FileDataSource(attachFilePath);
        attachFilePart.setDataHandler(new DataHandler(fds));
        attachFilePart.setFileName(fds.getName());
        }

        Multipart mp = new MimeMultipart();
        mp.addBodyPart(textPart);
        mp.addBodyPart(attachFilePart);

        message.setContent(mp);
        
        message.setFrom(new InternetAddress(emailFrom));
        message.addRecipient(Message.RecipientType.TO,new InternetAddress(emailTo));
        message.addRecipients(Message.RecipientType.CC, addressCC);
        

        try{
        transport.connect();
        //transport.sendMessage(message,message.getRecipients(Message.RecipientType.TO));
        transport.send(message);
        transport.close();
        res=true;
        }
        catch(Exception ex){ex.printStackTrace();}
        System.out.println("Email Send.");
        
        return res;
    }
public boolean javaSendInspectionConfirmationMailWithAttachent(String from,String to,String cc,String subject,String body,String filePath) throws Exception{
    	
    	
    	boolean res=false;
    	emailFrom=from;
    	emailTo=to;
    	emailCC=cc;
    	emailSubject=subject;
    	bodyContent=body;
    	attachFilePath=filePath;
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session mailSession=null;
        try{
        	mailSession = Session.getDefaultInstance(props, auth);
        }
        catch(Exception ex)
        {
        	mailSession=Session.getInstance(props, auth);
        }
        // uncomment for debugging infos to stdout
         //mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);

        message.setSubject(emailSubject);
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(bodyContent, "text/html");
        MimeBodyPart attachFilePart = new MimeBodyPart();
        
        if(attachFilePath!=null)
        {
        FileDataSource fds = new FileDataSource(attachFilePath);
        attachFilePart.setDataHandler(new DataHandler(fds));
        attachFilePart.setFileName(fds.getName());
        }

        Multipart mp = new MimeMultipart();
        mp.addBodyPart(textPart);
        mp.addBodyPart(attachFilePart);

        message.setContent(mp);
        
        message.setFrom(new InternetAddress(emailFrom));
        message.addRecipients(Message.RecipientType.TO,emailTo);
        message.addRecipients(Message.RecipientType.CC, emailCC);
        

        try{
        transport.connect();
        //transport.sendMessage(message,message.getRecipients(Message.RecipientType.TO));
        transport.send(message);
        transport.close();
        res=true;
        }
        catch(Exception ex){ex.printStackTrace();}
        System.out.println("Email Send.");
        
        return res;
    }


 public boolean javaSendMail(String from,String to,String cc,String subject,String body) throws Exception{
    	
//    	boolean res=false;
//    	emailFrom = from;
//    	emailTo = to;
//    	emailCC = cc;
//    	emailSubject = subject;
//    	bodyContent = body;
    	
    	/*
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session mailSession=null;
        try{
        	mailSession = Session.getDefaultInstance(props, auth);
        }
        catch(Exception ex)
        {
        	mailSession=Session.getInstance(props, auth);
        }
        // uncomment for debugging infos to stdout
         //mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);

        message.setSubject(emailSubject);
        MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(bodyContent, "text/html");
      

        Multipart mp = new MimeMultipart();
        mp.addBodyPart(textPart);
       // mp.addBodyPart(attachFilePart);

        message.setContent(mp);
        
        message.setFrom(new InternetAddress(emailFrom));
        message.addRecipients(Message.RecipientType.TO,emailTo);
      //  if(addressCC.length>0)
      //  message.addRecipients(Message.RecipientType.CC, addressCC);        

        try{
        transport.connect();
        //transport.sendMessage(message,message.getRecipients(Message.RecipientType.TO));
        transport.send(message);
        transport.close();
        res=true;
        }
        catch(Exception ex){ex.printStackTrace();}
        System.out.println("Email Send.");	*/
        
        
        //new codes
    	boolean res=false;
    	emailFrom = from;
    	emailTo = to;
    	emailCC = cc;
    	emailSubject = subject;
    	bodyContent = body;
    	
        final String username = "no-reply@myshipment.com";
        final String password = "A5vu23#x";

        Properties props = new Properties();
        try{
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "117.120.5.149");
        props.put("mail.debug", "true");
        props.put("mail.smtp.EnableSSL.enable","true");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.starttls.enable", "true");
        
        MailSSLSocketFactory sf=new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        
        props.put("mail.smtp.ssl.socketFactory", sf);
                
         }catch(Exception ex){
              ex.printStackTrace();
        }
        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
              }
          });

        try {

              Message message = new MimeMessage(session);
              message.setFrom(new InternetAddress(emailFrom));
              message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
              message.setSubject(emailSubject);
              //message.setText("Dear Mail Crawler," + "\n\n No spam to my email, please!");
              MimeBodyPart textPart = new MimeBodyPart();
              textPart.setContent(bodyContent, "text/html");
              Multipart mp = new MimeMultipart();
              mp.addBodyPart(textPart);
              message.setContent(mp);

              Transport.send(message);
              res=true;
              System.out.println("Done");

        } catch (MessagingException e) {
              e.printStackTrace();
              throw new RuntimeException(e);
        }
        //new code ends
        
        
        
        return res;
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
           String username = SMTP_AUTH_USER;
           String password = SMTP_AUTH_PWD;
           return new PasswordAuthentication(username, password);
        }
    }
    
    public void emailSend() throws Exception{
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session mailSession = Session.getDefaultInstance(props, auth);
        // uncomment for debugging infos to stdout
        // mailSession.setDebug(true);
        Transport transport = mailSession.getTransport();

        MimeMessage message = new MimeMessage(mailSession);
        message.setContent("This is a test", "text/plain");
        message.setFrom(new InternetAddress("me@myhost.org"));
        message.addRecipient(Message.RecipientType.TO,
             new InternetAddress("elvis@presley.org"));

        transport.connect();
        transport.sendMessage(message,
            message.getRecipients(Message.RecipientType.TO));
    }
    
    public static void main(String[] args) 
    {
    	SendMail sendMail=new SendMail();
    	String emailBodyContent="";
    	emailBodyContent="Successfully submitted one Shipment request to Inditex web service";
    	try{
    	sendMail.javaSendMailWithAttachent("edi.inditex@mghgroup.com", "ifta.khirul@mghgroup.com", "ifta.khirul@mghgroup.com", "Shipment Success !!!", emailBodyContent, null);
    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    
	}
    
    
}
