/**
*
*@author Ahmad Naquib
*/
package com.myshipment.util;

public class MyshipmentJspMapper {

	//public static final String LANDING_PAGE = "landing_myshipment_v2";
	public static final String LANDING_PAGE = "myshipment_landing";
	public static final String DASHBOARD_SHIPPER = "dashboard_shipper";
	public static final String DASHBOARD_BUYER = "dashboard_buyer";
	public static final String DASHBOARD_AGENT = "dashboard_agent";
	public static final String DASHBOARD_CRM = "dashboard_crm";
	public static final String EDIT_PASSWORD_PAGE = "changePassword";
	
}
