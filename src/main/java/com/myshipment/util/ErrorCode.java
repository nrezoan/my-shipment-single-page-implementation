package com.myshipment.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorCode implements Serializable {

	private static final Long serialVersionUID=-355321876L;
	
	public static final ErrorCode SERVICE_CONNECTION_ERROR=new ErrorCode(0l);
	
	public ErrorCode(Long errorCode)
	{
		this.errorCode=errorCode;
	}
	private Long errorCode =  new Long(-1);
	private List<String> parameters = new ArrayList<String>();
	private String detailMessage;
	
	public Long getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}
	public List<String> getParameters() {
		return parameters;
	}
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
	public String getDetailMessage() {
		return detailMessage;
	}
	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}
	
	
	
	
	
	
}
