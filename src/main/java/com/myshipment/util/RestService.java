package com.myshipment.util;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class RestService extends RestTemplate{
	
	

	@Override
	public void setMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
		// TODO Auto-generated method stub
		super.setMessageConverters(messageConverters);
	}

	@Override
	public void setErrorHandler(ResponseErrorHandler errorHandler) {
		// TODO Auto-generated method stub
		super.setErrorHandler(errorHandler);
	}

	@Override
	public ResponseErrorHandler getErrorHandler() {
		// TODO Auto-generated method stub
		return super.getErrorHandler();
	}

	@Override
	public <T> T getForObject(String url, Class<T> responseType, Object... urlVariables) throws RestClientException {
		// TODO Auto-generated method stub
		return super.getForObject(url, responseType, urlVariables);
	}

	@Override
	public <T> T getForObject(String url, Class<T> responseType, Map<String, ?> urlVariables)
			throws RestClientException {
		// TODO Auto-generated method stub
		return super.getForObject(url, responseType, urlVariables);
	}

	@Override
	public <T> T getForObject(URI url, Class<T> responseType) throws RestClientException {
		// TODO Auto-generated method stub
		return super.getForObject(url, responseType);
	}

	@Override
	public <T> ResponseEntity<T> getForEntity(String url, Class<T> responseType, Object... urlVariables)
			throws RestClientException {
		// TODO Auto-generated method stub
		return super.getForEntity(url, responseType, urlVariables);
	}

	@Override
	public <T> ResponseEntity<T> getForEntity(URI url, Class<T> responseType) throws RestClientException {
		// TODO Auto-generated method stub
		return super.getForEntity(url, responseType);
	}

	@Override
	public HttpHeaders headForHeaders(URI url) throws RestClientException {
		// TODO Auto-generated method stub
		return super.headForHeaders(url);
	}

	@Override
	public URI postForLocation(URI url, Object request) throws RestClientException {
		// TODO Auto-generated method stub
		return super.postForLocation(url, request);
	}

	@Override
	public <T> T postForObject(URI url, Object request, Class<T> responseType) throws RestClientException {
		// TODO Auto-generated method stub
		return super.postForObject(url, request, responseType);
	}

	@Override
	public <T> ResponseEntity<T> postForEntity(URI url, Object request, Class<T> responseType)
			throws RestClientException {
		// TODO Auto-generated method stub
		return super.postForEntity(url, request, responseType);
	}

	@Override
	public void put(String url, Object request, Object... urlVariables) throws RestClientException {
		// TODO Auto-generated method stub
		super.put(url, request, urlVariables);
	}

	@Override
	public void put(URI url, Object request) throws RestClientException {
		// TODO Auto-generated method stub
		super.put(url, request);
	}

	@Override
	public void delete(String url, Object... urlVariables) throws RestClientException {
		// TODO Auto-generated method stub
		super.delete(url, urlVariables);
	}

	@Override
	public void delete(String url, Map<String, ?> urlVariables) throws RestClientException {
		// TODO Auto-generated method stub
		super.delete(url, urlVariables);
	}

	@Override
	public Set<HttpMethod> optionsForAllow(URI url) throws RestClientException {
		// TODO Auto-generated method stub
		return super.optionsForAllow(url);
	}

	@Override
	protected void handleResponse(URI url, HttpMethod method, ClientHttpResponse response) throws IOException {
		// TODO Auto-generated method stub
		super.handleResponse(url, method, response);
	}

	
}
