package com.myshipment.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.stereotype.Service;

import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.TemplateModel;

@Service
public class ReadExcelFile {

	public List<PurchaseOrdersModel> readExcel(File file, TemplateModel template, String buyerId, String crmId)
			throws EncryptedDocumentException, InvalidFormatException, IOException {

		List<PurchaseOrdersModel> purchaseOrderList = new ArrayList<PurchaseOrdersModel>();

		PurchaseOrdersModel purchaseOrder = new PurchaseOrdersModel();

		String po_number = template.getPo_number();
		String shipper = template.getShipper();
		String template_id = template.getTemplate_id();
		Integer active = template.getActive();
		String total_gw = template.getTotal_gw();
		String carton_length = template.getCarton_height();
		String carton_height = template.getCarton_width();
		String carton_width = template.getCarton_width();
		String total_pieces = template.getTotal_pieces();
		String carton_quantity = template.getCarton_quantity();
		String carton_unit = template.getCarton_unit();
		String total_cbm = template.getTotal_cbm();
		String hs_code = template.getHs_code();
		String commodity = template.getCommodity();
		String style_no = template.getStyle_no();
		String wh_code = template.getWh_code();
		String size_no = template.getSize_no();
		String sku = template.getSku();
		String article_no = template.getArticle_no();
		String color = template.getColor();
		String total_nw = template.getTotal_nw();
		String comm_inv = template.getComm_inv();
		String comm_inv_date = template.getComm_inv_date();
		String pieces_ctn = template.getPieces_ctn();
		String carton_sr_no = template.getCarton_sr_no();
		String reference_1 = template.getReference_1();
		String reference_2 = template.getReference_2();
		String reference_3 = template.getReference_3();
		String terms_of_shipment = template.getTerms_of_shipment();
		String freight_mode = template.getFreight_mode();
		String division = template.getDivision();
		String pol = template.getPol();
		String pod = template.getPod();
		String place_of_delivery = template.getPlace_of_delivery();
		String place_of_delivery_address = template.getPlace_of_delivery_address();
		String cargo_handover_date = template.getCargo_handover_date();
		String fvsl = template.getFvsl();
		String mvsl1 = template.getMvsl1();
		String mvsl2 = template.getMvsl2();
		String mvsl3 = template.getMvsl3();
		String voyage1 = template.getVoyage1();
		String voyage2 = template.getVoyage2();
		String voyage3 = template.getVoyage3();
		String etd = template.getEtd();
		String transhipment1etd = template.getTranshipment1etd();
		String transhipment2etd = template.getTranshipment2etd();
		String ata = template.getAta();
		String atd = template.getAtd();
		String transhipment1eta = template.getTranshipment1eta();
		String transhipment2eta = template.getTranshipment2eta();
		String eta = template.getEta();

		int po_number_col = Integer.MAX_VALUE;
		int shipper_col = Integer.MAX_VALUE;
		int template_id_col = 0;
		int active_col = 0;
		int total_gw_col = Integer.MAX_VALUE;
		int carton_length_col = Integer.MAX_VALUE;
		int carton_height_col = Integer.MAX_VALUE;
		int carton_width_col = Integer.MAX_VALUE;
		int total_pieces_col = Integer.MAX_VALUE;
		int carton_quantity_col = Integer.MAX_VALUE;
		int carton_unit_col = Integer.MAX_VALUE;
		int total_cbm_col = Integer.MAX_VALUE;
		int hs_code_col = Integer.MAX_VALUE;
		int commodity_col = Integer.MAX_VALUE;
		int style_no_col = Integer.MAX_VALUE;
		int wh_code_col = Integer.MAX_VALUE;
		int size_no_col = Integer.MAX_VALUE;
		int sku_col = Integer.MAX_VALUE;
		int article_no_col = Integer.MAX_VALUE;
		int color_col = Integer.MAX_VALUE;
		int total_nw_col = Integer.MAX_VALUE;
		int comm_inv_col = Integer.MAX_VALUE;
		int comm_inv_date_col = Integer.MAX_VALUE;
		int pieces_ctn_col = Integer.MAX_VALUE;
		int carton_sr_no_col = Integer.MAX_VALUE;
		int reference_1_col = Integer.MAX_VALUE;
		int reference_2_col = Integer.MAX_VALUE;
		int reference_3_col = Integer.MAX_VALUE;
		int terms_of_shipment_col = Integer.MAX_VALUE;
		int freight_mode_col = Integer.MAX_VALUE;
		int division_col = Integer.MAX_VALUE;
		int pol_col = Integer.MAX_VALUE;
		int pod_col = Integer.MAX_VALUE;
		int place_of_delivery_col = Integer.MAX_VALUE;
		int place_of_delivery_address_col = Integer.MAX_VALUE;
		int cargo_handover_date_col = Integer.MAX_VALUE;

		int fvsl_col = Integer.MAX_VALUE;
		int mvsl1_col = Integer.MAX_VALUE;
		int mvsl2_col = Integer.MAX_VALUE;
		int mvsl3_col = Integer.MAX_VALUE;
		int voyage1_col = Integer.MAX_VALUE;
		int voyage2_col = Integer.MAX_VALUE;
		int voyage3_col = Integer.MAX_VALUE;
		int etd_col = Integer.MAX_VALUE;
		int transhipment1etd_col = Integer.MAX_VALUE;
		int transhipment2etd_col = Integer.MAX_VALUE;
		int ata_col = Integer.MAX_VALUE;
		int atd_col = Integer.MAX_VALUE;
		int transhipment1eta_col = Integer.MAX_VALUE;
		int transhipment2eta_col = Integer.MAX_VALUE;
		int eta_col = Integer.MAX_VALUE;

		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(file);

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		// List<Product> products = new ArrayList<Product>();

		Row row0 = sheet.getRow(0);
		Iterator<Cell> cellIterator = row0.cellIterator();

		while (cellIterator.hasNext()) {

			Cell cell = cellIterator.next();
			String cellName = dataFormatter.formatCellValue(cell);

			if (cellName.equals(po_number)) {
				po_number_col = cell.getColumnIndex();
			}

			else if (cellName.equals(shipper)) {
				shipper_col = cell.getColumnIndex();
			}

			else if (cellName.equals(total_gw)) {
				total_gw_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_length)) {
				carton_length_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_height)) {
				carton_height_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_width)) {
				carton_width_col = cell.getColumnIndex();
			}

			else if (cellName.equals(total_pieces)) {
				total_pieces_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_quantity)) {
				carton_quantity_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_quantity)) {
				carton_quantity_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_unit)) {
				carton_unit_col = cell.getColumnIndex();
			}

			else if (cellName.equals(total_cbm)) {
				total_cbm_col = cell.getColumnIndex();
			}

			else if (cellName.equals(hs_code)) {
				hs_code_col = cell.getColumnIndex();
			}

			else if (cellName.equals(commodity)) {
				commodity_col = cell.getColumnIndex();
			}

			else if (cellName.equals(style_no)) {
				style_no_col = cell.getColumnIndex();
			}

			else if (cellName.equals(wh_code)) {
				wh_code_col = cell.getColumnIndex();
			}

			else if (cellName.equals(size_no)) {
				size_no_col = cell.getColumnIndex();
			}

			else if (cellName.equals(sku)) {
				sku_col = cell.getColumnIndex();
			}

			else if (cellName.equals(article_no)) {
				article_no_col = cell.getColumnIndex();
			}

			else if (cellName.equals(color)) {
				color_col = cell.getColumnIndex();
			}

			else if (cellName.equals(total_nw)) {
				total_nw_col = cell.getColumnIndex();
			}

			else if (cellName.equals(comm_inv)) {
				comm_inv_col = cell.getColumnIndex();
			}

			else if (cellName.equals(comm_inv_date)) {
				comm_inv_date_col = cell.getColumnIndex();
			}

			else if (cellName.equals(pieces_ctn)) {
				pieces_ctn_col = cell.getColumnIndex();
			}

			else if (cellName.equals(carton_sr_no)) {
				carton_sr_no_col = cell.getColumnIndex();
			}

			else if (cellName.equals(reference_1)) {
				reference_1_col = cell.getColumnIndex();
			}

			else if (cellName.equals(reference_2)) {
				reference_2_col = cell.getColumnIndex();
			}

			else if (cellName.equals(reference_3)) {
				reference_3_col = cell.getColumnIndex();
			}

			else if (cellName.equals(terms_of_shipment)) {
				terms_of_shipment_col = cell.getColumnIndex();
			}

			else if (cellName.equals(freight_mode)) {
				freight_mode_col = cell.getColumnIndex();
			}

			else if (cellName.equals(division)) {
				division_col = cell.getColumnIndex();
			}

			else if (cellName.equals(pol)) {
				pol_col = cell.getColumnIndex();
			}

			else if (cellName.equals(pod)) {
				pod_col = cell.getColumnIndex();
			}

			else if (cellName.equals(place_of_delivery)) {
				place_of_delivery_col = cell.getColumnIndex();
			}

			else if (cellName.equals(place_of_delivery_address)) {
				place_of_delivery_address_col = cell.getColumnIndex();
			}

			else if (cellName.equals(cargo_handover_date)) {
				cargo_handover_date_col = cell.getColumnIndex();
			}

			else if (cellName.equals(fvsl)) {
				fvsl_col = cell.getColumnIndex();
			}

			else if (cellName.equals(mvsl1)) {
				mvsl1_col = cell.getColumnIndex();
			}

			else if (cellName.equals(mvsl2)) {
				mvsl2_col = cell.getColumnIndex();
			}

			else if (cellName.equals(mvsl3)) {
				mvsl3_col = cell.getColumnIndex();
			}

			else if (cellName.equals(voyage1)) {
				voyage1_col = cell.getColumnIndex();
			}

			else if (cellName.equals(voyage2)) {
				voyage2_col = cell.getColumnIndex();
			}

			else if (cellName.equals(voyage3)) {
				voyage3_col = cell.getColumnIndex();
			}

			else if (cellName.equals(etd)) {
				etd_col = cell.getColumnIndex();
			}

			else if (cellName.equals(transhipment1etd)) {
				transhipment1etd_col = cell.getColumnIndex();
			}

			else if (cellName.equals(transhipment2etd)) {
				transhipment2etd_col = cell.getColumnIndex();
			}

			else if (cellName.equals(ata)) {
				ata_col = cell.getColumnIndex();
			}

			else if (cellName.equals(atd)) {
				atd_col = cell.getColumnIndex();

			}

			else if (cellName.equals(transhipment1eta)) {
				transhipment1eta_col = cell.getColumnIndex();
			}

			else if (cellName.equals(transhipment2eta)) {
				transhipment2eta_col = cell.getColumnIndex();
			}

			else if (cellName.equals(eta)) {
				eta_col = cell.getColumnIndex();
			}
		}

		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();
			System.out.println("Iterating Row");
			if (row.getRowNum() != 0) {

				purchaseOrder.setBuyer_id(buyerId);
				purchaseOrder.setEntry_date(new Date());
				purchaseOrder.setEntry_by(crmId);

				// System.out.println("Iterating Column");

				Cell poNumberCell = row.getCell(po_number_col);
				String poNumberCheck = dataFormatter.formatCellValue(poNumberCell);

				// if there is no po number/order number that row will not be iterated
				if (!poNumberCheck.equals("")) {

					if (poNumberCell != null) {

						String poNumber = dataFormatter.formatCellValue(poNumberCell);
						purchaseOrder.setPo_no(poNumber);

					} else {

						purchaseOrder.setPo_no("");
					}

					Cell shipperCell = row.getCell(shipper_col);
					if (shipperCell != null) {

						String shipperStr = dataFormatter.formatCellValue(shipperCell);
						try {

							int numberConversion = Integer.parseInt(shipperStr);
							purchaseOrder.setShipper(shipperStr);

						} catch (NumberFormatException ex) {

							purchaseOrder.setShipper("");
						}
						// purchaseOrder.setShipper(shipperStr);

					} else {

						purchaseOrder.setShipper("");
					}

					Cell totalGwCell = row.getCell(total_gw_col);
					if (totalGwCell != null) {

						String totalGw = dataFormatter.formatCellValue(totalGwCell);
						purchaseOrder.setTotal_gw(totalGw);

					} else {

						purchaseOrder.setTotal_gw("");
					}

					Cell cartonLengthCell = row.getCell(carton_length_col);
					if (cartonLengthCell != null) {

						String cartonLength = dataFormatter.formatCellValue(cartonLengthCell);
						purchaseOrder.setCarton_length(cartonLength);

					} else {

						purchaseOrder.setCarton_length("");
					}

					Cell cartonHeightCell = row.getCell(carton_height_col);
					if (cartonHeightCell != null) {

						String cartonHeight = dataFormatter.formatCellValue(cartonHeightCell);
						purchaseOrder.setCarton_height(cartonHeight);

					} else {

						purchaseOrder.setCarton_height("");
					}

					Cell cartonWidthCell = row.getCell(carton_width_col);
					if (cartonWidthCell != null) {

						String cartonWidth = dataFormatter.formatCellValue(cartonWidthCell);
						purchaseOrder.setCarton_width(cartonWidth);

					} else {
						purchaseOrder.setCarton_width("");
					}

					Cell totalPiecesCell = row.getCell(total_pieces_col);
					if (totalPiecesCell != null) {

						String totalPieces = dataFormatter.formatCellValue(totalPiecesCell);
						purchaseOrder.setTotal_pieces(totalPieces);

					} else {

						purchaseOrder.setTotal_pieces("");
					}

					Cell cartonQuantityCell = row.getCell(carton_quantity_col);
					if (cartonQuantityCell != null) {

						String cartonQuantity = dataFormatter.formatCellValue(cartonQuantityCell);
						purchaseOrder.setCarton_quantity(cartonQuantity);

					} else {

						purchaseOrder.setCarton_quantity("");
					}

					Cell cartonUnitCell = row.getCell(carton_unit_col);
					if (cartonUnitCell != null) {

						String cartonUnit = dataFormatter.formatCellValue(cartonUnitCell);
						purchaseOrder.setCarton_unit(cartonUnit);

					} else {

						purchaseOrder.setCarton_unit("");
					}

					Cell totalCbmCell = row.getCell(total_cbm_col);
					if (totalCbmCell != null) {

						String totalCbm = dataFormatter.formatCellValue(totalCbmCell);
						purchaseOrder.setTotal_cbm(totalCbm);

					} else {

						purchaseOrder.setTotal_cbm("");
					}

					Cell hsCodeCell = row.getCell(hs_code_col);
					if (hsCodeCell != null) {

						String hsCode = dataFormatter.formatCellValue(hsCodeCell);
						purchaseOrder.setHs_code(hsCode);

					} else {

						purchaseOrder.setHs_code("");
					}

					Cell commodityCell = row.getCell(commodity_col);
					if (commodityCell != null) {

						String commodityStr = dataFormatter.formatCellValue(commodityCell);
						purchaseOrder.setCommodity(commodityStr);

					} else {
						purchaseOrder.setCommodity("");
					}

					Cell styleNoCell = row.getCell(style_no_col);
					if (styleNoCell != null) {

						String styleNo = dataFormatter.formatCellValue(styleNoCell);
						purchaseOrder.setStyle_no(styleNo);

					} else {

						purchaseOrder.setStyle_no("");
					}

					Cell whCodeCell = row.getCell(wh_code_col);
					if (whCodeCell != null) {

						String whCode = dataFormatter.formatCellValue(whCodeCell);
						purchaseOrder.setWh_code(whCode);

					} else {

						purchaseOrder.setWh_code("");
					}

					Cell sizeNoCell = row.getCell(size_no_col);
					if (sizeNoCell != null) {

						String sizeNo = dataFormatter.formatCellValue(sizeNoCell);
						purchaseOrder.setSize_no(sizeNo);

					} else {

						purchaseOrder.setSize_no("");
					}

					Cell skuCell = row.getCell(sku_col);
					if (skuCell != null) {

						String skuStr = dataFormatter.formatCellValue(skuCell);
						purchaseOrder.setSku(skuStr);

					} else {

						purchaseOrder.setSku("");
					}

					Cell articleNoCell = row.getCell(article_no_col);
					if (articleNoCell != null) {

						String articleNo = dataFormatter.formatCellValue(articleNoCell);
						purchaseOrder.setArticle_no(articleNo);

					} else {

						purchaseOrder.setArticle_no("");
					}

					Cell colorCell = row.getCell(color_col);
					if (colorCell != null) {

						String colorStr = dataFormatter.formatCellValue(colorCell);
						purchaseOrder.setColor(colorStr);

					} else {

						purchaseOrder.setColor("");
					}

					Cell totalNwCell = row.getCell(total_nw_col);
					if (totalNwCell != null) {

						String totalNw = dataFormatter.formatCellValue(totalNwCell);
						purchaseOrder.setTotal_nw(totalNw);

					} else {

						purchaseOrder.setTotal_nw("");
					}

					Cell commInvCell = row.getCell(comm_inv_col);
					if (commInvCell != null) {

						String commInv = dataFormatter.formatCellValue(commInvCell);
						purchaseOrder.setComm_inv(commInv);

					} else {

						purchaseOrder.setComm_inv("");
					}

					Cell commInvDateCell = row.getCell(comm_inv_date_col);
					if (commInvDateCell != null) {

						String commInvDate = dataFormatter.formatCellValue(commInvDateCell);
						purchaseOrder.setComm_inv_date(commInvDate);

					} else {

						purchaseOrder.setComm_inv_date("");
					}

					Cell piecesCtnCell = row.getCell(pieces_ctn_col);
					if (piecesCtnCell != null) {

						String piecesCtn = dataFormatter.formatCellValue(piecesCtnCell);
						purchaseOrder.setPieces_ctn(piecesCtn);
					} else {
						purchaseOrder.setPieces_ctn("");
					}

					Cell cartonSrNoCell = row.getCell(carton_sr_no_col);
					if (cartonSrNoCell != null) {

						String cartonSrNo = dataFormatter.formatCellValue(cartonSrNoCell);
						purchaseOrder.setCarton_sr_no(cartonSrNo);

					} else {

						purchaseOrder.setCarton_sr_no("");
					}

					Cell reference1Cell = row.getCell(reference_1_col);
					if (reference1Cell != null) {

						String reference1 = dataFormatter.formatCellValue(reference1Cell);
						purchaseOrder.setReference_1(reference1);

					} else {

						purchaseOrder.setReference_1("");
					}

					Cell reference2Cell = row.getCell(reference_2_col);
					if (reference2Cell != null) {

						String reference2 = dataFormatter.formatCellValue(reference2Cell);
						purchaseOrder.setReference_2(reference2);

					} else {

						purchaseOrder.setReference_2("");
					}

					Cell reference3Cell = row.getCell(reference_3_col);
					if (reference3Cell != null) {

						String reference3 = dataFormatter.formatCellValue(reference3Cell);
						purchaseOrder.setReference_3(reference3);

					} else {
						purchaseOrder.setReference_3("");
					}

					Cell termsOfShipmentCell = row.getCell(terms_of_shipment_col);
					if (termsOfShipmentCell != null) {

						String termsOfShipment = dataFormatter.formatCellValue(termsOfShipmentCell);
						purchaseOrder.setTerms_of_shipment(termsOfShipment);

					} else {
						purchaseOrder.setTerms_of_shipment("");
					}

					Cell freightModeCell = row.getCell(freight_mode_col);
					if (freightModeCell != null) {

						String frieghtMode = dataFormatter.formatCellValue(freightModeCell);
						purchaseOrder.setFreight_mode(frieghtMode);

					} else {
						purchaseOrder.setFreight_mode("");
					}

					Cell divisionCell = row.getCell(division_col);
					if (divisionCell != null) {

						String divisionStr = dataFormatter.formatCellValue(divisionCell);
						purchaseOrder.setDivision(divisionStr);

					} else {
						purchaseOrder.setDivision("");
					}

					Cell polCell = row.getCell(pol_col);
					if (polCell != null) {

						String polStr = dataFormatter.formatCellValue(polCell);
						purchaseOrder.setPol(polStr);

					} else {
						purchaseOrder.setPol("");
					}

					Cell podCell = row.getCell(pod_col);
					if (podCell != null) {

						String podStr = dataFormatter.formatCellValue(podCell);
						purchaseOrder.setPod(podStr);

					} else {

						purchaseOrder.setPod("");
					}

					Cell placeOfDeliveryCell = row.getCell(place_of_delivery_col);
					if (placeOfDeliveryCell != null) {

						String placeOfDelivery = dataFormatter.formatCellValue(placeOfDeliveryCell);
						purchaseOrder.setPlace_of_delivery(placeOfDelivery);

					} else {

						purchaseOrder.setPlace_of_delivery("");
					}

					Cell placeOfDeliveryAddressCell = row.getCell(place_of_delivery_address_col);
					if (placeOfDeliveryAddressCell != null) {

						String placeOfDeliveryAddress = dataFormatter.formatCellValue(placeOfDeliveryAddressCell);
						purchaseOrder.setPlace_of_delivery_address(placeOfDeliveryAddress);

					} else {
						purchaseOrder.setPlace_of_delivery_address("");
					}

					Cell cargoHandoverDateCell = row.getCell(cargo_handover_date_col);
					if (cargoHandoverDateCell != null) {

						String cargoHandoverDate = dataFormatter.formatCellValue(cargoHandoverDateCell);
						purchaseOrder.setCargo_handover_date(cargoHandoverDate);

					} else {

						purchaseOrder.setCargo_handover_date("");
					}

					Cell fvslCell = row.getCell(fvsl_col);
					if (fvslCell != null) {

						String fvslStr = dataFormatter.formatCellValue(fvslCell);
						purchaseOrder.setFvsl(fvslStr);
					} else {

						purchaseOrder.setFvsl("");
					}

					Cell mvsl1Cell = row.getCell(mvsl1_col);
					if (fvslCell != null) {

						String mvsl1Str = dataFormatter.formatCellValue(mvsl1Cell);
						purchaseOrder.setMvsl1(mvsl1Str);

					} else {

						purchaseOrder.setMvsl1("");
					}

					Cell mvsl2Cell = row.getCell(mvsl2_col);
					if (mvsl2Cell != null) {
						String mvsl2Str = dataFormatter.formatCellValue(mvsl2Cell);
						purchaseOrder.setMvsl2(mvsl2Str);

					} else {
						purchaseOrder.setMvsl2("");
					}

					Cell mvsl3Cell = row.getCell(mvsl3_col);
					if (mvsl3Cell != null) {

						String mvsl3Str = dataFormatter.formatCellValue(mvsl3Cell);
						purchaseOrder.setMvsl3(mvsl3Str);

					} else {

						purchaseOrder.setMvsl3("");
					}

					Cell voyage1Cell = row.getCell(voyage1_col);
					if (voyage1Cell != null) {

						String voyage1Str = dataFormatter.formatCellValue(voyage1Cell);
						purchaseOrder.setVoyage1(voyage1Str);

					} else {

						purchaseOrder.setVoyage1("");
					}

					Cell voyage2Cell = row.getCell(voyage2_col);
					if (voyage2Cell != null) {

						String voyage2Str = dataFormatter.formatCellValue(voyage2Cell);
						purchaseOrder.setVoyage2(voyage2Str);

					} else {

						purchaseOrder.setVoyage2("");
					}

					Cell voyage3Cell = row.getCell(voyage3_col);
					if (voyage3Cell != null) {

						String voyage3Str = dataFormatter.formatCellValue(voyage3Cell);
						purchaseOrder.setVoyage3(voyage3Str);

					} else {

						purchaseOrder.setVoyage3("");
					}

					Cell etdCell = row.getCell(etd_col);
					if (etdCell != null) {

						String etdStr = dataFormatter.formatCellValue(etdCell);
						purchaseOrder.setEtd(etdStr);

					} else {

						purchaseOrder.setEtd("");
					}

					Cell transhipment1etdCell = row.getCell(transhipment1etd_col);
					if (transhipment1etdCell != null) {

						String transhipment1etdStr = dataFormatter.formatCellValue(transhipment1etdCell);
						purchaseOrder.setTranshipment1etd(transhipment1etdStr);

					} else {

						purchaseOrder.setTranshipment1etd("");
					}

					Cell transhipment2etdCell = row.getCell(transhipment2etd_col);
					if (transhipment2etdCell != null) {

						String transhipment2etdStr = dataFormatter.formatCellValue(transhipment2etdCell);
						purchaseOrder.setTranshipment2etd(transhipment2etdStr);

					} else {

						purchaseOrder.setTranshipment2etd("");
					}

					Cell ataCell = row.getCell(ata_col);
					if (ataCell != null) {

						String ataStr = dataFormatter.formatCellValue(ataCell);
						purchaseOrder.setAta(ataStr);

					} else {

						purchaseOrder.setAta("");
					}

					Cell atdCell = row.getCell(atd_col);
					if (atdCell != null) {

						String atdStr = dataFormatter.formatCellValue(atdCell);
						purchaseOrder.setAtd(atdStr);

					} else {

						purchaseOrder.setAtd("");
					}
					
					Cell transhipment1etaCell = row.getCell(transhipment1eta_col);
					if (transhipment1etaCell != null) {

						String transhipment1etaStr = dataFormatter.formatCellValue(transhipment1etaCell);
						purchaseOrder.setTranshipment1eta(transhipment1etaStr);

					} else {

						purchaseOrder.setTranshipment1eta("");
					}

					Cell transhipment2etaCell = row.getCell(transhipment2eta_col);
					if (transhipment2etaCell != null) {

						String transhipment2etaStr = dataFormatter.formatCellValue(transhipment2etaCell);
						purchaseOrder.setTranshipment2eta(transhipment2etaStr);

					} else {

						purchaseOrder.setTranshipment2eta("");
					}
					
					Cell etaCell = row.getCell(eta_col);
					if (etaCell != null) {

						String etaStr = dataFormatter.formatCellValue(etaCell);
						purchaseOrder.setEta(etaStr);

					} else {

						purchaseOrder.setEta("");
					}

					purchaseOrderList.add(purchaseOrder);
					purchaseOrder = new PurchaseOrdersModel();
				}
			}

		}
		return purchaseOrderList;
	}

	public List<PurchaseOrdersModel> readExcelGeneralTemplate(File file, String buyerId, String crmId)
			throws EncryptedDocumentException, InvalidFormatException, IOException {

		List<PurchaseOrdersModel> purchaseOrderList = new ArrayList<PurchaseOrdersModel>();

		PurchaseOrdersModel purchaseOrder = new PurchaseOrdersModel();

		int po_number_col = 0;
		int shipper_col = 11;
		int total_gw_col = 1;
		int carton_length_col = 2;
		int carton_height_col = 4;
		int carton_width_col = 3;
		int total_pieces_col = 8;
		int carton_quantity_col = 5;
		int carton_unit_col = 6;
		int total_cbm_col = 7;
		int hs_code_col = 9;
		int commodity_col = 10;

		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(file);

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();

		while (rowIterator.hasNext()) {

			Row row = rowIterator.next();
			System.out.println("Iterating Row");
			if (row.getRowNum() != 0) {

				purchaseOrder.setBuyer_id(buyerId);
				purchaseOrder.setEntry_date(new Date());
				purchaseOrder.setEntry_by(crmId);

				// System.out.println("Iterating Column");

				Cell poNumberCell = row.getCell(po_number_col);
				String poNumberCheck = dataFormatter.formatCellValue(poNumberCell);

				// if there is no po number/order number that row will not be iterated
				if (!poNumberCheck.equals("")) {

					if (poNumberCell != null) {

						String poNumber = dataFormatter.formatCellValue(poNumberCell);
						purchaseOrder.setPo_no(poNumber);

					} else {

						purchaseOrder.setPo_no("");
					}

					Cell shipperCell = row.getCell(shipper_col);
					if (shipperCell != null) {

						String shipperStr = dataFormatter.formatCellValue(shipperCell);
						try {

							int numberConversion = Integer.parseInt(shipperStr);
							purchaseOrder.setShipper(shipperStr);

						} catch (NumberFormatException ex) {

							purchaseOrder.setShipper("");
						}
						// purchaseOrder.setShipper(shipperStr);

					} else {

						purchaseOrder.setShipper("");
					}

					Cell totalGwCell = row.getCell(total_gw_col);
					if (totalGwCell != null) {

						String totalGw = dataFormatter.formatCellValue(totalGwCell);
						purchaseOrder.setTotal_gw(totalGw);

					} else {

						purchaseOrder.setTotal_gw("");
					}

					Cell cartonLengthCell = row.getCell(carton_length_col);
					if (cartonLengthCell != null) {

						String cartonLength = dataFormatter.formatCellValue(cartonLengthCell);
						purchaseOrder.setCarton_length(cartonLength);

					} else {

						purchaseOrder.setCarton_length("");
					}

					Cell cartonHeightCell = row.getCell(carton_height_col);
					if (cartonHeightCell != null) {

						String cartonHeight = dataFormatter.formatCellValue(cartonHeightCell);
						purchaseOrder.setCarton_height(cartonHeight);

					} else {

						purchaseOrder.setCarton_height("");
					}

					Cell cartonWidthCell = row.getCell(carton_width_col);
					if (cartonWidthCell != null) {

						String cartonWidth = dataFormatter.formatCellValue(cartonWidthCell);
						purchaseOrder.setCarton_width(cartonWidth);

					} else {
						purchaseOrder.setCarton_width("");
					}

					Cell totalPiecesCell = row.getCell(total_pieces_col);
					if (totalPiecesCell != null) {

						String totalPieces = dataFormatter.formatCellValue(totalPiecesCell);
						purchaseOrder.setTotal_pieces(totalPieces);

					} else {

						purchaseOrder.setTotal_pieces("");
					}

					Cell cartonQuantityCell = row.getCell(carton_quantity_col);
					if (cartonQuantityCell != null) {

						String cartonQuantity = dataFormatter.formatCellValue(cartonQuantityCell);
						purchaseOrder.setCarton_quantity(cartonQuantity);

					} else {

						purchaseOrder.setCarton_quantity("");
					}

					Cell cartonUnitCell = row.getCell(carton_unit_col);
					if (cartonUnitCell != null) {

						String cartonUnit = dataFormatter.formatCellValue(cartonUnitCell);
						purchaseOrder.setCarton_unit(cartonUnit);

					} else {

						purchaseOrder.setCarton_unit("");
					}

					Cell totalCbmCell = row.getCell(total_cbm_col);
					if (totalCbmCell != null) {

						String totalCbm = dataFormatter.formatCellValue(totalCbmCell);
						purchaseOrder.setTotal_cbm(totalCbm);

					} else {

						purchaseOrder.setTotal_cbm("");
					}

					Cell hsCodeCell = row.getCell(hs_code_col);
					if (hsCodeCell != null) {

						String hsCode = dataFormatter.formatCellValue(hsCodeCell);
						purchaseOrder.setHs_code(hsCode);

					} else {

						purchaseOrder.setHs_code("");
					}

					Cell commodityCell = row.getCell(commodity_col);
					if (commodityCell != null) {

						String commodityStr = dataFormatter.formatCellValue(commodityCell);
						purchaseOrder.setCommodity(commodityStr);

					} else {
						purchaseOrder.setCommodity("");
					}

					purchaseOrder.setStyle_no("");
					purchaseOrder.setWh_code("");
					purchaseOrder.setSize_no("");
					purchaseOrder.setSku("");
					purchaseOrder.setArticle_no("");
					purchaseOrder.setColor("");
					purchaseOrder.setTotal_nw("");
					purchaseOrder.setComm_inv("");
					purchaseOrder.setComm_inv_date("");
					purchaseOrder.setPieces_ctn("");
					purchaseOrder.setCarton_sr_no("");
					purchaseOrder.setReference_1("");
					purchaseOrder.setReference_2("");
					purchaseOrder.setReference_3("");
					purchaseOrder.setTerms_of_shipment("");
					purchaseOrder.setFreight_mode("");
					purchaseOrder.setDivision("");
					purchaseOrder.setPol("");
					purchaseOrder.setPod("");
					purchaseOrder.setPlace_of_delivery("");
					purchaseOrder.setPlace_of_delivery_address("");
					purchaseOrder.setCargo_handover_date("");
					purchaseOrderList.add(purchaseOrder);
					purchaseOrder = new PurchaseOrdersModel();
				}
			}

		}
		return purchaseOrderList;
	}
}
