package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class Fournisseur {

	private String cod_trs_exp;
	private String lib_trs1;
	private String lib_trs2;
	private String adr1;
	private String adr2;
	private String adr3;
	private String cod_pay;
	private String cod_ean_fou;
	
	public String getCod_trs_exp() {
		return cod_trs_exp;
	}
	public void setCod_trs_exp(String cod_trs_exp) {
		this.cod_trs_exp = cod_trs_exp;
	}
	public String getLib_trs1() {
		return lib_trs1;
	}
	public void setLib_trs1(String lib_trs1) {
		this.lib_trs1 = lib_trs1;
	}
	public String getLib_trs2() {
		return lib_trs2;
	}
	public void setLib_trs2(String lib_trs2) {
		this.lib_trs2 = lib_trs2;
	}
	public String getAdr1() {
		return adr1;
	}
	public void setAdr1(String adr1) {
		this.adr1 = adr1;
	}
	public String getAdr2() {
		return adr2;
	}
	public void setAdr2(String adr2) {
		this.adr2 = adr2;
	}
	public String getAdr3() {
		return adr3;
	}
	public void setAdr3(String adr3) {
		this.adr3 = adr3;
	}
	public String getCod_pay() {
		return cod_pay;
	}
	public void setCod_pay(String cod_pay) {
		this.cod_pay = cod_pay;
	}
	public String getCod_ean_fou() {
		return cod_ean_fou;
	}
	public void setCod_ean_fou(String cod_ean_fou) {
		this.cod_ean_fou = cod_ean_fou;
	}
	
	
}
