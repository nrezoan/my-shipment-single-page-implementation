package com.myshipment.xmlpobean;

import java.util.ArrayList;
import java.util.List;

/*
 * @Ranjeet Kumar
 */
public class Produit {

	private String cod_pro;
	private String cod_pro_pro;
	private String lib_pro;
	private String cod_emb;
	private String pcb;
	private String sou_pcb;
	private String cod_pay;
	private String cod_ray;
	private String mnt_ach;
	private String tau_ach;
	private String mnt_ces;
	private String mnt_vte;
	private String cod_dev_vte;
	private String tau_vte;
	private String mnt_vte_soc;
	private String col_lon;
	private String col_lar;
	private String col_hau;
	private String col_vol;
	private String col_pds;
	private String ref_fou;
	private String pct_dou;
	private String typ_pro;
	private String lib_pro_lng;
	private String dan_cla;
	private String dan_onu;
	private String cod_pal;
	private String pal_lon;
	private String pal_lar;
	private String pal_hau;
	private String pal_vol;
	private String pal_pds;
	private String col_cou;
	private String cou_pal;
	private String col_pal;
	private String cod_unl_ach;
	
	private List<LibLangue> libLangueLst = new ArrayList<LibLangue>();
	private List<Ean> eanLst = new ArrayList<Ean>();
	private List<RefCli2> refCli2Lst = new ArrayList<RefCli2>();
	
	public String getCod_pro() {
		return cod_pro;
	}
	public void setCod_pro(String cod_pro) {
		this.cod_pro = cod_pro;
	}
	public String getCod_pro_pro() {
		return cod_pro_pro;
	}
	public void setCod_pro_pro(String cod_pro_pro) {
		this.cod_pro_pro = cod_pro_pro;
	}
	public String getLib_pro() {
		return lib_pro;
	}
	public void setLib_pro(String lib_pro) {
		this.lib_pro = lib_pro;
	}
	public String getCod_emb() {
		return cod_emb;
	}
	public void setCod_emb(String cod_emb) {
		this.cod_emb = cod_emb;
	}
	public String getPcb() {
		return pcb;
	}
	public void setPcb(String pcb) {
		this.pcb = pcb;
	}
	public String getSou_pcb() {
		return sou_pcb;
	}
	public void setSou_pcb(String sou_pcb) {
		this.sou_pcb = sou_pcb;
	}
	public String getCod_pay() {
		return cod_pay;
	}
	public void setCod_pay(String cod_pay) {
		this.cod_pay = cod_pay;
	}
	public String getCod_ray() {
		return cod_ray;
	}
	public void setCod_ray(String cod_ray) {
		this.cod_ray = cod_ray;
	}
	public String getMnt_ach() {
		return mnt_ach;
	}
	public void setMnt_ach(String mnt_ach) {
		this.mnt_ach = mnt_ach;
	}
	public String getTau_ach() {
		return tau_ach;
	}
	public void setTau_ach(String tau_ach) {
		this.tau_ach = tau_ach;
	}
	public String getMnt_ces() {
		return mnt_ces;
	}
	public void setMnt_ces(String mnt_ces) {
		this.mnt_ces = mnt_ces;
	}
	public String getMnt_vte() {
		return mnt_vte;
	}
	public void setMnt_vte(String mnt_vte) {
		this.mnt_vte = mnt_vte;
	}
	public String getCod_dev_vte() {
		return cod_dev_vte;
	}
	public void setCod_dev_vte(String cod_dev_vte) {
		this.cod_dev_vte = cod_dev_vte;
	}
	public String getTau_vte() {
		return tau_vte;
	}
	public void setTau_vte(String tau_vte) {
		this.tau_vte = tau_vte;
	}
	public String getMnt_vte_soc() {
		return mnt_vte_soc;
	}
	public void setMnt_vte_soc(String mnt_vte_soc) {
		this.mnt_vte_soc = mnt_vte_soc;
	}
	public String getCol_lon() {
		return col_lon;
	}
	public void setCol_lon(String col_lon) {
		this.col_lon = col_lon;
	}
	public String getCol_lar() {
		return col_lar;
	}
	public void setCol_lar(String col_lar) {
		this.col_lar = col_lar;
	}
	public String getCol_hau() {
		return col_hau;
	}
	public void setCol_hau(String col_hau) {
		this.col_hau = col_hau;
	}
	public String getCol_vol() {
		return col_vol;
	}
	public void setCol_vol(String col_vol) {
		this.col_vol = col_vol;
	}
	public String getCol_pds() {
		return col_pds;
	}
	public void setCol_pds(String col_pds) {
		this.col_pds = col_pds;
	}
	public String getRef_fou() {
		return ref_fou;
	}
	public void setRef_fou(String ref_fou) {
		this.ref_fou = ref_fou;
	}
	public String getPct_dou() {
		return pct_dou;
	}
	public void setPct_dou(String pct_dou) {
		this.pct_dou = pct_dou;
	}
	public String getTyp_pro() {
		return typ_pro;
	}
	public void setTyp_pro(String typ_pro) {
		this.typ_pro = typ_pro;
	}
	public String getLib_pro_lng() {
		return lib_pro_lng;
	}
	public void setLib_pro_lng(String lib_pro_lng) {
		this.lib_pro_lng = lib_pro_lng;
	}
	public String getDan_cla() {
		return dan_cla;
	}
	public void setDan_cla(String dan_cla) {
		this.dan_cla = dan_cla;
	}
	public String getDan_onu() {
		return dan_onu;
	}
	public void setDan_onu(String dan_onu) {
		this.dan_onu = dan_onu;
	}
	public String getCod_pal() {
		return cod_pal;
	}
	public void setCod_pal(String cod_pal) {
		this.cod_pal = cod_pal;
	}
	public String getPal_lon() {
		return pal_lon;
	}
	public void setPal_lon(String pal_lon) {
		this.pal_lon = pal_lon;
	}
	public String getPal_lar() {
		return pal_lar;
	}
	public void setPal_lar(String pal_lar) {
		this.pal_lar = pal_lar;
	}
	public String getPal_hau() {
		return pal_hau;
	}
	public void setPal_hau(String pal_hau) {
		this.pal_hau = pal_hau;
	}
	public String getPal_vol() {
		return pal_vol;
	}
	public void setPal_vol(String pal_vol) {
		this.pal_vol = pal_vol;
	}
	public String getPal_pds() {
		return pal_pds;
	}
	public void setPal_pds(String pal_pds) {
		this.pal_pds = pal_pds;
	}
	public String getCol_cou() {
		return col_cou;
	}
	public void setCol_cou(String col_cou) {
		this.col_cou = col_cou;
	}
	public String getCou_pal() {
		return cou_pal;
	}
	public void setCou_pal(String cou_pal) {
		this.cou_pal = cou_pal;
	}
	public String getCol_pal() {
		return col_pal;
	}
	public void setCol_pal(String col_pal) {
		this.col_pal = col_pal;
	}
	public String getCod_unl_ach() {
		return cod_unl_ach;
	}
	public void setCod_unl_ach(String cod_unl_ach) {
		this.cod_unl_ach = cod_unl_ach;
	}
	public List<LibLangue> getLibLangueLst() {
		return libLangueLst;
	}
	public void setLibLangueLst(List<LibLangue> libLangueLst) {
		this.libLangueLst = libLangueLst;
	}
	public List<Ean> getEanLst() {
		return eanLst;
	}
	public void setEanLst(List<Ean> eanLst) {
		this.eanLst = eanLst;
	}
	public List<RefCli2> getRefCli2Lst() {
		return refCli2Lst;
	}
	public void setRefCli2Lst(List<RefCli2> refCli2Lst) {
		this.refCli2Lst = refCli2Lst;
	}

	
	
}
