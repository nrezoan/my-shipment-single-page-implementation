package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class Ean {

	private String cod_cou;
	private String cod_tai;
	private String cod_ean;
	private String cod_ean_mas;
	private String cod_ean_inn;
	
	public String getCod_cou() {
		return cod_cou;
	}
	public void setCod_cou(String cod_cou) {
		this.cod_cou = cod_cou;
	}
	public String getCod_tai() {
		return cod_tai;
	}
	public void setCod_tai(String cod_tai) {
		this.cod_tai = cod_tai;
	}
	public String getCod_ean() {
		return cod_ean;
	}
	public void setCod_ean(String cod_ean) {
		this.cod_ean = cod_ean;
	}
	public String getCod_ean_mas() {
		return cod_ean_mas;
	}
	public void setCod_ean_mas(String cod_ean_mas) {
		this.cod_ean_mas = cod_ean_mas;
	}
	public String getCod_ean_inn() {
		return cod_ean_inn;
	}
	public void setCod_ean_inn(String cod_ean_inn) {
		this.cod_ean_inn = cod_ean_inn;
	}
	
	
}
