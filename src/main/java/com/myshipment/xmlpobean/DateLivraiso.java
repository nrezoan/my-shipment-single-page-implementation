package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class DateLivraiso {

	private String cod_pro_date_livraiso;
	private String num_tra_date_livraiso;
	private String nbr_col_date_livraiso;
	private String qte;
	private String dat_etd_lim;
	private String 	dat_ent_cal;
	private String dat_ent_sou;
	private String dat_mag_cal;
	private String dat_mag_sou;
	//private String Liste_catalogue;
	
	
	public String getCod_pro_date_livraiso() {
		return cod_pro_date_livraiso;
	}
	public void setCod_pro_date_livraiso(String cod_pro_date_livraiso) {
		this.cod_pro_date_livraiso = cod_pro_date_livraiso;
	}
	public String getNum_tra_date_livraiso() {
		return num_tra_date_livraiso;
	}
	public void setNum_tra_date_livraiso(String num_tra_date_livraiso) {
		this.num_tra_date_livraiso = num_tra_date_livraiso;
	}
	public String getNbr_col_date_livraiso() {
		return nbr_col_date_livraiso;
	}
	public void setNbr_col_date_livraiso(String nbr_col_date_livraiso) {
		this.nbr_col_date_livraiso = nbr_col_date_livraiso;
	}
	public String getQte() {
		return qte;
	}
	public void setQte(String qte) {
		this.qte = qte;
	}
	public String getDat_etd_lim() {
		return dat_etd_lim;
	}
	public void setDat_etd_lim(String dat_etd_lim) {
		this.dat_etd_lim = dat_etd_lim;
	}
	public String getDat_ent_cal() {
		return dat_ent_cal;
	}
	public void setDat_ent_cal(String dat_ent_cal) {
		this.dat_ent_cal = dat_ent_cal;
	}
	public String getDat_ent_sou() {
		return dat_ent_sou;
	}
	public void setDat_ent_sou(String dat_ent_sou) {
		this.dat_ent_sou = dat_ent_sou;
	}
	public String getDat_mag_cal() {
		return dat_mag_cal;
	}
	public void setDat_mag_cal(String dat_mag_cal) {
		this.dat_mag_cal = dat_mag_cal;
	}
	public String getDat_mag_sou() {
		return dat_mag_sou;
	}
	public void setDat_mag_sou(String dat_mag_sou) {
		this.dat_mag_sou = dat_mag_sou;
	}
	
	
}
