package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class Documents {

	private String cod_pro_documents;
	private String cod_ndp;
	private String cod_amf;
	private String cod_doc;
	private String nbr_ori;
	private String nbr_cop;
	public String getCod_pro_documents() {
		return cod_pro_documents;
	}
	public void setCod_pro_documents(String cod_pro_documents) {
		this.cod_pro_documents = cod_pro_documents;
	}
	public String getCod_ndp() {
		return cod_ndp;
	}
	public void setCod_ndp(String cod_ndp) {
		this.cod_ndp = cod_ndp;
	}
	public String getCod_amf() {
		return cod_amf;
	}
	public void setCod_amf(String cod_amf) {
		this.cod_amf = cod_amf;
	}
	public String getCod_doc() {
		return cod_doc;
	}
	public void setCod_doc(String cod_doc) {
		this.cod_doc = cod_doc;
	}
	public String getNbr_ori() {
		return nbr_ori;
	}
	public void setNbr_ori(String nbr_ori) {
		this.nbr_ori = nbr_ori;
	}
	public String getNbr_cop() {
		return nbr_cop;
	}
	public void setNbr_cop(String nbr_cop) {
		this.nbr_cop = nbr_cop;
	}
	
	
}
