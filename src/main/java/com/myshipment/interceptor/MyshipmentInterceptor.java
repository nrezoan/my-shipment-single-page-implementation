package com.myshipment.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.myshipment.util.SessionUtil;

public class MyshipmentInterceptor implements HandlerInterceptor{
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		System.out.println("Pre-handle");
		System.out.println("Request URL: "+request.getRequestURI());
		if(request.getRequestURI().equals("/MyShipmentFrontApp/")||request.getRequestURI().equals("/")||request.getRequestURI().equals("/MyShipment/")||request.getRequestURI().startsWith("/MyShipmentFrontApp/resources/")||request.getRequestURI().startsWith("/MyShipmentFrontApp/login")||request.getRequestURI().startsWith("/MyShipmentFrontApp/getCommonTrackingDetailInfo")||request.getRequestURI().startsWith("/getCommonTrackingDetailInfo")){
			return true;
		}
		if(SessionUtil.getHttpSession(request).getAttribute(SessionUtil.LOGIN_DETAILS)==null){
			System.out.println("URI: "+request.getRequestURI());
				response.sendRedirect(request.getContextPath()+"/");
				System.out.println("come into login null");
				
			//return false;
		}
		return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		System.out.println("Post-handle");
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		System.out.println("After completion handle");
	}

}
