package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class TrackingParams {

	private String trackingNo;

	public String getTrackingNo() {
		return trackingNo;
	}

	public void setTrackingNo(String trackingNo) {
		this.trackingNo = trackingNo;
	}
	
	
}
