package com.myshipment.model;

public class BuyerWiseShipmentJson {
	private String buyerNo;
	private String buyerName;
	private double totalShipment;
	private double totalShipmentPerc;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public double getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(double totalShipment) {
		this.totalShipment = totalShipment;
	}
	public double getTotalShipmentPerc() {
		return totalShipmentPerc;
	}
	public void setTotalShipmentPerc(double totalShipmentPerc) {
		this.totalShipmentPerc = totalShipmentPerc;
	}
	

}
