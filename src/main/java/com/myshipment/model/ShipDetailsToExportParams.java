package com.myshipment.model;

public class ShipDetailsToExportParams {
	private String buyerNo;
	private String shipperNo;
	private String bookingDateFrom;
	private String bookingDateTo;
	private String salesOrg;
	private String division;
	private String distChannel;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getBookingDateFrom() {
		return bookingDateFrom;
	}
	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}
	public String getBookingDateTo() {
		return bookingDateTo;
	}
	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	@Override
	public String toString() {
		return "ShipDetailsToExportParams [buyerNo=" + buyerNo + ", shipperNo=" + shipperNo + ", bookingDateFrom="
				+ bookingDateFrom + ", bookingDateTo=" + bookingDateTo + ", salesOrg=" + salesOrg + ", division="
				+ division + ", distChannel=" + distChannel + "]";
	}
	

}
