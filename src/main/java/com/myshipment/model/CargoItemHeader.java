package com.myshipment.model;


import java.math.BigDecimal;
import java.util.Date;


public class CargoItemHeader {

		
	private String vbEln;	
	private String KunNr;
	private String Name1;
	private String zzHblHawbNo;
	private String zzCommInvNo;
	private Date zzCustApovDt;
	private BigDecimal zzSoQuantity;
	private String vkOrg;
	private Date buDat;
	public String getVbEln() {
		return vbEln;
	}
	public void setVbEln(String vbEln) {
		this.vbEln = vbEln;
	}
	public String getKunNr() {
		return KunNr;
	}
	public void setKunNr(String kunNr) {
		KunNr = kunNr;
	}
	public String getName1() {
		return Name1;
	}
	public void setName1(String name1) {
		Name1 = name1;
	}
	public String getZzHblHawbNo() {
		return zzHblHawbNo;
	}
	public void setZzHblHawbNo(String zzHblHawbNo) {
		this.zzHblHawbNo = zzHblHawbNo;
	}
	public String getZzCommInvNo() {
		return zzCommInvNo;
	}
	public void setZzCommInvNo(String zzCommInvNo) {
		this.zzCommInvNo = zzCommInvNo;
	}
	public Date getZzCustApovDt() {
		return zzCustApovDt;
	}
	public void setZzCustApovDt(Date zzCustApovDt) {
		this.zzCustApovDt = zzCustApovDt;
	}
	public BigDecimal getZzSoQuantity() {
		return zzSoQuantity;
	}
	public void setZzSoQuantity(BigDecimal zzSoQuantity) {
		this.zzSoQuantity = zzSoQuantity;
	}
	public String getVkOrg() {
		return vkOrg;
	}
	public void setVkOrg(String vkOrg) {
		this.vkOrg = vkOrg;
	}
	public Date getBuDat() {
		return buDat;
	}
	public void setBuDat(Date buDat) {
		this.buDat = buDat;
	}
	

	
	
}
