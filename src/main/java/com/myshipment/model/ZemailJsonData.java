package com.myshipment.model;

import java.util.List;



public class ZemailJsonData {
	
	private BapiRet bapiReturn;
	private List<ZbapiPacListH> headeritem; 
	private List<ZbapiPacListI> lineItem; 
	private List<zmail> emailAddress;
	public BapiRet getBapiReturn() {
		return bapiReturn;
	}
	public void setBapiReturn(BapiRet bapiReturn) {
		this.bapiReturn = bapiReturn;
	}
	public List<ZbapiPacListH> getHeaderitem() {
		return headeritem;
	}
	public void setHeaderitem(List<ZbapiPacListH> headeritem) {
		this.headeritem = headeritem;
	}
	public List<ZbapiPacListI> getLineItem() {
		return lineItem;
	}
	public void setLineItem(List<ZbapiPacListI> lineItem) {
		this.lineItem = lineItem;
	}
	public List<zmail> getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(List<zmail> emailAddress) {
		this.emailAddress = emailAddress;
	} 
	
	
}
