package com.myshipment.model;

public class CommercialInvoiceJasper {
	
	private String vendorAdd1;
	private String vendorAdd2;
	private String vendorAdd3;
	private String vendorAdd4;
	private String vendorCity;
	private String vendorCountry;
	private String makerAdd1;
	private String makerAdd2;
	private String makerAdd3;
	private String makerAdd4;
	private String makerCity;
	private String makerCountry;
	private String buyerAdd1;
	private String buyerAdd2;
	private String buyerAdd3;
	private String buyerAdd4;
	private String buyerCity;
	private String buyerCountry;
	private String consigneeAdd1;
	private String consigneeAdd2;
	private String consigneeAdd3;
	private String consigneeAdd4;
	private String consigneeCity;
	private String consigneeCountry;
	private String invoiceNo;
	private String invoiceDt;
	private String so;
	private String vendorRef;
	private String hbl;
	private String countryOfOrg;
	private String countryOfDest;
	private String incoterm;
	private String placeOfReceipt;
	private String lcNo;
	private String  lcDt;
	private String  lcExpDt;
	private String issuingBank;
	private String invoiceCurrency;
	private String orderNo;
	private String itemRef;
	private String itemDesc;
	private String hsCode;
	private Integer qty;
	private String unitPrice;
	private Double amount;
	private String bankAdd1;
	private String bankAdd2;
	private String bankAdd3;
	private String bankCity;
	private String bankCountry;
	private String bankCode;
	private String expNo;
	private String  expDt;
	private String accNumber;
	private String swiftTransfer;
	private String interBank;
	private String remarks;
	public String getVendorAdd1() {
		return vendorAdd1;
	}
	public void setVendorAdd1(String vendorAdd1) {
		this.vendorAdd1 = vendorAdd1;
	}
	public String getVendorAdd2() {
		return vendorAdd2;
	}
	public void setVendorAdd2(String vendorAdd2) {
		this.vendorAdd2 = vendorAdd2;
	}
	public String getVendorAdd3() {
		return vendorAdd3;
	}
	public void setVendorAdd3(String vendorAdd3) {
		this.vendorAdd3 = vendorAdd3;
	}
	public String getVendorAdd4() {
		return vendorAdd4;
	}
	public void setVendorAdd4(String vendorAdd4) {
		this.vendorAdd4 = vendorAdd4;
	}
	public String getVendorCity() {
		return vendorCity;
	}
	public void setVendorCity(String vendorCity) {
		this.vendorCity = vendorCity;
	}
	public String getVendorCountry() {
		return vendorCountry;
	}
	public void setVendorCountry(String vendorCountry) {
		this.vendorCountry = vendorCountry;
	}
	public String getMakerAdd1() {
		return makerAdd1;
	}
	public void setMakerAdd1(String makerAdd1) {
		this.makerAdd1 = makerAdd1;
	}
	public String getMakerAdd2() {
		return makerAdd2;
	}
	public void setMakerAdd2(String makerAdd2) {
		this.makerAdd2 = makerAdd2;
	}
	public String getMakerAdd3() {
		return makerAdd3;
	}
	public void setMakerAdd3(String makerAdd3) {
		this.makerAdd3 = makerAdd3;
	}
	public String getMakerAdd4() {
		return makerAdd4;
	}
	public void setMakerAdd4(String makerAdd4) {
		this.makerAdd4 = makerAdd4;
	}
	public String getMakerCity() {
		return makerCity;
	}
	public void setMakerCity(String makerCity) {
		this.makerCity = makerCity;
	}
	public String getMakerCountry() {
		return makerCountry;
	}
	public void setMakerCountry(String makerCountry) {
		this.makerCountry = makerCountry;
	}
	public String getBuyerAdd1() {
		return buyerAdd1;
	}
	public void setBuyerAdd1(String buyerAdd1) {
		this.buyerAdd1 = buyerAdd1;
	}
	public String getBuyerAdd2() {
		return buyerAdd2;
	}
	public void setBuyerAdd2(String buyerAdd2) {
		this.buyerAdd2 = buyerAdd2;
	}
	public String getBuyerAdd3() {
		return buyerAdd3;
	}
	public void setBuyerAdd3(String buyerAdd3) {
		this.buyerAdd3 = buyerAdd3;
	}
	public String getBuyerAdd4() {
		return buyerAdd4;
	}
	public void setBuyerAdd4(String buyerAdd4) {
		this.buyerAdd4 = buyerAdd4;
	}
	public String getBuyerCity() {
		return buyerCity;
	}
	public void setBuyerCity(String buyerCity) {
		this.buyerCity = buyerCity;
	}
	public String getBuyerCountry() {
		return buyerCountry;
	}
	public void setBuyerCountry(String buyerCountry) {
		this.buyerCountry = buyerCountry;
	}
	public String getConsigneeAdd1() {
		return consigneeAdd1;
	}
	public void setConsigneeAdd1(String consigneeAdd1) {
		this.consigneeAdd1 = consigneeAdd1;
	}
	public String getConsigneeAdd2() {
		return consigneeAdd2;
	}
	public void setConsigneeAdd2(String consigneeAdd2) {
		this.consigneeAdd2 = consigneeAdd2;
	}
	public String getConsigneeAdd3() {
		return consigneeAdd3;
	}
	public void setConsigneeAdd3(String consigneeAdd3) {
		this.consigneeAdd3 = consigneeAdd3;
	}
	public String getConsigneeAdd4() {
		return consigneeAdd4;
	}
	public void setConsigneeAdd4(String consigneeAdd4) {
		this.consigneeAdd4 = consigneeAdd4;
	}
	public String getConsigneeCity() {
		return consigneeCity;
	}
	public void setConsigneeCity(String consigneeCity) {
		this.consigneeCity = consigneeCity;
	}
	public String getConsigneeCountry() {
		return consigneeCountry;
	}
	public void setConsigneeCountry(String consigneeCountry) {
		this.consigneeCountry = consigneeCountry;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInvoiceDt() {
		return invoiceDt;
	}
	public void setInvoiceDt(String invoiceDt) {
		this.invoiceDt = invoiceDt;
	}
	public String getSo() {
		return so;
	}
	public void setSo(String so) {
		this.so = so;
	}
	public String getVendorRef() {
		return vendorRef;
	}
	public void setVendorRef(String vendorRef) {
		this.vendorRef = vendorRef;
	}
	public String getHbl() {
		return hbl;
	}
	public void setHbl(String hbl) {
		this.hbl = hbl;
	}
	public String getCountryOfOrg() {
		return countryOfOrg;
	}
	public void setCountryOfOrg(String countryOfOrg) {
		this.countryOfOrg = countryOfOrg;
	}
	public String getCountryOfDest() {
		return countryOfDest;
	}
	public void setCountryOfDest(String countryOfDest) {
		this.countryOfDest = countryOfDest;
	}
	public String getIncoterm() {
		return incoterm;
	}
	public void setIncoterm(String incoterm) {
		this.incoterm = incoterm;
	}
	public String getPlaceOfReceipt() {
		return placeOfReceipt;
	}
	public void setPlaceOfReceipt(String placeOfReceipt) {
		this.placeOfReceipt = placeOfReceipt;
	}
	public String getLcNo() {
		return lcNo;
	}
	public void setLcNo(String lcNo) {
		this.lcNo = lcNo;
	}
	public String getLcDt() {
		return lcDt;
	}
	public void setLcDt(String lcDt) {
		this.lcDt = lcDt;
	}
	public String getLcExpDt() {
		return lcExpDt;
	}
	public void setLcExpDt(String lcExpDt) {
		this.lcExpDt = lcExpDt;
	}
	public String getIssuingBank() {
		return issuingBank;
	}
	public void setIssuingBank(String issuingBank) {
		this.issuingBank = issuingBank;
	}
	public String getInvoiceCurrency() {
		return invoiceCurrency;
	}
	public void setInvoiceCurrency(String invoiceCurrency) {
		this.invoiceCurrency = invoiceCurrency;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getItemRef() {
		return itemRef;
	}
	public void setItemRef(String itemRef) {
		this.itemRef = itemRef;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getHsCode() {
		return hsCode;
	}
	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public String getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getBankAdd1() {
		return bankAdd1;
	}
	public void setBankAdd1(String bankAdd1) {
		this.bankAdd1 = bankAdd1;
	}
	public String getBankAdd2() {
		return bankAdd2;
	}
	public void setBankAdd2(String bankAdd2) {
		this.bankAdd2 = bankAdd2;
	}
	public String getBankAdd3() {
		return bankAdd3;
	}
	public void setBankAdd3(String bankAdd3) {
		this.bankAdd3 = bankAdd3;
	}
	public String getBankCity() {
		return bankCity;
	}
	public void setBankCity(String bankCity) {
		this.bankCity = bankCity;
	}
	public String getBankCountry() {
		return bankCountry;
	}
	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getExpNo() {
		return expNo;
	}
	public void setExpNo(String expNo) {
		this.expNo = expNo;
	}
	public String getExpDt() {
		return expDt;
	}
	public void setExpDt(String expDt) {
		this.expDt = expDt;
	}
	public String getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
	public String getSwiftTransfer() {
		return swiftTransfer;
	}
	public void setSwiftTransfer(String swiftTransfer) {
		this.swiftTransfer = swiftTransfer;
	}
	public String getInterBank() {
		return interBank;
	}
	public void setInterBank(String interBank) {
		this.interBank = interBank;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	
	
	
	
	
	

}
