package com.myshipment.model;

public class SOWiseShipDetailsBuyerJson {
	public String getBlNo() {
		return blNo;
	}
	public void setBlNo(String blNo) {
		this.blNo = blNo;
	}
	public String getBldate() {
		return bldate;
	}
	public void setBldate(String bldate) {
		this.bldate = bldate;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getBuyerName() {
		return BuyerName;
	}
	public void setBuyerName(String buyerName) {
		BuyerName = buyerName;
	}
	public String getPolCode() {
		return polCode;
	}
	public void setPolCode(String polCode) {
		this.polCode = polCode;
	}
	public String getPodCode() {
		return podCode;
	}
	public void setPodCode(String podCode) {
		this.podCode = podCode;
	}
	public double getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public double getChargeWeight() {
		return chargeWeight;
	}
	public void setChargeWeight(double chargeWeight) {
		this.chargeWeight = chargeWeight;
	}
	public double getTotVolume() {
		return totVolume;
	}
	public void setTotVolume(double totVolume) {
		this.totVolume = totVolume;
	}
	public String getGrDate() {
		return grDate;
	}
	public void setGrDate(String grDate) {
		this.grDate = grDate;
	}
	public String getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(String shipmentDate) {
		this.shipmentDate = shipmentDate;
	}
	private String blNo;
	private String bldate;
	private String bookingDate;
	private String buyerNo;
	private String BuyerName;
	private String polCode;
	private String podCode;
	private double grossWeight;
	private double chargeWeight;
	private double totVolume;
	private String grDate;
	private String shipmentDate;

}
