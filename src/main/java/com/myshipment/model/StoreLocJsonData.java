package com.myshipment.model;

import java.util.List;



public class StoreLocJsonData {

	
	
	private List<StoreLocListBean> porList;

	public List<StoreLocListBean> getPorList() {
		return porList;
	}

	public void setPorList(List<StoreLocListBean> porList) {
		this.porList = porList;
	}

	@Override
	public String toString() {
		return "StoreLocJsonData [porList=" + porList + "]";
	}

	
	
}
