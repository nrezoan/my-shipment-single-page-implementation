package com.myshipment.model;

public class PackingListOrderHeader {

	private String countryOfOrigin;
	private String countryOfDestination;
	private String incoterm;
	private String modeOfShipment;
	private String placeOfReciept;
	private String portOfLoading;
	private String portOfDischarge;
	private String finalDestination;
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}
	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	public String getCountryOfDestination() {
		return countryOfDestination;
	}
	public void setCountryOfDestination(String countryOfDestination) {
		this.countryOfDestination = countryOfDestination;
	}
	public String getIncoterm() {
		return incoterm;
	}
	public void setIncoterm(String incoterm) {
		this.incoterm = incoterm;
	}
	public String getModeOfShipment() {
		return modeOfShipment;
	}
	public void setModeOfShipment(String modeOfShipment) {
		this.modeOfShipment = modeOfShipment;
	}
	public String getPlaceOfReciept() {
		return placeOfReciept;
	}
	public void setPlaceOfReciept(String placeOfReciept) {
		this.placeOfReciept = placeOfReciept;
	}
	public String getPortOfLoading() {
		return portOfLoading;
	}
	public void setPortOfLoading(String portOfLoading) {
		this.portOfLoading = portOfLoading;
	}
	public String getPortOfDischarge() {
		return portOfDischarge;
	}
	public void setPortOfDischarge(String portOfDischarge) {
		this.portOfDischarge = portOfDischarge;
	}
	public String getFinalDestination() {
		return finalDestination;
	}
	public void setFinalDestination(String finalDestination) {
		this.finalDestination = finalDestination;
	}
	
}
