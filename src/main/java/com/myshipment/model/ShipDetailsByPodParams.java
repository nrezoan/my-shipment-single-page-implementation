package com.myshipment.model;

public class ShipDetailsByPodParams {
	private String pod;
	private DashboardShipperParams dashboardShipperParams;
	
	
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public DashboardShipperParams getDashboardShipperParams() {
		return dashboardShipperParams;
	}
	public void setDashboardShipperParams(DashboardShipperParams dashboardShipperParams) {
		this.dashboardShipperParams = dashboardShipperParams;
	}

}
