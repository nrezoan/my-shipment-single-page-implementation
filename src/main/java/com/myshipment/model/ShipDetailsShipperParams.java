package com.myshipment.model;

public class ShipDetailsShipperParams {
	private String buyerNo;
	private DashboardShipperParams dashboardShipperParams;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public DashboardShipperParams getDashboardShipperParams() {
		return dashboardShipperParams;
	}
	public void setDashboardShipperParams(DashboardShipperParams dashboardShipperParams) {
		this.dashboardShipperParams = dashboardShipperParams;
	}
	

}
