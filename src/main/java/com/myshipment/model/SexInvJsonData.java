package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;



public class SexInvJsonData {

	
	private String payerCountry;	
	private String payerNo;	
	private Adrc payer;	
	private String billNo;	
	private Date billDt;	
	private String soNo;		
	private String shipper;	
	private String consignee;	
	private String destAgent;	
	private String paymentMode;	
	private BigDecimal totalQty;	
	private String hbl;		
	private String mbl;	
	private String detin;	
	private String fVsl;	
	private String fVoyage;	
	private String mVsl;	
	private String mVoyage;		
	private Date eta;	
	private Date etd;	
	private String shippingLine;	
	private String words;	
	private String vkOrg;
	private String volume;
	private String sOrg;	
	private Adrc tFooter;	
	private String email;	
	private String code;	
	private BankDetail tBank;	
	private BigDecimal total;		
	private BigDecimal roundOff;	
	private String user;	
	private String currency;	
	private String headerFlag;	
	private String itemFlag;	
	private String companyCode;	
	private String outputName;		
	private BigDecimal grWt;	
	private BigDecimal chWt;	
	private String gvStaxNo;	
	private String gvPanNo;	
	private String gvErnam;	
	private BigDecimal vTax;	
	private BigDecimal gvTotal;	
	private BigDecimal gvNtTotal;	
	private String vAmt;	
	private String vAmount;	
	private String country;	
	private String vWords;	
	private String commInv;	
	private Date commDat;	
	private String gstNo;
	private String odn;
	private String gstn;
	private String plos;
	private List<TLines> tLineList;	
	private List<TZConSize> tzConSizeList;	
	private List<ZsdSexInvDetail> tConList;	
	private List<ZsdSexInvDetail> tHeaderList;	
	private List<ZsdSexInvDetail> tPerconList;	
	private List<ZsdSexInvDetailInd> tFHeaderList;	
	private List<ZsdSexInvDetailInd> tFPerConList;
	private List<ZsdSexInvDetailInd> tFinalConList;
	private List<TDocHeader> tDocHeader;
	
	public String getOdn() {
		return odn;
	}
	public void setOdn(String odn) {
		this.odn = odn;
	}
	public String getGstn() {
		return gstn;
	}
	public void setGstn(String gstn) {
		this.gstn = gstn;
	}
	public String getPlos() {
		return plos;
	}
	public void setPlos(String plos) {
		this.plos = plos;
	}
	public String getGstNo() {
		return gstNo;
	}
	public List<TDocHeader> gettDocHeader() {
		return tDocHeader;
	}
	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}
	public void settDocHeader(List<TDocHeader> tDocHeader) {
		this.tDocHeader = tDocHeader;
	}
	public String getPayerCountry() {
		return payerCountry;
	}
	public void setPayerCountry(String payerCountry) {
		this.payerCountry = payerCountry;
	}
	public String getPayerNo() {
		return payerNo;
	}
	public void setPayerNo(String payerNo) {
		this.payerNo = payerNo;
	}
	public Adrc getPayer() {
		return payer;
	}
	public void setPayer(Adrc payer) {
		this.payer = payer;
	}
	public String getBillNo() {
		return billNo;
	}
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	public Date getBillDt() {
		return billDt;
	}
	public void setBillDt(Date billDt) {
		this.billDt = billDt;
	}
	public String getSoNo() {
		return soNo;
	}
	public void setSoNo(String soNo) {
		this.soNo = soNo;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getConsignee() {
		return consignee;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	public String getDestAgent() {
		return destAgent;
	}
	public void setDestAgent(String destAgent) {
		this.destAgent = destAgent;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public BigDecimal getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}
	public String getHbl() {
		return hbl;
	}
	public void setHbl(String hbl) {
		this.hbl = hbl;
	}
	public String getMbl() {
		return mbl;
	}
	public void setMbl(String mbl) {
		this.mbl = mbl;
	}
	public String getDetin() {
		return detin;
	}
	public void setDetin(String detin) {
		this.detin = detin;
	}
	public String getfVsl() {
		return fVsl;
	}
	public void setfVsl(String fVsl) {
		this.fVsl = fVsl;
	}
	public String getfVoyage() {
		return fVoyage;
	}
	public void setfVoyage(String fVoyage) {
		this.fVoyage = fVoyage;
	}
	public String getmVsl() {
		return mVsl;
	}
	public void setmVsl(String mVsl) {
		this.mVsl = mVsl;
	}
	public String getmVoyage() {
		return mVoyage;
	}
	public void setmVoyage(String mVoyage) {
		this.mVoyage = mVoyage;
	}
	public Date getEta() {
		return eta;
	}
	public void setEta(Date eta) {
		this.eta = eta;
	}
	public Date getEtd() {
		return etd;
	}
	public void setEtd(Date etd) {
		this.etd = etd;
	}
	public String getShippingLine() {
		return shippingLine;
	}
	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}
	public String getWords() {
		return words;
	}
	public void setWords(String words) {
		this.words = words;
	}
	public String getVkOrg() {
		return vkOrg;
	}
	public void setVkOrg(String vkOrg) {
		this.vkOrg = vkOrg;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getsOrg() {
		return sOrg;
	}
	public void setsOrg(String sOrg) {
		this.sOrg = sOrg;
	}
	public Adrc gettFooter() {
		return tFooter;
	}
	public void settFooter(Adrc tFooter) {
		this.tFooter = tFooter;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BankDetail gettBank() {
		return tBank;
	}
	public void settBank(BankDetail tBank) {
		this.tBank = tBank;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public BigDecimal getRoundOff() {
		return roundOff;
	}
	public void setRoundOff(BigDecimal roundOff) {
		this.roundOff = roundOff;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getHeaderFlag() {
		return headerFlag;
	}
	public void setHeaderFlag(String headerFlag) {
		this.headerFlag = headerFlag;
	}
	public String getItemFlag() {
		return itemFlag;
	}
	public void setItemFlag(String itemFlag) {
		this.itemFlag = itemFlag;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getOutputName() {
		return outputName;
	}
	public void setOutputName(String outputName) {
		this.outputName = outputName;
	}
	public BigDecimal getGrWt() {
		return grWt;
	}
	public void setGrWt(BigDecimal grWt) {
		this.grWt = grWt;
	}
	public BigDecimal getChWt() {
		return chWt;
	}
	public void setChWt(BigDecimal chWt) {
		this.chWt = chWt;
	}
	public String getGvStaxNo() {
		return gvStaxNo;
	}
	public void setGvStaxNo(String gvStaxNo) {
		this.gvStaxNo = gvStaxNo;
	}
	public String getGvPanNo() {
		return gvPanNo;
	}
	public void setGvPanNo(String gvPanNo) {
		this.gvPanNo = gvPanNo;
	}
	public String getGvErnam() {
		return gvErnam;
	}
	public void setGvErnam(String gvErnam) {
		this.gvErnam = gvErnam;
	}
	public BigDecimal getvTax() {
		return vTax;
	}
	public void setvTax(BigDecimal vTax) {
		this.vTax = vTax;
	}
	public BigDecimal getGvTotal() {
		return gvTotal;
	}
	public void setGvTotal(BigDecimal gvTotal) {
		this.gvTotal = gvTotal;
	}
	
	
	public BigDecimal getGvNtTotal() {
		return gvNtTotal;
	}
	public void setGvNtTotal(BigDecimal gvNtTotal) {
		this.gvNtTotal = gvNtTotal;
	}
	public String getvAmt() {
		return vAmt;
	}
	public void setvAmt(String vAmt) {
		this.vAmt = vAmt;
	}
	public String getvAmount() {
		return vAmount;
	}
	public void setvAmount(String vAmount) {
		this.vAmount = vAmount;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getvWords() {
		return vWords;
	}
	public void setvWords(String vWords) {
		this.vWords = vWords;
	}
	public String getCommInv() {
		return commInv;
	}
	public void setCommInv(String commInv) {
		this.commInv = commInv;
	}
	public Date getCommDat() {
		return commDat;
	}
	public void setCommDat(Date commDat) {
		this.commDat = commDat;
	}
	public List<TLines> gettLineList() {
		return tLineList;
	}
	public void settLineList(List<TLines> tLineList) {
		this.tLineList = tLineList;
	}
	public List<TZConSize> getTzConSizeList() {
		return tzConSizeList;
	}
	public void setTzConSizeList(List<TZConSize> tzConSizeList) {
		this.tzConSizeList = tzConSizeList;
	}
	public List<ZsdSexInvDetail> gettConList() {
		return tConList;
	}
	public void settConList(List<ZsdSexInvDetail> tConList) {
		this.tConList = tConList;
	}
	public List<ZsdSexInvDetail> gettHeaderList() {
		return tHeaderList;
	}
	public void settHeaderList(List<ZsdSexInvDetail> tHeaderList) {
		this.tHeaderList = tHeaderList;
	}
	public List<ZsdSexInvDetail> gettPerconList() {
		return tPerconList;
	}
	public void settPerconList(List<ZsdSexInvDetail> tPerconList) {
		this.tPerconList = tPerconList;
	}
	public List<ZsdSexInvDetailInd> gettFHeaderList() {
		return tFHeaderList;
	}
	public void settFHeaderList(List<ZsdSexInvDetailInd> tFHeaderList) {
		this.tFHeaderList = tFHeaderList;
	}
	public List<ZsdSexInvDetailInd> gettFPerConList() {
		return tFPerConList;
	}
	public void settFPerConList(List<ZsdSexInvDetailInd> tFPerConList) {
		this.tFPerConList = tFPerConList;
	}
	public List<ZsdSexInvDetailInd> gettFinalConList() {
		return tFinalConList;
	}
	public void settFinalConList(List<ZsdSexInvDetailInd> tFinalConList) {
		this.tFinalConList = tFinalConList;
	}
	
			
}
