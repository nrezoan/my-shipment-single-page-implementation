package com.myshipment.model;

/**
 * @author Virendra Kumar
 *
 */
public class Login {
	private static final long serialVersionUID = -17135253366201L;
	private String userCode;
	private String password;

	/**
	 * @return
	 */
	

	public String getPassword() {
		return password;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
