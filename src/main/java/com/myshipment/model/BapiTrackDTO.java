package com.myshipment.model;

import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class BapiTrackDTO {

	private Date zzmblmawbdt;	
	private String zzmblmawbno;	
	private String zzhblhawbno;	
	private String zzcomminvno;	
	private String zzportofdest;	
	private String zzreference1;	
	private String zzponumber;	
	private String zzstyleno;	
	private String matnr;	
	private Date budat;	
	private Date zstuffingdate;	
	private String vhilm;	
	private String matkl;	
	private String zzvoynam1;	
	private Date datbg;	
	private String zzvoynam2;	
	private String zzarlinenm1;	
	private String zzarlinenm2;	
	private Date dptbg;	
	private Date dpten;	
	private String zzskuno;	
	private String zzarticleno;	
	private String zzportofloading;	
	private String wgbez60;	
	private String exti1;	
	private String exti2;	
	private String zzairlinename1;	
	private String zzairlinename2;	
	private String knota;	
	private String knotz;	
	private Date zzloadingdt;	
	private Date zztrans1etd;	
	private String zzairlineno2;	
	private String zzvoyno2;	
	private String zzarlineno3;	
	private Date zzeta;	
	private Date zzata_dt;	
	private String zzcolour;	
	private String zzedisize;	
	private String vbeln;	
	private String port_pol;	
	private String port_pod;	
	private String zzairlineno1;	
	private Date fvetdport;	
	private Date fvetaport;	
	private Date mvetdport;	
	private Date mvetaport;	
	private String vhilm_ku;	
	private Date zzhblhawbdt;	
	private Integer zzsoquantity;	
	private Integer zzsoquanuom;	
	private Double zztot_chrg_wt;	
	private Double zztotalvolwt;	
	private String flight_no;	
	private String zzplacedelivery;	
	private String lport_name;	
	private String dport_name;	
	private String delport_name;	
	private String originagent;	
	private String shippingline;	
	private String shipper;	
	private String consignee;	
	private String soldtoparty;
	public Date getZzmblmawbdt() {
		return zzmblmawbdt;
	}
	public void setZzmblmawbdt(Date zzmblmawbdt) {
		this.zzmblmawbdt = zzmblmawbdt;
	}
	public String getZzmblmawbno() {
		return zzmblmawbno;
	}
	public void setZzmblmawbno(String zzmblmawbno) {
		this.zzmblmawbno = zzmblmawbno;
	}
	public String getZzhblhawbno() {
		return zzhblhawbno;
	}
	public void setZzhblhawbno(String zzhblhawbno) {
		this.zzhblhawbno = zzhblhawbno;
	}
	public String getZzcomminvno() {
		return zzcomminvno;
	}
	public void setZzcomminvno(String zzcomminvno) {
		this.zzcomminvno = zzcomminvno;
	}
	public String getZzportofdest() {
		return zzportofdest;
	}
	public void setZzportofdest(String zzportofdest) {
		this.zzportofdest = zzportofdest;
	}
	public String getZzreference1() {
		return zzreference1;
	}
	public void setZzreference1(String zzreference1) {
		this.zzreference1 = zzreference1;
	}
	public String getZzponumber() {
		return zzponumber;
	}
	public void setZzponumber(String zzponumber) {
		this.zzponumber = zzponumber;
	}
	public String getZzstyleno() {
		return zzstyleno;
	}
	public void setZzstyleno(String zzstyleno) {
		this.zzstyleno = zzstyleno;
	}
	public String getMatnr() {
		return matnr;
	}
	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}
	public Date getBudat() {
		return budat;
	}
	public void setBudat(Date budat) {
		this.budat = budat;
	}
	public Date getZstuffingdate() {
		return zstuffingdate;
	}
	public void setZstuffingdate(Date zstuffingdate) {
		this.zstuffingdate = zstuffingdate;
	}
	public String getVhilm() {
		return vhilm;
	}
	public void setVhilm(String vhilm) {
		this.vhilm = vhilm;
	}
	public String getMatkl() {
		return matkl;
	}
	public void setMatkl(String matkl) {
		this.matkl = matkl;
	}
	public String getZzvoynam1() {
		return zzvoynam1;
	}
	public void setZzvoynam1(String zzvoynam1) {
		this.zzvoynam1 = zzvoynam1;
	}
	public Date getDatbg() {
		return datbg;
	}
	public void setDatbg(Date datbg) {
		this.datbg = datbg;
	}
	public String getZzvoynam2() {
		return zzvoynam2;
	}
	public void setZzvoynam2(String zzvoynam2) {
		this.zzvoynam2 = zzvoynam2;
	}
	public String getZzarlinenm1() {
		return zzarlinenm1;
	}
	public void setZzarlinenm1(String zzarlinenm1) {
		this.zzarlinenm1 = zzarlinenm1;
	}
	public String getZzarlinenm2() {
		return zzarlinenm2;
	}
	public void setZzarlinenm2(String zzarlinenm2) {
		this.zzarlinenm2 = zzarlinenm2;
	}
	public Date getDptbg() {
		return dptbg;
	}
	public void setDptbg(Date dptbg) {
		this.dptbg = dptbg;
	}
	public Date getDpten() {
		return dpten;
	}
	public void setDpten(Date dpten) {
		this.dpten = dpten;
	}
	public String getZzskuno() {
		return zzskuno;
	}
	public void setZzskuno(String zzskuno) {
		this.zzskuno = zzskuno;
	}
	public String getZzarticleno() {
		return zzarticleno;
	}
	public void setZzarticleno(String zzarticleno) {
		this.zzarticleno = zzarticleno;
	}
	public String getZzportofloading() {
		return zzportofloading;
	}
	public void setZzportofloading(String zzportofloading) {
		this.zzportofloading = zzportofloading;
	}
	public String getWgbez60() {
		return wgbez60;
	}
	public void setWgbez60(String wgbez60) {
		this.wgbez60 = wgbez60;
	}
	public String getExti1() {
		return exti1;
	}
	public void setExti1(String exti1) {
		this.exti1 = exti1;
	}
	public String getExti2() {
		return exti2;
	}
	public void setExti2(String exti2) {
		this.exti2 = exti2;
	}
	public String getZzairlinename1() {
		return zzairlinename1;
	}
	public void setZzairlinename1(String zzairlinename1) {
		this.zzairlinename1 = zzairlinename1;
	}
	public String getZzairlinename2() {
		return zzairlinename2;
	}
	public void setZzairlinename2(String zzairlinename2) {
		this.zzairlinename2 = zzairlinename2;
	}
	public String getKnota() {
		return knota;
	}
	public void setKnota(String knota) {
		this.knota = knota;
	}
	public String getKnotz() {
		return knotz;
	}
	public void setKnotz(String knotz) {
		this.knotz = knotz;
	}
	public Date getZzloadingdt() {
		return zzloadingdt;
	}
	public void setZzloadingdt(Date zzloadingdt) {
		this.zzloadingdt = zzloadingdt;
	}
	public Date getZztrans1etd() {
		return zztrans1etd;
	}
	public void setZztrans1etd(Date zztrans1etd) {
		this.zztrans1etd = zztrans1etd;
	}
	public String getZzairlineno2() {
		return zzairlineno2;
	}
	public void setZzairlineno2(String zzairlineno2) {
		this.zzairlineno2 = zzairlineno2;
	}
	public String getZzvoyno2() {
		return zzvoyno2;
	}
	public void setZzvoyno2(String zzvoyno2) {
		this.zzvoyno2 = zzvoyno2;
	}
	public String getZzarlineno3() {
		return zzarlineno3;
	}
	public void setZzarlineno3(String zzarlineno3) {
		this.zzarlineno3 = zzarlineno3;
	}
	public Date getZzeta() {
		return zzeta;
	}
	public void setZzeta(Date zzeta) {
		this.zzeta = zzeta;
	}
	public Date getZzata_dt() {
		return zzata_dt;
	}
	public void setZzata_dt(Date zzata_dt) {
		this.zzata_dt = zzata_dt;
	}
	public String getZzcolour() {
		return zzcolour;
	}
	public void setZzcolour(String zzcolour) {
		this.zzcolour = zzcolour;
	}
	public String getZzedisize() {
		return zzedisize;
	}
	public void setZzedisize(String zzedisize) {
		this.zzedisize = zzedisize;
	}
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public String getPort_pol() {
		return port_pol;
	}
	public void setPort_pol(String port_pol) {
		this.port_pol = port_pol;
	}
	public String getPort_pod() {
		return port_pod;
	}
	public void setPort_pod(String port_pod) {
		this.port_pod = port_pod;
	}
	public String getZzairlineno1() {
		return zzairlineno1;
	}
	public void setZzairlineno1(String zzairlineno1) {
		this.zzairlineno1 = zzairlineno1;
	}
	public Date getFvetdport() {
		return fvetdport;
	}
	public void setFvetdport(Date fvetdport) {
		this.fvetdport = fvetdport;
	}
	public Date getFvetaport() {
		return fvetaport;
	}
	public void setFvetaport(Date fvetaport) {
		this.fvetaport = fvetaport;
	}
	public Date getMvetdport() {
		return mvetdport;
	}
	public void setMvetdport(Date mvetdport) {
		this.mvetdport = mvetdport;
	}
	public Date getMvetaport() {
		return mvetaport;
	}
	public void setMvetaport(Date mvetaport) {
		this.mvetaport = mvetaport;
	}
	public String getVhilm_ku() {
		return vhilm_ku;
	}
	public void setVhilm_ku(String vhilm_ku) {
		this.vhilm_ku = vhilm_ku;
	}
	public Date getZzhblhawbdt() {
		return zzhblhawbdt;
	}
	public void setZzhblhawbdt(Date zzhblhawbdt) {
		this.zzhblhawbdt = zzhblhawbdt;
	}
	public Integer getZzsoquantity() {
		return zzsoquantity;
	}
	public void setZzsoquantity(Integer zzsoquantity) {
		this.zzsoquantity = zzsoquantity;
	}
	public Integer getZzsoquanuom() {
		return zzsoquanuom;
	}
	public void setZzsoquanuom(Integer zzsoquanuom) {
		this.zzsoquanuom = zzsoquanuom;
	}
	public Double getZztot_chrg_wt() {
		return zztot_chrg_wt;
	}
	public void setZztot_chrg_wt(Double zztot_chrg_wt) {
		this.zztot_chrg_wt = zztot_chrg_wt;
	}
	public Double getZztotalvolwt() {
		return zztotalvolwt;
	}
	public void setZztotalvolwt(Double zztotalvolwt) {
		this.zztotalvolwt = zztotalvolwt;
	}
	public String getFlight_no() {
		return flight_no;
	}
	public void setFlight_no(String flight_no) {
		this.flight_no = flight_no;
	}
	public String getZzplacedelivery() {
		return zzplacedelivery;
	}
	public void setZzplacedelivery(String zzplacedelivery) {
		this.zzplacedelivery = zzplacedelivery;
	}
	public String getLport_name() {
		return lport_name;
	}
	public void setLport_name(String lport_name) {
		this.lport_name = lport_name;
	}
	public String getDport_name() {
		return dport_name;
	}
	public void setDport_name(String dport_name) {
		this.dport_name = dport_name;
	}
	public String getDelport_name() {
		return delport_name;
	}
	public void setDelport_name(String delport_name) {
		this.delport_name = delport_name;
	}
	public String getOriginagent() {
		return originagent;
	}
	public void setOriginagent(String originagent) {
		this.originagent = originagent;
	}
	public String getShippingline() {
		return shippingline;
	}
	public void setShippingline(String shippingline) {
		this.shippingline = shippingline;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getConsignee() {
		return consignee;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	public String getSoldtoparty() {
		return soldtoparty;
	}
	public void setSoldtoparty(String soldtoparty) {
		this.soldtoparty = soldtoparty;
	}
	
	
}
