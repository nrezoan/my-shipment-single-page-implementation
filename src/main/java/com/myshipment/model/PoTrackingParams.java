package com.myshipment.model;

import java.util.Date;

/*
 * @Ranjeet Kumar
 */
public class PoTrackingParams {

/*	private String poNo;

	private String division;
	private String statusType;
	private String expStatusType;	
	private String shipper_no;
	private String buyer_no;
	private String agent_no;*/
	
	private String customer;	
	private String salesOrg;	
	private String distChan;	
	private String division;
	private String fromDate;
	private String toDate;
	private String action;
	private String doc_no;	
	private String statusType;
	private String expStatusType;
	private String shipper_no;
	private String buyer_no;
	private String agent_no;

	public String getShipper_no() {
		return shipper_no;
	}

	public String getBuyer_no() {
		return buyer_no;
	}

	public String getAgent_no() {
		return agent_no;
	}

	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}

	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}

	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}

	public String getCustomer() {
		return customer;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public String getDistChan() {
		return distChan;
	}

	public String getDivision() {
		return division;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public String getAction() {
		return action;
	}

	public String getDoc_no() {
		return doc_no;
	}

	public String getStatusType() {
		return statusType;
	}

	public String getExpStatusType() {
		return expStatusType;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public void setDistChan(String distChan) {
		this.distChan = distChan;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setDoc_no(String doc_no) {
		this.doc_no = doc_no;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public void setExpStatusType(String expStatusType) {
		this.expStatusType = expStatusType;
	}
	
	

	
}
