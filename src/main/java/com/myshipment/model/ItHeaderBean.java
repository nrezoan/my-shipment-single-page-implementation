package com.myshipment.model;

import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class ItHeaderBean {

	private String vbeln;	// so number
	private Date erdat;	// BOOKING DATE
	private String zzhblhawbno;	//hbl number
	//private String zzhblhawbdt;	//hbl date
	private Date zzhblhawbdt;
	private String zzcomminvno;	//commmercial invoice nu
	private String zzportofloading;	//POL
	private String zzportofdest;	//POD
	private String zzairlinename1;	// fvsl name
	private String zzairlineno1;	
	private String zzairlinename2;	//mvsl name
	private String zzairlineno2;	
	private Date fvetdport;	// fvsl etd 
	private Date fvetaport;	 // fvsl eta
	private Date mvetdport;	 //mvsl etd
	private Date mvetaport;	// mvsl eta
	private String zzmblmawbno;// MBL No
	private String bookingdate;
	private String hblDate;
	private String fvEtdPort;
	private String fvEtaPort;
	private String mvEtdPort;
	private String mvEtaaPort;
	
	public String getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(String bookingdate) {
		this.bookingdate = bookingdate;
	}
	public String getHblDate() {
		return hblDate;
	}
	public void setHblDate(String hblDate) {
		this.hblDate = hblDate;
	}
	public String getFvEtdPort() {
		return fvEtdPort;
	}
	public void setFvEtdPort(String fvEtdPort) {
		this.fvEtdPort = fvEtdPort;
	}
	public String getFvEtaPort() {
		return fvEtaPort;
	}
	public void setFvEtaPort(String fvEtaPort) {
		this.fvEtaPort = fvEtaPort;
	}
	public String getMvEtdPort() {
		return mvEtdPort;
	}
	public void setMvEtdPort(String mvEtdPort) {
		this.mvEtdPort = mvEtdPort;
	}
	public String getMvEtaaPort() {
		return mvEtaaPort;
	}
	public void setMvEtaaPort(String mvEtaaPort) {
		this.mvEtaaPort = mvEtaaPort;
	}
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public Date getErdat() {
		return erdat;
	}
	public void setErdat(Date erdat) {
		this.erdat = erdat;
	}
	public String getZzhblhawbno() {
		return zzhblhawbno;
	}
	public void setZzhblhawbno(String zzhblhawbno) {
		this.zzhblhawbno = zzhblhawbno;
	}

	public String getZzcomminvno() {
		return zzcomminvno;
	}
	public void setZzcomminvno(String zzcomminvno) {
		this.zzcomminvno = zzcomminvno;
	}
	public String getZzportofloading() {
		return zzportofloading;
	}
	public void setZzportofloading(String zzportofloading) {
		this.zzportofloading = zzportofloading;
	}
	public String getZzportofdest() {
		return zzportofdest;
	}
	public void setZzportofdest(String zzportofdest) {
		this.zzportofdest = zzportofdest;
	}
	public String getZzairlinename1() {
		return zzairlinename1;
	}
	public void setZzairlinename1(String zzairlinename1) {
		this.zzairlinename1 = zzairlinename1;
	}
	public String getZzairlineno1() {
		return zzairlineno1;
	}
	public void setZzairlineno1(String zzairlineno1) {
		this.zzairlineno1 = zzairlineno1;
	}
	public String getZzairlinename2() {
		return zzairlinename2;
	}
	public void setZzairlinename2(String zzairlinename2) {
		this.zzairlinename2 = zzairlinename2;
	}
	public String getZzairlineno2() {
		return zzairlineno2;
	}
	public void setZzairlineno2(String zzairlineno2) {
		this.zzairlineno2 = zzairlineno2;
	}
	public Date getFvetdport() {
		return fvetdport;
	}
	public void setFvetdport(Date fvetdport) {
		this.fvetdport = fvetdport;
	}
	public Date getFvetaport() {
		return fvetaport;
	}
	public void setFvetaport(Date fvetaport) {
		this.fvetaport = fvetaport;
	}
	public Date getMvetdport() {
		return mvetdport;
	}
	public void setMvetdport(Date mvetdport) {
		this.mvetdport = mvetdport;
	}
	public Date getMvetaport() {
		return mvetaport;
	}
	public void setMvetaport(Date mvetaport) {
		this.mvetaport = mvetaport;
	}
	public String getZzmblmawbno() {
		return zzmblmawbno;
	}
	public void setZzmblmawbno(String zzmblmawbno) {
		this.zzmblmawbno = zzmblmawbno;
	}
	public Date getZzhblhawbdt() {
		return zzhblhawbdt;
	}
	public void setZzhblhawbdt(Date zzhblhawbdt) {
		this.zzhblhawbdt = zzhblhawbdt;
	}
	@Override
	public String toString() {
		return "ItHeaderBean [vbeln=" + vbeln + ", erdat=" + erdat + ", zzhblhawbno=" + zzhblhawbno + ", zzhblhawbdt="
				+ zzhblhawbdt + ", zzcomminvno=" + zzcomminvno + ", zzportofloading=" + zzportofloading
				+ ", zzportofdest=" + zzportofdest + ", zzairlinename1=" + zzairlinename1 + ", zzairlineno1="
				+ zzairlineno1 + ", zzairlinename2=" + zzairlinename2 + ", zzairlineno2=" + zzairlineno2
				+ ", fvetdport=" + fvetdport + ", fvetaport=" + fvetaport + ", mvetdport=" + mvetdport + ", mvetaport="
				+ mvetaport + ", zzmblmawbno=" + zzmblmawbno + ", bookingdate=" + bookingdate + ", hblDate=" + hblDate
				+ ", fvEtdPort=" + fvEtdPort + ", fvEtaPort=" + fvEtaPort + ", mvEtdPort=" + mvEtdPort + ", mvEtaaPort="
				+ mvEtaaPort + "]";
	}
	
	
}
