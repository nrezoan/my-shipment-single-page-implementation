/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.util.List;
import java.util.Map;

public class PreAlertBL {

	private Map<String, List<PreAlertHBLDetails>> mbl;
	private List<PreAlertHBLDetails> hbl;
	
	public Map<String, List<PreAlertHBLDetails>> getMbl() {
		return mbl;
	}
	public void setMbl(Map<String, List<PreAlertHBLDetails>> mbl) {
		this.mbl = mbl;
	}
	public List<PreAlertHBLDetails> getHbl() {
		return hbl;
	}
	public void setHbl(List<PreAlertHBLDetails> hbl) {
		this.hbl = hbl;
	}
	
	
	
}
