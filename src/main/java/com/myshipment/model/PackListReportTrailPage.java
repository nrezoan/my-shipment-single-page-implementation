package com.myshipment.model;

import java.util.List;

/**
 * @author virendra
 *
 */
public class PackListReportTrailPage {
	private static final long serialVersionUID=-34123414L;
	private SellerDetails sellerDetails;
	private MakerDetails makerDetails;
	private BuyerDetails buyerDetails;
	private ConsigneeDetails consigneeDetails;
	private String packinglistno;
	private String date;
	private PackingListOrderHeader packingListOrderHeader;
	private List<PackingListItems> lstPackingListItems;
	private List<String> shippingMarks;
	public SellerDetails getSellerDetails() {
		return sellerDetails;
	}
	public void setSellerDetails(SellerDetails sellerDetails) {
		this.sellerDetails = sellerDetails;
	}
	public MakerDetails getMakerDetails() {
		return makerDetails;
	}
	public void setMakerDetails(MakerDetails makerDetails) {
		this.makerDetails = makerDetails;
	}
	public BuyerDetails getBuyerDetails() {
		return buyerDetails;
	}
	public void setBuyerDetails(BuyerDetails buyerDetails) {
		this.buyerDetails = buyerDetails;
	}
	public ConsigneeDetails getConsigneeDetails() {
		return consigneeDetails;
	}
	public void setConsigneeDetails(ConsigneeDetails consigneeDetails) {
		this.consigneeDetails = consigneeDetails;
	}
	public String getPackinglistno() {
		return packinglistno;
	}
	public void setPackinglistno(String packinglistno) {
		this.packinglistno = packinglistno;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public PackingListOrderHeader getPackingListOrderHeader() {
		return packingListOrderHeader;
	}
	public void setPackingListOrderHeader(PackingListOrderHeader packingListOrderHeader) {
		this.packingListOrderHeader = packingListOrderHeader;
	}
	public List<PackingListItems> getLstPackingListItems() {
		return lstPackingListItems;
	}
	public void setLstPackingListItems(List<PackingListItems> lstPackingListItems) {
		this.lstPackingListItems = lstPackingListItems;
	}
	public List<String> getShippingMarks() {
		return shippingMarks;
	}
	public void setShippingMarks(List<String> shippingMarks) {
		this.shippingMarks = shippingMarks;
	}
	

}
