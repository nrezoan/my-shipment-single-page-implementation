package com.myshipment.model;

public class BuyerWiseCBMJson {
	private String buyerNo;
	private String buyerName;
	private double totalCBM;
	private double totalCBMPerc;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public double getTotalCBMPerc() {
		return totalCBMPerc;
	}
	public void setTotalCBMPerc(double totalCBMPerc) {
		this.totalCBMPerc = totalCBMPerc;
	}
	

}
