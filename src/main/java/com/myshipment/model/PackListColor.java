package com.myshipment.model;

import java.util.List;

public class PackListColor {
	private static final long serialVersionUID=-9874343l;
	private long colorId;
	private String name;
	private long pcsPerCartoon;
	private long totalPcs;
	private String remarks;
	private long plId;
	private List<PackListSize> packListSize;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPcsPerCartoon() {
		return pcsPerCartoon;
	}
	public void setPcsPerCartoon(long pcsPerCartoon) {
		this.pcsPerCartoon = pcsPerCartoon;
	}
	public long getTotalPcs() {
		return totalPcs;
	}
	public void setTotalPcs(long totalPcs) {
		this.totalPcs = totalPcs;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public long getColorId() {
		return colorId;
	}
	public void setColorId(long colorId) {
		this.colorId = colorId;
	}
	public List<PackListSize> getPackListSize() {
		return packListSize;
	}
	public void setPackListSize(List<PackListSize> packListSize) {
		this.packListSize = packListSize;
	}
	public long getPlId() {
		return plId;
	}
	public void setPlId(long plId) {
		this.plId = plId;
	}
	
	

}
