package com.myshipment.model;

public class SupRequestParams {

	private String customerId;
	private String salesOrg;
	private String distChan;
	private String division;
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDistChan() {
		return distChan;
	}
	public void setDistChan(String distChan) {
		this.distChan = distChan;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
}
	
