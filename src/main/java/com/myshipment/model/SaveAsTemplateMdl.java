package com.myshipment.model;

/*
 * @ Gufranur Rahman
 */
public class SaveAsTemplateMdl {

	private long template_Id;
	public String customerCode = "";
	public String company = "";
	public String operationType = "";
	public String seaOrAir = "";
	public String templateName = "";
	public String templateData = "";
	public String createdDate = "";
	public String createdBy = "";
	public String isActive = "";
	
	public long getTemplate_Id() {
		return template_Id;
	}
	public void setTemplate_Id(long template_Id) {
		this.template_Id = template_Id;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getOperationType() {
		return operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getSeaOrAir() {
		return seaOrAir;
	}
	public void setSeaOrAir(String seaOrAir) {
		this.seaOrAir = seaOrAir;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getTemplateData() {
		return templateData;
	}
	public void setTemplateData(String templateData) {
		this.templateData = templateData;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	
	
}
