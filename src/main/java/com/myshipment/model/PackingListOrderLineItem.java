package com.myshipment.model;

/**
 * @author virendra
 *
 */
public class PackingListOrderLineItem {
	
	private static final long serialVersionUID=-234123431434l;
	private String orderNumber;
	private String styleNumber;
	private String productDescription;
	private String packType;
	private long totalQuantity;
	private CartoonDimension ctnDimension;
	private long numberOfCartoons;
	private double unitVolume;
	private double netWgt;
	private double grossWgt;
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getStyleNumber() {
		return styleNumber;
	}
	public void setStyleNumber(String styleNumber) {
		this.styleNumber = styleNumber;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public long getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	public CartoonDimension getCtnDimension() {
		return ctnDimension;
	}
	public void setCtnDimension(CartoonDimension ctnDimension) {
		this.ctnDimension = ctnDimension;
	}
	public long getNumberOfCartoons() {
		return numberOfCartoons;
	}
	public void setNumberOfCartoons(long numberOfCartoons) {
		this.numberOfCartoons = numberOfCartoons;
	}
	public double getUnitVolume() {
		return unitVolume;
	}
	public void setUnitVolume(double unitVolume) {
		this.unitVolume = unitVolume;
	}
	public double getNetWgt() {
		return netWgt;
	}
	public void setNetWgt(double netWgt) {
		this.netWgt = netWgt;
	}
	public double getGrossWgt() {
		return grossWgt;
	}
	public void setGrossWgt(double grossWgt) {
		this.grossWgt = grossWgt;
	}
	
	
}
