package com.myshipment.model;

import java.util.Date;

public class BookedPOApproval {
	/*
	 * @Hamid
	 */
	//private String document_no;
	private String so_no;
	private String bl_no;	
	private String item_no;	
	private String po_no;	
	private Date item_bl_dt;	
	private Double item_pcs;	
	private Double item_qty;	
	private String style;	
	private Double item_vol;
	private Date item_chdt;	
	private Date indc_dt;	
	private Date po_req_ship_dt;	
	private String fvsl_name;	
	private Date fetd;	
	private String mvsl_name;	
	private Date meta;	
	private String carrier_name;	
	private String status;	
	private String remarks;
	private String buyer_no;
	private String size_no;
	private String color;
	private Double item_grwt;
	private String shipper_no;
	private String action_user;
	private Date action_timestamp;
	private String buyer_name;
	private String shipper_name;

/*	public String getDocument_no() {
		return document_no;
	}
	public void setDocument_no(String document_no) {
		this.document_no = document_no;
	}*/
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
/*	public String getLine_item_no() {
		return line_item_no;
	}
	public void setLine_item_no(String line_item_no) {
		this.line_item_no = line_item_no;
	}*/
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	/*public Date getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(Date booking_date) {
		this.booking_date = booking_date;
	}
	public Double getTot_pieces() {
		return tot_pieces;
	}
	public void setTot_pieces(Double tot_pieces) {
		this.tot_pieces = tot_pieces;
	}
	public Double getCar_qty() {
		return car_qty;
	}
	public void setCar_qty(Double car_qty) {
		this.car_qty = car_qty;
	}
	public String getStyle_no() {
		return style_no;
	}
	public void setStyle_no(String style_no) {
		this.style_no = style_no;
	}
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	public Date getChdt() {
		return chdt;
	}
	public void setChdt(Date chdt) {
		this.chdt = chdt;
	}*/
	public Date getIndc_dt() {
		return indc_dt;
	}
	public void setIndc_dt(Date indc_dt) {
		this.indc_dt = indc_dt;
	}
	public Date getPo_req_ship_dt() {
		return po_req_ship_dt;
	}
	public void setPo_req_ship_dt(Date po_req_ship_dt) {
		this.po_req_ship_dt = po_req_ship_dt;
	}
	public String getFvsl_name() {
		return fvsl_name;
	}
	public void setFvsl_name(String fvsl_name) {
		this.fvsl_name = fvsl_name;
	}
	public Date getFetd() {
		return fetd;
	}
	public void setFetd(Date fetd) {
		this.fetd = fetd;
	}
	public String getMvsl_name() {
		return mvsl_name;
	}
	public void setMvsl_name(String mvsl_name) {
		this.mvsl_name = mvsl_name;
	}
	public Date getMeta() {
		return meta;
	}
	public void setMeta(Date meta) {
		this.meta = meta;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
/*	public String getSize_no() {
		return size_no;
	}
	public void setSize_no(String size_no) {
		this.size_no = size_no;
	}*/
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
/*	public Double getGross_wt() {
		return gross_wt;
	}
	public void setGross_wt(Double gross_wt) {
		this.gross_wt = gross_wt;
	}*/
	public String getSo_no() {
		return so_no;
	}
	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}
	public String getItem_no() {
		return item_no;
	}
	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}
	public Date getItem_bl_dt() {
		return item_bl_dt;
	}
	public void setItem_bl_dt(Date item_bl_dt) {
		this.item_bl_dt = item_bl_dt;
	}
	public Double getItem_pcs() {
		return item_pcs;
	}
	public void setItem_pcs(Double item_pcs) {
		this.item_pcs = item_pcs;
	}
	public Double getItem_qty() {
		return item_qty;
	}
	public void setItem_qty(Double item_qty) {
		this.item_qty = item_qty;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public Double getItem_vol() {
		return item_vol;
	}
	public void setItem_vol(Double item_vol) {
		this.item_vol = item_vol;
	}
	public Date getItem_chdt() {
		return item_chdt;
	}
	public void setItem_chdt(Date item_chdt) {
		this.item_chdt = item_chdt;
	}
	public String getSize_no() {
		return size_no;
	}
	public void setSize_no(String size_no) {
		this.size_no = size_no;
	}
/*	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}*/
	public Double getItem_grwt() {
		return item_grwt;
	}
	public void setItem_grwt(Double item_grwt) {
		this.item_grwt = item_grwt;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer_name() {
		return buyer_name;
	}
	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}
	public String getShipper_name() {
		return shipper_name;
	}
	public void setShipper_name(String shipper_name) {
		this.shipper_name = shipper_name;
	}
	public String getAction_user() {
		return action_user;
	}
	public void setAction_user(String action_user) {
		this.action_user = action_user;
	}
	public Date getAction_timestamp() {
		return action_timestamp;
	}
	public void setAction_timestamp(Date action_timestamp) {
		this.action_timestamp = action_timestamp;
	}
	
}
