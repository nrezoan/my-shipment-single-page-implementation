package com.myshipment.model;

import java.math.BigDecimal;

public class CargoAckJasper {
	private String poSnr;
	private String name1;
	private String name2;
	private String name3;
	private String name4;
	private String zzPoNumber;
	private String zzCommInvNo;
	private String buDat;
	private BigDecimal menge;
	private String zzHblHawbNo;
	private BigDecimal zzSoQuantity;
	
	public String getPoSnr() {
		return poSnr;
	}
	public void setPoSnr(String poSnr) {
		this.poSnr = poSnr;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getName3() {
		return name3;
	}
	public void setName3(String name3) {
		this.name3 = name3;
	}
	public String getName4() {
		return name4;
	}
	public void setName4(String name4) {
		this.name4 = name4;
	}
	public String getZzPoNumber() {
		return zzPoNumber;
	}
	public void setZzPoNumber(String zzPoNumber) {
		this.zzPoNumber = zzPoNumber;
	}
	public String getZzCommInvNo() {
		return zzCommInvNo;
	}
	public void setZzCommInvNo(String zzCommInvNo) {
		this.zzCommInvNo = zzCommInvNo;
	}
	public String getBuDat() {
		return buDat;
	}
	public void setBuDat(String buDat) {
		this.buDat = buDat;
	}
	public BigDecimal getMenge() {
		return menge;
	}
	public void setMenge(BigDecimal menge) {
		this.menge = menge;
	}
	public String getZzHblHawbNo() {
		return zzHblHawbNo;
	}
	public void setZzHblHawbNo(String zzHblHawbNo) {
		this.zzHblHawbNo = zzHblHawbNo;
	}
	public BigDecimal getZzSoQuantity() {
		return zzSoQuantity;
	}
	public void setZzSoQuantity(BigDecimal zzSoQuantity) {
		this.zzSoQuantity = zzSoQuantity;
	}
	
	
	

}
