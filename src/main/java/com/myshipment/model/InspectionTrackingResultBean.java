package com.myshipment.model;

import java.util.Date;

/*
 * @Ranjeet Kumar
 */
public class InspectionTrackingResultBean {

	private int BOOKING_NO;
	private String SALES_ORG;
	private String DIST;
	private String DIVISION;
	private String SUPPLIER_CODE;
	private String BUYING_CODE;
	private String INSPECTOR_CODE;
	private Date INSPECTION_DATE;
	private String PO_NUMBER;
	private Date DELIVERY_DATE;
	private int ORDER_QTY;
	private String SHIPMENT_MODE;
	private String REMARKS;
	private Date ENTRY_TIME;
	private String SAP_NO;
	private int INSPECTION_QTY;
	private String LOCATION;
	private String ADV_ORDER;
	private String PACK_METHOD;
	private String CUSTOMER_NAME;
	private String SCOTA_FILE_SUBMISSION;
	private Date SCOTA_FILE_SUBMISSION_DATE;
	private Date CARGO_DELIVERY_DATE;
	private String INSPECTION_BOOKING_STATUS;
	
	public int getBOOKING_NO() {
		return BOOKING_NO;
	}
	public void setBOOKING_NO(int bOOKING_NO) {
		BOOKING_NO = bOOKING_NO;
	}
	public String getSALES_ORG() {
		return SALES_ORG;
	}
	public void setSALES_ORG(String sALES_ORG) {
		SALES_ORG = sALES_ORG;
	}
	public String getDIST() {
		return DIST;
	}
	public void setDIST(String dIST) {
		DIST = dIST;
	}
	public String getDIVISION() {
		return DIVISION;
	}
	public void setDIVISION(String dIVISION) {
		DIVISION = dIVISION;
	}
	public String getSUPPLIER_CODE() {
		return SUPPLIER_CODE;
	}
	public void setSUPPLIER_CODE(String sUPPLIER_CODE) {
		SUPPLIER_CODE = sUPPLIER_CODE;
	}
	public String getBUYING_CODE() {
		return BUYING_CODE;
	}
	public void setBUYING_CODE(String bUYING_CODE) {
		BUYING_CODE = bUYING_CODE;
	}
	public String getINSPECTOR_CODE() {
		return INSPECTOR_CODE;
	}
	public void setINSPECTOR_CODE(String iNSPECTOR_CODE) {
		INSPECTOR_CODE = iNSPECTOR_CODE;
	}
	public Date getINSPECTION_DATE() {
		return INSPECTION_DATE;
	}
	public void setINSPECTION_DATE(Date iNSPECTION_DATE) {
		INSPECTION_DATE = iNSPECTION_DATE;
	}
	public String getPO_NUMBER() {
		return PO_NUMBER;
	}
	public void setPO_NUMBER(String pO_NUMBER) {
		PO_NUMBER = pO_NUMBER;
	}
	public Date getDELIVERY_DATE() {
		return DELIVERY_DATE;
	}
	public void setDELIVERY_DATE(Date dELIVERY_DATE) {
		DELIVERY_DATE = dELIVERY_DATE;
	}
	public int getORDER_QTY() {
		return ORDER_QTY;
	}
	public void setORDER_QTY(int oRDER_QTY) {
		ORDER_QTY = oRDER_QTY;
	}
	public String getSHIPMENT_MODE() {
		return SHIPMENT_MODE;
	}
	public void setSHIPMENT_MODE(String sHIPMENT_MODE) {
		SHIPMENT_MODE = sHIPMENT_MODE;
	}
	public String getREMARKS() {
		return REMARKS;
	}
	public void setREMARKS(String rEMARKS) {
		REMARKS = rEMARKS;
	}
	public Date getENTRY_TIME() {
		return ENTRY_TIME;
	}
	public void setENTRY_TIME(Date eNTRY_TIME) {
		ENTRY_TIME = eNTRY_TIME;
	}
	public String getSAP_NO() {
		return SAP_NO;
	}
	public void setSAP_NO(String sAP_NO) {
		SAP_NO = sAP_NO;
	}
	public int getINSPECTION_QTY() {
		return INSPECTION_QTY;
	}
	public void setINSPECTION_QTY(int iNSPECTION_QTY) {
		INSPECTION_QTY = iNSPECTION_QTY;
	}
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getADV_ORDER() {
		return ADV_ORDER;
	}
	public void setADV_ORDER(String aDV_ORDER) {
		ADV_ORDER = aDV_ORDER;
	}
	public String getPACK_METHOD() {
		return PACK_METHOD;
	}
	public void setPACK_METHOD(String pACK_METHOD) {
		PACK_METHOD = pACK_METHOD;
	}
	public String getCUSTOMER_NAME() {
		return CUSTOMER_NAME;
	}
	public void setCUSTOMER_NAME(String cUSTOMER_NAME) {
		CUSTOMER_NAME = cUSTOMER_NAME;
	}
	public String getSCOTA_FILE_SUBMISSION() {
		return SCOTA_FILE_SUBMISSION;
	}
	public void setSCOTA_FILE_SUBMISSION(String sCOTA_FILE_SUBMISSION) {
		SCOTA_FILE_SUBMISSION = sCOTA_FILE_SUBMISSION;
	}
	public Date getSCOTA_FILE_SUBMISSION_DATE() {
		return SCOTA_FILE_SUBMISSION_DATE;
	}
	public void setSCOTA_FILE_SUBMISSION_DATE(Date sCOTA_FILE_SUBMISSION_DATE) {
		SCOTA_FILE_SUBMISSION_DATE = sCOTA_FILE_SUBMISSION_DATE;
	}
	public Date getCARGO_DELIVERY_DATE() {
		return CARGO_DELIVERY_DATE;
	}
	public void setCARGO_DELIVERY_DATE(Date cARGO_DELIVERY_DATE) {
		CARGO_DELIVERY_DATE = cARGO_DELIVERY_DATE;
	}
	public String getINSPECTION_BOOKING_STATUS() {
		return INSPECTION_BOOKING_STATUS;
	}
	public void setINSPECTION_BOOKING_STATUS(String iNSPECTION_BOOKING_STATUS) {
		INSPECTION_BOOKING_STATUS = iNSPECTION_BOOKING_STATUS;
	}
	
	
	
}
