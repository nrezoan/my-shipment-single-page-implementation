package com.myshipment.model;

import java.math.BigDecimal;

public class ItVbap {
	
	private String zzPoNumber;
	private String vbeln;
	private String matnr;
	private String matwa;
	private String arktx;
	private String zzReference2;
	private String zzStyleNo;
	private BigDecimal zzQuantity;
	private BigDecimal zzPcsPerCarton;
	private String zzSkuNo;
	private BigDecimal zzNtWt;
	private BigDecimal zzGrWt;
	private BigDecimal zzVolume;
	private BigDecimal zzTotalNoPcs;
	public String getZzPoNumber() {
		return zzPoNumber;
	}
	public void setZzPoNumber(String zzPoNumber) {
		this.zzPoNumber = zzPoNumber;
	}
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public String getMatnr() {
		return matnr;
	}
	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}
	public String getMatwa() {
		return matwa;
	}
	public void setMatwa(String matwa) {
		this.matwa = matwa;
	}
	public String getArktx() {
		return arktx;
	}
	public void setArktx(String arktx) {
		this.arktx = arktx;
	}
	public String getZzReference2() {
		return zzReference2;
	}
	public void setZzReference2(String zzReference2) {
		this.zzReference2 = zzReference2;
	}
	public String getZzStyleNo() {
		return zzStyleNo;
	}
	public void setZzStyleNo(String zzStyleNo) {
		this.zzStyleNo = zzStyleNo;
	}
	public BigDecimal getZzQuantity() {
		return zzQuantity;
	}
	public void setZzQuantity(BigDecimal zzQuantity) {
		this.zzQuantity = zzQuantity;
	}
	public BigDecimal getZzPcsPerCarton() {
		return zzPcsPerCarton;
	}
	public void setZzPcsPerCarton(BigDecimal zzPcsPerCarton) {
		this.zzPcsPerCarton = zzPcsPerCarton;
	}
	public String getZzSkuNo() {
		return zzSkuNo;
	}
	public void setZzSkuNo(String zzSkuNo) {
		this.zzSkuNo = zzSkuNo;
	}
	public BigDecimal getZzNtWt() {
		return zzNtWt;
	}
	public void setZzNtWt(BigDecimal zzNtWt) {
		this.zzNtWt = zzNtWt;
	}
	public BigDecimal getZzGrWt() {
		return zzGrWt;
	}
	public void setZzGrWt(BigDecimal zzGrWt) {
		this.zzGrWt = zzGrWt;
	}
	public BigDecimal getZzVolume() {
		return zzVolume;
	}
	public void setZzVolume(BigDecimal zzVolume) {
		this.zzVolume = zzVolume;
	}
	public BigDecimal getZzTotalNoPcs() {
		return zzTotalNoPcs;
	}
	public void setZzTotalNoPcs(BigDecimal zzTotalNoPcs) {
		this.zzTotalNoPcs = zzTotalNoPcs;
	}
	
	

}
