package com.myshipment.model;

public class seaBillOfLadingPdfGeneration {
	private String shipperName;
	private String address1;
	private String address2;
	private String address3;
	private String shipperCity;
	private String country;
	private String countryOfOrigin;
	private String billOfLadingNo;
	// consignee
	private String consigneeName;
	private String consigneeAd1;
	private String consigneeAd2;
	private String consigneeAd3;
	private String congineeCity;
	private String consigneeCountry;
	// notify
	private String notify1;
	private String notify2;
	private String notify3;
	private String notify4;
	private String notifyCity;
	private String notifyCountry;
	private String portOfDischarge;
	private String placeOfReceipt;
	private String preCarrige;
	private String vessel;
	private String portOfLoading;
	private String placeOfDelivery;
	// description box
	private String marksNnumber;
	private String description;
	private int pacakgeNo;
	private String grossWeight;
	private String measurment;
	// ship on board
	private String shippingPortName;
	private String shippingPortCountry;
	private String departureDate;
	private String shippingAirlineName;
	private String shippingAirlineNo;
	private String transPort;
	// delivery
	private String delName1;
	private String delName2;
	private String delName3;
	private String delName4;
	private String delCity;
	private String delCountry;
	// company
	private String company;
	private String comAdd1;
	private String comAdd2;
	private String comAdd3;
	private String comAdd4;
	private String comCity;
	private String comCountry;
	// invoice
	private String invoiceNo;
	private String type;
	private String no;

	private String expNo;
	private String shippingBilNo;
	private String mvsl;
	private String mvsl1;

	// date
	private String date1;
	private String date2;
	private String date3;
	private String date4;
	// table
	private String containerNo;
	private String sealNo;
	private String size;
	private int qty;
	private String cbm;
	private String mode;
	private String kgs;
	//
	private String freightMode;
	private String placeOfissue;
	private String dateOfissue;
	private String date;
	private String ctnWords;

	public String getCtnWords() {
		return ctnWords;
	}

	public void setCtnWords(String ctnWords) {
		this.ctnWords = ctnWords;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getShipperName() {
		return shipperName;
	}

	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getShipperCity() {
		return shipperCity;
	}

	public void setShipperCity(String shipperCity) {
		this.shipperCity = shipperCity;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getMvsl1() {
		return mvsl1;
	}

	public void setMvsl1(String mvsl1) {
		this.mvsl1 = mvsl1;
	}

	public String getBillOfLadingNo() {
		return billOfLadingNo;
	}

	public void setBillOfLadingNo(String billOfLadingNo) {
		this.billOfLadingNo = billOfLadingNo;
	}

	public String getConsigneeName() {
		return consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneeAd1() {
		return consigneeAd1;
	}

	public void setConsigneeAd1(String consigneeAd1) {
		this.consigneeAd1 = consigneeAd1;
	}

	public String getConsigneeAd2() {
		return consigneeAd2;
	}

	public void setConsigneeAd2(String consigneeAd2) {
		this.consigneeAd2 = consigneeAd2;
	}

	public String getConsigneeAd3() {
		return consigneeAd3;
	}

	public void setConsigneeAd3(String consigneeAd3) {
		this.consigneeAd3 = consigneeAd3;
	}

	public String getConsigneeCountry() {
		return consigneeCountry;
	}

	public void setConsigneeCountry(String consigneeCountry) {
		this.consigneeCountry = consigneeCountry;
	}

	public String getNotify1() {
		return notify1;
	}

	public void setNotify1(String notify1) {
		this.notify1 = notify1;
	}

	public String getNotify2() {
		return notify2;
	}

	public void setNotify2(String notify2) {
		this.notify2 = notify2;
	}

	public String getNotify3() {
		return notify3;
	}

	public void setNotify3(String notify3) {
		this.notify3 = notify3;
	}

	public String getNotify4() {
		return notify4;
	}

	public void setNotify4(String notify4) {
		this.notify4 = notify4;
	}

	public String getNotifyCity() {
		return notifyCity;
	}

	public void setNotifyCity(String notifyCity) {
		this.notifyCity = notifyCity;
	}

	public String getNotifyCountry() {
		return notifyCountry;
	}

	public void setNotifyCountry(String notifyCountry) {
		this.notifyCountry = notifyCountry;
	}

	public String getPortOfDischarge() {
		return portOfDischarge;
	}

	public void setPortOfDischarge(String portOfDischarge) {
		this.portOfDischarge = portOfDischarge;
	}

	public String getPlaceOfReceipt() {
		return placeOfReceipt;
	}

	public void setPlaceOfReceipt(String placeOfReceipt) {
		this.placeOfReceipt = placeOfReceipt;
	}

	public String getPreCarrige() {
		return preCarrige;
	}

	public void setPreCarrige(String preCarrige) {
		this.preCarrige = preCarrige;
	}

	public String getVessel() {
		return vessel;
	}

	public void setVessel(String vessel) {
		this.vessel = vessel;
	}

	public String getPortOfLoading() {
		return portOfLoading;
	}

	public void setPortOfLoading(String portOfLoading) {
		this.portOfLoading = portOfLoading;
	}

	public String getPlaceOfDelivery() {
		return placeOfDelivery;
	}

	public void setPlaceOfDelivery(String placeOfDelivery) {
		this.placeOfDelivery = placeOfDelivery;
	}

	public String getMarksNnumber() {
		return marksNnumber;
	}

	public void setMarksNnumber(String marksNnumber) {
		this.marksNnumber = marksNnumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPacakgeNo() {
		return pacakgeNo;
	}

	public void setPacakgeNo(int pacakgeNo) {
		this.pacakgeNo = pacakgeNo;
	}

	public String getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(String grossWeight) {
		this.grossWeight = grossWeight;
	}

	public String getMeasurment() {
		return measurment;
	}

	public void setMeasurment(String measurment) {
		this.measurment = measurment;
	}

	public String getShippingPortName() {
		return shippingPortName;
	}

	public void setShippingPortName(String shippingPortName) {
		this.shippingPortName = shippingPortName;
	}

	public String getShippingPortCountry() {
		return shippingPortCountry;
	}

	public void setShippingPortCountry(String shippingPortCountry) {
		this.shippingPortCountry = shippingPortCountry;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getShippingAirlineName() {
		return shippingAirlineName;
	}

	public void setShippingAirlineName(String shippingAirlineName) {
		this.shippingAirlineName = shippingAirlineName;
	}

	public String getShippingAirlineNo() {
		return shippingAirlineNo;
	}

	public void setShippingAirlineNo(String shippingAirlineNo) {
		this.shippingAirlineNo = shippingAirlineNo;
	}

	public String getTransPort() {
		return transPort;
	}

	public void setTransPort(String transPort) {
		this.transPort = transPort;
	}

	public String getDelName1() {
		return delName1;
	}

	public void setDelName1(String delName1) {
		this.delName1 = delName1;
	}

	public String getDelName2() {
		return delName2;
	}

	public void setDelName2(String delName2) {
		this.delName2 = delName2;
	}

	public String getDelName3() {
		return delName3;
	}

	public void setDelName3(String delName3) {
		this.delName3 = delName3;
	}

	public String getDelName4() {
		return delName4;
	}

	public void setDelName4(String delName4) {
		this.delName4 = delName4;
	}

	public String getDelCity() {
		return delCity;
	}

	public void setDelCity(String delCity) {
		this.delCity = delCity;
	}

	public String getDelCountry() {
		return delCountry;
	}

	public void setDelCountry(String delCountry) {
		this.delCountry = delCountry;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComAdd1() {
		return comAdd1;
	}

	public void setComAdd1(String comAdd1) {
		this.comAdd1 = comAdd1;
	}

	public String getComAdd2() {
		return comAdd2;
	}

	public void setComAdd2(String comAdd2) {
		this.comAdd2 = comAdd2;
	}

	public String getComAdd3() {
		return comAdd3;
	}

	public void setComAdd3(String comAdd3) {
		this.comAdd3 = comAdd3;
	}

	public String getComAdd4() {
		return comAdd4;
	}

	public void setComAdd4(String comAdd4) {
		this.comAdd4 = comAdd4;
	}

	public String getComCity() {
		return comCity;
	}

	public void setComCity(String comCity) {
		this.comCity = comCity;
	}

	public String getComCountry() {
		return comCountry;
	}

	public void setComCountry(String comCountry) {
		this.comCountry = comCountry;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExpNo() {
		return expNo;
	}

	public void setExpNo(String expNo) {
		this.expNo = expNo;
	}

	public String getShippingBilNo() {
		return shippingBilNo;
	}

	public void setShippingBilNo(String shippingBilNo) {
		this.shippingBilNo = shippingBilNo;
	}

	public String getMvsl() {
		return mvsl;
	}

	public void setMvsl(String mvsl) {
		this.mvsl = mvsl;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getCongineeCity() {
		return congineeCity;
	}

	public void setCongineeCity(String congineeCity) {
		this.congineeCity = congineeCity;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public String getDate4() {
		return date4;
	}

	public void setDate4(String date) {
		this.date4 = date;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getSealNo() {
		return sealNo;
	}

	public void setSealNo(String sealNo) {
		this.sealNo = sealNo;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getCbm() {
		return cbm;
	}

	public void setCbm(String cbm) {
		this.cbm = cbm;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getKgs() {
		return kgs;
	}

	public void setKgs(String kgs) {
		this.kgs = kgs;
	}

	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	public String getPlaceOfissue() {
		return placeOfissue;
	}

	public void setPlaceOfissue(String placeOfissue) {
		this.placeOfissue = placeOfissue;
	}

	public String getDateOfissue() {
		return dateOfissue;
	}

	public void setDateOfissue(String dateOfissue) {
		this.dateOfissue = dateOfissue;
	}

}
