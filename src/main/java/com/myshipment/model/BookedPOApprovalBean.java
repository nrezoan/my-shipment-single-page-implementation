package com.myshipment.model;
/*
 * @ Hamid
 */
public class BookedPOApprovalBean {
	private String blno;
	private String pono;
	private String sono;
	private String item_bl_dt;
	private String item_no;
	private String style;
	private String size;
	private String color;
	private String item_pcs;
	private String item_qty;
	private String item_grwt;
	private String item_vol;
	private String item_chdt;
	private String buyerno;
	private String actionuser;
	private String shipperno;	
	private String buyername;
	private String shippername;	
	private String indcdt;
	private String poreqdt;
	private String fvsl;
	private String fetd;
	private String mvsl;
	private String meta;
	private String car;
	private String sta;
	private String rem;
	
	public String getBlno() {
		return blno;
	}
	public void setBlno(String blno) {
		this.blno = blno;
	}
	public String getPono() {
		return pono;
	}
	public void setPono(String pono) {
		this.pono = pono;
	}
	public String getSono() {
		return sono;
	}
	public void setSono(String sono) {
		this.sono = sono;
	}
	public String getItem_bl_dt() {
		return item_bl_dt;
	}
	public void setItem_bl_dt(String item_bl_dt) {
		this.item_bl_dt = item_bl_dt;
	}
	public String getItem_no() {
		return item_no;
	}
	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getItem_pcs() {
		return item_pcs;
	}
	public void setItem_pcs(String item_pcs) {
		this.item_pcs = item_pcs;
	}
	public String getItem_qty() {
		return item_qty;
	}
	public void setItem_qty(String item_qty) {
		this.item_qty = item_qty;
	}
	public String getItem_grwt() {
		return item_grwt;
	}
	public void setItem_grwt(String item_grwt) {
		this.item_grwt = item_grwt;
	}
	public String getItem_vol() {
		return item_vol;
	}
	public void setItem_vol(String item_vol) {
		this.item_vol = item_vol;
	}
	public String getItem_chdt() {
		return item_chdt;
	}
	public void setItem_chdt(String item_chdt) {
		this.item_chdt = item_chdt;
	}
	public String getBuyerno() {
		return buyerno;
	}
	public void setBuyerno(String buyerno) {
		this.buyerno = buyerno;
	}
	public String getActionuser() {
		return actionuser;
	}
	public void setActionuser(String actionuser) {
		this.actionuser = actionuser;
	}
	public String getIndcdt() {
		return indcdt;
	}
	public void setIndcdt(String indcdt) {
		this.indcdt = indcdt;
	}
	public String getPoreqdt() {
		return poreqdt;
	}
	public void setPoreqdt(String poreqdt) {
		this.poreqdt = poreqdt;
	}
	public String getFvsl() {
		return fvsl;
	}
	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}
	public String getFetd() {
		return fetd;
	}
	public void setFetd(String fetd) {
		this.fetd = fetd;
	}
	public String getMvsl() {
		return mvsl;
	}
	public void setMvsl(String mvsl) {
		this.mvsl = mvsl;
	}
	public String getMeta() {
		return meta;
	}
	public void setMeta(String meta) {
		this.meta = meta;
	}
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public String getSta() {
		return sta;
	}
	public void setSta(String sta) {
		this.sta = sta;
	}
	public String getRem() {
		return rem;
	}
	public void setRem(String rem) {
		this.rem = rem;
	}
	public String getShipperno() {
		return shipperno;
	}
	public void setShipperno(String shipperno) {
		this.shipperno = shipperno;
	}
	public String getBuyername() {
		return buyername;
	}
	public void setBuyername(String buyername) {
		this.buyername = buyername;
	}
	public String getShippername() {
		return shippername;
	}
	public void setShippername(String shippername) {
		this.shippername = shippername;
	}	
}
