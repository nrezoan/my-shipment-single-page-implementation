package com.myshipment.model;

import java.util.Date;

public class ItVbak {

	private String vbeln;
	private String bstnk;
	private String comp_code;
	private Date zzCargoHandOverDt;
	private String zzPortOfFinalDst;
	private String zzPlaceOfReceipt;
	private String zzPortOfLoading;
	private String zzPlaceOfDelivery;
	private String zzAirlineName1;
	private String zzAirlinerName2;
	private String zzAirLineNo1;
	private String zzAirlineNo2;
	private String zzRotation1;
	private Date zzDepartureDt;
	private Date zzEtaFinalDest;
	private Date zzTrans1Etd;
	private Date zzTrans2Eta;
	private Date zzTrans2Etd;
	private String destCountry;
	private String origCountry;
	private String orderType;

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getDestCountry() {
		return destCountry;
	}

	public void setDestCountry(String destCountry) {
		this.destCountry = destCountry;
	}

	public String getOrigCountry() {
		return origCountry;
	}

	public void setOrigCountry(String origCountry) {
		this.origCountry = origCountry;
	}

	public String getVbeln() {
		return vbeln;
	}

	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}

	public String getBstnk() {
		return bstnk;
	}

	public void setBstnk(String bstnk) {
		this.bstnk = bstnk;
	}

	public String getComp_code() {
		return comp_code;
	}

	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}

	public Date getZzCargoHandOverDt() {
		return zzCargoHandOverDt;
	}

	public void setZzCargoHandOverDt(Date zzCargoHandOverDt) {
		this.zzCargoHandOverDt = zzCargoHandOverDt;
	}

	public String getZzPortOfFinalDst() {
		return zzPortOfFinalDst;
	}

	public void setZzPortOfFinalDst(String zzPortOfFinalDst) {
		this.zzPortOfFinalDst = zzPortOfFinalDst;
	}

	public String getZzPlaceOfReceipt() {
		return zzPlaceOfReceipt;
	}

	public void setZzPlaceOfReceipt(String zzPlaceOfReceipt) {
		this.zzPlaceOfReceipt = zzPlaceOfReceipt;
	}

	public String getZzPortOfLoading() {
		return zzPortOfLoading;
	}

	public void setZzPortOfLoading(String zzPortOfLoading) {
		this.zzPortOfLoading = zzPortOfLoading;
	}

	public String getZzPlaceOfDelivery() {
		return zzPlaceOfDelivery;
	}

	public void setZzPlaceOfDelivery(String zzPlaceOfDelivery) {
		this.zzPlaceOfDelivery = zzPlaceOfDelivery;
	}

	public String getZzAirlineName1() {
		return zzAirlineName1;
	}

	public void setZzAirlineName1(String zzAirlineName1) {
		this.zzAirlineName1 = zzAirlineName1;
	}

	public String getZzAirlinerName2() {
		return zzAirlinerName2;
	}

	public void setZzAirlinerName2(String zzAirlinerName2) {
		this.zzAirlinerName2 = zzAirlinerName2;
	}

	public String getZzAirLineNo1() {
		return zzAirLineNo1;
	}

	public void setZzAirLineNo1(String zzAirLineNo1) {
		this.zzAirLineNo1 = zzAirLineNo1;
	}

	public String getZzAirlineNo2() {
		return zzAirlineNo2;
	}

	public void setZzAirlineNo2(String zzAirlineNo2) {
		this.zzAirlineNo2 = zzAirlineNo2;
	}

	public String getZzRotation1() {
		return zzRotation1;
	}

	public void setZzRotation1(String zzRotation1) {
		this.zzRotation1 = zzRotation1;
	}

	public Date getZzDepartureDt() {
		return zzDepartureDt;
	}

	public void setZzDepartureDt(Date zzDepartureDt) {
		this.zzDepartureDt = zzDepartureDt;
	}

	public Date getZzEtaFinalDest() {
		return zzEtaFinalDest;
	}

	public void setZzEtaFinalDest(Date zzEtaFinalDest) {
		this.zzEtaFinalDest = zzEtaFinalDest;
	}

	public Date getZzTrans1Etd() {
		return zzTrans1Etd;
	}

	public void setZzTrans1Etd(Date zzTrans1Etd) {
		this.zzTrans1Etd = zzTrans1Etd;
	}

	public Date getZzTrans2Eta() {
		return zzTrans2Eta;
	}

	public void setZzTrans2Eta(Date zzTrans2Eta) {
		this.zzTrans2Eta = zzTrans2Eta;
	}

	public Date getZzTrans2Etd() {
		return zzTrans2Etd;
	}

	public void setZzTrans2Etd(Date zzTrans2Etd) {
		this.zzTrans2Etd = zzTrans2Etd;
	}

}
