package com.myshipment.model;

import java.util.List;

public class OAGResponseDetails {
	
	private List<OAG> oagResponse;
	private OAG inAirFlightDetails;

	public List<OAG> getOagResponse() {
		return oagResponse;
	}

	public void setOagResponse(List<OAG> oagResponse) {
		this.oagResponse = oagResponse;
	}

	public OAG getInAirFlightDetails() {
		return inAirFlightDetails;
	}

	public void setInAirFlightDetails(OAG inAirFlightDetails) {
		this.inAirFlightDetails = inAirFlightDetails;
	}

}
