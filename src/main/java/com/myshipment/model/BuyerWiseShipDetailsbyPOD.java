package com.myshipment.model;

import java.util.List;

public class BuyerWiseShipDetailsbyPOD {
	private List<SOWiseShipDetailsBuyerJson> lstSOWiseShipDetailsBuyerJson;
	private String message;
	
	public List<SOWiseShipDetailsBuyerJson> getLstSOWiseShipDetailsBuyerJson() {
		return lstSOWiseShipDetailsBuyerJson;
	}
	public void setLstSOWiseShipDetailsBuyerJson(List<SOWiseShipDetailsBuyerJson> lstSOWiseShipDetailsBuyerJson) {
		this.lstSOWiseShipDetailsBuyerJson = lstSOWiseShipDetailsBuyerJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
