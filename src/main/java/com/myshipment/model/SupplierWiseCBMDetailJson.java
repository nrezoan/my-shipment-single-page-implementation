package com.myshipment.model;

import java.util.List;

public class SupplierWiseCBMDetailJson {
	private List<SupplierWiseCBMJson> lstSupplierWiseCBMJson;
	private String message;
	public List<SupplierWiseCBMJson> getLstSupplierWiseCBMJson() {
		return lstSupplierWiseCBMJson;
	}
	public void setLstSupplierWiseCBMJson(List<SupplierWiseCBMJson> lstSupplierWiseCBMJson) {
		this.lstSupplierWiseCBMJson = lstSupplierWiseCBMJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	} 
}
