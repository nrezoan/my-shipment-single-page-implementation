package com.myshipment.model;

import java.util.List;

import com.myshipment.dto.MyshipmentTrackContainerBeanDto;
/*
 * @Ranjeet Kumar
 */
public class SeaExportHBLData {
		
	private List<MyshipmentTrackHeaderBean> myshipmentTrackHeader;		
	private List<MyshipmentTrackItemBean> myshipmentTrackItem;		
	private List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule;
	private List<MyshipmentTrackContainerBeanDto> myshipmentTrackContainer;
	
	public List<MyshipmentTrackHeaderBean> getMyshipmentTrackHeader() {
		return myshipmentTrackHeader;
	}
	public void setMyshipmentTrackHeader(
			List<MyshipmentTrackHeaderBean> myshipmentTrackHeader) {
		this.myshipmentTrackHeader = myshipmentTrackHeader;
	}
	public List<MyshipmentTrackItemBean> getMyshipmentTrackItem() {
		return myshipmentTrackItem;
	}
	public void setMyshipmentTrackItem(
			List<MyshipmentTrackItemBean> myshipmentTrackItem) {
		this.myshipmentTrackItem = myshipmentTrackItem;
	}
	public List<MyshipmentTrackScheduleBean> getMyshipmentTrackSchedule() {
		return myshipmentTrackSchedule;
	}
	public void setMyshipmentTrackSchedule(
			List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule) {
		this.myshipmentTrackSchedule = myshipmentTrackSchedule;
	}
	public List<MyshipmentTrackContainerBeanDto> getMyshipmentTrackContainer() {
		return myshipmentTrackContainer;
	}
	public void setMyshipmentTrackContainer(
			List<MyshipmentTrackContainerBeanDto> myshipmentTrackContainer) {
		this.myshipmentTrackContainer = myshipmentTrackContainer;
	}
	
	
}
