package com.myshipment.model;
/*
 * @ Gufranur Rahman
 */


public class StoreLocListBean {
	
	private String lgOrt;
	private String lgObe;
	
	public String getLgOrt() {
		return lgOrt;
	}
	public void setLgOrt(String lgOrt) {
		this.lgOrt = lgOrt;
	}
	public String getLgObe() {
		return lgObe;
	}
	public void setLgObe(String lgObe) {
		this.lgObe = lgObe;
	}
	@Override
	public String toString() {
		return "StoreLocListBean [lgOrt=" + lgOrt + ", lgObe=" + lgObe + "]";
	}
	

	

	
}
