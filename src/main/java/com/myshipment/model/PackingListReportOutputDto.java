package com.myshipment.model;

import java.util.List;

public class PackingListReportOutputDto {
	private static final long serialVersionUID=-3214325253466546L;
	private List<PackListReportMainPage> lstPackListReportMainPage;
	private PackListReportTrailPage packListReportTrailPage;
	public List<PackListReportMainPage> getLstPackListReportMainPage() {
		return lstPackListReportMainPage;
	}
	public void setLstPackListReportMainPage(List<PackListReportMainPage> lstPackListReportMainPage) {
		this.lstPackListReportMainPage = lstPackListReportMainPage;
	}
	public PackListReportTrailPage getPackListReportTrailPage() {
		return packListReportTrailPage;
	}
	public void setPackListReportTrailPage(PackListReportTrailPage packListReportTrailPage) {
		this.packListReportTrailPage = packListReportTrailPage;
	}
	

}
