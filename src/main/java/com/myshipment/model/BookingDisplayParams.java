package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class BookingDisplayParams {

	private String salesOrderNumber;

	public String getSalesOrderNumber() {
		return salesOrderNumber;
	}

	public void setSalesOrderNumber(String salesOrderNumber) {
		this.salesOrderNumber = salesOrderNumber;
	}
	
	
}
