package com.myshipment.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
/*
 * @Ranjeet Kumar
 */
public class MblTrackingParams implements Serializable{

	
	private String customer;	
	private String salesOrg;	
	private String distChan;	
	private String division;
	private String fromDate;
	private String toDate;
	private String action;
	private String doc_no;	
	private String statusType;
	private String expStatusType;
	private String shipper_no;
	private String buyer_no;
	private String agent_no;
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDistChan() {
		return distChan;
	}
	public void setDistChan(String distChan) {
		this.distChan = distChan;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getDoc_no() {
		return doc_no;
	}
	public void setDoc_no(String doc_no) {
		this.doc_no = doc_no;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	public String getExpStatusType() {
		return expStatusType;
	}
	public void setExpStatusType(String expStatusType) {
		this.expStatusType = expStatusType;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	public String getAgent_no() {
		return agent_no;
	}
	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}
	
	
	
	
	/*
	 * 
	 * 
	 * Shammi
	 * 
	 * private static final long serialVersionUID = 1L;

	private String mblNo;
	private String fromDate;
	private String toDate;
	private String buyer;
	private Map<String, String> buyersLst;
	private List<String> buyerLst;
	
	public String getMblNo() {
		return mblNo;
	}
	public void setMblNo(String mblNo) {
		this.mblNo = mblNo;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getBuyer() {
		return buyer;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public Map<String, String> getBuyersLst() {
		return buyersLst;
	}
	public void setBuyersLst(Map<String, String> buyersLst) {
		this.buyersLst = buyersLst;
	}
	public List<String> getBuyerLst() {
		return buyerLst;
	}
	public void setBuyerLst(List<String> buyerLst) {
		this.buyerLst = buyerLst;
	}*/
	
	
	
}
