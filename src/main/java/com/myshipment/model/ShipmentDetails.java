package com.myshipment.model;

public class ShipmentDetails {
	private String blNo;
	private String bldate;
	private String bookingDate;
	private String shipperNo;
	private String shipperName;
	private String polCode;
	private String podCode;
	private double grossWeight;
	private double chargeWeight;
	private double totVolume;
	private String grDate;
	private String shipmentDate;
	private String buyerNo;
	private String buyerName;
	public String getBlNo() {
		return blNo;
	}
	public void setBlNo(String blNo) {
		this.blNo = blNo;
	}
	public String getBldate() {
		return bldate;
	}
	public void setBldate(String bldate) {
		this.bldate = bldate;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	public String getPolCode() {
		return polCode;
	}
	public void setPolCode(String polCode) {
		this.polCode = polCode;
	}
	public String getPodCode() {
		return podCode;
	}
	public void setPodCode(String podCode) {
		this.podCode = podCode;
	}
	public double getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(double grossWeight) {
		this.grossWeight = grossWeight;
	}
	public double getChargeWeight() {
		return chargeWeight;
	}
	public void setChargeWeight(double chargeWeight) {
		this.chargeWeight = chargeWeight;
	}
	public double getTotVolume() {
		return totVolume;
	}
	public void setTotVolume(double totVolume) {
		this.totVolume = totVolume;
	}
	public String getGrDate() {
		return grDate;
	}
	public void setGrDate(String grDate) {
		this.grDate = grDate;
	}
	public String getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(String shipmentDate) {
		this.shipmentDate = shipmentDate;
	}
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	@Override
	public String toString() {
		return "ShipmentDetails [blNo=" + blNo + ", bldate=" + bldate + ", bookingDate=" + bookingDate + ", shipperNo="
				+ shipperNo + ", shipperName=" + shipperName + ", polCode=" + polCode + ", podCode=" + podCode
				+ ", grossWeight=" + grossWeight + ", chargeWeight=" + chargeWeight + ", totVolume=" + totVolume
				+ ", grDate=" + grDate + ", shipmentDate=" + shipmentDate + ", buyerNo=" + buyerNo + ", buyerName="
				+ buyerName + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blNo == null) ? 0 : blNo.hashCode());
		result = prime * result + ((bldate == null) ? 0 : bldate.hashCode());
		result = prime * result + ((bookingDate == null) ? 0 : bookingDate.hashCode());
		result = prime * result + ((buyerName == null) ? 0 : buyerName.hashCode());
		result = prime * result + ((buyerNo == null) ? 0 : buyerNo.hashCode());
		long temp;
		temp = Double.doubleToLongBits(chargeWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((grDate == null) ? 0 : grDate.hashCode());
		temp = Double.doubleToLongBits(grossWeight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((podCode == null) ? 0 : podCode.hashCode());
		result = prime * result + ((polCode == null) ? 0 : polCode.hashCode());
		result = prime * result + ((shipmentDate == null) ? 0 : shipmentDate.hashCode());
		result = prime * result + ((shipperName == null) ? 0 : shipperName.hashCode());
		result = prime * result + ((shipperNo == null) ? 0 : shipperNo.hashCode());
		temp = Double.doubleToLongBits(totVolume);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipmentDetails other = (ShipmentDetails) obj;
		if (blNo == null) {
			if (other.blNo != null)
				return false;
		} else if (!blNo.equals(other.blNo))
			return false;
		if (bldate == null) {
			if (other.bldate != null)
				return false;
		} else if (!bldate.equals(other.bldate))
			return false;
		if (bookingDate == null) {
			if (other.bookingDate != null)
				return false;
		} else if (!bookingDate.equals(other.bookingDate))
			return false;
		if (buyerName == null) {
			if (other.buyerName != null)
				return false;
		} else if (!buyerName.equals(other.buyerName))
			return false;
		if (buyerNo == null) {
			if (other.buyerNo != null)
				return false;
		} else if (!buyerNo.equals(other.buyerNo))
			return false;
		if (Double.doubleToLongBits(chargeWeight) != Double.doubleToLongBits(other.chargeWeight))
			return false;
		if (grDate == null) {
			if (other.grDate != null)
				return false;
		} else if (!grDate.equals(other.grDate))
			return false;
		if (Double.doubleToLongBits(grossWeight) != Double.doubleToLongBits(other.grossWeight))
			return false;
		if (podCode == null) {
			if (other.podCode != null)
				return false;
		} else if (!podCode.equals(other.podCode))
			return false;
		if (polCode == null) {
			if (other.polCode != null)
				return false;
		} else if (!polCode.equals(other.polCode))
			return false;
		if (shipmentDate == null) {
			if (other.shipmentDate != null)
				return false;
		} else if (!shipmentDate.equals(other.shipmentDate))
			return false;
		if (shipperName == null) {
			if (other.shipperName != null)
				return false;
		} else if (!shipperName.equals(other.shipperName))
			return false;
		if (shipperNo == null) {
			if (other.shipperNo != null)
				return false;
		} else if (!shipperNo.equals(other.shipperNo))
			return false;
		if (Double.doubleToLongBits(totVolume) != Double.doubleToLongBits(other.totVolume))
			return false;
		return true;
	}
	

}
