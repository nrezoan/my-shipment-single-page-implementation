package com.myshipment.model;

import java.util.Date;


/*
 * @Mohammad Salahuddin
 */
public class VesselTrackingBean {

	private String bl_no;
	private double mmsi;
	private String destination;
	private double last_port_id;
	private String last_port;
	private String last_port_unlocode;
	private Date last_port_time;
	private double next_port_id;
	private String next_port_name;
	private String next_port_unlocode;
	private Date eta;
	private Date eta_calc;
	private double distance_travelled;
	private double distance_to_go;
	private double speed_calc;
	private double draught;
	private double draught_max;
	private String load_status_name;
	private String route;
	private VesselBean vesselBean;
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public double getMmsi() {
		return mmsi;
	}
	public void setMmsi(double mmsi) {
		this.mmsi = mmsi;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public double getLast_port_id() {
		return last_port_id;
	}
	public void setLast_port_id(double last_port_id) {
		this.last_port_id = last_port_id;
	}
	public String getLast_port() {
		return last_port;
	}
	public void setLast_port(String last_port) {
		this.last_port = last_port;
	}
	public String getLast_port_unlocode() {
		return last_port_unlocode;
	}
	public void setLast_port_unlocode(String last_port_unlocode) {
		this.last_port_unlocode = last_port_unlocode;
	}
	public Date getLast_port_time() {
		return last_port_time;
	}
	public void setLast_port_time(Date last_port_time) {
		this.last_port_time = last_port_time;
	}
	public double getNext_port_id() {
		return next_port_id;
	}
	public void setNext_port_id(double next_port_id) {
		this.next_port_id = next_port_id;
	}
	public String getNext_port_name() {
		return next_port_name;
	}
	public void setNext_port_name(String next_port_name) {
		this.next_port_name = next_port_name;
	}
	public String getNext_port_unlocode() {
		return next_port_unlocode;
	}
	public void setNext_port_unlocode(String next_port_unlocode) {
		this.next_port_unlocode = next_port_unlocode;
	}
	public Date getEta() {
		return eta;
	}
	public void setEta(Date eta) {
		this.eta = eta;
	}
	public Date getEta_calc() {
		return eta_calc;
	}
	public void setEta_calc(Date eta_calc) {
		this.eta_calc = eta_calc;
	}
	public double getDistance_travelled() {
		return distance_travelled;
	}
	public void setDistance_travelled(double distance_travelled) {
		this.distance_travelled = distance_travelled;
	}
	public double getDistance_to_go() {
		return distance_to_go;
	}
	public void setDistance_to_go(double distance_to_go) {
		this.distance_to_go = distance_to_go;
	}
	public double getSpeed_calc() {
		return speed_calc;
	}
	public void setSpeed_calc(double speed_calc) {
		this.speed_calc = speed_calc;
	}
	public double getDraught() {
		return draught;
	}
	public void setDraught(double draught) {
		this.draught = draught;
	}
	public double getDraught_max() {
		return draught_max;
	}
	public void setDraught_max(double draught_max) {
		this.draught_max = draught_max;
	}
	public String getLoad_status_name() {
		return load_status_name;
	}
	public void setLoad_status_name(String load_status_name) {
		this.load_status_name = load_status_name;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public VesselBean getVesselBean() {
		return vesselBean;
	}
	public void setVesselBean(VesselBean vesselBean) {
		this.vesselBean = vesselBean;
	}
	
	
	
	
}
