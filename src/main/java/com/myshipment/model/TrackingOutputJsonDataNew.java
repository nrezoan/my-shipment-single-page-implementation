package com.myshipment.model;

import java.io.Serializable;
import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class TrackingOutputJsonDataNew implements Serializable{


	private static final long serialVersionUID = 1L;

	List<ItDocumentHeaderBean> documentHeaderLst;
	List<ItDocumentDetailBean> documentDetailLst;
	private String response;
	public List<ItDocumentHeaderBean> getDocumentHeaderLst() {
		return documentHeaderLst;
	}
	public void setDocumentHeaderLst(List<ItDocumentHeaderBean> documentHeaderLst) {
		this.documentHeaderLst = documentHeaderLst;
	}
	public List<ItDocumentDetailBean> getDocumentDetailLst() {
		return documentDetailLst;
	}
	public void setDocumentDetailLst(List<ItDocumentDetailBean> documentDetailLst) {
		this.documentDetailLst = documentDetailLst;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}

	
}
