package com.myshipment.model;

import java.util.Date;

/*
 * @Mohammad Salahuddin
 */
/**
 * @author salahuddin
 *
 */
public class PoTrackingDetailResultBean {

	private String po_no;
	private String document_no;
	private String bl_no;
	private String booking_date;
	private String bl_date;
	private String shipper_name;
	private String buyer_name;
	private String shipper_no;
	private String buyer_no;
	private String agent_name;
	private String carrier_name;
	private String pol_name;
	private String pod_name;
	private String place_of_discharge;
	private String discharge_port_name;
	private String comm_invoice_no;
	private Date comm_invoice_date;
	private String lc_tt_po_no;
	private Date lc_tt_po_date;
	private String freight_mode;
	private double tot_qty;
	private double tot_pcs;
	private double net_wt;
	private Date doc_rcv_date;
	private Date bl_release_date;
	private Date noc_date;
	private Date fvsletd;
	private Date fvsleta;
	private Date mvsletd;
	private Date mvsleta;
	private String material_desc;
	private String style_no;
	private String sku_no;
	private String article_no;
	private String indent_no;
	private String hs_code;
	private String color;
	private String size1;
	private String pol_code;
	private String pod_code;
	private String tship_port_code;
	private Double gross_wt;
	private Double charge_wt;
	private double cbm;
	private Double tot_volume;
	private String gr_date;
	private String shipment_date;
	private String container_no;
	private String container_type;
	private String poStatus;
	private String shipmentStatus;
	private String sales_org;
	private String fvslName;
	private String mvslname;
	
	private Date etd;
	private Date eta;
	private Date fetd;
	private Date feta;
	private Date atd;
	private Date ata;
	
	private String chdt;	//hamid

	private String unitOfWeight;
	private String length;
	private String width;
	private String height;
	private String product_code;
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	public String getDocument_no() {
		return document_no;
	}
	public void setDocument_no(String document_no) {
		this.document_no = document_no;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public String getBl_date() {
		return bl_date;
	}
	public void setBl_date(String bl_date) {
		this.bl_date = bl_date;
	}
	public String getShipper_name() {
		return shipper_name;
	}
	public void setShipper_name(String shipper_name) {
		this.shipper_name = shipper_name;
	}
	
	public String getBuyer_name() {
		return buyer_name;
	}
	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getPol_name() {
		return pol_name;
	}
	public void setPol_name(String pol_name) {
		this.pol_name = pol_name;
	}
	public String getPod_name() {
		return pod_name;
	}
	public void setPod_name(String pod_name) {
		this.pod_name = pod_name;
	}
	public String getPlace_of_discharge() {
		return place_of_discharge;
	}
	public void setPlace_of_discharge(String place_of_discharge) {
		this.place_of_discharge = place_of_discharge;
	}
	public String getDischarge_port_name() {
		return discharge_port_name;
	}
	public void setDischarge_port_name(String discharge_port_name) {
		this.discharge_port_name = discharge_port_name;
	}
	public String getComm_invoice_no() {
		return comm_invoice_no;
	}
	public void setComm_invoice_no(String comm_invoice_no) {
		this.comm_invoice_no = comm_invoice_no;
	}
	public Date getComm_invoice_date() {
		return comm_invoice_date;
	}
	public void setComm_invoice_date(Date comm_invoice_date) {
		this.comm_invoice_date = comm_invoice_date;
	}
	public String getLc_tt_po_no() {
		return lc_tt_po_no;
	}
	public void setLc_tt_po_no(String lc_tt_po_no) {
		this.lc_tt_po_no = lc_tt_po_no;
	}
	public Date getLc_tt_po_date() {
		return lc_tt_po_date;
	}
	public void setLc_tt_po_date(Date lc_tt_po_date) {
		this.lc_tt_po_date = lc_tt_po_date;
	}
	public String getFreight_mode() {
		return freight_mode;
	}
	public void setFreight_mode(String freight_mode) {
		this.freight_mode = freight_mode;
	}
	public double getTot_qty() {
		return tot_qty;
	}
	public void setTot_qty(double tot_qty) {
		this.tot_qty = tot_qty;
	}
	public double getTot_pcs() {
		return tot_pcs;
	}
	public void setTot_pcs(double tot_pcs) {
		this.tot_pcs = tot_pcs;
	}
	public double getNet_wt() {
		return net_wt;
	}
	public void setNet_wt(double net_wt) {
		this.net_wt = net_wt;
	}
	public Date getDoc_rcv_date() {
		return doc_rcv_date;
	}
	public void setDoc_rcv_date(Date doc_rcv_date) {
		this.doc_rcv_date = doc_rcv_date;
	}
	public Date getBl_release_date() {
		return bl_release_date;
	}
	public void setBl_release_date(Date bl_release_date) {
		this.bl_release_date = bl_release_date;
	}
	public Date getNoc_date() {
		return noc_date;
	}
	public void setNoc_date(Date noc_date) {
		this.noc_date = noc_date;
	}
	
	public String getMaterial_desc() {
		return material_desc;
	}
	public void setMaterial_desc(String material_desc) {
		this.material_desc = material_desc;
	}
	public String getStyle_no() {
		return style_no;
	}
	public void setStyle_no(String style_no) {
		this.style_no = style_no;
	}
	public String getSku_no() {
		return sku_no;
	}
	public void setSku_no(String sku_no) {
		this.sku_no = sku_no;
	}
	public String getArticle_no() {
		return article_no;
	}
	public void setArticle_no(String article_no) {
		this.article_no = article_no;
	}
	public String getIndent_no() {
		return indent_no;
	}
	public void setIndent_no(String indent_no) {
		this.indent_no = indent_no;
	}
	public String getHs_code() {
		return hs_code;
	}
	public void setHs_code(String hs_code) {
		this.hs_code = hs_code;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize1() {
		return size1;
	}
	public void setSize1(String size1) {
		this.size1 = size1;
	}
	public String getPol_code() {
		return pol_code;
	}
	public void setPol_code(String pol_code) {
		this.pol_code = pol_code;
	}
	public String getPod_code() {
		return pod_code;
	}
	public void setPod_code(String pod_code) {
		this.pod_code = pod_code;
	}
	public Double getGross_wt() {
		return gross_wt;
	}
	public void setGross_wt(Double gross_wt) {
		this.gross_wt = gross_wt;
	}
	public Double getCharge_wt() {
		return charge_wt;
	}
	public void setCharge_wt(Double charge_wt) {
		this.charge_wt = charge_wt;
	}
	public Double getTot_volume() {
		return tot_volume;
	}
	public void setTot_volume(Double tot_volume) {
		this.tot_volume = tot_volume;
	}
	public String getGr_date() {
		return gr_date;
	}
	public void setGr_date(String gr_date) {
		this.gr_date = gr_date;
	}
	public String getShipment_date() {
		return shipment_date;
	}
	public void setShipment_date(String shipment_date) {
		this.shipment_date = shipment_date;
	}
	public String getContainer_no() {
		return container_no;
	}
	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}
	public String getContainer_type() {
		return container_type;
	}
	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}
	public String getPoStatus() {
		return poStatus;
	}
	public void setPoStatus(String poStatus) {
		this.poStatus = poStatus;
	}
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	public Date getFvsletd() {
		return fvsletd;
	}
	public void setFvsletd(Date fvsletd) {
		this.fvsletd = fvsletd;
	}
	public Date getFvsleta() {
		return fvsleta;
	}
	public void setFvsleta(Date fvsleta) {
		this.fvsleta = fvsleta;
	}
	public Date getMvsletd() {
		return mvsletd;
	}
	public void setMvsletd(Date mvsletd) {
		this.mvsletd = mvsletd;
	}
	public Date getMvsleta() {
		return mvsleta;
	}
	public void setMvsleta(Date mvsleta) {
		this.mvsleta = mvsleta;
	}
	public String getTship_port_code() {
		return tship_port_code;
	}
	public void setTship_port_code(String tship_port_code) {
		this.tship_port_code = tship_port_code;
	}
	public double getCbm() {
		return cbm;
	}
	public void setCbm(double cbm) {
		this.cbm = cbm;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getFvslName() {
		return fvslName;
	}
	public void setFvslName(String fvslName) {
		this.fvslName = fvslName;
	}
	public String getMvslname() {
		return mvslname;
	}
	public void setMvslname(String mvslname) {
		this.mvslname = mvslname;
	}
	
	public Date getEtd() {
		return etd;
	}
	public void setEtd(Date etd) {
		this.etd = etd;
	}
	public Date getEta() {
		return eta;
	}
	public void setEta(Date eta) {
		this.eta = eta;
	}
	public Date getFetd() {
		return fetd;
	}
	public void setFetd(Date fetd) {
		this.fetd = fetd;
	}
	public Date getFeta() {
		return feta;
	}
	public void setFeta(Date feta) {
		this.feta = feta;
	}
	public Date getAtd() {
		return atd;
	}
	public void setAtd(Date atd) {
		this.atd = atd;
	}
	public Date getAta() {
		return ata;
	}
	public void setAta(Date ata) {
		this.ata = ata;
	}
	public String getUnitOfWeight() {
		return unitOfWeight;
	}
	public void setUnitOfWeight(String unitOfWeight) {
		this.unitOfWeight = unitOfWeight;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getHeight() {
		return height;
	}
	public void setHeight(String height) {
		this.height = height;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	//hamid
	public String getChdt() {
		return chdt;
	}
	public void setChdt(String chdt) {
		this.chdt = chdt;
	}
	
	
	
}
