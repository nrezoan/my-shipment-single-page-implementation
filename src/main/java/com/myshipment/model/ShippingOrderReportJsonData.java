package com.myshipment.model;

import java.util.List;

public class ShippingOrderReportJsonData {

	
	private BapiRet BapiRet1;
	private String itPortLoad;
	private String itPortDest;
	private String itPortFiDest;
	private String itPortPlDel;
	private List<ShippingReport> itShipperList;
	private List<ShippingReport> itCosigneeList;
	private List<ShippingReport> itPickList;
	private List<ShippingReport> itFooterList;
	private List<ShippingReport> itAgentList;
	private List<ShippingReport> itSlineList;
	private List<ShippingReport> itAddList;
	private List<ItVbak> itVbak;
	private List<ItVbap> itVbap;
	public BapiRet getBapiRet1() {
		return BapiRet1;
	}
	public void setBapiRet1(BapiRet bapiRet1) {
		BapiRet1 = bapiRet1;
	}
	public String getItPortLoad() {
		return itPortLoad;
	}
	public void setItPortLoad(String itPortLoad) {
		this.itPortLoad = itPortLoad;
	}
	public String getItPortDest() {
		return itPortDest;
	}
	public void setItPortDest(String itPortDest) {
		this.itPortDest = itPortDest;
	}
	public String getItPortFiDest() {
		return itPortFiDest;
	}
	public void setItPortFiDest(String itPortFiDest) {
		this.itPortFiDest = itPortFiDest;
	}
	public String getItPortPlDel() {
		return itPortPlDel;
	}
	public void setItPortPlDel(String itPortPlDel) {
		this.itPortPlDel = itPortPlDel;
	}
	public List<ShippingReport> getItShipperList() {
		return itShipperList;
	}
	public void setItShipperList(List<ShippingReport> itShipperList) {
		this.itShipperList = itShipperList;
	}
	public List<ShippingReport> getItCosigneeList() {
		return itCosigneeList;
	}
	public void setItCosigneeList(List<ShippingReport> itCosigneeList) {
		this.itCosigneeList = itCosigneeList;
	}
	
	
	public List<ShippingReport> getItPickList() {
		return itPickList;
	}
	public void setItPickList(List<ShippingReport> itPickList) {
		this.itPickList = itPickList;
	}
	public List<ShippingReport> getItFooterList() {
		return itFooterList;
	}
	public void setItFooterList(List<ShippingReport> itFooterList) {
		this.itFooterList = itFooterList;
	}
	public List<ShippingReport> getItAgentList() {
		return itAgentList;
	}
	public void setItAgentList(List<ShippingReport> itAgentList) {
		this.itAgentList = itAgentList;
	}
	public List<ShippingReport> getItSlineList() {
		return itSlineList;
	}
	public void setItSlineList(List<ShippingReport> itSlineList) {
		this.itSlineList = itSlineList;
	}
	public List<ShippingReport> getItAddList() {
		return itAddList;
	}
	public void setItAddList(List<ShippingReport> itAddList) {
		this.itAddList = itAddList;
	}
	public List<ItVbak> getItVbak() {
		return itVbak;
	}
	public void setItVbak(List<ItVbak> itVbak) {
		this.itVbak = itVbak;
	}
	public List<ItVbap> getItVbap() {
		return itVbap;
	}
	public void setItVbap(List<ItVbap> itVbap) {
		this.itVbap = itVbap;
	}
	
	
			
}
