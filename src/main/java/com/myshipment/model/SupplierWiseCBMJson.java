package com.myshipment.model;

public class SupplierWiseCBMJson {
	private String shipperNo;
	private String shipperName;
	private double totalCBM;
	private double totalCBMPerc;
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	public double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public double getTotalCBMPerc() {
		return totalCBMPerc;
	}
	public void setTotalCBMPerc(double totalCBMPerc) {
		this.totalCBMPerc = totalCBMPerc;
	}
	

}
