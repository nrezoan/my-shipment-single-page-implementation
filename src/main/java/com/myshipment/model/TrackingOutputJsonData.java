package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class TrackingOutputJsonData {

	List<BapiTrackDTO> bapiTrackDTOLst;

	public List<BapiTrackDTO> getBapiTrackDTOLst() {
		return bapiTrackDTOLst;
	}

	public void setBapiTrackDTOLst(List<BapiTrackDTO> bapiTrackDTOLst) {
		this.bapiTrackDTOLst = bapiTrackDTOLst;
	}
	
	
}
