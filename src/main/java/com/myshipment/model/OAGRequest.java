package com.myshipment.model;

import java.util.List;

public class OAGRequest {

	/*
	 * private String flightNo; private String date;
	 * 
	 * public String getFlightNo() { return flightNo; }
	 * 
	 * public String getDate() { return date; }
	 * 
	 * public void setFlightNo(String flightNo) { this.flightNo = flightNo; }
	 * 
	 * public void setDate(String date) { this.date = date; }
	 */

	private List<FlightParamDetails> flightParamDetailsList;

	public List<FlightParamDetails> getFlightParamDetailsList() {
		return flightParamDetailsList;
	}

	public void setFlightParamDetailsList(List<FlightParamDetails> flightParamDetailsList) {
		this.flightParamDetailsList = flightParamDetailsList;
	}

}