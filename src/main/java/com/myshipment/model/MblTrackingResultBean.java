package com.myshipment.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class MblTrackingResultBean implements Serializable{
	private String cust_no;
	private String comp_code;
	private String sales_org;
	private String sales_org_desc;
	private String dist_ch;
	private String division;
	private String order_type;
	private String so_no;
	private Date so_dt;
	private String bl_no;
	private Date bl_dt;
	private String shipper;
	private String shipper_no;
	private String buyer;
	private String buyer_no;
	private String agent;
	private String agent_no;
	private String carrier;
	private String coloader;
	private String frt_mode;
	private String frt_mode_ds;
	private String inco1;
	private String inco2;
	private String comm_inv;
	private Date comm_inv_dt;
	private String lc_type;
	private String lc_no;
	private Date lc_dt;
	private String exp_no;
	private Date exp_dt;
	private String ship_bl_no;
	private Date ship_bl_dt;
	private Date exp_ch_dt;
	private String orig_cn;	
	private String dest_cn;
	private String por;
	private String pol;
	private String pod;
	private Date load_dt;
	private Date dep_dt;
	private Date eta_dt;
	private Date ata_dt;
	private String plod;
	private String cargo_type;
	private String manifest;
	private Date manf_dt;
	private Date noc_dt;
	private Date ship_rel_dt;
	private Date unstf_dt;
	private BigDecimal tot_qty;
	private BigDecimal net_wt;
	private BigDecimal grs_wt;
	private BigDecimal volume;
	private BigDecimal crg_wt;
	private String tot_pcs;
	private Date docr_dt;
	private Date bl_rel_dt;
	private String cour_no;
	private Date cour_dt;
	private String mbl_no;
	private Date mbl_dt;
	private String mbl2_no;
	private Date mbl2_dt;
	private String mbl_type;
	private String ship_mode;
	private String cont_mode;
	private String rot_no;	
	private Date gr_dt;
	private String truck_no;
	private String shipment_no;
	private Date pod_dt;
	private Date stuff_dt;
	private String status;
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getSales_org_desc() {
		return sales_org_desc;
	}
	public void setSales_org_desc(String sales_org_desc) {
		this.sales_org_desc = sales_org_desc;
	}
	public String getDist_ch() {
		return dist_ch;
	}
	public void setDist_ch(String dist_ch) {
		this.dist_ch = dist_ch;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public String getSo_no() {
		return so_no;
	}
	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}
	public Date getSo_dt() {
		return so_dt;
	}
	public void setSo_dt(Date so_dt) {
		this.so_dt = so_dt;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public Date getBl_dt() {
		return bl_dt;
	}
	public void setBl_dt(Date bl_dt) {
		this.bl_dt = bl_dt;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer() {
		return buyer;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getAgent_no() {
		return agent_no;
	}
	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getColoader() {
		return coloader;
	}
	public void setColoader(String coloader) {
		this.coloader = coloader;
	}
	public String getFrt_mode() {
		return frt_mode;
	}
	public void setFrt_mode(String frt_mode) {
		this.frt_mode = frt_mode;
	}
	public String getFrt_mode_ds() {
		return frt_mode_ds;
	}
	public void setFrt_mode_ds(String frt_mode_ds) {
		this.frt_mode_ds = frt_mode_ds;
	}
	public String getInco1() {
		return inco1;
	}
	public void setInco1(String inco1) {
		this.inco1 = inco1;
	}
	public String getInco2() {
		return inco2;
	}
	public void setInco2(String inco2) {
		this.inco2 = inco2;
	}
	public String getComm_inv() {
		return comm_inv;
	}
	public void setComm_inv(String comm_inv) {
		this.comm_inv = comm_inv;
	}
	public Date getComm_inv_dt() {
		return comm_inv_dt;
	}
	public void setComm_inv_dt(Date comm_inv_dt) {
		this.comm_inv_dt = comm_inv_dt;
	}
	public String getLc_type() {
		return lc_type;
	}
	public void setLc_type(String lc_type) {
		this.lc_type = lc_type;
	}
	public String getLc_no() {
		return lc_no;
	}
	public void setLc_no(String lc_no) {
		this.lc_no = lc_no;
	}
	public Date getLc_dt() {
		return lc_dt;
	}
	public void setLc_dt(Date lc_dt) {
		this.lc_dt = lc_dt;
	}
	public String getExp_no() {
		return exp_no;
	}
	public void setExp_no(String exp_no) {
		this.exp_no = exp_no;
	}
	public Date getExp_dt() {
		return exp_dt;
	}
	public void setExp_dt(Date exp_dt) {
		this.exp_dt = exp_dt;
	}
	public String getShip_bl_no() {
		return ship_bl_no;
	}
	public void setShip_bl_no(String ship_bl_no) {
		this.ship_bl_no = ship_bl_no;
	}
	public Date getShip_bl_dt() {
		return ship_bl_dt;
	}
	public void setShip_bl_dt(Date ship_bl_dt) {
		this.ship_bl_dt = ship_bl_dt;
	}
	public Date getExp_ch_dt() {
		return exp_ch_dt;
	}
	public void setExp_ch_dt(Date exp_ch_dt) {
		this.exp_ch_dt = exp_ch_dt;
	}
	public String getOrig_cn() {
		return orig_cn;
	}
	public void setOrig_cn(String orig_cn) {
		this.orig_cn = orig_cn;
	}
	public String getDest_cn() {
		return dest_cn;
	}
	public void setDest_cn(String dest_cn) {
		this.dest_cn = dest_cn;
	}
	public String getPor() {
		return por;
	}
	public void setPor(String por) {
		this.por = por;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public Date getLoad_dt() {
		return load_dt;
	}
	public void setLoad_dt(Date load_dt) {
		this.load_dt = load_dt;
	}
	public Date getDep_dt() {
		return dep_dt;
	}
	public void setDep_dt(Date dep_dt) {
		this.dep_dt = dep_dt;
	}
	public Date getEta_dt() {
		return eta_dt;
	}
	public void setEta_dt(Date eta_dt) {
		this.eta_dt = eta_dt;
	}
	public Date getAta_dt() {
		return ata_dt;
	}
	public void setAta_dt(Date ata_dt) {
		this.ata_dt = ata_dt;
	}
	public String getPlod() {
		return plod;
	}
	public void setPlod(String plod) {
		this.plod = plod;
	}
	public String getCargo_type() {
		return cargo_type;
	}
	public void setCargo_type(String cargo_type) {
		this.cargo_type = cargo_type;
	}
	public String getManifest() {
		return manifest;
	}
	public void setManifest(String manifest) {
		this.manifest = manifest;
	}
	public Date getManf_dt() {
		return manf_dt;
	}
	public void setManf_dt(Date manf_dt) {
		this.manf_dt = manf_dt;
	}
	public Date getNoc_dt() {
		return noc_dt;
	}
	public void setNoc_dt(Date noc_dt) {
		this.noc_dt = noc_dt;
	}
	public Date getShip_rel_dt() {
		return ship_rel_dt;
	}
	public void setShip_rel_dt(Date ship_rel_dt) {
		this.ship_rel_dt = ship_rel_dt;
	}
	public Date getUnstf_dt() {
		return unstf_dt;
	}
	public void setUnstf_dt(Date unstf_dt) {
		this.unstf_dt = unstf_dt;
	}
	public BigDecimal getTot_qty() {
		return tot_qty;
	}
	public void setTot_qty(BigDecimal tot_qty) {
		this.tot_qty = tot_qty;
	}
	public BigDecimal getNet_wt() {
		return net_wt;
	}
	public void setNet_wt(BigDecimal net_wt) {
		this.net_wt = net_wt;
	}
	public BigDecimal getGrs_wt() {
		return grs_wt;
	}
	public void setGrs_wt(BigDecimal grs_wt) {
		this.grs_wt = grs_wt;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getCrg_wt() {
		return crg_wt;
	}
	public void setCrg_wt(BigDecimal crg_wt) {
		this.crg_wt = crg_wt;
	}
	public String getTot_pcs() {
		return tot_pcs;
	}
	public void setTot_pcs(String tot_pcs) {
		this.tot_pcs = tot_pcs;
	}
	public Date getDocr_dt() {
		return docr_dt;
	}
	public void setDocr_dt(Date docr_dt) {
		this.docr_dt = docr_dt;
	}
	public Date getBl_rel_dt() {
		return bl_rel_dt;
	}
	public void setBl_rel_dt(Date bl_rel_dt) {
		this.bl_rel_dt = bl_rel_dt;
	}
	public String getCour_no() {
		return cour_no;
	}
	public void setCour_no(String cour_no) {
		this.cour_no = cour_no;
	}
	public Date getCour_dt() {
		return cour_dt;
	}
	public void setCour_dt(Date cour_dt) {
		this.cour_dt = cour_dt;
	}
	public String getMbl_no() {
		return mbl_no;
	}
	public void setMbl_no(String mbl_no) {
		this.mbl_no = mbl_no;
	}
	public Date getMbl_dt() {
		return mbl_dt;
	}
	public void setMbl_dt(Date mbl_dt) {
		this.mbl_dt = mbl_dt;
	}
	public String getMbl2_no() {
		return mbl2_no;
	}
	public void setMbl2_no(String mbl2_no) {
		this.mbl2_no = mbl2_no;
	}
	public Date getMbl2_dt() {
		return mbl2_dt;
	}
	public void setMbl2_dt(Date mbl2_dt) {
		this.mbl2_dt = mbl2_dt;
	}
	public String getMbl_type() {
		return mbl_type;
	}
	public void setMbl_type(String mbl_type) {
		this.mbl_type = mbl_type;
	}
	public String getShip_mode() {
		return ship_mode;
	}
	public void setShip_mode(String ship_mode) {
		this.ship_mode = ship_mode;
	}
	public String getCont_mode() {
		return cont_mode;
	}
	public void setCont_mode(String cont_mode) {
		this.cont_mode = cont_mode;
	}
	public String getRot_no() {
		return rot_no;
	}
	public void setRot_no(String rot_no) {
		this.rot_no = rot_no;
	}
	public Date getGr_dt() {
		return gr_dt;
	}
	public void setGr_dt(Date gr_dt) {
		this.gr_dt = gr_dt;
	}
	public String getTruck_no() {
		return truck_no;
	}
	public void setTruck_no(String truck_no) {
		this.truck_no = truck_no;
	}
	public String getShipment_no() {
		return shipment_no;
	}
	public void setShipment_no(String shipment_no) {
		this.shipment_no = shipment_no;
	}
	public Date getPod_dt() {
		return pod_dt;
	}
	public void setPod_dt(Date pod_dt) {
		this.pod_dt = pod_dt;
	}
	public Date getStuff_dt() {
		return stuff_dt;
	}
	public void setStuff_dt(Date stuff_dt) {
		this.stuff_dt = stuff_dt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
	
	
	
	
	
	
	/*
	 * 
	 * shammi
	 * 
	 * 
	 * private static final long serialVersionUID = 1L;

	private String  po_no;
	private String  document_no;
	private String  bl_no;
	private String  mbl_no;
	private String  shipment_type;                           
	private String  container_no;
	private String  container_type;
	private String  booking_date;
	private String  bl_date;
	private String  shipper_name;
	private String  pol_code;
	private String  pod_code;
	private String  gross_wt;
	private String  charge_wt;
	private String  tot_volume;
	private String  gr_date;
	private String  shipment_date;
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	public String getDocument_no() {
		return document_no;
	}
	public void setDocument_no(String document_no) {
		this.document_no = document_no;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getMbl_no() {
		return mbl_no;
	}
	public void setMbl_no(String mbl_no) {
		this.mbl_no = mbl_no;
	}
	public String getShipment_type() {
		return shipment_type;
	}
	public void setShipment_type(String shipment_type) {
		this.shipment_type = shipment_type;
	}
	public String getContainer_no() {
		return container_no;
	}
	public void setContainer_no(String container_no) {
		this.container_no = container_no;
	}
	public String getContainer_type() {
		return container_type;
	}
	public void setContainer_type(String container_type) {
		this.container_type = container_type;
	}
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public String getBl_date() {
		return bl_date;
	}
	public void setBl_date(String bl_date) {
		this.bl_date = bl_date;
	}
	public String getShipper_name() {
		return shipper_name;
	}
	public void setShipper_name(String shipper_name) {
		this.shipper_name = shipper_name;
	}
	public String getPol_code() {
		return pol_code;
	}
	public void setPol_code(String pol_code) {
		this.pol_code = pol_code;
	}
	public String getPod_code() {
		return pod_code;
	}
	public void setPod_code(String pod_code) {
		this.pod_code = pod_code;
	}
	public String getGross_wt() {
		return gross_wt;
	}
	public void setGross_wt(String gross_wt) {
		this.gross_wt = gross_wt;
	}
	public String getCharge_wt() {
		return charge_wt;
	}
	public void setCharge_wt(String charge_wt) {
		this.charge_wt = charge_wt;
	}
	public String getTot_volume() {
		return tot_volume;
	}
	public void setTot_volume(String tot_volume) {
		this.tot_volume = tot_volume;
	}
	public String getGr_date() {
		return gr_date;
	}
	public void setGr_date(String gr_date) {
		this.gr_date = gr_date;
	}
	public String getShipment_date() {
		return shipment_date;
	}
	public void setShipment_date(String shipment_date) {
		this.shipment_date = shipment_date;
	}*/
	
	
	

}
