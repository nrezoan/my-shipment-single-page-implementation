/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class ContainerTrackingParams {

	private String containerNumber;

	public String getContainerNumber() {
		return containerNumber;
	}

	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
}
