package com.myshipment.model;

public class BlStatusDetailsParam {
	private String status;
	private DashboardShipperParams dashboardShipperParams;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DashboardShipperParams getDashboardShipperParams() {
		return dashboardShipperParams;
	}
	public void setDashboardShipperParams(DashboardShipperParams dashboardShipperParams) {
		this.dashboardShipperParams = dashboardShipperParams;
	}

}
