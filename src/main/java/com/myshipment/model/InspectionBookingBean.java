package com.myshipment.model;

import java.io.Serializable;

public class InspectionBookingBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2238616823089313426L;
	private String supplierId="";
	private String buyingHouse="";
	private String poNumber="";
	private String inspectionDate="";
	private int inspectionQuantity=0;
	private String packMethod="";
	private String cargoDeliveryDate="";
	private String location="";
	private String customerName="";
	private String remarks="";
	private String deliveryDate="";
	private int quantity=0;
	private String sapNo="";
	private String merchantDiser="";
	private String advOrder="";
	private String salesOrg="";
	private String division="";
	private String distChannel="";
	private String mode="";
	private String entryTime="";
	private String type="";
	private double bookingNo=0;
	private String scotaFileSubmission="";
	private String bookingStatus="";
		
	public String getScotaFileSubmission() {
		return scotaFileSubmission;
	}
	public void setScotaFileSubmission(String scotaFileSubmission) {
		this.scotaFileSubmission = scotaFileSubmission;
	}
	public String getBookingStatus() {
		return bookingStatus;
	}
	public void setBookingStatus(String bookingStatus) {
		this.bookingStatus = bookingStatus;
	}
	public void setSupplierId(String supplierId){
		this.supplierId=supplierId;
	}
	public String getSupplierId(){
		return supplierId;
	}
	public void setBuyingHouse(String buyingHouse){
		this.buyingHouse=buyingHouse;
	}
	public String getBuyingHouse(){
		return buyingHouse;
	}
	public void setPoNumber(String poNumber){
		this.poNumber=poNumber;
	}
	public String getPoNumber(){
		return poNumber;
	}
	public void setInspectionDate(String inspectionDate){
		this.inspectionDate=inspectionDate;
	}
	public String getInspectionDate(){
		return inspectionDate;
	}
	public void setInspectionQuantity(int inspectionQuantity){
		this.inspectionQuantity=inspectionQuantity;
	}
	public int getInspectionQuantity(){
		return inspectionQuantity;
	}
	public void setPackMethod(String packMethod){
		this.packMethod=packMethod;
	}
	public String getPackMethod(){
		return packMethod;
	}
	public void setCargoDeliveryDate(String cargoDeliveryDate){
		this.cargoDeliveryDate=cargoDeliveryDate;
	}
	public String getCargoDeliveryDate(){
		return cargoDeliveryDate;
	}
	public void setLocation(String location){
		this.location=location;
	}
	public String getLocation(){
		return location;
	}
	public void setCustomerName(String customerName){
		this.customerName=customerName;
	}
	public String getCustomerName(){
		return customerName;
	}
	public void setRemarks(String remarks){
		this.remarks=remarks;
	}
	public String getRemarks(){
		return remarks;
	}public void setDeliveryDate(String deliveryDate){
		this.deliveryDate=deliveryDate;
	}
	public String getDeliveryDate(){
		return deliveryDate;
	}
	public void setQuantity(int quantity){
		this.quantity=quantity;
	}
	public int getQuantity(){
		return quantity;
	}
	public void setSapNo(String sapNo){
		this.sapNo=sapNo;
	}
	public String getSapNo(){
		return sapNo;
	}
	public void setMerchantDiser(String merchantDiser){
		this.merchantDiser=merchantDiser;
	}
	public String getMerchantDiser(){
		return merchantDiser;
	}
	public void setAdvOrder(String advOrder){
		this.advOrder=advOrder;
	}
	public String getAdvOrder(){
		return advOrder;
	}	
	
	public void setSalesOrg(String salesOrg){
		this.salesOrg=salesOrg;
	}
	public String getSalesOrg(){
		return salesOrg;
	}
	
	public void setDivision(String division){
		this.division=division;
	}
	public String getDivision(){
		return division;
	}
	
	public void setDistChannel(String distChannel){
		this.distChannel=distChannel;
	}
	public String getDistChannel(){
		return distChannel;
	}	

	public void setMode(String mode){
		this.mode=mode;
	}
	public String getMode(){
		return mode;
	}	
	   
    public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return type;
	}
	public void setBookingNo(double bookingNo){
		this.bookingNo=bookingNo;
	}
	public double getBookingNo(){
		return bookingNo;
	}
	public void setEntryTime(String entryTime){
		this.entryTime=entryTime;
	}
	public String getEntryTime(){
		return entryTime;
	}
	

}
