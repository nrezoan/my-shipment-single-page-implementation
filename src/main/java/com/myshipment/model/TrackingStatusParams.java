package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class TrackingStatusParams {

	private String bookingDate;
	private String goodsReceivedDate;
	private String stuffingDate;
	private String shippedOnboardDate;
	private String deliveredDate;
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getGoodsReceivedDate() {
		return goodsReceivedDate;
	}
	public void setGoodsReceivedDate(String goodsReceivedDate) {
		this.goodsReceivedDate = goodsReceivedDate;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getShippedOnboardDate() {
		return shippedOnboardDate;
	}
	public void setShippedOnboardDate(String shippedOnboardDate) {
		this.shippedOnboardDate = shippedOnboardDate;
	}
	public String getDeliveredDate() {
		return deliveredDate;
	}
	public void setDeliveredDate(String deliveredDate) {
		this.deliveredDate = deliveredDate;
	}
	
	
}
