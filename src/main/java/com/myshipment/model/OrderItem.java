package com.myshipment.model;

import java.util.HashMap;
import java.util.Map;

public class OrderItem {

	private String buyerNo;
	private String itemNumber;
	private Integer posnerNumber;
	private String docNumber;
	private String refDoc;
	private String material;
	private String materialText;
	private String materialUnit;
	private String poNumber;
	private String styleNo;
	private String refNo;
	private String projectNo;
	private String productCode;
	public String color;
	private String sizeNo;
	private Integer totalPieces;
	private Integer curtonQuantity;
	private String unit;
	private Double totalCbm;
	private Double totalVolume;
	private Double volumeWeight;
	private Double grossWeight;
	private Double netWeight;
	private String payCond;
	private Double unitPrice;
	private String currency;
	private String plant;
	
	private String grFlag;

	private String hblNo;
	private String buyerName;
	private String customerName;
	private String portName;
	private String voyageNo1;
	private String voyageNo2;
	private String docCategory;
	private String wareHouseNo;
	private String salesOrgString;
	private String distChannel;
	private String division;
	private String receiveDate;
	private String outDate;
	private String outTime;
	private String inDate;
	private String inTime;
	private String storageType;
	private String destPort;
	private String forwardingAgentNo;
	private String feederVslName;
	private String motherVslName;
	private String articleNo;
	private String skuNo;

	// Item Detail
	private String hsCode;
	private String commInvoice;
	private String commInvoiceDate;
	private Double netCost;
	private Integer pcsPerCarton;
	private Double cbmPerCarton;
	private Double grossWeightPerCarton;
	private Double netWeightPerCarton;
	private Double cartonLength;
	private Double cartonWidth;
	private Double cartonHeight;
	private String cartonUnit;
	private String cartonSerNo;
	private String reference1;
	private String reference2;
	private String reference3;
	private String reference4;
	private String reference5;
	private String qcDate;
	private String releaseDate;
	private String description;
	private String itemDescription;
	private String shippingMark;
	private Double invoiceValue;
	private Double crfValue;
	private String etdDate;

	private String size1;
	private String size2;
	private String size3;
	private String size4;
	private String size5;
	private String size6;
	private String size7;
	private String size8;
	private String size9;
	private String size10;
	private String size11;
	private String size12;
	private String size13;
	private String size14;
	private Map<Integer, PackingItem> packingList = new HashMap<Integer, PackingItem>();

	public String getBuyerNo() {
		return buyerNo;
	}

	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}

	
	public String getItemNumber() {
		return itemNumber;
	}

	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}

	public Integer getPosnerNumber() {
		return posnerNumber;
	}

	public void setPosnerNumber(Integer posnerNumber) {
		this.posnerNumber = posnerNumber;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getRefDoc() {
		return refDoc;
	}

	public void setRefDoc(String refDoc) {
		this.refDoc = refDoc;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getMaterialText() {
		return materialText;
	}

	public void setMaterialText(String materialText) {
		this.materialText = materialText;
	}

	public String getMaterialUnit() {
		return materialUnit;
	}

	public void setMaterialUnit(String materialUnit) {
		this.materialUnit = materialUnit;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getStyleNo() {
		return styleNo;
	}

	public void setStyleNo(String styleNo) {
		this.styleNo = styleNo;
	}

	public String getRefNo() {
		return refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(String projectNo) {
		this.projectNo = projectNo;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSizeNo() {
		return sizeNo;
	}

	public void setSizeNo(String sizeNo) {
		this.sizeNo = sizeNo;
	}

	public Integer getTotalPieces() {
		return totalPieces;
	}

	public void setTotalPieces(Integer totalPieces) {
		this.totalPieces = totalPieces;
	}

	public Integer getCurtonQuantity() {
		return curtonQuantity;
	}

	public void setCurtonQuantity(Integer curtonQuantity) {
		this.curtonQuantity = curtonQuantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getTotalCbm() {
		return totalCbm;
	}

	public void setTotalCbm(Double totalCbm) {
		this.totalCbm = totalCbm;
	}

	
	public Double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Double getVolumeWeight() {
		return volumeWeight;
	}

	public void setVolumeWeight(Double volumeWeight) {
		this.volumeWeight = volumeWeight;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public String getPayCond() {
		return payCond;
	}

	public void setPayCond(String payCond) {
		this.payCond = payCond;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getHblNo() {
		return hblNo;
	}

	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	public String getVoyageNo1() {
		return voyageNo1;
	}

	public void setVoyageNo1(String voyageNo1) {
		this.voyageNo1 = voyageNo1;
	}

	public String getVoyageNo2() {
		return voyageNo2;
	}

	public void setVoyageNo2(String voyageNo2) {
		this.voyageNo2 = voyageNo2;
	}

	public String getDocCategory() {
		return docCategory;
	}

	public void setDocCategory(String docCategory) {
		this.docCategory = docCategory;
	}

	public String getWareHouseNo() {
		return wareHouseNo;
	}

	public void setWareHouseNo(String wareHouseNo) {
		this.wareHouseNo = wareHouseNo;
	}

	public String getSalesOrgString() {
		return salesOrgString;
	}

	public void setSalesOrgString(String salesOrgString) {
		this.salesOrgString = salesOrgString;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getOutDate() {
		return outDate;
	}

	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public String getInDate() {
		return inDate;
	}

	public void setInDate(String inDate) {
		this.inDate = inDate;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getStorageType() {
		return storageType;
	}

	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}

	public String getDestPort() {
		return destPort;
	}

	public void setDestPort(String destPort) {
		this.destPort = destPort;
	}

	public String getForwardingAgentNo() {
		return forwardingAgentNo;
	}

	public void setForwardingAgentNo(String forwardingAgentNo) {
		this.forwardingAgentNo = forwardingAgentNo;
	}

	public String getFeederVslName() {
		return feederVslName;
	}

	public void setFeederVslName(String feederVslName) {
		this.feederVslName = feederVslName;
	}

	public String getMotherVslName() {
		return motherVslName;
	}

	public void setMotherVslName(String motherVslName) {
		this.motherVslName = motherVslName;
	}

	public String getArticleNo() {
		return articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getSkuNo() {
		return skuNo;
	}

	public void setSkuNo(String skuNo) {
		this.skuNo = skuNo;
	}

	public String getHsCode() {
		return hsCode;
	}

	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}

	public String getCommInvoice() {
		return commInvoice;
	}

	public void setCommInvoice(String commInvoice) {
		this.commInvoice = commInvoice;
	}

	public String getCommInvoiceDate() {
		return commInvoiceDate;
	}

	public void setCommInvoiceDate(String commInvoiceDate) {
		this.commInvoiceDate = commInvoiceDate;
	}

	public Double getNetCost() {
		return netCost;
	}

	public void setNetCost(Double netCost) {
		this.netCost = netCost;
	}

	public Integer getPcsPerCarton() {
		return pcsPerCarton;
	}

	public void setPcsPerCarton(Integer pcsPerCarton) {
		this.pcsPerCarton = pcsPerCarton;
	}

	public Double getCbmPerCarton() {
		return cbmPerCarton;
	}

	public void setCbmPerCarton(Double cbmPerCarton) {
		this.cbmPerCarton = cbmPerCarton;
	}

	public Double getGrossWeightPerCarton() {
		return grossWeightPerCarton;
	}

	public void setGrossWeightPerCarton(Double grossWeightPerCarton) {
		this.grossWeightPerCarton = grossWeightPerCarton;
	}

	public Double getNetWeightPerCarton() {
		return netWeightPerCarton;
	}

	public void setNetWeightPerCarton(Double netWeightPerCarton) {
		this.netWeightPerCarton = netWeightPerCarton;
	}

	public Double getCartonLength() {
		return cartonLength;
	}

	public void setCartonLength(Double cartonLength) {
		this.cartonLength = cartonLength;
	}

	public Double getCartonWidth() {
		return cartonWidth;
	}

	public void setCartonWidth(Double cartonWidth) {
		this.cartonWidth = cartonWidth;
	}

	public Double getCartonHeight() {
		return cartonHeight;
	}

	public void setCartonHeight(Double cartonHeight) {
		this.cartonHeight = cartonHeight;
	}

	public String getCartonUnit() {
		return cartonUnit;
	}

	public void setCartonUnit(String cartonUnit) {
		this.cartonUnit = cartonUnit;
	}

	public String getCartonSerNo() {
		return cartonSerNo;
	}

	public void setCartonSerNo(String cartonSerNo) {
		this.cartonSerNo = cartonSerNo;
	}

	public String getReference1() {
		return reference1;
	}

	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}

	public String getReference2() {
		return reference2;
	}

	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}

	public String getReference3() {
		return reference3;
	}

	public void setReference3(String reference3) {
		this.reference3 = reference3;
	}

	public String getReference4() {
		return reference4;
	}

	public void setReference4(String reference4) {
		this.reference4 = reference4;
	}

	public String getReference5() {
		return reference5;
	}

	public void setReference5(String reference5) {
		this.reference5 = reference5;
	}

	public String getQcDate() {
		return qcDate;
	}

	public void setQcDate(String qcDate) {
		this.qcDate = qcDate;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getShippingMark() {
		return shippingMark;
	}

	public void setShippingMark(String shippingMark) {
		this.shippingMark = shippingMark;
	}

	public Double getInvoiceValue() {
		return invoiceValue;
	}

	public void setInvoiceValue(Double invoiceValue) {
		this.invoiceValue = invoiceValue;
	}

	public Double getCrfValue() {
		return crfValue;
	}

	public void setCrfValue(Double crfValue) {
		this.crfValue = crfValue;
	}

	public String getEtdDate() {
		return etdDate;
	}

	public void setEtdDate(String etdDate) {
		this.etdDate = etdDate;
	}

	public String getSize1() {
		return size1;
	}

	public void setSize1(String size1) {
		this.size1 = size1;
	}

	public String getSize2() {
		return size2;
	}

	public void setSize2(String size2) {
		this.size2 = size2;
	}

	public String getSize3() {
		return size3;
	}

	public void setSize3(String size3) {
		this.size3 = size3;
	}

	public String getSize4() {
		return size4;
	}

	public void setSize4(String size4) {
		this.size4 = size4;
	}

	public String getSize5() {
		return size5;
	}

	public void setSize5(String size5) {
		this.size5 = size5;
	}

	public String getSize6() {
		return size6;
	}

	public void setSize6(String size6) {
		this.size6 = size6;
	}

	public String getSize7() {
		return size7;
	}

	public void setSize7(String size7) {
		this.size7 = size7;
	}

	public String getSize8() {
		return size8;
	}

	public void setSize8(String size8) {
		this.size8 = size8;
	}

	public String getSize9() {
		return size9;
	}

	public void setSize9(String size9) {
		this.size9 = size9;
	}

	public String getSize10() {
		return size10;
	}

	public void setSize10(String size10) {
		this.size10 = size10;
	}

	public String getSize11() {
		return size11;
	}

	public void setSize11(String size11) {
		this.size11 = size11;
	}

	public String getSize12() {
		return size12;
	}

	public void setSize12(String size12) {
		this.size12 = size12;
	}

	public String getSize13() {
		return size13;
	}

	public void setSize13(String size13) {
		this.size13 = size13;
	}

	public String getSize14() {
		return size14;
	}

	public void setSize14(String size14) {
		this.size14 = size14;
	}

	public Map<Integer, PackingItem> getPackingList() {
		return packingList;
	}

	public void setPackingList(Map<Integer, PackingItem> packingList) {
		this.packingList = packingList;
	}

	public String getPlant() {
		return plant;
	}

	public void setPlant(String plant) {
		this.plant = plant;
	}

	public String getGrFlag() {
		return grFlag;
	}

	public void setGrFlag(String grFlag) {
		this.grFlag = grFlag;
	}

	@Override
	public String toString() {
		return "OrderItem [buyerNo=" + buyerNo + ", itemNumber=" + itemNumber + ", posnerNumber=" + posnerNumber
				+ ", docNumber=" + docNumber + ", refDoc=" + refDoc + ", material=" + material + ", materialText="
				+ materialText + ", materialUnit=" + materialUnit + ", poNumber=" + poNumber + ", styleNo=" + styleNo
				+ ", refNo=" + refNo + ", projectNo=" + projectNo + ", productCode=" + productCode + ", color=" + color
				+ ", sizeNo=" + sizeNo + ", totalPieces=" + totalPieces + ", curtonQuantity=" + curtonQuantity
				+ ", unit=" + unit + ", totalCbm=" + totalCbm + ", totalVolume=" + totalVolume + ", volumeWeight="
				+ volumeWeight + ", grossWeight=" + grossWeight + ", netWeight=" + netWeight + ", payCond=" + payCond
				+ ", unitPrice=" + unitPrice + ", currency=" + currency + ", plant=" + plant + ", hblNo=" + hblNo
				+ ", buyerName=" + buyerName + ", customerName=" + customerName + ", portName=" + portName
				+ ", voyageNo1=" + voyageNo1 + ", voyageNo2=" + voyageNo2 + ", docCategory=" + docCategory
				+ ", wareHouseNo=" + wareHouseNo + ", salesOrgString=" + salesOrgString + ", distChannel=" + distChannel
				+ ", division=" + division + ", receiveDate=" + receiveDate + ", outDate=" + outDate + ", outTime="
				+ outTime + ", inDate=" + inDate + ", inTime=" + inTime + ", storageType=" + storageType + ", destPort="
				+ destPort + ", forwardingAgentNo=" + forwardingAgentNo + ", feederVslName=" + feederVslName
				+ ", motherVslName=" + motherVslName + ", articleNo=" + articleNo + ", skuNo=" + skuNo + ", hsCode="
				+ hsCode + ", commInvoice=" + commInvoice + ", commInvoiceDate=" + commInvoiceDate + ", netCost="
				+ netCost + ", pcsPerCarton=" + pcsPerCarton + ", cbmPerCarton=" + cbmPerCarton
				+ ", grossWeightPerCarton=" + grossWeightPerCarton + ", netWeightPerCarton=" + netWeightPerCarton
				+ ", cartonLength=" + cartonLength + ", cartonWidth=" + cartonWidth + ", cartonHeight=" + cartonHeight
				+ ", cartonUnit=" + cartonUnit + ", cartonSerNo=" + cartonSerNo + ", reference1=" + reference1
				+ ", reference2=" + reference2 + ", reference3=" + reference3 + ", reference4=" + reference4
				+ ", reference5=" + reference5 + ", qcDate=" + qcDate + ", releaseDate=" + releaseDate
				+ ", description=" + description + ", itemDescription=" + itemDescription + ", shippingMark="
				+ shippingMark + ", invoiceValue=" + invoiceValue + ", crfValue=" + crfValue + ", etdDate=" + etdDate
				+ ", size1=" + size1 + ", size2=" + size2 + ", size3=" + size3 + ", size4=" + size4 + ", size5=" + size5
				+ ", size6=" + size6 + ", size7=" + size7 + ", size8=" + size8 + ", size9=" + size9 + ", size10="
				+ size10 + ", size11=" + size11 + ", size12=" + size12 + ", size13=" + size13 + ", size14=" + size14
				+ ", packingList=" + packingList + "]";
	}

}
