package com.myshipment.model;

public class DashboardShipperParams {
	private String shipperNo;
	private String division;
	private String distChannel;
	private String bookingDateFrom;
	private String bookingDateTo;
	
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getBookingDateFrom() {
		return bookingDateFrom;
	}
	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}
	public String getBookingDateTo() {
		return bookingDateTo;
	}
	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}
	
	
	
	@Override
	public String toString() {
		return "DashboardParams [shipperNo=" + shipperNo + ", division=" + division + ", distChannel=" + distChannel
				+ ", bookingDateFrom=" + bookingDateFrom + ", bookingDateTo=" + bookingDateTo + "]";
	}
	
	
	

}
