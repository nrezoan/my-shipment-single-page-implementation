package com.myshipment.model;

import java.util.List;

public class SupplierWiseGWTDetailJson {
	private List<SupplierWiseGWTJson> lstSupplierWiseGWTJson;
	private String message;
	public List<SupplierWiseGWTJson> getLstSupplierWiseGWTJson() {
		return lstSupplierWiseGWTJson;
	}
	public void setLstSupplierWiseGWTJson(List<SupplierWiseGWTJson> lstSupplierWiseGWTJson) {
		this.lstSupplierWiseGWTJson = lstSupplierWiseGWTJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
