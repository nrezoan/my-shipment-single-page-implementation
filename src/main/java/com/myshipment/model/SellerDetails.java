package com.myshipment.model;

/**
 * @author virendra
 *
 */
public class SellerDetails {
	private static final long serialVersionUID=3213232L;
	private String sellerName;
	private String sellerAddress;
	private String sellerCity;
	public String getSellerCity() {
		return sellerCity;
	}
	public void setSellerCity(String sellerCity) {
		this.sellerCity = sellerCity;
	}
	private String sellerCountry;
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getSellerAddress() {
		return sellerAddress;
	}
	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress;
	}
	public String getSellerCountry() {
		return sellerCountry;
	}
	public void setSellerCountry(String sellerCountry) {
		this.sellerCountry = sellerCountry;
	}
	

}
