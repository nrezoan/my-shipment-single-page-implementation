package com.myshipment.model;

import java.util.List;

public class BookingByPoDTO {
	private List<ApprovedPurchaseOrder> lstAppPurchaseOrder;
	
	public List<ApprovedPurchaseOrder> getLstAppPurchaseOrder() {
		return lstAppPurchaseOrder;
	}
	public void setLstAppPurchaseOrder(List<ApprovedPurchaseOrder> lstAppPurchaseOrder) {
		this.lstAppPurchaseOrder = lstAppPurchaseOrder;
	}
	
	

}
