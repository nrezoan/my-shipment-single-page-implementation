package com.myshipment.model;

public class DetailsEditParam {
	private String customerNo;
	private String password;
	private String newPassword;
	private String verifyPassword;
	private String newMail;
	private String verifyMail;
	private String prefUserName;
	
	public String getCustomerNo() {
		return customerNo;
	}
	public String getPassword() {
		return password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public String getVerifyPassword() {
		return verifyPassword;
	}
	public String getNewMail() {
		return newMail;
	}
	public String getVerifyMail() {
		return verifyMail;
	}
	public String getPrefUserName() {
		return prefUserName;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}
	public void setNewMail(String newMail) {
		this.newMail = newMail;
	}
	public void setVerifyMail(String verifyMail) {
		this.verifyMail = verifyMail;
	}
	public void setPrefUserName(String prefUserName) {
		this.prefUserName = prefUserName;
	}
	

}
