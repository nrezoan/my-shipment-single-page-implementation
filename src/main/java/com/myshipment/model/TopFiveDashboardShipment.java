/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class TopFiveDashboardShipment {

	private String port;
	private Integer totalShipment;
	private Double totalCBM;
	private Double totalGWT;
	
	public Integer getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(Integer totalShipment) {
		this.totalShipment = totalShipment;
	}
	public Double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(Double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public Double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(Double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	
	
}
