package com.myshipment.model;

import java.util.List;

public class DirectBookingJsonData {

	private String hblnumber;
	private String zsalesdocument;
	private String fvsl;
	private String mvsl1;
	private String mvsl2;
	private String mvsl3;

	private String voyage1;
	private String voyage2;
	private String voyage3;

	private String etd;
	private String transhipment1etd;
	private String transhipment2etd;
	private String ata;
	private String atd;
	private String transhipment1eta;
	private String transhipment2eta;
	private String eta;
	private String division;

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getFvsl() {
		return fvsl;
	}

	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}

	public String getMvsl1() {
		return mvsl1;
	}

	public void setMvsl1(String mvsl1) {
		this.mvsl1 = mvsl1;
	}

	public String getMvsl2() {
		return mvsl2;
	}

	public void setMvsl2(String mvsl2) {
		this.mvsl2 = mvsl2;
	}

	public String getMvsl3() {
		return mvsl3;
	}

	public void setMvsl3(String mvsl3) {
		this.mvsl3 = mvsl3;
	}

	public String getVoyage1() {
		return voyage1;
	}

	public void setVoyage1(String voyage1) {
		this.voyage1 = voyage1;
	}

	public String getVoyage2() {
		return voyage2;
	}

	public void setVoyage2(String voyage2) {
		this.voyage2 = voyage2;
	}

	public String getVoyage3() {
		return voyage3;
	}

	public void setVoyage3(String voyage3) {
		this.voyage3 = voyage3;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getTranshipment1etd() {
		return transhipment1etd;
	}

	public void setTranshipment1etd(String transhipment1etd) {
		this.transhipment1etd = transhipment1etd;
	}

	public String getTranshipment2etd() {
		return transhipment2etd;
	}

	public void setTranshipment2etd(String transhipment2etd) {
		this.transhipment2etd = transhipment2etd;
	}

	public String getAta() {
		return ata;
	}

	public void setAta(String ata) {
		this.ata = ata;
	}

	public String getAtd() {
		return atd;
	}

	public void setAtd(String atd) {
		this.atd = atd;
	}

	public String getTranshipment1eta() {
		return transhipment1eta;
	}

	public void setTranshipment1eta(String transhipment1eta) {
		this.transhipment1eta = transhipment1eta;
	}

	public String getTranshipment2eta() {
		return transhipment2eta;
	}

	public void setTranshipment2eta(String transhipment2eta) {
		this.transhipment2eta = transhipment2eta;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	private List<BapiReturn2> bapiReturn2;

	public String getHblnumber() {
		return hblnumber;
	}

	public void setHblnumber(String hblnumber) {
		this.hblnumber = hblnumber;
	}

	public String getZsalesdocument() {
		return zsalesdocument;
	}

	public void setZsalesdocument(String zsalesdocument) {
		this.zsalesdocument = zsalesdocument;
	}

	public List<BapiReturn2> getBapiReturn2() {
		return bapiReturn2;
	}

	public void setBapiReturn2(List<BapiReturn2> bapiReturn2) {
		this.bapiReturn2 = bapiReturn2;
	}

}
