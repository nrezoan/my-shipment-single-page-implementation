package com.myshipment.model;

import java.util.List;


/**
 * @author Virendra Kumar
 *
 */

public class UserDetails {
	private static final long serialVersionUID=2324536464L;
	private BapiReturn1 bapiReturn1;
	private String partner;
	//private List<SalesArea> salesAreas;
	private String accgroup;
	private PeAddressBean peAddress;
	
	private BapiReturn2 bapiReturn2;
	
	private List<SalesorgListBean> salesorgList; 
	private List<CompcodeListBean> companycodeList; 
	private List<Salesareas> salesAreas; 

	public BapiReturn1 getBapiReturn1() {
		return bapiReturn1;
	}
	public void setBapiReturn1(BapiReturn1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
/*	public List<SalesArea> getSalesAreas() {
		return salesAreas;
	}
	public void setSalesAreas(List<SalesArea> salesAreas) {
		this.salesAreas = salesAreas;
	}*/
	public String getAccgroup() {
		return accgroup;
	}
	public void setAccgroup(String accgroup) {
		this.accgroup = accgroup;
	}
	public BapiReturn2 getBapiReturn2() {
		return bapiReturn2;
	}
	public void setBapiReturn2(BapiReturn2 bapiReturn2) {
		this.bapiReturn2 = bapiReturn2;
	}
	public PeAddressBean getPeAddress() {
		return peAddress;
	}
	public void setPeAddress(PeAddressBean peAddress) {
		this.peAddress = peAddress;
	}
	public List<SalesorgListBean> getSalesorgList() {
		return salesorgList;
	}
	public void setSalesorgList(List<SalesorgListBean> salesorgList) {
		this.salesorgList = salesorgList;
	}
	public List<CompcodeListBean> getCompanycodeList() {
		return companycodeList;
	}
	public void setCompanycodeList(List<CompcodeListBean> companycodeList) {
		this.companycodeList = companycodeList;
	}
	public List<Salesareas> getSalesAreas() {
		return salesAreas;
	}
	public void setSalesAreas(List<Salesareas> salesAreas) {
		this.salesAreas = salesAreas;
	}
	


}
