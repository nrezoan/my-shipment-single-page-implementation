package com.myshipment.model;

public class SalesOrgWiseShipCount {
	private static final long serialVersionUID=-688765L;
	
	private long shipmentCount;
	private String salesOrg;
	private String salesOrgName;
	public long getShipmentCount() {
		return shipmentCount;
	}
	public void setShipmentCount(long shipmentCount) {
		this.shipmentCount = shipmentCount;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getSalesOrgName() {
		return salesOrgName;
	}
	public void setSalesOrgName(String salesOrgName) {
		this.salesOrgName = salesOrgName;
	}
	@Override
	public String toString() {
		return "SalesOrgWiseShipCount [shipmentCount=" + shipmentCount + ", salesOrg=" + salesOrg + ", salesOrgName="
				+ salesOrgName + "]";
	}

}
