package com.myshipment.model;

import java.util.List;

public class SuppwiseShipDetailsbyBLstatus {
	private List<SalesOrgWiseShipmentJson>  lstSalesOrgWiseShipmentJson;
	private String message;
	
	
	public List<SalesOrgWiseShipmentJson> getLstSalesOrgWiseShipmentJson() {
		return lstSalesOrgWiseShipmentJson;
	}
	public void setLstSalesOrgWiseShipmentJson(List<SalesOrgWiseShipmentJson> lstSalesOrgWiseShipmentJson) {
		this.lstSalesOrgWiseShipmentJson = lstSalesOrgWiseShipmentJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
