package com.myshipment.model;

import java.io.Serializable;
import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class ItScheduleDetailBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private String pol;	
	//private Date pol_etd;	
	private String pol_etd;	
	private String transhipment_port;	
	//private Date  transhipment_port_eta;
	private String  transhipment_port_eta;
	//private Date  transhipment_port_etd;	
	private String  transhipment_port_etd;	
	private String transhipment2_port;	
	//private Date  transhipment2_port_eta;
	private String  transhipment2_port_eta;
	//private Date  transhipment2_port_etd;	
	private String  transhipment2_port_etd;	
	private String pod;	
	//private Date pod_eta;
	private String pod_eta;
	
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getTranshipment_port() {
		return transhipment_port;
	}
	public void setTranshipment_port(String transhipment_port) {
		this.transhipment_port = transhipment_port;
	}

	public String getTranshipment2_port() {
		return transhipment2_port;
	}
	public void setTranshipment2_port(String transhipment2_port) {
		this.transhipment2_port = transhipment2_port;
	}

	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getPol_etd() {
		return pol_etd;
	}
	public void setPol_etd(String pol_etd) {
		this.pol_etd = pol_etd;
	}
	public String getTranshipment_port_eta() {
		return transhipment_port_eta;
	}
	public void setTranshipment_port_eta(String transhipment_port_eta) {
		this.transhipment_port_eta = transhipment_port_eta;
	}
	public String getTranshipment_port_etd() {
		return transhipment_port_etd;
	}
	public void setTranshipment_port_etd(String transhipment_port_etd) {
		this.transhipment_port_etd = transhipment_port_etd;
	}
	public String getTranshipment2_port_eta() {
		return transhipment2_port_eta;
	}
	public void setTranshipment2_port_eta(String transhipment2_port_eta) {
		this.transhipment2_port_eta = transhipment2_port_eta;
	}
	public String getTranshipment2_port_etd() {
		return transhipment2_port_etd;
	}
	public void setTranshipment2_port_etd(String transhipment2_port_etd) {
		this.transhipment2_port_etd = transhipment2_port_etd;
	}
	public String getPod_eta() {
		return pod_eta;
	}
	public void setPod_eta(String pod_eta) {
		this.pod_eta = pod_eta;
	}

	
	
	
}
