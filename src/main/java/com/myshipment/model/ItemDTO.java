package com.myshipment.model;

import java.math.BigDecimal;
/*
 * @Ranjeet Kumar
 */
public class ItemDTO {

	private String soNumber;		
	private String itemNumber;	
	private String maretial;	
	private BigDecimal quantity;	
	private String materialDescription;	
	private String poNumber;	
	private String styleNo;	
	private String articleNo;	
	private String skuNo;
	private String colour;	
	private String size;	
	private BigDecimal totalPcs;	
	private String unit;	
	private String unitCost;	
	private String unitCostUnit;	
	private String hsCose;	
	private String invoiceDate;	
	private String netCost;	
	private BigDecimal pcsPerCartoon;	
	private BigDecimal cbm;	
	private BigDecimal wtPerCartoon;	
	private BigDecimal netWtPerCartoon;	
	private BigDecimal length;	
	private BigDecimal width;	
	private BigDecimal height;	
	private String inHcm;	
	private String carSerlNo;	
	private String reference1;	
	private String reference2;	
	private String reference3;	
	private String reference4;	
	private String qcDate;	
	private String orderReleaseDate;	
	private String commInvoiceNo;	
	private BigDecimal volume;	
	private BigDecimal grWt;	
	private BigDecimal netWt;	
	private String grDate;	
	private String stuffingDate;	
	private String grFlag;	
	private String shipmentFlag;	
	private String reference5;
	private BigDecimal volumeWt;
	
	public BigDecimal getVolumeWt() {
		return volumeWt;
	}
	public void setVolumeWt(BigDecimal volumeWt) {
		this.volumeWt = volumeWt;
	}
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getMaretial() {
		return maretial;
	}
	public void setMaretial(String maretial) {
		this.maretial = maretial;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getMaterialDescription() {
		return materialDescription;
	}
	public void setMaterialDescription(String materialDescription) {
		this.materialDescription = materialDescription;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getStyleNo() {
		return styleNo;
	}
	public void setStyleNo(String styleNo) {
		this.styleNo = styleNo;
	}
	public String getArticleNo() {
		return articleNo;
	}
	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}
	public String getSkuNo() {
		return skuNo;
	}
	public void setSkuNo(String skuNo) {
		this.skuNo = skuNo;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public BigDecimal getTotalPcs() {
		return totalPcs;
	}
	public void setTotalPcs(BigDecimal totalPcs) {
		this.totalPcs = totalPcs;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}
	public String getUnitCostUnit() {
		return unitCostUnit;
	}
	public void setUnitCostUnit(String unitCostUnit) {
		this.unitCostUnit = unitCostUnit;
	}
	public String getHsCose() {
		return hsCose;
	}
	public void setHsCose(String hsCose) {
		this.hsCose = hsCose;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getNetCost() {
		return netCost;
	}
	public void setNetCost(String netCost) {
		this.netCost = netCost;
	}
	public BigDecimal getPcsPerCartoon() {
		return pcsPerCartoon;
	}
	public void setPcsPerCartoon(BigDecimal pcsPerCartoon) {
		this.pcsPerCartoon = pcsPerCartoon;
	}
	public BigDecimal getCbm() {
		return cbm;
	}
	public void setCbm(BigDecimal cbm) {
		this.cbm = cbm;
	}
	public BigDecimal getWtPerCartoon() {
		return wtPerCartoon;
	}
	public void setWtPerCartoon(BigDecimal wtPerCartoon) {
		this.wtPerCartoon = wtPerCartoon;
	}
	public BigDecimal getNetWtPerCartoon() {
		return netWtPerCartoon;
	}
	public void setNetWtPerCartoon(BigDecimal netWtPerCartoon) {
		this.netWtPerCartoon = netWtPerCartoon;
	}
	public BigDecimal getLength() {
		return length;
	}
	public void setLength(BigDecimal length) {
		this.length = length;
	}
	public BigDecimal getWidth() {
		return width;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	public String getInHcm() {
		return inHcm;
	}
	public void setInHcm(String inHcm) {
		this.inHcm = inHcm;
	}
	public String getCarSerlNo() {
		return carSerlNo;
	}
	public void setCarSerlNo(String carSerlNo) {
		this.carSerlNo = carSerlNo;
	}
	public String getReference1() {
		return reference1;
	}
	public void setReference1(String reference1) {
		this.reference1 = reference1;
	}
	public String getReference2() {
		return reference2;
	}
	public void setReference2(String reference2) {
		this.reference2 = reference2;
	}
	public String getReference3() {
		return reference3;
	}
	public void setReference3(String reference3) {
		this.reference3 = reference3;
	}
	public String getReference4() {
		return reference4;
	}
	public void setReference4(String reference4) {
		this.reference4 = reference4;
	}
	public String getQcDate() {
		return qcDate;
	}
	public void setQcDate(String qcDate) {
		this.qcDate = qcDate;
	}
	public String getOrderReleaseDate() {
		return orderReleaseDate;
	}
	public void setOrderReleaseDate(String orderReleaseDate) {
		this.orderReleaseDate = orderReleaseDate;
	}
	public String getCommInvoiceNo() {
		return commInvoiceNo;
	}
	public void setCommInvoiceNo(String commInvoiceNo) {
		this.commInvoiceNo = commInvoiceNo;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getGrWt() {
		return grWt;
	}
	public void setGrWt(BigDecimal grWt) {
		this.grWt = grWt;
	}
	public BigDecimal getNetWt() {
		return netWt;
	}
	public void setNetWt(BigDecimal netWt) {
		this.netWt = netWt;
	}
	public String getGrDate() {
		return grDate;
	}
	public void setGrDate(String grDate) {
		this.grDate = grDate;
	}
	public String getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(String stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getGrFlag() {
		return grFlag;
	}
	public void setGrFlag(String grFlag) {
		this.grFlag = grFlag;
	}
	public String getShipmentFlag() {
		return shipmentFlag;
	}
	public void setShipmentFlag(String shipmentFlag) {
		this.shipmentFlag = shipmentFlag;
	}
	public String getReference5() {
		return reference5;
	}
	public void setReference5(String reference5) {
		this.reference5 = reference5;
	}
	
	
}
