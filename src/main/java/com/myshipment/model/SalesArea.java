package com.myshipment.model;

public class SalesArea {
     
	private static final long serialVersionUID=34254634l;
	private String compCode;
	private String compName;
	private String salesOrg;
	private String distrChn;
	private String division;

	public String getCompCode() {
		return compCode;
	}

	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getDistrChn() {
		return distrChn;
	}

	public void setDistrChn(String distrChn) {
		this.distrChn = distrChn;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

}
