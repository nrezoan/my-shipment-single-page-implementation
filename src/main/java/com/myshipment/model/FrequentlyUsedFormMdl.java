package com.myshipment.model;

import java.util.Date;
/*
 * @ Gufranur Rahman
 */
public class FrequentlyUsedFormMdl {

	private long pageFeq_Id;
	public String customerCode = "";
	public String company = "";
	public String distChannel = "";
	public String divison = "";
	public String pageName="";
	public String context ="";
	public int counter;
	
	public long getPageFeq_Id() {
		return pageFeq_Id;
	}
	public void setPageFeq_Id(long pageFeq_Id) {
		this.pageFeq_Id = pageFeq_Id;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getDivison() {
		return divison;
	}
	public void setDivison(String divison) {
		this.divison = divison;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	
		
}
