package com.myshipment.model;

import java.util.Set;
/*
 * @Ranjeet Kumar
 */
public class PoBookingPacklistDetailBean {

	private Integer plId;		
	private String hblNumber;	
	private String zSalesDocument;	
	private String ctnSrlNo;	
	private Integer noOfContainers;
	private Set<PoBookingPacklistColorBean> packlistColors;
	
	public Integer getPlId() {
		return plId;
	}
	public void setPlId(Integer plId) {
		this.plId = plId;
	}
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getzSalesDocument() {
		return zSalesDocument;
	}
	public void setzSalesDocument(String zSalesDocument) {
		this.zSalesDocument = zSalesDocument;
	}
	public String getCtnSrlNo() {
		return ctnSrlNo;
	}
	public void setCtnSrlNo(String ctnSrlNo) {
		this.ctnSrlNo = ctnSrlNo;
	}
	public Integer getNoOfContainers() {
		return noOfContainers;
	}
	public void setNoOfContainers(Integer noOfContainers) {
		this.noOfContainers = noOfContainers;
	}
	public Set<PoBookingPacklistColorBean> getPacklistColors() {
		return packlistColors;
	}
	public void setPacklistColors(Set<PoBookingPacklistColorBean> packlistColors) {
		this.packlistColors = packlistColors;
	}

	
}
