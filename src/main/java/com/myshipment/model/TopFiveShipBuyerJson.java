package com.myshipment.model;

import java.util.List;

public class TopFiveShipBuyerJson {
	private List<TopFiveShipmentBuyer> lstTopFiveShipmentBuyer;
	private String message;
	public List<TopFiveShipmentBuyer> getLstTopFiveShipmentBuyer() {
		return lstTopFiveShipmentBuyer;
	}
	public void setLstTopFiveShipmentBuyer(List<TopFiveShipmentBuyer> lstTopFiveShipmentBuyer) {
		this.lstTopFiveShipmentBuyer = lstTopFiveShipmentBuyer;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
