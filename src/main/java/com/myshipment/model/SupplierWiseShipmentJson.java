package com.myshipment.model;

public class SupplierWiseShipmentJson {
	private String shipperNo;
	private String shipperName;
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String supplierNo) {
		this.shipperNo = supplierNo;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String supplierName) {
		this.shipperName = supplierName;
	}
	public double getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(double totalShipment) {
		this.totalShipment = totalShipment;
	}
	public double getTotalShipmentPerc() {
		return totalShipmentPerc;
	}
	public void setTotalShipmentPerc(double totalShipmentPerc) {
		this.totalShipmentPerc = totalShipmentPerc;
	}
	private double totalShipment;
	private double totalShipmentPerc;

}
