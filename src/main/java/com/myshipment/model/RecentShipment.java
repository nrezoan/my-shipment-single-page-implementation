/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.util.Date;

public class RecentShipment {

	private String hblNo;
	private String name;
	private String pol;
	private String pod;
	private String status;
	private Date bookingDate;
	private Date eta;
	private Date etd;
	
	
	public String getHblNo() {
		return hblNo;
	}
	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public Date getEta() {
		return eta;
	}
	public void setEta(Date eta) {
		this.eta = eta;
	}
	public Date getEtd() {
		return etd;
	}
	public void setEtd(Date etd) {
		this.etd = etd;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
