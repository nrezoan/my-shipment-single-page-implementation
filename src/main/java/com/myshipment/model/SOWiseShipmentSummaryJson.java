package com.myshipment.model;

import java.util.List;

public class SOWiseShipmentSummaryJson {
	private List<SalesOrgWiseShipCount> lstSalesOrgWiseShipCount;
	private long totalShipmentCount;
	private double totalCBMCount;
	private double totalGWTCount;
	private double blPendingPercentage;
	private double blReleasePercentage;
	private long pendingBlCount;
	private long releasedBlCount;
	private String message;
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<SalesOrgWiseShipCount> getLstSalesOrgWiseShipCount() {
		return lstSalesOrgWiseShipCount;
	}
	
	public void setLstSalesOrgWiseShipCount(List<SalesOrgWiseShipCount> lstSalesOrgWiseShipCount) {
		this.lstSalesOrgWiseShipCount = lstSalesOrgWiseShipCount;
	}
	public long getTotalShipmentCount() {
		return totalShipmentCount;
	}
	public void setTotalShipmentCount(long totalShipmentCount) {
		this.totalShipmentCount = totalShipmentCount;
	}
	public double getTotalCBMCount() {
		return totalCBMCount;
	}
	public void setTotalCBMCount(double totalCBMCount) {
		this.totalCBMCount = totalCBMCount;
	}
	public double getTotalGWTCount() {
		return totalGWTCount;
	}
	public void setTotalGWTCount(double totalGWTCount) {
		this.totalGWTCount = totalGWTCount;
	}
	public double getBlPendingPercentage() {
		return blPendingPercentage;
	}
	
	public double getBlReleasePercentage() {
		return blReleasePercentage;
	}

	public void setBlReleasePercentage(double blReleasePercentage) {
		this.blReleasePercentage = blReleasePercentage;
	}

	public long getPendingBlCount() {
		return pendingBlCount;
	}

	public void setPendingBlCount(long pendingBlCount) {
		this.pendingBlCount = pendingBlCount;
	}

	public long getReleasedBlCount() {
		return releasedBlCount;
	}

	public void setReleasedBlCount(long releasedBlCount) {
		this.releasedBlCount = releasedBlCount;
	}

	public void setBlPendingPercentage(double blPendingPercentage) {
		this.blPendingPercentage = blPendingPercentage;
	}

	@Override
	public String toString() {
		return "SOWiseShipmentSummaryJson [lstSalesOrgWiseShipCount=" + lstSalesOrgWiseShipCount
				+ ", totalShipmentCount=" + totalShipmentCount + ", totalCBMCount=" + totalCBMCount + ", totalGWTCount="
				+ totalGWTCount + ", blPendingPercentage=" + blPendingPercentage + ", blReleasePercentage="
				+ blReleasePercentage + ", pendingBlCount=" + pendingBlCount + ", releasedBlCount=" + releasedBlCount
				+ ", message=" + message + "]";
	}

	
}
