/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.util.List;

public class DashboardInfoBox {

	private double totalGWT;
	private double totalCBM;
	private double totalCRGWT;
	private Integer totalShipment;
	private Integer inTransit;
	private Integer openOrder;
	private Integer goodsReceived;
	private Integer stuffingDone;
	private Integer delivered;
	private List<MyshipmentTrackHeaderBean> totalShipmentList;
	private List<MyshipmentTrackHeaderBean> inTransitList;
	private List<MyshipmentTrackHeaderBean> openOrderList;
	private List<MyshipmentTrackHeaderBean> goodsReceivedList;
	private List<MyshipmentTrackHeaderBean> stuffingDoneList;
	private List<MyshipmentTrackHeaderBean> deliveredList;
	
	public Integer getGoodsReceived() {
		return goodsReceived;
	}
	public void setGoodsReceived(Integer goodsReceived) {
		this.goodsReceived = goodsReceived;
	}
	public Integer getStuffingDone() {
		return stuffingDone;
	}
	public void setStuffingDone(Integer stuffingDone) {
		this.stuffingDone = stuffingDone;
	}
	public Integer getDelivered() {
		return delivered;
	}
	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}
	public List<MyshipmentTrackHeaderBean> getGoodsReceivedList() {
		return goodsReceivedList;
	}
	public void setGoodsReceivedList(List<MyshipmentTrackHeaderBean> goodsReceivedList) {
		this.goodsReceivedList = goodsReceivedList;
	}
	public List<MyshipmentTrackHeaderBean> getStuffingDoneList() {
		return stuffingDoneList;
	}
	public void setStuffingDoneList(List<MyshipmentTrackHeaderBean> stuffingDoneList) {
		this.stuffingDoneList = stuffingDoneList;
	}
	public List<MyshipmentTrackHeaderBean> getDeliveredList() {
		return deliveredList;
	}
	public void setDeliveredList(List<MyshipmentTrackHeaderBean> deliveredList) {
		this.deliveredList = deliveredList;
	}
	public double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public double getTotalCRGWT() {
		return totalCRGWT;
	}
	public void setTotalCRGWT(double totalCRGWT) {
		this.totalCRGWT = totalCRGWT;
	}
	public Integer getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(Integer totalShipment) {
		this.totalShipment = totalShipment;
	}
	public List<MyshipmentTrackHeaderBean> getInTransitList() {
		return inTransitList;
	}
	public void setInTransitList(List<MyshipmentTrackHeaderBean> inTransitList) {
		this.inTransitList = inTransitList;
	}
	public Integer getInTransit() {
		return inTransit;
	}
	public void setInTransit(Integer inTransit) {
		this.inTransit = inTransit;
	}
	public Integer getOpenOrder() {
		return openOrder;
	}
	public void setOpenOrder(Integer openOrder) {
		this.openOrder = openOrder;
	}
	public List<MyshipmentTrackHeaderBean> getOpenOrderList() {
		return openOrderList;
	}
	public void setOpenOrderList(List<MyshipmentTrackHeaderBean> openOrderList) {
		this.openOrderList = openOrderList;
	}
	public List<MyshipmentTrackHeaderBean> getTotalShipmentList() {
		return totalShipmentList;
	}
	public void setTotalShipmentList(List<MyshipmentTrackHeaderBean> totalShipmentList) {
		this.totalShipmentList = totalShipmentList;
	}
	
}
