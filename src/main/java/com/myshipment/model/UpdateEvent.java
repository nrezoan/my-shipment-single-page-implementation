package com.myshipment.model;

public class UpdateEvent {
	private long up_ev_id;
	private String vc_po_no;
	private Long seg_id;
	private String action;
	private double items;
	public long getUp_ev_id() {
		return up_ev_id;
	}
	public void setUp_ev_id(long up_ev_id) {
		this.up_ev_id = up_ev_id;
	}
	public String getVc_po_no() {
		return vc_po_no;
	}
	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}
	public Long getSeg_id() {
		return seg_id;
	}
	public void setSeg_id(Long seg_id) {
		this.seg_id = seg_id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public double getItems() {
		return items;
	}
	public void setItems(double items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "UpdateEvent [up_ev_id=" + up_ev_id + ", vc_po_no=" + vc_po_no + ", seg_id=" + seg_id + ", action="
				+ action + ", items=" + items + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		long temp;
		temp = Double.doubleToLongBits(items);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((seg_id == null) ? 0 : seg_id.hashCode());
		result = prime * result + (int) (up_ev_id ^ (up_ev_id >>> 32));
		result = prime * result + ((vc_po_no == null) ? 0 : vc_po_no.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UpdateEvent other = (UpdateEvent) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (Double.doubleToLongBits(items) != Double.doubleToLongBits(other.items))
			return false;
		if (seg_id == null) {
			if (other.seg_id != null)
				return false;
		} else if (!seg_id.equals(other.seg_id))
			return false;
		if (up_ev_id != other.up_ev_id)
			return false;
		if (vc_po_no == null) {
			if (other.vc_po_no != null)
				return false;
		} else if (!vc_po_no.equals(other.vc_po_no))
			return false;
		return true;
	}
	

}
