package com.myshipment.model;

public class TDocHeader {

	private String inco1;
	private String carrier;
	private String shipmentNo;
	private String pol;
	public String getInco1() {
		return inco1;
	}
	public String getCarrier() {
		return carrier;
	}
	public String getShipmentNo() {
		return shipmentNo;
	}
	public String getPol() {
		return pol;
	}
	public void setInco1(String inco1) {
		this.inco1 = inco1;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public void setShipmentNo(String shipmentNo) {
		this.shipmentNo = shipmentNo;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}

	
}
