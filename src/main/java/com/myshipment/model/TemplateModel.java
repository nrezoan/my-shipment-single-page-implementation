package com.myshipment.model;

import java.util.Date;

public class TemplateModel {
	private String template_id;
	private int active;
	private String total_gw;
	private String carton_length;
	private String carton_height;
	private String carton_width;
	private String total_pieces;
	private String carton_quantity;
	private String carton_unit;
	private String total_cbm;
	private String hs_code;
	private String commodity;
	private String style_no;
	private String wh_code;
	private String size_no;
	private String sku;
	private String article_no;
	private String color;
	private String total_nw;
	private String comm_inv;
	private String comm_inv_date;
	private String pieces_ctn;
	private String carton_sr_no;
	private String reference_1;
	private String reference_2;
	private String reference_3;
	private String terms_of_shipment;
	private String freight_mode;
	private String division;
	private String pol;
	private String pod;
	private String place_of_delivery;
	private String place_of_delivery_address;
	private String cargo_handover_date;
	private String po_number;
	private Date entry_date;
	private String entry_by;
	private String shipper;

	// Added on 11 November 2018 by Tahmid Alam

	private String fvsl;
	private String mvsl1;
	private String mvsl2;
	private String mvsl3;

	private String voyage1;
	private String voyage2;
	private String voyage3;

	private String etd;
	private String transhipment1etd;
	private String transhipment2etd;
	private String ata;
	private String atd;
	private String transhipment1eta;
	private String transhipment2eta;
	private String eta;

		////////
	
	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public Date getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(Date entry_date) {
		this.entry_date = entry_date;
	}

	public String getEntry_by() {
		return entry_by;
	}

	public void setEntry_by(String entry_by) {
		this.entry_by = entry_by;
	}

	public String getPo_number() {
		return po_number;
	}

	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}

	public String getTemplate_id() {
		return template_id;
	}

	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getTotal_gw() {
		return total_gw;
	}

	public void setTotal_gw(String total_gw) {
		this.total_gw = total_gw;
	}

	public String getCarton_length() {
		return carton_length;
	}

	public void setCarton_length(String carton_length) {
		this.carton_length = carton_length;
	}

	public String getCarton_height() {
		return carton_height;
	}

	public void setCarton_height(String carton_height) {
		this.carton_height = carton_height;
	}

	public String getCarton_width() {
		return carton_width;
	}

	public void setCarton_width(String carton_width) {
		this.carton_width = carton_width;
	}

	public String getTotal_pieces() {
		return total_pieces;
	}

	public void setTotal_pieces(String total_pieces) {
		this.total_pieces = total_pieces;
	}

	public String getCarton_quantity() {
		return carton_quantity;
	}

	public void setCarton_quantity(String carton_quantity) {
		this.carton_quantity = carton_quantity;
	}

	public String getCarton_unit() {
		return carton_unit;
	}

	public void setCarton_unit(String carton_unit) {
		this.carton_unit = carton_unit;
	}

	public String getTotal_cbm() {
		return total_cbm;
	}

	public void setTotal_cbm(String total_cbm) {
		this.total_cbm = total_cbm;
	}

	public String getHs_code() {
		return hs_code;
	}

	public void setHs_code(String hs_code) {
		this.hs_code = hs_code;
	}

	public String getCommodity() {
		return commodity;
	}

	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}

	public String getStyle_no() {
		return style_no;
	}

	public void setStyle_no(String style_no) {
		this.style_no = style_no;
	}

	public String getWh_code() {
		return wh_code;
	}

	public void setWh_code(String wh_code) {
		this.wh_code = wh_code;
	}

	public String getSize_no() {
		return size_no;
	}

	public void setSize_no(String size_no) {
		this.size_no = size_no;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getArticle_no() {
		return article_no;
	}

	public void setArticle_no(String article_no) {
		this.article_no = article_no;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTotal_nw() {
		return total_nw;
	}

	public void setTotal_nw(String total_nw) {
		this.total_nw = total_nw;
	}

	public String getComm_inv() {
		return comm_inv;
	}

	public void setComm_inv(String comm_inv) {
		this.comm_inv = comm_inv;
	}

	public String getComm_inv_date() {
		return comm_inv_date;
	}

	public void setComm_inv_date(String comm_inv_date) {
		this.comm_inv_date = comm_inv_date;
	}

	public String getPieces_ctn() {
		return pieces_ctn;
	}

	public void setPieces_ctn(String pieces_ctn) {
		this.pieces_ctn = pieces_ctn;
	}

	public String getCarton_sr_no() {
		return carton_sr_no;
	}

	public void setCarton_sr_no(String carton_sr_no) {
		this.carton_sr_no = carton_sr_no;
	}

	public String getReference_1() {
		return reference_1;
	}

	public void setReference_1(String reference_1) {
		this.reference_1 = reference_1;
	}

	public String getReference_2() {
		return reference_2;
	}

	public void setReference_2(String reference_2) {
		this.reference_2 = reference_2;
	}

	public String getReference_3() {
		return reference_3;
	}

	public void setReference_3(String reference_3) {
		this.reference_3 = reference_3;
	}

	public String getTerms_of_shipment() {
		return terms_of_shipment;
	}

	public void setTerms_of_shipment(String terms_of_shipment) {
		this.terms_of_shipment = terms_of_shipment;
	}

	public String getFreight_mode() {
		return freight_mode;
	}

	public void setFreight_mode(String freight_mode) {
		this.freight_mode = freight_mode;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getPlace_of_delivery() {
		return place_of_delivery;
	}

	public void setPlace_of_delivery(String place_of_delivery) {
		this.place_of_delivery = place_of_delivery;
	}

	public String getPlace_of_delivery_address() {
		return place_of_delivery_address;
	}

	public void setPlace_of_delivery_address(String place_of_delivery_address) {
		this.place_of_delivery_address = place_of_delivery_address;
	}

	public String getCargo_handover_date() {
		return cargo_handover_date;
	}

	public void setCargo_handover_date(String cargo_handover_date) {
		this.cargo_handover_date = cargo_handover_date;
	}

	public String getFvsl() {
		return fvsl;
	}

	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}

	public String getMvsl1() {
		return mvsl1;
	}

	public void setMvsl1(String mvsl1) {
		this.mvsl1 = mvsl1;
	}

	public String getMvsl2() {
		return mvsl2;
	}

	public void setMvsl2(String mvsl2) {
		this.mvsl2 = mvsl2;
	}

	public String getMvsl3() {
		return mvsl3;
	}

	public void setMvsl3(String mvsl3) {
		this.mvsl3 = mvsl3;
	}

	public String getVoyage1() {
		return voyage1;
	}

	public void setVoyage1(String voyage1) {
		this.voyage1 = voyage1;
	}

	public String getVoyage2() {
		return voyage2;
	}

	public void setVoyage2(String voyage2) {
		this.voyage2 = voyage2;
	}

	public String getVoyage3() {
		return voyage3;
	}

	public void setVoyage3(String voyage3) {
		this.voyage3 = voyage3;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getTranshipment1etd() {
		return transhipment1etd;
	}

	public void setTranshipment1etd(String transhipment1etd) {
		this.transhipment1etd = transhipment1etd;
	}

	public String getTranshipment2etd() {
		return transhipment2etd;
	}

	public void setTranshipment2etd(String transhipment2etd) {
		this.transhipment2etd = transhipment2etd;
	}

	public String getAta() {
		return ata;
	}

	public void setAta(String ata) {
		this.ata = ata;
	}

	public String getAtd() {
		return atd;
	}

	public void setAtd(String atd) {
		this.atd = atd;
	}

	public String getTranshipment1eta() {
		return transhipment1eta;
	}

	public void setTranshipment1eta(String transhipment1eta) {
		this.transhipment1eta = transhipment1eta;
	}

	public String getTranshipment2eta() {
		return transhipment2eta;
	}

	public void setTranshipment2eta(String transhipment2eta) {
		this.transhipment2eta = transhipment2eta;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

}
