package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;
/*
 * @ Hamid
 */
public class MyshipmentTrackItemBean {
	//@Parameter("SO_NO")
	private String so_no;
	//@Parameter("BL_NO")
	private String bl_no;
	//@Parameter("ITEM_NO")
	private String item_no;
	//@Parameter("MATERIAL")
	private String material;
	//@Parameter("COMMODITY")
	private String commodity;
	//@Parameter("ITEM_QTY")
	private BigDecimal item_qty;
	//@Parameter("T_ITEM-BL_DT")
	private String item_bl_dt;
	private Date item_bl_dt_openTracking;
	//@Parameter("T_ITEM-EXP_CH_DT")
	private String item_chdt;
	private Date item_chdt_openTracking;
	//@Parameter("PO_NO")
	private String po_no;
	//@Parameter("STYLE")
	private String style;
	//@Parameter("SKU")
	private String sku;
	//@Parameter("ART_NO")
	private String art_no;
	//@Parameter("IND_NO")
	private String ind_no;
	//@Parameter("HSCODE")
	private String hscode;
	//@Parameter("COLOR")
	private String color;
	//@Parameter("SIZE")
	private String size;
	//@Parameter("CART_SNO")
	private String cart_sno;
	//@Parameter("UOM")
	private String uom;
	//@Parameter("LENGTH")
	private BigDecimal length;
	//@Parameter("WIDTH")
	private BigDecimal width;
	//@Parameter("HEIGHT")
	private BigDecimal height;
	//@Parameter("IN_CM")
	private String in_cm;
	//@Parameter("ITEM_PCS")
	private BigDecimal item_pcs;
	//@Parameter("ITEM_VOL")
	private BigDecimal item_vol;
	//@Parameter("ITEM_GRWT")
	private BigDecimal item_grwt;
	//@Parameter("ITEM_NTWT")
	private BigDecimal item_ntwt;
	//@Parameter("ITEM_VLWT")
	private BigDecimal item_vlwt;
	//@Parameter("ULD_NO")
	private String uld_no;
	//@Parameter("PROD_CODE")
	private String prod_code;
	//@Parameter("REF1_NO")
	private String ref1_no;
	//@Parameter("REF1_DT")
	private String ref1_dt;
	private Date ref1_dt_openTracking;
	//@Parameter("REF2_NO")
	private String ref2_no;
	//@Parameter("REF2_DT")
	private String ref2_dt;
	private Date ref2_dt_openTracking;
	//@Parameter("REF3_NO")
	private String ref3_no;
	//@Parameter("REF3_DT")
	private String ref3_dt;
	private Date ref3_dt_openTracking;
	//@Parameter("REF4_NO")
	private String ref4_no;
	//@Parameter("REF4_DT")
	private String ref4_dt;
	private Date ref4_dt_openTracking;
	//@Parameter("REF5_NO")
	private String ref5_no;
	//@Parameter("REF5_DT")
	private String ref5_dt;
	private Date ref5_dt_openTracking;
	//@Parameter("QC_DT")
	private String qc_dt;
	private Date qc_dt_openTracking;
	//@Parameter("REL_DT")
	private String rel_dt;
	private Date rel_dt_openTracking;
	//@Parameter("USER_CODE")
	private String user_code;
	//@Parameter("SUPP_CODE")
	private String supp_code;
	//@Parameter("CUST_CODE")
	private String cust_code;
	//@Parameter("CC_ALOC")
	private String cc_aloc;
	//@Parameter("CC_ORIG")
	private String cc_orig;
	//@Parameter("CONT_NO")
	private String cont_no;
	//@Parameter("CONT_SIZE")
	private String cont_size;
	//@Parameter("SEAL_NO")
	private String seal_no;
	//@Parameter("CONT_MODE")
	private String cont_mode;
	//@Parameter("SEQ_NO")
	private String seq_no;
	
	public String getSo_no() {
		return so_no;
	}
	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getItem_no() {
		return item_no;
	}
	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	public BigDecimal getItem_qty() {
		return item_qty;
	}
	public void setItem_qty(BigDecimal item_qty) {
		this.item_qty = item_qty;
	}
	public String getItem_bl_dt() {
		return item_bl_dt;
	}
	public void setItem_bl_dt(String item_bl_dt) {
		this.item_bl_dt = item_bl_dt;
	}
	public String getItem_chdt() {
		return item_chdt;
	}
	public void setItem_chdt(String item_chdt) {
		this.item_chdt = item_chdt;
	}
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getArt_no() {
		return art_no;
	}
	public void setArt_no(String art_no) {
		this.art_no = art_no;
	}
	public String getInd_no() {
		return ind_no;
	}
	public void setInd_no(String ind_no) {
		this.ind_no = ind_no;
	}
	public String getHscode() {
		return hscode;
	}
	public void setHscode(String hscode) {
		this.hscode = hscode;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getCart_sno() {
		return cart_sno;
	}
	public void setCart_sno(String cart_sno) {
		this.cart_sno = cart_sno;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public BigDecimal getLength() {
		return length;
	}
	public void setLength(BigDecimal length) {
		this.length = length;
	}
	public BigDecimal getWidth() {
		return width;
	}
	public void setWidth(BigDecimal width) {
		this.width = width;
	}
	public BigDecimal getHeight() {
		return height;
	}
	public void setHeight(BigDecimal height) {
		this.height = height;
	}
	public String getIn_cm() {
		return in_cm;
	}
	public void setIn_cm(String in_cm) {
		this.in_cm = in_cm;
	}
	public BigDecimal getItem_pcs() {
		return item_pcs;
	}
	public void setItem_pcs(BigDecimal item_pcs) {
		this.item_pcs = item_pcs;
	}
	public BigDecimal getItem_vol() {
		return item_vol;
	}
	public void setItem_vol(BigDecimal item_vol) {
		this.item_vol = item_vol;
	}
	public BigDecimal getItem_grwt() {
		return item_grwt;
	}
	public void setItem_grwt(BigDecimal item_grwt) {
		this.item_grwt = item_grwt;
	}
	public BigDecimal getItem_ntwt() {
		return item_ntwt;
	}
	public void setItem_ntwt(BigDecimal item_ntwt) {
		this.item_ntwt = item_ntwt;
	}
	public BigDecimal getItem_vlwt() {
		return item_vlwt;
	}
	public void setItem_vlwt(BigDecimal item_vlwt) {
		this.item_vlwt = item_vlwt;
	}
	public String getUld_no() {
		return uld_no;
	}
	public void setUld_no(String uld_no) {
		this.uld_no = uld_no;
	}
	public String getProd_code() {
		return prod_code;
	}
	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}
	public String getRef1_no() {
		return ref1_no;
	}
	public void setRef1_no(String ref1_no) {
		this.ref1_no = ref1_no;
	}
	public String getRef1_dt() {
		return ref1_dt;
	}
	public void setRef1_dt(String ref1_dt) {
		this.ref1_dt = ref1_dt;
	}
	public String getRef2_no() {
		return ref2_no;
	}
	public void setRef2_no(String ref2_no) {
		this.ref2_no = ref2_no;
	}
	public String getRef2_dt() {
		return ref2_dt;
	}
	public void setRef2_dt(String ref2_dt) {
		this.ref2_dt = ref2_dt;
	}
	public String getRef3_no() {
		return ref3_no;
	}
	public void setRef3_no(String ref3_no) {
		this.ref3_no = ref3_no;
	}
	public String getRef3_dt() {
		return ref3_dt;
	}
	public void setRef3_dt(String ref3_dt) {
		this.ref3_dt = ref3_dt;
	}
	public String getRef4_no() {
		return ref4_no;
	}
	public void setRef4_no(String ref4_no) {
		this.ref4_no = ref4_no;
	}
	public String getRef4_dt() {
		return ref4_dt;
	}
	public void setRef4_dt(String ref4_dt) {
		this.ref4_dt = ref4_dt;
	}
	public String getRef5_no() {
		return ref5_no;
	}
	public void setRef5_no(String ref5_no) {
		this.ref5_no = ref5_no;
	}
	public String getRef5_dt() {
		return ref5_dt;
	}
	public void setRef5_dt(String ref5_dt) {
		this.ref5_dt = ref5_dt;
	}
	public String getQc_dt() {
		return qc_dt;
	}
	public void setQc_dt(String qc_dt) {
		this.qc_dt = qc_dt;
	}
	public String getRel_dt() {
		return rel_dt;
	}
	public void setRel_dt(String rel_dt) {
		this.rel_dt = rel_dt;
	}
	public String getUser_code() {
		return user_code;
	}
	public void setUser_code(String user_code) {
		this.user_code = user_code;
	}
	public String getSupp_code() {
		return supp_code;
	}
	public void setSupp_code(String supp_code) {
		this.supp_code = supp_code;
	}
	public String getCust_code() {
		return cust_code;
	}
	public void setCust_code(String cust_code) {
		this.cust_code = cust_code;
	}
	public String getCc_aloc() {
		return cc_aloc;
	}
	public void setCc_aloc(String cc_aloc) {
		this.cc_aloc = cc_aloc;
	}
	public String getCc_orig() {
		return cc_orig;
	}
	public void setCc_orig(String cc_orig) {
		this.cc_orig = cc_orig;
	}
	public String getCont_no() {
		return cont_no;
	}
	public void setCont_no(String cont_no) {
		this.cont_no = cont_no;
	}
	public String getCont_size() {
		return cont_size;
	}
	public void setCont_size(String cont_size) {
		this.cont_size = cont_size;
	}
	public String getSeal_no() {
		return seal_no;
	}
	public void setSeal_no(String seal_no) {
		this.seal_no = seal_no;
	}
	public String getCont_mode() {
		return cont_mode;
	}
	public void setCont_mode(String cont_mode) {
		this.cont_mode = cont_mode;
	}
	public String getSeq_no() {
		return seq_no;
	}
	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}
	public Date getItem_bl_dt_openTracking() {
		return item_bl_dt_openTracking;
	}
	public void setItem_bl_dt_openTracking(Date item_bl_dt_openTracking) {
		this.item_bl_dt_openTracking = item_bl_dt_openTracking;
	}
	public Date getItem_chdt_openTracking() {
		return item_chdt_openTracking;
	}
	public void setItem_chdt_openTracking(Date item_chdt_openTracking) {
		this.item_chdt_openTracking = item_chdt_openTracking;
	}
	public Date getRef1_dt_openTracking() {
		return ref1_dt_openTracking;
	}
	public void setRef1_dt_openTracking(Date ref1_dt_openTracking) {
		this.ref1_dt_openTracking = ref1_dt_openTracking;
	}
	public Date getRef2_dt_openTracking() {
		return ref2_dt_openTracking;
	}
	public void setRef2_dt_openTracking(Date ref2_dt_openTracking) {
		this.ref2_dt_openTracking = ref2_dt_openTracking;
	}
	public Date getRef3_dt_openTracking() {
		return ref3_dt_openTracking;
	}
	public void setRef3_dt_openTracking(Date ref3_dt_openTracking) {
		this.ref3_dt_openTracking = ref3_dt_openTracking;
	}
	public Date getRef4_dt_openTracking() {
		return ref4_dt_openTracking;
	}
	public void setRef4_dt_openTracking(Date ref4_dt_openTracking) {
		this.ref4_dt_openTracking = ref4_dt_openTracking;
	}
	public Date getRef5_dt_openTracking() {
		return ref5_dt_openTracking;
	}
	public void setRef5_dt_openTracking(Date ref5_dt_openTracking) {
		this.ref5_dt_openTracking = ref5_dt_openTracking;
	}
	public Date getQc_dt_openTracking() {
		return qc_dt_openTracking;
	}
	public void setQc_dt_openTracking(Date qc_dt_openTracking) {
		this.qc_dt_openTracking = qc_dt_openTracking;
	}
	public Date getRel_dt_openTracking() {
		return rel_dt_openTracking;
	}
	public void setRel_dt_openTracking(Date rel_dt_openTracking) {
		this.rel_dt_openTracking = rel_dt_openTracking;
	}
	
	
}
