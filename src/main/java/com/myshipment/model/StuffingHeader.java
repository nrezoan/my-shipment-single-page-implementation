package com.myshipment.model;

import java.util.Date;


/*
 * @ Gufranur Rahman
 */

public class StuffingHeader {

	private String vbeln;
	private String zzHblHawbNo;
	private String zzMblMabNo;
	private int zzSoQuantitiy;
	private int zzTotChrgWt;
	private Date auDat;
	private String zzComInvNo;
	private Date zzCommInvDt;
	private String zzLcpottNo;
	private Date zzLcDt;
	private String zzPortOfLoading;
	private String zzFreightMode;
	private String zzPortOfDest;
	private String zzPlacedelivery;
	private Date zzPodDt;
	private Date zzTrans3Etd;
	private String zzAirlineName1;
	private Date zzDepartureDt;
	private String zzAirlineNam21;
	private Date zzEta;
	private Date zzTrans1Etd;
	private String kunNr;
	private String name1;
	private String name2;
	private String name3;
	private String name4;
	private String name5;
	private Date zzStuffingDate;
	private String belad;
	private Date dptBg;
	private String knoTa;
	private String knoTz;
	private String zzFreightModeDes;
	private String zzAirLineNo1;
	private String zzAirLineNo2;
	private Date fvEtDport;	
	private Date fvEtAport;
	private Date mvEtDport;
	private Date mvEtAport;
	private Date grDt;
	private String companyCode;
	
	

	public Date getGrDt() {
		return grDt;
	}

	public void setGrDt(Date grDt) {
		this.grDt = grDt;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getVbeln() {
		return vbeln;
	}

	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}

	public String getZzHblHawbNo() {
		return zzHblHawbNo;
	}

	public void setZzHblHawbNo(String zzHblHawbNo) {
		this.zzHblHawbNo = zzHblHawbNo;
	}

	public String getZzMblMabNo() {
		return zzMblMabNo;
	}

	public void setZzMblMabNo(String zzMblMabNo) {
		this.zzMblMabNo = zzMblMabNo;
	}

	public int getZzSoQuantitiy() {
		return zzSoQuantitiy;
	}

	public void setZzSoQuantitiy(int zzSoQuantitiy) {
		this.zzSoQuantitiy = zzSoQuantitiy;
	}

	public int getZzTotChrgWt() {
		return zzTotChrgWt;
	}

	public void setZzTotChrgWt(int zzTotChrgWt) {
		this.zzTotChrgWt = zzTotChrgWt;
	}

	public Date getAuDat() {
		return auDat;
	}

	public void setAuDat(Date auDat) {
		this.auDat = auDat;
	}

	public String getZzComInvNo() {
		return zzComInvNo;
	}

	public void setZzComInvNo(String zzComInvNo) {
		this.zzComInvNo = zzComInvNo;
	}

	public Date getZzCommInvDt() {
		return zzCommInvDt;
	}

	public void setZzCommInvDt(Date zzCommInvDt) {
		this.zzCommInvDt = zzCommInvDt;
	}

	public String getZzLcpottNo() {
		return zzLcpottNo;
	}

	public void setZzLcpottNo(String zzLcpottNo) {
		this.zzLcpottNo = zzLcpottNo;
	}

	public Date getZzLcDt() {
		return zzLcDt;
	}

	public void setZzLcDt(Date zzLcDt) {
		this.zzLcDt = zzLcDt;
	}

	public String getZzPortOfLoading() {
		return zzPortOfLoading;
	}

	public void setZzPortOfLoading(String zzPortOfLoading) {
		this.zzPortOfLoading = zzPortOfLoading;
	}

	public String getZzFreightMode() {
		return zzFreightMode;
	}

	public void setZzFreightMode(String zzFreightMode) {
		this.zzFreightMode = zzFreightMode;
	}

	public String getZzPortOfDest() {
		return zzPortOfDest;
	}

	public void setZzPortOfDest(String zzPortOfDest) {
		this.zzPortOfDest = zzPortOfDest;
	}

	public String getZzPlacedelivery() {
		return zzPlacedelivery;
	}

	public void setZzPlacedelivery(String zzPlacedelivery) {
		this.zzPlacedelivery = zzPlacedelivery;
	}

	public Date getZzPodDt() {
		return zzPodDt;
	}

	public void setZzPodDt(Date zzPodDt) {
		this.zzPodDt = zzPodDt;
	}

	public Date getZzTrans3Etd() {
		return zzTrans3Etd;
	}

	public void setZzTrans3Etd(Date zzTrans3Etd) {
		this.zzTrans3Etd = zzTrans3Etd;
	}

	public String getZzAirlineName1() {
		return zzAirlineName1;
	}

	public void setZzAirlineName1(String zzAirlineName1) {
		this.zzAirlineName1 = zzAirlineName1;
	}

	public Date getZzDepartureDt() {
		return zzDepartureDt;
	}

	public void setZzDepartureDt(Date zzDepartureDt) {
		this.zzDepartureDt = zzDepartureDt;
	}

	public String getZzAirlineNam21() {
		return zzAirlineNam21;
	}

	public void setZzAirlineNam21(String zzAirlineNam21) {
		this.zzAirlineNam21 = zzAirlineNam21;
	}

	public Date getZzEta() {
		return zzEta;
	}

	public void setZzEta(Date zzEta) {
		this.zzEta = zzEta;
	}

	public Date getZzTrans1Etd() {
		return zzTrans1Etd;
	}

	public void setZzTrans1Etd(Date zzTrans1Etd) {
		this.zzTrans1Etd = zzTrans1Etd;
	}

	public String getKunNr() {
		return kunNr;
	}

	public void setKunNr(String kunNr) {
		this.kunNr = kunNr;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getName5() {
		return name5;
	}

	public void setName5(String name5) {
		this.name5 = name5;
	}

	public Date getZzStuffingDate() {
		return zzStuffingDate;
	}

	public void setZzStuffingDate(Date zzStuffingDate) {
		this.zzStuffingDate = zzStuffingDate;
	}

	public String getBelad() {
		return belad;
	}

	public void setBelad(String belad) {
		this.belad = belad;
	}

	public Date getDptBg() {
		return dptBg;
	}

	public void setDptBg(Date dptBg) {
		this.dptBg = dptBg;
	}

	public String getKnoTa() {
		return knoTa;
	}

	public void setKnoTa(String knoTa) {
		this.knoTa = knoTa;
	}

	public String getKnoTz() {
		return knoTz;
	}

	public void setKnoTz(String knoTz) {
		this.knoTz = knoTz;
	}

	public String getZzFreightModeDes() {
		return zzFreightModeDes;
	}

	public void setZzFreightModeDes(String zzFreightModeDes) {
		this.zzFreightModeDes = zzFreightModeDes;
	}

	public String getZzAirLineNo1() {
		return zzAirLineNo1;
	}

	public void setZzAirLineNo1(String zzAirLineNo1) {
		this.zzAirLineNo1 = zzAirLineNo1;
	}

	public String getZzAirLineNo2() {
		return zzAirLineNo2;
	}

	public void setZzAirLineNo2(String zzAirLineNo2) {
		this.zzAirLineNo2 = zzAirLineNo2;
	}

	public Date getFvEtDport() {
		return fvEtDport;
	}

	public void setFvEtDport(Date fvEtDport) {
		this.fvEtDport = fvEtDport;
	}

	public Date getFvEtAport() {
		return fvEtAport;
	}

	public void setFvEtAport(Date fvEtAport) {
		this.fvEtAport = fvEtAport;
	}

	public Date getMvEtDport() {
		return mvEtDport;
	}

	public void setMvEtDport(Date mvEtDport) {
		this.mvEtDport = mvEtDport;
	}

	public Date getMvEtAport() {
		return mvEtAport;
	}

	public void setMvEtAport(Date mvEtAport) {
		this.mvEtAport = mvEtAport;
	}
	
	
}
