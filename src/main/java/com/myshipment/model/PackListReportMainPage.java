package com.myshipment.model;

import java.util.List;

/**
 * @author virendra
 *
 */
public class PackListReportMainPage {
	private static final long serialVersionUID=-243135151L;
	private SellerDetails sellerDetails;
	private MakerDetails makerDetails;
	private String packingListNo;
	private String reportDate;
	private String hblNumber;
	private String soNumber;
	private BuyerDetails buyerDetails;
	private ConsigneeDetails consigneeDetails;
	private PackingListOrderHeader packingListOrderHeader;
	private PackingListOrderLineItem  packingListOrderLineItem;
	private List<PackingListItems> lstPackingListItems;
	private long totalPackingCarton;
	private long totalPackQty;
	private TotalOfPackingList totalOfPackingList;
	public SellerDetails getSellerDetails() {
		return sellerDetails;
	}
	public void setSellerDetails(SellerDetails sellerDetails) {
		this.sellerDetails = sellerDetails;
	}
	public MakerDetails getMakerDetails() {
		return makerDetails;
	}
	public void setMakerDetails(MakerDetails makerDetails) {
		this.makerDetails = makerDetails;
	}
	public String getPackingListNo() {
		return packingListNo;
	}
	public void setPackingListNo(String packingListNo) {
		this.packingListNo = packingListNo;
	}
	public String getReportDate() {
		return reportDate;
	}
	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	public BuyerDetails getBuyerDetails() {
		return buyerDetails;
	}
	public void setBuyerDetails(BuyerDetails buyerDetails) {
		this.buyerDetails = buyerDetails;
	}
	public ConsigneeDetails getConsigneeDetails() {
		return consigneeDetails;
	}
	public void setConsigneeDetails(ConsigneeDetails consigneeDetails) {
		this.consigneeDetails = consigneeDetails;
	}
	public PackingListOrderHeader getPackingListOrderHeader() {
		return packingListOrderHeader;
	}
	public void setPackingListOrderHeader(PackingListOrderHeader packingListOrderHeader) {
		this.packingListOrderHeader = packingListOrderHeader;
	}
	public PackingListOrderLineItem getPackingListOrderLineItem() {
		return packingListOrderLineItem;
	}
	public void setPackingListOrderLineItem(PackingListOrderLineItem packingListOrderLineItem) {
		this.packingListOrderLineItem = packingListOrderLineItem;
	}
	public List<PackingListItems> getLstPackingListItems() {
		return lstPackingListItems;
	}
	public void setLstPackingListItems(List<PackingListItems> lstPackingListItems) {
		this.lstPackingListItems = lstPackingListItems;
	}
	public long getTotalPackingCarton() {
		return totalPackingCarton;
	}
	public void setTotalPackingCarton(long totalPackingCarton) {
		this.totalPackingCarton = totalPackingCarton;
	}
	public long getTotalPackQty() {
		return totalPackQty;
	}
	public void setTotalPackQty(long totalPackQty) {
		this.totalPackQty = totalPackQty;
	}
	public TotalOfPackingList getTotalOfPackingList() {
		return totalOfPackingList;
	}
	public void setTotalOfPackingList(TotalOfPackingList totalOfPackingList) {
		this.totalOfPackingList = totalOfPackingList;
	}
	
	
	

}
