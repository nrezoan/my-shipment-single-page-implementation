/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class PreAlertSaveBLStatus {

	public String hblNumber;
	public String saveBLStatusError;
	public String bookingSupplierId;
	public String bookingBuyerId;
	public String bookingNotifyId;
	public String bookingSupplier;
	public String bookingBuyer;
	public String bookingNotify;
	public String isPreAlertDoc;
	
	public String getIsPreAlertDoc() {
		return isPreAlertDoc;
	}
	public void setIsPreAlertDoc(String isPreAlertDoc) {
		this.isPreAlertDoc = isPreAlertDoc;
	}
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getSaveBLStatusError() {
		return saveBLStatusError;
	}
	public void setSaveBLStatusError(String saveBLStatusError) {
		this.saveBLStatusError = saveBLStatusError;
	}
	public String getBookingSupplierId() {
		return bookingSupplierId;
	}
	public void setBookingSupplierId(String bookingSupplierId) {
		this.bookingSupplierId = bookingSupplierId;
	}
	public String getBookingBuyerId() {
		return bookingBuyerId;
	}
	public void setBookingBuyerId(String bookingBuyerId) {
		this.bookingBuyerId = bookingBuyerId;
	}
	public String getBookingNotifyId() {
		return bookingNotifyId;
	}
	public void setBookingNotifyId(String bookingNotifyId) {
		this.bookingNotifyId = bookingNotifyId;
	}
	public String getBookingSupplier() {
		return bookingSupplier;
	}
	public void setBookingSupplier(String bookingSupplier) {
		this.bookingSupplier = bookingSupplier;
	}
	public String getBookingBuyer() {
		return bookingBuyer;
	}
	public void setBookingBuyer(String bookingBuyer) {
		this.bookingBuyer = bookingBuyer;
	}
	public String getBookingNotify() {
		return bookingNotify;
	}
	public void setBookingNotify(String bookingNotify) {
		this.bookingNotify = bookingNotify;
	}
}
