package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */


public class CommercialInvoiceJsonOutputData {

	private List<ZbapiPacListHDTO> waHeader;	
	private List<ZbapiPacListHDTO> itCntryorig;	
	private List<ZbapiPacListHDTO> itCntrydest;	
	private List<ZbapiPacListHDTO> itVendor;	
	private List<ZbapiPacListHDTO> itVendorcountry;	
	private List<ZbapiPacListHDTO> itBuyer;	
	private List<ZbapiPacListHDTO> itBuyercountry;	
	private List<ZbapiPacListHDTO> itPol;	
	private List<ZbapiPacListHDTO> itPod;	
	private List<ZbapiPacListHDTO> itPodel;	
	private List<ZbapiPacListIDTO> waItem;	
	private List<ZbapiPacListHDTO> itVbkd;	
	private List<ZbapiPacListHDTO> itIssueBank;	
	private List<ZbapiPacListHDTO> itMaker;	
	private List<ZbapiPacListHDTO> itConsignee;	
	private List<ZbapiPacListHDTO> itMakercountry;	
	private List<ZbapiPacListHDTO> itConsigneecountry;	
	private List<ZbapiPacListHDTO> itSupplierBank;	
	private List<ZbapiPacListHDTO> itSbcountry;	
	private List<ZbapiPacListHDTO> itCostUnit;
	public List<ZbapiPacListHDTO> getWaHeader() {
		return waHeader;
	}
	public void setWaHeader(List<ZbapiPacListHDTO> waHeader) {
		this.waHeader = waHeader;
	}
	public List<ZbapiPacListHDTO> getItCntryorig() {
		return itCntryorig;
	}
	public void setItCntryorig(List<ZbapiPacListHDTO> itCntryorig) {
		this.itCntryorig = itCntryorig;
	}
	public List<ZbapiPacListHDTO> getItCntrydest() {
		return itCntrydest;
	}
	public void setItCntrydest(List<ZbapiPacListHDTO> itCntrydest) {
		this.itCntrydest = itCntrydest;
	}
	public List<ZbapiPacListHDTO> getItVendor() {
		return itVendor;
	}
	public void setItVendor(List<ZbapiPacListHDTO> itVendor) {
		this.itVendor = itVendor;
	}
	public List<ZbapiPacListHDTO> getItVendorcountry() {
		return itVendorcountry;
	}
	public void setItVendorcountry(List<ZbapiPacListHDTO> itVendorcountry) {
		this.itVendorcountry = itVendorcountry;
	}
	public List<ZbapiPacListHDTO> getItBuyer() {
		return itBuyer;
	}
	public void setItBuyer(List<ZbapiPacListHDTO> itBuyer) {
		this.itBuyer = itBuyer;
	}
	public List<ZbapiPacListHDTO> getItBuyercountry() {
		return itBuyercountry;
	}
	public void setItBuyercountry(List<ZbapiPacListHDTO> itBuyercountry) {
		this.itBuyercountry = itBuyercountry;
	}
	public List<ZbapiPacListHDTO> getItPol() {
		return itPol;
	}
	public void setItPol(List<ZbapiPacListHDTO> itPol) {
		this.itPol = itPol;
	}
	public List<ZbapiPacListHDTO> getItPod() {
		return itPod;
	}
	public void setItPod(List<ZbapiPacListHDTO> itPod) {
		this.itPod = itPod;
	}
	public List<ZbapiPacListHDTO> getItPodel() {
		return itPodel;
	}
	public void setItPodel(List<ZbapiPacListHDTO> itPodel) {
		this.itPodel = itPodel;
	}
	public List<ZbapiPacListIDTO> getWaItem() {
		return waItem;
	}
	public void setWaItem(List<ZbapiPacListIDTO> waItem) {
		this.waItem = waItem;
	}
	public List<ZbapiPacListHDTO> getItVbkd() {
		return itVbkd;
	}
	public void setItVbkd(List<ZbapiPacListHDTO> itVbkd) {
		this.itVbkd = itVbkd;
	}
	public List<ZbapiPacListHDTO> getItIssueBank() {
		return itIssueBank;
	}
	public void setItIssueBank(List<ZbapiPacListHDTO> itIssueBank) {
		this.itIssueBank = itIssueBank;
	}
	public List<ZbapiPacListHDTO> getItMaker() {
		return itMaker;
	}
	public void setItMaker(List<ZbapiPacListHDTO> itMaker) {
		this.itMaker = itMaker;
	}
	public List<ZbapiPacListHDTO> getItConsignee() {
		return itConsignee;
	}
	public void setItConsignee(List<ZbapiPacListHDTO> itConsignee) {
		this.itConsignee = itConsignee;
	}
	public List<ZbapiPacListHDTO> getItMakercountry() {
		return itMakercountry;
	}
	public void setItMakercountry(List<ZbapiPacListHDTO> itMakercountry) {
		this.itMakercountry = itMakercountry;
	}
	public List<ZbapiPacListHDTO> getItConsigneecountry() {
		return itConsigneecountry;
	}
	public void setItConsigneecountry(List<ZbapiPacListHDTO> itConsigneecountry) {
		this.itConsigneecountry = itConsigneecountry;
	}
	public List<ZbapiPacListHDTO> getItSupplierBank() {
		return itSupplierBank;
	}
	public void setItSupplierBank(List<ZbapiPacListHDTO> itSupplierBank) {
		this.itSupplierBank = itSupplierBank;
	}
	public List<ZbapiPacListHDTO> getItSbcountry() {
		return itSbcountry;
	}
	public void setItSbcountry(List<ZbapiPacListHDTO> itSbcountry) {
		this.itSbcountry = itSbcountry;
	}
	public List<ZbapiPacListHDTO> getItCostUnit() {
		return itCostUnit;
	}
	public void setItCostUnit(List<ZbapiPacListHDTO> itCostUnit) {
		this.itCostUnit = itCostUnit;
	}
	
	
}
