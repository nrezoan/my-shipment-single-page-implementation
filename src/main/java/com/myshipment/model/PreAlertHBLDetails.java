/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class PreAlertHBLDetails {

	private MyshipmentTrackHeaderBean trackHeaderBean;
	private PreAlertHBL preAlertHbl;
	
	public MyshipmentTrackHeaderBean getTrackHeaderBean() {
		return trackHeaderBean;
	}
	public void setTrackHeaderBean(MyshipmentTrackHeaderBean trackHeaderBean) {
		this.trackHeaderBean = trackHeaderBean;
	}
	public PreAlertHBL getPreAlertHbl() {
		return preAlertHbl;
	}
	public void setPreAlertHbl(PreAlertHBL preAlertHbl) {
		this.preAlertHbl = preAlertHbl;
	}
	
	
}
