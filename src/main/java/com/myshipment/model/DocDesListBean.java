package com.myshipment.model;
/*
 * @ Gufranur Rahman
 */


public class DocDesListBean {
	
	private String bezei;	
	private String auart;
	
	public String getBezei() {
		return bezei;
	}
	public void setBezei(String bezei) {
		this.bezei = bezei;
	}
	public String getAuart() {
		return auart;
	}
	public void setAuart(String auart) {
		this.auart = auart;
	}	
		
}
