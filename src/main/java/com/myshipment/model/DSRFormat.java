package com.myshipment.model;

public class DSRFormat {
	private String slNo;
	private String hawb1;
	private String hawb2;
	private String commInvoiceNo;
	private String commInvoice;
	private String costCenter;
	private String consolidate;
	private String invoiceDt;
	private String receiveDtInvoice;
	private String origin;
	private String mawb1;
	private String mawb2;
	private String transhipmnet;
	private String preAlertDt;
	private String status;
	private String destination;
	private String destcountry;
	private String palletsNo;
	private String stillage;
	private String gw;
	private String cw;
	private String cbm;
	private String dieAfter;
	private String shipmentMode;
	private String cargoHandoverDt;
	private String etd;
	private String eta;
	private String ata;
	private String atdOrigin;
	private String ataTranshipment;
	private String ataExTranshipment;
	private String etaAirlines;
	private String bookingDt;
	private String domT1;
	private String airT2;
	private String customer;
	private String crossingDieDt;
	private String remarks;
	private String leadTime;
	private String partRef;
	private String qty;
	private String invoiceNo;
	private String invoiceDt2;
	private String dasNo;
	private String awb;
	private String freightCost;
	private String hawbNo;
	private String mode;
	private String ConsigneeCost;
	private String supplierName;
	private String projectName;
	private String reasonForShipment;
	public String getSlNo() {
		return slNo;
	}
	public void setSlNo(String slNo) {
		this.slNo = slNo;
	}
	public String getHawb1() {
		return hawb1;
	}
	public void setHawb1(String hawb1) {
		this.hawb1 = hawb1;
	}
	public String getHawb2() {
		return hawb2;
	}
	public void setHawb2(String hawb2) {
		this.hawb2 = hawb2;
	}
	public String getCommInvoiceNo() {
		return commInvoiceNo;
	}
	public void setCommInvoiceNo(String commInvoiceNo) {
		this.commInvoiceNo = commInvoiceNo;
	}
	public String getCommInvoice() {
		return commInvoice;
	}
	public void setCommInvoice(String commInvoice) {
		this.commInvoice = commInvoice;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getConsolidate() {
		return consolidate;
	}
	public void setConsolidate(String consolidate) {
		this.consolidate = consolidate;
	}
	public String getInvoiceDt() {
		return invoiceDt;
	}
	public void setInvoiceDt(String invoiceDt) {
		this.invoiceDt = invoiceDt;
	}
	public String getReceiveDtInvoice() {
		return receiveDtInvoice;
	}
	public void setReceiveDtInvoice(String receiveDtInvoice) {
		this.receiveDtInvoice = receiveDtInvoice;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getMawb1() {
		return mawb1;
	}
	public void setMawb1(String mawb1) {
		this.mawb1 = mawb1;
	}
	public String getMawb2() {
		return mawb2;
	}
	public void setMawb2(String mawb2) {
		this.mawb2 = mawb2;
	}
	public String getTranshipmnet() {
		return transhipmnet;
	}
	public void setTranshipmnet(String transhipmnet) {
		this.transhipmnet = transhipmnet;
	}
	public String getPreAlertDt() {
		return preAlertDt;
	}
	public void setPreAlertDt(String preAlertDt) {
		this.preAlertDt = preAlertDt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestcountry() {
		return destcountry;
	}
	public void setDestcountry(String destcountry) {
		this.destcountry = destcountry;
	}
	public String getPalletsNo() {
		return palletsNo;
	}
	public void setPalletsNo(String palletsNo) {
		this.palletsNo = palletsNo;
	}
	public String getStillage() {
		return stillage;
	}
	public void setStillage(String stillage) {
		this.stillage = stillage;
	}
	public String getGw() {
		return gw;
	}
	public void setGw(String gw) {
		this.gw = gw;
	}
	public String getCw() {
		return cw;
	}
	public void setCw(String cw) {
		this.cw = cw;
	}
	public String getCbm() {
		return cbm;
	}
	public void setCbm(String cbm) {
		this.cbm = cbm;
	}
	public String getDieAfter() {
		return dieAfter;
	}
	public void setDieAfter(String dieAfter) {
		this.dieAfter = dieAfter;
	}
	public String getShipmentMode() {
		return shipmentMode;
	}
	public void setShipmentMode(String shipmentMode) {
		this.shipmentMode = shipmentMode;
	}
	public String getCargoHandoverDt() {
		return cargoHandoverDt;
	}
	public void setCargoHandoverDt(String cargoHandoverDt) {
		this.cargoHandoverDt = cargoHandoverDt;
	}
	public String getEtd() {
		return etd;
	}
	public void setEtd(String etd) {
		this.etd = etd;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public String getAta() {
		return ata;
	}
	public void setAta(String ata) {
		this.ata = ata;
	}
	public String getAtdOrigin() {
		return atdOrigin;
	}
	public void setAtdOrigin(String atdOrigin) {
		this.atdOrigin = atdOrigin;
	}
	public String getAtaTranshipment() {
		return ataTranshipment;
	}
	public void setAtaTranshipment(String ataTranshipment) {
		this.ataTranshipment = ataTranshipment;
	}
	public String getAtaExTranshipment() {
		return ataExTranshipment;
	}
	public void setAtaExTranshipment(String ataExTranshipment) {
		this.ataExTranshipment = ataExTranshipment;
	}
	public String getEtaAirlines() {
		return etaAirlines;
	}
	public void setEtaAirlines(String etaAirlines) {
		this.etaAirlines = etaAirlines;
	}
	public String getBookingDt() {
		return bookingDt;
	}
	public void setBookingDt(String bookingDt) {
		this.bookingDt = bookingDt;
	}
	public String getDomT1() {
		return domT1;
	}
	public void setDomT1(String domT1) {
		this.domT1 = domT1;
	}
	public String getAirT2() {
		return airT2;
	}
	public void setAirT2(String airT2) {
		this.airT2 = airT2;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getCrossingDieDt() {
		return crossingDieDt;
	}
	public void setCrossingDieDt(String crossingDieDt) {
		this.crossingDieDt = crossingDieDt;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getLeadTime() {
		return leadTime;
	}
	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}
	public String getPartRef() {
		return partRef;
	}
	public void setPartRef(String partRef) {
		this.partRef = partRef;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInvoiceDt2() {
		return invoiceDt2;
	}
	public void setInvoiceDt2(String invoiceDt2) {
		this.invoiceDt2 = invoiceDt2;
	}
	public String getDasNo() {
		return dasNo;
	}
	public void setDasNo(String dasNo) {
		this.dasNo = dasNo;
	}
	public String getAwb() {
		return awb;
	}
	public void setAwb(String awb) {
		this.awb = awb;
	}
	public String getFreightCost() {
		return freightCost;
	}
	public void setFreightCost(String freightCost) {
		this.freightCost = freightCost;
	}
	public String getHawbNo() {
		return hawbNo;
	}
	public void setHawbNo(String hawbNo) {
		this.hawbNo = hawbNo;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getConsigneeCost() {
		return ConsigneeCost;
	}
	public void setConsigneeCost(String consigneeCost) {
		ConsigneeCost = consigneeCost;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getReasonForShipment() {
		return reasonForShipment;
	}
	public void setReasonForShipment(String reasonForShipment) {
		this.reasonForShipment = reasonForShipment;
	}
	
	
	
	
	
	

}
