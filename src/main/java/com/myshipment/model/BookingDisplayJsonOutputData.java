package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class BookingDisplayJsonOutputData {

	private HeaderDisplayDTO headerDisplayDTO; 
	private List<ItemDTO> itemDtoLst;
	private List<PartnerDTO> partnerDtoLst;
	private List<OrderTextDTO> headerTextDtoLst;
	private List<OrderTextDTO> itemTextDtoLst;
	private List<BapiReturn3> bapiReturnLst;
	private PoBookingPacklistDetailBean packingDetail;
	private List<PoBookingPacklistColorBean> colors;
	private List<PoBookingPacklistSizeBean> sizeLst;
	
	public PoBookingPacklistDetailBean getPackingDetail() {
		return packingDetail;
	}
	public void setPackingDetail(PoBookingPacklistDetailBean packingDetail) {
		this.packingDetail = packingDetail;
	}
	public List<PoBookingPacklistColorBean> getColors() {
		return colors;
	}
	public void setColors(List<PoBookingPacklistColorBean> colors) {
		this.colors = colors;
	}
	public List<PoBookingPacklistSizeBean> getSizeLst() {
		return sizeLst;
	}
	public void setSizeLst(List<PoBookingPacklistSizeBean> sizeLst) {
		this.sizeLst = sizeLst;
	}
	
	public HeaderDisplayDTO getHeaderDisplayDTO() {
		return headerDisplayDTO;
	}
	public void setHeaderDisplayDTO(HeaderDisplayDTO headerDisplayDTO) {
		this.headerDisplayDTO = headerDisplayDTO;
	}
	public List<ItemDTO> getItemDtoLst() {
		return itemDtoLst;
	}
	public void setItemDtoLst(List<ItemDTO> itemDtoLst) {
		this.itemDtoLst = itemDtoLst;
	}
	public List<PartnerDTO> getPartnerDtoLst() {
		return partnerDtoLst;
	}
	public void setPartnerDtoLst(List<PartnerDTO> partnerDtoLst) {
		this.partnerDtoLst = partnerDtoLst;
	}
	public List<OrderTextDTO> getHeaderTextDtoLst() {
		return headerTextDtoLst;
	}
	public void setHeaderTextDtoLst(List<OrderTextDTO> headerTextDtoLst) {
		this.headerTextDtoLst = headerTextDtoLst;
	}
	public List<OrderTextDTO> getItemTextDtoLst() {
		return itemTextDtoLst;
	}
	public void setItemTextDtoLst(List<OrderTextDTO> itemTextDtoLst) {
		this.itemTextDtoLst = itemTextDtoLst;
	}
	public List<BapiReturn3> getBapiReturnLst() {
		return bapiReturnLst;
	}
	public void setBapiReturnLst(List<BapiReturn3> bapiReturnLst) {
		this.bapiReturnLst = bapiReturnLst;
	}
	
	
}
