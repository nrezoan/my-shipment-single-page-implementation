package com.myshipment.model;

public class ShippingOrderQR {
	private String so;
	private String hbl;
	private String shipper;
	private String shippername;
	private String buyer;
	private String buyername;
	private String pol;
	private String pod;
	private String expchdt;
	private String salesorg;
	private String compcode;
	private String fvsl;
	private String fetd;
	private String mvsl;
	private String meta;
	public String getSo() {
		return so;
	}
	public String getHbl() {
		return hbl;
	}
	public String getShipper() {
		return shipper;
	}
	public String getShippername() {
		return shippername;
	}
	public String getBuyer() {
		return buyer;
	}
	public String getBuyername() {
		return buyername;
	}
	public String getPol() {
		return pol;
	}
	public String getPod() {
		return pod;
	}
	public String getExpchdt() {
		return expchdt;
	}
	public String getSalesorg() {
		return salesorg;
	}
	public String getCompcode() {
		return compcode;
	}
	public String getFvsl() {
		return fvsl;
	}
	public String getFetd() {
		return fetd;
	}
	public String getMvsl() {
		return mvsl;
	}
	public String getMeta() {
		return meta;
	}
	public void setSo(String so) {
		this.so = so;
	}
	public void setHbl(String hbl) {
		this.hbl = hbl;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public void setShippername(String shippername) {
		this.shippername = shippername;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public void setBuyername(String buyername) {
		this.buyername = buyername;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public void setExpchdt(String expchdt) {
		this.expchdt = expchdt;
	}
	public void setSalesorg(String salesorg) {
		this.salesorg = salesorg;
	}
	public void setCompcode(String compcode) {
		this.compcode = compcode;
	}
	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}
	public void setFetd(String fetd) {
		this.fetd = fetd;
	}
	public void setMvsl(String mvsl) {
		this.mvsl = mvsl;
	}
	public void setMeta(String meta) {
		this.meta = meta;
	}
	
}
