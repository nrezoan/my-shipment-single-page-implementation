
package com.myshipment.model;

/*
 * @ Gufranur Rahman
 */

public class BapiRet1 {

	
	private String type;	
	private String code;
	private String message;
	private String logNo;
	private String log_msg_no;
	private String messageV1;
	private String messageV2;
	private String messageV3;
	private String messageV4;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLogNo() {
		return logNo;
	}
	public void setLogNo(String logNo) {
		this.logNo = logNo;
	}
	public String getLog_msg_no() {
		return log_msg_no;
	}
	public void setLog_msg_no(String log_msg_no) {
		this.log_msg_no = log_msg_no;
	}
	public String getMessageV1() {
		return messageV1;
	}
	public void setMessageV1(String messageV1) {
		this.messageV1 = messageV1;
	}
	public String getMessageV2() {
		return messageV2;
	}
	public void setMessageV2(String messageV2) {
		this.messageV2 = messageV2;
	}
	public String getMessageV3() {
		return messageV3;
	}
	public void setMessageV3(String messageV3) {
		this.messageV3 = messageV3;
	}
	public String getMessageV4() {
		return messageV4;
	}
	public void setMessageV4(String messageV4) {
		this.messageV4 = messageV4;
	}

	
}
