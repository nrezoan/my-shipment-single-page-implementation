/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;

public class ShipmentStatusDetails {

	private String blNo;
	private String nameBuyerSupplier;
	private Date blDate;
	private BigDecimal totalWeight;
	private BigDecimal totalCBM;
	private BigDecimal totalQuantity;
	private Date stuffingDate;
	private String mblNo;
	private String shippingLine;
	private String nameAgentSupplier;
	private String placeOfReceipt;
	private String pol;
	private String pod;
	private Date etd;
	private Date eta;
	private Date gr_date;
	private Date shipmentDate;
	
	public Date getBlDate() {
		return blDate;
	}
	public void setBlDate(Date blDate) {
		this.blDate = blDate;
	}
	public Date getEtd() {
		return etd;
	}
	public void setEtd(Date etd) {
		this.etd = etd;
	}
	public Date getEta() {
		return eta;
	}
	public void setEta(Date eta) {
		this.eta = eta;
	}
	public Date getGr_date() {
		return gr_date;
	}
	public void setGr_date(Date gr_date) {
		this.gr_date = gr_date;
	}
	public Date getShipmentDate() {
		return shipmentDate;
	}
	public void setShipmentDate(Date shipmentDate) {
		this.shipmentDate = shipmentDate;
	}
	public BigDecimal getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(BigDecimal totalWeight) {
		this.totalWeight = totalWeight;
	}
	public BigDecimal getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(BigDecimal totalCBM) {
		this.totalCBM = totalCBM;
	}
	public BigDecimal getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(BigDecimal totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Date getStuffingDate() {
		return stuffingDate;
	}
	public void setStuffingDate(Date stuffingDate) {
		this.stuffingDate = stuffingDate;
	}
	public String getMblNo() {
		return mblNo;
	}
	public void setMblNo(String mblNo) {
		this.mblNo = mblNo;
	}
	public String getShippingLine() {
		return shippingLine;
	}
	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}
	public String getPlaceOfReceipt() {
		return placeOfReceipt;
	}
	public void setPlaceOfReceipt(String placeOfReceipt) {
		this.placeOfReceipt = placeOfReceipt;
	}
	public String getBlNo() {
		return blNo;
	}
	public void setBlNo(String blNo) {
		this.blNo = blNo;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getNameBuyerSupplier() {
		return nameBuyerSupplier;
	}
	public void setNameBuyerSupplier(String nameBuyerSupplier) {
		this.nameBuyerSupplier = nameBuyerSupplier;
	}
	public String getNameAgentSupplier() {
		return nameAgentSupplier;
	}
	public void setNameAgentSupplier(String nameAgentSupplier) {
		this.nameAgentSupplier = nameAgentSupplier;
	}
	
}
