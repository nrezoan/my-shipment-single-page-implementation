package com.myshipment.dto;

public class SalesGroup {
	private String salesGrp;	
	private String salesgrpText;	
	private String salesOff;	
	public String getSalesGrp() {
		return salesGrp;
	}
	public void setSalesGrp(String salesGrp) {
		this.salesGrp = salesGrp;
	}
	public String getSalesgrpText() {
		return salesgrpText;
	}
	public void setSalesgrpText(String salesgrpText) {
		this.salesgrpText = salesgrpText;
	}
	public String getSalesOff() {
		return salesOff;
	}
	public void setSalesOff(String salesOff) {
		this.salesOff = salesOff;
	}
	public String getSalesOffText() {
		return salesOffText;
	}
	public void setSalesOffText(String salesOffText) {
		this.salesOffText = salesOffText;
	}
	private String salesOffText;

}
