package com.myshipment.dto;

/*
 * @ Ranjeet Kumar
 */
public class FileDTO {

	private String fileName;
	private boolean approved;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}


}
