package com.myshipment.dto;

import java.util.Date;
/*
 * @ Ranjeet Kumar
 */
public class SearchPoDTO {

	private String poNo;
	
	private String toDate;
	private String fromDate;
	
	private String redirectedRequest;
	
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}


	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getRedirectedRequest() {
		return redirectedRequest;
	}
	public void setRedirectedRequest(String redirectedRequest) {
		this.redirectedRequest = redirectedRequest;
	}	

	
}
