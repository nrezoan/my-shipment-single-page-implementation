package com.myshipment.dto;

import java.util.Set;

public class SalesOrgTree {

	private String salesOrg;	
	private String salesOrgText;
	private Set<DistributionChannel> lstDistChnl;
	
	/*private Set<SalesGroup> lstSalesGrp;*/
	
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getSalesOrgText() {
		return salesOrgText;
	}
	public void setSalesOrgText(String salesOrgText) {
		this.salesOrgText = salesOrgText;
	}
	public Set<DistributionChannel> getLstDistChnl() {
		return lstDistChnl;
	}
	public void setLstDistChnl(Set<DistributionChannel> lstDistChnl) {
		this.lstDistChnl = lstDistChnl;
	}
	
	/*public Set<SalesGroup> getLstSalesGrp() {
		return lstSalesGrp;
	}
	public void setLstSalesGrp(Set<SalesGroup> lstSalesGrp) {
		this.lstSalesGrp = lstSalesGrp;
	}*/
	
		
	
	

}
