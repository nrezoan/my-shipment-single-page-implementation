package com.myshipment.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * @author virendra
 *
 */
public class BuyersSuppliersmap {
	private static final Long serialVersionUID = -4546784366l;
	private Map<String, String> docTypes = new HashMap<String, String>();
	private Map<String, String> buyers = new HashMap<String, String>();
	private Map<String, String> buyersBank = new HashMap<String, String>();
	private Map<String, String> shippersBank = new HashMap<String, String>();
	private Map<String, String> shippersAddress = new HashMap<String, String>();
	private Map<String, String> localBuyingHouse = new HashMap<String, String>();
	private Map<String, String> tos = new HashMap<String, String>();
	private Map<String, String> freightMode = new HashMap<String, String>();
	private Map<String, String> transType = new HashMap<String, String>();
	private Map<String, String> hblInit = new HashMap<String, String>();
	private Map<String, String> portLink = new HashMap<String, String>();
	private Map<String, String> portLoad = new HashMap<String, String>();
	private Map<String, String> unit = new HashMap<String, String>();
	private Map<String, String> cartonUnit = new HashMap<String, String>();
	private Map<String, String> shipppingLine = new HashMap<String, String>();
	private Map<String, String> shipper = new HashMap<String, String>();
	private Map<String, String> consignee = new HashMap<String, String>();
	private Map<String, String> notifyParty = new HashMap<String, String>();
	private Map<String, String> shipmentMode = new HashMap<String, String>();
	private Map<String, String> containerMode = new HashMap<String, String>();

	public Map<String, String> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(Map<String, String> docTypes) {
		this.docTypes = docTypes;
	}

	public Map<String, String> getBuyers() {
		return buyers;
	}

	public void setBuyers(Map<String, String> buyers) {
		this.buyers = buyers;
	}

	public Map<String, String> getBuyersBank() {
		return buyersBank;
	}

	public void setBuyersBank(Map<String, String> buyersBank) {
		this.buyersBank = buyersBank;
	}

	public Map<String, String> getShippersBank() {
		return shippersBank;
	}

	public void setShippersBank(Map<String, String> shippersBank) {
		this.shippersBank = shippersBank;
	}

	public Map<String, String> getShippersAddress() {
		return shippersAddress;
	}

	public void setShippersAddress(Map<String, String> shippersAddress) {
		this.shippersAddress = shippersAddress;
	}

	public Map<String, String> getLocalBuyingHouse() {
		return localBuyingHouse;
	}

	public void setLocalBuyingHouse(Map<String, String> localBuyingHouse) {
		this.localBuyingHouse = localBuyingHouse;
	}

	public Map<String, String> getTos() {
		return tos;
	}

	public void setTos(Map<String, String> tos) {
		this.tos = tos;
	}

	public Map<String, String> getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(Map<String, String> freightMode) {
		this.freightMode = freightMode;
	}

	public Map<String, String> getTransType() {
		return transType;
	}

	public void setTransType(Map<String, String> transType) {
		this.transType = transType;
	}

	public Map<String, String> getHblInit() {
		return hblInit;
	}

	public void setHblInit(Map<String, String> hblInit) {
		this.hblInit = hblInit;
	}

	public Map<String, String> getPortLink() {
		return portLink;
	}

	public void setPortLink(Map<String, String> portLink) {
		this.portLink = portLink;
	}

	public Map<String, String> getPortLoad() {
		return portLoad;
	}

	public void setPortLoad(Map<String, String> portLoad) {
		this.portLoad = portLoad;
	}

	public Map<String, String> getUnit() {
		return unit;
	}

	public void setUnit(Map<String, String> unit) {
		this.unit = unit;
	}

	public Map<String, String> getCartonUnit() {
		return cartonUnit;
	}

	public void setCartonUnit(Map<String, String> cartonUnit) {
		this.cartonUnit = cartonUnit;
	}

	public Map<String, String> getShipppingLine() {
		return shipppingLine;
	}

	public void setShipppingLine(Map<String, String> shipppingLine) {
		this.shipppingLine = shipppingLine;
	}

	public Map<String, String> getShipper() {
		return shipper;
	}

	public void setShipper(Map<String, String> shipper) {
		this.shipper = shipper;
	}

	public Map<String, String> getConsignee() {
		return consignee;
	}

	public void setConsignee(Map<String, String> consignee) {
		this.consignee = consignee;
	}

	public Map<String, String> getNotifyParty() {
		return notifyParty;
	}

	public void setNotifyParty(Map<String, String> notifyParty) {
		this.notifyParty = notifyParty;
	}

	public Map<String, String> getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(Map<String, String> shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public Map<String, String> getContainerMode() {
		return containerMode;
	}

	public void setContainerMode(Map<String, String> containerMode) {
		this.containerMode = containerMode;
	}

	@Override
	public String toString() {
		return "BuyersSuppliersmap [docTypes=" + docTypes + ", buyers=" + buyers + ", buyersBank=" + buyersBank
				+ ", shippersBank=" + shippersBank + ", shippersAddress=" + shippersAddress + ", localBuyingHouse="
				+ localBuyingHouse + ", tos=" + tos + ", freightMode=" + freightMode + ", transType=" + transType
				+ ", hblInit=" + hblInit + ", portLink=" + portLink + ", portLoad=" + portLoad + ", unit=" + unit
				+ ", cartonUnit=" + cartonUnit + ", shipppingLine=" + shipppingLine + ", shipper=" + shipper
				+ ", consignee=" + consignee + ", notifyParty=" + notifyParty + ", shipmentMode=" + shipmentMode
				+ ", containerMode=" + containerMode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyers == null) ? 0 : buyers.hashCode());
		result = prime * result + ((buyersBank == null) ? 0 : buyersBank.hashCode());
		result = prime * result + ((cartonUnit == null) ? 0 : cartonUnit.hashCode());
		result = prime * result + ((consignee == null) ? 0 : consignee.hashCode());
		result = prime * result + ((containerMode == null) ? 0 : containerMode.hashCode());
		result = prime * result + ((docTypes == null) ? 0 : docTypes.hashCode());
		result = prime * result + ((freightMode == null) ? 0 : freightMode.hashCode());
		result = prime * result + ((hblInit == null) ? 0 : hblInit.hashCode());
		result = prime * result + ((localBuyingHouse == null) ? 0 : localBuyingHouse.hashCode());
		result = prime * result + ((notifyParty == null) ? 0 : notifyParty.hashCode());
		result = prime * result + ((portLink == null) ? 0 : portLink.hashCode());
		result = prime * result + ((portLoad == null) ? 0 : portLoad.hashCode());
		result = prime * result + ((shipmentMode == null) ? 0 : shipmentMode.hashCode());
		result = prime * result + ((shipper == null) ? 0 : shipper.hashCode());
		result = prime * result + ((shippersAddress == null) ? 0 : shippersAddress.hashCode());
		result = prime * result + ((shippersBank == null) ? 0 : shippersBank.hashCode());
		result = prime * result + ((shipppingLine == null) ? 0 : shipppingLine.hashCode());
		result = prime * result + ((tos == null) ? 0 : tos.hashCode());
		result = prime * result + ((transType == null) ? 0 : transType.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BuyersSuppliersmap other = (BuyersSuppliersmap) obj;
		if (buyers == null) {
			if (other.buyers != null)
				return false;
		} else if (!buyers.equals(other.buyers))
			return false;
		if (buyersBank == null) {
			if (other.buyersBank != null)
				return false;
		} else if (!buyersBank.equals(other.buyersBank))
			return false;
		if (cartonUnit == null) {
			if (other.cartonUnit != null)
				return false;
		} else if (!cartonUnit.equals(other.cartonUnit))
			return false;
		if (consignee == null) {
			if (other.consignee != null)
				return false;
		} else if (!consignee.equals(other.consignee))
			return false;
		if (containerMode == null) {
			if (other.containerMode != null)
				return false;
		} else if (!containerMode.equals(other.containerMode))
			return false;
		if (docTypes == null) {
			if (other.docTypes != null)
				return false;
		} else if (!docTypes.equals(other.docTypes))
			return false;
		if (freightMode == null) {
			if (other.freightMode != null)
				return false;
		} else if (!freightMode.equals(other.freightMode))
			return false;
		if (hblInit == null) {
			if (other.hblInit != null)
				return false;
		} else if (!hblInit.equals(other.hblInit))
			return false;
		if (localBuyingHouse == null) {
			if (other.localBuyingHouse != null)
				return false;
		} else if (!localBuyingHouse.equals(other.localBuyingHouse))
			return false;
		if (notifyParty == null) {
			if (other.notifyParty != null)
				return false;
		} else if (!notifyParty.equals(other.notifyParty))
			return false;
		if (portLink == null) {
			if (other.portLink != null)
				return false;
		} else if (!portLink.equals(other.portLink))
			return false;
		if (portLoad == null) {
			if (other.portLoad != null)
				return false;
		} else if (!portLoad.equals(other.portLoad))
			return false;
		if (shipmentMode == null) {
			if (other.shipmentMode != null)
				return false;
		} else if (!shipmentMode.equals(other.shipmentMode))
			return false;
		if (shipper == null) {
			if (other.shipper != null)
				return false;
		} else if (!shipper.equals(other.shipper))
			return false;
		if (shippersAddress == null) {
			if (other.shippersAddress != null)
				return false;
		} else if (!shippersAddress.equals(other.shippersAddress))
			return false;
		if (shippersBank == null) {
			if (other.shippersBank != null)
				return false;
		} else if (!shippersBank.equals(other.shippersBank))
			return false;
		if (shipppingLine == null) {
			if (other.shipppingLine != null)
				return false;
		} else if (!shipppingLine.equals(other.shipppingLine))
			return false;
		if (tos == null) {
			if (other.tos != null)
				return false;
		} else if (!tos.equals(other.tos))
			return false;
		if (transType == null) {
			if (other.transType != null)
				return false;
		} else if (!transType.equals(other.transType))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}

}
