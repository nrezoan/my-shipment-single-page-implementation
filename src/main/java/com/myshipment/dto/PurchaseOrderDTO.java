package com.myshipment.dto;

import java.util.List;

import com.myshipment.model.CarrierMaster;

public class PurchaseOrderDTO {
	
	private Long po_id;
	private Long seg_id;
	private String vc_po_no;
	private String nu_client_code;
	private String vc_product_no;
	private String vc_sku_no;
	private String vc_color = "";
	private String vc_size = "";
	private String sales_org;
	private List<CarrierMaster> carrierMaster;
	private String carrierSchedule;
	private String carrierId;
	private String supplier_code;
	private Double vc_tot_pcs;
	public Double getVc_tot_pcs() {
		return vc_tot_pcs;
	}

	public void setVc_tot_pcs(Double vc_tot_pcs) {
		this.vc_tot_pcs = vc_tot_pcs;
	}

	private String po_creation_date;

	public Long getPo_id() {
		return po_id;
	}

	public List<CarrierMaster> getCarrierMaster() {
		return carrierMaster;
	}

	public void setCarrierMaster(List<CarrierMaster> carrierMaster) {
		this.carrierMaster = carrierMaster;
	}

	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}

	public Long getSeg_id() {
		return seg_id;
	}

	public void setSeg_id(Long seg_id) {
		this.seg_id = seg_id;
	}

	public String getVc_po_no() {
		return vc_po_no;
	}

	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}

	public String getNu_client_code() {
		return nu_client_code;
	}

	public void setNu_client_code(String nu_client_code) {
		this.nu_client_code = nu_client_code;
	}

	public String getVc_product_no() {
		return vc_product_no;
	}

	public void setVc_product_no(String vc_product_no) {
		this.vc_product_no = vc_product_no;
	}

	public String getVc_sku_no() {
		return vc_sku_no;
	}

	public void setVc_sku_no(String vc_sku_no) {
		this.vc_sku_no = vc_sku_no;
	}

	public String getVc_color() {
		return vc_color;
	}

	public void setVc_color(String vc_color) {
		this.vc_color = vc_color;
	}

	public String getVc_size() {
		return vc_size;
	}

	public void setVc_size(String vc_size) {
		this.vc_size = vc_size;
	}

	public String getSales_org() {
		return sales_org;
	}

	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}

	public String getCarrierSchedule() {
		return carrierSchedule;
	}

	public void setCarrierSchedule(String carrierSchedule) {
		this.carrierSchedule = carrierSchedule;
	}
	
	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getSupplier_code() {
		return supplier_code;
	}

	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}

	public String getPo_creation_date() {
		return po_creation_date;
	}

	public void setPo_creation_date(String po_creation_date) {
		this.po_creation_date = po_creation_date;
	}
	


	
}
